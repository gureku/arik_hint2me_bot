/*
SQLyog Community v13.1.2 (64 bit)
MySQL - 5.6.43-84.3-log : Database - gurekucom_test
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`gurekucom_test` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `gurekucom_test`;

/*Table structure for table `arik_ci_sessions` */

DROP TABLE IF EXISTS `arik_ci_sessions`;

CREATE TABLE `arik_ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `arik_ci_sessions` */

insert  into `arik_ci_sessions`(`id`,`ip_address`,`timestamp`,`data`) values 
('019bf40b468f03a5c7a71a527b9010cf5216450e','91.108.6.126',1651736242,'__ci_last_regenerate|i:1651736242;'),
('067a7c1490ba9f5fc912280297c74f39ec3203a6','91.108.6.126',1651737223,'__ci_last_regenerate|i:1651737223;'),
('0981281c045286f3c3507681b64611661b588e6c','91.108.6.126',1651738054,'__ci_last_regenerate|i:1651738054;'),
('0a73f05ee7f4d03996f0016385984d39b7be047d','91.108.6.126',1651738087,'__ci_last_regenerate|i:1651738087;'),
('0d92b79a0137a9d19e131297015b099056b0ec0c','91.108.6.126',1651738711,'__ci_last_regenerate|i:1651738711;'),
('12db28ff3af1fb33b35afbdd53137a10271888b1','91.108.6.126',1651737340,'__ci_last_regenerate|i:1651737340;'),
('12fbf39cf8b3d136077a7a18d6901ec3c6bc00d7','91.108.6.126',1651738161,'__ci_last_regenerate|i:1651738161;'),
('1743af222cebc73945b31bd08009de9ab1ac2004','91.108.6.126',1651738447,'__ci_last_regenerate|i:1651738447;'),
('177ed0e8c1d6777fc6e6a28fcd8df6558120dca0','91.108.6.126',1651738449,'__ci_last_regenerate|i:1651738449;'),
('1b6786de9a6cce9c1385120d980ebf14f9d56416','91.108.6.126',1651723892,'__ci_last_regenerate|i:1651723892;'),
('1b9f7b4abd3e8ee903db9d4094e3265b40a4c343','91.108.6.126',1651737975,'__ci_last_regenerate|i:1651737975;'),
('1ca8004f0bfc04322b4e70171d10f410d8dcb9a9','91.108.6.126',1651724103,'__ci_last_regenerate|i:1651724102;'),
('2135f3ac2d51fb4d3d6caac50a7d08f73bad2065','91.108.6.126',1651723904,'__ci_last_regenerate|i:1651723904;'),
('2459ed3e0598196d7cb12f884487a5d38a16184c','91.108.6.126',1651737335,'__ci_last_regenerate|i:1651737335;'),
('2462f1aa800aa303cc65bcdd61bf391f29d311f7','91.108.6.126',1651737275,'__ci_last_regenerate|i:1651737275;'),
('2b3935ea3b67f8151fed6fc8ca5305c50a2eb343','91.108.6.126',1651738260,'__ci_last_regenerate|i:1651738260;'),
('2c34aafb20d023626ec20180683c7f372613fb94','91.108.6.126',1651723797,'__ci_last_regenerate|i:1651723797;'),
('2df15fddf343a242a4241628220e645bb160d4fe','91.108.6.126',1651738703,'__ci_last_regenerate|i:1651738703;'),
('2e5b3cf0397d43535985faed38dc735288df2d32','91.108.6.126',1651738735,'__ci_last_regenerate|i:1651738735;'),
('2f48e06dccbedc8f7c13439b0dc3ce2c7a7edad1','91.108.6.126',1651738215,'__ci_last_regenerate|i:1651738215;'),
('3036777728ffbde4e1a90b140f55fe1dbeef0c73','91.108.6.126',1651738765,'__ci_last_regenerate|i:1651738765;'),
('3548549f7dc62810ac88add7a391910e2d20d413','91.108.6.126',1651732890,'__ci_last_regenerate|i:1651732890;'),
('39472cd66cfd5ffffc337de442716ddf0803923a','91.108.6.126',1651738162,'__ci_last_regenerate|i:1651738162;'),
('39e4f7e6816475bf3ad7290396f6d53a383424e7','91.108.6.126',1651732878,'__ci_last_regenerate|i:1651732878;'),
('3ae9b8b3619e2c2c3ca43ce9e45908841cf73ce3','91.108.6.126',1651737217,'__ci_last_regenerate|i:1651737217;'),
('3b5c88a1b9e90eb4e3c8554ead78c9ec8b06c7ec','91.108.6.126',1651736967,'__ci_last_regenerate|i:1651736967;'),
('41557dd81e7b66581e5fea13b1577ad1ec1264c4','91.108.6.126',1651723553,'__ci_last_regenerate|i:1651723553;'),
('4bd5c8ad850112bccc6446624945b4cbcb7cda78','91.108.6.126',1651738304,'__ci_last_regenerate|i:1651738304;'),
('4cacdec640ffb071357b0b546d5b52bf108b4963','91.108.6.126',1651738720,'__ci_last_regenerate|i:1651738720;'),
('4e6efb25b1850c3480154ef284649a9c55b409f8','91.108.6.126',1651736035,'__ci_last_regenerate|i:1651736035;'),
('50dddb318264618752b45e803873042c5345db73','91.108.6.126',1651738332,'__ci_last_regenerate|i:1651738332;'),
('51765e9990779d959ff9ec50f6fa9b4c49d62f65','91.108.6.126',1651735647,'__ci_last_regenerate|i:1651735647;'),
('525f7e422c56a9dbe19a55f09668d1f2536c8de5','91.108.6.126',1651732825,'__ci_last_regenerate|i:1651732825;'),
('54dfb655ce9297178c24246b71ebeb1c1931dc96','91.108.6.126',1651738258,'__ci_last_regenerate|i:1651738258;'),
('5a6167ae1f6d3bc88777f225ccd161c1e661b9dc','91.108.6.126',1651736094,'__ci_last_regenerate|i:1651736094;'),
('65508d309dc84e16479ce4fde5e0ed5a1c9ea661','91.108.6.126',1651738536,'__ci_last_regenerate|i:1651738536;'),
('6a5180de9baf7b8c1a6d0fc6fea1af0bc8f06a2f','91.108.6.126',1651724034,'__ci_last_regenerate|i:1651724034;'),
('6acc6e182f6d1fee90293b8e1b2cb0912c7cdeb1','91.108.6.126',1651738155,'__ci_last_regenerate|i:1651738155;'),
('6afac89219132870abbde75d48fb652b8b2a27b5','91.108.6.126',1651737898,'__ci_last_regenerate|i:1651737898;'),
('6c02aea9bf8842bd8ab4e3ebcd2468acbbc5db2c','91.108.6.126',1651724025,'__ci_last_regenerate|i:1651724025;'),
('6dbe0473515a496054e5dd1db188fe7ca256766d','91.108.6.126',1651736181,'__ci_last_regenerate|i:1651736181;'),
('6e292544e097a611cd46ffe91b01ab4f3277f8b2','91.108.6.126',1651723664,'__ci_last_regenerate|i:1651723664;'),
('6eb4eb6affa9ea341a7d7adc48a3da8874ecab17','91.108.6.126',1651738517,'__ci_last_regenerate|i:1651738517;'),
('6fc5bd2f439e12d7e31d74cdd5bf4a985684792b','91.108.6.126',1651732884,'__ci_last_regenerate|i:1651732884;'),
('702b73be3582cc1d0025cedeb456a821c49b22ff','91.108.6.126',1651738241,'__ci_last_regenerate|i:1651738241;'),
('72de639fd99fae5a42bc6d0acd3d6b4da818f628','91.108.6.126',1651728169,'__ci_last_regenerate|i:1651728169;'),
('7fb86732e0638c14b627e28735edcbb59eec95e2','91.108.6.126',1651738009,'__ci_last_regenerate|i:1651738009;'),
('82dbfdf8afa4f232ac9103d5a62559777f6f13b9','91.108.6.126',1651737104,'__ci_last_regenerate|i:1651737104;'),
('8443bb873bc9986cdd67dc778c7f789fcdfc3e71','91.108.6.126',1651739385,'__ci_last_regenerate|i:1651739385;'),
('8485531e01e9095b02cd8adec56bad716298e823','91.108.6.126',1651735898,'__ci_last_regenerate|i:1651735898;'),
('9087f8201883dedef5bc189733213061a295044f','91.108.6.126',1651738388,'__ci_last_regenerate|i:1651738388;'),
('91b8378dd36aca7ca243b8e9a236db1ca86e33ee','91.108.6.126',1651724100,'__ci_last_regenerate|i:1651724100;'),
('93026801208a2c3dd345949fa546aa9ce4d1a4f1','91.108.6.126',1651736001,'__ci_last_regenerate|i:1651736001;'),
('94713740ff9d9ad5f76cba5d7645356dc3e98218','91.108.6.126',1651738250,'__ci_last_regenerate|i:1651738250;'),
('96fdb1088721285560acc856a6306473fc42157c','91.108.6.126',1651732893,'__ci_last_regenerate|i:1651732893;'),
('99a8b09ce25c72031a20d4d91d2037108ceef7d9','91.108.6.126',1651724032,'__ci_last_regenerate|i:1651724032;'),
('9da9734b371c0f85dfa0e4bfdbe6bf72701771b8','91.108.6.126',1651738371,'__ci_last_regenerate|i:1651738371;'),
('9ed080223ba8164b0a8adbede80d0b05ff48948c','91.108.6.126',1651724031,'__ci_last_regenerate|i:1651724031;'),
('a02e071c3972decf01039b359be44a4414bb88a5','91.108.6.126',1651737162,'__ci_last_regenerate|i:1651737162;'),
('a05981533447ee18a85c5b8097c409f63be63cc3','91.108.6.126',1651738781,'__ci_last_regenerate|i:1651738781;'),
('a0bf005fad0a7351e5f1055d295869e9e1847c26','91.108.6.126',1651723705,'__ci_last_regenerate|i:1651723705;'),
('a4ccc057ca9f19f04397c2ddd4920132b880a121','91.108.6.126',1651734938,'__ci_last_regenerate|i:1651734938;'),
('a98b995b9e5974226af74f8fecad86a610562ae8','91.108.6.126',1651738275,'__ci_last_regenerate|i:1651738275;'),
('aa72a11db710f7b11c1b0f60289f9b7a7b24cca5','91.108.6.126',1651739339,'__ci_last_regenerate|i:1651739339;'),
('ab7cbce9f460501bc567ac1b7013683e8e822bc9','91.108.6.126',1651724104,'__ci_last_regenerate|i:1651724104;'),
('aba6944799d3d600e9fc74efaa7c569787b1ae08','91.108.6.126',1651738332,'__ci_last_regenerate|i:1651738332;'),
('ac115530e0dea875b1df0af3500acb70751e8c11','91.108.6.126',1651738728,'__ci_last_regenerate|i:1651738728;'),
('ac8ae6854f5fe6d6525ee0a7db1a23b6bd0a3ab9','91.108.6.126',1651737203,'__ci_last_regenerate|i:1651737203;'),
('aca37ea28edab5b4b76f612220d30e3ad6c9de30','91.108.6.126',1651724096,'__ci_last_regenerate|i:1651724096;'),
('af0e8af422c088b1a073f02ef42b002af147e857','91.108.6.126',1651738330,'__ci_last_regenerate|i:1651738330;'),
('afe7b2b0fd6fbae3af0eab45db0113b2d9b568ac','91.108.6.126',1651738375,'__ci_last_regenerate|i:1651738375;'),
('b05c2f6ab662d136b1e452b30ebc77183cef3852','91.108.6.126',1651736169,'__ci_last_regenerate|i:1651736169;'),
('b11afd5c82305c61b32e0c1cc2a590b04aa61038','91.108.6.126',1651738779,'__ci_last_regenerate|i:1651738779;'),
('b4d34545c1655b9e08edee0d34fda06b71ca8b3f','91.108.6.126',1651738993,'__ci_last_regenerate|i:1651738993;'),
('b6d223bf3f32c36e469fcc8dd8e0176ad9597621','91.132.107.97',1651723546,'__ci_last_regenerate|i:1651723545;'),
('b86727e5ca98663a34cfbc538be496487901be74','91.108.6.126',1651724091,'__ci_last_regenerate|i:1651724091;'),
('bcdfbe08e35fbfcd6433c6d055f5ef43b911badc','91.108.6.126',1651736972,'__ci_last_regenerate|i:1651736972;'),
('c239fc97cb4c03c6c06354ad4f7fea1238a34a7c','91.108.6.126',1651724026,'__ci_last_regenerate|i:1651724026;'),
('c2ac96467a366f9ba0884e6ef6072ad187d079e9','91.108.6.126',1651723902,'__ci_last_regenerate|i:1651723902;'),
('c331f0462040fabc0f2c066e7ee3e6e33c55d352','91.108.6.126',1651738299,'__ci_last_regenerate|i:1651738299;'),
('c446507892d1c5fd6b0792b1af66d7174694b61f','91.108.6.126',1651738440,'__ci_last_regenerate|i:1651738440;'),
('c6c833e03ccba0f0eefafee1cec7f68f7dfd527f','91.108.6.126',1651724023,'__ci_last_regenerate|i:1651724023;'),
('c752f7bfc7cd287cb3a87d5fae58055921824bc5','91.108.6.126',1651735199,'__ci_last_regenerate|i:1651735199;'),
('c7afb2995cccb970a9acd93d55b60b9cc08b276d','91.108.6.126',1651735935,'__ci_last_regenerate|i:1651735935;'),
('ce24bc53933181f66237760896cee5c44cb969a3','91.108.6.126',1651725576,'__ci_last_regenerate|i:1651725576;'),
('d171080960eb84ace9dd09754dab5787848870ad','91.108.6.126',1651736098,'__ci_last_regenerate|i:1651736098;'),
('d548c0655e1e93207d1eb2c204168f3a1ffe3f90','91.108.6.126',1651736179,'__ci_last_regenerate|i:1651736179;'),
('d7517cc87f8382636d44662959eae8d801db4024','91.108.6.126',1651732892,'__ci_last_regenerate|i:1651732892;'),
('d766ec2d39cb558a0f443fe8129398c6a7fe1d6c','91.108.6.126',1651723907,'__ci_last_regenerate|i:1651723907;'),
('db75ab310428fb7f0a9d84405b2823fe108ad2a1','91.108.6.126',1651737258,'__ci_last_regenerate|i:1651737258;'),
('de4abb9fc951107c300614f693ada025eb1712a5','91.108.6.126',1651724094,'__ci_last_regenerate|i:1651724094;'),
('df64dd3a701e83af405c1401110f9356ed174264','91.108.6.126',1651735499,'__ci_last_regenerate|i:1651735499;'),
('e154c0840b8006ab7c921a9412020e04cd601f40','91.108.6.126',1651738184,'__ci_last_regenerate|i:1651738184;'),
('e21c0583629e915d13f2c0cf30ba29d3870857a6','91.108.6.126',1651737275,'__ci_last_regenerate|i:1651737275;'),
('eda66f7015a47c7a9c3ae5f397269f71f937f618','91.108.6.126',1651738410,'__ci_last_regenerate|i:1651738410;'),
('ee2b0edb217f94c328dcebb92559c1749d15fc64','91.108.6.126',1651738336,'__ci_last_regenerate|i:1651738336;'),
('f0e5584b99aa017cd4223aa502c627e2ce1c2e43','91.108.6.126',1651736529,'__ci_last_regenerate|i:1651736529;'),
('f14399b029dd86805434078c214ecc344cd2cf34','91.108.6.126',1651739112,'__ci_last_regenerate|i:1651739112;'),
('f33bebbc2095762f7a85488b0636631cedf2625a','91.108.6.126',1651725564,'__ci_last_regenerate|i:1651725564;'),
('f751dd50e7d0dbb2e91e22d2ba15f613b7dbf4e4','91.108.6.126',1651724093,'__ci_last_regenerate|i:1651724092;'),
('f881fd1a881bc023d9df774891ebc6e5535dca0f','91.108.6.126',1651724028,'__ci_last_regenerate|i:1651724028;'),
('f9ffaafbfd7c844dc9b97a225fb22d9acc3ec37e','91.108.6.126',1651732891,'__ci_last_regenerate|i:1651732891;'),
('fe9e54ddf8e58e2ee7f57b95e15c79f3920b0f1a','91.108.6.126',1651738600,'__ci_last_regenerate|i:1651738600;'),
('ff0b935d740b76fb43045ff779fc3b2999181885','91.108.6.126',1651738729,'__ci_last_regenerate|i:1651738729;');

/*Table structure for table `arik_faq_statistics` */

DROP TABLE IF EXISTS `arik_faq_statistics`;

CREATE TABLE `arik_faq_statistics` (
  `topic_id` int(10) unsigned NOT NULL,
  `counter` int(10) unsigned NOT NULL COMMENT 'how many times it has been requested',
  `last_update` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `arik_faq_statistics` */

/*Table structure for table `arik_faq_topics` */

DROP TABLE IF EXISTS `arik_faq_topics`;

CREATE TABLE `arik_faq_topics` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` varchar(10) DEFAULT NULL COMMENT 'for sharing in the internet',
  `title` varchar(300) DEFAULT NULL,
  `description` mediumtext,
  `key_phrases` mediumtext COMMENT 'newline-separated key phrases',
  `last_update` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8 COMMENT='1. key phrases to be newline-separated!\r\n2. TODO: needs chat_id(signed bigint) to separate phrases between multiple chats!';

/*Data for the table `arik_faq_topics` */

insert  into `arik_faq_topics`(`id`,`uid`,`title`,`description`,`key_phrases`,`last_update`) values 
(1,NULL,'Визовые правила, получение ВНЖ и гражданства Армении','1. Резиденты России могут становиться гражданами Армении без подписания отказа от российского гражданства (то есть двойное гражданство).\r\n2. Если у вас есть армянские корни (даже не родившись в Армении) и вы можете документально это подтвердить, можете получить гражданство в упрощенном порядке.\r\n\r\nПодробнее:\r\n1. https://docs.google.com/document/d/1niOj_ckkViPMsgjVRv3EgqBo_0Jo0fsgxYG-WIbepH0/edit\r\n2. на сайте <b>министерства диаспоры</b>: http://diaspora.gov.am/ru/pages/100/racitizenship\r\n3. Полный список документов для людей с арм. предками: https://telegra.ph/Poluchenie-grazhdanstva-Armenii-na-osnovanii-arm-kornej-03-15\r\n\r\nПеречень документов: медицинская справка, фотографии, пошлина 105000 драм.\r\nСрок: 22 дня. Недельки через 2 начнут звонить из ОВИР и на беседу приглашать.\r\n\r\n\r\nСрок пребывания в Армении <b>180 дней без оформления резидентства</b>.\r\nПосле этого либо 1) регистрация по месту жительства (делается элементарно), либо 2) пересечение границы (нужен загран).\r\n\r\nГраждане РФ могут лететь по заграну или рос.паспорту, <b>дети до 14 лет только по заграну</b>\r\n• Граждане России показывают ПЦР тест (72 ч) или сертификат о вакцинации только при выходе из аэропорта в Ереване на телефоне или в распечатке. \r\n• Отрицательный результат теста или свидетельство о вакцинации не требуется для детей младше 7 лет\r\n\r\n<b>Автомобилистам:</b>\r\n• Российские права действуют в Армении, различий в категориях нет, с российскими номерами безвыездно можно передвигаться до 2-х лет.\r\nДо истечения этого срока можно пересечь любую сухопутную границу — тогда срок обнулится. \r\n• При въезде в Армению автовладельцам необходимо оформлять полис ОСАГО минимум на 3 месяца, максимум на 1 год (это зафиксировано в армянских законах). Размер страховки зависит от объема двигателя машины и возраста водителя (23 года и более), а также водительского стажа (3 года и более).\r\n\r\nP.S. в Армении в учреждения, клиники и пр. записывайтесь в электронную очередь через моб. приложение <b>EarlyOne</b> - сэкономите себе часы жизни.','как получить ВНЖ\r\nкак сделать ВНЖ\r\nможно ли сделать ВНЖ\r\nзарегистрировать брак\r\nвид на жительство\r\nполучить гражданство\r\nоформить гражданство\r\nстать гражданином Армении\r\nстать гражданкой Армении\r\nполучить гражданство Армении\r\nкак лететь с детьми\r\nправила перевозки детей\r\nкак перевезти ребёнка\r\nкак перевезти ребенка\r\nна каких условиях можно получить ВНЖ\r\nостаться в Армении на более длительный срок\r\nостаться в Армении на длительный срок\r\nостаться на длительный срок\r\nкакие документы надо собрать для арм. гражданств\r\nза получением гражданства','2022-04-09 16:45:16'),
(3,NULL,'Счёт в армянском банке: как открыть (в т.ч. удалённо) и переводить','все вопросы, связанные с банками, переводами, снятиями наличных и т.д. просьба обсуждать в отдельном чате \"Банки в Армении\" https://t.me/+lkiNkjysjYxmNGZi , тут сообщения будут удаляться.','открыть счет в банке\r\nоткрыть счета в банках Армении\r\nоткрыть счёт\r\nоткрыть счет\r\nкак сделать банковский перевод\r\nкак снять наличные\r\nснять валюту в банкоматах\r\nБанковские переводы\r\nпроценты за перевод\r\nснятие наличных\r\nобналичивание в банкоматах\r\nснять валюту\r\nоткрытие счетов онлайн\r\nоткрытие счетa онлайн\r\nоткрытие банковского счета онлайн\r\nоткрытие банковского счёта онлайн\r\nв Армении открыть банковский счёт','2022-05-05 11:03:01'),
(4,NULL,'Регистрация компании, ИП в Армении','все вопросы, связанные с регистрацией юрлиц, просьба обсуждать в отдельном чате https://t.me/+TG55UcS6PjViOThi , тут сообщения будут удаляться ','открыть юрлицо\r\nоткрыть компанию\r\nоткрыть ИП\r\nзарегистрировать ИП\r\nзарегистрировать юрлицо\r\nкак в Армении по ИП и налогам\r\nперевёз сюда свой бизнес\r\nперевёз сюда бизнес\r\nперевёз в Армению свой бизнес\r\nконтакт для открытия компании\r\nпомощь по регистрации компании\r\nнужна консультация по открытию','2022-05-05 11:03:12'),
(7,NULL,'Подробный FAQ по релокации в Армению','Подробный F.A.Q по релокации по этой ссылке https://docs.google.com/document/d/17lp38m7JlK2HP2LNRvJ4Y43lJabkHNor1ca5_uyRWlc/edit ','FAQ по релокации\r\nплан релокации\r\nплан по релокации\r\nпомощь по релокации','2022-05-05 11:03:22'),
(9,NULL,'Где снять жильё: аренда квартир, домов, в каких городах','в данном чате запрещено публиковать рекламные сообщения об аренде жилья, используйте для этого отдельный чат https://t.me/relocationArmenia','дом в аренду\r\nквартиру в аренду\r\nгде снять жильё\r\nгде снять квартиру\r\nхочу снять квартиру\r\nгде снять дом\r\nгде можно снять жильё\r\nкак можно снять жильё\r\nгде снять жильё\r\nкак арендовать квартиру\r\nкак арендовать дом\r\nгде арендовать квартиру\r\nгде арендовать дом\r\nкак правильно заключать договор аренды\r\nкак заключать договор аренды\r\nнайти квартиру в аренду\r\nнайти дом в аренду\r\nгде кроме list.am искать','2022-05-05 11:11:05'),
(11,NULL,'Узнать курс валюты, рубля','все вопросы, связанные с банками, переводами, снятиями наличных и т.д. просьба обсуждать в отдельном чате \"Банки в Армении\" https://t.me/+lkiNkjysjYxmNGZi , тут сообщения будут удаляться ','каков курс валюты\r\nкаков курс рубля\r\nкакой курс рубля\r\nкакой курс валюты\r\nгде поменять рубли\r\nгде обменять рубли\r\nдрамы в доллары поменять\r\nдрамы в евро поменять\r\nгде выгоднее обменять\r\nгде выгодно обменять\r\nкакие есть обменники\r\nкурс доллара в Ереване где\r\nкурс евро в Ереване где','2022-05-05 11:03:57'),
(12,NULL,'Работа с Тинькофф. Как снять деньги со счёта в Тинкофф?','все вопросы, связанные с банками, переводами, снятиями наличных и т.д. просьба обсуждать в отдельном чате \"Банки в Армении\" https://t.me/+lkiNkjysjYxmNGZi , тут сообщения будут удаляться ','как снять наличку\r\nснять со счёта в тинькове\r\nснять с тиньков-карты\r\nснять с тинькофф-карты\r\nснять деньги с тинька\r\nполучить карту тинька\r\nполучить карту тиньков\r\nполучить карту тинькоф','2022-05-05 11:04:08'),
(16,NULL,'Какие банки делают дебетовые карты?','все вопросы, связанные с банками, переводами, снятиями наличных и т.д. просьба обсуждать в отдельном чате \"Банки в Армении\" https://t.me/+lkiNkjysjYxmNGZi , тут сообщения будут удаляться ','какие банки делают дебетовые карты\r\nкакие банки делают дебетовые карты\r\nкакие банки дают дебетовые карты\r\nкакие банки выдают дебетовые карты','2022-05-05 11:04:26'),
(17,NULL,'Как из Армении в РФ деньги переводить?','все вопросы, связанные с банками, переводами, снятиями наличных и т.д. просьба обсуждать в отдельном чате \"Банки в Армении\" https://t.me/+lkiNkjysjYxmNGZi , тут сообщения будут удаляться ','перевести деньги из Армении\r\nперевести деньги в Россию\r\nперевести деньги в РФ','2022-05-05 11:04:31'),
(21,NULL,'Где можно снять евро, доллары и прочую валюту?','все вопросы, связанные с банками, переводами, снятиями наличных и т.д. просьба обсуждать в отдельном чате \"Банки в Армении\" https://t.me/+lkiNkjysjYxmNGZi , тут сообщения будут удаляться ','где можно снять евро\r\nгде можно снять доллары\r\nгде можно снять валюту\r\nможно ли снять валюту\r\nможно ли снять евро\r\nможно ли снять доллары\r\nбанкоматах можно снять EUR\r\nбанкоматах можно снять USD','2022-05-05 11:04:42'),
(22,NULL,'Где можно посмотреть IT-вакансии?','посмотрите чат с ИТ вакансиями в Армении https://t.me/itjobsinam ','где можно посмотреть IT-вакансии\r\nпосмотреть IT-вакансии\r\nнайти IT-вакансии\r\nгде можно посмотреть ИТ-вакансии\r\nпосмотреть ИТ-вакансии\r\nнайти ИТ-вакансии\r\nгде можно посмотреть IT вакансии\r\nпосмотреть IT вакансии\r\nнайти IT вакансии\r\nвакансии в IT','2022-05-05 11:13:05'),
(26,NULL,'Снять валюту в банкоматах Армении невозможно.','все вопросы, связанные с банками, переводами, снятиями наличных и т.д. просьба обсуждать в отдельном чате \"Банки в Армении\" https://t.me/+lkiNkjysjYxmNGZi , тут сообщения будут удаляться ','снять в банкомате доллары\r\nснять в банкомате валюту\r\nснять в банкомате евро\r\nснять доллары\r\nснять валюту\r\nснять евро\r\n','2022-05-05 11:05:05'),
(35,NULL,'Чат с IT-вакансиями в Армении и вокруг','посмотрите чат с ИТ вакансиями в Армении https://t.me/itjobsinam ','подскажите ИТшные чаты\r\nесть ли чат по IT-вакансиям\r\nчат по IT-вакансиям\r\nчат по ИТ-вакансиям\r\nгде смотреть ИТ-шные вакансии\r\nищу работу или вакансии в ИТ\r\nищу работу в ИТ\r\nищу работу вакансии в ИТ','2022-05-05 11:12:06'),
(37,NULL,'Адреса банкоматов, где можно снять деньги с карты МИР','все вопросы, связанные с банками, переводами, снятиями наличных и т.д. просьба обсуждать в отдельном чате \"Банки в Армении\" https://t.me/+lkiNkjysjYxmNGZi , тут сообщения будут удаляться ','где снять с карты Мир\r\nснять с рублевой карты Мир\r\nснять с карты Мир\r\nснять с Мира\r\nснять деньги с виртуальной карты мир\r\nснять кэш с рф карт\r\nналичные с виртуальных карт мир\r\nналичные с виртуальной карты мир','2022-05-05 11:05:44'),
(40,NULL,'Как перевести деньги с карты РФ на карту РА','все вопросы, связанные с банками, переводами, снятиями наличных и т.д. просьба обсуждать в отдельном чате \"Банки в Армении\" https://t.me/+lkiNkjysjYxmNGZi , тут сообщения будут удаляться ','перевести деньги с карты РФ\r\nперевести деньги из России\r\nсделать перевод из России','2022-05-05 11:05:56'),
(41,NULL,'Аренда автомобилей','вот удобный агрегатор локальных сервисов аренды авто http://localrent.com/l/e6fe','где можно арендовать машину\r\nгде арендовать машину\r\nкак арендовать машину\r\nгде можно арендовать авто\r\nкак можно арендовать авто\r\nгде арендовать авто\r\nкак можно арендовать машину\r\nкак арендовать авто','2022-05-05 11:06:06');

/*Table structure for table `arik_filedata` */

DROP TABLE IF EXISTS `arik_filedata`;

CREATE TABLE `arik_filedata` (
  `id` int(11) NOT NULL,
  `path` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `arik_filedata` */

/*Table structure for table `arik_fileinfo` */

DROP TABLE IF EXISTS `arik_fileinfo`;

CREATE TABLE `arik_fileinfo` (
  `yurlico_id` int(10) unsigned DEFAULT NULL,
  `organization_id` int(10) unsigned DEFAULT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `arik_fileinfo` */

/*Table structure for table `arik_login_attempts` */

DROP TABLE IF EXISTS `arik_login_attempts`;

CREATE TABLE `arik_login_attempts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(40) COLLATE utf8_bin NOT NULL,
  `login` varchar(50) COLLATE utf8_bin NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin PACK_KEYS=0 ROW_FORMAT=DYNAMIC;

/*Data for the table `arik_login_attempts` */

/*Table structure for table `arik_meeting_tgusers_subscribed` */

DROP TABLE IF EXISTS `arik_meeting_tgusers_subscribed`;

CREATE TABLE `arik_meeting_tgusers_subscribed` (
  `tg_ticket_id` int(11) unsigned DEFAULT NULL COMMENT 'primarily only ticket should be recorded, not tg_user: because we can subscribe a user by its ticket, event w/out knowing his tg_user_id!',
  `meeting_id` int(10) unsigned NOT NULL,
  `visit_planned` tinyint(3) unsigned DEFAULT NULL COMMENT '0: no, 1: yes, 2: maybe',
  `visited` tinyint(1) DEFAULT NULL COMMENT 'after the event took place, mark the visit',
  UNIQUE KEY `tg_user_id_meeting_id` (`meeting_id`,`tg_ticket_id`),
  KEY `meeting_id` (`meeting_id`),
  KEY `tg_ticket_id` (`tg_ticket_id`),
  CONSTRAINT `arik_meeting_tgusers_subscribed_ibfk_2` FOREIGN KEY (`meeting_id`) REFERENCES `arik_meetings` (`id`),
  CONSTRAINT `arik_meeting_tgusers_subscribed_ibfk_3` FOREIGN KEY (`tg_ticket_id`) REFERENCES `arik_tg_tickets` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `arik_meeting_tgusers_subscribed` */

/*Table structure for table `arik_meetings` */

DROP TABLE IF EXISTS `arik_meetings`;

CREATE TABLE `arik_meetings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `price` tinytext COMMENT 'put the currency in it, too. E.g. "12 USD"',
  `url_info` varchar(255) DEFAULT NULL COMMENT 'link to a page with more details about the event',
  `url_web_conference` varchar(255) DEFAULT NULL COMMENT 'that could be Zoom or whatever',
  `meeting_datetime` timestamp NULL DEFAULT NULL COMMENT 'fcuk the timezones and datetime type',
  `meeting_timezone` varchar(50) DEFAULT NULL COMMENT 'explicitly mention the timezone for each event',
  `date` date DEFAULT NULL COMMENT 'Fields "date, time_from, time_till" to replace meeting_datetime',
  `time_from` time DEFAULT NULL,
  `time_till` time DEFAULT NULL COMMENT 'this also needs "time_from" field to be replace meeting_datetime field.',
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `removed` tinyint(1) DEFAULT NULL COMMENT 'do logicaly removal only.',
  `removal_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `arik_meetings` */

/*Table structure for table `arik_options` */

DROP TABLE IF EXISTS `arik_options`;

CREATE TABLE `arik_options` (
  `name` varchar(60) NOT NULL DEFAULT '',
  `value` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `arik_options` */

insert  into `arik_options`(`name`,`value`) values 
('27240274771087968824__SHOW_EVENTS','2'),
('2724027477113088389__SHOW_EVENTS','2'),
('272402747794326973__SHOW_EVENTS','2'),
('last_msg_id_124655235','51887'),
('last_msg_id_126812548','216'),
('last_msg_id_336475711','-1'),
('last_msg_id_482173415','51916'),
('last_rcvd_msg_id_124655235','51907'),
('last_rcvd_msg_id_126812548','215'),
('last_rcvd_msg_id_1416722411','53'),
('last_rcvd_msg_id_1456691918','51912'),
('last_rcvd_msg_id_1528292971','51888'),
('last_rcvd_msg_id_1689652320','51914'),
('last_rcvd_msg_id_2206296482','51894'),
('last_rcvd_msg_id_2324220122','51873'),
('last_rcvd_msg_id_2328430054','51900'),
('last_rcvd_msg_id_2344492012','51889'),
('last_rcvd_msg_id_2559549317','51893'),
('last_rcvd_msg_id_2579906965','51913'),
('last_rcvd_msg_id_2695419163','52'),
('last_rcvd_msg_id_2741486285','51895'),
('last_rcvd_msg_id_2831606649','51908'),
('last_rcvd_msg_id_3055746038','51919'),
('last_rcvd_msg_id_3342321525','51892'),
('last_rcvd_msg_id_3549616569','51902'),
('last_rcvd_msg_id_373154140','51901'),
('last_rcvd_msg_id_3809063134','51906'),
('last_rcvd_msg_id_3958663961','51890'),
('last_rcvd_msg_id_4160342410','51917'),
('last_rcvd_msg_id_424457462','51897'),
('last_rcvd_msg_id_482173415','51915'),
('last_rcvd_msg_id_542045478','51911'),
('last_rcvd_msg_id_546673175','51909'),
('last_rcvd_msg_id_549557468','51918'),
('last_rcvd_msg_id_650853223','51896');

/*Table structure for table `arik_organizations` */

DROP TABLE IF EXISTS `arik_organizations`;

CREATE TABLE `arik_organizations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `arik_organizations` */

insert  into `arik_organizations`(`id`,`name`,`email`,`phone`) values 
(1,'test club',NULL,NULL);

/*Table structure for table `arik_personal_requests` */

DROP TABLE IF EXISTS `arik_personal_requests`;

CREATE TABLE `arik_personal_requests` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_tg_userid` bigint(20) DEFAULT NULL COMMENT 'tg user id',
  `message` mediumtext,
  `request_type` tinyint(3) unsigned DEFAULT NULL COMMENT '1 - consult, 2 - create yurlico, 3 - relocate company',
  `opt_register_company` tinyint(1) DEFAULT '0',
  `opt_relocate_employees` tinyint(1) DEFAULT '0',
  `opt_relocate_owner` tinyint(1) DEFAULT '0',
  `status` tinyint(3) unsigned DEFAULT '1' COMMENT '1: draft, 2: sent for processing',
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `arik_personal_requests` */

/*Table structure for table `arik_spam_track` */

DROP TABLE IF EXISTS `arik_spam_track`;

CREATE TABLE `arik_spam_track` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(16) NOT NULL,
  `session_id` varchar(40) NOT NULL,
  `last_activity` int(10) DEFAULT NULL,
  `activity_type` tinyint(1) NOT NULL DEFAULT '1',
  `user_agent` varchar(150) DEFAULT '',
  `status` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `arik_spam_track` */

/*Table structure for table `arik_tg_expirable_links` */

DROP TABLE IF EXISTS `arik_tg_expirable_links`;

CREATE TABLE `arik_tg_expirable_links` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL COMMENT 'to hint the user what''s in',
  `uid` varchar(64) DEFAULT NULL COMMENT 'publicily available link UID',
  `expires_after` datetime DEFAULT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `arik_tg_expirable_links` */

/*Table structure for table `arik_tg_files` */

DROP TABLE IF EXISTS `arik_tg_files`;

CREATE TABLE `arik_tg_files` (
  `tg_user_id` bigint(20) DEFAULT NULL COMMENT 'the file uploader',
  `ticket_id` int(10) unsigned DEFAULT NULL COMMENT 'file OWNER''s ticket id',
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'localid kept integer for speed purposes',
  `file_id` varchar(150) DEFAULT NULL COMMENT 'e.g. BQACAgIAAxkBAAIP9l6q1tIrRSFQ1EhmQsEMIgv_5qbOAAImCAACxqZQSc0fDPB0Oh_GGQQ',
  `file_unique_id` varchar(30) DEFAULT NULL COMMENT 'e.g. AgADJggAAsamUEk',
  `file_size` int(10) unsigned DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `mime_type` varchar(90) DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP COMMENT 'null: just an attachment, 1:payment file, 2: etc.',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='telegram files (!) received.';

/*Data for the table `arik_tg_files` */

/*Table structure for table `arik_tg_message_threads` */

DROP TABLE IF EXISTS `arik_tg_message_threads`;

CREATE TABLE `arik_tg_message_threads` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `request_id` int(10) unsigned NOT NULL COMMENT 'the key (a starting point, the 1st question) to the entire thread.',
  `author_tg_userid` bigint(20) DEFAULT NULL COMMENT 'who wrote that message: either admin(s) or the requestor.',
  `message` mediumtext,
  `status` tinyint(3) unsigned DEFAULT NULL COMMENT '1: draft, 2: sent for processing. Replicates values for personal_requests->status',
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'last update date and time is traced, too',
  PRIMARY KEY (`id`),
  KEY `request_id` (`request_id`),
  CONSTRAINT `arik_tg_message_threads_ibfk_1` FOREIGN KEY (`request_id`) REFERENCES `arik_personal_requests` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='stores threads (messaging) between single end-user and multiple admins in place. By design, when answering, Admins will AMEND each other''s DRAFT responses. Indeed, the end-user amends its messages alone.';

/*Data for the table `arik_tg_message_threads` */

/*Table structure for table `arik_tg_messages` */

DROP TABLE IF EXISTS `arik_tg_messages`;

CREATE TABLE `arik_tg_messages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bot_id_crc32` bigint(20) DEFAULT NULL,
  `from__messaging_id` varchar(20) DEFAULT NULL COMMENT 'refers to telegram_users field',
  `to__messaging_id` varchar(20) DEFAULT NULL COMMENT 'refers to telegram_users field',
  `type` tinyint(4) DEFAULT NULL COMMENT 'used to differentiate messages by custom actions (e.g. online reservation, system messages, etc.)',
  `body` text,
  `date_delivered` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `yurlico_id` int(11) DEFAULT NULL,
  `organization_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `compound_index` (`yurlico_id`,`organization_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `arik_tg_messages` */

/*Table structure for table `arik_tg_tickets` */

DROP TABLE IF EXISTS `arik_tg_tickets`;

CREATE TABLE `arik_tg_tickets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bot_name` varchar(100) DEFAULT NULL COMMENT 'used to distinguish tickets for the same tg_user_id but for different bots',
  `secure_code` varchar(30) NOT NULL COMMENT 'unique access key for family, trainers, admins, etc.',
  `role` tinyint(4) DEFAULT NULL COMMENT 'see TG roles (WizardHelper): 33:admin, 3:family, 1:teacher, 4:guest',
  `messaging_id` varchar(20) NOT NULL COMMENT 'used to distinguish different roles for the same tg_user_id (e.g. guest vs. Teacher, guest vs. Admin, etc.)',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `yurlico_id` int(10) unsigned DEFAULT NULL COMMENT 'to provide more uniqueness: somehow prefilled during tickets creation',
  `organization_id` int(10) unsigned DEFAULT NULL COMMENT 'to provide more uniqueness: somehow prefilled during tickets creation',
  `ticket_owner_name` varchar(60) DEFAULT NULL COMMENT 'a human name to welcome to.',
  `local_userid` int(10) unsigned DEFAULT NULL COMMENT 'optional reference to local user id: used to access personal schedule.',
  `extra_data` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `messaging_id` (`messaging_id`),
  KEY `yurlico_and_organization` (`yurlico_id`,`organization_id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

/*Data for the table `arik_tg_tickets` */

insert  into `arik_tg_tickets`(`id`,`bot_name`,`secure_code`,`role`,`messaging_id`,`created`,`yurlico_id`,`organization_id`,`ticket_owner_name`,`local_userid`,`extra_data`) values 
(3,'armrelo2bot','HTKDfF8u',4,'pzobMZ5H','2022-05-05 07:05:53',NULL,NULL,'Регистрант (@walltouch)',NULL,NULL),
(4,'armrelo2bot','ZaSezHxW',4,'etAhcn7N','2022-05-05 09:40:25',NULL,NULL,'Юзер (@GroupAnonymousBot)',NULL,NULL),
(5,'armrelo2bot','MPehUBpQ',4,'Zvn7VCFi','2022-05-05 10:15:38',NULL,NULL,'Юзер (@serving_my_master)',NULL,NULL),
(6,'armrelo2bot','D8QAwPXd',4,'THzhxF4L','2022-05-05 10:19:59',NULL,NULL,'Юзер (@Alexandrareind)',NULL,NULL),
(7,'armrelo2bot','6cBYrRWt',4,'CIfaVFuH','2022-05-05 10:24:59',NULL,NULL,'Юзер',NULL,NULL),
(8,'armrelo2bot','ydWk6sO3',4,'KFSBLint','2022-05-05 10:27:27',NULL,NULL,'Юзер (@easypixi)',NULL,NULL),
(9,'armrelo2bot','hZrPDOg8',4,'gM1K06vH','2022-05-05 10:31:38',NULL,NULL,'Юзер (@LAN39)',NULL,NULL),
(10,'armrelo2bot','MGNIq1BU',4,'gO1eJHpB','2022-05-05 10:33:21',NULL,NULL,'Юзер (@ArthurReimer)',NULL,NULL),
(11,'armrelo2bot','dJ3ejuMn',4,'fQnlZXVK','2022-05-05 10:33:55',NULL,NULL,'Юзер (@User_1783)',NULL,NULL),
(12,'armrelo2bot','MY2WfdSF',4,'vqJ6nBkW','2022-05-05 10:34:54',NULL,NULL,'Юзер (@dark8bit)',NULL,NULL),
(13,'armrelo2bot','drb7TLOy',4,'E6CF8wzM','2022-05-05 10:34:58',NULL,NULL,'Юзер (@filinswe)',NULL,NULL),
(14,'armrelo2bot','k4idMhts',4,'kFGxdqLY','2022-05-05 10:36:21',NULL,NULL,'Юзер (@michaelklishin)',NULL,NULL),
(15,'armrelo2bot','feplPtJ3',4,'Dw6OJ1gr','2022-05-05 10:42:09',NULL,NULL,'Юзер (@lalonzadentro)',NULL,NULL),
(16,'armrelo2bot','N7uQcvpx',4,'81WVhTEd','2022-05-05 10:49:27',NULL,NULL,'Юзер (@urban_animal)',NULL,NULL),
(17,'armrelo2bot','rAjX7vGO',4,'t5fSjlnF','2022-05-05 10:49:32',NULL,NULL,'Юзер (@sovasergey)',NULL,NULL),
(18,'armrelo2bot','IYJlOnNi',4,'BGp3xtlf','2022-05-05 10:51:44',NULL,NULL,'Юзер',NULL,NULL),
(19,'armrelo2bot','Whd27zrC',4,'JBQTz47L','2022-05-05 10:52:42',NULL,NULL,'Юзер (@lyubovkolosova)',NULL,NULL),
(20,'armrelo2bot','whkfxGp1',4,'4oWd2jDw','2022-05-05 10:53:43',NULL,NULL,'Юзер (@krnhsh)',NULL,NULL),
(21,'armrelo2bot','ieIafn49',4,'CXxvwHde','2022-05-05 10:54:18',NULL,NULL,'Юзер (@arikvcv)',NULL,NULL),
(22,'armrelo2bot','jUqrvlhw',4,'XompYh4V','2022-05-05 10:54:35',NULL,NULL,'Юзер (@dfnsk)',NULL,NULL),
(23,'armrelo2bot','sNbEnaPt',4,'NHOo6Pgw','2022-05-05 10:55:35',NULL,NULL,'Юзер (@ap_tg)',NULL,NULL),
(24,'armrelo2bot','lC8Bduqa',4,'wpk7qngN','2022-05-05 10:55:40',NULL,NULL,'Юзер (@alexpelikh)',NULL,NULL),
(25,'armrelo2bot','CMU7Aat4',4,'7cn59MHX','2022-05-05 11:07:34',NULL,NULL,'Юзер (@pisarevsky)',NULL,NULL),
(26,'armrelo2bot','z56Bd8aZ',4,'bdDScOwX','2022-05-05 11:12:12',NULL,NULL,'Юзер (@DianaCrypt)',NULL,NULL),
(27,'armrelo2bot','lGcudRDq',4,'8c1G42io','2022-05-05 11:16:40',NULL,NULL,'Юзер (@ohjimmy)',NULL,NULL),
(28,'armrelo2bot','JEmdO8MG',4,'7ZRxcMQ9','2022-05-05 11:19:39',NULL,NULL,'Юзер (@RuassianTeamInArmenia)',NULL,NULL),
(29,'armrelo2bot','HeJdpW52',4,'SN40jc6f','2022-05-05 11:29:45',NULL,NULL,'Юзер',NULL,NULL);

/*Table structure for table `arik_tg_users` */

DROP TABLE IF EXISTS `arik_tg_users`;

CREATE TABLE `arik_tg_users` (
  `tg_user_id` bigint(20) NOT NULL COMMENT 'telegram user id',
  `bot_name` varchar(40) DEFAULT NULL COMMENT 'user roles are specific to bot.',
  `role` tinyint(3) unsigned DEFAULT NULL COMMENT '1 - guest, 2 - user, 3 - admin',
  `messaging_id` varchar(20) DEFAULT NULL COMMENT 'used to distinguish different roles for the same tg_user_id (e.g. guest vs. Teacher, guest vs. Admin, etc.)',
  `logged_in` tinyint(4) DEFAULT NULL COMMENT '1: true, 2: false',
  `ticket_owner_name` varchar(50) DEFAULT NULL COMMENT 'initially copied from ticket, later could be customized',
  `ticket_id` int(11) DEFAULT NULL,
  `yurlico_id` int(10) unsigned DEFAULT NULL COMMENT 'NULL if none selected, or int otherwise.',
  `organization_id` int(10) unsigned DEFAULT NULL COMMENT 'NULL if none selected, or int otherwise.',
  `local_userid` int(10) unsigned DEFAULT NULL COMMENT 'optional reference to local user id: used to access personal schedule.',
  `lang` varchar(2) DEFAULT NULL COMMENT 'en, ru, etc.',
  `timezone_id` int(11) DEFAULT NULL COMMENT 'refers to tz_zone.',
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  KEY `yurlico_and_organization` (`yurlico_id`,`organization_id`),
  KEY `tg_user_id_bot_role` (`tg_user_id`,`bot_name`,`role`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `arik_tg_users` */

insert  into `arik_tg_users`(`tg_user_id`,`bot_name`,`role`,`messaging_id`,`logged_in`,`ticket_owner_name`,`ticket_id`,`yurlico_id`,`organization_id`,`local_userid`,`lang`,`timezone_id`,`created`) values 
(113088389,'armrelo2bot',4,'pzobMZ5H',1,'Регистрант (@walltouch)',3,NULL,NULL,NULL,'ru',NULL,'2022-05-05 07:05:53'),
(1087968824,'armrelo2bot',4,'etAhcn7N',1,'Юзер (@GroupAnonymousBot)',4,NULL,NULL,NULL,'ru',NULL,'2022-05-05 09:40:25'),
(94326973,'armrelo2bot',4,'Zvn7VCFi',1,'Юзер (@serving_my_master)',5,NULL,NULL,NULL,'ru',NULL,'2022-05-05 10:15:38'),
(301749916,'armrelo2bot',4,'THzhxF4L',1,'Юзер (@Alexandrareind)',6,NULL,NULL,NULL,NULL,NULL,'2022-05-05 10:19:59'),
(800505666,'armrelo2bot',4,'CIfaVFuH',1,'Юзер',7,NULL,NULL,NULL,NULL,NULL,'2022-05-05 10:24:59'),
(1052241768,'armrelo2bot',4,'KFSBLint',1,'Юзер (@easypixi)',8,NULL,NULL,NULL,NULL,NULL,'2022-05-05 10:27:27'),
(24873506,'armrelo2bot',4,'gM1K06vH',1,'Юзер (@LAN39)',9,NULL,NULL,NULL,NULL,NULL,'2022-05-05 10:31:38'),
(221403313,'armrelo2bot',4,'gO1eJHpB',1,'Юзер (@ArthurReimer)',10,NULL,NULL,NULL,NULL,NULL,'2022-05-05 10:33:21'),
(2054445295,'armrelo2bot',4,'fQnlZXVK',1,'Юзер (@User_1783)',11,NULL,NULL,NULL,NULL,NULL,'2022-05-05 10:33:55'),
(92102152,'armrelo2bot',4,'vqJ6nBkW',1,'Юзер (@dark8bit)',12,NULL,NULL,NULL,NULL,NULL,'2022-05-05 10:34:54'),
(304386511,'armrelo2bot',4,'E6CF8wzM',1,'Юзер (@filinswe)',13,NULL,NULL,NULL,NULL,NULL,'2022-05-05 10:34:58'),
(520095195,'armrelo2bot',4,'kFGxdqLY',1,'Юзер (@michaelklishin)',14,NULL,NULL,NULL,NULL,NULL,'2022-05-05 10:36:21'),
(326556018,'armrelo2bot',4,'Dw6OJ1gr',1,'Юзер (@lalonzadentro)',15,NULL,NULL,NULL,NULL,NULL,'2022-05-05 10:42:09'),
(177583677,'armrelo2bot',4,'81WVhTEd',1,'Юзер (@urban_animal)',16,NULL,NULL,NULL,NULL,NULL,'2022-05-05 10:49:27'),
(32530886,'armrelo2bot',4,'t5fSjlnF',1,'Юзер (@sovasergey)',17,NULL,NULL,NULL,NULL,NULL,'2022-05-05 10:49:32'),
(693405401,'armrelo2bot',4,'BGp3xtlf',1,'Юзер',18,NULL,NULL,NULL,NULL,NULL,'2022-05-05 10:51:44'),
(304716764,'armrelo2bot',4,'JBQTz47L',1,'Юзер (@lyubovkolosova)',19,NULL,NULL,NULL,NULL,NULL,'2022-05-05 10:52:42'),
(316956388,'armrelo2bot',4,'4oWd2jDw',1,'Юзер (@krnhsh)',20,NULL,NULL,NULL,NULL,NULL,'2022-05-05 10:53:43'),
(1633600108,'armrelo2bot',4,'CXxvwHde',1,'Юзер (@arikvcv)',21,NULL,NULL,NULL,NULL,NULL,'2022-05-05 10:54:18'),
(340334794,'armrelo2bot',4,'XompYh4V',1,'Юзер (@dfnsk)',22,NULL,NULL,NULL,NULL,NULL,'2022-05-05 10:54:35'),
(57830526,'armrelo2bot',4,'NHOo6Pgw',1,'Юзер (@ap_tg)',23,NULL,NULL,NULL,NULL,NULL,'2022-05-05 10:55:35'),
(284863171,'armrelo2bot',4,'wpk7qngN',1,'Юзер (@alexpelikh)',24,NULL,NULL,NULL,NULL,NULL,'2022-05-05 10:55:40'),
(295632,'armrelo2bot',4,'7cn59MHX',1,'Юзер (@pisarevsky)',25,NULL,NULL,NULL,NULL,NULL,'2022-05-05 11:07:34'),
(5314720961,'armrelo2bot',4,'bdDScOwX',1,'Юзер (@DianaCrypt)',26,NULL,NULL,NULL,NULL,NULL,'2022-05-05 11:12:12'),
(401915807,'armrelo2bot',4,'8c1G42io',1,'Юзер (@ohjimmy)',27,NULL,NULL,NULL,NULL,NULL,'2022-05-05 11:16:40'),
(5104191085,'armrelo2bot',4,'7ZRxcMQ9',1,'Юзер (@RuassianTeamInArmenia)',28,NULL,NULL,NULL,NULL,NULL,'2022-05-05 11:19:39'),
(5378210843,'armrelo2bot',4,'SN40jc6f',1,'Юзер',29,NULL,NULL,NULL,NULL,NULL,'2022-05-05 11:29:45');

/*Table structure for table `arik_tg_users_profiles` */

DROP TABLE IF EXISTS `arik_tg_users_profiles`;

CREATE TABLE `arik_tg_users_profiles` (
  `tg_user_id` bigint(20) NOT NULL,
  `tg_ticket_id` varchar(20) DEFAULT NULL,
  `web_conference_link` varchar(255) DEFAULT NULL COMMENT 'Zoom personal room link.',
  `contract_serial_number` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='extra fields needed for tg suers.';

/*Data for the table `arik_tg_users_profiles` */

/*Table structure for table `arik_tg_wizard_state` */

DROP TABLE IF EXISTS `arik_tg_wizard_state`;

CREATE TABLE `arik_tg_wizard_state` (
  `bot_id_crc32` bigint(20) DEFAULT NULL,
  `tg_user_id` bigint(20) DEFAULT NULL,
  `tg_chat_id` bigint(20) DEFAULT NULL,
  `wizard_name` varchar(30) DEFAULT NULL,
  `step_no` tinyint(3) unsigned DEFAULT NULL COMMENT 'the next (to current) step number: 0, 1, 2, ...',
  `lastupdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `chain_to_wizard` varchar(30) DEFAULT NULL COMMENT 'if set, then upon completion (???) current wizard teh chaining wizard shall be called: using its name (and step no) specified here.',
  `chain_to_wizard_step_no` tinyint(4) DEFAULT NULL COMMENT 'if set, then upon completion (???) current wizard teh chaining wizard shall be called: using its name (and step no) specified here.',
  `callback_messaging_id` int(10) unsigned DEFAULT NULL,
  `callback_data` varchar(256) DEFAULT NULL COMMENT 'any data passed in'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `arik_tg_wizard_state` */

insert  into `arik_tg_wizard_state`(`bot_id_crc32`,`tg_user_id`,`tg_chat_id`,`wizard_name`,`step_no`,`lastupdate`,`chain_to_wizard`,`chain_to_wizard_step_no`,`callback_messaging_id`,`callback_data`) values 
(2724027477,113088389,113088389,'WizardTrainerChecker',1,'2022-05-05 07:15:04',NULL,NULL,NULL,NULL),
(2724027477,-1001654493563,-1001654493563,'WizardTrainerChecker',1,'2022-05-05 09:41:33',NULL,NULL,51887,'cbbtn_OptionsIA#OptionsIA#Act_ShowEvents#-1'),
(2724027477,94326973,-1001654493563,'WizardTrainerChecker',1,'2022-05-05 10:15:38',NULL,NULL,NULL,NULL);

/*Table structure for table `arik_tg_wizards_steps` */

DROP TABLE IF EXISTS `arik_tg_wizards_steps`;

CREATE TABLE `arik_tg_wizards_steps` (
  `bot_id_crc32` bigint(20) DEFAULT NULL,
  `tg_user_id` bigint(20) DEFAULT NULL COMMENT 'Telegram user id',
  `tg_chat_id` bigint(20) DEFAULT NULL COMMENT 'Telegram chat id',
  `wizard_name` varchar(30) DEFAULT NULL COMMENT 'either name or GUID (not decided yet)',
  `step_no` tinyint(3) unsigned DEFAULT NULL COMMENT '0, 1, 2, 3',
  `step_data` varchar(256) DEFAULT NULL COMMENT 'data user has entered on each step',
  `lastupdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `metadata` varchar(256) DEFAULT NULL COMMENT 'optional data storage',
  UNIQUE KEY `compound_index` (`tg_user_id`,`tg_chat_id`,`wizard_name`,`step_no`,`bot_id_crc32`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `arik_tg_wizards_steps` */

insert  into `arik_tg_wizards_steps`(`bot_id_crc32`,`tg_user_id`,`tg_chat_id`,`wizard_name`,`step_no`,`step_data`,`lastupdate`,`metadata`) values 
(2724027477,113088389,113088389,'WizardTrainerChecker',0,'inition_wzrd','2022-05-05 07:15:04',NULL),
(2724027477,-1001654493563,-1001654493563,'WizardTrainerChecker',0,'inition_wzrd','2022-05-05 09:40:25',NULL),
(2724027477,94326973,-1001654493563,'WizardTrainerChecker',0,'inition_wzrd','2022-05-05 10:15:38',NULL);

/*Table structure for table `arik_upload` */

DROP TABLE IF EXISTS `arik_upload`;

CREATE TABLE `arik_upload` (
  `key` varchar(40) DEFAULT NULL,
  `sessid` varchar(50) DEFAULT NULL,
  `dir` varchar(255) DEFAULT NULL,
  `postprocess_code` int(11) DEFAULT '0',
  `dword` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0 ROW_FORMAT=DYNAMIC;

/*Data for the table `arik_upload` */

/*Table structure for table `arik_user_autologin` */

DROP TABLE IF EXISTS `arik_user_autologin`;

CREATE TABLE `arik_user_autologin` (
  `key_id` char(32) COLLATE utf8_bin NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `user_agent` varchar(150) COLLATE utf8_bin NOT NULL,
  `last_ip` varchar(40) COLLATE utf8_bin NOT NULL,
  `last_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`key_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin PACK_KEYS=0 ROW_FORMAT=DYNAMIC;

/*Data for the table `arik_user_autologin` */

/*Table structure for table `arik_user_groups` */

DROP TABLE IF EXISTS `arik_user_groups`;

CREATE TABLE `arik_user_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `name_ru` varchar(50) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `group_type` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 PACK_KEYS=0 ROW_FORMAT=DYNAMIC;

/*Data for the table `arik_user_groups` */

insert  into `arik_user_groups`(`id`,`name`,`name_ru`,`description`,`group_type`) values 
(1,'administrator',NULL,NULL,2),
(3,'SERVER_berkana','SERVER_berkana','',2),
(4,'cabinet','cabinet','automatically created missing role by assignRole_to_User() call.',2),
(5,'mobile_client','mobile_client','mobile login role for clubs\' customers.',2),
(6,'yurlico_web_login','yurlico_web_login','yurlico website login role only.',2),
(7,'trainer','','',2);

/*Table structure for table `arik_user_profiles` */

DROP TABLE IF EXISTS `arik_user_profiles`;

CREATE TABLE `arik_user_profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT 'this is FIO',
  `user_id` int(11) NOT NULL,
  `yurlico_id` int(11) DEFAULT NULL COMMENT 'for Clients: when Yurlico logs in to manage his Personal Cabinet.',
  `organization_id` int(11) DEFAULT NULL COMMENT 'used for WinSvc accounts only.',
  `country` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `preferred_language` tinyint(4) DEFAULT '0',
  `phone` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `phone2` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `phone_extension` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `has_exclusive_access` int(11) DEFAULT '0',
  `description` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `userdata` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `temp_delete_me` varchar(350) COLLATE utf8_bin DEFAULT NULL COMMENT 'remove after tests!',
  `temp_new_hash` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`yurlico_id`,`organization_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin PACK_KEYS=0 ROW_FORMAT=DYNAMIC;

/*Data for the table `arik_user_profiles` */

/*Table structure for table `arik_users` */

DROP TABLE IF EXISTS `arik_users`;

CREATE TABLE `arik_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE utf8_bin NOT NULL,
  `password` varchar(255) COLLATE utf8_bin NOT NULL,
  `email` varchar(100) COLLATE utf8_bin NOT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT '1',
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `ban_reason` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `new_password_key` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `new_password_requested` datetime DEFAULT NULL,
  `new_email` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `new_email_key` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `last_ip` varchar(40) COLLATE utf8_bin NOT NULL,
  `last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin PACK_KEYS=0 ROW_FORMAT=DYNAMIC;

/*Data for the table `arik_users` */

/*Table structure for table `arik_users_groups_link` */

DROP TABLE IF EXISTS `arik_users_groups_link`;

CREATE TABLE `arik_users_groups_link` (
  `userId` int(10) unsigned NOT NULL,
  `groupId` int(10) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0 ROW_FORMAT=FIXED;

/*Data for the table `arik_users_groups_link` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
