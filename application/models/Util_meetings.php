<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH . '/controllers/admin/SecuredMatrix.php'); // greco.

class Util_meetings extends CI_Model /* see http://codeigniter.com/wiki/Inserting_Multiple_Records_Into_a_Table/ */
{
    const MTG__INVITATION_PREFIX = 'mtg';
    // date selection modes:
    const MTG__DATE_MODE_BEFORE             = 1;
    const MTG__DATE_MODE_TODAY_AND_LATER    = 2;

    const VISIT_PLANNED_NO      = 0;
    const VISIT_PLANNED_YES     = 1;
    const VISIT_PLANNED_MAYBE   = 2;

    const TG_FILETYPE_PAYMENT           = 1; // file attachment is related to the payment (cheques, reports, etc.).
    const TG_FILETYPE_CONTRACT          = 2; // client contract-related file. There could be multiple types but for now I'm using one for all.
    const TG_FILETYPE_REGATA    = 2; // client contract-related file. There could be multiple types but for now I'm using one for all.

    // Reggata bot specifics:
    const TG_FILETYPE_PARTICIPATION_REQUEST = 30; // заявка на регату
    const TG_FILETYPE_LISENCES              = 31; // права капитана и рулевого
    const TG_FILETYPE_SHIP_TICKET           = 32; // судовой билет
    const TG_FILETYPE_TECH_INSPECTION       = 33; // технический осмотр
    const TG_FILETYPE_INSURANCE             = 34; // действительный страховой полис
    const TG_FILETYPE_MEASUREMENT           = 35; // мерительное свидетельство


    function __construct()
    {
        parent::__construct();

        $this->load->library(array('transliterate', 'tank_auth'));
        $this->load->helper('string');

        $this->load->model('utilitar');
        $this->load->model('utilitar_db');
        $this->load->model('utilitar_date');
        $this->load->model('util_messages');

        $ci = &get_instance();
        $ci->load->model('tank_auth/users');
    }

    // returns user's timezone if set or meetings's timezone (or null if it's not set: which is impossible, but still)
    public static function get_user_timezone_obj(&$tg_user)
    {
        if ($tg_user && $tg_user->timezone_id)
        {   $ci = &get_instance();
            $tz_row = $ci->utilitar_date->get_timezone_row_by_timezone_id($tg_user->timezone_id);
            return new DateTimeZone($tz_row->zone_name); // Current User's timezone
        }

        return null;
    }

    public static function create_meeting($name, $tz_obj)
    {
        log_message('error', "----create_meeting($name)");
        $ci = &get_instance();

        $tz_name = 'Europe/Moscow';
        $datetime = new DateTime('tomorrow');
        if ($tz_obj)
        {   $datetime->setTimezone($tz_obj); // A MUST-have to SET IT EXPLICITLY (and not from DateTime constructor - it doesn't work that way!!!)
            $tz_name = $tz_obj->getName();
        }

        $tomorrow = $datetime->format('Y-m-d');
        $ci->db->insert('meetings', array('name' => $name, 'date' => $tomorrow, 'time_from' =>'09:00:00', 'meeting_timezone' => $tz_name));
        return $ci->utilitar_db->get_last_inserted_id();
    }

    public static function create_meeting_ext($params)
    {
        log_message('error', "----create_meeting_ext(): ".print_r($params, true));
        return Utilitar_db::_create_entity('meetings', $params);
    }

    public static function set_meeting_datetime($meeting_id, $date, $time_from)
    {
        $ci = &get_instance();
        $ci->db->where('id', $meeting_id);
        $ci->db->update('meetings', array('date' => $date, 'time_from' => $time_from));
    }

    public static function get_meeting_by_invitation_id($invitation_id)
    {
        $ci = &get_instance();

        $select_fields = Util_meetings::_get_meetings_select_string($ci->db->dbprefix);
        $ci->db->select($select_fields);
        $ci->db->where('invite_url', $invitation_id);
        $ci->db->from('meetings');

        $query = $ci->db->get();
        return Utilitar_db::safe_resultRow($query);
    }


    public static function format_meeting_invite_url($meeting_invite_url)
    {   return Util_telegram::generate_tg_invite_link(Util_meetings::MTG__INVITATION_PREFIX, $meeting_invite_url);
    }

    // (re)generates the invite url.
    public static function generate_meeting_invite_url($meeting_id)
    {
        $ci = &get_instance();
        $ci->db->where('id', $meeting_id);
        $ci->db->update('meetings', array('invite_url' => random_string('alnum', 6)));
    }

    public static function set_meeting_enddate($meeting_id, $end_date)
    {   Utilitar_db::_update_entity('meetings', $meeting_id, array('date_till' => $end_date));
    }

    public static function set_meeting_description($meeting_id, $description)
    {   Utilitar_db::_update_entity('meetings', $meeting_id, array('description' => $description));
    }

    public static function set_meeting_timezone($meeting_id, $tz_name)
    {
        $ci = &get_instance();
        $ci->db->where('id', $meeting_id);
        $ci->db->update('meetings', array('meeting_timezone' => $tz_name));
    }

    public static function set_meetings_datetime(array $actual_days_ids)
    {
/*  UPDATE educ_meetings
    LEFT JOIN educ_schedule_days_to_meetings ON educ_schedule_days_to_meetings.meeting_id = educ_meetings.id
    LEFT JOIN educ_group_schedule_days ON educ_schedule_days_to_meetings.group_schedule_day_id = educ_group_schedule_days.id
    SET educ_meetings.meeting_datetime = STR_TO_DATE(CONCAT(educ_group_schedule_days.date, ' ', educ_group_schedule_days.time_from), '%Y-%m-%d %H:%i:%s'),
    educ_meetings.date = educ_group_schedule_days.date,
    educ_meetings.time_from = educ_group_schedule_days.time_from,
    educ_meetings.time_till = educ_group_schedule_days.time_till
    WHERE educ_schedule_days_to_meetings.meeting_id IS NOT NULL
    AND educ_schedule_days_to_meetings.group_schedule_day_id IN (661)
*/
        if (0 == count($actual_days_ids))
        {   return;
        }

        $ci = &get_instance();
        $dbprefix = $ci->db->dbprefix;

        $sql = 'UPDATE '.$dbprefix.'meetings
        LEFT JOIN '.$dbprefix.'schedule_days_to_meetings ON '.$dbprefix.'schedule_days_to_meetings.meeting_id = '.$dbprefix.'meetings.id
        LEFT JOIN '.$dbprefix.'group_schedule_days ON '.$dbprefix.'schedule_days_to_meetings.group_schedule_day_id = '.$dbprefix.'group_schedule_days.id ';

        $sql .= ' SET '.$dbprefix."meetings.meeting_datetime = STR_TO_DATE(CONCAT(".$dbprefix."group_schedule_days.date, ' ', ".$dbprefix."group_schedule_days.time_from), '%Y-%m-%d %H:%i:%s'), ";
        $sql .= $dbprefix.'meetings.date = '.$dbprefix.'group_schedule_days.date,
                '.$dbprefix.'meetings.time_from = '.$dbprefix.'group_schedule_days.time_from,
                '.$dbprefix.'meetings.time_till = '.$dbprefix.'group_schedule_days.time_till
                WHERE '.$dbprefix.'schedule_days_to_meetings.meeting_id IS NOT NULL ';
        $sql .= " AND educ_schedule_days_to_meetings.group_schedule_day_id IN (".implode(',', $actual_days_ids).")";

        $query  = $ci->db->query($sql);
    }

    public static function set_meeting_title($meeting_id, $title)
    {
        $ci = &get_instance();
        $ci->db->where('id', $meeting_id);
        $ci->db->update('meetings', array('name' => $title));
    }

    public static function set_meeting_web_conference_link($meeting_id, $web_conference_link)
    {
        $ci = &get_instance();
        $ci->db->where('id', $meeting_id);
        $ci->db->update('meetings', array('url_web_conference' => $web_conference_link));
    }

    public static function set_meeting_price($meeting_id, $price)
    {
        $ci = &get_instance();
        $ci->db->where('id', $meeting_id);

        $real_price = (0 == strcasecmp('free', $price)) ? NULL : $price;

        $ci->db->update('meetings', array('price' => $real_price));
    }

    public static function set_meeting_details_webpage($meeting_id, $url_info)
    {
        $ci = &get_instance();
        $ci->db->where('id', $meeting_id);
        $ci->db->update('meetings', array('url_info' => $url_info));
    }

    public static function get_meeting($id)
    {
        if ($id <= 0)
        {   return null;
        }

        $ci = &get_instance();

        $select_str = Util_meetings::_get_meetings_select_string($ci->db->dbprefix);
        $ci->db->select($select_str);

        $ci->db->from('meetings');
        $ci->db->where('meetings.id', $id);

        $query = $ci->db->get();
        return Utilitar_db::safe_resultRow($query);
    }

    public static function subscribe_to_meeting($tg_ticket_id, $meeting_id, $choice)
    {
        $ci = &get_instance();
        $ci->load->model('utilitar_db');

        // NOTE: using {'visit_planned' => "$choice"} instead of {'visit_planned' => $choice} ensures that "0" will be inserted not as NULL.
        // I consider NULL for UNANSWERED events only. And the "Won't come" answer this way will be recorded with zero - not with NULL, which is good for me.
        $where = array( 'tg_ticket_id'  => $tg_ticket_id,
                        'meeting_id'    => $meeting_id,
                      );

        $params = array_merge($where, array('visit_planned' => "$choice"));

        $user_subscription= Utilitar_db::_get_entity('meeting_tgusers_subscribed', $where);
        if (!$user_subscription)
        {   Utilitar_db::_create_entity('meeting_tgusers_subscribed', $params);
        }
        else
        {   Utilitar_db::_update_entity('meeting_tgusers_subscribed', null, $params, $where);
        }
    }

    public static function get_tg_users_by_ticket_ids(array $ticket_ids, $role_id = null)
    {
        $ci = &get_instance();

        $ci->db->from('tg_users');
        $ci->db->where_in('tg_users.ticket_id', $ticket_ids);

        if ($role_id)
        {   $ci->db->where('tg_users.role', $role_id);
        }

        $query = $ci->db->get();

        return Utilitar_db::safe_resultSet($query);
    }

    // returns only "YES" and "MAYBE"s.
    // NOTE: it's a slight modification of get_meeting_subscriptions() due to lack of time!! :(((
    public static function get_meeting_positive_subscriptions($meeting_id, array $subscription_statuses = null)
    {
/*  SELECT educ_meeting_tgusers_subscribed.*, educ_meetings.*
    FROM educ_meeting_tgusers_subscribed
    LEFT JOIN educ_meetings ON educ_meetings.id = educ_meeting_tgusers_subscribed.meeting_id
    WHERE educ_meetings.id = 28
    AND educ_meeting_tgusers_subscribed.visit_planned > 0;
*/
        $ci = &get_instance();

        $select_fields = "meeting_tgusers_subscribed.*, meetings.id, meetings.name, meetings.price, ";
        $select_fields .= 'meetings.date, meetings.meeting_timezone, TIME_FORMAT('.$ci->db->dbprefix.'meetings.time_from, "%k:%i") AS time_from, ';
        $select_fields .= 'TIME_FORMAT('.$ci->db->dbprefix.'meetings.time_till, "%k:%i") AS time_till';

        $ci->db->select($select_fields);

        $ci->db->from('meeting_tgusers_subscribed');
        $ci->db->join('meetings', 'meetings.id = meeting_tgusers_subscribed.meeting_id', 'left');
        $ci->db->where('meeting_tgusers_subscribed.visit_planned > 0');
        $ci->db->where('meetings.id', $meeting_id);

        if ($subscription_statuses && count($subscription_statuses))
        {   $ci->db->where_in('meeting_tgusers_subscribed.visit_planned', $subscription_statuses);
        }

        $query = $ci->db->get();

        // if single meeting requested then return a row, otherwise return a rowset:
        return Utilitar_db::safe_resultSet($query);
    }

    public static function get_mimes_map()
    {
        $MIMES = array(
                'images'        => array('image/apng','image/bmp','image/gif','image/x-icon', 'jpg', 'image/jpeg','image/png','image/svg+xml','image/tiff','image/webp'),
                'audio-video'   => array('audio/wave', 'audio/wav', 'audio/x-wav', 'audio/x-pn-wav', 'audio/webm', 'video/webm', 'audio/ogg', 'video/ogg', 'application/ogg', 'audio/mpeg', 'video/mpeg'),
                'documents'     => array('application/msword', 'application/pdf', 'text/plain', 'application/vnd.ms-excel', 'application/vnd.ms-powerpoint', 'text/csv', 'application/gzip', 'application/zip', 'application/x-7z-compressed', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/vnd.openxmlformats-officedocument.presentationml.presentation'),
            );

        return $MIMES;
    }

    public static function get_all_subscriptions($ticket_id)
    {
        return Util_meetings::get_meeting_subscriptions($ticket_id);
    }

    // uses User's TimeZone object to identify upcoming meetings. Returns count of upcoming meetings and also fills in the "$responses" array by response types (YES/MAYBE/NO).
    public static function count_user_upcoming_subscriptions(&$user_subscriptions, &$responses, &$tz)
    {
        $upcoming_events_count = 0;

        $today = new DateTime('now', $tz);
        $today_timestamp = $today->getTimestamp();

        // init by zeros. This is for extended stats:
        $responses[0] = 0;
        $responses[1] = 0;
        $responses[2] = 0;

        foreach ($user_subscriptions as $subscription)
        {
            // this is for extended stats:
            $responses[(int)$subscription->visit_planned]++; // 0, 1, 2

            // this is for general stats:
            $tmp = DateTime::createFromFormat('Y-m-d H:i', $subscription->date.' '.$subscription->time_from, new DateTimeZone($subscription->meeting_timezone));
            $event_timestamp = $tmp->getTimestamp();
            if (
                ($event_timestamp > $today_timestamp) &&
                (WizardTrainerChecker::MEETING_VISIT_YES == $subscription->visit_planned || WizardTrainerChecker::MEETING_VISIT_MAYBE == $subscription->visit_planned)
               )
            {   $upcoming_events_count++;
            }
        }

        return $upcoming_events_count;
    }

    // get UPCOMING (today and later) subscriptions only.
    public static function get_upcoming_subscriptions($ticket_id)
    {   return Util_meetings::get_meeting_subscriptions($ticket_id, null, false, Util_meetings::MTG__DATE_MODE_TODAY_AND_LATER);
    }

    // get PASSED (BEFORE today) subscriptions only.
    public static function get_passed_subscriptions($ticket_id)
    {   return Util_meetings::get_meeting_subscriptions($ticket_id, null, false, Util_meetings::MTG__DATE_MODE_BEFORE);
    }

    public static function count_user_subscriptions($ticket_id)
    {
        $ci = &get_instance();
        $ci->db->from('meeting_tgusers_subscribed');
        $ci->db->where('tg_ticket_id', $ticket_id);
        return $ci->db->count_all_results();
    }

    // enriches a meeting row by participant's go/no go (tristate) decision.
    // if no $meeting_id passed then method returns user's ALL the subscriptions existing.
    // if "$b_participants_only== TRUE" then considered only YES and MAYBEs (NOs ignored). Otherwise get everybody who gave ANY (including negative) reaction.
    // if "$date_mode" specified then also add a date cut-off condition.
    public static function get_meeting_subscriptions($ticket_id, $meeting_id = null, $b_participants_only = false, $date_mode = null)
    {
/*  SELECT educ_meeting_tgusers_subscribed.*, educ_meetings.*, educ_tg_users.*
    FROM educ_meeting_tgusers_subscribed
    LEFT JOIN educ_meetings ON educ_meetings.id = educ_meeting_tgusers_subscribed.meeting_id
    LEFT JOIN educ_tg_users ON educ_tg_users.ticket_id = educ_meeting_tgusers_subscribed.tg_ticket_id
    WHERE educ_tg_users.bot_name = 'events_stream_bot'
    AND (educ_meeting_tgusers_subscribed.visit_planned IS NOT NULL)
    AND educ_meeting_tgusers_subscribed.tg_ticket_id = 84
    AND educ_tg_users.role = 4
    ORDER BY educ_meetings.meeting_datetime, educ_tg_users.ticket_owner_name
*/
        $ci = &get_instance();

        $select_fields = "meeting_tgusers_subscribed.*, tg_users.*, ";
        $select_fields .= Util_meetings::_get_meetings_select_string($ci->db->dbprefix);

        $ci->db->select($select_fields);

        $ci->db->from('meeting_tgusers_subscribed');
        $ci->db->join('meetings', 'meetings.id = meeting_tgusers_subscribed.meeting_id', 'left');
        $ci->db->join('tg_users', 'tg_users.ticket_id = meeting_tgusers_subscribed.tg_ticket_id', 'left');
        //$ci->db->where('tg_users.bot_name', WizardFather::$bot_name); // disabled because it is NOT ALWAYS STABLY loaded: sometimes (from CRON jobs) it is NULL! Use Util_telegram::get_bot_name() instead!
        $ci->db->where('meeting_tgusers_subscribed.visit_planned IS NOT NULL');

        if (WC_S1::MODE_SCHOOLBOT == Util_meetings::get_work_mode())
        {   $ci->db->where('tg_users.role', WizardHelper::USER_ROLE__CLIENT); // at SchoolBot meetings assigned to PARENTS (not to children!)
        }
        else
        {   $ci->db->where('tg_users.role', WizardHelper::USER_ROLE__GUEST);
        }


        if ($ticket_id > 0)
        {   $ci->db->where('tg_users.ticket_id', $ticket_id);
        }

        if ($meeting_id > 0)
        {   $ci->db->where('meetings.id', $meeting_id);
        }

        if ($b_participants_only)
        {   $ci->db->where('meeting_tgusers_subscribed.visit_planned > 0');
        }


        if ($date_mode > 0)
        {   // if a cut-off date specified then use it:
            $actions = array(   Util_meetings::MTG__DATE_MODE_BEFORE            => 'date < DATE(NOW())',
                                Util_meetings::MTG__DATE_MODE_TODAY_AND_LATER   => 'date >= DATE(NOW())', // today included!
                            );

            if (isset($actions[$date_mode]))
            {   $ci->db->where($actions[$date_mode]);
            }
        };

        $ci->db->order_by('meetings.date, meetings.time_from, tg_users.ticket_owner_name');
        $query = $ci->db->get();

        // if single meeting requested then return a row, otherwise return a rowset:
        return ($meeting_id > 0) ? Utilitar_db::safe_resultRow($query) : Utilitar_db::safe_resultSet($query);
    }

    // dooes logical removal only: to keep all the attachments and details in place.
    public static function delete_meetings(array $meetings_ids)
    {
        if (0 == count($meetings_ids))
        {   return;
        }

        $ci = &get_instance();
        $ci->db->where_in('id', $meetings_ids);
        $query = $ci->db->update('meetings', array('removed'=> true, 'removal_date' => date('Y-m-d H:i:s'))); // delete actual group schedule days
    }

    public static function get_work_mode()
    {
        return WC_S1::MODE_SCHOOLBOT;
    }

    // attemtping validating date against MySQL format, then (if failed) then tries also Berkana format.
    public static function get_meetings($by_specific_date = null)
    {
        $ci = &get_instance();

        $date_from__mysql = null;
        if ($by_specific_date)
        { // assumed input format of 'd/m/Y', so let's
            $tmp = DateTime::createFromFormat('d/m/Y', $by_specific_date);
            if (!$tmp)
            {   log_message('error', "-------------- ERROR ERROR ERROR creating DT of format 'd/m/Y' from input parameter '$by_specific_date'!!!!");
                return array();
            }

            $date_from__mysql = $tmp->format('Y-m-d');
        }

        $select_fields = Util_meetings::_get_meetings_select_string($ci->db->dbprefix);
        $ci->db->select($select_fields);
        $ci->db->from('meetings');

        // only if passed in and the date was of correct fotmat:
        if ($date_from__mysql)
        {   $ci->db->where('date', $date_from__mysql);
            $ci->db->or_where('date_till >=', $date_from__mysql);
        }

        $ci->db->order_by('meetings.date, meetings.time_from, meetings.name');
        $query = $ci->db->get();

        return Utilitar_db::safe_resultSet($query);
    }

    public static function get_meetings_in_month($month_year_string)
    {
        $dt = DateTime::createFromFormat('d/m/Y', '01/'.$month_year_string);

        $date_start = $dt->format('Y-m-d');
        $date_end   = $dt->format('Y-m-t'); // get the end of THAT month.
        return Util_meetings::get_meetings_between($date_start, $date_end);
    }

    // NOTE: $date_start and $date_end to be MySql-formatted dates!
    public static function get_meetings_between($date_start, $date_end)
    {
/*  SELECT educ_meetings.*
    FROM educ_meetings
    WHERE (date >= "2020-06-02" AND date <= "2020-06-03")
    ORDER BY date, time_from
*/
        $ci = &get_instance();


        $select_fields = Util_meetings::_get_meetings_select_string($ci->db->dbprefix);
        $ci->db->select($select_fields);

        $ci->db->from('meetings');
        $ci->db->where('(meetings.date >= "'.$date_start.'" AND meetings.date <= "'.$date_end.'")');
        $ci->db->order_by('meetings.date, meetings.time_from');

        $query = $ci->db->get();
        return Utilitar_db::safe_resultSet($query);
    }

    // to overcome MySQL's policy returning also seconds with TIME field, I need custom formatting (do I really need to put that everywhere and get bugs every time i forget about that? Instead of formatting the results in a correct way???)
    public static function _get_meetings_select_string($dbprefix)
    {
        $select_fields = "meetings.id, meetings.name, meetings.invite_url, meetings.description, meetings.price, meetings.url_info, meetings.url_web_conference, ";
        $select_fields .= 'meetings.created, meetings.removed, meetings.removal_date, ';
        $select_fields .= 'meetings.date, meetings.date_till, meetings.meeting_timezone, TIME_FORMAT('.$dbprefix.'meetings.time_from, "%k:%i") AS time_from, ';
        $select_fields .= 'TIME_FORMAT('.$dbprefix.'meetings.time_till, "%k:%i") AS time_till';

        return $select_fields;
    }

    // the difference is that it takes EXTRA 1 day BEFORE the today. Just to cover all coditions in php code later: by filtering out the events unneeded.
    // TODO: we shall retrieve UPCOMING meetings for ALL OVER THE GLOBE. Thus we shall base the "upcoming" on each event timezone: RELATIVE to server's timezone!!
    // get all meetings taking place after some specific date.
    public static function get_upcoming_meetings_with_margin()
    {
        $ci = &get_instance();

        $select_fields = Util_meetings::_get_meetings_select_string($ci->db->dbprefix);
        $ci->db->select($select_fields);
        $ci->db->from('meetings');
        $ci->db->where('date >= (CURDATE() - INTERVAL 1 DAY)');
        $ci->db->order_by('meetings.date, meetings.time_from');

        $query = $ci->db->get();

        return Utilitar_db::safe_resultSet($query);
    }

    // TODO: we shall retrieve UPCOMING meetings for ALL OVER THE GLOBE. Thus we shall base the "upcoming" on each event timezone: RELATIVE to server's timezone!!
    // get all meetings taking place after some specific date.
    public static function get_upcoming_meetings($date_from__mysql = null)
    {
        $ci = &get_instance();

        if (!$date_from__mysql)
        {   $date_from__mysql = date('Y-m-d');
        }
        else
        { // assumed input format of 'd/m/Y', so let's
            $tmp = DateTime::createFromFormat('d/m/Y', $date_from__mysql);
            if (!$tmp)
            {   log_message('error', "-------------- ERROR ERROR ERROR creating DT of format 'd/m/Y' from input parameter '$date_from__mysql'!!!!");
                return array();
            }

            $date_from__mysql = $tmp->format('Y-m-d');
        }

        $select_fields = Util_meetings::_get_meetings_select_string($ci->db->dbprefix);
        $ci->db->select($select_fields);
        $ci->db->from('meetings');
        $ci->db->where('date >= ', $date_from__mysql);
        $ci->db->or_where('date_till >=', $date_from__mysql);
        $ci->db->order_by('meetings.date, meetings.time_from, meetings.name');

        $query = $ci->db->get();

        return Utilitar_db::safe_resultSet($query);
    }

    // get all meetings taking place after some specific date.
    public static function get_meeting_participants($meeting_id)
    {
/*  SELECT educ_meeting_tgusers_subscribed.*, educ_tg_tickets.*, educ_meetings.name, educ_meetings.date, educ_meetings.time_from, educ_meetings.meeting_timezone
    FROM educ_meeting_tgusers_subscribed
    LEFT JOIN educ_meetings ON educ_meetings.id = educ_meeting_tgusers_subscribed.meeting_id
    LEFT JOIN educ_tg_users ON educ_tg_users.ticket_id = educ_meeting_tgusers_subscribed.tg_ticket_id
    WHERE bot_name = 'events_stream_bot'
    AND educ_meetings.id = 26
    ORDER BY educ_meetings.date, educ_meetings.time_from, educ_tg_tickets.ticket_owner_name
 */
        $ci = &get_instance();

        $ci->db->select('meeting_tgusers_subscribed.*, tg_tickets.id AS ticket_id, tg_tickets.*, meetings.id, meetings.name, meetings.date, meetings.time_from, meetings.time_till, meetings.meeting_timezone');
        $ci->db->from('meeting_tgusers_subscribed');
        $ci->db->join('meetings', 'meetings.id = meeting_tgusers_subscribed.meeting_id', 'left');
        $ci->db->join('tg_tickets', 'tg_tickets.id = meeting_tgusers_subscribed.tg_ticket_id', 'left');

        //$ci->db->where(array('bot_name' => WizardFather::$bot_name, 'meetings.id' => $meeting_id)); // disabled because it is NOT ALWAYS STABLY loaded: sometimes (from CRON jobs) it is NULL! Use Util_telegram::get_bot_name() instead!
        $ci->db->where(array('meetings.id' => $meeting_id));
        $ci->db->order_by('meetings.date, meetings.time_from, tg_tickets.ticket_owner_name, tg_tickets.id');

        $query = $ci->db->get();

        return Utilitar_db::safe_resultSet($query);
    }

    public static function get_meeting_tg_attachments($meeting_id)
    {
//    SELECT educ_meeting_files.*, `educ_tg_files`.*
//    FROM educ_meeting_files
//    LEFT JOIN educ_tg_files ON educ_tg_files.id = educ_meeting_files.local_tg_file_id
//    ORDER BY educ_tg_files.mime_type, educ_tg_files.file_name

        $ci = &get_instance();

        $ci->db->select('meeting_files.meeting_id, tg_files.*');
        $ci->db->from('meeting_files');
        $ci->db->join('tg_files', 'tg_files.id = meeting_files.local_tg_file_id', 'left');

        $ci->db->where('meeting_files.meeting_id', $meeting_id);
        $ci->db->where('tg_files.id > 0');
        $ci->db->order_by('tg_files.mime_type, tg_files.file_name');

        $query = $ci->db->get();
        return Utilitar_db::safe_resultSet($query);
    }

    // we ALLOW attaching SAME FILE to DIFFERENT meetings.
    public static function get_meeting_attachment_by_tg_file_id($meeting_id, $tg_file_unique_id)
    {
//    SELECT educ_meeting_files.*, educ_tg_files.*
//    FROM educ_meeting_files
//    LEFT JOIN educ_tg_files ON educ_tg_files.id = educ_meeting_files.local_tg_file_id
//    WHERE educ_meeting_files.meeting_id = 30
//    AND educ_tg_files.file_id = 'BQACAgIAAxkBAAIQHl6q5VK7sWguMnRVrN0T1uRlf4JKAAJCCAACxqZQSVLWPDXnxPVJGQQ'

        $ci = &get_instance();
        $ci->db->select('meeting_files.*, tg_files.*');
        $ci->db->from('meeting_files');
        $ci->db->join('tg_files', 'tg_files.id = meeting_files.local_tg_file_id', 'left');
        $ci->db->where(array('meeting_files.meeting_id' => $meeting_id, 'tg_files.file_unique_id' => $tg_file_unique_id, ));

        $query = $ci->db->get();

        return Utilitar_db::safe_resultRow($query);
    }

    public static function get_attachment_by_tg_file_id($tg_file_unique_id)
    {
        $ci = &get_instance();
        return $ci->utilitar_db->_getTableRow('tg_files', 'file_unique_id', $tg_file_unique_id);
    }

    public static function delete_attachment($meeting_id, $tg_file_id)
    {
        $ci = &get_instance();

        log_message('error', "---- delete_attachment(meeting_id:$meeting_id, local_tg_file_id:$tg_file_id)");

        // NOTE: I decided to NOT to delete rows from "tg_files" to keep the links between different meetings and files, just in case.
        $ci->db->where(array('meeting_id' => $meeting_id, 'local_tg_file_id' => $tg_file_id));
        $ci->db->delete('meeting_files');
    }

    // Meeting's files are different beasts: they allow attaching AUDIO-messages which HAVE NO FILENAME! For that I assign a SEQUENTIAL counter for audio messages (thus the extra code below).
    // Returns the $local_tg_file_id inserted.
    // Parses input Telegram file structure and inserts it into DB.
    // $tg_file_struct may be single file or an array of IMAGES.
    // TODO: get rid of "$file_owner_tg_user_id" and use "$tg_ticket_id" only!
    public static function attach_file_to_meeting($user_role, &$telegram, $file_owner_tg_user_id, $tg_ticket_id, $meeting_id, &$tg_file_struct)
    {
        $ci = &get_instance();

        $tg_file_struct = TelegramHelper::identify_largest_image_sent($tg_file_struct); // this is a sign: a single file has been sent, so get that (and not its thumbnails).

        // only if the file IS registered in our records, it makes sense checking whether that file is ALREADY assigned to THIS SPECIFIC meeting:
        $local_tg_file_id       = -1;
        $meeting_attachment_file= null;

        $tg_registered_file = TelegramHelper::get_tg_file_by_unique_id($tg_file_struct['file_unique_id']);  // gets just a TG-file registered INSPITE OF any meeting.
        log_message('error', "---- available TG file with id:'".$tg_file_struct['file_unique_id']."',\n".print_r($tg_registered_file, true));
        if ($tg_registered_file)
        {
            $meeting_attachment_file = null;

            if ((WizardHelper::USER_ROLE__TRAINER == $user_role) || (WizardHelper::USER_ROLE__CLUB_ADMIN == $user_role))
            {   $meeting_attachment_file = Util_school::get_client_attachment_by_tg_file_id($file_owner_tg_user_id, $tg_file_struct['file_unique_id'], $meeting_id); // gets TG-file attached TO THAT SPECIFIC meeting.
            }
            else if (WizardHelper::USER_ROLE__CLIENT == $user_role)
            {   $meeting_attachment_file = Util_meetings::get_meeting_attachment_by_tg_file_id($tg_file_struct['file_unique_id'], $meeting_id); // gets TG-file attached TO THAT SPECIFIC meeting.
            }

            if ($meeting_attachment_file)
            {   // no need to proceed as this meeting already contains that file. Return
                log_message('error', "---- NOTE: meeting_id:$meeting_id already contains the TG file ".((WizardHelper::USER_ROLE__CLIENT == $user_role) ? 'from MANAGERS ' : 'from USER ').": '$tg_registered_file->id'. So we skip ATTACHING DUPLICATES and return.");
                return false;//$tg_registered_file->id;
            }

            $local_tg_file_id = $tg_registered_file->id; // otherwise get the local id of a TG file.
        }
        else
        {
            if (!isset($tg_file_struct['file_name']))
            {   // if filename is n/a (that's true for audio-messages) then 1) get the files count of the same mime-type and set an incremental filename to it EXPLICITLY:
                $attachments = Util_meetings::get_meeting_tg_attachments($meeting_id);
                $of_mime_type = TelegramHelper::choose_attachments_of_type($attachments, $tg_file_struct['mime_type']);
                //log_message('error', "ATTACHMENTS of_mime_type: ".print_r($of_mime_type, true));

                $extension = '.ogg';
                if (isset($tg_file_struct['width']))
                {   $extension = '.jpg';
                }

                log_message('error', "--- for unknwon extension i use '$extension'");

                $tg_file_struct['file_name'] = 'file_'.(count($of_mime_type) + 1).'.'.TelegramHelper::mimetype_to_extension($tg_file_struct['mime_type']); // increment it.
            }
            else
            {   ;//$tg_file_struct['file_name'] = $tg_file_struct['file_name'];//$ci->security->xss_clean($tg_file_struct['file_name'], true); // as we're shoing the filename to end-users on a web page, ensure it's safe from the very begin: once it is being entered our system.
            }

            $local_tg_file_id = TelegramHelper::register_tg_attachment('', $telegram, $tg_ticket_id, $tg_file_struct, NULL);
        }

        if ((WizardHelper::USER_ROLE__TRAINER == $user_role) || (WizardHelper::USER_ROLE__CLUB_ADMIN == $user_role))
        { // privileged users get own files attached TO THE MEETING:
            $ci->db->insert('meeting_files', array('meeting_id' => $meeting_id, 'local_tg_file_id' => $local_tg_file_id));
        }
        else if (WizardHelper::USER_ROLE__CLIENT == $user_role)
        {   Util_meetings::register_client_homework_file_for_meeting($file_owner_tg_user_id, $meeting_id, $local_tg_file_id);
        }

        return $local_tg_file_id;
    }

    // Marks the homework file as "removed".
    public static function remove_client_homework_file_for_meeting($file_owner_tg_user_id, $meeting_id, $existing_localfile_id)
    {
        $ci = &get_instance();
        $ci->db->where(array('owner_tg_user_id' => $file_owner_tg_user_id, 'meeting_id' => $meeting_id, 'local_tg_file_id' => $existing_localfile_id));
        $ci->db->update('meeting_user_files', array('removed' => true));

        // also "remove" from tg_files:
        $ci->db->where('id', $existing_localfile_id);
        $ci->db->update('tg_files', array('removed' => true));
    }

    // register client's homework file (because non-privileged users deal with OWN ATTACHMENTS only):
    public static function register_client_homework_file_for_meeting($file_owner_tg_user_id, $meeting_id, $local_tg_file_id)
    {
        $ci = &get_instance();
        $data = array('owner_tg_user_id' => $file_owner_tg_user_id, 'meeting_id' => $meeting_id, 'local_tg_file_id' => $local_tg_file_id);
        $ci->db->insert('meeting_user_files', $data);
    }

    // Considering the screenshots/files attached as client's payments.
    public static function get_client_payments_files($client_tg_user_id)
    {   $ci = &get_instance();
        return $ci->utilitar_db->_getTableContents('tg_files', 'created DESC', 'type', Util_meetings::TG_FILETYPE_PAYMENT, 'tg_user_id', $client_tg_user_id);
    }

    // Considering the screenshots/files attached as client's payments.
    public static function get_client_payments_files_by_ticket_id($client_ticket_id, $last_N_days = null)
    {   $ci = &get_instance();

        if (!$last_N_days)
        {   return $ci->utilitar_db->_getTableContents('tg_files', 'created DESC', 'type', Util_meetings::TG_FILETYPE_PAYMENT, 'ticket_id', $client_ticket_id);
        }

/*
    SELECT educ_tg_files.id, educ_tg_files.created, educ_tg_files.ticket_id
    FROM educ_tg_files
    WHERE educ_tg_files.created >= (CURRENT_DATE - INTERVAL 14 DAY)
    ORDER BY educ_tg_files.created DESC
*/
        $ci->db->select('tg_files.id, tg_files.created, tg_files.ticket_id');
        $ci->db->from('tg_files');
        $ci->db->join('tg_users', 'tg_users.tg_user_id = tg_files.tg_user_id', 'left');
        $ci->db->where('tg_files.ticket_id', $client_ticket_id);
        $ci->db->where('tg_files.created >= (CURRENT_DATE - INTERVAL '.$last_N_days.' DAY)');
        $ci->db->order_by('tg_files.created DESC');

        $query = $ci->db->get();
        return Utilitar_db::safe_resultSet($query);
    }

    public static function get_clients_payments_files()
    {
/*  SELECT educ_tg_files.tg_user_id, educ_tg_files.file_unique_id, educ_tg_files.file_name, educ_tg_files.mime_type, educ_tg_files.created, educ_tg_users.ticket_owner_name
    FROM educ_tg_files
    LEFT JOIN educ_tg_users ON educ_tg_users.tg_user_id = educ_tg_files.tg_user_id
    WHERE educ_tg_files.type = 1
    ORDER BY educ_tg_users.ticket_owner_name, educ_tg_files.created DESC
*/
        $ci = &get_instance();

        $ci->db->select('tg_files.tg_user_id, tg_files.file_unique_id, tg_files.file_name, tg_files.mime_type, tg_files.created, tg_users.ticket_owner_name');
        $ci->db->from('tg_files');
        $ci->db->join('tg_users', 'tg_users.tg_user_id = tg_files.tg_user_id', 'left');
        $ci->db->where('tg_files.type', Util_meetings::TG_FILETYPE_PAYMENT);
        $ci->db->order_by('tg_users.ticket_owner_name, tg_files.created DESC');

        $query = $ci->db->get();
        return Utilitar_db::safe_resultSet($query);
    }

    public static function create_payment_tg_file_link($title, $file_unique_id)
    {
        $fileurl = base_url('dlpmt/'.$file_unique_id, 'https');

        return "<a href='$fileurl'>$title</a>";
    }

    public static function create_contract_tg_file_link($title, $file_unique_id)
    {
        $fileurl = base_url('dlctr/'.$file_unique_id, 'https');

        return "<a href='$fileurl'>$title</a>";
    }

    public static function get_client_payment_file($tg_file_unique_id)
    {   $ci = &get_instance();

        $ci->db->where(array('tg_files.file_unique_id' => $tg_file_unique_id, 'tg_files.type' => Util_meetings::TG_FILETYPE_PAYMENT));
        $query = $ci->db->get('tg_files');
        return Utilitar_db::safe_resultRow($query);
    }

    // Returns the $local_tg_file_id inserted.
    // Parses input Telegram file structure and inserts it into DB.
    // $tg_file_struct may be single file or an array of IMAGES.
    public static function attach_client_payment_file(&$telegram, $file_owner_tg_user_id, &$tg_file_struct)
    {
        $ci = &get_instance();

        $tg_file_struct = TelegramHelper::identify_largest_image_sent($tg_file_struct); // this is a sign: a single file has been sent, so get that (and not its thumbnails).

        // only if the file IS registered in our records, it makes sense checking whether that file is ALREADY assigned to THIS SPECIFIC meeting:
        $local_tg_file_id       = -1;

        //-----------------------------------------------------------+
        // detect whether there's already a TG file existing:
        $tg_registered_file = TelegramHelper::get_tg_file_by_unique_id($tg_file_struct['file_unique_id']);  // gets just a TG-file registered INSPITE OF any meeting.
        if ($tg_registered_file)
        {
            log_message('error', "---- attach_client_payment_file() TG file with id:'".$tg_file_struct['file_unique_id']."' is ALREADY available:\n".print_r($tg_registered_file, true));
            $local_tg_file_id = $tg_registered_file->id; // otherwise get the local id of a TG file.
        }
        else
        {
            if (!isset($tg_file_struct['file_name']))
            {   // manual audio recordings have no file_name, so we make a trick below.
                // if filename is n/a (that's true for audio-messages) then 1) get the files count of the same mime-type and set an incremental filename to it explicitly:
                //$attachments = Util_meetings::get_meeting_tg_attachments($meeting_id);
                //$of_mime_type = TelegramHelper::choose_attachments_of_type($attachments, $tg_file_struct['mime_type']);
                //log_message('error', "ATTACHMENTS of_mime_type: ".print_r($of_mime_type, true));

                if (isset($tg_file_struct['file_path']))
                {   $ci->load->model('utilitar');
                    $tg_file_struct['file_name'] = 'Payment'.$ci->utilitar->locale_safe_basename($tg_file_struct['file_path']);
                }
                else
                {   $tg_file_struct['file_name'] = 'Payment'. TelegramHelper::mimetype_to_extension($tg_file_struct['mime_type']);
                }
            }

            $local_tg_file_id = TelegramHelper::insert_tg_file($file_owner_tg_user_id, $tg_file_struct, $telegram); // otherwise register it just as a "Telegram file" first.
        }
        //-----------------------------------------------------------|


        //-----------------------------------------------------------+
        // finally mark the TG file as a Payment for a client($file_owner_tg_user_id):
        $ci->db->where(array('id' => $local_tg_file_id, 'tg_user_id' => $file_owner_tg_user_id));
        $ci->db->update('tg_files', array('type' => Util_meetings::TG_FILETYPE_PAYMENT));
        //-----------------------------------------------------------|

        return $local_tg_file_id;
    }

    public static function rename_client($tg_ticket_id, $client_name)
    {
        $organization_id = 1; // hardcoded

        // update both tg_tickets and tg_users:
        $res = Utilitar_db::_update_entity('tg_tickets', null, array('ticket_owner_name' => $client_name), array('id' => $tg_ticket_id));
        Utilitar_db::_update_entity('tg_users', null, array('ticket_owner_name' => $client_name), array('ticket_id' => $tg_ticket_id));
        return $res;
    }

    //
    // Returns events SPLIT BY DIAPASONS: most recent/upcoming ones are shown as is ('days'), the preceeding ones are as 'm/Y' arrays.
    // The logic is:
    //  If today_dayno <= $today_dayno_limit of month then show also PREVIOUS month's events without grouping. Otherwise collect meetings into "$months_years" array.
    //  The "$today_dayno_limit" gives a "buffer" to show past events (instead of hiding them).
    public static function split_meetings_by_dates_diapason(&$meetings, $today_dayno_limit = 7)
    {
        $ci = &get_instance();
        $ci->load->model('utilitar_date');


        $today      = date('Y-m-d'); // MySQL date format.
        $ts_today   = strtotime($today); // that way we exclude hours/minutes/seconds!!!

        // based on "$today_dayno_limit" we decide whether PAST events to show in previous MONTH or show as DAYS.
        // That's useful when TODAY is in the very begin of a month (e.g. dayno = 1, 2, ...7), so that PREVIOUS month's events
        // will be shown just to show some margin instead of hiding past ones immediately.
        // So that "$today_dayno_limit" gives a "buffer" to show (before hiding past events).
        $today_day_number       = date('j');
        $max_difference_in_days = -$today_day_number;
        if ($today_day_number <= $today_dayno_limit)
        {   $prev_month_days_count = date('t', mktime(0,0,0, date('n') - 1));
            $max_difference_in_days -= $prev_month_days_count; // extend the limit to include prev. whole month days diapason into 'DAYS' array.
        }

        $months_years = array();
        $days = array();
        foreach ($meetings as $meeting)
        {
            $ts_mtg = strtotime($meeting->date);
            $diff_in_days = $ci->utilitar_date->diff_days($ts_mtg, $ts_today);
            //log_message('error', "---- diff_in_days=$diff_in_days between today and ".$meeting->date);

            if ($diff_in_days < 0)
            { // those are passed events:
                if ($diff_in_days < $max_difference_in_days)
                {   $mtg_month_year = date('m/Y', strtotime($meeting->date));
                    $months_years[$mtg_month_year][] = $meeting; // it's too old so we archive it into 'm/Y' array.
                }
                else
                {   $days[] = $meeting; // past meetings within allowed diapason also are being shown as DAYS: useful when TODAY is amongst first "$today_dayno_limit" days of the current month.
                }
            }
            else // those are upcoming events:
            {   $days[] = $meeting; // upcoming meetings show pear each day.
            }
        }

        return array('months' => $months_years, 'days' => $days); // months are formatted along with corresponding years, so we're safe showing them as is!
    }

    // returns recipients count processed.
    public static function send_message_to_participants($meeting_id, $notification_text, $dt_created, $author_tg_user_id)
    {
        $people_notified_for_all_meetings = 0;
        $positive_subscriptions = Util_meetings::get_meeting_positive_subscriptions($meeting_id); // identify who said YES/MAYBE.
        if (count($positive_subscriptions) > 0)
        {
            $meeting = Util_meetings::get_meeting($meeting_id);

            $tmp_assoc  = DictionaryService::_to_key_value_array($positive_subscriptions, 'tg_ticket_id', 'tg_ticket_id');
            $tg_tickets_ids_to_notify= array_keys($tmp_assoc);

            // now let's see which tg users do really have those tg tickets:
            $tg_users   = Util_meetings::get_tg_users_by_ticket_ids($tg_tickets_ids_to_notify);
            $tmp_assoc  = DictionaryService::_to_key_value_array($tg_users, 'tg_user_id', 'lang');

            // initialize constants, etc.:
            $options = array('disable_web_page_preview' => true);

            // finally let's send the notifications:
            $meeting_users_to_notify = array_keys($tmp_assoc);
            foreach ($meeting_users_to_notify as $tg_user_id)
            {
    //            if (113088389 != $tg_user_id)
    //            {   continue; // DEBUG-ONLY: send to myself only!
    //            }

                $user_lang = $tmp_assoc[$tg_user_id];
                if ((!$user_lang) || (0 == strlen($user_lang)))
                {   $user_lang = 'ru'; // by default use Russian text.
                }

                $msg = Util_meetings::wrap_meeting_group_message($meeting, $notification_text, $dt_created, $user_lang, false);
                $msg = str_replace(array('\n'), chr(10), $msg); // do a message cleanup.

                $message_delivered = TelegramHelper::hideRegularButtons($msg, $tg_user_id); // cleaning up REGULAR BUTTONS (if any). Useful when RETURNING (actually cancelling) from Geo-location assignment menu.

                $extra_actions = array();
                $extra_actions['OK'] = TelegramHelper::wrap_button_action_name(WC_S1::ACTION__STREAM_SHOW_EVENTS);
                $grouped_buttons = TelegramHelper::to_callback_grouped_buttons(WC_S1::NAME, __CLASS__, $extra_actions);

                $message_delivered =  TelegramHelper::echo_inline(('ru' == $user_lang ? 'Все сообщения также доступны в меню события "'.$meeting->name.'".' :
                                                                                        'All the messages are also available in "'.$meeting->name.'" event\'s menu.'),
                                                                    $grouped_buttons, $options, $tg_user_id);
                $people_notified_for_all_meetings += (isset($message_delivered['ok']) && (1 == $message_delivered['ok'])) ? 1 : 0;
            }
        }

        // add to messages archive IN ANY CASE:
        Util_messages::add_message($notification_text, Util_messages::TYPE_EVENT, $meeting_id, $author_tg_user_id);

        return $people_notified_for_all_meetings; // count($meeting_users_to_notify)
    }

    // adds some prefix/postfix to the message shown about the regatta.
    // NOTE: it s
    public static function wrap_meeting_group_message(&$meeting, $message, $dt_created, $user_lang, $b_shorter)
    {
        $intro_string = $b_shorter ? '' : "Сообщение от организаторов от ";

        $msg = '<code>'.date('H:i:s, d/m/Y', strtotime($dt_created)).'</code><i>\n'.$intro_string;//.'\n';

        $msg .= WC_S1::format_meeting_name_and_datetime(null, $user_lang, $meeting, 'd-M-Y', true); // TODO: pass user's timezone_id instead!!
        //$msg .= WC_S1::format_meeting_urls($meeting);
        $msg .= '</i>';
        //$msg .= "\xE2\x84\xB9  ".$message;
        $msg .= '\n\n'.$message;
        $msg .= "\n\n<i>----\nПишите про найденные ошибки и пожелания сюда: @walltouch</i>";

        return $msg;
    }

    // adds some prefix/postfix to the message shown about the regatta.
    // NOTE: it s
    public static function wrap__notification_message($message, $dt_created, $lang)
    {
        $msg = '<code>'.date('H:i:s, d/m/Y', strtotime($dt_created)).'</code>';
        $msg .= '\n\n'.$message;
        $msg .= "\n\n<i>----\nПишите про найденные ошибки и пожелания сюда: @walltouch</i>";

        return $msg;
    }

    // NOTE: my server time iz GMT0, and I shall fix all the dates acordingly.
    // TODO: account user's timezone instead of hardcoding '4' hours!!
    public static function message_time_shift($datetime)
    {   $hours = 4; // move forward by 4 hours, compared with EVN and GMT. // TODO: account user's timezone instead of hardcoding '4' hours!!

        $str_time = date('-'.$hours.' hour', strtotime($datetime));
        return date('H:i', strtotime($str_time));
    }
}
