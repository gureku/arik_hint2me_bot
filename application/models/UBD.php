<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH.'libraries/wizardry/library/WizardFather.php');
require_once(APPPATH . '/models/Util_berkana.php');        // greco.
require_once(APPPATH . '/models/Util_berkana_data.php');        // greco.

class UBD extends CI_Model /* see http://codeigniter.com/wiki/Inserting_Multiple_Records_Into_a_Table/ */
{
    private $CI;

    // values that will be excluded when returning trainings types for Client application.
//    private static $stop_words__training_types = array('Аренда', 'День Рождения', 'Тест', 'Индивидуальное');
//    private static $stop_words__training_types = array('Аренда', 'День Рождения', 'Тест',);
    private static $stop_words__training_types = array('Аренда', 'День Рождения', 'Тест', 'Внутреннее');

	function __construct()
	{
		parent::__construct();

        $this->CI = &get_instance();

//        $this->load->library(array('transliterate', 'tank_auth'));
//
        $this->load->model('utilitar');
        $this->load->model('utilitar_db');
        $this->load->model('utilitar_date');
//
//        $this->CI->load->model('tank_auth/users');

        $this->lang->load('data', 'russian');
	}

    // club owners are allowed to categorize their training types on their own.
    // NOTE: do not mix it with training types!!!
    //  NOTE: we are getting and setting *only ONE categorization* per yurlico: no difference whether there is a single organization or a number of them within a same yurlico!!!
    public function get_coursetypes_categories($yurlico_id, $organization_id)
    {
        $this->db->select("coursetypes_categorized.*");
        // $this->dbfilter_yurlico_and_organization_multipletables('coursetypes_categorized', $yurlico_id, $organization_id); // NOTE: this call is mandatory to be present when accessing berkana assests!
        $this->db->where('yurlico_id', $yurlico_id); // NOTE: this call is mandatory to be present when accessing berkana assests!
        $this->db->limit(1);

        $query = $this->db->get('coursetypes_categorized');

        return Utilitar_db::safe_resultRow($query);
    }

    public function getRemoteSchedule_with_yurlico_orgid($yurlico_id, $organization_id, $days_before = -1, $days_after = -1)
    {
        $this->db->select("berk_schedule.*");
        $this->dbfilter_yurlico_and_organization_multipletables(array('berk_schedule', 'berk_rooms'), $yurlico_id, $organization_id); // NOTE: this call is mandatory to be present when accessing berkana assests!

       return $this->_getRemoteSchedule_conditionally($days_before, $days_after);

    }

    // NOTE: Added LEFT JOIN by ROOMS so that trainings get SORTED BY ROOM_TITLE. This eases doing output of Web Widget schedule!
    private function _getRemoteSchedule_conditionally($days_before, $days_after)
    {
        //  LEFT JOIN wt_berk_rooms ON wt_berk_rooms.id = wt_berk_schedule.plannedroom_id
        $this->db->join('berk_rooms', 'berk_rooms.id = berk_schedule.plannedroom_id', 'left');

        if (($days_before >= 0) && ($days_after >= 0))
        {   $db_prefix = $this->db->dbprefix;
            $this->db_where_date__relative2today($db_prefix.'berk_schedule', 'planneddate', $days_before, $days_after);
        }

        $this->db->order_by('berk_schedule.planneddate, berk_schedule.plannedstarttime, berk_rooms.title ASC');

//        $this->_dbNotRemoved('berk_schedule');

        $query = $this->db->get('berk_schedule');

//        log_message('error', 'ZZZ LAST SQL (getRemoteSchedule) = '.$this->utilitar_db->get_sql_last_run());

        return Utilitar_db::safe_resultSet($query);
    }


    // NOTE: i disabled "WHERE berk_coursetypes.active > 0" check: inactive ones allowed, *removed* ones - are not.
    public function getCourseTypes($yurlico_id, $organization_id, $bIncludeRemoved = false, $training_type_IDs = NULL)
    {
        $this->db->select('berk_coursetypes.id, berk_coursetypes.name, berk_coursetypes.removed, berk_coursetypes.active');
        $this->dbfilter_yurlico_and_organization_multipletables(array('berk_coursetypes'), $yurlico_id, $organization_id); // NOTE: this call is mandatory to be present when accessing berkana assests!

        if (!$bIncludeRemoved)
        {   //$this->_dbNotRemoved($this->db->dbprefix.'berk_coursetypes');
            $this->_dbNotRemoved($this->db->dbprefix.'berk_coursetypes');
        }

//        $this->db->where('berk_coursetypes.active > 0', NULL); // NOTES: I've disabled "WHERE berk_coursetypes.active > 0" check: inactive ones allowed, *removed* ones - are not.

        if ($training_type_IDs && is_array($training_type_IDs) && (count($training_type_IDs) > 0))
        {   $this->db->where_in('id', $training_type_IDs);
        }

        $this->db->order_by('name');

//        log_message('error', "SQL = ".print_r($this->db->_compile_select(), true));
        $query = $this->db->get('berk_coursetypes');

//        log_message('error', 'LAST SQL getTrainingNames = '.$this->utilitar_db->get_sql_last_run());

        return Utilitar_db::safe_resultSet($query);
    }

    public function get_coursetypes_array_filtered($yurlico_id, $organization_id, $bIncludeRemoved, $training_type_IDs, $exclude_stopwords = true, $exclude_duplications = true)
    {
        $coursetypes = $this->getCourseTypes($yurlico_id, $organization_id, $bIncludeRemoved, $training_type_IDs);
        //log_message('error', "QQQ: get_coursetypes_array_filtered RAW($yurlico_id, $organization_id), count=".count($coursetypes).")".print_r($coursetypes, true));

        $coursetypes_without_stopwords = $this->coursetypes_to__id_name__array__filter_stopwords($coursetypes, $exclude_stopwords);
        log_message('error', "QQQ: get_coursetypes_array_filtered NO STOPWORDS($yurlico_id, $organization_id), count=".count($coursetypes_without_stopwords).")".print_r($coursetypes_without_stopwords, true));

        if ($exclude_duplications)
        {
            $coursetypes_without_duplications = $this->compact_coursetypes_by_same_names($coursetypes_without_stopwords);
            //log_message('error', "QQQ: get_coursetypes_array_filtered NO DUPLICATES($yurlico_id, $organization_id), count=".count($coursetypes_without_duplications)." items: ".print_r($coursetypes_without_duplications, true));
            return $coursetypes_without_duplications;
        }
        else
        {   return $coursetypes_without_stopwords;
        }
    }

    // the IDs are incorrect: stored the very first ID from a set of similar-named names!
    // e.g. array {1 => "Английский", 555 => "Английский"}  will become  array{555 => "Английский"} only!
    private function compact_coursetypes_by_same_names(&$coursetypes)
    {
//[75] =>     Раннее развитие 1-2 ВТ и ПТ
//[254] =>    Раннее развитие 1-2 СР
//[1650] =>   Раннее развитие 1-2 ср+пт
//[350] =>    Раннее развитие 2-3 Вт и Пт
//[2600] =>   Раннее развитие 2-3 ПТ
//[255] =>    Раннее развитие 2-3 СР

//        $ci = &get_instance();
//
//        // 1. find check/uncheck marks
//        if ($ci->utilitar->ends_with($client_name, WizardPeopleChecker::MARK_ON))


//    $teststr = "слово0-слово1 слово2 слово3 3312-5321 слово6";
//    $str = UBD::cut_out_ending__of_numbers_pair__dashed($teststr);

//        log_message('error', "XXX before compacting, ".count($coursetypes).': '.print_r($coursetypes, true));
        $ci     = &get_instance();
        $reverted_and_compacted = array();
        foreach ($coursetypes as $id => $name) // include everything, even stop-words:
        {
            $compacted_name = UBD::cut_out_ending__of_numbers_pair__dashed($name);
            $reverted_and_compacted[$compacted_name] = $id;
        }

        // let's revert it back:
        $res = array();
        foreach ($reverted_and_compacted as $compacted_name => $id)
        {   $res[$id] = $compacted_name;
        }

//        log_message('error', "XXX AFTER compacting, ".count($res).': '.print_r($res, true));
        return $res;
    }


    // converts from "слово0-слово1 слово2 слово3 3312-5321 слово6" the "слово0-слово1 слово2 слово3".
    // It is helpful to shorten the "Гончарная мастерская 3-4" to just "Гончарная мастерская".
    // Or should be:
    //          from 'слово0-слово1 слово2 слово3 3312-5321 слово6' to 'слово0-слово1 слово2 слово3'.
    public static function cut_out_ending__of_numbers_pair__dashed($text)
    {
        $numbers_pair__dashed = '/\b(\d+-\d+)/u'; // 'слово0-слово1 слово2 слово3 3312-5321 слово6', after: 'слово0-слово1 слово2 слово3  слово6'

        $matches = array();
        $pos = preg_match($numbers_pair__dashed, $text, $matches);
        if (count($matches) > 0)
        {   $search_for = $matches[0];
            $ss = mb_substr($text, 0, mb_strpos($text, $search_for) - mb_strlen($text));
            return trim($ss);
        }

        return $text;

        $clean_text = "";
        // greco: this just CUTS out the pattern we pass in. Not exaclty our case, but just in case.
        $clean_text = preg_replace($numbers_pair__dashed, '', $text);
    }

    // convert to coursetypes array: the format: {id -> name} elements.
    private function coursetypes_to__id_name__array__filter_stopwords(&$coursetypes, $exclude_stopwords = true)
    {
        $ci     = &get_instance();
        $coursetypes_array = array();
        if ($exclude_stopwords)
        {   foreach ($coursetypes as $elem)
            {   if (!$ci->UBD->is_stop_word($elem->name)) // skip stop-words!
                {   $coursetypes_array[$elem->id] = $elem->name;
                }
            }
        }
        else
        {   foreach ($coursetypes as $elem) // include everything, even stop-words:
            {   $coursetypes_array[$elem->id] = $elem->name;
            }
        }

        return $coursetypes_array;
    }


    // applies CI's ActiveRecord DB "where" condition.
    private function db_where_date__relative2today($table_name, $field_name, $days_before, $days_after)
    {
        if ((0 == $days_before) && (0 == $days_after))
        {   $this->db->where(sprintf('%s.%s BETWEEN CURRENT_DATE AND CURRENT_DATE', $table_name, $field_name), NULL, false);
        }
        else
        {   $this->db->where(sprintf('%s.%s BETWEEN CURRENT_DATE - INTERVAL \'%d\' DAY AND CURRENT_DATE + INTERVAL \'%d\' DAY ', $table_name, $field_name, $days_before, $days_after));
        }
    }
    

    //
    //  Accepts either single table-name or array of table names.
    //  NOTE: This is an optimized version that uses input parameters instead of checking session values each time.
    //
    public function dbfilter_yurlico_and_organization_multipletables($tables_names, $yurlico_id, $organization_id)
    {
        if (is_string($tables_names))
        {   $this->db_careful_filter($tables_names, $yurlico_id, $organization_id);
        }
        else if (is_array($tables_names))
        {
            foreach ($tables_names as $single_table)
            {
                if (is_string($single_table))
                {   $this->db_careful_filter($single_table, $yurlico_id, $organization_id);
                }
            }
        }
    }


    //
    // Ensures that two custom tables will be filtered not by FK-field-name. but by original field-name(s).
    private function db_careful_filter($table_name, $yurlico_id, $organization_id)
    {
        switch ($table_name)
        {
            case 'berk_organizations':
                $this->db->where($table_name.'.id', $organization_id); // note the shortened filed name (id)
                $this->db->where($table_name.'.yurlico_id', $yurlico_id);
                break;

            case 'berk_yurlica':
                $this->db->where($table_name.'.organization_id', $organization_id);
                $this->db->where($table_name.'.id', $yurlico_id); // note the shortened filed name (id)
                break;

            default:
                $this->db->where($table_name.'.yurlico_id', $yurlico_id);
                $this->db->where($table_name.'.organization_id', $organization_id);
                break;
        }
    }

    // searches for words not to be supplied to the End-User, like "Аренда", "АрендаС", "АрендаТ", etc.
    public function is_stop_word($word)
    {
        // log_message('error', "---- YYYY is_stop_word('$word') search in:\n".print_r(UBD::$stop_words__training_types, true));
        foreach (UBD::$stop_words__training_types as $stop_word)
        {   if (0 === mb_strpos($word, $stop_word)) // ignores stopwords contained in a middle/end of the "$word"!
            {   return true;
            }
        }

        return false;
    }


    //
    // Unfortunately Berkana provides not "training TYPEs" but "training group's names"!
    // So we are creating ARTIFICAL DATA STRUCTURE: due to missing REAL training types in Berkana :(
    // This is for making a RESERVATION web-server must supply real TRAINING TYPES (because User is not interested in "Wu-Shu-1" or "Wu-shu-2" groups - he just want "Wu-shu".
    // Those FAKE structures to be used during RESERVATION and NOTIFICATION (in the email to User/Administrator).
    //
    //  Note #1: that this function makes similar training types spelled "Wu-Shu" and "WU-SHU" (previously they were error source for a detection of trt_id).
    //  Note #2: Skips training_types (and their trainings) contained in stop-words. E.g. "аренда", "ТЕСТОВЫЙ ФИТНЕС" and etc.: because those "trainings" are NOT FOR PUBLICITY.
    //
    public function getTrainingTypesAndNamesMapping(&$training_names)
    {
        $training_type_names = array();
        $training_type_training_ids = array(); // training type maps to its trainings list.

        // let's first collect training_ids in one place - for specific trainig_type:
        foreach ($training_names as $training)
        {
            // extract artifical (!) training_type from the training_name:
            $typename                       = $this->convert_name_to_type($training->name);

            if ($this->is_stop_word($typename))
            {   log_message('debug', "ignoring '$typename' as a stop-word for training types!");
                continue;
            }


            // Use the lowercase typename as a key only.
            // We don't use lower-cased training_type_name as the one to show to the end-user, due to obvious reason (hopefully for you, dear Reader).
            $typename_lower_case            = mb_strtolower($typename);

            $training_type_names[$typename_lower_case]= $typename; // Actual training type name for the End-User.
            $training_type_training_ids[$typename_lower_case][]= $training->id;
        }

//log_message('error', "QQQ training_type_training_ids:\n".print_r($training_type_training_ids, true));

        // now let's combine all this into a sinle result array:
        $res = array();
        foreach ($training_type_names as $typename_lower_case => $typename)
        {   $res[] = array('id' => $this->calc_training_type__code($typename), 'name' => $typename, 'training_ids' => $training_type_training_ids[$typename_lower_case]);
        }

        return $res;
    }

    private function calc_training_type__code($training_type_name)
    {
        return crc32(mb_strtolower($training_type_name)."LgiCvsW"); // concatenate with secret code.
    }

    // disallowed chars: digits and "."
    const MAX_NESTING_LEVEL = 3;

    private function convert_name_to_type($training_name, $nesting_level = 0)
    {
        // exit recursion here:
        if ($nesting_level > Util_berkana_data::MAX_NESTING_LEVEL)
        {   return trim($training_name); // return the original with almost no changes to it.
        }

        $last_symbol        = mb_substr($training_name, -1, 1);
        if (is_numeric($last_symbol) || ('.' == $last_symbol) || ('-' == $last_symbol)) // disallowed chars are listed here.
        {   return $this->convert_name_to_type(trim(mb_substr($training_name, 0, -1)), ++$nesting_level);
        }

        return trim($training_name);
    }

    // returns object of null:
    public function getOrganizationContacts($organization_id)
    {
        // ASSUMPTION: email is assumed COMMON across all the organizations and equal to yurlico's website.
        $this->db->select("berk_organizations.name, berk_organizations.address, berk_organizations.email, berk_organizations.phone, berk_organizations.trello_email_for_tasks, berk_organizations.guid, berk_yurlica.web__website");
        $this->db->join('berk_yurlica', 'berk_yurlica.id = berk_organizations.yurlico_id', 'left');
        $this->db->where('berk_organizations.id', $organization_id);

//        log_message('error', "SQL = ".print_r($this->db->_compile_select(), true));
        $query = $this->db->get('berk_organizations');
        return Utilitar_db::safe_resultRow($query);
    }

    public function get_family_trainings_for_date($family_id, $days_before, $days_after, $yurlico_id, $organization_id)
    {
/*SELECT DISTINCT
wt_berk_clients_visits.id AS membershipelement_id,
wt_berk_clients.family_id,
wt_berk_clients_visits.client_id,
wt_berk_clients_visits.schedule_id,
wt_berk_schedule.coursetype_id,
wt_berk_coursetypes.name,
wt_berk_schedule.teacher_id,
wt_berk_schedule.plannedroom_id AS room_id,
wt_berk_schedule.planneddate,
wt_berk_schedule.plannedstarttime,
wt_berk_schedule.plannedfinishtime,
wt_berk_clients_visits.schedule_id AS actualday_id
FROM  wt_berk_clients_visits
LEFT JOIN wt_berk_schedule ON wt_berk_clients_visits.schedule_id=wt_berk_schedule.id
LEFT JOIN wt_berk_coursetypes ON wt_berk_schedule.coursetype_id = wt_berk_coursetypes.id
LEFT JOIN wt_berk_clients ON wt_berk_clients.id = wt_berk_clients_visits.client_id
WHERE
    (wt_berk_clients_visits.yurlico_id = 27 AND wt_berk_clients_visits.organization_id = 3)
    AND (wt_berk_schedule.yurlico_id = 27 AND wt_berk_schedule.organization_id = 3)
    AND (wt_berk_coursetypes.yurlico_id = 27 AND wt_berk_coursetypes.organization_id = 3)
    AND (wt_berk_clients.yurlico_id = 27 AND wt_berk_clients.organization_id = 3)
    AND wt_berk_clients.family_id=85
    AND  (wt_berk_schedule.planneddate BETWEEN CURRENT_DATE - INTERVAL '0' DAY AND CURRENT_DATE + INTERVAL '8' DAY)
    AND  (
        ( (wt_berk_schedule.removed = 0) OR (wt_berk_schedule.removed IS NULL) )
        AND ( (wt_berk_schedule.canceled = 0) OR (wt_berk_schedule.canceled IS NULL) )
         )
ORDER BY wt_berk_schedule.planneddate
    */
        $db_prefix = $this->db->dbprefix;

        $this->db->distinct();
        $this->db->select(' berk_clients_visits.id AS membershipelement_id,
                            berk_clients_visits.client_id,
                            berk_clients_visits.schedule_id,
                            berk_clients_visits.schedule_id AS actualday_id,
                            berk_clients.family_id,
                            berk_coursetypes.name,
                            berk_schedule.teacher_id,
                            berk_schedule.plannedroom_id AS room_id,
                            berk_schedule.planneddate,
                            berk_schedule.plannedstarttime,
                            berk_schedule.plannedfinishtime,
                            berk_schedule.coursetype_id');

        $this->db->join('berk_schedule',    'berk_clients_visits.schedule_id = berk_schedule.id',   'left');
        $this->db->join('berk_coursetypes', 'berk_schedule.coursetype_id = berk_coursetypes.id',    'left');
        $this->db->join('berk_clients',     'berk_clients.id = berk_clients_visits.client_id',      'left');

        $this->dbfilter_yurlico_and_organization_multipletables(    array( $db_prefix.'berk_clients_visits',
                                                                            $db_prefix.'berk_schedule',
                                                                            $db_prefix.'berk_coursetypes',
                                                                            $db_prefix.'berk_clients'
                                                                         ),
                                                                    $yurlico_id,
                                                                    $organization_id
                                                 ); // NOTE: this call is mandatory to be present when accessing berkana assests!
        $this->db->where('berk_clients.family_id', $family_id);
        $this->db->where('berk_coursetypes.active = true'); // Inactive ones are not allowed.

        $this->db_where_date__relative2today($db_prefix.'berk_schedule', 'planneddate', $days_before, $days_after);

        $this->_dbNotRemoved($db_prefix.'berk_coursetypes');// we also hide removed course-types as well.
        $this->_dbNotRemoved($db_prefix.'berk_schedule');
        $this->_dbNotCanceled($db_prefix.'berk_schedule');

        $this->db->order_by('berk_schedule.planneddate');

//        log_message('error', "SQL = ".print_r($this->db->_compile_select(), true));
//        log_message('error', "QQQ SQL = ".print_r($this->utilitar_db->get_sql_compiled(), true));

        $query = $this->db->get('berk_clients_visits');

        return Utilitar_db::safe_resultSet($query);
    }

    public function getRemoteTrainers($yurlico_id, $organization_id, $bAll_fields, $active_only, $trainers_IDs = NULL)
    {
        $this->db->select(($bAll_fields ?   "berk_trainers.*, users.email AS local_user_email" :
                                            "berk_trainers.id, berk_trainers.name")); // or use shorter version, when needed.

        $this->dbfilter_yurlico_and_organization_multipletables(Util_berkana_data::TBL_TRAINERS, $yurlico_id, $organization_id); // NOTE: this call is mandatory to be present when accessing berkana assests!

        $this->db->join('users', "users.id = berk_trainers.local_user_id", 'left');
        $this->db->order_by('berk_trainers.name');

        if ($active_only)
        {   $this->_dbNotRemoved('wt_berk_trainers');
        }

        if ($trainers_IDs && is_array($trainers_IDs) && (count($trainers_IDs) > 0))
        {   $this->db->where_in('berk_trainers.id', $trainers_IDs);
        }

        $query = $this->db->get('berk_trainers');

        $res = Utilitar_db::safe_resultSet($query);;
        return $res;
	}

    // removes patronymic for shorter presentation on mobile client:
    public function getTrainersShort($yurlico_id, $organization_id, $active_only, $trainers_IDs = NULL)
    {
        $trainers = $this->getRemoteTrainers($yurlico_id, $organization_id, false, $active_only, $trainers_IDs);

        $this->shortenNames($trainers);

        return $trainers;
    }

    //
    // Renames title to name: for mobile client (TODO: change mobile client namings indeed!)
    //
    public function getRooms($yurlico_id, $organization_id)
    {
        $this->db->select("berk_rooms.id, berk_rooms.title as name");
        $this->db->order_by('berk_rooms.title ASC');

        $this->dbfilter_yurlico_and_organization_multipletables(Util_berkana_data::TBL_ROOMS, $yurlico_id, $organization_id); // NOTE: this call is mandatory to be present when accessing berkana assests!

        $query = $this->db->get('berk_rooms');

        return Utilitar_db::safe_resultSet($query);
    }

    public function getOrganization($organization_id)
    {
        return $this->CI->utilitar_db->_getTableRow('berk_organizations', 'id', $organization_id);
    }

    // shortens human names. Mandatory field-name
    private function shortenNames(&$rows, $name_column = 'name')
    {
        foreach ($rows as $row)
        {   $row->$name_column = $this->reduce_FIO($row->$name_column);
        }
    }

    // Removes patronymic from "$str_FIO" and leaves fistname, lastname only.
    public function reduce_FIO($str_FIO, $b_swap_firstname_lastname = false)
    {
        $res = null;

        $arr = explode(' ', $str_FIO);
        if (count($arr) >= 2)
        {
            if ($b_swap_firstname_lastname)
            {   $res = $arr[0].' '.$arr[1]; // lastname, firstname.
            }
            else
            {   $res = $arr[1].' '.$arr[0]; // firstname, lastname.
            }
        }
        else if (1 === count($arr))
        {   $res = $arr[0]; // fisrtname only.
        }
        else
        {   $res = 'n/a';
        }

//        log_message('error', "reduce_FIO('".$str_FIO."') => ".$res);
        return $res;
    }
    
    public function get_family_trainers($family_id, $active_only, $yurlico_id, $organization_id)
    {
/*SELECT DISTINCT
    wt_berk_schedule.teacher_id,
    wt_berk_trainers.name,
    wt_berk_clients_visits.client_id
FROM  wt_berk_clients_visits

    LEFT JOIN wt_berk_schedule ON wt_berk_clients_visits.schedule_id=wt_berk_schedule.id
    LEFT JOIN `wt_berk_trainers` ON wt_berk_trainers.id = wt_berk_schedule.teacher_id
    LEFT JOIN wt_berk_coursetypes ON wt_berk_schedule.coursetype_id = wt_berk_coursetypes.id
    LEFT JOIN wt_berk_clients ON wt_berk_clients.id = wt_berk_clients_visits.client_id

    WHERE
    	(wt_berk_schedule.yurlico_id = 37 AND wt_berk_schedule.organization_id = 5)
    	AND (wt_berk_coursetypes.yurlico_id = 37 AND wt_berk_coursetypes.organization_id = 5)
    	AND (wt_berk_clients_visits.yurlico_id = 37 AND wt_berk_clients_visits.organization_id = 5)
    	AND (wt_berk_clients.yurlico_id = 37 AND wt_berk_clients.organization_id = 5)
    	AND wt_berk_clients.family_id=345
    GROUP BY wt_berk_schedule.teacher_id
    */
        $db_prefix = $this->db->dbprefix;

        $this->db->distinct();
        $this->db->select(' berk_schedule.teacher_id, berk_trainers.name, berk_clients_visits.client_id');

        $this->db->join('berk_schedule',    'berk_clients_visits.schedule_id = berk_schedule.id',   'left');
        $this->db->join('berk_trainers',    'berk_trainers.id = berk_schedule.teacher_id',    'left');
        $this->db->join('berk_coursetypes', 'berk_schedule.coursetype_id = berk_coursetypes.id',    'left');
        $this->db->join('berk_clients',     'berk_clients.id = berk_clients_visits.client_id',      'left');

        $this->dbfilter_yurlico_and_organization_multipletables(array( $db_prefix.'berk_clients_visits',
                                                        $db_prefix.'berk_schedule',
                                                        $db_prefix.'berk_trainers',
                                                        $db_prefix.'berk_coursetypes',
                                                        $db_prefix.'berk_clients'),
                                                        $yurlico_id,
                                                        $organization_id
                                                 ); // NOTE: this call is mandatory to be present when accessing berkana assests!
        $this->db->where('berk_clients.family_id', $family_id);
        $this->db->where('berk_coursetypes.active = true'); // Inactive ones are not allowed.

        if ($active_only)
        {   $this->_dbNotRemoved($db_prefix.'berk_trainers');// we could also skip removed trainers as well.
        }
        $this->_dbNotRemoved($db_prefix.'berk_coursetypes');// we also hide removed course-types as well.
        $this->_dbNotRemoved($db_prefix.'berk_schedule');
        $this->_dbNotCanceled($db_prefix.'berk_schedule');

        $this->db->group_by('berk_schedule.teacher_id');

//        log_message('error', "SQL = ".print_r($this->db->_compile_select(), true));
//        log_message('error', "QQQ SQL = ".print_r($this->utilitar_db->get_sql_compiled(), true));

        $query = $this->db->get('berk_clients_visits');

        return Utilitar_db::safe_resultSet($query);
    }

    // returns family record in case if it is linked to existing "$user_id".
    public function get_family_by_local_user_id($local_user_id, $yurlico_id, $organization_id)
    {
        $this->db->where('local_user_id', $local_user_id);
        $this->dbfilter_yurlico_and_organization_multipletables(Util_berkana_data::TBL_FAMILIES, $yurlico_id, $organization_id); // NOTE: this call is mandatory to be present when accessing berkana assests!
        $this->db->limit(1);

        $query = $this->db->get(Util_berkana_data::TBL_FAMILIES);
        return Utilitar_db::safe_resultRow($query);
    }

    public function get_family_childrennames($family_id, $yurlico_id, $organization_id)
    {
    /*
      SELECT
              wt_berk_clients.id,
        CONCAT(wt_berk_clients.firstname, ' ', wt_berk_clients.lastname) AS childname
      FROM wt_berk_clients

      where wt_berk_clients.yurlico_id = 37
      AND wt_berk_clients.organization_id=5
      and wt_berk_clients.family_id = 345
     */

        $this->db->select('id, CONCAT(wt_berk_clients.firstname, \' \', wt_berk_clients.lastname) AS childname', FALSE);
        $this->db->where('berk_clients.family_id', $family_id);
        $this->dbfilter_yurlico_and_organization_multipletables('berk_clients', $yurlico_id, $organization_id); // NOTE: this call is mandatory to be present when accessing berkana assests!
        $this->db->order_by('id');

        $query = $this->db->get('berk_clients');

        return Utilitar_db::safe_resultSet($query);
    }

    public function get_family_abonements($family_id, $b_active_only, $yurlico_id, $organization_id)
    {
    /*
SELECT DISTINCT
	wt_berk_abonements.id,
	wt_berk_abonements.`child_id`,
	wt_berk_abonements.`family_id`,
	wt_berk_abonements.`date_start` AS valid_from,
	wt_berk_abonements.`date_end` AS valid_till,
	wt_berk_abonements.`visits_passed`,
	wt_berk_abonements.`visits_count`,
	0 AS visits_moved,
	wt_berk_coursetypes.name
FROM `wt_berk_abonements`
LEFT JOIN wt_berk_coursetypes ON wt_berk_coursetypes.id = wt_berk_abonements.coursetype_id
WHERE wt_berk_coursetypes.active = TRUE
AND wt_berk_abonements.removed = 0
 AND wt_berk_abonements.family_id = 345
 AND (wt_berk_abonements.date_end >= CURRENT_DATE)
ORDER BY valid_till ASC;
     */

        $this->db->select('berk_abonements.id, berk_abonements.coursetype_id, berk_abonements.child_id, berk_abonements.family_id, berk_abonements.date_start AS valid_from, berk_abonements.date_end AS valid_till,
        berk_abonements.visits_passed, berk_abonements.visits_count, 0 AS visits_moved, berk_coursetypes.name');
        $this->db->distinct();
        $this->db->join('berk_coursetypes', 'berk_coursetypes.id = berk_abonements.coursetype_id', 'left');

        $this->db->where('berk_coursetypes.active = true'); // removed are allowed. Inactive ones - are not.
        $this->db->where('berk_abonements.family_id', $family_id);
        $this->db->where('berk_abonements.removed', 0);
        if ($b_active_only)
        {   $this->db->where('wt_berk_abonements.date_end >= CURRENT_DATE', NULL, FALSE);
        }

        $this->dbfilter_yurlico_and_organization_multipletables('berk_abonements', $yurlico_id, $organization_id); // NOTE: this call is mandatory to be present when accessing berkana assests!
        $this->db->order_by('berk_abonements.date_end ASC');

//        log_message('error', "SQL = ".print_r($this->db->_compile_select(), true));
        $query = $this->db->get('berk_abonements');

        return Utilitar_db::safe_resultSet($query);
    }



    // differentiate training names (by name post-fix) if family has numtiple children
    private function rename_trainings_personal_for_multiple_children($family_id, &$family_trainings, $yurlico_id, $organization_id)
    {
        $ci     = &get_instance();
        $children           = $this->get_family_childrennames($family_id, $yurlico_id, $organization_id);
        $children_hashtable = Util_berkana::transformToHashTable($children);

        foreach ($family_trainings as &$family_training)
        {   $family_training->name .= ' ('.$children_hashtable[$family_training->client_id]->childname.')';
        }
    }

    public function get_organization_by_guid($guid)
	{   return $this->utilitar_db->_getTableRow('berk_organizations', 'guid', $guid);
	}

    public function getYurlicoById($row_id)
    {
        return $this->CI->utilitar_db->_getTableRow('berk_yurlica', 'id', $row_id);
    }


    // WARNINNG: in some cases (configurable by widget!) it is possible to get on input not TR_TYPES_IDS but TRAINING_IDS!
    public function training_directions_to_text($yurlico_id, $organization_id, $training_type_IDs)
    {
        log_message('error', "training_directions_to_text() training_type_IDs: ".print_r($training_type_IDs, true));
        $SEPARATOR = ', ';
        $res = '';

        $coursetypes = $this->getCourseTypes($yurlico_id, $organization_id, false, $training_type_IDs);

        foreach ($coursetypes as $coursetype)
        {   $res .= '"'.$coursetype->name.'"'.$SEPARATOR;
        }

        // remove the trailing separator (if any):
        if (strlen($res) > 0)
        {   $res = substr($res, 0, -strlen($SEPARATOR));
        }

        return $res;
    }

    public static function json_CORS_print($error_code, $message)
    {
        UBD::set_CORS_json_header();

        $res = array('errcode' => $error_code, 'message' => $message);
        $returnValue = json_encode($res);

        $ci = &get_instance();
        $ci->output->set_output($returnValue);
    }

    public static function json_CORS_success($message)
    {
        UBD::json_CORS_print(0, $message);
    }

    // TODO: check out the source! Get rid of "access to everybody" mask!
    private static function set_CORS_json_header()
    {
        $CI = &get_instance();

        // This header a must for CORS!!! Or reply per each site: "Access-Control-Allow-Origin: http://zinoui.com":
        header("Access-Control-Allow-Origin: *"); // TODO: check for $organization_guid to provide proper Access-Control-Origin web-site mask! (might make no sense if other wusers will want to put that in).
        header('Content-type: application/json'); // Json_base::set_json_header();
//        $CI->output->set_status_header('200');
//
        $CI->output->set_header("X-Accel-Buffering: no");
        $CI->output->set_header("X-Accel-Limit-Rate: off");
//        $CI->output->set_header("MyHeader: 12345");
    }


    public function get_personal_trainings($local_userid, $days_before = 0, $days_after = 7)
    {
        $trainings_personal = array(); // NOTE: club's schedule is retrieved via "/schedule" method separately!

        // only for logged-in user this data makes sense:

        $user   = $this->users->get_user_by_id_full($local_userid);
        $family = $this->get_family_by_local_user_id($local_userid, $user->yurlico_id, $user->organization_id);

        if (!$user || !$family)
        {   log_message('error', "get_personal_trainings(local_userid:$local_userid): no Berkana user or family found!!");
            return array();
        }

        $family_trainings = $this->get_family_trainings_for_date($family->id, $days_before, $days_after, $user->yurlico_id, $user->organization_id);

        if ($this->get_children_count($family_trainings, 'client_id') > 1)
        {   $this->rename_trainings_personal_for_multiple_children($family->id, $family_trainings, $user->yurlico_id, $user->organization_id);
        }
//                log_message('error', 'FAMILY_TRAININGS ('.Json_base::get_current_yurlico_id__org_id_string().', f:'.$family->id.'): '.count($family_trainings)." retrieved.\n".print_r($family_trainings, true));
//                log_message('error', 'FAMILY_TRAININGS ('.Json_base::get_current_yurlico_id__org_id_string().', f:'.$family->id.'): '.count($family_trainings)." retrieved:\n".print_r($family_trainings, true));
        foreach ($family_trainings as $family_training)
        {
            $trainings_personal[] = array(
                'id'        => $family_training->membershipelement_id,
                'club_id'   => $user->organization_id,
                'training'  => $family_training->name,

                'trainer_id'=> $family_training->teacher_id,
                'cabinet_id'=> $family_training->room_id,

                'state'     => '0', // canceled if == 1. If 0 then that training has NOT BEEN CANCLEDED.

                'datetime' => array('startdate' => $family_training->planneddate, 'starttime' => $family_training->plannedstarttime, 'endtime' => $family_training->plannedfinishtime),
            );
        }

//        log_message('error', "XXX trainings_personal=".print_r($trainings_personal, true));
        return $trainings_personal;
    }

    // gets remote id based on local_id:
    public function get_berkana_trainer($local_user_id)
    {
        $this->db->where('local_user_id', $local_user_id);
        $query = $this->db->get('berk_trainers');

        return Utilitar_db::safe_resultRow($query);
    }
    
    public function get_trainer_trainings_for_date($trainer_remote_id, $days_before, $days_after, $yurlico_id, $organization_id)
    {
/*
SELECT
  wt_berk_schedule.planneddate AS PL_DATE,
  wt_berk_schedule.plannedstarttime AS PL_STARTTIME,
  wt_berk_schedule.plannedfinishtime AS PL_ENDTIME,
wt_berk_schedule.id,
wt_berk_coursetypes.id AS training_type,
wt_berk_coursetypes.name AS training,
wt_berk_schedule.`plannedroom_id` AS cabinet_id,
-- wt_berk_schedule.canceled AS state,
UNIX_TIMESTAMP(TIMESTAMP(wt_berk_schedule.planneddate, wt_berk_schedule.plannedstarttime)) AS start_time,
UNIX_TIMESTAMP(TIMESTAMP(wt_berk_schedule.planneddate, wt_berk_schedule.plannedfinishtime)) AS end_time
FROM wt_berk_schedule
LEFT JOIN wt_berk_trainers ON wt_berk_trainers.id = wt_berk_schedule.teacher_id
LEFT JOIN wt_berk_coursetypes ON wt_berk_schedule.coursetype_id = wt_berk_coursetypes.id
WHERE
	(wt_berk_schedule.yurlico_id = 37 AND wt_berk_schedule.organization_id = 5)
	AND (wt_berk_coursetypes.yurlico_id = 37 AND wt_berk_coursetypes.organization_id = 5)
	AND (wt_berk_trainers.yurlico_id = 37 AND wt_berk_trainers.organization_id = 5)
	AND wt_berk_schedule.teacher_id = 30 -- 30:Marina Krasnoyarskaya, 9:Asiya Ravilevna
	AND  (wt_berk_schedule.planneddate BETWEEN CURRENT_DATE - INTERVAL '0' DAY AND CURRENT_DATE + INTERVAL '7' DAY)
	AND  (
		( (wt_berk_schedule.removed = 0) OR (wt_berk_schedule.removed IS NULL) )
		AND ( (wt_berk_schedule.canceled = 0) OR (wt_berk_schedule.canceled IS NULL) )
	     )
ORDER BY wt_berk_schedule.planneddate, wt_berk_schedule.plannedstarttime, wt_berk_coursetypes.name;
*/
        $db_prefix = $this->db->dbprefix;
        $this->db->select(' wt_berk_schedule.id,
                            wt_berk_coursetypes.id AS training_type,
                            wt_berk_coursetypes.name AS training,
                            wt_berk_schedule.`plannedroom_id` AS cabinet_id,
                            UNIX_TIMESTAMP(TIMESTAMP(wt_berk_schedule.planneddate, wt_berk_schedule.plannedstarttime)) AS start_time,
                            UNIX_TIMESTAMP(TIMESTAMP(wt_berk_schedule.planneddate, wt_berk_schedule.plannedfinishtime)) AS end_time', FALSE);

        $this->db->join('berk_trainers',    'berk_trainers.id = berk_schedule.teacher_id',      'left');
        $this->db->join('berk_coursetypes', 'berk_schedule.coursetype_id = berk_coursetypes.id','left');

        $this->dbfilter_yurlico_and_organization_multipletables(array(  $db_prefix.'berk_schedule',
                                                                        $db_prefix.'berk_coursetypes',
                                                                        $db_prefix.'berk_trainers'),
                                                        $yurlico_id,
                                                        $organization_id
                                                 ); // NOTE: this call is mandatory to be present when accessing berkana assests!
        $this->db->where('berk_schedule.teacher_id', $trainer_remote_id);

        $this->db->where('berk_coursetypes.active = true'); // Inactive ones are not allowed.

        $this->db_where_date__relative2today($db_prefix.'berk_schedule', 'planneddate', $days_before, $days_after);

        $this->_dbNotRemoved($db_prefix.'berk_schedule');
        $this->_dbNotCanceled($db_prefix.'berk_schedule');

        $this->db->order_by('berk_schedule.planneddate, berk_schedule.plannedstarttime, berk_coursetypes.name');

        $query = $this->db->get('berk_schedule');

        return Utilitar_db::safe_resultSet($query);
    }

    //
    //  NOTE:    Returns either 'mobile' or 'berkana' set of visits' fields only!
    //
    //  Returns the list of clients' visits for trainings relative to current (!) day. See $days_before and $days_after.
    //  NOTE: Ensure to keep in sync the fields/fieldnames with Berkana_PlannedVisit fields!
    //
    //  NOTE: version #2 returns NO COUTSETYPE_NAME!!!
    //
    //  TODO: check if returning only verion/mobileVersion > 0 items make sense!!!
    ///
    public function get_planned_visits_for_date_v2($type, $days_before, $days_after, $client_IDs, $yurlico_id, $organization_id, $modified_only = false, $cabinet_id = -1, $schedule_id = -1)
    {
//        log_message('error', "get_planned_visits_for_date(type, days_before, days_after, yurlico_id, organization_id)=($type, $days_before, $days_after, $yurlico_id, $organization_id)");
        $db_field_name__VISITED = null;
        $db_field_name__VERSION = null;
        switch ($type)
        {
            case Util_berkana_data::VISITS_TBL_MOBILE:
                $db_field_name__VISITED = 'mobileVisited';
                $db_field_name__VERSION = 'mobileVersion';
                break;

            case Util_berkana_data::VISITS_TBL_BERKANA:
                $db_field_name__VISITED = 'visited';
                $db_field_name__VERSION = 'version';
                break;

            default:
            {   log_message('error', 'ERROR! unrecognized type of planned visits ('.$type.') requested. SKIPPING!');
                return array();
            }
        }

        $db_prefix = $this->db->dbprefix;

        return $this->rewrittenQuery_v2($yurlico_id, $organization_id, $client_IDs, $db_field_name__VISITED, $db_field_name__VERSION, $days_before, $days_after, $modified_only, $cabinet_id, $schedule_id);
    }

    //
    //  Function provides data for "get_planned_visits_for_date_v2().
    //  NOTE: version #2 returns NO COUTSETYPE_NAME!!!
    //
    //  IMPORTANT:
    //  Ensure to update WHERE conditions simultaneously with "getRoomTrainingsArray()" or you will face data discrepancy in a result set!
    private function rewrittenQuery_v2($yurlico_id, $organization_id, $client_IDs, $db_field_name__VISITED, $db_field_name__VERSION, $days_before, $days_after, $modified_only, $cabinet_id, $schedule_id)
    {
/*
SELECT
DISTINCT wt_berk_schedule.coursetype_id, IFNULL(wt_berk_clients_visits.mobileVisited, 0) AS visited,
IFNULL(wt_berk_clients_visits.mobileVersion, 0) AS VERSION,
wt_berk_clients_visits.id,
wt_berk_clients_visits.client_id, -- wt_berk_clients_visits.yurlico_id AS clientvisits_yurlico_id,
wt_berk_clients.firstname, wt_berk_clients.lastname,
wt_berk_clients_visits.schedule_id,
wt_berk_schedule.*
FROM  wt_berk_clients_visits
LEFT JOIN wt_berk_schedule ON wt_berk_clients_visits.schedule_id=wt_berk_schedule.id
LEFT JOIN wt_berk_clients ON wt_berk_clients.id = wt_berk_clients_visits.client_id
WHERE
wt_berk_schedule.id = 24615 AND         -- is optional!
client_id IN (1041, 1042, 1043) AND     -- is optional!
(wt_berk_clients_visits.yurlico_id = 37 AND wt_berk_clients_visits.organization_id = 5)
AND (wt_berk_schedule.yurlico_id = 37 AND wt_berk_schedule.organization_id = 5)
AND (wt_berk_schedule.planneddate BETWEEN CURRENT_DATE - INTERVAL '0' DAY AND CURRENT_DATE + INTERVAL '7' DAY)
AND ( ( (`wt_berk_schedule`.`removed` = 0) OR (`wt_berk_schedule`.`removed` IS NULL) ) AND ( (`wt_berk_schedule`.`canceled` = 0) OR (`wt_berk_schedule`.`canceled` IS NULL) ) )
AND ( (`wt_berk_clients_visits`.`removed` = 0) OR (`wt_berk_clients_visits`.`removed` IS NULL) )
ORDER BY wt_berk_schedule.planneddate
*/
        log_message('error', "rewrittenQuery_v2($yurlico_id, $organization_id, $db_field_name__VISITED, $db_field_name__VERSION, $days_before, $days_after, $modified_only, $cabinet_id, $schedule_id) for clients: ".print_r($client_IDs, true));
        $db_prefix = $this->db->dbprefix;

        $this->db->select(' berk_schedule.coursetype_id, IFNULL('.$db_prefix.'berk_clients_visits.'.$db_field_name__VISITED.', 0) AS visited,
                            IFNULL('.$db_prefix.'berk_clients_visits.'.$db_field_name__VERSION.', 0) as version,
                            berk_clients_visits.id,
                            berk_clients_visits.client_id,
                            berk_clients.firstname, berk_clients.lastname,
                            berk_clients_visits.schedule_id ', FALSE);
        $this->db->distinct();
        $this->db->from('berk_clients_visits');
        $this->db->join('berk_schedule',    'berk_clients_visits.schedule_id = berk_schedule.id',   'left');
        $this->db->join('berk_clients',     'berk_clients.id = berk_clients_visits.client_id',   'left');

        // get only MODIFIED fields. Makes sense for WTService "/get_data/1" calls:
        if ($modified_only)
        {   $this->db->where('berk_clients_visits.'.$db_field_name__VERSION.' > 0');
        }

        if (count($client_IDs) > 0)
        {   $this->db->where_in('berk_clients_visits.client_id', $client_IDs);
        }

        if ($cabinet_id > 0)
        {   $this->db->where('berk_schedule.plannedroom_id', $cabinet_id);
        }

        if ($schedule_id > 0)
        {   $this->db->where('berk_schedule.id', $schedule_id);
        }

        // NOTE: this call is mandatory to be present when accessing berkana assests!
        $this->dbfilter_yurlico_and_organization_multipletables(array('berk_clients_visits', 'berk_schedule', 'berk_clients'), $yurlico_id, $organization_id);

//        $this->db->where('berk_coursetypes.active > 0'); // даже неактивные показываются. А вот удалённые (removed==true) уже не должны показываться.

        $this->db_where_date__relative2today($db_prefix.'berk_schedule', 'planneddate', $days_before, $days_after);

        $this->_dbNotRemoved ($db_prefix.'berk_schedule');
        $this->_dbNotCanceled($db_prefix.'berk_schedule');
        $this->_dbNotRemoved ($db_prefix.'berk_clients_visits');

        $this->db->order_by('berk_schedule.planneddate');

        $query = $this->db->get();

        return Utilitar_db::safe_resultSet($query);
    }


    // differentiate abonements (by name post-fix) if family has numtiple children: postprocess the "coursename".
    public function rename_abonements_for_multiple_children($family_id, &$abonements, $yurlico_id, $organization_id)
    {
        $children           = $this->get_family_childrennames($family_id, $yurlico_id, $organization_id);
        $children_hashtable = Util_berkana::transformToHashTable($children);

        foreach ($abonements as &$abonement)
        {   $abonement->name .= ' ('.$children_hashtable[$abonement->child_id]->childname.')';
        }
    }

    public function get_children_count(&$rowset, $child_id_rowname)
    {
        $children = array();
        foreach ($rowset as $row)
        {   $children[$row->$child_id_rowname] = 1;
        }

        return count($children);
    }

    // fully prefixed table name neeed.
    private function _dbNotRemoved($table_name)
    {   $this->db->where('( (`'.$table_name.'`.`removed` = 0) OR (`'.$table_name.'`.`removed` IS NULL) ) ', NULL, FALSE);
    }

    // fully prefixed table name neeed.
    private function _dbNotCanceled($table_name)
    {   $this->db->where('( (`'.$table_name.'`.`canceled` = 0) OR (`'.$table_name.'`.`canceled` IS NULL) ) ', NULL, FALSE);
    }

    private function _getQueryNotRemovedAndNotCanceled($table_name)
    {
        return ' ( ( (`'.$table_name.'`.`removed` = 0) OR (`'.$table_name.'`.`removed` IS NULL) ) AND ( (`'.$table_name.'`.`canceled` = 0) OR (`'.$table_name.'`.`canceled` IS NULL) ) ) ';
    }


    private function _getQueryNotRemoved($table_name)
    {
        return  ' ( (`'.$table_name.'`.`removed` = 0) OR (`'.$table_name.'`.`removed` IS NULL) ) ';
    }


    public function get_training_type_description_by_crc32($yurlico_id, $crc32_salted)
    {
        $this->db->where('crc32_salted', $crc32_salted);
        $this->db->where('yurlico_id', $yurlico_id); // this is be default.
        $this->db->limit(1);

        $query = $this->db->get('tr_types_descriptions');
        $row = Utilitar_db::safe_resultRow($query);

        return $row ? $row->tr_type_description : '';
    }

    public static function hash_trtype_name($yurlico_id, $tr_type_name)
    {
        return crc32(Util_berkana_data::TRTYPE_SALT.$yurlico_id.$tr_type_name); // contact the salt, yurlicoid and the value itself.
    }

    public function session_get_yurlico_id()
    {   //return $this->CI->session->userdata(Util_berkana_data::SESSIONKEY__YURLICO_ID);
        $res = $this->get_current_user_yurlico_and_organization();
        return ($res ? $res->yurlico_id : -1);
    }

    private function get_current_user_yurlico_and_organization()
    {
        $user_id = $this->utilitar->get_current_user_id();
        if ($user_id <= 0)
        {   return NULL;
        }

        $this->db->select('users.id, users.username, users.email, users.activated, users.banned, user_profiles.name, user_profiles.yurlico_id, user_profiles.organization_id, user_profiles.phone, user_profiles.description');
        $this->db->from('users');
        $this->db->join('user_profiles', 'user_profiles.user_id = users.id', 'left');
        $this->db->where('users.id', $user_id);

        $query = $this->db->get();
        return Utilitar_db::safe_resultRow($query);

//        return $this->get_user_by_id_full_SECURED($user_id);
    }

}
