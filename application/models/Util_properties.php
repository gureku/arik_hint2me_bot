<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

// The "Util_properties" class manages (variable) set of entity properties CRU(D) operations.
//  Useful for dealing with nodes with different count/types of leaves (a.k.a. "Document Storage").
class Util_properties extends CI_Model /* see http://codeigniter.com/wiki/Inserting_Multiple_Records_Into_a_Table/ */
{
    const ICON_MISSING = "\xE2\x97\xBB";
    const ICON_PRESENT = "\xE2\x9C\x85";

    const ENTITY_TYPE_YACHT = 1;

	function __construct()
	{   parent::__construct();
	}

    public static function set_property($entity_id, $entity_type_id, $property_type_id, $value)
    {
        $ci = &get_instance();

        $where_params = array('entity_id' => $entity_id, 'type' => $entity_type_id, 'subtype' => $property_type_id);
        $row = Utilitar_db::_get_entity('entity_properties', $where_params);

        if ($row)
        {   $row_id = Utilitar_db::_update_entity('entity_properties', null, array('value' => $value), $where_params);
        }
        else
        {   $row_id = Utilitar_db::_create_entity('entity_properties', array_merge($where_params, array('value' => $value)));
        }
    }

    //-----------------------------------------------------------+
    // Returns set ot assoc. array of properties for specified entity of specified entity_type.
    // Uses "$properties_db_tablename" as a primary table, linking it with the "entity_properties" DB Table holding all the actual props.
    //
    // If passed "$template_properties_types" then the resultset WILL contain ALL the fields
    // from this template (with NULLS for those props with no actual data given).
    // That's useful to ensure that all the props needed exist in a final resultset (even with NULLs in it).
    public static function get_properties($properties_db_tablename, $entity_type_id, $entity_id, array $template_properties_types = null)
    {
        $ci = &get_instance();
        $ci->db->select($properties_db_tablename.'.*, entity_properties.*');
        $ci->db->from($properties_db_tablename);
        $ci->db->join('entity_properties', 'entity_properties.entity_id = '.$properties_db_tablename.'.id', 'left');
        $ci->db->where($properties_db_tablename.'.id', $entity_id);
        $ci->db->where('entity_properties.type', $entity_type_id);

        $ci->db->order_by('entity_properties.order');
        $query = $ci->db->get();
        $props = Utilitar_db::safe_resultSet($query);

        $res = array();
        if ($template_properties_types && count($template_properties_types) > 0)
        {   // returns ALL FIELDS by template, with DB FIELDS filled in:
            $props_type_keyvalue = DictionaryService::_to_key_rowvalue_array($props, 'subtype'); // compose an assoc. array of entities' properties.
            foreach ($template_properties_types as $type_id)
            {   $res[$type_id] = isset($props_type_keyvalue[$type_id]) ? $props_type_keyvalue[$type_id]->value : null;
            };
        }
        else // returns DB fields ONLY:
        {   foreach ($props as $prop)
            {   $res[$prop->subtype] = $prop->value;
            }
        }

        return $res;
    }
    //-----------------------------------------------------------|

    // marks checkboxes for props which are set from the ones expected.
    public static function props_to_checked_buttons($action_name, &$extra_actions, $entity_id, array $template_props_keyvalue, array $actual_props_keyvalue, $extra_value2 = null)
    {
        //-----------------------------------------------------------+
        // print the links to the file(s), if any:
        $ctr = 1;
        foreach ($template_props_keyvalue as $property_type => $property_name)
        {   $title = isset($actual_props_keyvalue[$property_type]) ? Util_properties::ICON_PRESENT : Util_properties::ICON_MISSING;
            $title .= ' '.($ctr++).'. '.$property_name;
            $extra_actions[$title] = TelegramHelper::wrap_button_action_name($action_name, $entity_id, $property_type, $extra_value2);
        }
        //-----------------------------------------------------------|
    }
}