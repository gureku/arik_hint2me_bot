<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH . '/controllers/admin/SecuredMatrix.php'); // greco.


// Message CRUD.
class Util_messages extends CI_Model /* see http://codeigniter.com/wiki/Inserting_Multiple_Records_Into_a_Table/ */
{
    const TYPE_EVENT = 1;   // This type of message is linked just to a single event.
    const TYPE_GROUP = 2;   // Group can have multiple events
    const TYPE_CLASS = 3;   // Class is a set of groups. We still differentiate them because list of groups in a class (which lasts 1 year)
                            // could change easily, while the messages for a Class should stay intact during groups' change.

    function __construct()
    {
        parent::__construct();

        $this->load->library(array('transliterate', 'tank_auth'));
        $this->load->helper('string');
    }

    // add a message and store its recipient entity.
    public static function add_message($msg, $recipient_type, $recipient_id, $author_tg_user_id)
    {
        $message_id = Utilitar_db::_create_entity('message_board', array('author_tg_user_id' => $author_tg_user_id, 'message_base64' => base64_encode($msg)));
        Utilitar_db::_create_entity('message_recipients', array('message_id' => $message_id, 'recipient_id' => $recipient_id, 'recipient_type' => $recipient_type));
        
        return $message_id;
    }

    public static function get_message($id)
    {   $res = Util_messages::get_all_messages(null, 'message_board.created', $id);
        return count($res) ? $res[0] : null;
    }

    // for get messages (of different types) for the specific recipient
    public static function get_messages_for($recipient_id, $entity_type)
    {
        return Util_messages::get_all_messages($entity_type, 'message_board.created DESC', null, $recipient_id);
    }

    // returns button title for Announcements (with count and localized).
    public static function get_announcements_btn_title($entity_id, $entity_type, $lang)
    {
        $count = Util_messages::count_messages_for($entity_id, $entity_type);
        $title = ('ru' == $lang) ? MessagingIA::BTN_SCHOOL_ANNOUNSEMENTS_RU : MessagingIA::BTN_SCHOOL_ANNOUNSEMENTS;

        return $title.': '.$count;
    }

    //
    //  "$message_id" if passed, then just a single message get retrieved.
    //  BEWARE: for "$recipient_ids" passing MIXED IDs for MIXED types will give WRONG RESULTS! So request either Class or Event or Group but NEVER BOTH at a time!!!
    //  TODO: add mechanism that accepts PAIRS of {$recipient_types => $recipient_IDs} so we do single query to retrieve MULTIPLE $recipient_types.
    public static function get_all_messages($recipient_types, $order_by = 'message_board.created', $message_id = null, $recipient_IDs = null)
    {
/*  SELECT reg_message_board.*, reg_message_recipients.recipient_type, reg_message_recipients.recipient_id
    FROM reg_message_board
    LEFT JOIN reg_message_recipients ON reg_message_recipients.message_id = reg_message_board.id
    WHERE reg_message_recipients.recipient_type IN (1, 2, 3)
    ORDER BY reg_message_board.created
*/
        $ci = &get_instance();
        $ci->db->select('message_board.*, message_recipients.recipient_type, message_recipients.recipient_id');
        $ci->db->from('message_board');
        $ci->db->join('message_recipients', 'message_recipients.message_id = message_board.id', 'left');

        // for specific recipient entity(-ies):
        if ($recipient_IDs)
        {   // BEWARE: for "$recipient_ids" passing MIXED IDs for MIXED types will give WRONG RESULTS! So request either Class or Event or Group but NEVER BOTH at a time!!!
            if (is_array($recipient_IDs) && count($recipient_IDs))
            {   $ci->db->where_in('message_recipients.recipient_id', $recipient_IDs);
            }
            else if (is_int($recipient_IDs))
            {   $ci->db->where('message_recipients.recipient_id', $recipient_IDs);
            }
        }

        // for specific types of entities:
        if ($recipient_types)
        {   if (is_array($recipient_types) && count($recipient_types)) // an array of types passed in
            {   $ci->db->where_in('message_recipients.recipient_type', $recipient_types);
            }
            else if (is_int($recipient_types)) // just a single value passed in
            {   $ci->db->where('message_recipients.recipient_type', $recipient_types);
            }
        }

        // single message only:
        if ($message_id > 0)
        {   $ci->db->where('message_board.id', $message_id);
        }

        $ci->db->order_by($order_by);
        $query = $ci->db->get();

        return Utilitar_db::safe_resultSet($query);
    }

    // The function is adopted from "get_all_messages()".
    // NOTE: input parameter "$recipient_types" could be a single integer or an array of integers.
    public static function count_messages_for($recipient_id, $recipient_types)
    {
/*  SELECT (reg_message_board.id) AS numrows
    FROM reg_message_board
    LEFT JOIN reg_message_recipients ON reg_message_recipients.message_id = reg_message_board.id
    WHERE reg_message_recipients.recipient_type IN (1, 2, 3)
*/
        $ci = &get_instance();
        $ci->db->select('message_board.id');
        $ci->db->from('message_board');
        $ci->db->join('message_recipients', 'message_recipients.message_id = message_board.id', 'left');

        // for specific recipient entity:
        if ($recipient_id > 0)
        {   $ci->db->where('message_recipients.recipient_id', $recipient_id);
        }

        // for specific types of entities:
        if ($recipient_types)
        {
            if (is_array($recipient_types) && count($recipient_types)) // an array of types passed in
            {   $ci->db->where_in('message_recipients.recipient_type', $recipient_types);
            }
            else if (is_int($recipient_types)) // just a single value passed in
            {   $ci->db->where('message_recipients.recipient_type', $recipient_types);
            }
        }
        
        $query = $ci->db->get();
        return $query->num_rows();
    }

    // used to present messages as buttons (for messages archive). The thing is to dissect mb-strings so that they stay valid!
    public static function get_shortened_message_rows($message_objs, $max_chars)
    {
        $res = array();
        foreach ($message_objs as $row)
        {   $tmp = Util_messages::shorten_if_longer(base64_decode($row->message_base64), $max_chars);
            $res[$row->id] = array( 'title'         => strip_tags($tmp),
                                    'recipient_id'  => $row->recipient_id,
                                    'recipient_type'=> $row->recipient_type, // see Util_messages::TYPE_EVENT, etc. for types.
                                  );
        }

        return $res;
    }

    public static function shorten_if_longer($msg, $max_chars)
    {   return (mb_strlen($msg) > $max_chars) ? mb_substr($msg, 0, $max_chars).'...' : $msg; // cut off lengthy strings (only).
    }

    public static function mb_str_strip_quotes($mb_string)
    {
        $REPLACE_BY = ' ';
        $reserved = preg_quote('\/:*?"<>', '/');
        return preg_replace("/([\\x00-\\x1f{$forbidden}])/e", $REPLACE_BY, $mb_string);
    }

    // see also https://searchcode.com/file/50857917/mb_trim.php/ for alternative version.
    public static function mb_trim($string, $charlist = null)
    {
        if (is_null($charlist)) {
            return trim($string);
        } else {
            $charlist = preg_quote($charlist, '/');
            return preg_replace("/(^[$charlist]+)|([$charlist]+$)/us", '', $string);
        }
    }

    private static function mb_rtrim($string, $charlist = null)
    {
        if (is_null($charlist)) {
            return rtrim($string);
        } else {
            $charlist = preg_quote($charlist, '/');
            return preg_replace("/([$charlist]+$)/us", '', $string);
        }
    }

    private static function mb_ltrim($string, $charlist = null)
    {
        if (is_null($charlist)) {
            return ltrim($string);
        } else {
            $charlist = preg_quote($charlist, '/');
            return preg_replace("/(^[$charlist]+)/us", '', $string);
        }
    }

    // NOTE: Parent might have messages from: events and classes.
    public static function get_parent_messages($parent_id)
    {
        $children = Util_school::get_parent_children($parent_id);
        //log_message('error', "---- PARENT CHILDREN (VIEW_PARENT_MESSAGE_BOARD): ".print_r($children, true));

        $assoc = DictionaryService::_to_key_rowvalue_array($children, 'id');
        $children_ids = array_keys($assoc);

        $classes = Util_school::get_classess_of_children($children_ids);

        $assoc = DictionaryService::_to_key_rowvalue_array($classes, 'class_id');
        $class_IDs = array_keys($assoc);
        //log_message('error', "---- CHILDREN CLASSES (VIEW_PARENT_MESSAGE_BOARD): ".print_r($class_IDs, true));

        $class_messages = Util_messages::get_all_messages(Util_messages::TYPE_CLASS, 'message_board.created DESC', null, $class_IDs);
        $events_messages= Util_messages::get_messages_for($parent_id, Util_messages::TYPE_EVENT);

        return array('events_messages' => $events_messages, 'class_messages' => $class_messages);
    }
}
