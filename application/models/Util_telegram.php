<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH.'libraries/wizardry/library/WizardFather.php');
include_once(APPPATH.'libraries/wizardry/core/WizardDAL.php');

class Util_telegram extends CI_Model /* see http://codeigniter.com/wiki/Inserting_Multiple_Records_Into_a_Table/ */
{
    const TBL__TG_USERS = 'tg_users';
    const STR_SEARCH    = "\xF0\x9F\x94\x8E Search";
    const STR_SEARCH_RU = "\xF0\x9F\x94\x8E Искать";

    const STR_PLUS = "\xE2\x9E\x95";

    private static $bot_token = null;
    private static $bot_token_crc32 = null;

    private static $loaded_strings = array();

    //
    // NOTE!!! It is VITAL for auth. routines to keep THIS ORDER of values below. It is linked to a number of SQL routines, etc.:
    const IS_LOGGED_IN       = 1;
    const IS_NOT_LOGGED_IN   = 2;

    private static $bot_name = null;


    private $CI;

	function __construct()
	{
		parent::__construct();
	}

    //
    // Returns null if string item does not exist in cofigured items.
    // Also returns null for items prohibited (private)
	public static function get_string($string_name, $lang = 'ru')
	{
	    $prohibited_sections = array('bot_token', 'bot_name');
	    if (in_array($string_name, $prohibited_sections))
	    {   log_message('error', "---- WARNING: this config item '$string_name' is prohibited to ask for!");
	        return null;
	    }

        $config_item_name = ('ru' == $lang) ? $string_name.'_ru' : $string_name;

        if (!isset(Util_telegram::$loaded_strings[$string_name]))
        {
            $ci = &get_instance();
            $ci->config->load('tg_bot_config');
            $value = $ci->config->item($config_item_name);
            if ($value)
            {   Util_telegram::$loaded_strings[$config_item_name] = $value; // remember for future use.
            }

            return $value;
        }

        return Util_telegram::$loaded_strings[$config_item_name];
	}


    public static function get_bot_welcome_message($lang = 'ru')
    {   return Util_telegram::get_string('bot_welcome_message', $lang);
    }

    public static function get_bot_welcome_details($lang = 'ru')
    {   return Util_telegram::get_string('bot_welcome_details', $lang);
    }

    public static function get_bot_FAQ_url()
    {   return Util_telegram::get_string('faq_url', 'en'); // leave EN locala, otherwise won't work! URLs have no locale-specifics (h-m, really?)
    }

    public static function get_bot_token()
    {
        if (!Util_telegram::$bot_token)
        {
            $ci = &get_instance();
            $ci->config->load('tg_bot_config');

            Util_telegram::$bot_token = $ci->config->item('bot_token');
        }

        return Util_telegram::$bot_token;
    }

    public static function get_bot_id_crc32()
    {
        if (!Util_telegram::$bot_token_crc32)
        {   Util_telegram::$bot_token_crc32 = WizardDAL::calc_crc32(Util_telegram::get_bot_token());
        }

        return Util_telegram::$bot_token_crc32;
    }

    public static function get_bot_name()
    {
        if (!Util_telegram::$bot_name)
        {
            $ci = &get_instance();
            $ci->config->load('tg_bot_config');
            Util_telegram::$bot_name = $ci->config->item('bot_name');
        }

        return Util_telegram::$bot_name;
    }

    //-----------------------------------------------------------+
    // Tickets allow to use TG functionality with this or that role (admin, visitor, trainer).
    public function get_tg_ticket_by_messaging_id($messaging_id)
    {
        $ci = &get_instance();
        $ci->db->where('messaging_id', $messaging_id);
        $ci->db->where('bot_name', Util_telegram::get_bot_name());
        $query = $ci->db->get('tg_tickets');
        return Utilitar_db::safe_resultRow($query);
    }

    // get_messaging_id_by_ownername. TODO: refactor for other criteria to use for the search!!!
    public static function get_messaging_id_by_ownername($owner_name)
    {
        $ci = &get_instance();
        $ci->db->where(array(   'ticket_owner_name' => $owner_name,
                                // 'organization_id' => $organization_id,
                         )
                      );
        $query = $ci->db->get('tg_tickets');
        $row = Utilitar_db::safe_resultRow($query);

        return ($row ? $row->messaging_id : NULL);
    }

    public static function get_tg_tickets($role = null)
    {
        $ci = &get_instance();
        if ($role)
        {   $ci->db->where('role', $role);
        }

        $ci->db->where('bot_name', Util_telegram::get_bot_name()); // get tickets of specific bot only!

        $ci->db->order_by('id');
        $query = $ci->db->get('tg_tickets');
        return Utilitar_db::safe_resultSet($query);
    }

    public function get_tg_ticket_by_id($id)
    {
        $ci = &get_instance();
        $ci->db->where('id', $id);
        $ci->db->where('bot_name', Util_telegram::get_bot_name()); // get tickets of specific bot only!

        $query = $ci->db->get('tg_tickets');
        return Utilitar_db::safe_resultRow($query);
    }

    // WARNING: this method ASSUMES that extra_data is UNIQUE!!!! However we have also yurlico_id and orgnization_id, and other criteria, too! BEWARE!
    public static function get_tg_ticket_by_extra_data($extra_data, $role)
    {   return Utilitar_db::_get_entity('tg_tickets', array('extra_data' => intval($extra_data), 'role' => $role, 'bot_name' => Util_telegram::get_bot_name()));
    }

    // search users of certain role by their extra_data (i.e. ids) passed in
    public static function get_tg_users_by_extra_data(array $IDs, $role)
    {
        if (0 == count($IDs)) // safety check #1
        {   return array();
        }

        $ci = &get_instance();
        $ci->db->select('id'); // this is a "ticket_id"
        $ci->db->where('role', $role);
        $ci->db->where('bot_name', Util_telegram::get_bot_name());
        $ci->db->where_in('extra_data', $IDs);
        $query = $ci->db->get('tg_tickets');
        $tickets = Utilitar_db::safe_resultSet($query);

        if (0 == count($tickets)) // safety check #2
        {   return array();
        }

        $assoc = DictionaryService::_to_key_rowvalue_array($tickets, 'id');

        $ci->db->where('role', $role);
        $ci->db->where_in('ticket_id', $IDs);
        $query = $ci->db->get('tg_users');

        return Utilitar_db::safe_resultSet($query);
    }

    public static function generate_user_invite_link($secure_code)
    {   return Util_telegram::generate_tg_invite_link('invid', $secure_code);
    }

    // a universal method for generating invite links by prefix. Ensure to provide prefix!
    public static function generate_tg_invite_link($prefix, $secure_code)
    {
        if (0 == strlen($prefix))
        {   return 'the link is composed incorrectly';
        }

        $res = 'https://t.me/'.Util_telegram::get_bot_name();
        $res.= '?start='.$prefix.$secure_code;

        return $res;
    }

    // this method is to protect from creating DUPLICATE TICKETS for the SAME "$extra_data_int"
    public function delete_tg_ticket($organization_id, $role_id, $extra_data_int)
    {
        $this->db->where(array('organization_id' => $organization_id, 'role' => $role_id, 'extra_data' => $extra_data_int));
        $this->db->delete('tg_tickets');
    }

    // this method is to protect from creating DUPLICATE TICKETS for the SAME "$extra_data_int"
    public function delete_tg_ticket_by_id($ticket_id)
    {
        $this->db->where('id', $ticket_id);
        $this->db->where('bot_name', Util_telegram::get_bot_name()); // get tickets of specific bot only!
        $this->db->delete('tg_tickets');
    }

    // NOTE: MULTIPLE user records are possible for both Guest and User roles!
    public static function get_tg_users_by_tg_user_id($tg_user_id)
    {
        $ci = &get_instance();
        $ci->db->where('bot_name', Util_telegram::get_bot_name()); // get tickets of specific bot only!
        $ci->db->where('tg_user_id', $tg_user_id);

        $query = $ci->db->get('tg_users');
        return Utilitar_db::safe_resultSet($query);
    }


    public function get_tg_ticket($secure_code)
    {
        $ci = &get_instance();
        $ci->db->where(array(   'secure_code'      => $secure_code,
                                // 'organization_id' => $organization_id,
                                'bot_name' => Util_telegram::get_bot_name(),
                         )
                      );
        $query = $ci->db->get('tg_tickets');
        return Utilitar_db::safe_resultRow($query);
    }

    // returnes newly-generated secure code (thus invalidating the previous one).
    // QUESTION: shall I do also the "logout" procedure?? H-m...
    public function invalidate_tg_ticket_secure_code($original_secure_code)
    {
        $ci = &get_instance();

        $new_secure_code = Util_telegram::create_random_no_zeros('alnum', 12);
        $ci->db->where(array('secure_code' => $original_secure_code, 'bot_name' => Util_telegram::get_bot_name()));

        $ci->db->update('tg_tickets', array('secure_code' => $new_secure_code));

        return $new_secure_code;
    }

    // returns object of row just created or null if failed.
    public function create_void_tg_ticket($secure_code, $role, $human_name, $yurlico_id = null, $organization_id = null, $extra_data = null)
    {
        $ci = &get_instance();
        $data = array(
                'bot_name'          => Util_telegram::get_bot_name(),
                'secure_code'       => $secure_code,
                'role'              => $role,
                'messaging_id'      => random_string('alnum', 8),
                'ticket_owner_name'  => $human_name,
                'local_userid'      => NULL,
                'yurlico_id'        => $yurlico_id,
                'organization_id'   => $organization_id,
                'extra_data'        => $extra_data, // helps tracking what for that particular ticket has been created. Or manage data integrity.
        );

        $query = $ci->db->insert('tg_tickets', $data);

        return $ci->utilitar_db->get_last_inserted_row('tg_tickets');
    }
    //-----------------------------------------------------------|

    // useful to retrieve tg_user based on ticket information.
    public static function get_bot_admins($bot_name)
    {
        $ci = &get_instance();
        return $ci->utilitar_db->_getTableContents(Util_telegram::TBL__TG_USERS, 'tg_user_id', 'bot_name', $bot_name, 'role', WizardHelper::USER_ROLE__CLUB_ADMIN);
    }

    // useful to retrieve tg_user based on ticket information.
    public static function get_tg_user_by_messaging_id($messaging_id)
    {
        $ci = &get_instance();
        return $ci->utilitar_db->_getTableRow(Util_telegram::TBL__TG_USERS, 'messaging_id', $messaging_id);
    }

    public static function set_WT_club_for_user($tg_user_id, $yurlico_id, $organization_id, $ticket_id)
    {
        //log_message('error', "XXX set_WT_club_for_user(tg_user_id:$tg_user_id, y:$yurlico_id, org:$organization_id, ticket_id:$ticket_id)");

        $ci = &get_instance();
        $ci->db->where('tg_user_id', $tg_user_id);
        $ci->db->where('bot_name', Util_telegram::get_bot_name());

        $ci->db->where('ticket_id', $ticket_id); // this is a must! Because user could be guest to one club and member in another!

        $ci->db->update(Util_telegram::TBL__TG_USERS, array('yurlico_id' => $yurlico_id, 'organization_id' => $organization_id,));
    }

    public static function set_tg_user_language($lang_code, $tg_user_id)
    {
        $ci = &get_instance();
        $ci->db->where(array('tg_user_id' => $tg_user_id, 'bot_name' => Util_telegram::get_bot_name()));
        $ci->db->update(Util_telegram::TBL__TG_USERS, array('lang' => $lang_code));
    }

    // NOTE: alternatively user language code is always available via "WizardHelper::get_current_user()->lang"
    public static function get_tg_user_language($tg_user_id)
    {
        $ci = &get_instance();
        $ci->db->where(array('tg_user_id' => $tg_user_id, 'bot_name' => Util_telegram::get_bot_name()));
        $query = $ci->db->get(Util_telegram::TBL__TG_USERS);
        $row = Utilitar_db::safe_resultRow($query);

        $lang_code = $row ? ($row->lang ? $row->lang : 'ru') : 'ru'; // Russian as default langugage
        return $lang_code;
    }

    //-----------------------------------------------------------+
    //   TG user account management:
    // this first tries to get LOGGED_IN user, then , if failed - LOGGED_OUT one (only!). It DOES NOT retrieve users with logged_in == NULL/0 values!
    //
    // returns either the very first or the very last user record.
    public static function get_tg_user_authorized_first($tg_user_id)
    {
        if ($tg_user_id <= 0)
        {   return NULL;
        }

        $rows = Util_telegram::get_tg_user__all_rows($tg_user_id);

        // First try to get logged-in user record for the $tg_user_id: that's why we use ORDER BY:
        $alternative_row = NULL;
        foreach ($rows as $row) // returns either the very first authorized one or the last existing (and non-authorised, obviously)
        {   if (Util_telegram::IS_LOGGED_IN == $row->logged_in)
            {   //log_message('error', "PPP get_tg_user_authorized_first($tg_user_id): found LOGGED IN user=".print_r($row, true));
                return $row;
            }

            $alternative_row = $row;
        }

        if ($alternative_row)
        {   return $alternative_row;
        }

        // create a guest if nothing found:
        $ci = &get_instance();
        $res = $ci->util_telegram->do_tg_login($tg_user_id, NULL);
        if (!$res)
        {   return NULL;
        }

        $rows = Util_telegram::get_tg_user__all_rows($tg_user_id);
        return reset($rows); // get the very first (and only) user row.
    }

    // N.B. Any tg_user may have 2 (or more??) rows (ordered by role!) with different ROLES!
    // TODO: optimie by adding filter conditions as input parameters!!!
    private static function get_tg_user__all_rows($tg_user_id)
    {
        $ci = &get_instance();
        $ci->db->select('tg_users.*, tg_tickets.extra_data');
        $ci->db->join('tg_tickets', 'tg_tickets.id = tg_users.ticket_id', 'left');

        $ci->db->where(array('tg_users.tg_user_id' => $tg_user_id, 'tg_users.bot_name' => Util_telegram::get_bot_name(),
                                // 'organization_id' => $organization_id,
                         )
                      );
        $ci->db->order_by('tg_users.logged_in ASC'); // the order is extremely important!
        $query = $ci->db->get('tg_users');

        return Utilitar_db::safe_resultSet($query);
    }

    public static function get_tg_user_by_ticket_id($ticket_id)
    {
        return Utilitar_db::_get_entity('tg_users', array('ticket_id' => $ticket_id));
    }


    public static function get_tg_users_by_tickets(array $ticket_ids)
    {
        if (0 == count($ticket_ids))
        {   return array();
        }

        $ci = &get_instance();
        $ci->db->where_in('ticket_id', $ticket_ids);
        $ci->db->where('bot_name', Util_telegram::get_bot_name());

        $ci->db->order_by('ticket_owner_name, logged_in ASC'); // authorised comes first, then non-authorized one.

        $query = $ci->db->get(Util_telegram::TBL__TG_USERS);
        return Utilitar_db::safe_resultSet($query);
    }

    public static function get_tg_users_by_role($role)
    {
        $ci = &get_instance();
        $ci->db->where('role', $role);
        $ci->db->order_by('ticket_owner_name');

        $query = $ci->db->get('tg_users');
        return Utilitar_db::safe_resultSet($query);
    }

    private static function get_tg_user($tg_user_id, $logged_in = NULL)
    {
        $ci = &get_instance();
        $ci->db->where(array(   'tg_user_id'    => $tg_user_id,
                                'bot_name'      => Util_telegram::get_bot_name(),
                                // 'organization_id' => $organization_id,
                         )
                      );

        if (NULL !== $logged_in)
        {   $ci->db->where('logged_in', $logged_in);
        }

        $ci->db->order_by('logged_in ASC'); // authorised comes first, then non-authorized one.

        $query = $ci->db->get(Util_telegram::TBL__TG_USERS);
        return Utilitar_db::safe_resultRow($query);
    }

    // NOTE: Guests are NOT considered as "authorized"!
    public static function is_authorized(&$userobj)
    {
//    log_message('error', "QQQ: is_authorized(): ".print_r($userobj, true));
        return ($userobj && (Util_telegram::IS_LOGGED_IN == $userobj->logged_in) && (WizardHelper::USER_ROLE__GUEST != $userobj->role));
    }

    public static function is_teacher(&$userobj, $b_check_if_is_logged_in)
    {
        if (!Util_telegram::is_eligible($userobj, $b_check_if_is_logged_in))
        {   return false;
        }

        return (WizardHelper::USER_ROLE__TRAINER == $userobj->role);
    }

    public static function is_guest(&$userobj, $b_check_if_is_logged_in)
    {
        if (!Util_telegram::is_eligible($userobj, $b_check_if_is_logged_in))
        {   return false;
        }

        return (WizardHelper::USER_ROLE__GUEST == $userobj->role);
    }

    public static function is_independent_partner(&$userobj, $b_check_if_is_logged_in)
    {
        if (!Util_telegram::is_eligible($userobj, $b_check_if_is_logged_in))
        {   return false;
        }

        return (WizardHelper::USER_ROLE__PARTNER == $userobj->role);
    }

    public static function is_club_owner(&$userobj, $b_check_if_is_logged_in)
    {
        if (!Util_telegram::is_eligible($userobj, $b_check_if_is_logged_in))
        {   return false;
        }

        return (WizardHelper::USER_ROLE__CLUB_OWNER == $userobj->role);
    }

    public static function is_client(&$userobj, $b_check_if_is_logged_in)
    {
        if (!Util_telegram::is_eligible($userobj, $b_check_if_is_logged_in))
        {   return false;
        }

        return (WizardHelper::USER_ROLE__CLIENT == $userobj->role);
    }

    public static function is_club_admin(&$userobj, $b_check_if_is_logged_in)
    {
        if (!Util_telegram::is_eligible($userobj, $b_check_if_is_logged_in))
        {   return false;
        }

        return (WizardHelper::USER_ROLE__CLUB_ADMIN == $userobj->role);
    }

    // does some sanity check, to avoid copy-paste.
    private static function is_eligible(&$userobj, $b_check_if_is_logged_in)
    {
        if (!$userobj)
        {   return false;
        }

        if ($b_check_if_is_logged_in && (Util_telegram::IS_LOGGED_IN != $userobj->logged_in))
        {   return false;
        }

        return true;
    }

    // add tg_user with related ticket's data.
    public function add__tg_user($tg_user_id, &$tg_ticket_obj)
    {
        $ci = &get_instance();

        // log_message('error', "add__tg_user($tg_user_id, local_userid:".$tg_ticket_obj->local_userid."): INSERT, fatherbot=".Util_telegram::get_bot_name());
        $data = array(  'tg_user_id'        => $tg_user_id,
                        'bot_name'          => Util_telegram::get_bot_name(),
                        'logged_in'         => Util_telegram::IS_LOGGED_IN,

                        // ticket data:
                        'messaging_id'      => $tg_ticket_obj->messaging_id,
                        'ticket_id'         => $tg_ticket_obj->id,
                        'ticket_owner_name' => $tg_ticket_obj->ticket_owner_name,
                        'role'              => $tg_ticket_obj->role,

                        'yurlico_id'        => $tg_ticket_obj->yurlico_id,
                        'organization_id'   => $tg_ticket_obj->organization_id,
                        'local_userid'      => $tg_ticket_obj->local_userid,
                     );

        //log_message('error', "add__tg_user($tg_user_id, local_userid:".$tg_ticket_obj->local_userid."): INSERT, fatherbot=".Util_telegram::get_bot_name().", data:\n".print_r($data, true));
        $ci->db->insert(Util_telegram::TBL__TG_USERS, $data);
    }

    // Returns ANY tg_user row but Guest (for the passed in $tg_user_id):
    private function get_nonGuest($tg_user_id)
    {
        $ci = &get_instance();
        $ci->db->where(array(   'tg_user_id'=> $tg_user_id,
                                'bot_name'  => Util_telegram::get_bot_name(),
                                // 'organization_id' => $organization_id,
                         )
                      );
        $ci->db->where('role <> '.WizardHelper::USER_ROLE__GUEST);

        $query = $ci->db->get(Util_telegram::TBL__TG_USERS);
        return Utilitar_db::safe_resultRow($query);
    }

    public static function create_random_no_zeros($type, $length)
    {
        $secure_code= random_string($type, $length);
        return str_replace(array('0', 'o'), 'd', $secure_code);
    }

    // returns true/false. To be called from WT TG exposed API.
    // If on input passed ($secure_code == NULL) then ASSIGNS GUEST ticket to that TG_USER!
    public function do_tg_login($tg_user_id, $secure_code)
    {
        $tg_ticket_obj = NULL; // initialize.

        if (!$secure_code)
        {
            $secure_code= Util_telegram::create_random_no_zeros('alnum', 8);
            log_message('error', "XXX do_tg_login(): NO TICKET found for code '$secure_code'. Creating a GUEST ticket!");
            $input_data = WizardHelper::get_tg_input_data();
            $alt_name = '';
            if (isset($input_data['message']) && isset($input_data['message']['from']) && isset($input_data['message']['from']['username']))
            {   $alt_name = ' (@'.$input_data['message']['from']['username'].')';
            }

            $tg_ticket_obj  = $this->create_void_tg_ticket($secure_code, WizardHelper::USER_ROLE__GUEST, 'Юзер'.$alt_name);
        }
        else
        {   $tg_ticket_obj  = $this->get_tg_ticket($secure_code);
        }

        if (!$tg_ticket_obj)
        {   log_message('error', "XXX do_tg_login(): ERROR retrieving tg ticket for the code '$secure_code'.");
            return false;
        }

        log_message('error', "XXX do_tg_login(): ticket found for tg_user_id=$tg_user_id: ".print_r($tg_ticket_obj, true));

        // 1. firstly unauthorise all existing tg_user_id entries (including guests):
        $this->do_tg_logout($tg_user_id, false);

        $nonGuest_user = $this->get_nonGuest($tg_user_id);
        log_message('error', "XXX do_tg_login() nonGuest_user: ".print_r($nonGuest_user, true));
        if ($nonGuest_user)
        {   // then update it by ticket details (for non-Guest users only!):
            $ci = &get_instance();
            $ci->db->where('tg_user_id', $tg_user_id);
            $ci->db->where('bot_name', Util_telegram::get_bot_name());
            $ci->db->where('role <> '.WizardHelper::USER_ROLE__GUEST); // ensure to not to modify Guest record for this $tg_user_id.

            $updatedata = array('messaging_id'      => $tg_ticket_obj->messaging_id,
                                'ticket_id'         => $tg_ticket_obj->id,
                                'ticket_owner_name' => $tg_ticket_obj->ticket_owner_name,
                                'role'              => $tg_ticket_obj->role,

                                'yurlico_id'        => $tg_ticket_obj->yurlico_id,
                                'organization_id'   => $tg_ticket_obj->organization_id,
                                'local_userid'      => $tg_ticket_obj->local_userid,

                                'logged_in'         => Util_telegram::IS_LOGGED_IN,
                               );
            $ci->db->update(Util_telegram::TBL__TG_USERS, $updatedata);
            //log_message('error', "XXX do_tg_login(): UPDATED the existing non-guest (tguid:$tg_user_id) instance(s) of a class by data:\n".print_r($updatedata, true));
            log_message('error', "XXX do_tg_login(): UPDATED the existing non-guest (tguid:$tg_user_id) instance(s).");
        }
        else
        {   log_message('error', "XXX do_tg_login(): ADDING new user with role_id=".$tg_ticket_obj->role);
            $this->add__tg_user($tg_user_id, $tg_ticket_obj); // create the VERY FIRST instance of a class, since none exists.
        }

        return true;
    }

    // WARNING! this method still can leave GUEST as logged in user. Dunno whether this is good approach, I'd still expect even GUESTS be not logged in...
    public function do_tg_logout($tg_user_id, $assign_guest_logged_in = true)
    {
        log_message('error', "xxx doing TG LOGOUT: tg_user_id=$tg_user_id");
        if ($tg_user_id <= 0)
        {   return false;
        }

        $ci = &get_instance();
        $ci->db->where('tg_user_id', $tg_user_id);
        $ci->db->where('bot_name', Util_telegram::get_bot_name());

        $ci->db->update(Util_telegram::TBL__TG_USERS, array('logged_in' => Util_telegram::IS_NOT_LOGGED_IN)
                       );

       if ($assign_guest_logged_in)
       {    // Usually (!) the Guest needs to be logged in, when non-guest gets logged out.
            $ci->db->where('tg_user_id', $tg_user_id);
            $ci->db->where('bot_name', Util_telegram::get_bot_name());
            $ci->db->where('role', WizardHelper::USER_ROLE__GUEST);

            $ci->db->update(Util_telegram::TBL__TG_USERS, array('logged_in' => Util_telegram::IS_LOGGED_IN)
                           );
       }

       return true;
    }
    //-----------------------------------------------------------|

    //-----------------------------------------------------------+
    //  Messaging functions:
    public static function msg_send($from_messaging_id, $to_messaging_id, $message, $action_type = WizardMessages::ACTION__GENERIC)
    {
        if (!$message || 0 == mb_strlen($message))
        {   return;
        }

        log_message('error', "msg_send(from messaging_id:$from_messaging_id, to messaging_id:$to_messaging_id, '$message')");

        $ci = &get_instance();
        $data_array = array('bot_id_crc32'      => WizardHelper::get_bot_id_crc32(),
                            'from__messaging_id'=> $from_messaging_id,
                            'to__messaging_id'  => $to_messaging_id,
                            'body'              => $message,
                            'type'              => $action_type,
                            );

        $ci->db->insert('tg_messages', $data_array);
    }

    // NOTE: Accepts NULL for $to_messaging_id. In that case retrieves ANY message where "$from_messaging_id" participates.
    public static function msg_get_messages($from_messaging_id, $to_messaging_id = NULL)
    {
/*  SELECT wt_tg_messages.from__messaging_id, wt_tg_messages.body, wt_tg_messages.type, wt_tg_messages.date_delivered
    FROM wt_tg_messages
    WHERE
        wt_tg_messages.`bot_id_crc32` = '355550590'
    AND
    (
        ( wt_tg_messages.from__messaging_id = '777' AND wt_tg_messages.to__messaging_id = '11' ) OR
        ( wt_tg_messages.from__messaging_id = '11' AND wt_tg_messages.to__messaging_id = '777')
    )
*/
        log_message('error', "msg_get_messages(from:$from_messaging_id, to:$to_messaging_id)");
        // get all the messages between two messaging_id-s:
        $ci = &get_instance();
        $db_prefix = $ci->db->dbprefix;

        $ci->db->select('id, from__messaging_id, to__messaging_id, body, type, date_delivered');
        $ci->db->where('bot_id_crc32', WizardHelper::get_bot_id_crc32());
        if ($from_messaging_id && $to_messaging_id)
        {   $ci->db->where("( ( ".$db_prefix."tg_messages.from__messaging_id = '$from_messaging_id' AND ".$db_prefix."tg_messages.to__messaging_id = '$to_messaging_id' ) OR ( ".$db_prefix."tg_messages.from__messaging_id = '$to_messaging_id' AND ".$db_prefix."tg_messages.to__messaging_id = '$from_messaging_id') )");
        }
        else if ($from_messaging_id)
        {   $ci->db->where("( ".$db_prefix."tg_messages.from__messaging_id = '$from_messaging_id' OR ".$db_prefix."tg_messages.to__messaging_id = '$from_messaging_id' )");

        }
        $ci->db->order_by('date_delivered ASC');

        $query = $ci->db->get('tg_messages');

        return Utilitar_db::safe_resultSet($query);
    }

    // saves into two tables: tg_users and tz_daylightsaving.
    public static function set_tg_user_timezone_and_dst($tg_user_id, &$compound)
    {
        // 1. get the timezone DB row by timezone name
        // 2. set a DST for a timezone from timezonedb.com API call result.
        // 3. assign timezone_id to user

        $ci = &get_instance();
        $ci->load->model('utilitar');
        $ci->load->model('utilitar_date');

        $country_name = $compound['countryName'];
        $timezone_name = $compound['zoneName'];

        // 1. get the timezone DB row by tiezone name
        $tz_row = $ci->utilitar_date->get_timezone_row_by_timezone_name($timezone_name);
        if (!$tz_row)
        {   log_message('error', "---- ERROR: no TZ record found for '$timezone_name' passed in! Strange!");
            return;
        }

//      Compound contents:
//            "countryCode":"AM",
//            "countryName":"Armenia",
//            "zoneName":"Asia/Yerevan",
//            "abbreviation":"AMT",
//            "gmtOffset":14400,
//            "dst":"0",
//            "zoneStart":1319925600,
//            "zoneEnd":null,
//            "nextAbbreviation":null,
//            "timestamp":1589514815,

        // convert to MySQL timestamp format:
        $zone_date_start= $compound['zoneStart'] ? date('Y-m-d H:i:s', $compound['zoneStart']) : null;
        $zone_date_end  = $compound['zoneEnd']   ? date('Y-m-d H:i:s', $compound['zoneEnd'])   : null;

        // 2. set a DST for a timezone from timezonedb.com API call result.
        $ci->utilitar_date->upsert_timezone_dst((null !== $tz_row->dst), $tz_row->zone_id, $compound['gmtOffset'], $compound['dst'], $zone_date_start, $zone_date_end);

        // 3. assign timezone_id to user
        $ci->db->where(array('tg_user_id' => $tg_user_id, 'bot_name' => Util_telegram::get_bot_name()));
        $ci->db->update(Util_telegram::TBL__TG_USERS, array('timezone_id' => $tz_row->zone_id));

        log_message('error', "---- called set_tg_user_timezone($tg_user_id): ".print_r($compound, true));
    }

    // Counts by "messaging_id".
    public static function count_messages($messaging_id)
    {
/*  SELECT COUNT(wt_tg_messages.id) AS messages_count
    FROM wt_tg_messages
    WHERE
        wt_tg_messages.`bot_id_crc32` = '355550590'
    AND
    (
        wt_tg_messages.from__messaging_id = '11' OR wt_tg_messages.to__messaging_id = '11'
    )
*/
        // get all the messages between two secret_codes:
        $ci = &get_instance();
        $db_prefix = $ci->db->dbprefix;

        $ci->db->select('COUNT('.$db_prefix.'tg_messages.id) AS messages_count');
        $ci->db->where('bot_id_crc32', WizardHelper::get_bot_id_crc32());
        $ci->db->where("( ".$db_prefix."tg_messages.from__messaging_id = '$messaging_id' OR ".$db_prefix."tg_messages.to__messaging_id = '$messaging_id' )");

        $query = $ci->db->get('tg_messages');

        $row = Utilitar_db::safe_resultRow($query);
        return ($row ? $row->messages_count : 0);
    }
    //-----------------------------------------------------------|

    //-----------------------------------------------------------+
    // TG User Ticket Profile methods
    public static function get_tg_ticket_profile($tg_ticket_id)
    {
        $ci = &get_instance();
        $ci->load->model('utilitar_db');
        return $ci->utilitar_db->_getTableRow('tg_users_profiles', 'tg_ticket_id', $tg_ticket_id);
    }

    public static function create_user_profile($tg_ticket_id)
    {
        $ci = &get_instance();
        $ci->load->model('utilitar_db');

        $ci->db->insert('tg_users_profiles', array('tg_ticket_id' => $tg_ticket_id));
        return $ci->utilitar_db->get_last_inserted_id();
    }

    // updates TG Ticket Profile with data supplied. E.g. 'web_conference_link', 'contract_serial_number' and other.
    public static function set_user_profile($tg_ticket_id, array $params)
    {
    log_message('error', "--- setting user profile with data: ".print_r($params, true));
        $ci = &get_instance();

        $profile = Util_telegram::get_tg_ticket_profile($tg_ticket_id);
        if (!$profile)
        {   Util_telegram::create_user_profile($tg_ticket_id);
        }

        $ci->db->where('tg_ticket_id', $tg_ticket_id);
        $ci->db->update('tg_users_profiles', $params);
    }
    //-----------------------------------------------------------|

    public static function get_option($tg_user_id, $option_name, $default_value = null)
    {
        $ci = &get_instance();
        return $ci->utilitar_db->getOption(Util_telegram::_compose_option_name($tg_user_id, $option_name), $default_value);
    }

    public static function set_option($tg_user_id, $option_name, $value)
    {
        $ci = &get_instance();
        return $ci->utilitar_db->setOption(Util_telegram::_compose_option_name($tg_user_id, $option_name), $value);
    }

    // creates user-specific (and bot-specific) option name.
    private static function _compose_option_name($tg_user_id, $option_name)
    {
        return WizardHelper::get_bot_id_crc32().$tg_user_id.'__'.$option_name;
    }

    // WARNING: returns null, true or false. Null is when there is NO such link exists.
    // TRUE if the link is already expired. Use "false === is_link_expired()" to check if the link is valid.
    public static function is_link_expired($link_object)
    {
        log_message('error', "---- WARNING: is_link_expired(uid:'$link_object->title', '$link_object->uid') is NYI!!!");
        return false;
    }

    // NOTE: the expiration time is relative to now().
    public static function get_expiring_link($link_uid)
    {
        $ci = &get_instance();
        return $ci->utilitar_db->_getTableRow('tg_expirable_links', 'uid', $link_uid);
    }

    // NOTE: the expiration time is relative to now().
    // TODO: this should require TG-confirmation to open access to this!!
    public static function create_expiring_link($link_title, $link_handler_controller_name, $expire_after_N_minutes)
    {
        $ci = &get_instance();

        $link_uid = random_string('alnum', 24);
        $now = date('Y-m-d H:i:s');
        $expires_after_datetime = $ci->utilitar_date->getDateIncremented($now, $expire_after_N_minutes.' minute');

        $data = array('title' => $link_title, 'uid' => $link_uid, 'expires_after' => $expires_after_datetime);

        $query = $ci->db->insert('tg_expirable_links', $data);

        $link_uri  = base_url($link_handler_controller_name.'/'.$link_uid, 'https');

        return array('link' => $link_uri, 'expires_after' => $expires_after_datetime);
    }

    public static function extract_tg_nickname(&$tg_user)
    {
        $separator_start = '(@';
        $pos = strpos($tg_user->ticket_owner_name, $separator_start);

        // if nickname not found then we give it another try to use direct messaging using Telegram UserId:
        if (false === $pos)
        {   return 'tg://user?id='.$tg_user->tg_user_id;
        }

        $stripped = substr($tg_user->ticket_owner_name, $pos + strlen($separator_start));
        $pos_ending = strpos($stripped, ')'); // assuming this is ok

        return 'https://t.me/'.substr($stripped, 0, $pos_ending);
    }
}
