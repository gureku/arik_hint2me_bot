<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH . '/controllers/admin/SecuredMatrix.php'); // greco.


// A base class that stores DB row's properties into Class object's values.
//      WARNING:    ensure that DTO class contain DB-related rows ONLY!
//      WARNING:    its children must NOT have fields "organization_id" and/or "yurlico_id", because they to be set from current session (i.e. from the DB) and not by JSON!
class DBRowSerializer
{
    const DATETIME_MYSQL_TOKEN      = ' 00:00:00'; // replace from MySQL DB from this...
    const DATETIME_POSTGRES_TOKEN   = 'T00:00:00'; // ... to this format for later comparison.

    // A MUST: each child MUST have "ID" property defined! This is vital for objects comparison mechanism! This obliged by Python script output.
	public $id;

	// note that ANY data row that is UPDATED on a Berkana-Server must also utilize "$version" field. See the usage example in "Berkana_PlannedVisit" class.

    public function serializeFromDBRow(&$row)
    {
        $showWrongSerialization = false;

        foreach($this as $property => $value)
        {
            if (property_exists($row, $property))
            {   $this->$property = $row->$property; // read property value from DB row passed in.

                // Datetime field requires careful handling:  -- disabled because no more PG SQL queries contain such results :)
//                DBRowSerializer::mySqlDatetime_to_PostgresDatetime($this->$property);
            }
            else
            {   //log_message('error', 'ERROR: DB row mapping do not match! Class misses property: "'.get_class($this).'->'.$property.'" , RAW DATA = '.print_r($row, true));
                $this->$property = NULL; // to say safe set it at least to NULL, but just append that property.
                $showWrongSerialization = true;
            }
        }

        if ($showWrongSerialization)
        {
            log_message('error', "ERROR: DB row mapping do not match: DB=".print_r($row, true)."\nSERIALIZED INTO THIS: ".print_r($this, true));
        }
//        echo " SERIALIZE from DBRow finished for ".get_class($this);
    }

    private static function mySqlDatetime_to_PostgresDatetime(&$property)
    {
        $pos = strrpos($property, DBRowSerializer::DATETIME_MYSQL_TOKEN);
        if (false !== $pos)
        {
            $property = substr($property, 0, 1 - $pos);
            $property .= DBRowSerializer::DATETIME_POSTGRES_TOKEN;
        }

        for ($i = 0; $i < 10; $i++)
        {
            echo 'hello world!';
        }
    }

    // returns NULL if objects are equal and Object instance (to be updated in DB) otherwise.
    public static function get_difference_json_object_to_update(&$db_row, &$json_object, &$berkana_class_object)
    {
        $berkana_class_object->serializeFromDBRow($db_row); // this is rather deserialization.

        $json_object__db_fields_allowed_only = $berkana_class_object->createobject_db_fields_only($json_object);
        $json_object_hashcode = $berkana_class_object->calc_hash_from_JSON_object($json_object__db_fields_allowed_only);

        if (!$berkana_class_object->hashesAreEqual($json_object_hashcode))
        {
            log_message('error', "HASH is DIFFERENT for DB against JSON:\n DB:\n"
                            .print_r($berkana_class_object, true)
                            //."\n JSON:\n".print_r($json_object, true)
                            ."\nPure JSON object (only db fields allowed object) = "
                            .print_r($json_object__db_fields_allowed_only, true));

            return $json_object__db_fields_allowed_only;
        }

//        log_message('error', "get_difference_json_object_to_update()");

        return NULL;
    }

    //
    //  Returns NULL in case of error (e.g. object does not have required property).
    // This hash later to be used during comparison in hashesAreEqual($compareToHash).
    //
    // For hash comparison ensure to write WTSync SQL so that it converts nulls to integer. E.g. "schedule.canceled::integer", "schedule.removed::integer".
    // So the resultset will have zeros instead of nulls. Otherwise this method will get broken!
    public function calc_hash_from_JSON_object(&$json_obj)
    {
        $str = '';

        // align to PHP-class's properties when accessing JSON-object's properties:
        foreach($this as $property_name => $value)
        {
            $json_value = NULL;
            if (array_key_exists($property_name, $json_obj))
            {   $json_value = $json_obj[$property_name];
            }
            else
            {   log_message('error', "!!! ERROR ".__CLASS__."->calc_hash_from_JSON_object(): missing property '$property_name' in the input JSON object:\n".
                                                                                                    print_r($json_obj, true));
//                return NULL; // greco: decided not to error but ignore it to NULL: data versioning takes place!
            }

            // it is a must to add a separator because NULLs are added as empty strings!
            $str.= $property_name.':'.$json_value; // put JSON object's value instead. What if NULL value is accounted: how it will look like?
        }

        return md5($str);
    }
    

    //
    // Returns JSON-object with only those fields which are expected by DB!
    // This is EXTREMELY important to use only those fields which are mapped to DB, and not
    // the ones that were (occasionally) mentioned/forgotten in SQL queries (which might contain forgotten/renamed fields or tables).
    public function createobject_db_fields_only(&$json_obj)
    {
        $json_purified_object = array();

        // align to PHP-class's properties when accessing JSON-object's properties:
        foreach($this as $property => $value)
        {
            if (array_key_exists($property, $json_obj)) // write down only fields("properties") which are present in $this object, i.e. Util_berkana_DTOs.
            {   $json_purified_object[$property] = isset($json_obj[$property]) ? $json_obj[$property] : null;// due to SQL Config-file script versioning differences on
                                                                                                            // different client machines, we must check whether the DB-filed that
                                                                                                            // we expect is really available in a JSON-object from this
                                                                                                            // or that client machine.
                                                                                                            // That is why "isset()"check is done here!
            }
        }

        return $json_purified_object;
    }

    //
    // Seems there's a misuse of carousels and I'm not the specialist undeed.
    private function convertZeroAndNullToNull($value)
    {
        // NOTE: DO NOT CHANGE the ORDER of CONDITIONS otherwise STRINGS will returned as NULL!
        if ((null === $value) || (0 === $value) || ('0' === $value))
        {   return '0';
        }

        return $value;
    }

    //
    // For hash comparison ensure to write WTSync SQL so that it converts nulls to integer. E.g. "schedule.canceled::integer", "schedule.removed::integer".
    // So the resultset will have zeros instead of nulls. Otherwise this method will get broken!
    public function hashCode()
    {
        $str = '';
        foreach($this as $property_name => $value)
        {
/*            $converted_value = $this->convertZeroAndNullToNull($value);
            if ($value !== $converted_value) // alert in case of any value change:
            {   log_message('error', "ALERT: hashCode(): convertZeroAndNullToNull('".print_r($value, true)."' -> '".print_r($converted_value, true)."') for property '$property'.");
            }
*/
            // it is a must to add a separator because NULLs are added as empty strings!
            $str.= $property_name.':'.$value;
        }

//        log_message('error', "---- hashCode=".$str);

        return md5($str);
    }


    // return true or false:
    public function hashesAreEqual($compareToHash)
    {
        if (null === $compareToHash)
        {   return false;
        }

        return (0 === strcmp($this->hashCode(), $compareToHash));
    }

    public function __toString()
    {
        $res = '';
        foreach($this as $key => $value) {
            $res .= "    $key => $value\n";
        }

        return $res;
    }
}

// Each onject of this class is specific to one single day!
class Berkana_Schedule extends DBRowSerializer
{
	public $removed;
	public $teacher_id;
	public $coursetype_id;
	public $planneddate;
	public $canceled;
	public $plannedstarttime;
	public $plannedfinishtime;
	public $actualdate;
	public $actualstarttime;
	public $actualfinishtime;
	public $plannedroom_id; // this is scheduleactualday.plannedroom_id.
	public $comment; // this field might be missing from the other
}

class Berkana_Room extends DBRowSerializer
{
    public $title;
//    public $organization_id;
}

class Berkana_Organization extends DBRowSerializer
{
    public $name;
    public $smallname;
    public $phone;
	public $removed;
}

class Berkana_Coursetype extends DBRowSerializer
{
	public $name;
	public $removed;
	public $active;
}

class Berkana_Trainer extends DBRowSerializer
{
	public $name;
	//public $local_user_id; -- temporarily is disabled!ue to SERIALIZATION potential issues!
	public $removed;
}

class Berkana_Client extends DBRowSerializer
{
    public $id;
    public $lastname;
    public $firstname;
//    public $organization_id;
}

// this is about visiting by Berkana_Client of Berkana_Coursetype
// Remember to update fields/fieldnames in get_planned_visits_for_date()!
class Berkana_PlannedVisit extends DBRowSerializer
{
    public $client_id;
    public $schedule_id;// In its turn the Schedule already contains a reference to the planned_room_id and course_type_id.

    public $nvgr;       // this is "ME.nvgrotvisitedgoodreason". NOTE: I read this INTO DB, but later I use "visited" as A TRI-STATE VARIABLE: will that be accepted by Berkana local? Doubght it...
    public $visited;    // to be managed together with "$version" field.
    public $version;    // IMPORTANT FIELD: handle it with care! Used in row versioning (also by Hibernate)!
                        // Berkana-server data to be updated only if its row's version is less than web-server's row "version".
	public $removed;    // this is basemembership.removed ! To be checked to return Cabinet visits only non-removed ones!

    // Note: this table also contains a field "visited" which managed by Mobile client only.
}

class Berkana_Family extends DBRowSerializer
{
    public $name; // sql v2.: this is actually family.familyname from Berkana. Added MANUALLY because of NOW possible DATA versioning!
    public $email;
    public $mkey;
}

class Berkana_Message extends DBRowSerializer
{
    public $family_id;
    public $child_id;
    public $subject;
    public $body;
    public $daysbeforesending;
    public $senddate;
    public $sendtime;      
}


class UserDTO
{
    public $user_id; // is mapped to the "id" field of "users" DB table.
    public $login;
    public $password;

    public $yurlico_id; // if any.
    public $organization_club_id; // if any.

    public function __construct($user_id, $login, $password, $yurlico_id, $organization_club_id)
    {
        $this->user_id   = $user_id;
        $this->login     = $login;
        $this->password  = $password;
        $this->yurlico_id           = $yurlico_id;
        $this->organization_club_id = $organization_club_id;
    }
}

class Util_berkana_DTOs extends CI_Model /* see http://codeigniter.com/wiki/Inserting_Multiple_Records_Into_a_Table/ */
{
    // dummy class.
    function __construct()
    {
          parent::__construct();
    }
}