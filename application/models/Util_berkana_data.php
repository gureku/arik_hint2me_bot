<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH . '/controllers/admin/SecuredMatrix.php'); // greco.
require_once(APPPATH . '/models/Util_berkana_DTOs.php');        // greco.
require_once(APPPATH . '/models/Util_berkana.php');        // greco.

class ClientVisit
{
    const PRESENT = 1; // this value maps to Postgre "TRUE"
    const ABSENT  = 0; // this value maps to Postgre "FALSE"
}

//
// Class handles all the mobile data requests.
class Util_berkana_data extends CI_Model /* see http://codeigniter.com/wiki/Inserting_Multiple_Records_Into_a_Table/ */
{
    // values that will be excluded when returning trainings types for Client application.
    private static $stop_words__training_types = array('Аренда', 'День Рождения', 'Тест', 'Индивидуальное');
    const TRTYPE_SALT = 'cus$@24tomStrIng##';
    const SEPARATOR = ';';

    //
    // NOTE: tariffs PRIMARY source is DB table "berk_tariffs"! These constants below are just a hardcopy of that table IDs!
    //      Ensure to keep in a DB table always same row IDs!!! Or invent other mechanism to store same valus in DB and code...
    const TARIFF_ID__CABINET        = 1; // let's make this specific to Android App only!
    const TARIFF_ID__CLIENT         = 2; // let's make this specific to Android App only!
    const TARIFF_ID__TRAINER        = 3; // let's make this specific to Android App only!
    const TARIFF_ID__ALL_INCLUSIVE  = 4;
    const TARIFF_ID__WIDGET_SCHEDULE= 5;
    const TARIFF_ID__TG_APP_TRAINER = 6;

    const TBL_COURSETYPES   = 'berk_coursetypes';
    const TBL_SCHEDULE      = 'berk_schedule';
    const TBL_TRAINERS      = 'berk_trainers';
    const TBL_ROOMS         = 'berk_rooms';
    const TBL_ORGANIZATIONS = 'berk_organizations';
    const TBL_PLANNED_VISITS= 'berk_clients_visits';
    const TBL_BERK_CLIENTS  = 'berk_clients';
    const TBL_FAMILIES      = 'berk_families';
    const TBL_USER_MESSAGES = 'berk_messages';
    const TBL_ABONEMENTS    = 'berk_abonements';

    // this is not related to Berkana: it is designed to serve any messages but berkana families messages.
    // Though it is also possible sending messages (by providing Berkana_profile->local_user_id) to Berkana families through this table:
    const TBL_ALLOTHER_MESSAGES = 'sys_messages';

    // used when extracting different fields from the same DB record for DailyVisits:
    const VISITS_TBL_MOBILE     = 1; // i.e. wt_berk_clients_visits->visitedMobile, wt_berk_clients_visits->versionMobile
    const VISITS_TBL_BERKANA    = 2; // i.e. wt_berk_clients_visits->visited, wt_berk_clients_visits->version


    const SESSIONKEY__YURLICO_ID        = 'yurlico_id';
    const SESSIONKEY__ORGANIZATION_ID   = 'organization_id';

    const SESSIONKEY__YURLICO_ID_ANONYMOUS      = '-99999';
    const SESSIONKEY__ORGANIZATION_ID_ANONYMOUS = '-99999';
    const SESSIONKEY__USER_ID_ANONYMOUS         = '-99999';

    public $CI;


    function __construct()
    {
        parent::__construct();


        $this->CI = &get_instance();

        $this->load->library(array('transliterate', 'tank_auth'));
        $this->load->helper('string');

        $this->load->model('utilitar');
        $this->load->model('utilitar_db');
        $this->load->model('utilitar_date');

        $this->CI->load->model('tank_auth/users');

        $this->lang->load('data', 'russian');

        // see some patch for loading library from model. see http://stackoverflow.com/questions/2365591/load-a-library-in-a-model-in-codeigniter/2365848#2365848
    }


    //
    // TODO: "$schedule_actual_day_id" to be replaced by "$membershipelement_id" for the sake of clarity!!! This to be done upon Vadim's approve on my message "Cabinet: /tunnel/presence !!" sent by  Aug 26, 2015 at 6:25 PM!!!
    //
    // NOTE: this method updates only MOBILE datafields of client visits, leaving Berkana's "visited" and "version" intact.
    // Returns true in case if update/insert succeeded.
    // NOTE2: in fact there's NO NEED to supply also client_id for this function, but for backward-compatibility (with Android app) we keep that parameter, though from tG will pass "-1" instead.
    public function set_client_visit__mobile($client_id, $yurlico_id, $organization_id, $schedule_actual_day_id, $mobileVisited)
    {

        //-------------------------------------------------+
        // retrieve membershipelement_id to update the correct visit for a client:
        /*$visit = $this->get_visit_row($client_id, $yurlico_id, $organization_id, $schedule_actual_day_id);
        if (!$visit) // do not add NULL visits. Also convert to the array.
        {   $visits[] = array(  'id'            => $visit->id,
                                'client_id'     => $visit->client_id,
                                'schedule_id'   => $visit->schedule_id,

                                'version'       => $visit->version,
                                'visited'       => $visit->visited,
                                'mobileVersion' => $visit->mobileVersion,
                                'mobileVisited' => $visit->mobileVisited,

                                'yurlico_id'    => $visit->yurlico_id,
                                'organization_id' => $visit->organization_id,
                                );
        }*/
        //-------------------------------------------------+

        log_message('error', "set_client_visit__mobile() for client#".$client_id
                                .", club_id (as organization_id, yurlico_id)=".$organization_id.', '.$yurlico_id
                                ."', schedule_actual_day_id (to be replaced by membershipelement_id!)#$schedule_actual_day_id, presence = ".($mobileVisited ? 'TRUE':'FALSE'));

/*      $sql = "INSERT INTO wt_berk_clients_visits (id, yurlico_id, organization_id, schedule_id, client_id, mobileVisited, mobileVersion)
            VALUES ($yurlico_id, $organization_id, $schedule_actual_day_id, $client_id, 0, IFNULL(mobileVersion, 0) + 1)
            WHERE
            $yurlico_id, $organization_id, $schedule_actual_day_id, $client_id
            ON DUPLICATE KEY UPDATE
                mobileVisited = $mobileVisited,
                mobileVersion=IFNULL(mobileVersion, 0) + 1";
 */
        $sql = "UPDATE wt_berk_clients_visits
                SET mobileVisited=$mobileVisited, mobileVersion=(IFNULL(mobileVersion, 0) + 1)
                WHERE yurlico_id=$yurlico_id AND organization_id=$organization_id AND schedule_id=$schedule_actual_day_id ";

        // NOTE2: in fact there's NO NEED to supply also client_id for this function, but for backward-compatibility (with Android app) we keep that parameter, though from tG will pass "-1" instead.
        if ($client_id > 0)
        {   $sql .= " AND client_id=$client_id";
        }

        // TODO: see comments in function's description: to be used upon Vadim's approval to my email:
        // WHERE yurlico_id=$yurlico_id AND organization_id=$organization_id AND id=$membershipelement_id AND client_id=$client_id";

        $query  = $this->db->query($sql);

        return ($this->db->affected_rows() > 0);
    }

    public function STATS_get_trainings_for_today_TEMP($yurlico_id)
    {

    }

    public function STATS_get_trainings_for_today($yurlico_id)
    {
/*
    SELECT count(wt_berk_schedule.id) AS trainings_count_today
    FROM wt_berk_schedule
    WHERE
        wt_berk_schedule.planneddate = CURDATE()
    AND wt_berk_schedule.yurlico_id = $yurlico_id
    AND ((wt_berk_schedule.canceled IS NULL) OR (wt_berk_schedule.canceled <= 0))
    AND ((wt_berk_schedule.removed IS NULL) OR (wt_berk_schedule.removed <= 0))
*/
        $sql1 = 'SELECT count(wt_berk_schedule.id) AS trainings_count_today FROM wt_berk_schedule ';
        $sql1 .= ' WHERE wt_berk_schedule.yurlico_id = '.$yurlico_id.' ';
        $sql1 .= ' AND wt_berk_schedule.planneddate = CURDATE() ';
        $sql1 .= ' AND '.$this->_getQueryNotRemovedAndNotCanceled('wt_berk_schedule');

        $query = $this->db->query($sql1);
        if (!$query || ($query->num_rows() == 0))
        {   return '0';
        }

        $row = $query->row();
        return $row->trainings_count_today;
    }

    public function STATS_get_unique_clients_for_today($yurlico_id)
    {
        return 0;

    }


    // NOTE: function accepts either single interger or an array of integers in "$organization_id__or_array".
    public function get_devices_by($organization_id__or_array = -1, $role_id = -1)
    {
/*  SELECT *
    FROM sys_devices
    WHERE $organization_id = 456 and  $role_id = 123    */

        $this->db->select('sys_devices.*');

        if (is_array($organization_id__or_array))
        {   $this->db->where_in('organization_id', $organization_id__or_array); // those are IDs array in fact.
        }
        else if ($organization_id__or_array > 0)
        {   $this->db->where('organization_id', $organization_id__or_array);
        }

        if ($role_id > 0)
        {   $this->db->where('role_id', $role_id);
        }

        $query = $this->db->get('sys_devices');
        return Utilitar_db::safe_resultSet($query);
    }

    public function get_devices__admin_mode($yurlico_id, $organization_id = -1, $role_id = -1)
    {
/*
SELECT wt_sys_devices.did, users.id AS user_id, wt_users.username, wt_sys_devices_connection_trace.access_timestamp, wt_users_groups_link.groupId
FROM wt_sys_devices_connection_trace
LEFT JOIN wt_sys_devices ON wt_sys_devices.id= wt_sys_devices_connection_trace.sys_device_id
LEFT JOIN wt_users ON wt_users.id= wt_sys_devices_connection_trace.user_id
LEFT JOIN wt_berk_organizations ON wt_berk_organizations.id= wt_sys_devices_connection_trace.organization_id
LEFT JOIN wt_users_groups_link ON wt_users_groups_link.userId= wt_users.id
WHERE  wt_berk_organizations.yurlico_id=27
AND wt_users_groups_link.groupId = 4
GROUP BY wt_sys_devices.did
*/
        $this->db->select('sys_devices.id, sys_devices.did, users.id AS user_id, users.username, sys_devices_connection_trace.access_timestamp, users_groups_link.groupId');
        $this->db->join('sys_devices', 'sys_devices.id = sys_devices_connection_trace.sys_device_id', 'left');
        $this->db->join('users', 'users.id = sys_devices_connection_trace.user_id', 'left');
        $this->db->join('berk_organizations', 'berk_organizations.id = sys_devices_connection_trace.organization_id', 'left');
        $this->db->join('users_groups_link', 'users_groups_link.userId= users.id', 'left');

        $this->db->where('berk_organizations.yurlico_id', $yurlico_id);

        if ($organization_id > 0)
        {   $this->db->where('sys_devices_connection_trace.organization_id', $organization_id);
        }

        if ($role_id > 0)
        {   $this->db->where('users_groups_link.groupId', $role_id);
        }

        $this->db->group_by('sys_devices.did');

//        log_message('error', "SQL = ".print_r($this->db->_compile_select(), true));
        $query = $this->db->get('sys_devices_connection_trace');

        return Utilitar_db::safe_resultSet($query);
    }

    private function count_devices($yurlico_id, $role_name)
    {
        $role = $this->users->get_role($role_name);
	    if (!$role)
	    {   log_message('error', "count_devices(): ERROR! role '$role_name' is not supported!");
	        return 0;
	    }

        $organizations      = $this->getOrganizationsByYurlico($yurlico_id);
        $organization_ids   = array();
        foreach ($organizations as $organization)
        {   $organization_ids[] = $organization->id;
        }

        $devices = $this->get_devices_by($organization_ids, $role->id);

        return count($devices);
    }

    public function STATS_get_visitor_devices_count($yurlico_id)
    {
        return $this->count_devices($yurlico_id, SecuredMatrix::ROLE_MOBILE_CLIENT);
    }

    public function STATS_get_cabinet_devices_count($yurlico_id)
    {
        return $this->count_devices($yurlico_id, SecuredMatrix::ROLE_CABINET);
    }

    public function STATS_count_mobile_clients($yurlico_id)
    {
/*
SELECT COUNT(wt_users.id) as numrows -- wt_users_groups_link.groupId, wt_user_profiles.yurlico_id, wt_users.*
FROM wt_users
LEFT JOIN wt_user_profiles ON wt_user_profiles.user_id = wt_users.id
LEFT JOIN wt_users_groups_link ON wt_users_groups_link.userId = wt_users.id
WHERE wt_user_profiles.yurlico_id = 37
AND
wt_users_groups_link.groupId = 5
*/
        $role = $this->users->get_role(SecuredMatrix::ROLE_MOBILE_CLIENT);
	    if (!$role)
	    {   log_message('error', "count_mobile_clients(): ERROR! role '".SecuredMatrix::ROLE_MOBILE_CLIENT."' is not supported!");
	        return 0;
	    }

        $this->db->select('COUNT(wt_users.id) as numrows');
        $this->db->join('user_profiles', 'user_profiles.user_id = users.id', 'left');
        $this->db->join('users_groups_link', 'users_groups_link.userId = users.id', 'left');

        $this->db->where('user_profiles.yurlico_id', $yurlico_id); // removed are allowed. Inactive ones - are not.
        $this->db->where('users_groups_link.groupId', $role->id);
        $query = $this->db->get('users');

        if ($query->num_rows() == 0)
        {   return 0;
        }

        $row = $query->row();
        return $row->numrows;
    }

    public function STATS_get_visits_for_today($yurlico_id)
    {
/*
SELECT
   count(wt_berk_clients_visits.client_id) AS visits_count_today
FROM  wt_berk_clients_visits, wt_berk_schedule
WHERE
    wt_berk_clients_visits.schedule_id=wt_berk_schedule.id
    AND wt_berk_clients_visits.yurlico_id = $yurlico_id
    AND wt_berk_schedule.planneddate = CURDATE()

    AND ((wt_berk_schedule.canceled IS NULL) OR (wt_berk_schedule.canceled <= 0))
    AND ((wt_berk_schedule.removed IS NULL) OR (wt_berk_schedule.removed <= 0))

 */
        $sql = 'SELECT count(wt_berk_clients_visits.client_id) AS visits_count_today FROM  wt_berk_clients_visits, wt_berk_schedule ';
        $sql .= ' WHERE wt_berk_clients_visits.yurlico_id = '.$yurlico_id.' ';
        $sql .= ' AND wt_berk_clients_visits.schedule_id=wt_berk_schedule.id ';
        $sql .= ' AND wt_berk_schedule.planneddate = CURDATE() ';
        $sql .= ' AND '.$this->_getQueryNotRemovedAndNotCanceled('wt_berk_schedule');

        $query = $this->db->query($sql);
        if (!$query || ($query->num_rows() == 0))
        {   return '0';
        }

        $row = $query->row();
        return $row->visits_count_today;
    }


    public function get_family_childrennames($family_id)
    {
    /*
      SELECT
              wt_berk_clients.id,
      	CONCAT(wt_berk_clients.firstname, ' ', wt_berk_clients.lastname) AS childname
      FROM wt_berk_clients

      where wt_berk_clients.yurlico_id = 37
      AND wt_berk_clients.organization_id=5
      and wt_berk_clients.family_id = 345
     */

        $this->db->select('id, CONCAT(wt_berk_clients.firstname, \' \', wt_berk_clients.lastname) AS childname', FALSE);
        $this->db->where('berk_clients.family_id', $family_id);
        $this->dbfilter_yurlico_and_organization('berk_clients'); // NOTE: this call is mandatory to be present when accessing berkana assests!
        $this->db->order_by('id');

        $query = $this->db->get('berk_clients');

        return Utilitar_db::safe_resultSet($query);
    }


    public function get_family_abonements_ext($family_id, $b_active_only, $yurlico_id, $organization_id)
    {
    /*
SELECT DISTINCT
	wt_berk_abonements.id,
	wt_berk_abonements.`child_id`,
	wt_berk_abonements.`family_id`,
	wt_berk_abonements.`date_start` AS valid_from,
	wt_berk_abonements.`date_end` AS valid_till,
	wt_berk_abonements.`visits_passed`,
	wt_berk_abonements.`visits_count`,
	0 AS visits_moved,
	wt_berk_coursetypes.name
FROM `wt_berk_abonements`
LEFT JOIN wt_berk_coursetypes ON wt_berk_coursetypes.id = wt_berk_abonements.coursetype_id
WHERE wt_berk_coursetypes.active = TRUE
AND wt_berk_abonements.removed = 0
 AND wt_berk_abonements.family_id = 345
 AND (wt_berk_abonements.date_end >= CURRENT_DATE)
ORDER BY valid_till ASC;
     */

        $this->db->select('berk_abonements.id, berk_abonements.coursetype_id, berk_abonements.child_id, berk_abonements.family_id, berk_abonements.date_start AS valid_from, berk_abonements.date_end AS valid_till,
        berk_abonements.visits_passed, berk_abonements.visits_count, 0 AS visits_moved, berk_coursetypes.name');
        $this->db->distinct();
        $this->db->join('berk_coursetypes', 'berk_coursetypes.id = berk_abonements.coursetype_id', 'left');

        $this->db->where('berk_coursetypes.active = true'); // removed are allowed. Inactive ones - are not.
        $this->db->where('berk_abonements.family_id', $family_id);
        $this->db->where('berk_abonements.removed', 0);
        if ($b_active_only)
        {   $this->db->where('wt_berk_abonements.date_end >= CURRENT_DATE', NULL, FALSE);
        }

        $this->dbfilter_yurlico_and_organization_multipletables('berk_abonements', $yurlico_id, $organization_id); // NOTE: this call is mandatory to be present when accessing berkana assests!
        $this->db->order_by('berk_abonements.date_end ASC');

//        log_message('error', "SQL = ".print_r($this->db->_compile_select(), true));
        $query = $this->db->get('berk_abonements');

        return Utilitar_db::safe_resultSet($query);
    }

    public function get_family_abonements($family_id)
    {
    /*
SELECT DISTINCT
	wt_berk_abonements.id,
	wt_berk_abonements.`child_id`,
	wt_berk_abonements.`family_id`,
	wt_berk_abonements.`date_start` AS valid_from,
	wt_berk_abonements.`date_end` AS valid_till,
	wt_berk_abonements.`visits_passed`,
	wt_berk_abonements.`visits_count`,
	0 AS visits_moved,
	wt_berk_coursetypes.name
FROM `wt_berk_abonements`
LEFT JOIN wt_berk_coursetypes ON wt_berk_coursetypes.id = wt_berk_abonements.coursetype_id
WHERE wt_berk_coursetypes.active = true
AND wt_berk_abonements.removed = 0
     */
        $yurlico_id         = Json_base::get_current_yurlico_id();
        $organization_id    = Json_base::get_current_organization_id();

        return $this->get_family_abonements_ext($family_id, true, $yurlico_id, $organization_id);
    
        /*$this->db->select('berk_abonements.id, berk_abonements.child_id, berk_abonements.family_id, berk_abonements.date_start AS valid_from, berk_abonements.date_end AS valid_till,
        berk_abonements.visits_passed, berk_abonements.visits_count, 0 AS visits_moved, berk_coursetypes.name');
        $this->db->distinct();
        $this->db->join('berk_coursetypes', 'berk_coursetypes.id = berk_abonements.coursetype_id', 'left');

        $this->db->where('berk_coursetypes.active = true'); // removed are allowed. Inactive ones - are not.
        $this->db->where('berk_abonements.family_id', $family_id);
        $this->db->where('berk_abonements.removed', 0);
        $this->dbfilter_yurlico_and_organization('berk_abonements'); // NOTE: this call is mandatory to be present when accessing berkana assests!
        $this->db->order_by('berk_abonements.date_end ASC');

//        log_message('error', "SQL = ".print_r($this->db->_compile_select(), true));
        $query = $this->db->get('berk_abonements');

        return Utilitar_db::safe_resultSet($query);*/
    }

    // gets remote id based on local_id:
    public function get_berkana_trainer_by_localuser_id($local_user_id)
    {
        $this->db->where('local_user_id', $local_user_id);
        $query = $this->db->get('berk_trainers');

        return Utilitar_db::safe_resultRow($query);
    }


    private function _get_trainer_trainings_for($trainer_id, $yurlico_id, $organization_id, $date_options)
    {
/*
SELECT
sched.id,
sched.coursetype_id,
sched.organization_id AS club_id,
coursetypes.name AS training,
trainers.id AS trainer_id,
sched.plannedroom_id AS cabinet_id,
sched.canceled AS state,
DATE_FORMAT(sched.planneddate, "%d/%m/%y") AS planneddate_cloud_fmt,
TIME_FORMAT(sched.plannedstarttime, "%H:%i") AS start_time,
TIME_FORMAT(sched.plannedfinishtime, "%H:%i") AS end_time
FROM wt_berk_schedule sched
LEFT JOIN wt_berk_trainers trainers ON trainers.id = sched.teacher_id
LEFT JOIN wt_berk_coursetypes coursetypes ON sched.coursetype_id = coursetypes.id
WHERE
	(sched.yurlico_id = 93 AND sched.organization_id = 63)
	AND (coursetypes.yurlico_id = 93 AND coursetypes.organization_id = 63)
	AND (trainers.yurlico_id = 93 AND trainers.organization_id = 63)
	AND sched.teacher_id=75
	AND  (sched.planneddate BETWEEN '2020-02-01' AND '2020-02-29')
	AND  (
		( (sched.removed = 0) OR (sched.removed IS NULL) )
		AND ( (sched.canceled = 0) OR (sched.canceled IS NULL) )
	     )
ORDER BY sched.planneddate, sched.plannedstarttime, coursetypes.name
*/
        $this->db->select(' sched.id,
                            sched.coursetype_id,
                            sched.organization_id AS club_id,
                            coursetypes.name AS coursetype_name,
                            trainers.id AS trainer_id,
                            sched.plannedroom_id AS cabinet_id,
                            sched.canceled AS state,
                            DATE_FORMAT(sched.planneddate, "%d/%m/%y") AS planneddate_cloud_fmt,
                            TIME_FORMAT(sched.plannedstarttime, "%H:%i") AS plannedstarttime,
                            TIME_FORMAT(sched.plannedfinishtime, "%H:%i") AS plannedfinishtime', FALSE);

        $this->db->from('berk_schedule sched');
        $this->db->join('berk_trainers trainers',       'trainers.id = sched.teacher_id',       'left');
        $this->db->join('berk_coursetypes coursetypes', 'sched.coursetype_id = coursetypes.id', 'left');

        // NOTE: this call is mandatory to be present when accessing berkana assests!
        $this->dbfilter_yurlico_and_organization_multipletables(array('sched', 'coursetypes', 'trainers'), $yurlico_id, $organization_id);

        if ($trainer_id > 0)
        {   $this->db->where('sched.teacher_id', $trainer_id);
        }

        $this->db->where('coursetypes.active = true'); // Inactive ones are not allowed.

        if (isset($date_options['days_before_and_after']))
        {   $days_before= $date_options['days_before_and_after']['days_before']; // days count relative to today.
            $days_after = $date_options['days_before_and_after']['days_after']; // days count relative to today.
            $this->db_where_date__relative2today('sched', 'planneddate', $days_before, $days_after);
        }
        else if (isset($date_options['between_dates']))
        {   $date_from = $date_options['between_dates']['date_from'];
            $date_till = $date_options['between_dates']['date_till'];
            $this->db_where_date__between('sched', 'planneddate', $date_from, $date_till);
        }

        $this->_getQueryNotRemovedAndNotCanceled('sched');

        $this->db->order_by('sched.planneddate, sched.plannedstarttime, coursetypes.name');

//        $sql = $this->db->get_compiled_select('', FALSE);
//        log_message('error', "XXX SQL=\n".print_r($sql, true));

        $query = $this->db->get();
        return Utilitar_db::safe_resultSet($query);
    }

    // if both $date_from==$date_to==0 then current day is chosen.
    public function get_trainer_trainings_between_dates($trainer_remote_id, $yurlico_id, $organization_id, $date_from, $date_to)
    {
        $date_options = array();
        $date_options['between_dates']['date_from'] = $date_from;
        $date_options['between_dates']['date_till'] = $date_to;

        return $this->_get_trainer_trainings_for($trainer_remote_id, $yurlico_id, $organization_id, $date_options);
    }

    public function get_trainer_trainings_for_days_interval($trainer_remote_id, $yurlico_id, $organization_id, $days_before, $days_after)
    {
        $date_options = array();
        $date_options['days_before_and_after']['days_before']   = $days_before; // days count relative to today.
        $date_options['days_before_and_after']['days_after']    = $days_after; // days count relative to today.

        return $this->_get_trainer_trainings_for($trainer_remote_id, $yurlico_id, $organization_id, $date_options);
    }

    public function get_family_trainings_for_date($family_id, $days_before, $days_after)
    {
/*
SELECT DISTINCT
wt_berk_clients_visits.id AS membershipelement_id,
wt_berk_clients.family_id,
wt_berk_clients_visits.client_id,
CONCAT (wt_berk_clients.firstname, wt_berk_clients.lastname) AS childname,
wt_berk_clients_visits.schedule_id,
wt_berk_schedule.coursetype_id,
wt_berk_coursetypes.name,
wt_berk_schedule.teacher_id,
wt_berk_trainers.name AS trainer_name,
wt_berk_schedule.plannedroom_id AS room_id,
wt_berk_schedule.planneddate,
wt_berk_schedule.plannedstarttime,
wt_berk_schedule.plannedfinishtime,
wt_berk_clients_visits.schedule_id AS actualday_id
FROM  wt_berk_clients_visits
LEFT JOIN wt_berk_schedule ON wt_berk_clients_visits.schedule_id=wt_berk_schedule.id
LEFT JOIN wt_berk_coursetypes ON wt_berk_schedule.coursetype_id = wt_berk_coursetypes.id
LEFT JOIN wt_berk_clients ON wt_berk_clients.id = wt_berk_clients_visits.client_id
LEFT JOIN `wt_berk_trainers` ON wt_berk_trainers.id = wt_berk_schedule.teacher_id
WHERE
	(wt_berk_clients_visits.yurlico_id = 24 AND wt_berk_clients_visits.organization_id = 2)
	AND (wt_berk_schedule.yurlico_id = 24 AND wt_berk_schedule.organization_id = 2)
	AND (wt_berk_coursetypes.yurlico_id = 24 AND wt_berk_coursetypes.organization_id = 2)
	AND (wt_berk_clients.yurlico_id = 24 AND wt_berk_clients.organization_id = 2)
	AND wt_berk_clients.family_id=10952
	AND  (wt_berk_schedule.planneddate BETWEEN CURRENT_DATE - INTERVAL '0' DAY AND CURRENT_DATE + INTERVAL '8' DAY)
	AND  (
		( (wt_berk_schedule.removed = 0) OR (wt_berk_schedule.removed IS NULL) )
		AND ( (wt_berk_schedule.canceled = 0) OR (wt_berk_schedule.canceled IS NULL) )
	     )
ORDER BY wt_berk_schedule.planneddate, wt_berk_coursetypes.name ASC
*/
        $db_prefix = $this->db->dbprefix;

        $this->db->distinct();
        $this->db->select(' berk_clients_visits.id AS membershipelement_id,
                            berk_clients_visits.client_id,
                            CONCAT ('.$db_prefix.'berk_clients.firstname, '.$db_prefix.'berk_clients.lastname) AS childname,
                            berk_clients_visits.organization_id,
                            berk_clients_visits.schedule_id,
                            berk_clients_visits.schedule_id AS actualday_id,
                            berk_clients.family_id,
                            berk_coursetypes.name,
                            berk_schedule.teacher_id,
                            berk_trainers.name AS trainer_name,
                            berk_schedule.plannedroom_id AS room_id,
                            berk_schedule.planneddate,
                            berk_schedule.plannedstarttime,
                            berk_schedule.plannedfinishtime,
                            berk_schedule.coursetype_id', FALSE);

        $this->db->join('berk_schedule',    'berk_clients_visits.schedule_id = berk_schedule.id',   'left');
        $this->db->join('berk_coursetypes', 'berk_schedule.coursetype_id = berk_coursetypes.id',    'left');
        $this->db->join('berk_clients',     'berk_clients.id = berk_clients_visits.client_id',      'left');
        $this->db->join('berk_trainers',    'berk_trainers.id = berk_schedule.teacher_id',      'left');

        $this->dbfilter_yurlico_and_organization(array( $db_prefix.'berk_clients_visits',
                                                        $db_prefix.'berk_schedule',
                                                        $db_prefix.'berk_coursetypes',
                                                        $db_prefix.'berk_clients',
                                                        $db_prefix.'berk_trainers',
                                                        )
                                                 ); // NOTE: this call is mandatory to be present when accessing berkana assests!
        $this->db->where('berk_clients.family_id', $family_id);
        $this->db->where('berk_coursetypes.active = true'); // Inactive ones are not allowed.

        $this->db_where_date__relative2today($db_prefix.'berk_schedule', 'planneddate', $days_before, $days_after);

        $this->_dbNotRemoved($db_prefix.'berk_coursetypes');// we also hide removed course-types as well.
        $this->_dbNotRemoved($db_prefix.'berk_schedule');
        $this->_dbNotCanceled($db_prefix.'berk_schedule');

        $this->db->order_by('berk_schedule.planneddate, berk_coursetypes.name ASC');

//        log_message('error', "SQL = ".print_r($this->db->_compile_select(), true));
//        log_message('error', "QQQ SQL = ".print_r($this->utilitar_db->get_sql_compiled(), true));

        $query = $this->db->get('berk_clients_visits');

        return Utilitar_db::safe_resultSet($query);
    }

    public function get_trainings_for_date($days_before, $days_after)
    {
/*
SELECT *
FROM wt_berk_schedule
WHERE
((wt_berk_schedule.canceled IS NULL) OR (wt_berk_schedule.canceled <= 0))
AND
((wt_berk_schedule.canceled IS NULL) OR (wt_berk_schedule.canceled <= 0))
-- AND	wt_berk_schedule.planneddate BETWEEN CURRENT_DATE - INTERVAL '3' DAY AND CURRENT_DATE + INTERVAL '1' DAY
wt_berk_schedule.planneddate BETWEEN CURRENT_DATE  AND  CURRENT_DATE
 */
        $db_prefix = $this->db->dbprefix;

        $this->db->select('id, plannedroom_id, teacher_id, coursetype_id,
DATE_FORMAT('.$db_prefix.'berk_schedule.planneddate, "%Y-%m-%d") AS planneddate,
TIME_FORMAT('.$db_prefix.'berk_schedule.plannedstarttime, "%H:%i") AS plannedstarttime,
TIME_FORMAT('.$db_prefix.'berk_schedule.plannedfinishtime, "%H:%i") AS plannedfinishtime ', FALSE);


        $this->_dbNotRemoved('wt_berk_schedule');
        $this->_dbNotCanceled('wt_berk_schedule');

        $this->db_where_date__relative2today($db_prefix.'berk_schedule', 'planneddate', $days_before, $days_after);

        $this->dbfilter_yurlico_and_organization($db_prefix.'berk_schedule'); // NOTE: this call is mandatory to be present when accessing berkana assests!

//        log_message('error', "SQL = ".print_r($this->db->_compile_select(), true));
        $query = $this->db->get('berk_schedule');

        return Utilitar_db::safe_resultSet($query);
    }

    // fully prefixed table name neeed.
    private function _dbNotRemoved($table_name)
    {   $this->db->where('( (`'.$table_name.'`.`removed` = 0) OR (`'.$table_name.'`.`removed` IS NULL) ) ', NULL, FALSE);
    }

    // fully prefixed table name neeed.
    private function _dbNotCanceled($table_name)
    {   $this->db->where('( (`'.$table_name.'`.`canceled` = 0) OR (`'.$table_name.'`.`canceled` IS NULL) ) ', NULL, FALSE);
    }

    private function _getQueryNotRemovedAndNotCanceled($table_name)
    {
        return ' ( ( (`'.$table_name.'`.`removed` = 0) OR (`'.$table_name.'`.`removed` IS NULL) ) AND ( (`'.$table_name.'`.`canceled` = 0) OR (`'.$table_name.'`.`canceled` IS NULL) ) ) ';
    }


    private function _getQueryNotRemoved($table_name)
    {
        return  ' ( (`'.$table_name.'`.`removed` = 0) OR (`'.$table_name.'`.`removed` IS NULL) ) ';
    }


    //
    // NOTE:    Returns either 'mobile' or 'berkana' set of visits' fields only!
    //
    // Returns the list of clients' visits for trainings relative to current (!) day. See $days_before and $days_after.
    // NOTE: Ensure to keep in sync the fields/fieldnames with Berkana_PlannedVisit fields!
    //
    //  TODO: check if returning only verion/mobileVersion > 0 items make sense!!!
    ///
    public function get_planned_visits_for_date($type, $bGetAdvancedData, $days_before, $days_after, $yurlico_id, $organization_id, $modified_only = false, $cabinet_id = -1)
    {
/*
SELECT
DISTINCT IFNULL(wt_berk_clients_visits.mobileVisited, 0) AS visited,
wt_berk_coursetypes.name,  IFNULL(wt_berk_clients_visits.mobileVersion, 0) as version,
wt_berk_clients_visits.id,
wt_berk_clients_visits.client_id, wt_berk_clients_visits.yurlico_id as clientvisits_yurlico_id,
wt_berk_clients_visits.schedule_id
FROM  wt_berk_clients_visits
LEFT JOIN wt_berk_schedule ON wt_berk_clients_visits.schedule_id=wt_berk_schedule.id
LEFT JOIN wt_berk_coursetypes ON wt_berk_coursetypes.id = wt_berk_schedule.coursetype_id
WHERE
    (wt_berk_clients_visits.yurlico_id = 44 AND wt_berk_clients_visits.organization_id = 13)
AND (wt_berk_schedule.yurlico_id = 44 AND wt_berk_schedule.organization_id = 13)
AND (wt_berk_coursetypes.yurlico_id = 44 AND wt_berk_coursetypes.organization_id = 13)
AND wt_berk_coursetypes.active > 0
AND (wt_berk_schedule.planneddate BETWEEN CURRENT_DATE - INTERVAL '0' DAY AND CURRENT_DATE + INTERVAL '7' DAY)
AND ( ( (`wt_berk_schedule`.`removed` = 0) OR (`wt_berk_schedule`.`removed` IS NULL) ) AND ( (`wt_berk_schedule`.`canceled` = 0) OR (`wt_berk_schedule`.`canceled` IS NULL) ) )
AND ( (`wt_berk_clients_visits`.`removed` = 0) OR (`wt_berk_clients_visits`.`removed` IS NULL) )
ORDER BY wt_berk_schedule.planneddate
*/
//        log_message('error', "get_planned_visits_for_date(type, bGetAdvancedData, days_before, days_after, yurlico_id, organization_id)=($type, $bGetAdvancedData, $days_before, $days_after, $yurlico_id, $organization_id)");
        $db_field_name__VISITED = null;
        $db_field_name__VERSION = null;
        switch ($type)
        {
            case Util_berkana_data::VISITS_TBL_MOBILE:
                $db_field_name__VISITED = 'mobileVisited';
                $db_field_name__VERSION = 'mobileVersion';
                break;

            case Util_berkana_data::VISITS_TBL_BERKANA:
                $db_field_name__VISITED = 'visited';
                $db_field_name__VERSION = 'version';
                break;

            default:
            {   log_message('error', 'ERROR! unrecognized type of planned visits ('.$type.') requested. SKIPPING!');
                return array();
            }
        }

        $db_prefix = $this->db->dbprefix;

        return $this->rewrittenQuery($yurlico_id, $organization_id, $db_field_name__VISITED, $db_field_name__VERSION, $days_before, $days_after, $modified_only, $cabinet_id);
    }


    //
    //  NOTE:    Returns either 'mobile' or 'berkana' set of visits' fields only!
    //
    //  Returns the list of clients' visits for trainings relative to current (!) day. See $days_before and $days_after.
    //  NOTE: Ensure to keep in sync the fields/fieldnames with Berkana_PlannedVisit fields!
    //
    //  NOTE: version #2 returns NO COUTSETYPE_NAME!!!
    //
    //  TODO: check if returning only verion/mobileVersion > 0 items make sense!!!
    ///
    public function get_planned_visits_for_date_v2($type, $days_before, $days_after, $client_IDs, $yurlico_id, $organization_id, $modified_only = false, $cabinet_id = -1, $schedule_id = -1)
    {
//        log_message('error', "get_planned_visits_for_date(type, days_before, days_after, yurlico_id, organization_id)=($type, $days_before, $days_after, $yurlico_id, $organization_id)");
        $db_field_name__VISITED = null;
        $db_field_name__VERSION = null;
        switch ($type)
        {
            case Util_berkana_data::VISITS_TBL_MOBILE:
                $db_field_name__VISITED = 'mobileVisited';
                $db_field_name__VERSION = 'mobileVersion';
                break;

            case Util_berkana_data::VISITS_TBL_BERKANA:
                $db_field_name__VISITED = 'visited';
                $db_field_name__VERSION = 'version';
                break;

            default:
            {   log_message('error', 'ERROR! unrecognized type of planned visits ('.$type.') requested. SKIPPING!');
                return array();
            }
        }

        $date_options = array('days_before_and_after' => array('days_before' => $days_before, 'days_after' => $days_after));
        return $this->rewrittenQuery_v2($yurlico_id, $organization_id, $client_IDs, $db_field_name__VISITED, $db_field_name__VERSION, $date_options, $modified_only, $cabinet_id, $schedule_id);
    }

    // function provides data for "get_planned_visits_for_date().
    //  IMPORTANT: ensure
    //  Ensure to update WHERE conditions simultaneously with "getRoomTrainingsArray()" or you will face data discrepancy in a result set!
    private function rewrittenQuery($yurlico_id, $organization_id, $db_field_name__VISITED, $db_field_name__VERSION, $days_before, $days_after, $modified_only, $cabinet_id)
    {
/*    
SELECT 
DISTINCT IFNULL(wt_berk_clients_visits.mobileVisited, 0) AS visited,
wt_berk_coursetypes.name,  IFNULL(wt_berk_clients_visits.mobileVersion, 0) as version,
wt_berk_clients_visits.id,
wt_berk_clients_visits.client_id, wt_berk_clients_visits.yurlico_id as clientvisits_yurlico_id,
wt_berk_clients_visits.schedule_id
FROM  wt_berk_clients_visits
LEFT JOIN wt_berk_schedule ON wt_berk_clients_visits.schedule_id=wt_berk_schedule.id
LEFT JOIN wt_berk_coursetypes ON wt_berk_coursetypes.id = wt_berk_schedule.coursetype_id
WHERE
(wt_berk_clients_visits.yurlico_id = 27 AND wt_berk_clients_visits.organization_id = 3)
AND (wt_berk_schedule.yurlico_id = 27 AND wt_berk_schedule.organization_id = 3)
AND (wt_berk_coursetypes.yurlico_id = 27 AND wt_berk_coursetypes.organization_id = 3)
AND wt_berk_coursetypes.active > 0
AND (wt_berk_schedule.planneddate BETWEEN CURRENT_DATE - INTERVAL '0' DAY AND CURRENT_DATE + INTERVAL '7' DAY)
AND ( ( (`wt_berk_schedule`.`removed` = 0) OR (`wt_berk_schedule`.`removed` IS NULL) ) AND ( (`wt_berk_schedule`.`canceled` = 0) OR (`wt_berk_schedule`.`canceled` IS NULL) ) )
AND ( (`wt_berk_clients_visits`.`removed` = 0) OR (`wt_berk_clients_visits`.`removed` IS NULL) )
ORDER BY wt_berk_schedule.planneddate
*/
        log_message('error', "QQQ rewrittenQuery($yurlico_id, $organization_id, $db_field_name__VISITED, $db_field_name__VERSION, $days_before, $days_after, $modified_only, $cabinet_id)");
        $db_prefix = $this->db->dbprefix;

        $this->db->select(' IFNULL('.$db_prefix.'berk_clients_visits.'.$db_field_name__VISITED.', 0) AS visited,
                            berk_coursetypes.name,  IFNULL('.$db_prefix.'berk_clients_visits.'.$db_field_name__VERSION.', 0) as version,
                            berk_clients_visits.id,
                            berk_clients_visits.client_id, berk_clients_visits.yurlico_id as clientvisits_yurlico_id,
                            berk_clients_visits.schedule_id ', FALSE);
        $this->db->distinct();
        $this->db->from('berk_clients_visits');
        $this->db->join('berk_schedule',    'berk_clients_visits.schedule_id = berk_schedule.id',   'left');
        $this->db->join('berk_coursetypes', 'berk_coursetypes.id = berk_schedule.coursetype_id',    'left');

        // get only MODIFIED fields. Makes sense for WTService "/get_data/1" calls:
        if ($modified_only)
        {   $this->db->where('berk_clients_visits.'.$db_field_name__VERSION.' > 0');
        }

        if ($cabinet_id > 0)
        {   $this->db->where('berk_schedule.plannedroom_id', $cabinet_id);
        }

        // NOTE: this call is mandatory to be present when accessing berkana assests!
        $this->dbfilter_yurlico_and_organization($this->utilitar_db->db_prefix(array('berk_clients_visits', 'berk_schedule', 'berk_coursetypes')));

//        $this->db->where('berk_coursetypes.active > 0'); // даже неактивные показываются. А вот удалённые (removed==true) уже не должны показываться.

        $this->db_where_date__relative2today($db_prefix.'berk_schedule', 'planneddate', $days_before, $days_after);

        $this->_dbNotRemoved ($db_prefix.'berk_schedule');
        $this->_dbNotCanceled($db_prefix.'berk_schedule');
        $this->_dbNotRemoved ($db_prefix.'berk_clients_visits');

        $this->db->order_by('berk_schedule.planneddate');

        $query = $this->db->get();

        return Utilitar_db::safe_resultSet($query);
    }

    // returns just SQL string.
    private function get_sql_limit_date__relative2today($table_name, $days_before, $days_after)
    {
        if (($days_before <= 0) && ($days_after <= 0))
        {
            return ' ('.$table_name.'.planneddate BETWEEN CURRENT_DATE AND CURRENT_DATE) ';
        }

        return ' ('.$table_name.'.planneddate BETWEEN CURRENT_DATE - INTERVAL \''.$days_before.'\' DAY AND CURRENT_DATE + INTERVAL \''.$days_after.'\' DAY) ';
    }

    // get rows BETWEEN DAYS COUNT INTERVAL specified:
    private function db_where_date__relative2today($table_name, $field_name, $days_before, $days_after)
    {
        if ((0 == $days_before) && (0 == $days_after))
        {   $this->db->where(sprintf('%s.%s BETWEEN CURRENT_DATE AND CURRENT_DATE', $table_name, $field_name), NULL, false);
        }
        else
        {   $this->db->where(sprintf('%s.%s BETWEEN CURRENT_DATE - INTERVAL \'%d\' DAY AND CURRENT_DATE + INTERVAL \'%d\' DAY ', $table_name, $field_name, $days_before, $days_after));
        }
    }

    // get rows BETWEEN TWO DATES specified:
    private function db_where_date__between($table_name, $field_name, $date_from, $date_till)
    {
        if ((!$date_till) && (!$date_till))
        {   $this->db->where(sprintf('%s.%s = CURRENT_DATE', $table_name, $field_name), NULL, false);
        }
        else if (0 === strcmp($date_from, $date_till)) // if same date specified
        {   $this->db->where(sprintf('%s.%s = \'%s\'', $table_name, $field_name, $date_from), NULL, false);
        }
        else // if date interval is specified
        {   $this->db->where(sprintf('%s.%s >= \'%s\' AND %s.%s <= \'%s\' ', $table_name, $field_name, $date_from, $table_name, $field_name, $date_till));
        }
    }

    private function getFakeClientsList()
    {
        $clients = array(
            array('id' => 1, 'name' => 'Андриановский Иван Владимирович'),
            array('id' => 1, 'name' => 'Балабанов Василий Андреевич'),
            array('id' => 1, 'name' => 'Блохин Евгений Сергеевич'),
            array('id' => 1, 'name' => 'Бородин Андрей Сергеевич'),
            array('id' => 1, 'name' => 'Вишератин Александр Александрович'),
            array('id' => 1, 'name' => 'Килин Игорь Константинович'),
            array('id' => 1, 'name' => 'Корнильцев Анатолий Владимирович'),
            array('id' => 1, 'name' => 'Лунев Даниил Олегович'),
            array('id' => 1, 'name' => 'Мельников Валентин Рафаэльевич'),
            array('id' => 1, 'name' => 'Новиков Владимир Петрович'),
            array('id' => 1, 'name' => 'Петрова Екатерина Петровна'),
            array('id' => 1, 'name' => 'Помпеев Олег Александрович'),
            array('id' => 1, 'name' => 'Потужнов Илья Андреевич'),
            array('id' => 1, 'name' => 'Романов Никита Константинович'),
            array('id' => 1, 'name' => 'Смирнов Алексей Олегович'),
            array('id' => 1, 'name' => 'Ушаков Сергей Дмитриевич'),
            array('id' => 1, 'name' => 'Филимонов Дмитрий Аркадьевич'),
            array('id' => 1, 'name' => 'Юрганов Алексей Константинович'),
            array('id' => 1, 'name' => 'Валуев Николай Арифметович'),
            array('id' => 1, 'name' => 'Богданов Иван Викторович'),
            array('id' => 1, 'name' => 'Валиев Радик Айратович'),
            array('id' => 1, 'name' => 'Воронина Екатерина Юрьевна'),
            array('id' => 1, 'name' => 'Гражевская Александра Сергеевна'),
            array('id' => 1, 'name' => 'Колчанов Дмитрий Петрович'),
            array('id' => 1, 'name' => 'Крикунов Алексей Владимирович'),
            array('id' => 1, 'name' => 'Лазарева Анастасия Сергеевна'),
            array('id' => 1, 'name' => 'Лашков Игорь Борисович'),
            array('id' => 1, 'name' => 'Николаев Игорь Анатольевич'),
            array('id' => 1, 'name' => 'Пак Денис Владимирович'),
            array('id' => 1, 'name' => 'Рагозин Кирилл Владимирович'),
            array('id' => 1, 'name' => 'Ромашкин Роман Станиславович'),
            array('id' => 1, 'name' => 'Смертина Ксения Михайловна'),
            array('id' => 1, 'name' => 'Терещенков Виктор Андреевич'),
            array('id' => 1, 'name' => 'Шаров Илья Сергеевич'),
            array('id' => 1, 'name' => 'Шеварнадзе Виктор Дмитриевич'),
            array('id' => 1, 'name' => 'Багаев Дмитрий Сергеевич'),
            array('id' => 1, 'name' => 'Береснев Никита Павлович'),
            array('id' => 1, 'name' => 'Брезинский Михаил Александрович'),
            array('id' => 1, 'name' => 'Васюк Владислав Алексеевич'),
            array('id' => 1, 'name' => 'Луковский Игорь Викторович'),
            array('id' => 1, 'name' => 'Магер Евгения Андреевна'),
            array('id' => 1, 'name' => 'Морозов Евгений Игоревич'),
            array('id' => 1, 'name' => 'Нестеров Антон Игоревич'),
            array('id' => 1, 'name' => 'Саргсян Арман Саргисович'),
            array('id' => 1, 'name' => 'Селин Антон Олегович'),
            array('id' => 1, 'name' => 'Сенько Александр Григорьевич'),
            array('id' => 1, 'name' => 'Синиченков Вадим Александрович'),
            array('id' => 1, 'name' => 'Соловьев Анатолий Олегович'),
            array('id' => 1, 'name' => 'Хегай Максим Вилорьевич'),
            array('id' => 1, 'name' => 'Черненко Илья Александрович'),
            array('id' => 1, 'name' => 'Шуткова Любовь Владимировна'),
            array('id' => 1, 'name' => 'Сигунов Тигран Алекперович'),
            array('id' => 1, 'name' => 'Веселов Денис Андреевич'),
            array('id' => 1, 'name' => 'Власюк Сергей Владимирович'),
            array('id' => 1, 'name' => 'Демиденко Юрий Сергеевич'),
            array('id' => 1, 'name' => 'Дёмин Мирослав Сергеевич'),
            array('id' => 1, 'name' => 'Домнин Виктор Геннадьевич'),
            array('id' => 1, 'name' => 'Евтеев Евгений Александрович'),
            array('id' => 1, 'name' => 'Козлов Владимир Валерьевич'),
            array('id' => 1, 'name' => 'Коледа Евгений Викторович'),
            array('id' => 1, 'name' => 'Крылов Семен Викторович'),
            array('id' => 1, 'name' => 'Кувшинов Михаил Алексеевич'),
            array('id' => 1, 'name' => 'Ладыгина Марина Владимировна'),
            array('id' => 1, 'name' => 'Репеха Михаил Николаевич'),
            array('id' => 1, 'name' => 'Сурков Андрей Сергеевич'),
            array('id' => 1, 'name' => 'Суходольский Артур Александрович'),
            array('id' => 1, 'name' => 'Улановский Филипп Александрович'),
            array('id' => 1, 'name' => 'Шедько Алексей Евгеньевич'),
            array('id' => 1, 'name' => 'Шедько Владимир Евгеньевич'),
            array('id' => 1, 'name' => 'Шейханов Сайпулла Гамзатович'),
            array('id' => 1, 'name' => 'Мокасов Гиви Гергоевич'),
            array('id' => 1, 'name' => 'Мишура Вадим Вадимович'),
            array('id' => 1, 'name' => 'Доцентов Кафедры Немаевич'),
            array('id' => 1, 'name' => 'Васисуалий Лоханкин Владленович'),
            array('id' => 1, 'name' => 'Войнович Марина Сидоровна'),
            array('id' => 1, 'name' => 'Этель Николай Николаевич'),
            array('id' => 1, 'name' => 'Лилианов Андрей Натанович'),
            array('id' => 1, 'name' => 'Борислав Артур Юсупович'),
            array('id' => 1, 'name' => 'Александрович Филлимон Уланович'),
            array('id' => 1, 'name' => 'Сибура Алексей Артамонович'),
            array('id' => 1, 'name' => 'Шевченто Тарас Евгеньевич'),
            array('id' => 1, 'name' => 'Цацко Тарас Бульбович'),
        );

        $i = 9000;
        foreach ($clients as &$client)
        {
            $client['id'] = ++$i;
        }

        return $clients;
    }

    public function get_array_part(&$arr, $offset, $length)
    {
        $res = array();

        $i = 0;
        $count = 0;
        foreach ($arr as $item)
        {
            if ($i++ >= $offset)
            {
                if ($count < $length)
                {   $count++;
                    $res[] = $item;
                }
                else
                {   break;
                }
            }
        }

        return $res;
    }

    public function multiexplode($delimiters,$data)
    {
        $make_ready = str_replace($delimiters, $delimiters[0], $data);
        return explode($delimiters[0], $make_ready);
    }

    //
    // Returns array per each custom fields it found.
    // When I refer custom field I mean data format like "в3-5, у2, м20" (возраст от 3 до 5 лет, уровень воторой (средний), максимум 10 человек).
     private function parse_schedule_comment(&$schedule_comment)
     {
        // make it lower-case first:
        $schedule_comment = mb_strtolower($schedule_comment);

        $parts = explode(',', $schedule_comment);
        if (FALSE === $parts)
        {   return null;
        }

        // let's trim all the strings:
        $parts = array_map('trim', $parts);

        // iterate by each part to find the corresponding section:
        $res = array();
        foreach ($parts as $part)
        {   // custom_age_from, custom_age_to, custom_skill_level, custom_max_count
            $first_char = mb_substr($part, 0, 1); // only the first char is a special char that we consider.
            $first_char_length = 1; // this stupid name just to show that it depends on the length of the symbol which is being analyzed in "switch()"!

            switch ($first_char)
            {   case 'в': // возраст от и до. Варианты записи: "в5" то же, что и "в5-" (которое то же самое, что и "в5+"), а также вариант "в5-12".
                    $numbers_part = mb_substr($part, $first_char_length, 22); // exclude the first code-symbol and watch for the (interval) numbers only.
                    $from_to = $this->multiexplode(array('-', '+'), $numbers_part); // numbers could be separated by either "-" or "+" signs.

                    if (1 == count($from_to))
                    {
                        if (floatval($from_to) > 0)
                        {   $res['custom_age_from'] = floatval($from_to);
                        }
                    } else if (2 == count($from_to))
                    {
                        if (floatval($from_to[0]) > 0)
                        {   $res['custom_age_from'] = floatval($from_to[0]);
                        }
                        if (floatval($from_to[1]) > 0)
                        {   $res['custom_age_to'] = floatval($from_to[1]);
                        }
                    }

                    break;

                case 'у': // уровень: 1, 2, 3  - от малого до продвинутого.
                    $res['custom_skill_level'] = intval(mb_substr($part, $first_char_length, 1)); // just one symbol denotes the level.
                    break;

                case 'м': // максимальное кол-во людей в группе
                    $res['custom_max_count'] = intval(mb_substr($part, $first_char_length, 2)); // two digits are more than enough for the max. people count!
                    break;
                default:
                    log_message('error', "XXX parse_schedule_comment() comment is UNDEFINED, first char: '$first_char'");
                    break;
            }
        }

        return $res;
     }

    // "Schedule" JSON contains also custom "comments" field which can be used for custom-fields parsing.
    // Those custom fields to be used when generating "ClubAdmin-view" of the Schedule: to provide an extended (!) info useful for Club Admins.
    public function parse_schedule_comments__to_extract_custom_fields($yurlico_id, $organization_id)
    {
        //----------------------------------------------------------------------------+
        // extract comments with custom format. Ignore the others (in terms of identifying custom fields!)
        $days_before   = 0;
        $days_after    = 7;

        $schedule_comments = $this->get_schedule_comments($yurlico_id, $organization_id, $days_before, $days_after);
        //log_message('error', "XXX SCHEDULE_COMMENTS: ".print_r($schedule_comments, true));
        $res = array();
        foreach ($schedule_comments as $schedule)
        {
            $set_of_fields = $this->parse_schedule_comment($schedule->comment);
            if ($set_of_fields)
            {   $res[$schedule->id] = $set_of_fields; // get the schedule_id (assuming we're operating ONLY for predefined yurlico_id & org_id) to update with custom fields.
            }
        }
        //----------------------------------------------------------------------------|

        //----------------------------------------------------------------------------+
        // Now let's update the custom fields: only the ones that parse_schedule_comment() considered VALID a step before!
        foreach ($res as $schedule_id => $set_of_fields)
        {   $this->db->where('id', $schedule_id);
            $this->db->set($set_of_fields); // it sets only those fields which were identified earlier by "parse_schedule_comment()".
            $this->db->update('berk_schedule');
        }
        //----------------------------------------------------------------------------|
    }

     // "Schedule" JSON contains also custom "comments" field which can be used for custom-fields parsing.
     // Those custom fields to be used when generating "ClubAdmin-view" of the Schedule: to provide an extended (!) info useful for Club Admins.
     private function get_schedule_comments($yurlico_id, $organization_id, $days_before, $days_after)
    {
/*  SELECT wt_berk_schedule.id, yurlico_id, organization_id, COMMENT, custom_age_from, custom_age_to, custom_skill_level, custom_max_count
    FROM `wt_berk_schedule`
    WHERE (yurlico_id = 37 AND organization_id = 5)
    AND comment != ''
    AND (planneddate BETWEEN CURRENT_DATE - INTERVAL '0' DAY AND CURRENT_DATE + INTERVAL '7' DAY) -- use the latest ones so we won't parse all the old staff!
 */

$sql = 'SELECT wt_berk_schedule.id, yurlico_id, organization_id, comment, custom_age_from, custom_age_to, custom_skill_level, custom_max_count
FROM wt_berk_schedule
WHERE  (yurlico_id = '.$yurlico_id.' AND organization_id = '.$organization_id.')
AND (comment != \'\')
AND  '.$this->get_sql_limit_date__relative2today('wt_berk_schedule', 'planneddate', $days_before, $days_after);

//        log_message('error', "XXX parse_schedule_comments sql =\n$sql\n");
        $query  = $this->db->query($sql);
//        log_message('error', "LAST SQL (get_clients_for_date($days_before, $days_after, $yurlico_id, $organization_id)) =\t".$this->utilitar_db->get_sql_last_run());

        return Utilitar_db::safe_resultSet($query);
    }

    //
    // Returns the list of active (non-removed, non-canceled) clients for trainings at days relative to the current one.
    // If both $days_before and $days_after are zero then it returns current day's data!
    public function get_clients_for_date($days_before, $days_after, $yurlico_id, $organization_id)
    {
/*
SELECT DISTINCT wt_berk_clients.id, CONCAT(wt_berk_clients.firstname, ' ', wt_berk_clients.lastname) AS NAME
FROM wt_berk_clients_visits, wt_berk_schedule, wt_berk_clients
WHERE wt_berk_clients_visits.schedule_id = wt_berk_schedule.id
AND wt_berk_clients_visits.client_id = wt_berk_clients.id
AND
( (wt_berk_schedule.removed IS NULL) OR (wt_berk_schedule.removed <= 0))
AND ((wt_berk_schedule.canceled IS NULL) OR (wt_berk_schedule.canceled <= 0))
AND
wt_berk_schedule.planneddate BETWEEN CURRENT_DATE AND CURRENT_DATE
*/
        $sql = 'SELECT DISTINCT wt_berk_clients.id, CONCAT(wt_berk_clients.firstname, \' \', wt_berk_clients.lastname) AS name
FROM wt_berk_clients
LEFT JOIN wt_berk_clients_visits ON wt_berk_clients_visits.client_id = wt_berk_clients.id
WHERE  (wt_berk_clients.yurlico_id = '.$yurlico_id.' AND wt_berk_clients.organization_id = '.$organization_id.')
AND (wt_berk_clients_visits.yurlico_id = '.$yurlico_id.' AND wt_berk_clients_visits.organization_id = '.$organization_id.')
AND wt_berk_clients_visits.schedule_id IN
    (
SELECT wt_berk_schedule.id
FROM wt_berk_schedule
WHERE 
(wt_berk_schedule.yurlico_id = '.$yurlico_id.' AND wt_berk_schedule.organization_id = '.$organization_id.')
AND
( ( (`wt_berk_schedule`.`removed` = 0) OR (`wt_berk_schedule`.`removed` IS NULL) )
AND ( (`wt_berk_schedule`.`canceled` = 0) OR (`wt_berk_schedule`.`canceled` IS NULL) ) 
)
AND  '.$this->get_sql_limit_date__relative2today('wt_berk_schedule', 'planneddate', $days_before, $days_after).'
    )';

//        log_message('error', "issue sql =\n$sql\n");
        $query  = $this->db->query($sql);
//        log_message('error', "LAST SQL (get_clients_for_date($days_before, $days_after, $yurlico_id, $organization_id)) =\t".$this->utilitar_db->get_sql_last_run());

        return Utilitar_db::safe_resultSet($query);
    }

    public function get_family_by_mkey($mkey)
    {
        return $this->CI->utilitar_db->_getTableRow(Util_berkana_data::TBL_FAMILIES, 'mkey', $mkey);
    }

    //
    // A lighter replacemenf of "->get_clients_for_date()": to be called based on results returned by "->get_planned_visits_for_date()".
    // Created to avoid complex date & other conditions filtering which have been ALREADY MADE by preceding (as ASSUMED) call of "->get_planned_visits_for_date()".
    //
    public function get_clients_by_id_shortinfo(&$clients_id_array, $yurlico_id, $organization_id)
    {
        if (count($clients_id_array) <= 0)
        {   return array();
        }
        $sql =  ' SELECT DISTINCT wt_berk_clients.id, wt_berk_clients.firstname AS fn, wt_berk_clients.lastname AS ln, CONCAT(wt_berk_clients.firstname, \' \', wt_berk_clients.lastname) AS name ';
        $sql .= ' FROM wt_berk_clients ';
        $sql .= ' WHERE id IN ('.implode(",", $clients_id_array).')';
        $sql .= ' AND wt_berk_clients.yurlico_id='.$yurlico_id.' AND wt_berk_clients.organization_id='.$organization_id.' ';

        $query  = $this->db->query($sql);
        return Utilitar_db::safe_resultSet($query);
    }

    //
    // Renames title to name: for mobile client (TODO: change mobile client namings indeed!)
    //
    public function getRooms()
    {
        $this->db->select("berk_rooms.id, berk_rooms.title as name");
        $this->db->order_by('berk_rooms.title');
        $this->dbfilter_yurlico_and_organization('berk_rooms'); // NOTE: this call is mandatory to be present when accessing berkana assests!

        $query = $this->db->get('berk_rooms');

        return Utilitar_db::safe_resultSet($query);
    }

    //
    // Renames title to name: for mobile client (TODO: change mobile client namings indeed!)
    //
    public function getRooms_ext($yurlico_id, $organization_id)
    {
        $this->db->select("berk_rooms.id, berk_rooms.title as name");
        $this->db->order_by('berk_rooms.title ASC');

        $this->dbfilter_yurlico_and_organization_multipletables(Util_berkana_data::TBL_ROOMS, $yurlico_id, $organization_id); // NOTE: this call is mandatory to be present when accessing berkana assests!

        $query = $this->db->get('berk_rooms');

        return Utilitar_db::safe_resultSet($query);
    }

    // removes patronymic for shorter presentation on mobile client:
    public function getTrainersShort_ext($yurlico_id, $organization_id, $active_only, $trainers_IDs = NULL)
    {
        $trainers = $this->getRemoteTrainers_ext($yurlico_id, $organization_id, false, $active_only, $trainers_IDs);

        $this->shortenNames($trainers);

        return $trainers;
    }

    // removes patronymic for shorter presentation on mobile client:
    public function getTrainersShort($active_only, $trainers_IDs = NULL)
    {
        $trainers = $this->getRemoteTrainers(false, $active_only, $trainers_IDs);

        $this->shortenNames($trainers);

        return $trainers;
    }


    // shortens human names. Mandatory field-name
    public function shortenNames(&$rows, $name_column = 'name')
    {
        foreach ($rows as $row)
        {   $row->$name_column = $this->reduce_FIO($row->$name_column);
        }
    }

    // Removes patronymic from "$str_FIO" and leaves fistname, lastname only.
    public function reduce_FIO($str_FIO)
    {
        $res = null;

        $arr = explode(' ', $str_FIO);
        if (count($arr) >= 2)
        {   $res = $arr[1].' '.$arr[0]; // firstname, lastname.
        }
        else if (1 === count($arr))
        {   $res = $arr[0]; // fisrtname only.
        }
        else
        {   $res = 'n/a';
        }

//        log_message('error', "reduce_FIO('".$str_FIO."') => ".$res);
        return $res;
    }

    public function get_user_by_id_full_SECURED($user_id)
    {
        if (Util_berkana_data::SESSIONKEY__USER_ID_ANONYMOUS == $user_id)
        {
            // emulate user full info for ANONYMOUS user!!!
            $v = new stdClass();

            $v->id = Util_berkana_data::SESSIONKEY__USER_ID_ANONYMOUS;
            $v->username = 'Anonymous_user';
            $v->email = '';
            $v->activated = true;
            $v->banned = false;
            $v->name = 'Anonymous';
            $v->yurlico_id = Util_berkana_data::SESSIONKEY__YURLICO_ID_ANONYMOUS;
            $v->organization_id = Util_berkana_data::SESSIONKEY__ORGANIZATION_ID_ANONYMOUS;
            $v->phone = '';
            $v->description = 'this is anonymous user';

            return $v;
        }
        else
        {   return $this->users->get_user_by_id_full($user_id);;
        }
    }

    private function get_current_user_yurlico_and_organization()
    {
        $user_id = $this->utilitar->get_current_user_id();
        if ($user_id <= 0)
        {   return NULL;
        }

        $this->db->select('users.id, users.username, users.email, users.activated, users.banned, user_profiles.name, user_profiles.yurlico_id, user_profiles.organization_id, user_profiles.phone, user_profiles.description');
        $this->db->from('users');
        $this->db->join('user_profiles', 'user_profiles.user_id = users.id', 'left');
        $this->db->where('users.id', $user_id);

        $query = $this->db->get();
        return Utilitar_db::safe_resultRow($query);
                        
//        return $this->get_user_by_id_full_SECURED($user_id);
    }

    // Returns true only if "$target_organization_id" belongs to current user (NO ROLE check though!) who is _in the same yurlico_.
    public function is_current_user_eligible_to_change_settings_of($test_organization_id)
    {
        $current_yurlico_id = $this->session_get_yurlico_id();
        return $this->organization_belongs_to_yurlico($current_yurlico_id, $test_organization_id);
    }

    public function session_get_yurlico_id()
    {   //return $this->CI->session->userdata(Util_berkana_data::SESSIONKEY__YURLICO_ID);
        $res = $this->get_current_user_yurlico_and_organization();
        return ($res ? $res->yurlico_id : -1);
    }

    public function session_get_organization_id()
    {   //return $this->CI->session->userdata(Util_berkana_data::SESSIONKEY__ORGANIZATION_ID);
        $res = $this->get_current_user_yurlico_and_organization();
        return ($res ? $res->organization_id : -1);
    }

    // returns current "$did" string or FALSE.
    public function session_get_current_device_id()
    {
        return $this->session->userdata('did'); // put during Json_base->login() only!
    }


    public function _set_session_yurlico_and_organization_id_by_userid($user_id, $organization_id = null)
    {
        // NOTE: if this is NOT the MOBILE ANONYMOUS USER and is not authorized,
        //       then just return (note we have already reset the Json_base::get_current_yurlico_id() and Json_base::get_current_organization_id()):
        if (($user_id < 0) && (Util_berkana_data::SESSIONKEY__USER_ID_ANONYMOUS !== $user_id))
        {   log_message('error', "SSYID(user_id:$user_id): NOT SETTING any SESSION yurlico_id, org_id, because user is not anonymous and is invalid!!!");
            return false; // this appears to be an Anonymous website visitor.
        }

        // NOTE: you can determine $yurlico_id by $organization_id.
        if ( ($organization_id > 0) && (Util_berkana_data::SESSIONKEY__USER_ID_ANONYMOUS == $user_id) )
        {   $yurlico = $this->getYurlicoByOrganizationId($organization_id);
            if ($yurlico)
            {   Json_base::set_current_yurlico_and_org_id($yurlico->id, $organization_id);
                log_message('error', "SSYID: success setting yurlico and organization ($yurlico->id, $organization_id)");
                return true;
            }
        }

        // retrieve "yurlico_id" or discard if none available:
        $db__user_data  = $this->util_berkana_data->db__get_user_yurlico_and_organization_id($user_id);
        if (is_object($db__user_data))
        {
            Json_base::set_current_yurlico_and_org_id($db__user_data->yurlico_id, $db__user_data->organization_id);

           /*$this->session->set_userdata( array(Util_berkana_data::SESSIONKEY__YURLICO_ID      => $db__user_data->yurlico_id,
                                               Util_berkana_data::SESSIONKEY__ORGANIZATION_ID => $db__user_data->organization_id
                                              )
                                         );*/
            log_message('error', 'success: _set_session_yurlico_and_organization_id(uid:'.$user_id.', y:'.$db__user_data->yurlico_id.', org_id:'.$db__user_data->organization_id.')');
            return true;
        }
        else
        {   log_message('error', '_set_session_yurlico_and_organization_id(): WARNING no DB data (y, org_id) retrieved for user_id='.$user_id);
            return false; // no data for session. Notify about that.
        }
    }

    // NOTE: due to current login-issues: print both places: SESSION's yurlico/org_id and Json_base yurlico/org_id: potentially they might differt -> thus bringing login-issues.
    public function logSessionYurlicoAndOrganization($title = '')
    {
        $user_id = $this->utilitar->get_current_user_id();
        $berkana_user_profile = $this->util_berkana_data->get_berkana_user_profile($user_id);
        if ($berkana_user_profile)
        {   log_message('error', 'SESSION ('.$title.') "'.$berkana_user_profile->organization_name.'", '
                    .': y:'.$berkana_user_profile->yurlico_id.', org_id:'.$berkana_user_profile->organization_id
                    ."\n".'JSON_BASE SESSION: '.Json_base::get_current_yurlico_id__org_id_string()
                    ."\n".Json_base::get_http_connection_ip_and_user_agent_string());
        }
        else
        {   log_message('error', 'SESSION ('.$title.'): NO berkana profile available for current user.'
                    ."\n".'JSON_BASE SESSION: '.Json_base::get_current_yurlico_id__org_id_string()
                    ."\n".Json_base::get_http_connection_ip_and_user_agent_string()
                    );
        }
    }

    public function setMessagingAllowedForYurlicoAllOrganizations($yurlico_id, $messaging_allowed)
    {
        $this->db->where('yurlico_id', $yurlico_id);
        $this->db->update('berk_organizations', array('messaging_allowed' => $messaging_allowed));
    }

    public function isMessagingAllowedForYurlicoAllOrganizations($yurlico_id)
    {
        $res = $this->CI->utilitar_db->_getTableRow('berk_organizations', 'yurlico_id', $yurlico_id);
        return ($res && $res->messaging_allowed);
    }

    // verifies not the DB but session!! BEware of mistakes.. i know this might be error-prune ;(
    public function DISABLED_session__is_admin()
    {
    return false;
/*
        $var = $this->CI->session->userdata('is_admin_session');
        // log_message('error', "session__is_admin() = ".print_r($var, true));;

        return (true == $var);
*/
    }

    //
    // Ensures that two custom tables will be filtered not by FK-field-name. but by original field-name(s).
    private function db_careful_filter($table_name, $yurlico_id, $organization_id)
    {
        switch ($table_name)
        {
            case 'berk_organizations':
                $this->db->where($table_name.'.id', $organization_id); // note the shortened filed name (id)
                $this->db->where($table_name.'.yurlico_id', $yurlico_id);
                break;

            case 'berk_yurlica':
                if ($organization_id > 0)
                {   // theoretically we are allowing getting infor for all the organizations of yurlico:
                    $this->db->where($table_name.'.organization_id', $organization_id);
                }
                $this->db->where($table_name.'.id', $yurlico_id); // note the shortened filed name (id)
                break;

            default:
                if ($yurlico_id > 0 && $organization_id > 0) // backward compatibility?
                {   $this->db->where(array($table_name.'.yurlico_id' => $yurlico_id, $table_name.'.organization_id' => $organization_id));
                }
                else
                {   $this->db->where($table_name.'.yurlico_id', $yurlico_id);
                }

                break;
        }
    }

    // returns null or assoc. array with two fields.
    public function get_yurlico_and_org_id__for_current_user()
    {
        $user_id    = $this->utilitar->get_current_user_id();
        if ($user_id <= 0)
        {   return null;
        }

        $berkana_user_profile = $this->util_berkana_data->get_berkana_user_profile($user_id);
        if (!$berkana_user_profile)
        {   return null;
        }

        return array('yurlico_id' => $berkana_user_profile->yurlico_id, 'organization_id' => $berkana_user_profile->organization_id);
    }

    //
    // Accepts either single table-name or array of table names.
    //
    public function dbfilter_yurlico_and_organization($tables_names)
    {
        $yurlico_id         = Json_base::get_current_yurlico_id();
        $organization_id    = Json_base::get_current_organization_id();

        // TODO: this is WRONG for website logins of club owners!!! There is "0 == $organization_id"!!! :((((
        if ($yurlico_id <=0 || $organization_id <= 0)
        {
            $user_id = $this->utilitar->get_current_user_id();
            $res = $this->get_yurlico_and_org_id__for_current_user();
            if (!$res)
            {   log_message('error', "WARNING!!! dbfilter_yurlico_and_organization(uid:$user_id): no yurlico and org_id exist in session!\nTable(s): ".print_r($tables_names, true)."\n".Json_base::get_http_connection_ip_and_user_agent_string());
            }
            else
            {   $organization_id= $res['organization_id'];
                $yurlico_id     = $res['yurlico_id'];
                log_message('error', "WARNING: dbfilter_yurlico_and_organization() ENFORCED yurlico and org_id by uid:$user_id, y:$yurlico_id, org_id:$organization_id\nTable(s): ".print_r($tables_names, true)."\n".Json_base::get_http_connection_ip_and_user_agent_string());

                Json_base::set_current_yurlico_and_org_id($yurlico_id, $organization_id);
            }
        }
        
        $this->dbfilter_yurlico_and_organization_multipletables($tables_names, $yurlico_id, $organization_id);
    }

    //
    //  Accepts either single table-name or array of table names.
    //  NOTE: This is an optimized version that uses input parameters instead of checking session values each time.
    //
    public function dbfilter_yurlico_and_organization_multipletables($tables_names, $yurlico_id, $organization_id)
    {
/*
        if (true == $this->session__is_admin())
        {   // NOTE: admin should see all the data! No filtering is needed.
            return;
        }
*/

        if (is_string($tables_names))
        {   $this->db_careful_filter($tables_names, $yurlico_id, $organization_id);
        }
        else if (is_array($tables_names))
        {
            foreach ($tables_names as $single_table)
            {
                if (is_string($single_table))
                {   $this->db_careful_filter($single_table, $yurlico_id, $organization_id);
                }
            }
        }
    }


    //
    //  Assumed that checking of true == session__is_admin() is done BEFORE calling this function!
    //
    // Returns a string to be inserted into SQL-requests.
    //  NOTE: you must mention TABLE_NAME with dot right before writing this method's return value:
    //      e.g. "SELECT * FROM table1 WHERE wt_berk_SOME_TABLE."$this->get_organization_id_field_name_value().
    //
    public function get_organization_id_field_name_value()
    {
        return 'organization_id='.$this->session_get_organization_id().' ';
    }

    //
    // Also modifies "$insert_items" input data: by adding columns "yurlico_id" and "organization_id".
    //
    public function insertJsonRecords(&$insert_items, $yurlico_id, $organization_id)
    {
//        log_message('error', "INSERT ITEMS INPUT:\n".print_r($insert_items, true));

        foreach ($insert_items as $berkana_table_name => $json_rows)
        {
            if (0 == count($json_rows))
            {   continue; // no data to proceed.
            }

            log_message('error', "insertJsonRecords(): berkana_table_name (INSERT) = $berkana_table_name");
            foreach ($json_rows as $row)
            {
//                log_message('error', "\tINSERT ITEM:\n".print_r($row, true));
                $this->protectedInsert($berkana_table_name, $row, $yurlico_id, $organization_id);
            }
        }
    }
    

    //
    // Function modifies input data by adding two fields into the result set: yurlico_id, $organization_id.
    //
    public function protectedInsert($table_name, $data, $yurlico_id, $organization_id)
    {
        //  NOTE: this addendum is *mandatory* to be present when creating Berkana assests!
        //  Berkana DB also could supply those fields but they make no sense usually (in 99.9% cases).
        $data['yurlico_id']         = $yurlico_id;
        $data['organization_id']    = $organization_id;

//        log_message('error', "protectedInsert('$table_name'): ".print_r($data, true));

        if (Util_berkana_data::TBL_ABONEMENTS == $table_name)
        {   // convert timestamps *MANUALLY*:
            $data['date_start'] = 'FROM_UNIXTIME('.$data['date_start'].')';
            $data['date_end']   = 'FROM_UNIXTIME('.$data['date_end'].')';

            // ensure to extract these *after* data modification:
            $field_names    = array_keys($data);
            $field_values   = array_values($data);

//            log_message('error', " field_names =".print_r($field_names, true)."\n  field_values=".print_r($field_values, true));

            $sql = "INSERT INTO wt_berk_abonements (".implode(',', $field_names).") VALUES (".implode(',', $field_values).")";
            $query = $this->db->query($sql);
            if (!$query)
            {   log_message('error', "(!) ERROR inserting TBL_ABONEMENTS manually. SQL=".$sql);
            }
        }
        else
        {   $this->db->insert($table_name, $data);
        }
    }

    //
    // Function ensures filtering by juridical id and organization_id!
    //  NOTE:   USUALLY both yurlico_id and organization_id are either nonsense in Berkana DB or invalid.
    //          Instead use currently availavle data rather than the two fields which came from Berkana DB.
    //
    public function protectedUpdate($table_name, $data, $yurlico_id, $organization_id)
    {
        // log_message('error', "protectedUpdate('$table_name') yurlico_id=$yurlico_id, org_id=$organization_id: , BEFORE MODIFICATION: ".print_r($data, true));

        // manual SQL creation for Abonements :((( !!!
        if ((0 == strcmp(Util_berkana_data::TBL_ABONEMENTS, $table_name)) || (0 == strcmp('wt_'.Util_berkana_data::TBL_ABONEMENTS, $table_name)))
        {   // since we're here composing MANUAL SQL UPDATE, then calling "dbfilter_yurlico_and_organization()" is prohibited here!!!

            //
            // ??? greco: A QUESTION: before calling this function we called "$this->db->where('id', $row->id)". Won't it affect (in a negative way!) this RAW SQL instead???
            //

            // ensure to not to overwrite existing organization_id and yurlico_id (the filtering above already ensures correct rows to be updated!):
            unset($data['yurlico_id']);
            unset($data['organization_id']);

            // convert timestamps *MANUALLY*:
            $data['date_start'] = 'FROM_UNIXTIME('.$data['date_start'].')';
            $data['date_end']   = 'FROM_UNIXTIME('.$data['date_end'].')';

            $sql = '';
            foreach ($data as $field_name => $field_value)
            {
                if (0 == strcmp('id', $field_name))
                {   continue; // skip assigning "id" (main) field.
                }

                if (0 == strcmp('date_start', $field_name) ||
                    0 == strcmp('date_end', $field_name)
                   )
                   {    $sql .= $field_name."=".$field_value.","; // do not wrap the value by ' symbol for formulas!
                   }
                   else
                   {    $sql .= $field_name."='".$field_value."',";
                   }
            }

            if (strlen($sql) > 0 )
            {   $sql = substr($sql, 0, -1); // remove trailing separator.
            }

            $sql = 'UPDATE wt_berk_abonements SET '.$sql.' WHERE id='.$data['id'].' AND yurlico_id='.$yurlico_id.' AND organization_id='.$organization_id.'  ';

            log_message('error', "\tPROTECTED UPDATE ITEM (table=$table_name):\n".print_r($data, true)."\nSQL:\n".$sql);

            $query = $this->db->query($sql);
            if (!$query)
            {   log_message('error', "error updating TBL_ABONEMENTS manually! SQL=".$sql);
            }
        }
        else
        {
            $this->dbfilter_yurlico_and_organization_multipletables($table_name, $yurlico_id, $organization_id); // NOTE: this call is mandatory to be present when accessing berkana assests!

            // ensure to not to overwrite existing organization_id and yurlico_id (the filtering above already ensures correct rows to be updated!):
            unset($data['yurlico_id']);
            unset($data['organization_id']);

            $this->db->update($table_name, $data);
        }
    }

    public function updateJsonRecords(&$update_items, $yurlico_id, $organization_id)
    {
        foreach ($update_items as $berkana_table_name => $json_rows)
        {
            if (0 == count($json_rows))
            {   continue; // no data to proceed.
            }

            log_message('error', "updateJsonRecords(): berkana_table_name (UPDATE) = $berkana_table_name");
            foreach ($json_rows as $row)
            {
//                log_message('error', "\tUPDATE ITEM:\n".print_r($row, true));
                $this->db->where('id', $row['id']);
                $this->protectedUpdate($berkana_table_name, $row, $yurlico_id, $organization_id);
            }
        }
    }

    public function getRemoteTrainers($bAll_fields, $active_only, $trainers_IDs = NULL)
    {
        $this->db->select(($bAll_fields ?   "berk_trainers.*, users.email, users.banned" :
                                            "berk_trainers.id, berk_trainers.name")) // or use shorter version, when needed.
                        ;

        $this->dbfilter_yurlico_and_organization('berk_trainers'); // NOTE: this call is mandatory to be present when accessing berkana assests!
//$this->db->where('berk_trainers'.'.yurlico_id', $this->session_get_yurlico_id());

        $this->db->join('users', "users.id = berk_trainers.local_user_id", 'left');
        $this->db->order_by('berk_trainers.name');

        if ($active_only)
        {   $this->_dbNotRemoved('wt_berk_trainers');
        }

        if ($trainers_IDs && is_array($trainers_IDs) && (count($trainers_IDs) > 0))
        {   $this->db->where_in('berk_trainers.id', $trainers_IDs);
        }
        
        $query = $this->db->get('berk_trainers');

//        log_message('error', 'ZZZ LAST SQL (getRemoteTrainers) = '.$this->utilitar_db->get_sql_last_run());
        $res = Utilitar_db::safe_resultSet($query);;
        return $res;
	}


    public function getRemoteTrainers_ext($yurlico_id, $organization_id, $bAll_fields, $active_only, $trainers_IDs = NULL)
    {
        $this->db->select(($bAll_fields ?   "berk_trainers.*, users.email, users.banned" :
                                            "berk_trainers.id, berk_trainers.name")) // or use shorter version, when needed.
                        ;

        $this->dbfilter_yurlico_and_organization_multipletables('berk_trainers', $yurlico_id, $organization_id); // NOTE: this call is mandatory to be present when accessing berkana assests!

        $this->db->join('users', "users.id = berk_trainers.local_user_id", 'left');
        $this->db->order_by('berk_trainers.name');

        if ($active_only)
        {   $this->_dbNotRemoved('wt_berk_trainers');
        }

        if ($trainers_IDs && is_array($trainers_IDs) && (count($trainers_IDs) > 0))
        {   $this->db->where_in('berk_trainers.id', $trainers_IDs);
        }

        $query = $this->db->get('berk_trainers');

//        log_message('error', 'ZZZ LAST SQL (getRemoteTrainers) = '.$this->utilitar_db->get_sql_last_run());
        $res = Utilitar_db::safe_resultSet($query);;
        return $res;
	}

    public function get_berkana_trainer($trainer_id, $yurlico_id, $organization_id)
    {
        $this->dbfilter_yurlico_and_organization_multipletables('berk_trainers', $yurlico_id, $organization_id); // NOTE: this call is mandatory to be present when accessing berkana assests!
        $this->db->where('berk_trainers.id', $trainer_id);
        $query = $this->db->get('berk_trainers');
        return Utilitar_db::safe_resultRow($query);;
    }

    // NOTE: days before/after compared to TODAY
    public function get_visits_discrepancies($yurlico_id, $organization_id, $days_before, $days_after)
    {
    /*
        SELECT wt_berk_clients.firstname, wt_berk_clients.lastname, wt_berk_schedule.planneddate, wt_berk_schedule.plannedstarttime, wt_berk_clients_visits.*, wt_berk_schedule.teacher_id, wt_berk_schedule.coursetype_id
        FROM wt_berk_clients_visits
        LEFT JOIN wt_berk_clients ON wt_berk_clients_visits.client_id = wt_berk_clients.id
        LEFT JOIN wt_berk_schedule ON wt_berk_clients_visits.schedule_id = wt_berk_schedule.id
        WHERE wt_berk_clients_visits.yurlico_id & organization_id = 63 AND wt_berk_clients.yurlico_id & organization_id = 63 AND wt_berk_schedule.yurlico_id & organization_id = 63
        AND (wt_berk_clients_visits.removed IS NULL OR wt_berk_clients_visits.removed = 0)
        AND (wt_berk_schedule.removed IS NULL OR wt_berk_schedule.removed = 0)
        AND   wt_berk_schedule.planneddate BETWEEN (CURRENT_DATE -INTERVAL '5' DAY) AND CURRENT_DATE
        AND (
              (wt_berk_clients_visits.visited <> wt_berk_clients_visits.mobileVisited) OR
              (wt_berk_clients_visits.version <> wt_berk_clients_visits.mobileVersion)
            )
        ORDER BY wt_berk_schedule.planneddate, wt_berk_schedule.plannedstarttime
     */
    
            $this->db->select('berk_clients.firstname, berk_clients.lastname, berk_schedule.planneddate, berk_schedule.plannedstarttime, berk_clients_visits.*, berk_schedule.teacher_id, berk_schedule.coursetype_id');
            $this->db->join('berk_clients', 'berk_clients_visits.client_id = berk_clients.id', 'left');
            $this->db->join('berk_schedule', 'berk_clients_visits.schedule_id = berk_schedule.id', 'left');
    
            $this->db->from('berk_clients_visits');

            $db_prefix = $this->db->dbprefix;
            $this->dbfilter_yurlico_and_organization_multipletables(array('berk_clients_visits', 'berk_clients', 'berk_schedule'), $yurlico_id, $organization_id); // NOTE: this call is mandatory to be present when accessing berkana assests!

            $this->db->where('('.$db_prefix.'berk_clients_visits.removed IS NULL OR '.$db_prefix.'berk_clients_visits.removed = 0)', NULL, FALSE);
            $this->db->where('('.$db_prefix.'berk_schedule.removed IS NULL OR '.$db_prefix.'berk_schedule.removed = 0)', NULL, FALSE);

            $interval_dayafter = (0 == $days_after) ? 'CURRENT_DATE' : "(CURRENT_DATE +INTERVAL '$days_after' DAY)";
            $this->db->where($db_prefix."berk_schedule.planneddate BETWEEN (CURRENT_DATE -INTERVAL '$days_before' DAY) AND $interval_dayafter", NULL, FALSE);
            $this->db->where('( ('.$db_prefix.'berk_clients_visits.visited <> '.$db_prefix.'berk_clients_visits.mobileVisited) OR ('.$db_prefix.'berk_clients_visits.version <> '.$db_prefix.'berk_clients_visits.mobileVersion) )', NULL, FALSE);

            $this->db->order_by('berk_schedule.planneddate, berk_schedule.plannedstarttime');

            $query = $this->db->get();
    
            return Utilitar_db::safe_resultSet($query);
    }

    // NOTE: returns also a flag whether that ticket has a logged-in user on not.
    public function get_organization_tickets_for_role($role_id, $yurlico_id, $organization_id, $bot_name)
    {
/*
    SELECT wt_tg_tickets.id, wt_tg_tickets.ticket_owner_name, wt_tg_tickets.extra_data, wt_tg_tickets.role, wt_tg_tickets.messaging_id, wt_tg_tickets.created AS ticket_created, 
    wt_tg_users.tg_user_id, wt_tg_users.logged_in, wt_tg_users.created AS user_created
    FROM wt_tg_tickets
    LEFT OUTER JOIN wt_tg_users ON wt_tg_tickets.id = wt_tg_users.ticket_id
    WHERE wt_tg_tickets.bot_name = 'WallTouch_Teacher_bot'
    AND wt_tg_tickets.role = 1 -- teachers only!
    AND (wt_tg_tickets.yurlico_id = 92 AND wt_tg_tickets.organization_id = 62)
    AND locate('(wt testing)', wt_tg_tickets.ticket_owner_name) = 0  -- exclude my testing tickets!!
    ORDER BY wt_tg_tickets.ticket_owner_name
  
 */
        //$where_array = array('tg_tickets.bot_name' => $bot_name, 'tg_tickets.yurlico_id' => $yurlico_id, 'tg_tickets.organization_id' => $organization_id);

        // DEBUGGING-ONLY! I put explicitly the bot_name which is WRONG!
        $where_array = array('tg_tickets.bot_name' => 'WallTouch_Teacher_bot', 'tg_tickets.yurlico_id' => $yurlico_id, 'tg_tickets.organization_id' => $organization_id);
        if ($role_id > 0)
        {   $where_array['tg_tickets.role'] = $role_id;
        }

        $this->db->select('tg_tickets.id, tg_tickets.ticket_owner_name, tg_tickets.extra_data, tg_tickets.role, tg_tickets.messaging_id, tg_tickets.created AS ticket_created,
        tg_users.tg_user_id, tg_users.logged_in, tg_users.created AS user_created');
        $this->db->join('tg_users', 'tg_tickets.id = tg_users.ticket_id', 'left');

        $this->db->from('tg_tickets');
        $this->db->where($where_array);
        $this->db->where("locate('(wt testing)', ".$this->db->dbprefix.'tg_tickets.ticket_owner_name) = 0', null, false); // exclude my testing tickets!!
        $this->db->order_by('tg_tickets.ticket_owner_name');

        //log_message('error', "SQL = ".print_r($this->db->get_compiled_select('', false), true));
        $query = $this->db->get();

        return Utilitar_db::safe_resultSet($query);
    }

    public function get_berkana_trainers_quick($yurlico_id, $organization_id, $b_active_only)
    {
        $this->dbfilter_yurlico_and_organization_multipletables('berk_trainers', $yurlico_id, $organization_id); // NOTE: this call is mandatory to be present when accessing berkana assests!
        $this->db->where(array('yurlico_id' => $yurlico_id, 'organization_id' => $organization_id));
        $this->db->order_by('berk_trainers.name');

        if ($b_active_only)
        {
            $this->_dbNotRemoved('wt_berk_trainers');
            $query = $this->db->get('berk_trainers');
            return Utilitar_db::safe_resultSet($query);;
        }
        else
        {   return $this->utilitar_db->_getTableContents('berk_trainers', 'name', 'yurlico_id', $yurlico_id, 'organization_id', $organization_id);
        }
    }

    //returns trainers along with their timezone (needed to wake them up).
    public function get_berkana_trainers($yurlico_id, $organization_id, $b_active_only, $trainer_id = NULL)
    {
/*
    SELECT wt_berk_trainers.*, wt_berk_cities.timezone_name
    FROM wt_berk_trainers
    LEFT JOIN wt_berk_organizations ON wt_berk_organizations.id = wt_berk_trainers.organization_id
    LEFT JOIN wt_berk_yurlica ON wt_berk_yurlica.id = wt_berk_organizations.yurlico_id
    LEFT JOIN wt_berk_cities ON wt_berk_cities.id = wt_berk_organizations.city_id
    WHERE (wt_berk_trainers.yurlico_id = 93 AND wt_berk_trainers.organization_id = 63)
    AND (wt_berk_organizations.yurlico_id = 93 AND wt_berk_organizations.id = 63)
    AND wt_berk_trainers.removed IS NULL
    ORDER BY wt_berk_trainers.name
 */
        $this->db->select('berk_trainers.*, berk_cities.timezone_name');

        $this->dbfilter_yurlico_and_organization_multipletables('berk_trainers', $yurlico_id, $organization_id); // NOTE: this call is mandatory to be present when accessing berkana assests!

        $this->db->join('berk_organizations', 'berk_organizations.id = berk_trainers.organization_id', 'left');
        $this->db->join('berk_yurlica', 'berk_yurlica.id = berk_organizations.yurlico_id', 'left');
        $this->db->join('berk_cities', 'berk_cities.id = berk_organizations.city_id', 'left');

        $this->db->order_by('berk_trainers.name');

        if ($b_active_only)
        {   $this->_dbNotRemoved('wt_berk_trainers');
        }

        if ($trainer_id > 0)
        {   $this->db->where('berk_trainers.id', $trainer_id);
        }

        $query = $this->db->get('berk_trainers');

//        log_message('error', 'ZZZ LAST SQL (getRemoteTrainers) = '.$this->utilitar_db->get_sql_last_run());
        $res = Utilitar_db::safe_resultSet($query);;
        return $res;
	}

    public function getRemoteTrainerByRemoteId($remote_id, $yurlico_id, $organization_id)
    {
        $this->dbfilter_yurlico_and_organization_multipletables('berk_trainers', $yurlico_id, $organization_id); // NOTE: this call is mandatory to be present when accessing berkana assests!
        return $this->CI->utilitar_db->_getTableRow('berk_trainers', 'id', $remote_id);
    }

    public function getRemoteTrainerByLocalUserId($local_user_id)
    {
        return $this->CI->utilitar_db->_getTableRow('berk_trainers', 'local_user_id', $local_user_id);
    }

    public function getOrganizations()
    {
        return $this->utilitar_db->_getTableContents('berk_organizations', 'name');
    }

    public function getOrganizationsByYurlico($yurlico_id)
    {
        return $this->utilitar_db->_getTableContents('berk_organizations', 'name', 'yurlico_id', $yurlico_id);
    }

    // returns boolean.
    public function organization_belongs_to_yurlico($yurlico_id, $test_organization_id)
    {
        $organization = $this->getOrganization($test_organization_id);
        if (!$organization)
        {   return false;
        }

        return ($yurlico_id == $organization->yurlico_id);
    }

    public function getOrganization($organization_id)
    {
        return $this->CI->utilitar_db->_getTableRow('berk_organizations', 'id', $organization_id);
    }


    // Note: returns user accounts disregarding if requested organization exists or not.
    public function getOrganizationUserAccounts($organization_id, $role = null)
    {
/*SELECT wt_users.id AS user_id, wt_user_groups.id AS role_id, wt_user_profiles.yurlico_id, wt_user_profiles.organization_id
    FROM wt_users
LEFT JOIN wt_users_groups_link ON wt_users_groups_link.userId = wt_users.id
LEFT JOIN wt_user_groups ON wt_user_groups.id = wt_users_groups_link.groupId
LEFT JOIN wt_user_profiles ON wt_user_profiles.user_id = wt_users.id

WHERE wt_user_profiles.yurlico_id = 27 AND
wt_user_profiles.organization_id = 3
ORDER BY wt_user_profiles.organization_id, wt_user_groups.id, wt_users.id
*/
        log_message('error', "getOrganizationUserAccounts($organization_id, $role)");
        $this->db->select("users.id AS user_id, users.username, users.last_login, user_groups.id AS role_id, user_profiles.yurlico_id, user_profiles.organization_id");
        
        $this->db->join('users_groups_link', 'users_groups_link.userId = users.id', 'left');
        $this->db->join('user_groups', 'user_groups.id = users_groups_link.groupId', 'left');
        $this->db->join('user_profiles', 'user_profiles.user_id = users.id', 'left');

        if ($organization_id > 0)
        {   $this->db->where('user_profiles.organization_id', $organization_id);
        }

        if (null != $role)
        {   $this->db->where('user_groups.group_type', Utilitar::GROUP_TYPE__ROLES);
            $this->db->where('user_groups.name', $role);
        }

        $this->db->order_by('user_profiles.organization_id, user_groups.id, users.id');
        $query = $this->db->get('users');

        return Utilitar_db::safe_resultSet($query);
    }


    public function get_local_trainers()
    {
        return $this->CI->users->get_users_with_roles_full(SecuredMatrix::ROLE_TRAINER);
    }

    // gets common info only:
    public function getOrganizationCabinets($organization_id)
    {
        $CI = &get_instance();
        $CI->load->model('tank_auth/users');

        // TEMPORARY SOLUTION: return ALL users (not only the ones that are CABINETS from particular ORGANIZATION):
        return $CI->users->get_users_with_roles_full(SecuredMatrix::ROLE_CABINET);

/*
        $this->db->select('users.id, users.username, users.activated, users.banned, berk_user_profiles.organization_id, berk_user_profiles.room_id ');
        $this->db->from('users');
        $this->db->join('berk_user_profiles', 'berk_account_settings.local_user_id = users.id', 'left');
        $this->db->where('users.id', $organization_id);
$this->dbfilter_yurlico_and_organization('berk_user_profiles'); // NOTE: this call is mandatory to be present when accessing berkana assests!
        $query = $this->db->get();

        return Utilitar_db::safe_resultRow($query);
*/
    }



    //
    //  Gets the object holding "yurlico_id" and "organization_id" (from database, not from session!).
    //  Returns error code (int) in case of failure.
    //
    public function db__get_user_yurlico_and_organization_id($local_user_id)
    {
        $local_user_id = intval($local_user_id);
        if ($local_user_id <= 0)
        {   return -2; // user is not logged in.
        }

	    $this->db->select('yurlico_id, organization_id');
	    $this->db->where('user_id', $local_user_id);
	    $query = $this->db->get('user_profiles');

	    if (!$query || $query->num_rows() <= 0)
	    {   return -3; // unrecognized error.
        }

        return $query->row();
    }


    //
    //  Get info specific for CABINET user profile: e.g. organization_id, etc.
    //
    public function get_berkana_user_profile($local_user_id)
    {
    /*
SELECT DISTINCT wt_users.id, wt_users.username, wt_users.activated, wt_users.banned, wt_user_profiles.name, wt_user_profiles.yurlico_id,
wt_user_profiles.organization_id, wt_berk_organizations.name AS organization_name
FROM wt_users
LEFT JOIN wt_user_profiles ON wt_user_profiles.user_id = wt_users.id
LEFT JOIN wt_berk_organizations ON wt_berk_organizations.id  = wt_user_profiles.organization_id
LEFT JOIN wt_berk_organizations AS BORG ON BORG.id = wt_user_profiles.organization_id
WHERE wt_user_profiles.user_id = '40'
     */

        $local_user_id = intval($local_user_id);
        if ($local_user_id <= 0)
        {   return null;
        }

     $sql1 = 'SELECT DISTINCT wt_users.id, wt_users.username, wt_users.activated, wt_users.banned, wt_user_profiles.name, wt_user_profiles.yurlico_id,
wt_user_profiles.organization_id, wt_berk_organizations.name AS organization_name
FROM wt_users
LEFT JOIN wt_user_profiles ON wt_user_profiles.user_id = wt_users.id
LEFT JOIN wt_berk_organizations ON wt_berk_organizations.id  = wt_user_profiles.organization_id
LEFT JOIN wt_berk_organizations AS BORG ON BORG.id = wt_user_profiles.organization_id
WHERE wt_user_profiles.user_id = '.$local_user_id;

        $query = $this->db->query($sql1);
        $res = Utilitar_db::safe_resultRow($query);
//        log_message('error', "123SQL = $sql1\n".print_r($res, true));
        return $res;
    }

    private function _getRemoteSchedule_conditionally($days_before, $days_after)
    {
        if (($days_before >= 0) && ($days_after >= 0))
        {   $db_prefix = $this->db->dbprefix;
            $this->db_where_date__relative2today($db_prefix.'berk_schedule', 'planneddate', $days_before, $days_after);
        }

        $this->db->order_by('berk_schedule.plannedroom_id, berk_schedule.planneddate, berk_schedule.plannedstarttime');

//        $this->_dbNotRemoved('berk_schedule');

        $query = $this->db->get('berk_schedule');

//        log_message('error', 'ZZZ LAST SQL (getRemoteSchedule) = '.$this->utilitar_db->get_sql_last_run());

        return Utilitar_db::safe_resultSet($query);
    }

    public function getRemoteSchedule($days_before = -1, $days_after = -1)
    {
        $this->db->select("berk_schedule.*");
        $this->dbfilter_yurlico_and_organization('berk_schedule'); // NOTE: this call is mandatory to be present when accessing berkana assests!

        return $this->_getRemoteSchedule_conditionally($days_before, $days_after);
    }

    // NOTE: the difference with getRemoteSchedule() is just passing in the yurlico and org_id.
    public function getRemoteSchedule_with_yurlico_orgid($yurlico_id, $organization_id, $days_before = -1, $days_after = -1)
    {
        $this->db->select("berk_schedule.*");
        $this->dbfilter_yurlico_and_organization_multipletables('berk_schedule', $yurlico_id, $organization_id); // NOTE: this call is mandatory to be present when accessing berkana assests!

       return $this->_getRemoteSchedule_conditionally($days_before, $days_after);
    }


    private function dateinc($the_date, $interval)
    {
        return strftime("%Y-%m/%d", strtotime("$the_date +$interval"));
    }

    // searches for words not to be supplied to the End-User, like "Аренда", "АрендаС", "АрендаТ", etc.
    private function is_stop_word($word)
    {

        foreach (Util_berkana_data::$stop_words__training_types as $stop_word)
        {
            if (0 === mb_strpos($word, $stop_word)) // ignores stopwords contained in a middle/end of the "$word"!
            {   return true;
            }
        }

        return false;
    }

    public function get_organization_by_guid($guid)
	{   return $this->utilitar_db->_getTableRow('berk_organizations', 'guid', $guid);
	}

    public function get_organization($organization_id)
	{   return $this->utilitar_db->_getTableRow('berk_organizations', 'id', $organization_id);
	}

    //
    // Unfortunately Berkana provides not "training TYPEs" but "training group's names"!
    // So we are creating ARTIFICAL DATA STRUCTURE: due to missing REAL training types in Berkana :(
    // This is for making a RESERVATION web-server must supply real TRAINING TYPES (because User is not interested in "Wu-Shu-1" or "Wu-shu-2" groups - he just want "Wu-shu".
    // Those FAKE structures to be used during RESERVATION and NOTIFICATION (in the email to User/Administrator).
    //
    //  Note #1: that this function makes similar training types spelled "Wu-Shu" and "WU-SHU" (previously they were error source for a detection of trt_id).
    //  Note #2: Skips training_types (and their trainings) contained in stop-words. E.g. "аренда", "ТЕСТОВЫЙ ФИТНЕС" and etc.: because those "trainings" are NOT FOR PUBLICITY.
    //
    public function getTrainingTypesAndNamesMapping(&$training_names)
    {
        $training_type_names = array();
        $training_type_training_ids = array(); // training type maps to its trainings list.

        // let's first collect training_ids in one place - for specific trainig_type:
        foreach ($training_names as $training)
        {
            // extract artifical (!) training_type from the training_name:
            $typename                       = $this->convert_name_to_type($training->name);

            if ($this->is_stop_word($typename))
            {   log_message('debug', "ignoring '$typename' as a stop-word for training types!");
                continue;
            }


            // Use the lowercase typename as a key only.
            // We don't use lower-cased training_type_name as the one to show to the end-user, due to obvious reason (hopefully for you, dear Reader).
            $typename_lower_case            = mb_strtolower($typename);

            $training_type_names[$typename_lower_case]= $typename; // Actual training type name for the End-User.
            $training_type_training_ids[$typename_lower_case][]= $training->id;
        }

//log_message('error', "QQQ training_type_training_ids:\n".print_r($training_type_training_ids, true));

        // now let's combine all this into a sinle result array:
        $res = array();
        foreach ($training_type_names as $typename_lower_case => $typename)
        {   $res[] = array('id' => $this->calc_training_type__code($typename), 'name' => $typename, 'training_ids' => $training_type_training_ids[$typename_lower_case]);
        }

        return $res;
    }

    private function calc_training_type__code($training_type_name)
    {
        return crc32(mb_strtolower($training_type_name)."LgiCvsW"); // concatenate with secret code.
    }

    // disallowed chars: digits and "."
    const MAX_NESTING_LEVEL = 3;

    private function convert_name_to_type($training_name, $nesting_level = 0)
    {
        // exit recursion here:
        if ($nesting_level > Util_berkana_data::MAX_NESTING_LEVEL)
        {   return trim($training_name); // return the original with almost no changes to it.
        }

        $last_symbol        = mb_substr($training_name, -1, 1);
        if (is_numeric($last_symbol) || ('.' == $last_symbol) || ('-' == $last_symbol)) // disallowed chars are listed here.
        {   return $this->convert_name_to_type(trim(mb_substr($training_name, 0, -1)), ++$nesting_level);
        }

        return trim($training_name);
    }

    private function convert_name_to_type_OLD_VERSION($training_name)
    {
        $last_symbol_is_numeric = is_numeric(mb_substr($training_name, -1, 1));
        if ($last_symbol_is_numeric)
        {   return trim(mb_substr($training_name, 0, -1));
        }

        $second_symbol_is_numeric = is_numeric(mb_substr($training_name, -2, 1));
        if ($second_symbol_is_numeric)
        {   return trim(mb_substr($training_name, 0, -2));
        }

        return $training_name;
    }


    // NOTE: i disabled "WHERE berk_coursetypes.active > 0" check: inactive ones allowed, *removed* ones - are not.
    public function getTrainingNames($bIncludeRemoved = false, $training_type_IDs = NULL)
    {
        $this->db->select('berk_coursetypes.id, berk_coursetypes.name, berk_coursetypes.removed, berk_coursetypes.active');
        $this->dbfilter_yurlico_and_organization(array('berk_coursetypes')); // NOTE: this call is mandatory to be present when accessing berkana assests!

        if (!$bIncludeRemoved)
        {   $this->_dbNotRemoved($this->db->dbprefix.'berk_coursetypes');
        }

//        $this->db->where('berk_coursetypes.active > 0', NULL); // NOTES: I've disabled "WHERE berk_coursetypes.active > 0" check: inactive ones allowed, *removed* ones - are not.

        if ($training_type_IDs && is_array($training_type_IDs) && (count($training_type_IDs) > 0))
        {   $this->db->where_in('id', $training_type_IDs);
        }

//        log_message('error', "SQL = ".print_r($this->db->_compile_select(), true));
        $query = $this->db->get('berk_coursetypes');

//        log_message('error', 'LAST SQL getTrainingNames = '.$this->utilitar_db->get_sql_last_run());

        return Utilitar_db::safe_resultSet($query);
    }


    //
    // NOTE: I've disabled "WHERE berk_coursetypes.active > 0" check: inactive ones allowed, *removed* ones - are not.
    //
    //  Ensure to update WHERE conditions simultaneously with "get_planned_visits_for_date()" or you will face data discrepancy in a result set!
    //  If $schedule_IDs_array is not empty then retrieves schedules only from those IDs (makes sence when retrieving Cabinet _visits_ only, i.e. not *all the available* trainings).
    // NOTE 1: Returns list of ARRAYS - not the objects list!
    // NOTE 2: Gets trainings for current day if "$days_before" is zero.
    //
    //  If both $days_before and $days_after are zero then it returns current day's data!
    public function getRoomTrainingsArray($room_id, $days_before, $days_after, $schedule_IDs_array = NULL)
    {
/*
SELECT DISTINCT
wt_berk_schedule.id, wt_berk_schedule.teacher_id AS trainer_id,
wt_berk_schedule.plannedroom_id AS cabinet_id, wt_berk_schedule.canceled AS state, wt_berk_schedule.coursetype_id AS training_name_id,
wt_berk_coursetypes.name AS coursetype_name,
DATE_FORMAT(wt_berk_schedule.planneddate, "%Y-%m-%d") AS DAY,
wt_berk_schedule.plannedstarttime AS start_time,
wt_berk_schedule.plannedfinishtime AS end_time
FROM wt_berk_schedule
LEFT JOIN wt_berk_coursetypes ON wt_berk_coursetypes.id = wt_berk_schedule.coursetype_id
WHERE wt_berk_schedule.yurlico_id=24 AND wt_berk_coursetypes.yurlico_id=24
AND (wt_berk_schedule.planneddate BETWEEN CURRENT_DATE - INTERVAL '0' DAY AND CURRENT_DATE + INTERVAL '7' DAY)
*/
        $db_prefix = $this->db->dbprefix;

        // TODO: remove "day" field as well (check data consistency first!):   DATE_FORMAT('.$db_prefix.'berk_schedule.planneddate, "%Y-%m-%d") AS day,
        $this->db->select('berk_schedule.id, berk_schedule.teacher_id AS trainer_id,
berk_schedule.plannedroom_id AS cabinet_id, berk_schedule.canceled AS state, berk_schedule.coursetype_id AS training_name_id,
DATE_FORMAT('.$db_prefix.'berk_schedule.planneddate, "%Y-%m-%d") AS day,
'.$db_prefix.'berk_schedule.plannedstarttime AS start_time,
'.$db_prefix.'berk_schedule.plannedfinishtime AS end_time ', FALSE);

        $this->db->distinct();
        $this->db->join('berk_coursetypes', 'berk_coursetypes.id = berk_schedule.coursetype_id', 'left');

//$this->db->select_sum('column_name')
//         ->from('table_name')
//         ->where("DATE_FORMAT(column_name,'%Y-%m') <","YYYY-MM")
//         ->get();

        if ($room_id > 0)
        {   $this->db->where('berk_schedule.plannedroom_id', $room_id);
        }

//        $this->db->where('berk_coursetypes.active > 0', NULL); // NOTES: I've disabled "WHERE berk_coursetypes.active > 0" check: inactive ones allowed, *removed* ones - are not.

        // retrieves schedules only from those IDs (makes sence when retrieving Cabinet _visits_ only, i.e. not *all the available* trainings):
        if ($schedule_IDs_array && is_array($schedule_IDs_array) && (count($schedule_IDs_array)  > 0))
        {   $this->db->where_in('berk_schedule.id', $schedule_IDs_array);
        }

        //----------------------------------------------------------------------------+
        // TODO: TEMPORARILY REMOVED DUE TO BUG in BERKANA!!!!
//        $this->_dbNotRemoved($this->db->dbprefix.'berk_coursetypes');
        //----------------------------------------------------------------------------|


        $this->db_where_date__relative2today($db_prefix.'berk_schedule', 'planneddate', $days_before, $days_after);

        $this->dbfilter_yurlico_and_organization(array('berk_schedule', 'berk_coursetypes')); // NOTE: this call is mandatory to be present when accessing berkana assests!
        $this->db->order_by('berk_schedule.planneddate');

//        log_message('error', "SQL2 = ".print_r($this->db->_compile_select(), true));
        $query = $this->db->get('berk_schedule');

//        log_message('error', 'LAST SQL1 = '.$this->utilitar_db->get_sql_last_run());

        return Utilitar_db::safe_resultSetArray($query);
    }

    public function getRemoteRooms()
    {
        $this->db->select("berk_rooms.*");
        $this->dbfilter_yurlico_and_organization('berk_rooms'); // NOTE: this call is mandatory to be present when accessing berkana assests!

        $this->db->order_by('berk_rooms.title');

//        $this->_dbNotRemoved('berk_schedule');

        $query = $this->db->get('berk_rooms');

//        log_message('error', 'ZZZ LAST SQL (getRemoteRooms) = '.$this->utilitar_db->get_sql_last_run());

        return Utilitar_db::safe_resultSet($query);
    }

    public function get_organization_city($organization_id)
    {
/*
    SELECT wt_berk_cities.*, wt_berk_organizations.id AS orgid, wt_berk_organizations.name AS orgname
    FROM wt_berk_cities
    LEFT JOIN wt_berk_organizations ON wt_berk_cities.id = wt_berk_organizations.city_id
 */
        $this->db->select('berk_cities.*');
        $this->db->join('berk_organizations', 'berk_cities.id = berk_organizations.city_id', 'left');
        $this->db->where('berk_organizations.id', $organization_id);
        $query = $this->db->get('berk_cities');

        return Utilitar_db::safe_resultRow($query);
    }

    public function getRemoteOrganizations()
    {
        $this->db->select("berk_organizations.*");
        $this->dbfilter_yurlico_and_organization('berk_organizations'); // NOTE: this call is mandatory to be present when accessing berkana assests!

        $this->db->order_by('berk_organizations.name');

//        $this->_dbNotRemoved('berk_organizations');

        $query = $this->db->get('berk_organizations');

//        log_message('error', 'ZZZ LAST SQL (getRemoteOrganizations) = '.$this->utilitar_db->get_sql_last_run());

        return Utilitar_db::safe_resultSet($query);
    }

    public function getRemoteClients()
    {
        $this->db->select("berk_clients.*");
        $this->dbfilter_yurlico_and_organization('berk_clients'); // NOTE: this call is mandatory to be present when accessing berkana assests!

        $this->db->order_by('berk_clients.firstname');

//        $this->_dbNotRemoved('berk_organizations');

        $query = $this->db->get('berk_clients');

//        log_message('error', 'ZZZ LAST SQL (getRemoteOrganizations) = '.$this->utilitar_db->get_sql_last_run());

        return Utilitar_db::safe_resultSet($query);
    }

    public function getRemoteFamilies()
    {
        $this->dbfilter_yurlico_and_organization('berk_families'); // NOTE: this call is mandatory to be present when accessing berkana assests!

        $this->db->order_by('berk_families.id');

//        $this->_dbNotRemoved('berk_families');

        $query = $this->db->get('berk_families');

//        log_message('error', 'ZZZ LAST SQL (getRemoteOrganizations) = '.$this->utilitar_db->get_sql_last_run());

        return Utilitar_db::safe_resultSet($query);
    }

    public function getRemoteRoomByRemoteId($remote_id)
    {
        return $this->CI->utilitar_db->_getTableRow('berk_rooms', 'id', $remote_id);
    }

    public function getRemoteLocationOfOrganizationByRemoteId($organization_id, $remote_location_id)
    {
        return NULL; // this function must be retired since there's no LOCATIONS table at all.
    }

    public function getRemoteScheduleForTrainer_ByLocalUserID($local_user_id)
    {
        $berk_remote_trainer = $this->getRemoteTrainerByLocalUserId($local_user_id);

        if (!$berk_remote_trainer) // for any user that has NO associated Berkana_ID return empty array of trainings.
        {   return array();
        }

        return $this->utilitar_db->_getTableContents(   'berk_schedule',
                                                        'planneddate, plannedstarttime',
                                                        'teacher_id', $berk_remote_trainer->id);

    }

    public function addAccountToOrganization($client_id, $organization_id)
    {
        $data = array(
               'local_user_id'  => $client_id,
               'organization_id'=> $organization_id,
            );

        $this->CI->utilitar_db->insert_or_update('local_user_id', $client_id, 'berk_user_profiles', $data);
    }

    public function getAccountsNotInOrganization($organization_id, $linked_accounts_only = false)
    {
/* SELECT rbow_users.id, wt_berk_user_profiles.organization_id, rbow_users.username
FROM rbow_users
LEFT JOIN wt_berk_user_profiles ON rbow_users.id = wt_berk_user_profiles.local_user_id
WHERE
(wt_berk_user_profiles.organization_id IS NULL) OR (wt_berk_user_profiles.organization_id <> 11)
 */
        $this->db->select("users.id, user_profiles.name, berk_user_profiles.organization_id, users.username");
        $this->db->join('berk_user_profiles', 'users.id = berk_user_profiles.local_user_id', 'left');
        $this->db->join('user_profiles', 'users.id = user_profiles.user_id', 'left');

        $this->db->where('berk_user_profiles.organization_id IS NULL');// this includes uncategorized items. Mandatory SQL-statement.
        if ($linked_accounts_only)
        {   $this->db->or_where('berk_user_profiles.organization_id <>', $organization_id);
        }

        $this->dbfilter_yurlico_and_organization('berk_user_profiles'); // NOTE: this call is mandatory to be present when accessing berkana assests!

        $query = $this->db->get('users');

        return Utilitar_db::safe_resultSet($query);
    }

    public function removeAccountFromOrganization($client_id, $organization_id)
    {
//        $this->db->where('organization_id', $organization_id);
//        $this->db->where('local_user_id', $client_id);
//
//        $this->protectedUpdate('berk_user_profiles', array('organization_id' => NULL));
    }

    public function getOrganizationRooms($organization_id)
    {
        $this->db->select("berk_rooms.*, berk_organizations.id AS organization_id, berk_organizations.name AS organization_name, berk_organizations.smallname AS organization_smallname ");
        $this->db->join('berk_organizations', 'berk_rooms.organization_id = berk_organizations.id', 'left');

        $this->db->where('organization_id', $organization_id);

        // NOTE: this call is mandatory to be present when accessing berkana assests!
        $this->dbfilter_yurlico_and_organization(array('berk_rooms','berk_organizations'));

        $query = $this->db->get('berk_rooms');

        return Utilitar_db::safe_resultSet($query);
    }

    public function getRoomsNotInOrganization($organization_id, $linked_accounts_only = true)
    {
        $this->db->select("berk_rooms.*, berk_organizations.id AS organization_id, berk_organizations.name AS organization_name, berk_organizations.smallname AS organization_smallname ");
        $this->db->join('berk_organizations', 'berk_rooms.organization_id = berk_organizations.id', 'left');

        $this->db->where('berk_rooms.organization_id IS NULL'); // this includes uncategorized items. Mandatory SQL-statement.
        $this->db->or_where('berk_rooms.organization_id', -1);  // two cases are possible :(
        $this->db->or_where('berk_rooms.organization_id', 0);   // two cases are possible -- this occurs upon DB manual (!) edit
        if ($linked_accounts_only)
        {   $this->db->or_where('berk_rooms.organization_id <>', $organization_id);
        }

        $this->dbfilter_yurlico_and_organization(array('berk_rooms', 'berk_organizations')); // NOTE: this call is mandatory to be present when accessing berkana assests!

        $query = $this->db->get('berk_rooms');

        return Utilitar_db::safe_resultSet($query);
    }

    //
    public function addRoomToOrganization($room_id, $organization_id)
    {
//        $this->db->where('id', $room_id);
//        $this->protectedUpdate('berk_rooms', array('organization_id' => $organization_id));
    }

    //
    //  "$user_id" input parameter is expected to be of CABINET role type only.
    public function assignCabinetAccountToRoom($room_id, $organization_id, $user_id)
    {
//        $this->db->where('local_user_id', $user_id);
//        $data = array('room_id' => $room_id, 'organization_id' => $organization_id);
//        $this->protectedUpdate('berk_user_profiles', $data);
    }

    public function removeRoomFromOrganization($room_id, $organization_id)
    {
        $this->db->where('organization_id', $organization_id);
        $this->db->where('id', $room_id);

//        $this->protectedUpdate('berk_rooms', array('organization_id' => NULL));
    }


    //----------------------------------------------------------------------------+
    //  Yurlica-related functions:
    public function getYurlica()
    {
        return $this->CI->utilitar_db->_getTableContents('berk_yurlica');
    }


    //
    // append organization INN to the Title and name the resulting column as "name".
    //  NOTE:   It is mandatory to keep 'berk_yurlica' table column names w/out (!) real "name" column.
    //          Otherwise enrichment at genericTypes() wil be broken as it expects "name" column to draw in SelectionCombobox!
    //
    public function getYurlicaEnriched()
    {
        $db_prefix = $this->db->dbprefix;

        $this->db->select("CONCAT(".$db_prefix."berk_yurlica.title, ', INN: ', ".$db_prefix."berk_yurlica.INN) AS name, ".$db_prefix."berk_yurlica.*", FALSE);

        $this->dbfilter_yurlico_and_organization($db_prefix.'berk_yurlica'); // NOTE: this call is mandatory to be present when accessing berkana assests!

        $query = $this->db->get('berk_yurlica');
        return Utilitar_db::safe_resultSet($query);
    }

    public function getYurlicoById($row_id)
    {
        return $this->CI->utilitar_db->_getTableRow('berk_yurlica', 'id', $row_id);
    }


    // WARNINNG: in some cases (configurable by widget!) it is possible to get on input not TR_TYPES_IDS but TRAINING_IDS!
    public function training_directions_to_text($yurlico_id, $organization_id, $training_type_IDs)
    {
        log_message('error', "training_directions_to_text() training_type_IDs: ".print_r($training_type_IDs, true));
        $SEPARATOR = ', ';
        $res = '';

        $coursetypes = $this->getCourseTypes($yurlico_id, $organization_id, false, $training_type_IDs);

        foreach ($coursetypes as $coursetype)
        {   $res .= '"'.$coursetype->name.'"'.$SEPARATOR;
        }

        // remove the trailing separator (if any):
        if (strlen($res) > 0)
        {   $res = substr($res, 0, -strlen($SEPARATOR));
        }

        return $res;
    }


    public function getYurlicoByOrganizationId($organization_id)
    {
        if ($organization_id <= 0)
        {   return NULL;
        }

/*      SELECT wt_berk_yurlica.*
        FROM wt_berk_yurlica
        LEFT JOIN wt_berk_organizations ON wt_berk_organizations.yurlico_id = wt_berk_yurlica.id
        WHERE wt_berk_organizations.id = 1121971361
*/
        $this->db->select('berk_yurlica.*');
        $this->db->join('berk_organizations', 'berk_organizations.yurlico_id = berk_yurlica.id', 'left');
        $this->db->where('berk_organizations.id', $organization_id);

        $query = $this->db->get('berk_yurlica');
//        log_message('error', "SQL = ".print_r($this->db->last_query(), true));

        return Utilitar_db::safe_resultRow($query);
    }

    // get stats over all the clubs in the organization for today (only).
    public function getTodayStats($yurlico_id)
    {
        return array(   "trainings_count"   => $this->STATS_get_trainings_for_today($yurlico_id),
                        "clients_count"     => $this->STATS_get_unique_clients_for_today($yurlico_id),
                        "visits_count"      => $this->STATS_get_visits_for_today($yurlico_id),
                        "cabinet_devices_count"   => $this->STATS_get_cabinet_devices_count($yurlico_id),
                        "visitor_devices_count"   => $this->STATS_get_visitor_devices_count($yurlico_id),
                        "mobile_clients_count"   => $this->STATS_count_mobile_clients($yurlico_id),
                    );
    }

    public function helper_get_mobile_presence_records($organization_id = -1, $user_id = -1)
    {
    /*
SELECT wt_sys_devices.did,
wt_sys_devices_connection_trace.user_id,
wt_sys_devices_connection_trace.access_timestamp,
wt_sys_devices_connection_trace.extra_data
FROM wt_sys_devices_connection_trace
LEFT JOIN wt_user_profiles ON wt_user_profiles.user_id = wt_sys_devices_connection_trace.user_id
LEFT JOIN wt_sys_devices ON wt_sys_devices.id = wt_sys_devices_connection_trace.sys_device_id
WHERE wt_sys_devices_connection_trace.organization_id = 3 AND ABS(wt_sys_devices_connection_trace.operation_type) = 8
ORDER BY wt_sys_devices_connection_trace.access_timestamp
*/
        $this->db->select('sys_devices.did, sys_devices_connection_trace.user_id, sys_devices_connection_trace.extra_data, sys_devices_connection_trace.access_timestamp');
        $this->db->join('user_profiles', 'user_profiles.user_id = sys_devices_connection_trace.user_id', 'left');
        $this->db->join('sys_devices', 'sys_devices.id = sys_devices_connection_trace.sys_device_id', 'left');

        if ($organization_id > 0)
        {   $this->db->where('sys_devices_connection_trace.organization_id', $organization_id);
        }

        $this->db->where('ABS('.$this->db->dbprefix.'sys_devices_connection_trace.operation_type)', Json_base::OP_SET_VISIT_MOBILE); // both success and fail results gathered.
        $this->db->order_by('sys_devices_connection_trace.access_timestamp DESC');
        $query = $this->db->get('sys_devices_connection_trace');

        return Utilitar_db::safe_resultSet($query);
    }

    //
    // Returns either the most recent reply for yurlico or for specific club of that yurlico.
    private function helper_get_berkana_svc_last_dataimport_datetime($yurlico_id, $organization_id = -1)
    {
/*SELECT access_timestamp
FROM wt_sys_devices_connection_trace
LEFT JOIN wt_user_profiles ON wt_user_profiles.user_id = wt_sys_devices_connection_trace.user_id
WHERE wt_sys_devices_connection_trace.organization_id = 5 AND operation_type = 7 AND `wt_user_profiles`.`yurlico_id`=37
ORDER BY access_timestamp DESC
LIMIT 1
    */
            $this->db->select('access_timestamp');
            $this->db->join('user_profiles', 'user_profiles.user_id = sys_devices_connection_trace.user_id', 'left');

            $this->db->where('operation_type', Json_base::OP_SVC_WRITE_DATA);
            $this->db->where('user_profiles.yurlico_id', $yurlico_id);
            if ($organization_id > 0)
            {   $this->db->where('organization_id', $organization_id); // filter for particular organization, if requested.
            }
            $this->db->limit(1);
            $this->db->order_by('access_timestamp DESC');

            $query = $this->db->get('sys_devices_connection_trace');

            if (0 == $query->num_rows())
            {   return null;
            }

            $row = $query->row();
            return $row->access_timestamp;

/* SELECT wt_users.username, wt_users.last_login, wt_user_profiles.yurlico_id, wt_user_profiles.organization_id
FROM wt_users
LEFT JOIN wt_user_profiles ON wt_user_profiles.user_id = wt_users.id
LEFT JOIN wt_users_groups_link ON wt_users_groups_link.userId = wt_users.id
LEFT JOIN wt_user_groups ON wt_user_groups.id = wt_users_groups_link.groupId
WHERE
    wt_user_profiles.yurlico_id = 27  AND  wt_user_groups.name = 'SERVER_berkana'
*/
    }

    public function getCabinet_YurlicoStatistics($yurlico_id)
    {
        $statistics = new stdClass();

        $statistics->last_update            = $this->helper_get_berkana_svc_last_dataimport_datetime($yurlico_id);//  WTService last poll time.

        $statistics->yurlico_id             = $yurlico_id;
        $statistics->today_stats            = $this->getTodayStats($yurlico_id);

        // events & notifications:
//        $statistics->events                 = 0;
//        $statistics->event_notifications    = 0;

        // Mobile Clients:
//        $statistics->mobile_clients_total          = 0;
//        $statistics->mobile_clients_online         = 0;
        
        $statistics->clubs_count            = $this->getClubsCount($yurlico_id);

//    даты последних обновлений инфы от: а) Berkana -> WT; b) WT->Berkana.
//    статистика на текущий день: кол-во занятий, кол-во клиентов, кол-во регистраций на занятия.
//    общее кол-во клиентов на сервере WT
//    общее кол-во занятий (schedule) на сервере WT
//    общее кол-во обработанных нотификаций (сообщений) от сервера Берканы

        return $statistics;
    }


    //
    // Pay attention to the use of "user_profiles.userdata".
    // NOTE: see users->get_users_with_roles_full($role_name) as a base version of SQL.
    private function get_users_in_organizations($organization_ids, $role_name = null)
    {
        if (!is_array($organization_ids) || count($organization_ids) <= 0)
        {   return array();
        }

        $this->db->select('users.id, users.username, users.email, users.created, users.activated, users.banned, user_groups.name as role_name, user_profiles.name, user_profiles.yurlico_id, user_profiles.organization_id, user_profiles.userdata');

        $this->db->from('users_groups_link');
        $this->db->join('users', 'users.id = users_groups_link.userId', 'right');
        $this->db->join('user_profiles', 'users.id = user_profiles.user_id', 'left');
        $this->db->join('user_groups', 'users_groups_link.groupId = user_groups.id', 'left');

        $this->db->where('user_groups.group_type', Utilitar::GROUP_TYPE__ROLES);
        if ($role_name)
        {   $this->db->where('user_groups.name', $role_name);
        }
        $this->db->where_in('user_profiles.organization_id', $organization_ids);

        $this->db->order_by('user_profiles.organization_id, name');

        $query = $this->db->get();
//        log_message('error', "SQL = ".print_r($this->db->last_query(), true));

        return Utilitar_db::safe_resultSet($query);
    }


    // Returns key->value [i.e. { org_id->{accounts} }]!
    // Each yurlico could have multipe website accounts: berkana_server, cabinet and other per *each* organization.
    public function getYurlicoAccountsConnectionSettings($yurlico_id, $exclude_visitor_accounts = true)
    {
        if (!$yurlico_id || ($yurlico_id <=0))
        {   return array();
        }
        
/*
SELECT wt_users.id AS user_id, wt_users.email, wt_user_groups.name AS role_name, wt_user_profiles.userdata,
wt_user_profiles.yurlico_id, wt_user_profiles.organization_id, wt_berk_organizations.name AS organization_name
    FROM wt_users
LEFT JOIN wt_users_groups_link ON wt_users_groups_link.userId = wt_users.id
LEFT JOIN wt_user_groups ON wt_user_groups.id = wt_users_groups_link.groupId
LEFT JOIN wt_user_profiles ON wt_user_profiles.user_id = wt_users.id
LEFT JOIN wt_berk_organizations ON wt_berk_organizations.id = wt_user_profiles.organization_id
WHERE wt_user_profiles.yurlico_id = '$yurlico_id'
AND wt_user_profiles.organization_id IS NOT NULL
ORDER BY wt_user_profiles.organization_id, wt_user_groups.name DESC, wt_users.id
*/
        $this->db->select("users.id AS user_id, users.email as account_email, user_groups.name AS role_name, user_profiles.userdata, user_profiles.yurlico_id, user_profiles.organization_id, berk_organizations.name AS organization_name, berk_organizations.phone, berk_organizations.address, berk_cities.name as city_name");

        $this->db->join('users_groups_link', "users_groups_link.userId = users.id", 'left');
        $this->db->join('user_groups', "user_groups.id = users_groups_link.groupId", 'left');
        $this->db->join('user_profiles', "user_profiles.user_id = users.id", 'left');
        $this->db->join('berk_organizations', "berk_organizations.id = user_profiles.organization_id", 'left');
        $this->db->join('berk_cities', "berk_cities.id = berk_organizations.city_id", 'left');
        $this->db->where('user_profiles.yurlico_id', $yurlico_id);
        $this->db->where('user_profiles.organization_id IS NOT NULL'); // exclude user website account from the result-set.

        if ($exclude_visitor_accounts)
        {   $this->db->where('user_groups.name !=', SecuredMatrix::ROLE_MOBILE_CLIENT);
        }

        $this->db->order_by('user_profiles.organization_id, user_groups.name DESC, users.id');

        $query  = $this->db->get('users');
        $rows   = Utilitar_db::safe_resultSet($query);

        // extract organization users:
        $organization_users = array();
        foreach ($rows as $row)
        {   $organization_users[$row->organization_id][] = $row;
        }

        return $organization_users;
    }
    

    public function getYurlicaTypes()
    {
        $res = array();

        $v = new stdClass();
        $v->name = 'ООО';
        $v->id = 0;
        $res[] = $v;

        $v = new stdClass();
        $v->name = 'ИП';
        $v->id = 1;
        $res[] = $v;

        return $res;
    }
    //----------------------------------------------------------------------------|


    // iterates over DB rows array and returns an UserDTO object or NULL.
    private function get_DB_client_row_by__client_id($client_id, &$db_client_rows)
    {
        foreach ($db_client_rows as $row)
        {
            if ($client_id == ($row->id))
            {   return $row;
            }
        }

        return null;
    }

    public function separateClientsForInsertAndUpdate(&$berkana_client_object, $berkana_datatype_string, &$json_collection, &$insert, &$update)
    {
        if (0 == count($json_collection))
        {   log_message('error', "separateClientsForInsertAndUpdate(): No clients passed in: nothing to insert or update.");
            return; // no clients to process.
        }

//        log_message('error', "json_collection for separateClientsForInsertAndUpdate() =\n".print_r($json_collection, true));

        //----------------------------------------------------------------------------+
        // Ask for only clients passed in from JSON: no need retrieving *all the clients* existing in our DB.
        $client_ids = array();
        foreach ($json_collection as $client)
        {   $client_ids[] = $client['id'];
        }

        // get clients of the same organization:
        $this->dbfilter_yurlico_and_organization(Util_berkana_data::TBL_BERK_CLIENTS); // NOTE: this call is mandatory to be present when accessing berkana assests!

        $this->db->where_in('id', $client_ids); // get all clients with the following ids...
        
        $query      = $this->db->get(Util_berkana_data::TBL_BERK_CLIENTS);
        $db_clients = Utilitar_db::safe_resultSet($query);

//        log_message('error', "separateClientsForInsertUpdate() Clients in DB from JSON: ".print_r($db_clients, true));
        //----------------------------------------------------------------------------|

        $session_organization_id    = $this->session_get_organization_id();
        // NOTE: The "key" is row_id, "value" is all the posted properties for a row.
        foreach ($json_collection as $json_object)
        {
            $db_client_record = $this->get_DB_client_row_by__client_id($json_object['id'], $db_clients);
            if ($db_client_record &&
                ($session_organization_id == $db_client_record->organization_id) // // this check is additional for "Client" objects, compared to $this->parseJSON() call.
               )
           {
                $berkana_client_object->serializeFromDBRow($db_client_record);

                $json_object_hashcode = $berkana_client_object->calc_hash_from_JSON_object($json_object);

                if (!$berkana_client_object->hashesAreEqual($json_object_hashcode))
                {
                    log_message('error', "separateClientsForInsertAndUpdate(): 2 HASH is DIFFERENT for DB against JSON:\n DB:\n".print_r($berkana_client_object, true)."\n JSON:\n".print_r($json_object, true));
                    $update[$berkana_datatype_string][] = $json_object;
                }
            }
            else // otherwise this is new Client record:
            {   $insert[$berkana_datatype_string][] = $json_object;
            }
        }
    }

    public function JSON_insertNewPlannedVisits(&$berkana_planned_visit, &$json_collection, $yurlico_id, $organization_id)
    {
        // 1. separate json-visits by membershipelement_id (not schedule_id!), containing a set of clients that plan visiting it (same client might visit multipe schedule_ids same day, but just one (!) membershipelement_id):
        $meids_clIDs        = array(); // this is membership_element_ids__to__client_ids.
        $meids2scheduleID   = array(); // this is membership_element_ids__to__schedule_id.

        log_message('error', "JSON_insertNewPlannedVisits() # 1");

        $json_formatted_schedules   = array();
        foreach ($json_collection as $json_object)
        {   $client_id = $json_object['client_id'];
            if (null == $client_id)
            {   log_message('error', "WARNING!!! JSON_insertNewPlannedVisits() json_object client_id seems to be NULL - so skipping this DailyVisit entirely:\n".print_r($json_object, true));
                continue; // skip null data!
            }

            $membershipelement_id   = $json_object['id'];
            $schedule_id            = $json_object['schedule_id'];

            $meids_clIDs[$membershipelement_id]     = $client_id;
            $meids2scheduleID[$membershipelement_id]= $schedule_id;

            // fill in the clients' list for a json-schedule:
            $json_formatted_schedules[$schedule_id][]       = array('clid' => $client_id, 'meid' => $membershipelement_id); // also rememeber to which client belongs particular membershipelement_id.
        }

        log_message('error', "JSON_insertNewPlannedVisits() # 2");

        // 2. know which "$meids" are absent from DB. For that first retrieve "$meids" which are PRESENT for schedule_ids specified:
        $schedulue_ids = array_unique(array_values($meids2scheduleID));
        $this->db->select('id');
        $this->db->where_in('schedule_id', $schedulue_ids);
        $this->dbfilter_yurlico_and_organization(Util_berkana_data::TBL_PLANNED_VISITS); // NOTE: this call is mandatory to be present when accessing berkana assests!

        $query          = $this->db->get(Util_berkana_data::TBL_PLANNED_VISITS);
        $db_meids       = Utilitar_db::safe_resultSetArray($query);

        $json_meids     = array_keys($meids_clIDs);
        $json_meids_missing_from_DB = array_diff($json_meids, $db_meids);

        log_message('error', "JSON_insertNewPlannedVisits() # 3");

        // 3. let's insert new meids into the DB:
        foreach ($json_meids_missing_from_DB as $json_meID)
        {
            $this->protectedInsert( Util_berkana_data::TBL_PLANNED_VISITS,
                                    array(  'id'            => $json_meID, // this is a "membershipelement_id" value in fact.
                                            'schedule_id'   => $meids2scheduleID[$json_meID],
                                            'client_id'     => $meids_clIDs[$json_meID],

//                                            'visited'       => $meids_clIDs[$json_meID], TODO!
//                                            'version'       => $meids_clIDs[$json_meID], TODO!
                                         ),
                                         $yurlico_id,
                                         $organization_id
                                   );
        }

        log_message('error', "DailyVisits: inserted ".count($json_meids_missing_from_DB)." new visits into DB! :)");

        // 4. anything to do with clients' visits removed from Berkana DB?
    }

    //  Behind the scene creates not only Yurlico itself, but a web-site login user (based on its "$support_email") for it as well!
    //  Note: returns the UserDTO object just created in case of success or NULL for error.
    public function createYurlicoAndWebsiteAccount($yurlico_name, $INN, $support_email)
    {
        // add a yurlico first:
        $this->db->insert('berk_yurlica', array('title' => $yurlico_name,
                                                'web__short_name' => 'shortName-'.$yurlico_name,
                                                'INN' => $INN,
                                                'support_email' => $support_email,
                                                ));

        // determine the ID of a just-added record:
        $yurlico_id = $this->utilitar_db->get_last_inserted_id();

        // now let's add website user account for that yurlico:
        $name_fio   = 'Юрлицо (#'.$yurlico_id.')';
        $description= 'Юрлицо (#'.$yurlico_id.')';

        $userDTO_or_array = $this->_createWebsite_account($yurlico_id, NULL, $support_email, $name_fio, $description, false, 10, NULL, SecuredMatrix::ROLE_YURLICO_WEBLOGIN);

        return $userDTO_or_array;
    }

    public function notifyYurlicoOnRegistrationApproval($yurlico_id)
    {
        log_message('error', "WARNING: notifyYurlicoOnRegistrationApproval($yurlico_id) IS NOT YET IMPLEMENTED!");

        // todo: send Customer email notification.
    }

    // now filters by some complex logic though it should be filtering by new role BERKANA_CUSTOMER.
    private function getYurlicoEmail($yurlico_id)
    {
/*  SELECT wt_users.id, wt_users.email, wt_user_groups.name AS role_name, wt_user_profiles.yurlico_id
    FROM wt_users
LEFT JOIN wt_user_profiles ON wt_user_profiles.`user_id` = wt_users.id
LEFT JOIN wt_users_groups_link ON wt_users_groups_link.userId = wt_users.id
LEFT JOIN wt_user_groups ON wt_user_groups.id = wt_users_groups_link.`groupId`
    WHERE wt_user_profiles.yurlico_id = NNNNN
    AND wt_user_profiles.organization_id IS NULL  -- this is always null for yurlico WEBSITE account.
    AND wt_user_groups.name = 'SERVER_berkana'
*/
        return null;// NYI.
    }

    public function handle_registration_confirmation($activation_code)
    {
        // get request in status INCOMING with specificactivation_code:
        $reg_request = $this->util_registration->getRegistrationRequest($activation_code, Util_registration::REQ_STATUS__INCOMING);
        if (!$reg_request)
        {   return Json_base::ERRCODE__NO_SUCH_REQUEST; // no such request or already activated.
        }


        //----------------------------------------------------------------------------+
        // let's setup all the stuff.
//    'yurlico_id'
//    'organization_id'
//    'new_accounts'    - associative array {role_name->userDTO} of new accounts just created for services, cabinets, etc. To be listed in the email.
//    'invite_yurlico_user_to_login_first_time' - optional. Contans newly created UserDTO to be invited to log in (only if any Yurlico User have been just created).
//    'errorcode'       - optional. Negative code.
//    'errmessage'      - optional. Description for the error.
        $res    = $this->core_setupYurlico( $reg_request->yurlico_id,
                                            $reg_request->yurlico_name,
                                            $reg_request->city,
                                            $reg_request->inn,
                                            $reg_request->email,
                                            $reg_request->tariff_ids
                                            );

        log_message('error', "CORE_SETUPYURLICO RESULT = \n".print_r($res, true));

        // firstly send to Yurlico user email invinting to log in for the first time:
        if (isset($res['invite_yurlico_user_to_login_first_time']))
        {
            $userDTO = $res['invite_yurlico_user_to_login_first_time'];
            $this->email_invite_user_to_login_first_time($userDTO->login, $userDTO->password, $res['new_accounts']);
        }
        else // notify on accouns ADDED to existing yurlico.
        {   //$yurlico_email = $this->getYurlicoEmail($res['yurlico_id']);
            //$this->email_notify_user_on_new_accounts($yurlico_email, $res['new_accounts']);
        }

        $json_error_code = Json_base::ERRCODE__SUCCESS; // by default.
        
        // if something went wrong:
        if (isset($res['errorcode']))
        {   log_message('error', "handle_registration_confirmation() core_setupYurlico() returned ERROR:\n".$res['errmessage']);
            $json_error_code = Json_base::ERRCODE__ERROR_SETTING_UP_YURLICO; // just log the error and set resulting error code for now.
        }
        
        // if yurlico has been (still) created:
        if (isset($res['yurlico_id']) && ($res['yurlico_id'] > 0))
        {   
            // creation was successful at least partially. Firstly update the related Request:
            $this->util_registration->setRequestStatus($reg_request->activation_code, Util_registration::REQ_STATUS__CUSTOMER_ACTIVATED, $res['yurlico_id']);
            log_message('error', "handle_registration_confirmation(): organization ACTIVATED (request #$reg_request->id, yurlico #".$res['yurlico_id'].").");

            // note that email notification has already been sent ('invite_yurlico_user_to_login_first_time').
        }
        else
        {   log_message('error', "handle_registration_confirmation(): ERROR (".Json_base::ERRCODE__ERROR_SETTING_UP_YURLICO.") processing activation request");
            $json_error_code = Json_base::ERRCODE__ERROR_SETTING_UP_YURLICO; // just log the error and set resulting error code for now.
        }
        
        // if something went wrong:
        if (Json_base::ERRCODE__SUCCESS != $json_error_code)
        {   log_message('error', "handle_registration_confirmation() has failed. See the error messages above to catch the reason.");
        }
        else
        {   log_message('error', "handle_registration_confirmation() SUCCEEDED!");
            $this->email_notify_user_on_new_accounts($reg_request->email, $res['new_accounts']);
        }

        return $json_error_code;
	}

    //
    // Assumption: login === email in my case.
    public function email_invite_user_to_login_first_time($login, $password, $new_accounts)
    {
        $msg    = $this->load->view('email_templates/customer_invite2login_after_registration', array('login' => $login, 'password' => $password), true);
        $data   = $this->utilitar->get_defaultEmailOptions('Успешная активация аккаунта | WallTouch.ru', $msg);

        $data['to']     = $login;
        $this->utilitar->send_email($data, false);
    }


    //
    // Assumption: login === email in my case.
    //    'new_accounts'    - associative array {role_name->userDTO} of new accounts just created for services, cabinets, etc. to be listed in the email.
    //
    //  Please note that website account of Yurlico itself has been ALREADY NOTIFIED via email,
    //  so there is no need to worry about notyfying "SERVER_berkana" UserDTO. Trainers' notification is not supported at the moment.
    public function email_notify_user_on_new_accounts($yurlico_email, $new_accounts)
    {
        $cabinet_userDTO = null;
        // notify on case of cabinet only!
        foreach ($new_accounts as $rolename => $userDTO)
        {   if (0 === strcmp(SecuredMatrix::ROLE_CABINET, $rolename))
            {   $cabinet_userDTO = $userDTO;
                break;
            }
        }

        if ($cabinet_userDTO)
        {   $msg    = $this->load->view('email_templates/customer_notify_new_accounts_created', array('cabinet_userDTO' => $cabinet_userDTO), true);
            $data   = $this->utilitar->get_defaultEmailOptions('Подключение услуг | WallTouch.ru', $msg);

            $data['to']     = $yurlico_email;
            $this->utilitar->send_email($data, false);
        }
        else
        {   log_message('error', "email_notify_user_on_new_accounts($yurlico_email): no CABINET account found. Email sending canceled (as redundant)!");
        }
    }



    //
    //  This function handles:
    //      1. new Yurlico registration;
    //      2. adding tariffs to existing Club;
    //      3. adding new Club to existing Yurlico.
    //
    //  Method sets up all the data structures in DB.
    //  Returns complex structure:
    //    'yurlico_id'
    //    'organization_id'
    //    'new_accounts'    - associative array {role_name->userDTO} of new accounts just created for services, cabinets, etc. To be listed in the email.
    //    'invite_yurlico_user_to_login_first_time' - optional. Contans newly created UserDTO to be invited to log in (only if any Yurlico User have been just created).
    //    'errorcode'       - optional. Negative code.
    //    'errmessage'      - optional. Description for the error.
    //
    //  NOTE: "core_" prefix means to be moved to WTCore class later.
    public function core_setupYurlico($yurlico_id, $yurlico_name, $city, $INN, $support_email, $tariff_ids_string)
    {
        $this->load->model('tank_auth/users');

        // The steps:
        // 1. create new Yurlico (and required website account) if none exist for a registration request.
        // 2. create an "Organization Club" entity ("berk_organization").
        // 3. create Windows Service Account for the Organization (! not for just Yurlico!).
        // 4. assign ROLE_SERVER_BERKANA role to wtsync account.
        // 5. assign ROLE_YURLICO_WEBLOGIN role to website account.
        // 6. !! SKIPPED !! // link local user to Berkana yurlico and default "club" entry (i.e. "organization" in terms of Berkana)

        //
        // Note: TRANSACTIONS support is also needed: also try $this->db->trans_start()... $this->db->trans_complete().
        //

        $website_account_DTO    = null; // filled in in case if new website user account created (only!)

        // 1. create new Yurlico (and required website account) if none exist for a registration request:
        if ($yurlico_id <= 0)
        {   $website_account_DTO = $this->createYurlicoAndWebsiteAccount($yurlico_name, $INN, $support_email);
            if ($website_account_DTO && is_object($website_account_DTO))
            {   $yurlico_id = $website_account_DTO->yurlico_id;
            }
            else if (is_array($website_account_DTO)) // errors are listed here.
            {   log_message('error', "ERRORS by createYurlicoAndWebsiteAccount(): ".print_r($website_account_DTO, true));
            }
            else
            {   log_message('error', "ERRORS by createYurlicoAndWebsiteAccount(): unrecognized error!");
            }
        }

        // a fool-proof: ensure if yurlico either has been correctly set up or existing one retrieved:
        if ($yurlico_id <= 0)
        {   return array(   'errorcode' => -1,
                            'errmessage'=> 'Error '.(($yurlico_id > 0) ? 'accessing EXISTING' : 'CREATING').
                            " yurlico! Parameters: yurlico_name: $yurlico_name, INN: $INN, support_email: $support_email, yurlico_id: $yurlico_id");
        }

        //  3. create default "club" entity ("berk_organization"). Later it might be (still not a must for Customer) edited.
        $organization_club_id = $this->createOrganizationForYurlico($yurlico_name, $yurlico_id, $city);
        if ($organization_club_id <= 0)
        {   $tmp = array(   'errorcode' => -2,
                            'errmessage'=> "error creating organization! params (yurlico_name, yurlico_id, tariff_ids_string): $yurlico_name, $yurlico_id",
                            'yurlico_id'=> $yurlico_id);

            // even if we failed creating organization for yurlico - invite to log in:
            if ($website_account_DTO)
            {   $tmp['invite_yurlico_user_to_login_first_time'] = $website_account_DTO; // invite user to login even if other operations failed!
            }
            
            return $tmp;
        }

        // register tariffs requested for the organization:
        $this->setupOrganizationTariffs($organization_club_id, $tariff_ids_string);

        // creates website accounts: BERKANA_SERVER (a must), CABINET (optional), etc.
        // Returns associative array {role_name->userDTO} with roles with accoutns (or NULLs in case of error!!!) created, as per tariff ids passed in.
        $new_accounts = $this->createAllAccountsForTheOrganization($tariff_ids_string, $organization_club_id, $yurlico_id);

        log_message('error', "core_setupYurlico(): successfully created Yurlico (#$yurlico_id) and all linked data structures and accounts!");

        $res = array();
        $res['yurlico_id']      = $yurlico_id;
        $res['organization_id'] = $organization_club_id;
        $res['new_accounts']    = $new_accounts;
        log_message('error', "YURLICO new_accounts = ".print_r($new_accounts, true));

        if ($website_account_DTO)
        {   $res['invite_yurlico_user_to_login_first_time'] = $website_account_DTO; // invite user to login.
        }
        
        return $res;
    }

    private function getRolesArrayRequested($tariff_ids_string)
    {
        return array();
    }
    // the IDs are incorrect: stored the very first ID from a set of similar-named names!
    // e.g. array {1 => "Английский", 555 => "Английский"}  will become  array{555 => "Английский"} only!
    private function compact_coursetypes_by_same_names(&$coursetypes)
    {
//[75] =>     Раннее развитие 1-2 ВТ и ПТ
//[254] =>    Раннее развитие 1-2 СР
//[1650] =>   Раннее развитие 1-2 ср+пт
//[350] =>    Раннее развитие 2-3 Вт и Пт
//[2600] =>   Раннее развитие 2-3 ПТ
//[255] =>    Раннее развитие 2-3 СР

//        $ci = &get_instance();
//
//        // 1. find check/uncheck marks
//        if ($ci->utilitar->ends_with($client_name, WizardPeopleChecker::MARK_ON))


//    $teststr = "слово0-слово1 слово2 слово3 3312-5321 слово6";
//    $str = UBD::cut_out_ending__of_numbers_pair__dashed($teststr);

//        log_message('error', "XXX before compacting, ".count($coursetypes).': '.print_r($coursetypes, true));
        $ci     = &get_instance();
        $reverted_and_compacted = array();
        foreach ($coursetypes as $id => $name) // include everything, even stop-words:
        {
            $compacted_name = Util_berkana_data::cut_out_ending__of_numbers_pair__dashed($name);
            $reverted_and_compacted[$compacted_name] = $id;
        }

        // let's revert it back:
        $res = array();
        foreach ($reverted_and_compacted as $compacted_name => $id)
        {   $res[$id] = $compacted_name;
        }

//        log_message('error', "XXX AFTER compacting, ".count($res).': '.print_r($res, true));
        return $res;
    }


    // converts from "слово0-слово1 слово2 слово3 3312-5321 слово6" the "слово0-слово1 слово2 слово3".
    // It is helpful to shorten the "Гончарная мастерская 3-4" to just "Гончарная мастерская".
    // Or should be:
    //          from 'слово0-слово1 слово2 слово3 3312-5321 слово6' to 'слово0-слово1 слово2 слово3'.
    public static function cut_out_ending__of_numbers_pair__dashed($text)
    {
        $numbers_pair__dashed = '/\b(\d+-\d+)/u'; // 'слово0-слово1 слово2 слово3 3312-5321 слово6', after: 'слово0-слово1 слово2 слово3  слово6'

        $matches = array();
        $pos = preg_match($numbers_pair__dashed, $text, $matches);
        if (count($matches) > 0)
        {   $search_for = $matches[0];
            $ss = mb_substr($text, 0, mb_strpos($text, $search_for) - mb_strlen($text));
            return trim($ss);
        }

        return $text;

        $clean_text = "";
        // greco: this just CUTS out the pattern we pass in. Not exaclty our case, but just in case.
        $clean_text = preg_replace($numbers_pair__dashed, '', $text);
    }

    public function get_coursetypes_array_filtered($yurlico_id, $organization_id, $bIncludeRemoved, $training_type_IDs, $exclude_stopwords = true, $exclude_duplications = true)
    {
        $coursetypes = $this->getCourseTypes($yurlico_id, $organization_id, $bIncludeRemoved, $training_type_IDs);
        //log_message('error', "QQQ: get_coursetypes_array_filtered RAW($yurlico_id, $organization_id), count=".count($coursetypes).")".print_r($coursetypes, true));

        $coursetypes_without_stopwords = $this->coursetypes_to__id_name__array__filter_stopwords($coursetypes, $exclude_stopwords);
//        log_message('error', "QQQ: get_coursetypes_array_filtered NO STOPWORDS($yurlico_id, $organization_id), count=".count($coursetypes_without_stopwords).")".print_r($coursetypes_without_stopwords, true));

        if ($exclude_duplications)
        {
            $coursetypes_without_duplications = $this->compact_coursetypes_by_same_names($coursetypes_without_stopwords);
            //log_message('error', "QQQ: get_coursetypes_array_filtered NO DUPLICATES($yurlico_id, $organization_id), count=".count($coursetypes_without_duplications)." items: ".print_r($coursetypes_without_duplications, true));
            return $coursetypes_without_duplications;
        }
        else
        {   return $coursetypes_without_stopwords;
        }
    }

    // convert to coursetypes array: the format: {id -> name} elements.
    private function coursetypes_to__id_name__array__filter_stopwords(&$coursetypes, $exclude_stopwords = true)
    {
        $coursetypes_array = array();
        if ($exclude_stopwords)
        {   foreach ($coursetypes as $elem)
            {   if (!$this->is_stop_word($elem->name)) // skip stop-words!
                {   $coursetypes_array[$elem->id] = $elem->name;
                }
            }
        }
        else
        {   foreach ($coursetypes as $elem) // include everything, even stop-words:
            {   $coursetypes_array[$elem->id] = $elem->name;
            }
        }

        return $coursetypes_array;
    }

    // NOTE: i disabled "WHERE berk_coursetypes.active > 0" check: inactive ones allowed, *removed* ones - are not.
    // Allows retrieving data from ALL the organizations of tgiven yurlico!!!
    public function getCourseTypes($yurlico_id, $organization_id, $bIncludeRemoved = false, $training_type_IDs = NULL)
    {
        $this->db->select('berk_coursetypes.id, berk_coursetypes.name, berk_coursetypes.removed, berk_coursetypes.active');
        $this->db->where('berk_coursetypes.yurlico_id', $yurlico_id);
        if ($organization_id > 0)
        {   $this->db->where('berk_coursetypes.organization_id', $organization_id);
        }

        if (!$bIncludeRemoved)
        {   //$this->_dbNotRemoved($this->db->dbprefix.'berk_coursetypes');
            $this->_dbNotRemoved($this->db->dbprefix.'berk_coursetypes');
        }

//        $this->db->where('berk_coursetypes.active > 0', NULL); // NOTES: I've disabled "WHERE berk_coursetypes.active > 0" check: inactive ones allowed, *removed* ones - are not.

        if ($training_type_IDs && is_array($training_type_IDs) && (count($training_type_IDs) > 0))
        {   $this->db->where_in('id', $training_type_IDs);
        }

        $this->db->order_by('name');

//        log_message('error', "SQL = ".print_r($this->db->_compile_select(), true));
        $query = $this->db->get('berk_coursetypes');

//        log_message('error', 'LAST SQL getTrainingNames = '.$this->utilitar_db->get_sql_last_run());

        return Utilitar_db::safe_resultSet($query);
    }


    //  Club owners are allowed to categorize their training types on their own.
    //  NOTE: do not mix it with training types!!!
    //  NOTE: we are getting and setting *only ONE categorization* per yurlico: no difference whether there is a single organization or a number of them within a same yurlico!!!
    public function set_coursetypes_categories($yurlico_id, $educational, $sports, $entertainment)
    {
        $data = array(  'yurlico_id'    => $yurlico_id,
                        'educational'   => $educational,
                        'sports'        => $sports,
                        'entertainment' => $entertainment);

        $this->utilitar_db->insert_or_update('yurlico_id', $yurlico_id, 'coursetypes_categorized', $data);
    }

    //  Club owners are allowed to categorize their training types on their own.
    //  NOTE: do not mix it with training types!!!
    //  NOTE: we are getting and setting *only ONE categorization* per yurlico: no difference whether there is a single organization or a number of them within a same yurlico!!!
    public static function get_coursetypes_categories($yurlico_id)
    {
        $ci = &get_instance();

        $ci->db->select("coursetypes_categorized.*");
        $ci->db->where('yurlico_id', $yurlico_id); // NOTE: this call is mandatory to be present when accessing berkana assests!
        $ci->db->limit(1); // only ONE row to be received! TODO: this is possibly a bug, because for different organizations have different trainings to offer. But that's also an opportunity for them to offer smth. new...

        $query = $ci->db->get('coursetypes_categorized');

        return Utilitar_db::safe_resultRow($query);
    }

    public static function extract_types_as_per_area($yurlico_id, $area, $coursetypes_names)
    {
        $categorized_tr_types = Util_berkana_data::get_coursetypes_categories($yurlico_id);
        switch ($area)
        {
            case 'sports':
                return Util_berkana_data::extract_from__separated_string($categorized_tr_types->sports, $coursetypes_names);
                break;

            case 'educational':
                return Util_berkana_data::extract_from__separated_string($categorized_tr_types->educational, $coursetypes_names);
                break;

            case 'entertainment':
                return Util_berkana_data::extract_from__separated_string($categorized_tr_types->entertainment, $coursetypes_names);
                break;

            default:
                return Util_berkana_data::extract_unsorted($categorized_tr_types, $coursetypes_names);
                break;
        }
    }

    private static function extract_from__separated_string($separated_string, &$coursetypes_names)
    {   $res = array();
        $list = explode(Util_berkana_data::SEPARATOR, $separated_string);
        foreach ($coursetypes_names as $name)
        {   if (in_array($name, $list))
            {   $res[] = $name;
            }
        }
        return $res;
    }

    // all but sport, enterntainment and education:
    public static function extract_unsorted(&$categorized_tr_types, &$coursetypes_names)
    { // all but sport, entertainment and educational.
        $excluded_categories = '';

        if ($categorized_tr_types)
        {   $excluded_categories =  $categorized_tr_types->sports.Util_berkana_data::SEPARATOR
                                    .$categorized_tr_types->educational.Util_berkana_data::SEPARATOR
                                    .$categorized_tr_types->entertainment;
        }

        // 1. contact categories strings which should be out of our consideration:
        // 2. extract all others.
        $res = array();
        $list = explode(Util_berkana_data::SEPARATOR, $excluded_categories);
        foreach ($coursetypes_names as $name)
        {   if (!in_array($name, $list))
            {   $res[] = $name;
            }
        }
        return $res;
    }

    //
    //  Creates necessary web-accounts as per tariffs ordered. At least 1 account is a must: berkana_server, others depend on particular tariff.
    //
    // Assumed the following contants are ALWAYS the same!!!   1 - Cabinet, 2 - Client, 3 - Trainer, 4 - All Inclusive.
    // Returns associative array {role_name->userDTO} with roles with accoutns (or NULLs in case of error!!!) created, as per tariff ids passed in.
    //  Useful for a notification email composition.
    public function createAllAccountsForTheOrganization($tariff_ids_string, $organization_club_id, $yurlico_id)
    {
        // create accounts for:
        //  1. Berkana Windows Service
        //  2. Cabinet

        // firstly check for already existing accounts for a particular organization:
        $organization_users = $this->get_users_in_organizations(array($organization_club_id));
        $organization_existing_roles = array();
        foreach ($organization_users as $org_user)
        {   $organization_existing_roles[$org_user->role_name] = $org_user; // Note: hopefully we won't create multiple users for the same role in a same *organization*!
        }

        // Missing accounts array get filled in by NULLs when particular account creation FAILED.
        // Those NULLS help determining which accounts failed and still TO BE CREATED.
        $missing_accounts   = array();
        $errors             = array();
        $tariff_ids         = explode(',', $tariff_ids_string);

        // 1. if no ROLE_SERVER_BERKANA user exists then create some:
        if (!isset($organization_existing_roles[SecuredMatrix::ROLE_SERVER_BERKANA]))
        {
            $missing_accounts[SecuredMatrix::ROLE_SERVER_BERKANA]  = $this->core_createWinService_account($organization_club_id, $yurlico_id);

            if (!$missing_accounts[SecuredMatrix::ROLE_SERVER_BERKANA])
            {   $errors[] = 'error creating BERKANA_SERVER account for organization_club_id, yurlico_id = '."$organization_club_id, $yurlico_id";
            }
        }

        // 2. if either CABINET or ALL-INCLUSIVE needed then create a ROLE_CABINET:
        if (    in_array(Util_berkana_data::TARIFF_ID__CABINET, $tariff_ids) ||
                in_array(Util_berkana_data::TARIFF_ID__ALL_INCLUSIVE, $tariff_ids)
           )
        {
            if (!isset($organization_existing_roles[SecuredMatrix::ROLE_CABINET]))
            {   $missing_accounts[SecuredMatrix::ROLE_CABINET]  = $this->core_createCabinet_account($organization_club_id, $yurlico_id);

                if (!$missing_accounts[SecuredMatrix::ROLE_CABINET])
                {   $errors[] = 'error creating CABINET account for organization_club_id, yurlico_id = '."$organization_club_id, $yurlico_id";
                }
            }
        }


        //  Assign roles to newly created *valid* accounts:
        if (count($errors) > 0)
        {   log_message('error', "ERRORS in createAllAccountsForTheOrganization():\n".print_r($errors, true));
        }

        return $missing_accounts; // Note: nulls for particular roles are allowed in this return value!
    }

    //
    // Assigns a role to user.
    // Creates a new role if the role does not exist!
    private function assignRole_to_User($role_name, $user_id)
    {
        $row = $this->users->get_role($role_name);
        if (!$row)
        {   $data = array(
                   'name'       => $role_name,
                   'name_ru'    => $role_name,
                   'description'=> "automatically created missing role by assignRole_to_User() call.",
                   'group_type' => Utilitar::GROUP_TYPE__ROLES
                );

            $this->db->insert('user_groups', $data);

            // give it a retry:
            $row = $this->users->get_role($role_name);
            if (!$row)
            {   return false;
            }
        }

        return $this->tank_auth->user_role_add_by_role_id($user_id, $row->id);
    }

    public function getYurlicoOrganizationsTariffs($yurlico_id)
    {
/*
SELECT
wt_berk_organizations.id as organization_id, wt_berk_organizations.name as organization_name, wt_berk_organization_tariffs.duration_months,
wt_berk_organization_tariffs.date_start, wt_berk_organization_tariffs.date_finish, wt_berk_organization_tariffs.date_ordered,
wt_berk_tariffs.id AS tariff_id, wt_berk_tariffs.name as tariff_name, wt_berk_organization_tariffs.paid
FROM wt_berk_organization_tariffs
LEFT JOIN wt_berk_organizations ON wt_berk_organizations.id = wt_berk_organization_tariffs.organization_id
LEFT JOIN wt_berk_tariffs ON wt_berk_tariffs.id = wt_berk_organization_tariffs.tariff_id
WHERE wt_berk_organizations.yurlico_id = 61
ORDER BY wt_berk_organizations.name, wt_berk_tariffs.name
 */

        $this->db->select("berk_organizations.id as organization_id, berk_organizations.name as organization_name, berk_organization_tariffs.duration_months, berk_organization_tariffs.paid, berk_organization_tariffs.date_start, berk_organization_tariffs.date_finish, berk_organization_tariffs.date_ordered, berk_tariffs.id AS tariff_id, berk_tariffs.name as tariff_name");

        $this->db->join('berk_organizations', "berk_organizations.id = berk_organization_tariffs.organization_id", 'left');
        $this->db->join('berk_tariffs', "berk_tariffs.id = berk_organization_tariffs.tariff_id", 'left');

        $this->db->order_by('berk_organizations.name, berk_tariffs.name');

        $this->db->where('berk_organizations.yurlico_id', $yurlico_id);


        $query = $this->db->get('berk_organization_tariffs');
//        $query = $this->db->get();

        return Utilitar_db::safe_resultSet($query);
    }

    public function getClubTariffs($club_id, $only_these_tariffs_to_check = null)
    {
/*
SELECT `wt_berk_organizations`.`id` as organization_id, `wt_berk_organizations`.`name` as organization_name, `wt_berk_organization_tariffs`.`duration_months`, `wt_berk_organization_tariffs`.`date_start`, `wt_berk_organization_tariffs`.`date_finish`, `wt_berk_organization_tariffs`.`date_ordered`, `wt_berk_tariffs`.`id` AS tariff_id, `wt_berk_tariffs`.`name` as tariff_name
FROM (`wt_berk_organization_tariffs`)
LEFT JOIN `wt_berk_organizations` ON `wt_berk_organizations`.`id` = `wt_berk_organization_tariffs`.`organization_id`
LEFT JOIN `wt_berk_tariffs` ON `wt_berk_tariffs`.`id` = `wt_berk_organization_tariffs`.`tariff_id`
WHERE `wt_berk_organizations`.`id` =  1095119810
AND `wt_berk_tariffs`.`id` IN ('5', '3')
ORDER BY `wt_berk_organizations`.`name`, `wt_berk_tariffs`.`name`
 */
        $this->db->select("berk_organizations.id as organization_id, berk_organizations.name as organization_name, berk_organization_tariffs.duration_months, berk_organization_tariffs.date_start, berk_organization_tariffs.date_finish, berk_organization_tariffs.date_ordered, berk_tariffs.id AS tariff_id, berk_tariffs.name as tariff_name");

        $this->db->join('berk_organizations', "berk_organizations.id = berk_organization_tariffs.organization_id", 'left');
        $this->db->join('berk_tariffs', "berk_tariffs.id = berk_organization_tariffs.tariff_id", 'left');

        $this->db->order_by('berk_organizations.name, berk_tariffs.name');

        $this->db->where('berk_organizations.id', $club_id);
        if ($only_these_tariffs_to_check && is_array($only_these_tariffs_to_check))
        {  $this->db->where_in('berk_tariffs.id', $only_these_tariffs_to_check);
        }

        $query = $this->db->get('berk_organization_tariffs');

        return Utilitar_db::safe_resultSet($query);
    }


    //
    // Note: for "$tariffs_array" input accepts either array of ids or IDs comma-separated string.
    public function setupOrganizationTariffs($organization_id, $tariffs_array)
    {
        $duration_months = 12; // 1 year ahead.
        $today = date('Y-m-d');
        $date_finish = $this->utilitar_date->getDateIncremented($today, $duration_months.' month');

        $res = array();
        $tariff_ids_array = null;
        if (is_array($tariffs_array))
        {   $tariff_ids_array = array_keys($tariffs_array);
        }
        else if (is_string($tariffs_array))
        {   $tariff_ids_array = explode(',', $tariffs_array);
        }
        else
        {   log_message('error', "setupOrganizationTariffs() ERROR: unsupported TYPE of tariffs_array: ".print_r($tariffs_array, true));
            return;
        }

        foreach ($tariff_ids_array as $tariff_id)
        {
            $res[] = array(
            'organization_id' => $organization_id,
            'tariff_id'       => $tariff_id,
            'duration_months' => $duration_months,
            'date_start'      => $today,      // today
            'date_finish'     => $date_finish,// today + $duration_months
            );
        }

        $this->db->insert_batch('berk_organization_tariffs', $res);
    }

    public function getTariffs($b_active_only = true)
    {
/*
    SELECT * FROM wt_berk_tariffs
    WHERE wt_berk_tariffs.id IN (1, 5)
    ORDER BY wt_berk_tariffs.order ASC
 */

        if ($b_active_only)
        {   $this->db->where('is_active', true); // greco: IDs hardcoded from DB: 5: Widget for Schedule, 6: TG Teacher Clicker
        }
        
        $this->db->order_by('order ASC'); // top messages go first.
        $query = $this->db->get('berk_tariffs');

        return Utilitar_db::safe_resultSet($query);
    }

    // returns family record in case if it is linked to existing "$user_id".
    public function get_family_by_local_user_id($local_user_id)
    {
        $this->db->where('local_user_id', $local_user_id);
        $this->dbfilter_yurlico_and_organization(Util_berkana_data::TBL_FAMILIES); // NOTE: this call is mandatory to be present when accessing berkana assests!
        $this->db->limit(1);

        $query = $this->db->get(Util_berkana_data::TBL_FAMILIES);
        return Utilitar_db::safe_resultRow($query);
    }

    // NOTE: ensure to DECODE subject and body afterwards, using Util_berkana::decode_WT_message()!
    public function get_family_messages($family_id, $only_unseen_before, $limit_count = 20)
    {

        if ($only_unseen_before)
        {   $this->db->where('actualsenddate IS NULL');
        }

        $this->db->where('family_id', $family_id);
        $this->dbfilter_yurlico_and_organization('berk_messages'); // NOTE: this call is mandatory to be present when accessing berkana assests!
        $this->db->limit($limit_count);
        $this->db->order_by('berk_messages.id DESC'); // top messages go first.

        $query = $this->db->get('berk_messages');

        return Utilitar_db::safe_resultSet($query);
    }

    // NOTE: this differs from berkana_messages table data!!!!
    public function get_user_SYS_messages($user_id, $only_unseen_before, $limit_count = 20)
    {
        if ($only_unseen_before)
        {   $this->db->where('actualsenddate IS NULL');
        }

        $this->db->where('user_id', $user_id);
        $this->db->order_by('sys_messages.id DESC'); // top messages go first.
        $this->db->limit($limit_count);

        $query = $this->db->get('sys_messages');

        return Utilitar_db::safe_resultSet($query);
    }

    private function getClubsCount($yurlico_id)
    {
        if (intval($yurlico_id) <= 0)
        {   return 0;        
        }
        return $this->CI->utilitar_db->my_count_all('id', 'berk_organizations', " `yurlico_id` = $yurlico_id ");
    }

    //
    // Returns organization_id of the entry created.
    // Note that "organization_id" is NOT auto-incremented in my DB due to the fact that it could come also from External DB so I have no control of its ID in fact.
    private function createOrganizationForYurlico($organization_name, $yurlico_id, $city)
    {
        //  NOTE: If you wish to strip everything except basic printable ASCII characters (all the example characters above will be stripped) you can use:
        //      $string = preg_replace( '/[^[:print:]]/', '',$string);
        //
        // src: http://stackoverflow.com/a/8171868
        //

        $current_clubs_count = $this->getClubsCount($yurlico_id);

//        $stripped       = htmlspecialchars($organization_name);
        $stripped       = $this->utilitar->strip_html_tags($organization_name);


        // also convert it to English:
        // Q: what for?!?
        $organization_name_transliterated = $this->utilitar->strip_html_tags($this->transliterate->transliterate_return($stripped, false));
        $club_full_name = 'Клуб #'.(++$current_clubs_count).' для '.$stripped;

        // NOTE: this can't be autoincrement via DB autoincrement type, because we CAN get this from REMOTE HOST also, not only from local DB.
  //      $id = ($yurlico_id*63 + (int)sprintf('%u', crc32($organization_name_transliterated)));

        $data = array(  'yurlico_id'    => $yurlico_id,
//                        'id'            => $id, // supply your own ID. Get ready to deal with REMOVE "id" which is ACTUALLY could be the same as the one we just created here.
                        'name'          => $stripped,
                        'smallname'     => $club_full_name,
                        'city'          => $city,
                        );

        log_message('error', "new Organization = ".print_r($data, true));
        $this->db->insert('berk_organizations', $data);

        return $this->utilitar_db->get_last_inserted_id();
    }

    private function _createEmailAddressFor($prefix, $organization_club_id, $yurlico_id)
    {
        return $prefix.dechex($yurlico_id).'-'.substr(md5($organization_club_id), 7, 3).'@walltouch.ru';
//        return $prefix.dechex($yurlico_id).'-'.dechex($organization_club_id).substr(random_string('unique'), 7, 3).'@walltouch.ru';
//        return 'svc-'.dechex($yurlico_id).'-'.dechex($organization_club_id).'@walltouch.ru';
    }

    // get rid of 0, O and l.
    public static function generate_password_no_zeros($password_length)
    {
        $pwd = substr(random_string('unique'), 0, $password_length);
        $beautified = str_replace(array('0', 'o'), 'd', strip_tags($pwd));
        return str_replace(array('l'), 'F', strip_tags($beautified));
    }

    private function get_IDs_from_table($yurlico_id, $organization_id, $table_name__no_db_prefix, $id_rowname)
    {
        $this->db->select($id_rowname);// "id" is a row id column name at Berkana.
        $this->dbfilter_yurlico_and_organization($this->db->dbprefix.$table_name__no_db_prefix); // NOTE: this call is mandatory to be present when accessing berkana assests!

        $query = $this->db->get($table_name__no_db_prefix);

        $rows = Utilitar_db::safe_resultSet($query);

        $res = array();
        foreach ($rows as $row)
        {   $res[] = $row->$id_rowname;
        }

        return $res;
    }

    public function get_family_IDs($yurlico_id, $organization_id)
    {
        return $this->get_IDs_from_table($yurlico_id, $organization_id, 'berk_families', 'id');
    }

    public function get_messages_IDs($yurlico_id, $organization_id)
    {
        return $this->get_IDs_from_table($yurlico_id, $organization_id, 'berk_messages', 'id');
    }

    public function get_admins_with_no_telegram_tickets()
    {
        return array(); // TBD!
    }

    // NOTE: returns true on success, false if no ticket created.
    // either creates a ticket or updates its password (shall we unauthorize logged-in users when changing the password??) and ticket_owner_name.
    //
    // 1: WizardHelper::USER_ROLE__TRAINER
    // 3: WizardHelper::USER_ROLE__CLIENT
    public function create_or_update_telegram_ticket($local_user_id, $role, $yurlico_id, $organization_id, $password, $ticket_owner_name = NULL)
    {
        if (empty($password))
        {   log_message('error', "CUTT: SKIPPING creation of tg ticket for empty secure code!!! (uid: $local_user_id, y:$yurlico_id, org_id:$organization_id, onwer: $ticket_owner_name");
            return false;
        }

        $data = array(  'local_userid'      => $local_user_id,
                        'secure_code'       => $password,
                        'role'              => $role,

                        'yurlico_id'        => $yurlico_id,
                        'organization_id'   => $organization_id,
                    );

        // username is optional:
        if ($ticket_owner_name)
        {   $data['ticket_owner_name'] = $ticket_owner_name;
        }

        $where_array = array(   'yurlico_id'        => $yurlico_id,
                                'organization_id'   => $organization_id,
                                'local_userid'      => $local_user_id,
                            );
        $this->db->where($where_array);
        $query  = $this->db->get('tg_tickets');
        $ticket = Utilitar_db::safe_resultRow($query);

        // check whether password change occured:
        $b_telegram_unauthorize_needed = $ticket && (0 != strcmp($password, $ticket->secure_code));
        log_message('error', "CUTT: need to logout a user_id:$local_user_id from Telegram? ".($b_telegram_unauthorize_needed ? 'YES!':'NO'));

        if ($ticket)
        {   // Update (password/secure_code and ticket_owner_name only!). It is prohibited UPDATING "messaging_id"!!!:
            log_message('error', "CUTT: updating ticket for family: (uid: $local_user_id, y:$yurlico_id, org_id:$organization_id, onwer: $ticket_owner_name");

            $this->db->where($where_array);
            $this->db->update('tg_tickets', $data);
        }
        else
        {   // Insert:
            log_message('error', "CUTT: inserting ticket for family: (uid: $local_user_id, y:$yurlico_id, org_id:$organization_id, onwer: $ticket_owner_name");
            $data['messaging_id'] = random_string('alnum', 10); // NOTE: this is mandatory to create!
            $this->db->insert('tg_tickets', $data);
        }

        // let's unauthorize everybody with the same *ticket*:
        if ($b_telegram_unauthorize_needed)
        {   $where_array = array(   'ticket_id'         => $ticket->id,

                                    'yurlico_id'        => $yurlico_id,
                                    'organization_id'   => $organization_id,
                                );
            $this->db->where($where_array);
            $this->db->update('tg_users', array('logged_in' => 2)); // 1: true, 2: false
        }

        return true;
    }

    public function get_teacher_ticket($bot_name, $teacher_id, $yurlico_id, $organization_id)
    {
        $this->db->where(array( 'yurlico_id'        => $yurlico_id,
                                'organization_id'   => $organization_id,
                                'role'              => WizardHelper::USER_ROLE__TRAINER,
                                'bot_name'          => $bot_name,
                                'extra_data'        => $teacher_id,
                              )
                         );
       
        $this->db->where("locate('(wt testing)', ".$this->db->dbprefix.'tg_tickets.ticket_owner_name) = 0', null, false); // exclude my testing tickets!!

        $query  = $this->db->get('tg_tickets');
        return Utilitar_db::safe_resultRow($query);
    }

    // returns secure code (string) for admin
    public function get_or_create__telegram_ticket_for_admin($yurlico_id, $organization_id)
    {
        // first try the most correct way: with both yurlico_id and org_id!
        if ($organization_id > 0)
        {
            $this->db->where(array( 'yurlico_id'      => $yurlico_id,
                                    'organization_id' => $organization_id,
                                    'role'            => WizardHelper::USER_ROLE__CLUB_ADMIN, // this is for Telegram ADMINISTRATOR role value! // WizardHelper::USER_ROLE__CLUB_ADMIN
                                   )
                             );
        }
        else // forgive my copy-paste! Or refactor it later.
        {
            $this->db->where(array( 'yurlico_id'      => $yurlico_id,
                                    'role'            => WizardHelper::USER_ROLE__CLUB_ADMIN, // this is for Telegram ADMINISTRATOR role value! // WizardHelper::USER_ROLE__CLUB_ADMIN
                                   )
                             );
        }

        $query  = $this->db->get('tg_tickets');
        $ticket = Utilitar_db::safe_resultRow($query);

        if ($ticket)
        {   return $ticket->secure_code;
        }

        // ...then try w/out organization_id:
        $this->db->where(array( 'yurlico_id'      => $yurlico_id,
                                'role'            => WizardHelper::USER_ROLE__CLUB_ADMIN, // this is for Telegram ADMINISTRATOR role value! // WizardHelper::USER_ROLE__CLUB_ADMIN
                               )
                         );
        $query  = $this->db->get('tg_tickets');
        $ticket = Utilitar_db::safe_resultRow($query);

        if ($ticket)
        {   return $ticket->secure_code;
        }

        $organization_name = '';
        if ($organization_id > 0) // if we were given an organization, then retrieve it:
        {   $organization = $this->getOrganization($organization_id);
            $organization_name  = ($organization ? $organization->name : 'unrecognized organization');
        }
        else
        {   $organizations = $this->getOrganizationsByYurlico($yurlico_id);
            foreach ($organizations as $organization)
            {   $organization_id    = $organization->id;
                $organization_name  = $organization->name;
                break; // the very first organization is enough for now.// GRECO: todo: rework this error!
            }
        }
        
        // otherwise create a ticket first:
        $secure_code = Util_berkana_data::generate_password_no_zeros(6);
        $data = array(  'yurlico_id'        => $yurlico_id,
                        'organization_id'   => $organization_id,
                        'secure_code'       => $secure_code,
                        'messaging_id'      => random_string('alnum', 10), // NOTE: this is mandatory to create!,
                        'role'              => WizardHelper::USER_ROLE__CLUB_ADMIN,
                        'ticket_owner_name' => 'Администратор для '.$organization_name, // this is for Telegram ADMINISTRATOR role value!
//                        'ticket_owner_name' => 'Администратор для '.$organization_name.' (Telegram y:'.$yurlico_id.')', // this is for Telegram ADMINISTRATOR role value!
                    );

        $this->db->insert('tg_tickets', $data);

        return $secure_code;
    }

    //
    // Creates user account and in case of success assigns SecuredMatrix::ROLE_MOBILE_CLIENT role to it.
    // Returns array (!) of errors in case of error, or UserDTO object (!) for the success.
    public function create_user_account_VISITOR($yurlico_id, $organization_club_id, $family_id, $email, $password)
    {
//        $this->load->model('tank_auth/users');

        $name_fio           = sprintf('visitor (yur:%d,org:%d,f:%d)', $yurlico_id, $organization_club_id, $family_id);
        $description        = sprintf('Berkana visitor (yur:%d,org:%d,f:%d)', $yurlico_id, $organization_club_id, $family_id);

        $userDTO_or_array   = $this->_createWebsite_account($yurlico_id, $organization_club_id, $email, $name_fio, $description, true, 0, $password, SecuredMatrix::ROLE_MOBILE_CLIENT);
        if (!$userDTO_or_array || is_array($userDTO_or_array))
        {   // array returned only in case of error!
            // Note: the errors already logged by previous call.
//            log_message('error', "ERRORS by create_user_account_VISITOR(): ".print_r($userDTO_or_array, true));
            return $userDTO_or_array; // it is of "array" type here.
        }

//        log_message('error', "userDTO = ".print_r($userDTO_or_array, true));

        return $userDTO_or_array; // here it is real UserDTO object indeed.
    }

    //
    // Creates user account and in case of success assigns SecuredMatrix::ROLE_TRAINER role to it.
    // Returns array (!) of errors in case of error, or UserDTO object (!) for the success.
    public function create_user_account_TRAINER(&$berkana_row_trainer, $email, $password)
    {
//        $this->load->model('tank_auth/users');

        $description    = sprintf('Berkana trainer (y:%d, org_id:%d, %s)', $berkana_row_trainer->yurlico_id, $berkana_row_trainer->organization_id, $berkana_row_trainer->name);

        $userDTO_or_array   = $this->_createWebsite_account($berkana_row_trainer->yurlico_id, $berkana_row_trainer->organization_id, $email, $berkana_row_trainer->name, $description, true, 0, $password, SecuredMatrix::ROLE_TRAINER);

        return $userDTO_or_array; // here it is real UserDTO object indeed.
    }

    //
    //  Method creates user account *linked* to yurlico [and organization].
    //  Note: method returns either UserDTO object or an array with errors!!!
    //  Note: method accepts "password" input parameter or NULL if it should be generated manually!
    //
    //  WARNING:    Method stores the user password in "userdata" column (if $store_plaintext_password = TRUE)!
    //              This makes sense for owned Organization-accounts that Website User should be able to edit.
    //
    private function _createWebsite_account($yurlico_id, $organization_club_id, $email, $name_fio, $description, $store_plaintext_password, $password_length, $password, $role)
    {
        // we deny creating admin accounts via this function!
        if (SecuredMatrix::ROLE_ADMINISTRATOR == $role)
        {   log_message('error', "ERROR! _createWebsite_account(y:$yurlico_id, org_id:$organization_club_id, email:$email): an attempt to create ADMIN ACCOUNT! Skipped: see prev. logs for details!");
            return NULL;
        }

        if (!$this->tank_auth->is_username_available($email))
        {   log_message('error', "WARNING: username '$email' is already registered (y:$yurlico_id, org_id:$organization_club_id).");
            return NULL;
        }

        $email_activation = FALSE;
        if (!$password) // if no "$password" has been passed in then generate new one:        
        {   if ($password_length <= 0)
            {   $password_length = 8;
            }

            $password   = Util_berkana_data::generate_password_no_zeros($password_length);
        }

        $user_data = NULL;
        if (!is_null($user_data = $this->tank_auth->create_user(    $email, // use email as username
                                                                    $email,
                                                                    $password,
                                                                    $email_activation))
           )
        {
            $new_user_id = $user_data['user_id'];
            $username    = $email;
            $account_status = 1; // i.e. activated.

            // add a few details to user account:
            $this->users->set_user_details($new_user_id, $yurlico_id, $organization_club_id, $username, $name_fio, $account_status, $email, NULL, $description);

            if ($store_plaintext_password)
            {   // also store the password: to show it in the Personal Cabinet later:
                $this->db->where('user_id', $new_user_id);
                $this->db->update('user_profiles', array('userdata' => $password)); // todo: greco: encrypt the pwd!
            }

            $res = $this->assignRole_to_User($role, $new_user_id);
            if (!$res)
            {   // this is not really bad, if failed. So let's just warn about that:
                log_message('error', '_createWebsite_account(): error assigning role='.$role.' to uid:'.$new_user_id.' for y:'.$yurlico_id.', org_id:'.$organization_club_id.', e-mail: '.$email);
            }

            // success! Let's return the DTO:
            return new UserDTO($user_data['user_id'], $email, $password, $yurlico_id, $organization_club_id);
        }

        $err_details = array();
        $errors = $this->tank_auth->get_error_message();
        foreach ($errors as $k => $v)
        {   $err_details['errors'][$k] = $this->lang->line($v);
        }

        log_message('error', "ERROR creating website USER ACCOUNT for y:$yurlico_id, org_id:$organization_club_id, email:$email\n".json_encode($err_details));
        return $err_details;
    }

    // returns UserDTO object.
    private function core_createWinService_account($organization_club_id, $yurlico_id)
    {
        $email      = $this->_createEmailAddressFor('svc-', $organization_club_id, $yurlico_id);
        $name_fio   = 'WinSvc (yur-'.$yurlico_id.', org-'.$organization_club_id.')';
        $description= 'WinSvc (yurlico #'.$yurlico_id.', org #'.$organization_club_id.')';

        $userDTO_or_array = $this->_createWebsite_account($yurlico_id, $organization_club_id, $email, $name_fio, $description, true, 10, NULL, SecuredMatrix::ROLE_SERVER_BERKANA);

        return $userDTO_or_array;
    }

    // returns UserDTO object.
    private function core_createCabinet_account($organization_club_id, $yurlico_id)
    {
        $email      = $this->_createEmailAddressFor('cab-', $organization_club_id, $yurlico_id);
        $name_fio   = 'Cabinet (yur-'.$yurlico_id.', org-'.$organization_club_id.')';
        $description= 'Cabinet (yurlico #'.$yurlico_id.', org #'.$organization_club_id.')';

        $userDTO_or_array = $this->_createWebsite_account($yurlico_id, $organization_club_id, $email, $name_fio, $description, true, 8, NULL, SecuredMatrix::ROLE_CABINET);
        
        return $userDTO_or_array;
    }

    // retrieves cities where organizations where live.
    public function getOrganizationCities()
    {
        $cities = $this->utilitar_db->_getTableContents('berk_cities', 'order DESC, name');
        $res = array();

        // exclude redundant fields from the resultset:
        foreach ($cities as $city)
        {   $res[] = array('id' => $city->id, 'name' => $city->name);
        }

        return $res;
    }

    // returns object of null:
    public function getOrganizationAddressAndContacts($organization_id)
    {
        $this->db->select("address, CONCAT(email, ', тел.:', phone) AS contacts", FALSE);
        $this->db->where('id', $organization_id);

//        log_message('error', "SQL = ".print_r($this->db->_compile_select(), true));
        $query = $this->db->get('berk_organizations');
        return Utilitar_db::safe_resultRow($query);
    }

    public function getOrganizationsArray()
    {
/*
SELECT
    id, yurlico_id, name, city_id, address, CONCAT(email, ', тел.:', phone) AS contacts
FROM
    wt_berk_organizations
WHERE
    city_id > 0
ORDER BY name
 */
 
//         $db_prefix = $this->db->dbprefix;
//         $this->db->select("CONCAT(".$db_prefix."berk_yurlica.title, ', INN: ', ".$db_prefix."berk_yurlica.INN) AS name, ".$db_prefix."berk_yurlica.*", FALSE);

        $this->db->select("id AS club_id, yurlico_id, name, city_id, '59.939026' as lat, '30.315841' as lon, address, CONCAT(email, ', тел.:', phone) AS contacts", FALSE);
        $this->db->where('berk_organizations.city_id > 0');
        $this->db->where('berk_organizations.show_in_wizard <> 0');
        $this->db->order_by('berk_organizations.name');

//        log_message('error', "SQL = ".print_r($this->db->_compile_select(), true));
        $query = $this->db->get('berk_organizations');

        return Utilitar_db::safe_resultSetArray($query);

        return array(
                    array('city_id' => 1, 'club_id' => 111, 'name' => 'Карапуз.МСК',    'address' => 'ул.Авангардная, 1', 'contacts' => '333-22-11, email: club1@mail.ru'),
                    array('city_id' => 1, 'club_id' => 122, 'name' => 'Бейбёныш.МСК',   'address' => 'ул.Балканская, 2', 'contacts' => '333-22-22, email: club2@mail.ru'),
                    array('city_id' => 1, 'club_id' => 133, 'name' => 'Юннат.МСК',      'address' => 'ул.Вавиловых, 3', 'contacts' => '333-22-33, email: club3@mail.ru'),

                    array('city_id' => 2, 'club_id' => 211, 'name'  => 'Развивашки.СПб','address' => 'ул.Гаванская, 4', 'contacts' => '333-22-44, email: club4@mail.ru'),
                    array('city_id' => 2, 'club_id' => 222, 'name'  => 'ПетрПервый.СПб','address' => 'ул.Дегтярева, 5', 'contacts' => '333-22-55, email: club5@mail.ru'),
                    array('city_id' => 2, 'club_id' => 233, 'name'  => 'СмартКидз.СПб', 'address' => 'ул.Зеленина, 6',  'contacts' => '333-22-66, email: club6@mail.ru'),
                    array('city_id' => 2, 'club_id' => 244, 'name'  => 'Зайкодром.СПб', 'address' => 'ул.Калинина, 7',  'contacts' => '333-22-77, email: club7@mail.ru'),

                    array('city_id' => 3, 'club_id' => 311, 'name'  => 'Чудовеки.ЕКА',  'address' => 'ул.Ленина, 8',    'contacts' => '333-22-88, email: club8@mail.ru'),
                    array('city_id' => 3, 'club_id' => 322, 'name'  => 'Пистончики.ЕКА','address' => 'ул.Лесная, 9',    'contacts' => '333-22-99, email: club9@mail.ru'),

                    array('city_id' => 4, 'club_id' => 411, 'name'  => 'Ёлочки.НСБ',    'address' => 'ул.Макаренко, 10','contacts' => '444-99-11, email: club91@mail.ru'),
                    array('city_id' => 4, 'club_id' => 422, 'name'  => 'Кедровики.НСБ', 'address' => 'ул.Нежинская, 11','contacts' => '444-99-22, email: club92@mail.ru'),
                    array('city_id' => 4, 'club_id' => 433, 'name'  => 'Пущинцы.НСБ',   'address' => 'ул.Речная, 12',   'contacts' => '444-99-33, email: club92@mail.ru'),
                );
    }



    private function get_visit_row($client_id, $yurlico_id, $organization_id, $actual_day_id)
    {
        if ($client_id > 0 && $yurlico_id > 0 && $organization_id > 0 && $actual_day_id > 0)
        {
            $this->db->where(array('yurlico_id'=> $yurlico_id, 'organization_id' => $organization_id, 'schedule_id' => $actual_day_id, 'client_id'=> $client_id));
            $query = $this->db->get('berk_clients_visits');

            return Utilitar_db::safe_resultRow($query);
        }

        return null;
    }

    private function get_visit_row_telegram($yurlico_id, $organization_id, $subscriptionElementId)
    {
        if ($yurlico_id > 0 && $organization_id > 0 && $subscriptionElementId > 0)
        {
            $this->db->where(array('yurlico_id'=> $yurlico_id, 'organization_id' => $organization_id, 'id' => $subscriptionElementId,));
            $query = $this->db->get('berk_clients_visits');

            return Utilitar_db::safe_resultRow($query);
        }

        return null;
    }

    //
    // Function adds "mobileVersion" and "mobileVisited" fields from the local DB to the JSON data passed in.
    // (Berkana misses those fields, obviously)
    //
    //  N.B. Pay attention to arrays' items references used!
    //
    private function enrichJSON_by_DB_mobileVersion_mobileVisited(&$json_PlannedDailyVisits)
    {
        log_message('error', "TTT JSON ENRICH: BEFORE:\n".print_r($json_PlannedDailyVisits, true));

        $json_visits_reformated = array();
        // reformat the data structure:
        foreach ($json_PlannedDailyVisits as &$json_visit)
        {   $json_visits_reformated[$json_visit['id']] = &$json_visit; // this format used to ease later update
        }

        //----------------------------------------------------------------------------+
        // inquiry the DB for same (as at JSON) records:
        $visits_ids = array_keys($json_visits_reformated);

        $this->dbfilter_yurlico_and_organization(Util_berkana_data::TBL_PLANNED_VISITS); // NOTE: this call is mandatory to be present when accessing berkana assests!
        $this->db->where_in('id', $visits_ids);
        $query      = $this->db->get(Util_berkana_data::TBL_PLANNED_VISITS);
        $db_visits  = Utilitar_db::safe_resultSet($query);
        //----------------------------------------------------------------------------|

        log_message('error', "TTT db_visits BEFORE enriching by mobileVersion/mobileVisited".print_r($db_visits, true));


        // enriching JSON data: iterate over "$db_visits" instead of "$json_visits_reformated": to avoid redundant call of array_key_exists() of for "$json_visits_reformated".
        foreach ($db_visits as $db_visit)
        {
            $elem = &$json_visits_reformated[$db_visit->id];
            $elem['mobileVersion'] = $db_visit->mobileVersion;
            $elem['mobileVisited'] = $db_visit->mobileVisited;
        }

        log_message('error', "TTT json_visits_reformated AFTER enriching by mobileVersion/mobileVisited".print_r($json_visits_reformated, true));
    }

    //
    // Function also acceps just single item (via client_id, $actual_day_id): useful for updates coming from mobile app.
    public function alignVersionsAndValues_MobileAndBerkana($yurlico_id, $organization_id, $json_PlannedDailyVisits, $client_id = null, $actual_day_id = null)
    {
        $visits = array(); // this must be an array of arrays - not objects.

        // manually fill in the rows set: the alignment event has been raised by mobile application (so there are already mobileVersion and mobileVisited fields passed in).
        if ($client_id > 0 && $actual_day_id > 0)
        {   $visit = $this->get_visit_row($client_id, $yurlico_id, $organization_id, $actual_day_id);
            if ($visit) // do not add NULL visits. Also convert to the array.
            {   $visits[] = array(  'id'            => $visit->id,
                                    'client_id'     => $visit->client_id,
                                    'schedule_id'   => $visit->schedule_id,

                                    'version'       => $visit->version,
                                    'visited'       => $visit->visited,
                                    'mobileVersion' => $visit->mobileVersion,
                                    'mobileVisited' => $visit->mobileVisited,

                                    'yurlico_id'    => $visit->yurlico_id,
                                    'organization_id' => $visit->organization_id,
                                    );
            }
        }
        else if (null != $json_PlannedDailyVisits)
        {   // i.e. the alignment event has been raised by Berkana server.
            $this->enrichJSON_by_DB_mobileVersion_mobileVisited($json_PlannedDailyVisits);

            // here is what we need from Berkana:
            $visits = $json_PlannedDailyVisits;
        }

//        log_message('error', "INPUT visit(s) (clid:$client_id, actual_day_id:$actual_day_id)=\n".print_r($visits, true));

        $res = $this->separateVisitsByVersions($visits);
//        log_message('error', "VERSIONS ALIGNMENT RESULT:\n".print_r($res, true));
        $berkana_over_mobile = $res['berkana_over_mobile'];
        $mobile_over_berkana = $res['mobile_over_berkana'];

        // let's update the data finally:
        $this->overwriteVisits($berkana_over_mobile, true, $yurlico_id, $organization_id);
        $this->overwriteVisits($mobile_over_berkana, false, $yurlico_id, $organization_id);
    }

    //  returns true if updated.
    // it is designed for Telegram visits marking only. Is designed as a MORE CORRECT VERSION of alignVersionsAndValues_MobileAndBerkana() because it uses "$subscriptionElementId" and not unknown "$actual_day_id".
    // The "$subscriptionElementId" 1:1 identifies both the visit date/time and the client, while the "$actual_day_id" possibly no. Also "$client_id" is redundant here so we got rid of that here.
    public function alignVersionsAndValues_MobileAndBerkana_telegram($yurlico_id, $organization_id, $subscriptionElementId)
    {
        // manually fill in the rows set: the alignment event has been raised by mobile application (so there are already mobileVersion and mobileVisited fields passed in).
        $visit = $this->get_visit_row_telegram($yurlico_id, $organization_id, $subscriptionElementId);
        if (!$visit)
        {   // we do not NULL visits!
             log_message('error', "---- ERROR: we failed getting a visit via get_visit_row_telegram(y:$yurlico_id, orgid:$organization_id, subscriptionElementId:$subscriptionElementId), so skipping marking process!");
             return false;
        }

        // let's store that single visit into the array: for Android-code backward compatibility :(
        $visits = array();
        $visits[] = array(  'id'            => $visit->id,
                            'schedule_id'   => $visit->schedule_id,

                            'version'       => $visit->version,
                            'visited'       => $visit->visited,
                            'mobileVersion' => $visit->mobileVersion,
                            'mobileVisited' => $visit->mobileVisited,

                            'yurlico_id'    => $visit->yurlico_id,
                            'organization_id' => $visit->organization_id,
                            );

//        log_message('error', "INPUT visit(s) (clid:$client_id, actual_day_id:$actual_day_id)=\n".print_r($visits, true));

        $res = $this->separateVisitsByVersions($visits);
//        log_message('error', "VERSIONS ALIGNMENT RESULT:\n".print_r($res, true));
        $berkana_over_mobile = $res['berkana_over_mobile'];
        $mobile_over_berkana = $res['mobile_over_berkana'];

        // let's update the data finally:
        $this->overwriteVisits($berkana_over_mobile, true, $yurlico_id, $organization_id);
        $this->overwriteVisits($mobile_over_berkana, false, $yurlico_id, $organization_id);

        return true;
    }

    //
    // Overwrites visits from/to mobile and Berkana, aligning them based on versions difference.
    private function overwriteVisits(&$visits, $bBerkanaOverMobile, $yurlico_id, $organization_id)
    {
        if (0 == count($visits))
        {   log_message('error', "overwriteVisits(bBerkanaOverMobile=".($bBerkanaOverMobile?'true':'false')."): no visits to align for. Exiting.");
            return;
        }

        // allocate once to avoid memory fragmentation. Also init by constant $yurlico_id, $organization_id values:
        $where_array = array(   'yurlico_id'        => $yurlico_id,
                                'organization_id'   => $organization_id,
                                'schedule_id'       => -1,
                                'id'                => -1, // in fact this is a "membershipelement_id".
                                );
        $data = array();


        log_message('error', "overwriteVisits(bBerkanaOverMobile=".($bBerkanaOverMobile?'true':'false')."): all VISITS to ALIGN:\n".print_r($visits, true));
        foreach ($visits as $visit)
        {
            $where_array['schedule_id']     = $visit['schedule_id']; // $visit->actual_day_id;
            $where_array['id']              = $visit['id'];

            $this->db->where($where_array);

            // Let's go for berkana-over-mobile:
            if ($bBerkanaOverMobile)
            {   $data['mobileVersion'] = $visit['version'];
                $data['mobileVisited'] = $visit['visited'];
            }
            else // if no, then go for mobile-over-berkana:
            {   $data['version'] = $visit['mobileVersion'];
                $data['visited'] = $visit['mobileVisited'];

                // Mobile version holds a tri-state variable. But since we need to supply this to Berkana offline, let's do the trick:
                // const STATE_NOT_VISITED_NO_GOOD_REASON  = 0; // НЕ посетил/-а (неуважительная причина)
        //    const STATE_VISITED                     = 1; // посетил/-а.
        //    const STATE_NOT_VISITED_GOOD_REASON     = 2; // НЕ посетил/-а (уважительная причина)

                // NOTE that we're checking the value of ALREADY OVERWRITTEN variable (adopted from tri-state "mobileVisited" field)!
                if (WizardTrainerChecker::STATE_VISITED != $data['visited'])
                {   // QUESTION: shall I mark "$visited_tristate" as zero in this case? For Berkana local DB compatibility...
                    $data['nvgr'] = (WizardTrainerChecker::STATE_NOT_VISITED_GOOD_REASON == $data['visited']) ? 1 : 0;
                    $data['visited'] = 0; // enforce it to zero.
                }
                else
                {   $data['nvgr'] = 0;
                }
            }

            //----------------------------------------------------------+
            // DEBUG logging!!!
            if ($bBerkanaOverMobile)
            {
                log_message('error', "QQQ overwriteVisits(bBerkanaOverMobile=TRUE)=".print_r($data, true)."\nwhere array=".print_r($where_array, true));
            }
            else
            {   log_message('error', "QQQ overwriteVisits(bBerkanaOverMobile=FALSE)=".print_r($data, true)."\nwhere array=".print_r($where_array, true));            
            }
            //----------------------------------------------------------|

            $this->db->update('berk_clients_visits', $data);
        }
    }


    //
    // The core comparison (of visits & versions) logic is here.
    // returns two arrays holding items to be DB-processed later.
    private function separateVisitsByVersions(&$visits)
    {
        $berkana_over_mobile = array();
        $mobile_over_berkana = array();
        foreach ($visits as $visit)
        {
            //  The comparison algorithm is:
            //    if (berkana.version > mobile.version) then OVERWRITE mobile.version (together with mobile.value, of course).
            //    else if (berkana.version < mobile.version) then OVERWRITE berkana.version (together with berkana.value, of course).
            //    else if (berkana.version == mobile.version) and (berkana.visited != mobile.visited) then decide based on TIMESTAMP OR just IGNORE MOBILE.

            // 'version' 'visited'
            // 'mobileVersion' 'mobileVisited'
            if ($visit['version'] > $visit['mobileVersion']) // we must align versions in spite of if "values" are equal or not.
            {   $berkana_over_mobile[] = $visit;
            }
            else if ($visit['mobileVersion'] > $visit['version']) // we must align versions in spite of if "values" are equal or not.
            {   $mobile_over_berkana[] = $visit;
            }
            else if (($visit['version'] == $visit['mobileVersion']) && ($visit['visited'] != $visit['mobileVisited'])) // i.e. makes sense updating only if visited are not equal to each other.
            {   $berkana_over_mobile[] = $visit; // berkana takes precedence over mobile.
            }
        }
        
        return array('berkana_over_mobile' => $berkana_over_mobile, 'mobile_over_berkana' => $mobile_over_berkana);
    }


    /*private function get_users_by_ids($array_user_IDs)
    {
        $this->db->where_in('id', $array_user_IDs);
        $query = $this->db->get('users');

        return Utilitar_db::safe_resultSet($query);
    }*/


    //
    // Gets access_timestamp and operation_type by user_id & org_id.
    // Sometimes same user can be transfered between differnt organizations.
    //
    //  NOTE: $bSuccess might be passed null, then it won't be used for comparison.
    private function get_last_operation__by_userid($user_id, $organization_id, $bSuccess = null, $op_code = null)
    {
/*
SELECT access_timestamp, operation_type
FROM wt_sys_devices_connection_trace
WHERE
	user_id=75
	AND operation_type > 0
	AND organization_id = 5
ORDER BY access_timestamp DESC
LIMIT 1
 */
        $this->db->select('access_timestamp, operation_type');
        $this->db->where('user_id', $user_id);
        $this->db->where('organization_id', $organization_id);

        // nulls are also supported (!), so we use strict comparison here:
        if (true === $bSuccess)
        {   $this->db->where('operation_type > 0', NULL);
        }
        else if (false === $bSuccess)
        {   $this->db->where('operation_type <= 0', NULL);
        }
        else if (null == $bSuccess)
        {   ; // this is the case when we do not care if op_code was successful or not.
        }

        if ($op_code)
        {   $op_code = intval($op_code);

            if (null == $bSuccess) // if we do not care if op_code was successfull or not, then filter by *absolute* values:
            {   $this->db->where('ABS(operation_type)', abs($op_code));
            }
            else
            {   $this->db->where('operation_type', $op_code);
            }
        }

        $this->db->order_by('access_timestamp DESC');
        $this->db->limit(1);

        $query = $this->db->get('sys_devices_connection_trace');
        return Utilitar_db::safe_resultRow($query);
    }

    public function STATS_get_club_health($organization_id)
    {
        $organization = $this->getOrganization($organization_id);
        if (!$organization)
        {   return NULL; // TODO: modify to return the stub.
        }

        $res_accounts = array();
        $accounts = $this->getOrganizationUserAccounts($organization_id);
        foreach ($accounts as $account)
        {   $res_accounts[] = array(
                'role'          => 'some_name',
                'username'      => $account->username, // we migh also call $this->get_last_operation__by_userid(..., Json_base::OP_LOGIN) for really true device access.
                'last_login'    => $account->last_login, // we migh also call $this->get_last_operation__by_userid(..., Json_base::OP_LOGIN) for really true device access.
                'last_success'  => $this->get_last_operation__by_userid($account->user_id, $organization->id, true),
                'last_failure'  => $this->get_last_operation__by_userid($account->user_id, $organization->id, false),
            );
        }

//        log_message('error', "res_accounts=".print_r($res_accounts, true));

    // Stats needed:
    //      Organization accounts: name and type (service, client, cabinet and trainer) and last activity and opcode (!)
    //      Data i/o from/to wtsync service (success and failure): date, opcode
    //      Data i/o from/to mobile cabinet (success and failure): date, opcode
    //      Last i/o from /to mobile client(s): date, opcode
    //      Client accounts count: verified and anonymous
    //      Devices count for:
    //          client verified
    //          client anonymous
    //          cabinet
    //          trainer
    //      Cabinet's (if any) last call of "/presence" date

        // get devices specific to organization only:
        $res_devices = array();
        $devices = $this->get_devices__admin_mode($organization->yurlico_id, $organization->id);
        foreach ($devices as $device)
        {
            $res_devices[] = array(
                'did'           => $device->did,
                //'roles'         => undefined comma-separated string!, // same device might run multiple accounts (mobile-only)
                'last_login'    => $this->get_last_operation__by_userid($device->user_id, $organization_id, null, Json_base::OP_LOGIN),// NOTE: device login time != user_account login time!!! Because accounts are *shared*!!!
                'last_success'  => $this->get_last_operation__by_userid($device->user_id, $organization_id, true),
                'last_failure'  => $this->get_last_operation__by_userid($device->user_id, $organization_id, false),
                'failures_count'=> $this->get_device_failures_count($device->id),
            );
        }

        return array(
            'accounts'  => $res_accounts,

            // Note that devices might have no username (if mobile anonymous).
            // Also different devices might SHARE same account (!). So devices' stats is more descriptive than accounts stats.
            'devices'   => $res_devices,
        );
    }

    // note: input parameter is not "did" but "sys_devices->id" instead!
    private function get_device_failures_count($device_id)
    {
        $this->db->select('count(id) as total_failures_count');
        $this->db->where('sys_device_id', $device_id);

        $this->db->where('operation_type <= 0', NULL);
        $query  = $this->db->get('sys_devices_connection_trace');

        if ($query->num_rows() == 0)
        {   return 0;
        }

        $row = $query->row();
        return $row->total_failures_count;
    }


    public function STATS_get_failed_logins()
    {
        $this->db->select('count(id) as total_failures_count');
        $this->db->where('sys_device_id', $device_id);

        $this->db->where('operation_type <= 0', NULL);
        $query  = $this->db->get('sys_devices_connection_trace');

        if ($query->num_rows() == 0)
        {   return 0;
        }

        $row = $query->row();
        return $row->total_failures_count;
    }

    public function get_schedule_and_trainings_by_schedule_IDs($schedule_IDs)
    {
/*
SELECT wt_berk_schedule.id, wt_berk_coursetypes.id AS coursetype_id, wt_berk_coursetypes.name 
FROM wt_berk_coursetypes
LEFT JOIN wt_berk_schedule ON wt_berk_schedule.`coursetype_id` = wt_berk_coursetypes.id
WHERE
wt_berk_schedule.id IN (15787, 15830, 15975, 15975, 15978, 15667, 15622, 15644, 15667)
AND (wt_berk_schedule.yurlico_id = 37 AND wt_berk_schedule.organization_id = 5)
AND (wt_berk_coursetypes.yurlico_id = 37 AND wt_berk_coursetypes.organization_id = 5)
*/
        if ($schedule_IDs && is_array($schedule_IDs) && (count($schedule_IDs) > 0))
        {   $this->db->where_in('berk_schedule.id', $schedule_IDs);
        }
        else
        {   return array();
        }

        $this->db->select('berk_schedule.id, berk_coursetypes.id AS coursetype_id, berk_coursetypes.name');
        $this->db->join('berk_schedule', 'berk_schedule.coursetype_id = berk_coursetypes.id', 'left');

        $db_prefix = $this->db->dbprefix;
        $this->dbfilter_yurlico_and_organization(array($db_prefix.'berk_schedule', $db_prefix.'berk_coursetypes')); // NOTE: this call is mandatory to be present when accessing berkana assests!

        $query = $this->db->get('berk_coursetypes');

        return Utilitar_db::safe_resultSet($query);
    }

    public function get_family_IDs__for_children($child_IDs)
    {
//SELECT id, family_id
//FROM wt_berk_clients
//WHERE yurlico_id = 37 AND organization_id = 5
//AND family_id IN (111, 222, 333)
        if ($child_IDs && is_array($child_IDs) && (count($child_IDs) > 0))
        {   $this->db->where_in('berk_clients.id', $child_IDs);
        }
        else
        {   return array();
        }

        $this->db->select('id, family_id');// id is "client id".
        $this->dbfilter_yurlico_and_organization($this->db->dbprefix.'berk_clients'); // NOTE: this call is mandatory to be present when accessing berkana assests!

        $query = $this->db->get('berk_clients');

        return Utilitar_db::safe_resultSet($query);
    }

    public function get_training_type_description_by_crc32($yurlico_id, $crc32_salted)
    {
        $this->db->where('crc32_salted', $crc32_salted);
        $this->db->where('yurlico_id', $yurlico_id); // this is be default.
        $this->db->limit(1);

        $query = $this->db->get('tr_types_descriptions');
        $row = Utilitar_db::safe_resultRow($query);

        return $row ? $row->tr_type_description : '';
    }

    public static function hash_trtype_name($yurlico_id, $tr_type_name)
    {
        return crc32(Util_berkana_data::TRTYPE_SALT.$yurlico_id.$tr_type_name); // contact the salt, yurlicoid and the value itself.
    }

    // we allow passing no $organization_id, calculating the stats for entire yurlico. Clubs ignored, but seems that's not an issue.
    public function get_weekly_schedule_by_weekdays(&$week_schedule, $yurlico_id, $organization_id = 0)
    {
        $res = array();
        $coursetype_ids = array();
        foreach ($week_schedule as $training) // trainings group by weekdays and by coursetype_id.
        {   $week_day_number = date('N', strtotime($training->planneddate));
            $res[$week_day_number][$training->coursetype_id][] = $training; // collect by weekday number.

            $coursetype_ids[] = $training->coursetype_id; // collect trainings fit into the upcoming week.
        }

        //public function get_coursetypes_array_filtered($yurlico_id, $organization_id, $bIncludeRemoved, $training_type_IDs, $exclude_stopwords = true, $exclude_duplications = true)

        $coursetypes_array  = $this->get_coursetypes_array_filtered($yurlico_id, $organization_id, true, $coursetype_ids);
        return array('week_days' => $res, 'coursetypes_array' => $coursetypes_array);
    }

    public function split_personal_trainings_by_weekdays(&$personal_trainings)
    {
        $res = array();
        $coursetype_ids = array();
        foreach ($personal_trainings as $training) // trainings group by weekdays and by coursetype_id.
        {   $week_day_number = date('N', strtotime($training->planneddate));
            $res[$week_day_number][] = $training; // collect by weekday number.

            $coursetype_ids[] = $training->coursetype_id; // collect trainings fit into the upcoming week.
        }

        return $res;

        // $coursetypes_array  = $this->get_coursetypes_array_filtered($yurlico_id, $organization_id, true, $coursetype_ids);
        //return array('week_days' => $res, 'coursetypes_array' => $coursetypes_array);
    }

    public function get_personal_trainings_ext($local_userid, $days_before = 0, $days_after = 7)
    {

        // only for logged-in user this data makes sense:

        $user   = $this->users->get_user_by_id_full($local_userid);
        $family = $this->get_family_by_local_user_id($local_userid, $user->yurlico_id, $user->organization_id);

//        log_message('error', "USER: ".print_r($user, true));
//        log_message('error', "FAMILY: ".print_r($family, true));

        if (!$user || !$family)
        {   log_message('error', "get_personal_trainings_ext(local_userid:$local_userid): no Berkana user or family found!!");
            return array();
        }

        $family_trainings = $this->get_family_trainings_for_date($family->id, $days_before, $days_after, $user->yurlico_id, $user->organization_id);
//        log_message('error', "FAMILY_TRAININGS: ".print_r($family_trainings, true));

        if ($this->get_children_count($family_trainings, 'client_id') > 1)
        {   ;//$this->rename_trainings_personal_for_multiple_children($family->id, $family_trainings, $user->yurlico_id, $user->organization_id);
        }

        return $family_trainings;
        /*
//                log_message('error', 'FAMILY_TRAININGS ('.Json_base::get_current_yurlico_id__org_id_string().', f:'.$family->id.'): '.count($family_trainings)." retrieved.\n".print_r($family_trainings, true));
//                log_message('error', 'FAMILY_TRAININGS ('.Json_base::get_current_yurlico_id__org_id_string().', f:'.$family->id.'): '.count($family_trainings)." retrieved:\n".print_r($family_trainings, true));

        $trainings_personal = array(); // NOTE: club's schedule is retrieved via "/schedule" method separately!
        foreach ($family_trainings as &$family_training)
        {
            $trainings_personal[] = array(
                'id'        => $family_training->membershipelement_id,
                'club_id'   => $user->organization_id,
                'coursetype_id' => $family_training->coursetype_id,
                'training'      => $family_training->name,

                'trainer_id'=> $family_training->teacher_id,
                'cabinet_id'=> $family_training->room_id,

                'state'     => '0', // canceled if == 1. If 0 then that training has NOT BEEN CANCLEDED.

                'planneddate' => $family_training->planneddate,
                'plannedstarttime' => $family_training->plannedstarttime,
                'plannedfinishtime' => $family_training->plannedfinishtime,
            );
        }

        log_message('error', "FINAL RESUULT trainings_personal=".print_r($trainings_personal, true));
        return $trainings_personal;*/
    }

    //
    //  Function provides data for "get_planned_visits_for_date_v2().
    //  NOTE: version #2 returns NO COUTSETYPE_NAME!!!
    //
    //  IMPORTANT:
    //  Ensure to update WHERE conditions simultaneously with "getRoomTrainingsArray()" or you will face data discrepancy in a result set!
    public function rewrittenQuery_v2($yurlico_id, $organization_id, $client_IDs, $db_field_name__VISITED, $db_field_name__VERSION, $date_options, $modified_only, $cabinet_id, $schedule_id)
    {
/*
SELECT
DISTINCT wt_berk_schedule.coursetype_id, IFNULL(wt_berk_clients_visits.mobileVisited, 0) AS visited,
IFNULL(wt_berk_clients_visits.mobileVersion, 0) AS VERSION,
wt_berk_clients_visits.nvgr,
wt_berk_clients_visits.id,
wt_berk_clients_visits.client_id, -- wt_berk_clients_visits.yurlico_id AS clientvisits_yurlico_id,
wt_berk_clients.firstname, wt_berk_clients.lastname,
wt_berk_clients_visits.schedule_id,
wt_berk_schedule.*
FROM  wt_berk_clients_visits
LEFT JOIN wt_berk_schedule ON wt_berk_clients_visits.schedule_id=wt_berk_schedule.id
LEFT JOIN wt_berk_clients ON wt_berk_clients.id = wt_berk_clients_visits.client_id
WHERE
wt_berk_schedule.id = 24615 AND         -- is optional!
client_id IN (1041, 1042, 1043) AND     -- is optional!
(wt_berk_clients_visits.yurlico_id = 37 AND wt_berk_clients_visits.organization_id = 5)
AND (wt_berk_schedule.yurlico_id = 37 AND wt_berk_schedule.organization_id = 5)
AND (wt_berk_schedule.planneddate BETWEEN CURRENT_DATE - INTERVAL '0' DAY AND CURRENT_DATE + INTERVAL '7' DAY)
AND ( ( (`wt_berk_schedule`.`removed` = 0) OR (`wt_berk_schedule`.`removed` IS NULL) ) AND ( (`wt_berk_schedule`.`canceled` = 0) OR (`wt_berk_schedule`.`canceled` IS NULL) ) )
AND ( (`wt_berk_clients_visits`.`removed` = 0) OR (`wt_berk_clients_visits`.`removed` IS NULL) )
ORDER BY wt_berk_schedule.planneddate
*/
        log_message('error', "rewrittenQuery_v2(y:$yurlico_id, orgid:$organization_id, db_field_name__VISITED:$db_field_name__VISITED, db_field_name__VERSION:$db_field_name__VERSION, dates: ".print_r($date_options, true).", modified_only:$modified_only, cabinet_id:$cabinet_id, schedule_id:$schedule_id) for clients: ".print_r($client_IDs, true));
        $db_prefix = $this->db->dbprefix;

        $this->db->select(' berk_schedule.coursetype_id, IFNULL('.$db_prefix.'berk_clients_visits.'.$db_field_name__VISITED.', 0) AS visited,
                            IFNULL('.$db_prefix.'berk_clients_visits.'.$db_field_name__VERSION.', 0) as version,
                            berk_clients_visits.nvgr,
                            berk_clients_visits.id,
                            berk_clients_visits.client_id,
                            berk_clients.firstname, berk_clients.lastname,
                            berk_clients_visits.schedule_id ', FALSE);
        $this->db->distinct();
        $this->db->from('berk_clients_visits');
        $this->db->join('berk_schedule',    'berk_clients_visits.schedule_id = berk_schedule.id',   'left');
        $this->db->join('berk_clients',     'berk_clients.id = berk_clients_visits.client_id',   'left');

        // get only MODIFIED fields. Makes sense for WTService "/get_data/1" calls:
        if ($modified_only)
        {   $this->db->where('berk_clients_visits.'.$db_field_name__VERSION.' > 0');
        }

        if (count($client_IDs) > 0)
        {   $this->db->where_in('berk_clients_visits.client_id', $client_IDs);
        }

        if ($cabinet_id > 0)
        {   $this->db->where('berk_schedule.plannedroom_id', $cabinet_id);
        }

        if ($schedule_id > 0)
        {   $this->db->where('berk_schedule.id', $schedule_id);
        }

        // NOTE: this call is mandatory to be present when accessing berkana assests!
        $this->dbfilter_yurlico_and_organization_multipletables(array('berk_clients_visits', 'berk_schedule', 'berk_clients'), $yurlico_id, $organization_id);

//        $this->db->where('berk_coursetypes.active > 0'); // даже неактивные показываются. А вот удалённые (removed==true) уже не должны показываться.

        if (isset($date_options['plannedstarttime']))
        {
            $this->db->where('berk_schedule.plannedstarttime', $date_options['plannedstarttime']); // MySql  works ok with both versions: '09:30' and '09:30:00'
        }

        if (isset($date_options['days_before_and_after']))
        {
            $days_before= $date_options['days_before_and_after']['days_before']; // days count relative to today.
            $days_after = $date_options['days_before_and_after']['days_after']; // days count relative to today.

            $this->db_where_date__relative2today($db_prefix.'berk_schedule', 'planneddate', $days_before, $days_after);
        }
        else if (isset($date_options['between_dates']))
        {
            $date_from = $date_options['between_dates']['date_from'];
            $date_till = $date_options['between_dates']['date_till'];
            $this->db_where_date__between($db_prefix.'berk_schedule', 'planneddate', $date_from, $date_till);
        }

        $this->_dbNotRemoved ($db_prefix.'berk_schedule');
        $this->_dbNotCanceled($db_prefix.'berk_schedule');
        $this->_dbNotRemoved ($db_prefix.'berk_clients_visits');

        $this->db->order_by('berk_schedule.planneddate');

        $query = $this->db->get();

        return Utilitar_db::safe_resultSet($query);
    }

    // sample calls:
    //      get_visits_between('2020-02-25', '17:00', 93, 63, 'visited', 'version', 77, null, false); // at 17:00, for specific teacher, at any cabinet, get visits' all versions (including the ones not changed).
    //      get_visits_between('2020-02-25', null, 93, 63, 'visited', 'version', 77, null, false);  // at any time, for specific teacher, at any cabinet, get visits' all versions (including the ones not changed).
    //      get_visits_between('2020-02-25', null, 93, 63, 'mobileVisited', 'mobileVersion', 77, null, false);// at any time, mobile-version, for specific teacher, at any cabinet, get visits' all versions (including the ones not changed).
    //      get_visits_between('2020-02-25', null, 93, 63, 'mobileVisited', 'mobileVersion', null, 3, true); // at any time, any teacher, for specific cabinet, get visits' only changed versions.
    public function get_visits_between($dates_between, $start_time, $yurlico_id, $organization_id, $db_field_name__VISITED, $db_field_name__VERSION, $teacher_id, $cabinet_id, $modified_only)
    {
/*
SELECT clients.firstname, clients.lastname,
    sched.teacher_id, sched.coursetype_id, sched.planneddate,
    TIME_FORMAT(sched.plannedstarttime, "%H:%i") AS plannedstarttime,
    sched.plannedroom_id, sched.visitedcount, sched.plannedchildrencount, sched.removed, sched.canceled,
    visits.visited, visits.version,
    IFNULL(visits.'.$db_field_name__VISITED.', 0) AS visited,
    visits.mobileVersion, visits.mobileVisited
    visits.nvgr, visits.id AS subscriptionElementId, visits.client_id, visits.schedule_id,
FROM wt_berk_schedule sched
LEFT JOIN wt_berk_clients_visits visits ON visits.schedule_id = sched.id
LEFT JOIN wt_berk_clients clients ON clients.id = visits.client_id
WHERE (sched.yurlico_id = 93 AND sched.organization_id = 63)
    AND (clients.yurlico_id = 93 AND clients.organization_id = 63)
    AND (visits.yurlico_id = 93 AND visits.organization_id = 63)
    AND sched.planneddate = '2020-02-25'
    AND sched.plannedstarttime = '17:00'
    AND sched.teacher_id = 77
ORDER BY sched.coursetype_id, clients.firstname
*/
        log_message('error', 'XXX get_visits_between(dates:'.print_r($dates_between, true).", start_time:'$start_time', y:$yurlico_id, orgid:$organization_id, visited:$db_field_name__VISITED, version:$db_field_name__VERSION, teacher_id:$teacher_id, cabinet_id:$cabinet_id, modified_only:$modified_only)");

        $this->db->select(' clients.firstname, clients.lastname,

                            sched.teacher_id, sched.coursetype_id, sched.planneddate, TIME_FORMAT(sched.plannedstarttime, "%H:%i") AS plannedstarttime,
                            sched.plannedroom_id, sched.visitedcount, sched.plannedchildrencount, sched.removed, sched.canceled,

                            IFNULL(visits.'.$db_field_name__VISITED.', 0) AS visited,
                            IFNULL(visits.'.$db_field_name__VERSION.', 0) as version,
                            visits.nvgr, visits.id AS subscriptionElementId,
                            visits.membershiptype_id,
                            visits.is_reservation,
                            visits.client_id, visits.schedule_id, visits.mobileVersion, visits.mobileVisited,
                            FALSE as last_lesson',
                            FALSE
                         );

        $this->db->from('berk_schedule sched');
        $this->db->join('berk_clients_visits visits',   'visits.schedule_id = sched.id',   'left');
        $this->db->join('berk_clients clients',         'clients.id = visits.client_id',   'left');

        if ($teacher_id > 0)
        {   $this->db->where('sched.teacher_id', $teacher_id);
        }

        if ($cabinet_id > 0)
        {   $this->db->where('sched.plannedroom_id', $cabinet_id);
        }

        // get only MODIFIED fields. Makes sense for WTService "/get_data/1" calls:
        if ($modified_only)
        {   $this->db->where('visits.'.$db_field_name__VERSION.' > 0');
        }

        // NOTE: this call is mandatory to be present when accessing berkana assests!
        $this->dbfilter_yurlico_and_organization_multipletables(array('visits', 'sched', 'clients'), $yurlico_id, $organization_id);

//        $this->db->where('berk_coursetypes.active > 0'); // даже неактивные показываются. А вот удалённые (removed==true) уже не должны показываться.

        if ($start_time)
        {   $this->db->where('sched.plannedstarttime', $start_time); // MySql  works ok with both versions: '09:30' and '09:30:00'
        }

        $this->db_where_date__between('sched', 'planneddate', $dates_between['date_from'], $dates_between['date_till']);

        $this->_getQueryNotRemovedAndNotCanceled('sched');
//        $this->_dbNotRemoved ('sched');
//        $this->_dbNotCanceled('sched');
        $this->_dbNotRemoved('visits');

        $this->db->order_by('sched.planneddate, sched.coursetype_id, clients.firstname');

        $query = $this->db->get();

        return Utilitar_db::safe_resultSet($query);
    }

    // differentiate abonements (by name post-fix) if family has numtiple children: postprocess the "coursename".
    public function rename_abonements_for_multiple_children($family_id, &$abonements, $yurlico_id, $organization_id)
    {
        $children           = $this->get_family_childrennames($family_id, $yurlico_id, $organization_id);
        $children_hashtable = Util_berkana::transformToHashTable($children);

        foreach ($abonements as &$abonement)
        {   $abonement->name .= ' ('.$children_hashtable[$abonement->child_id]->childname.')';
        }
    }

    // differentiate training names (by name post-fix) if family has numtiple children
    private function rename_trainings_personal_for_multiple_children($family_id, &$family_trainings, $yurlico_id, $organization_id)
    {
        $ci     = &get_instance();
        $children           = $this->get_family_childrennames($family_id, $yurlico_id, $organization_id);
        $children_hashtable = Util_berkana::transformToHashTable($children);

        foreach ($family_trainings as &$family_training)
        {   $family_training->name .= ' ('.$children_hashtable[$family_training->client_id]->childname.')';
        }
    }

    public function get_children_count(&$rowset, $child_id_rowname)
    {
        $children = array();
        foreach ($rowset as $row)
        {   $children[$row->$child_id_rowname] = 1;
        }

        return count($children);
    }


    // unfortunately abonement entity lies about remaining visits count. So we check each visit separately!
    public function get_upcoming_visits(&$extracted_abonements, $yurlico_id, $organization_id)
    {
        log_message('error', "extracted_abonements=".print_r($extracted_abonements, true));
        //	- get their IDs: berk_clients->id // ensure that real data
        //	- get all upcoming visits for them!
        //	- split by abonement training type

        $client_IDs = array();
        foreach ($extracted_abonements as $abonement)
        {   $client_IDs[$abonement->child_id] = $abonement->child_id;
        }
        $client_IDs = array_keys($client_IDs);

        $ci         = &get_instance();
        return $this->get_planned_visits_for_date_v2(Util_berkana_data::VISITS_TBL_MOBILE, 0, 55, $client_IDs, $yurlico_id, $organization_id);
/*
    extracted_abonements=Array
    (
        [0] => stdClass Object
        (
            [id] => 33373
            [child_id] => 1041
            [family_id] => 345
            [valid_from] => 2017-01-28 03:00:00
            [valid_till] => 2017-03-04 03:00:00
            [visits_passed] => 7
            [visits_count] => 10
            [visits_moved] => 0
            [name] => Ушу (И. Яценко)
        )
 */

    }

    // extracts *active* abonements only (i.e. wt_berk_abonements.date_end >= CURRENT_DATE):
    public function extract_abonements($yurlico_id, $organization_id, $family_id)
    {
        $ci         = &get_instance();

        $abonements = $this->get_family_abonements_ext($family_id, true, $yurlico_id, $organization_id);
        if ($this->get_children_count($abonements, 'child_id') > 1)
        {   $this->rename_abonements_for_multiple_children($family_id, $abonements, $yurlico_id, $organization_id);
        }

        return $abonements;
    }

    //
    // NOTE:    Berkana abonements contain WRONG "remaining visits count"!
    //          So we calculate by ourselves from "$upcoming_visits", split by client_id (child_id) - as thre could be multiple children in a family.
    public function recalculate_abonements_remaining_visits(&$abonements, &$upcoming_visits)
    {
        $green_flag = "\xF0\x9F\x93\x97 ";
        $red_flag   = "\xF0\x9F\x93\x95 ";
        $MINIMUM_VISITS = 1;
        $SEPARATOR = ')\n';

        $ctr    = 1;
        $res    = '';

        //-----------------------------------------------------------+
//        log_message('error', "UPCOMING_VISITS=".print_r($upcoming_visits, true));
        // count visits by each child_id (client_id) as there could be multiple in a family:
        $splitted = array();
        foreach ($upcoming_visits as $upcoming_visit)
        {   $splitted[$upcoming_visit->client_id][$upcoming_visit->coursetype_id][] = $upcoming_visit; // add more items there.
        }
        // so here we have "$splitted[client_id][course_type_id]"
        log_message('error', "SPLITTED=".print_r($splitted, true));
        //-----------------------------------------------------------|


        $ci     = &get_instance();
        foreach ($abonements as &$abonement)
        {
            $remaining_visits_count = 0;
            $child_courses = $splitted[$abonement->child_id];
            if (isset($child_courses[$abonement->coursetype_id]))
            {   $remaining_visits_count = count($child_courses[$abonement->coursetype_id]);
                $abonement->remaining_visits_count = $remaining_visits_count;
            }

            //  NOTE: Berkana fools about remaining visits count!!! So I use approach above.
            //  $remaining_visits_count = $abonement->visits_count - $abonement->visits_passed; // also pay attention to "$abonement->visits_moved"!
            $ending                 = Util_berkana_data::get_russian_ending($remaining_visits_count, '', 'а', 'ов');

            $flag = ($remaining_visits_count > $MINIMUM_VISITS ? $green_flag : $red_flag);
            $res .= $flag.$abonement->name.', '.$remaining_visits_count.' визит'.$ending.' (до '.$ci->utilitar_date->formatDateRu($abonement->valid_till).$SEPARATOR;
            $ctr++;
        }

        if (strlen($res) > 0)
        {   $res = substr($res, 0, -strlen($SEPARATOR));
        }

        return $res;
    }

    // возвращает окончания для числа "$number". Для женского ($b_male = false) или мужского ($b_male = true) рода.
    public static function get_russian_ending($number, $ending_1, $ending_234, $ending_5more = '')
    {
        $ending = '';
        switch ($number % 10)
        {
            case 0: // одна задачА, один шест_, одно солнцЕ
                $ending = $ending_5more;
                break;
            case 1: // одна задачА, один шест_, одно солнцЕ
                $ending = $ending_1;
                break;
            case 2: // две задачИ, два шестА, два солнцА
            case 3: // три задачИ, три шестА, три солнцА
            case 4: // четыре задачИ, четыре шестА, четыре солнцА
                $ending = $ending_234;
                break;
            case 5: // пять задач_, пять шестОВ, пять солнц_
            case 6: // пять задач_, пять шестОВ, пять солнц_
            case 7: // пять задач_, пять шестОВ, пять солнц_
            case 8: // пять задач_, пять шестОВ, пять солнц_
            case 9: // пять задач_, пять шестОВ, пять солнц_
                $ending = $ending_5more;
                break;
        }

        return $ending;
    }

    // mysql and Berkana have different date formats, so this method fixes that.
    public static function cloud_date_to_mysql_date($cloud_date_format, $date_cloud)
    {
        $tmp = DateTime::createFromFormat($cloud_date_format, $date_cloud);
        return $tmp->format('Y-m-d');
    }
}

// UPDATE wt_berk_clients_visits SET client_id = 9702, schedule_id = 35459, visited = 1, version = 1, removed = NULL, id = '90048'WHERE id = '38970'AND wt_berk_abonements.yurlico_id = '24'AND wt_berk_abonements.organization_id = '2'AND id = '39519'AND wt_berk_abonements.yurlico_id = '24'AND wt_berk_abonements.organization_id = '2'AND id = '40006'AND wt_berk_abonements.yurlico_id = '24'AND wt_berk_abonements.organization_id = '2'AND id = '90048'AND wt_berk_clients_visits.yurlico_id = '24'AND wt_berk_clients_visits.organization_id = '2'
