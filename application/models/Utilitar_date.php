<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH . '/controllers/admin/SecuredMatrix.php'); // greco.

class Utilitar_date extends CI_Model /* see http://codeigniter.com/wiki/Inserting_Multiple_Records_Into_a_Table/ */
{
	private $CI;

	function __construct()
	{
		parent::__construct();
	}

	// converts iso8601 to DateTime/timestamp format:
    public function convertDate(&$sape_date)
    {
        return date('Y-m-d H:i:s', strtotime($sape_date));
    }

    public static function ru_month_disabled($date, $long = false)
    {
        $ru_months = null;
        if ($long)
        {   $ru_months = array( 'stub', 'Январь', 'Февраль', 'Март', 'Амрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь' );
        }
        else
        {   $ru_months = array( 'stub', 'янв', 'фев', 'март', 'апр', 'май', 'июнь', 'июль', 'авг', 'сент', 'окт', 'ноя', 'дек' );
        }

        return $ru_months[date('n', strtotime($date))];
    }

    public static function ru_month_by_month_number($month_number, $long = false)
    {
        $ru_months = null;
        if ($long)
        {   $ru_months = array( 'stub', 'Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь' );
            //$ru_months = array( 'stub', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' );
        }
        else
        {   $ru_months = array( 'stub', 'янв', 'фев', 'март', 'апр', 'май', 'июнь', 'июль', 'авг', 'сент', 'окт', 'ноя', 'дек' );
            //$ru_months = array( 'stub', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' );
        }

        return $ru_months[$month_number];
    }

    public static function ru_month_by_month_number_v2($month_number)
    {
        $ru_months = array( 'stub', 'января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря' );

        return $ru_months[$month_number];
    }

    public static function ru_weekday_name($date)
    {
        $ru_weekdays = array( 'stub', 'пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс');
        //$ru_weekdays = array( 'stub', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun');
        $day_number = date('N', strtotime($date));

        return $ru_weekdays[$day_number];
    }

    public static function ru_weekday_name_by_dayno($day_number, $long_name, $lower_case)
    {
        $ru_weekdays = null;
        if ($long_name)
        {
            $ru_weekdays = array( 'stub', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота', 'воскресенье');
            //$ru_weekdays = array( 'stub', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
            return $lower_case ? mb_strtolower($ru_weekdays[$day_number]) : $ru_weekdays[$day_number];
        }
        else
        {   $ru_weekdays = array( 'stub', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс');
        }

        return $lower_case ? mb_strtolower($ru_weekdays[$day_number]) : $ru_weekdays[$day_number];
    }

    //
    // e.g. "1 day", "2 month", etc.
    //
    function getDateIncremented($the_date, $interval)
    {
        return strftime("%Y-%m-%d", strtotime("$the_date +$interval"));
    }

    //
    // e.g. "1 day", "2 month", etc.
    //
    function getDateDecremented($the_date, $interval)
    {
        return strftime("%Y-%m-%d", strtotime("$the_date -$interval"));
    }
    
    private function increment_date_by_one_second($the_date)
    {
        $H = date('H', strtotime($the_date));
        $i = date('i', strtotime($the_date));
        $s = date('s', strtotime($the_date));

        $Y = date('Y', strtotime($the_date));
        $m = date('m', strtotime($the_date));
        $d = date('d', strtotime($the_date));

         // +1 second. the overflow handled by date() correctly! :)
        $timestamp = mktime($H, $i, $s+1, $d, $m, $Y);
        return date('Y-m-d H:i:s', $timestamp);
    }

    public function formatDateRu($timestamp)
    {
        $search  = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
        $replace = array('января', 'февраля', 'марта', 'апреля', 'мая', 'июня','июля','августа','сентября','октября','ноября','декабря');

        $res = date("j M Y", strtotime($timestamp));
        return str_replace($search, $replace, $res);
    }

    // returns a string.
    // Note: "smartly" provides either minutes or hours with minutes (if difference > 60 minutes).
    // E.g. the call would be: $this->formatDurationSmart( $this->split_time_difference_by_hours_minutes( $this->$this->getTimeDifferenceInMinutes($time_from, $time_till) ) )
    public function formatDurationSmart($hours_minutes_array)
    {
        if (0 == $hours_minutes_array['hours'])
        {   return $hours_minutes_array['minutes'].' мин.';
        }

        if (0 == $hours_minutes_array['minutes'])
        {   return $hours_minutes_array['hours'].' ч.';
        }

        return $hours_minutes_array['hours'].' ч. '.$hours_minutes_array['minutes'].' мин.';
    }
    public function formatDurationSmart_from_to($time_from, $time_till)
    {
        $diff_minutes       = $this->getTimeDifferenceInMinutes($time_from, $time_till);
        $hours_minutes_array= $this->split_time_difference_by_hours_minutes($diff_minutes);

        return $this->formatDurationSmart($hours_minutes_array);
    }

    // returns array with hours and minutes.
    public function split_time_difference_by_hours_minutes($diff_minutes)
    {
        $hours  = floor($diff_minutes/60); //round down to nearest minute.
        $minutes= $diff_minutes % 60;

        return array('hours' => $hours, 'minutes' => $minutes);
    }

    public function time_events_as_training_types($title, &$events)
    {
        if (0 == count($events))
        {   return 'n/a';
        }
    }

    public function get_extended_list($coursetypes_array, &$wd_numbers, &$events)
    {
        return array();
    }

    // return 2-elements array: #0 is a flag if there are weekdays which are same as today. #1 is the list of weekdays themselves.
    public function get_weekdays_list(&$wd_numbers, &$events)
    {
        if (0 == count($events))
        {   return 'n/a';
        }

        $today_weekday_no   = date('N');
        $contains_today     = false;

        $res = array();
        foreach ($events as $event)
        {
            $weekday_no = date('N', strtotime($event->planneddate));
            $contains_today |= ($today_weekday_no == $weekday_no);

//            log_message('error', "PST (weekday_no=$weekday_no) = ".$event->planneddate.", ".$wd_numbers[$weekday_no]);
            $res[$weekday_no] = $wd_numbers[$weekday_no];
        }

        ksort($res);
        $vals = array_values($res);

        return array($contains_today, implode(', ', $vals));
    }


    public function shorten_list_of_time_events($title, &$events)
    {
        if (0 == count($events))
        {   return 'n/a';
        }

        $separator = ', ';
        $res = '';

        $prev_diff_minutes  = 0;
        $diff_minutes       = 0;

        $items_count    = count($events);
        $processed_count= 0;

        $differences_found = 0;
        $differences_processed = 0;
        foreach ($events as $training)
        {
            // NEEDED smth. like: 11:30, 14:00, 19:00 (30 мин.) 12:00 (45 мин.)
            $res .= date("H:i", strtotime($training->plannedstarttime));
            $diff_minutes = $this->getTimeDifferenceInMinutes($training->plannedstarttime, $training->plannedfinishtime);

            if ($prev_diff_minutes != $diff_minutes)
            {
                $differences_found++;

                // for the very first time we don't add the duration string, unless that's the only item in a list:
                if ((0 != $processed_count)/* || (1 == count($events))*/)
                {   $str = $this->formatDurationSmart($this->split_time_difference_by_hours_minutes($diff_minutes));
                    $res .= ' ('.$str.')';

                    $differences_processed++;
                }

                $prev_diff_minutes = $diff_minutes;
            }

            $processed_count++;
            $res .= $separator;
        }

        if ($differences_processed != $differences_found)
        {   // the very last diff was not processed. Do it now.

                $ccc = substr($res, -(1+strlen($separator)), 1);
//                log_message('error', "ccc='$ccc'");

                if (')' != $ccc)
                {
                    $res = substr($res, 0, -strlen($separator));

                    $str = $this->formatDurationSmart($this->split_time_difference_by_hours_minutes($diff_minutes));
                    $res .= ' ('.$str.')';

                    $res .= $separator;
                }
        }


//        log_message('error', "FFF: for '$title' processed $differences_processed of $differences_found differences. Last diff_minutes=$diff_minutes.");
//        log_message('error', "RES = ".print_r($res, true));
        return substr($res, 0, -strlen($separator));
    }

    public function getTimeDifferenceInMinutes($from, $till)
    {
        $H = date('H', strtotime($from));
        $i = date('i', strtotime($from));
        $t1 = mktime($H, $i, 0, 0, 0, 0);

        $H = date('H', strtotime($till));
        $i = date('i', strtotime($till));
        $t2 = mktime($H, $i, 0, 0, 0, 0);

        return ($t2-$t1)/60;
    }

    // accepts timestamps only. Ensure to format the strings to the SAME STRING FORMAT: before converting those strings to timestamp!!!
    public function diff_days($timestamp1, $timestamp2)
    {
        return (int)ceil(($timestamp1 - $timestamp2) / (24*60*60));
    }

    public function getTime($datetime, $format = 'H:i')
    {   return date($format, strtotime($datetime));
    }

    public function getHoursAndMinutes($datetime)
    {   return array (date('H', strtotime($datetime)), date('i', strtotime($datetime)));
    }

    public function stripAllButTime($datetime)
    {
        $h  = date('H', strtotime($datetime));
        $m  = date('i', strtotime($datetime));
        $timestamp  = mktime($h, $m, 0, 0, 0, 0);

        return date('Y-m-d H:i:s', $timestamp);
    }

    //
    // Useful when willing to some existing date add some time, to have valid "datetime".
    // Returns UNIX TIMESTAMP formatted data.
    // To the date passed adds the time.
    public function concat_date_with_time($date_string, $time_string = NULL)
    {
        // add *current* time in case if no $time_string passed in:
        if (!$time_string)
        {   $time_string = date("H:i:s");
        }
//        $current_time = date("H:i:s");
        return strtotime($date_string.' '.$time_string); // concat current tiem with existing date

    }

    public function mysqldate_to_unix_timestamp($date_string)
    {
        // add *current* time in case if no $time_string passed in:
        if (!$date_string)
        {   return time(); // current time
        }

        return strtotime($date_string);
    }

    // возвращает окончания для числа "$number". Для женского ($b_male = false) или мужского ($b_male = true) рода.
    public static function get_russian_ending($number, $ending_1, $ending_234, $ending_5more = '')
    {
        $ending = '';
        switch ($number % 10)
        {
            case 0: // одна задачА, один шест_, одно солнцЕ
                $ending = $ending_5more;
                break;
            case 1: // одна задачА, один шест_, одно солнцЕ
                $ending = $ending_1;
                break;
            case 2: // две задачИ, два шестА, два солнцА
            case 3: // три задачИ, три шестА, три солнцА
            case 4: // четыре задачИ, четыре шестА, четыре солнцА
                $ending = $ending_234;
                break;
            case 5: // пять задач_, пять шестОВ, пять солнц_
            case 6: // пять задач_, пять шестОВ, пять солнц_
            case 7: // пять задач_, пять шестОВ, пять солнц_
            case 8: // пять задач_, пять шестОВ, пять солнц_
            case 9: // пять задач_, пять шестОВ, пять солнц_
                $ending = $ending_5more;
                break;
        }

        return $ending;
    }


    // On input expects an array with { 'latitude', 'longitude' } variables.
    // Also possible to call: http://api.timezonedb.com/v2.1/get-time-zone?key=MJ1D6K43HPQB&format=json&by=zone&zone=Asia/Yerevan
    public function reverse_geocoding(&$lat_lon)
    {
        //$apikey = 'MJ1D6K43HPQB'; // obsolete. you MAY change it for new bots.
        $apikey = '3TKB4HCUKJKP'; // you MAY change it for new bots.
        $full_url = 'http://api.timezonedb.com/v2.1/get-time-zone?key='.$apikey.'&format=json&by=position&lat='.$lat_lon['latitude'].'&lng='.$lat_lon['longitude'];

        ob_start();
        $out = fopen('php://output', 'w');

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => $full_url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
                // CURLOPT_VERBOSE => true, // DEBUG mode.
                CURLOPT_STDERR => $out, // Here we set the library verbosity and redirect the error output to the output buffer.
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        ));

        $response   = curl_exec($curl);
        $httpcode   = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $err        = curl_error($curl);
        fclose($out);
        curl_close($curl);

//      Response fields:
//        {
//            "status":"OK",
//            "message":"",
//            "countryCode":"AM",
//            "countryName":"Armenia",
//            "zoneName":"Asia/Yerevan",
//            "abbreviation":"AMT",
//            "gmtOffset":14400,
//            "dst":"0",
//            "zoneStart":1319925600,
//            "zoneEnd":null,
//            "nextAbbreviation":null,
//            "timestamp":1589514815,
//            "formatted":"2020-05-15 03:53:35"
//        }

        return array('response' => $response, 'httpcode' => $httpcode, 'err' => $err);
    }

    // NOTE: it uses table WITH NO DB PREFIXED name! So raw query is being run.
    public function get_timezone_row_by_timezone_name($timezone_name)
    {   // NOTE: possibly we shall also filter by start date, because from year to year the DST/time changes.
        $sql = 'SELECT tz_country.country_name, tz_zone.*, tz_daylightsaving.* FROM tz_zone ';
        $sql .= ' LEFT JOIN tz_daylightsaving ON tz_daylightsaving.timezone_id = tz_zone.zone_id ';
        $sql .= ' LEFT JOIN tz_country ON tz_country.country_code = tz_zone.country_code ';
        $sql .= ' WHERE tz_zone.zone_name = '.$this->db->escape($timezone_name);

        $query = $this->db->query($sql, false); // NOTE that 'tz_zone' table ahs no db prefix, so raw select is needed!
        return Utilitar_db::safe_resultRow($query);
    }

    // NOTE: it uses table WITH NO DB PREFIXED name! So raw query is being run.
    public function get_timezone_row_by_timezone_id($timezone_id)
    {   // NOTE: possibly we shall also filter by start date, because from year to year the DST/time changes.
        if (intval($timezone_id) <= 0)
        {   return null;
        }
        
        $sql = 'SELECT tz_country.country_name, tz_zone.*, tz_daylightsaving.* FROM tz_zone ';
        $sql .= ' LEFT JOIN tz_daylightsaving ON tz_daylightsaving.timezone_id = tz_zone.zone_id ';
        $sql .= ' LEFT JOIN tz_country ON tz_country.country_code = tz_zone.country_code ';
        $sql .= ' WHERE tz_zone.zone_id = '.$timezone_id;

        $query = $this->db->query($sql, false); // NOTE that 'tz_zone' table ahs no db prefix, so raw select is needed!
        return Utilitar_db::safe_resultRow($query);
    }

    // NOTE: it uses table WITH NO DB PREFIXED name! So raw query is being run.
    public function upsert_timezone_dst($b_dst_exists, $timezone_id, $gmt_offset, $dst, $zone_date_start, $zone_date_end)
    {   // NOTE: possibly we shall also filter by start date, because from year to year the DST/time changes.
        $sql  = '';
        if ($b_dst_exists)
        {   $sql = "UPDATE tz_daylightsaving SET gmt_offset='$gmt_offset', dst='$dst', zone_date_start='$zone_date_start', zone_date_end='$zone_date_end'";
            $sql .= " WHERE timezone_id=$timezone_id";
        }
        else
        {   $sql = 'INSERT INTO tz_daylightsaving (timezone_id, gmt_offset, dst, zone_date_start, zone_date_end) ';
            $sql .= " VALUES ($timezone_id, $gmt_offset, $dst, '$zone_date_start', '$zone_date_end')";
        }

        $query = $this->db->query($sql, false); // NOTE that raw select is needed!
    }

    // if date passed is monday then it is returned as is. Otherwise closest previous Monday will be returned.
    // NOTE: accept date and returns in 'Y-m-d' format only!
    public function get_monday_no_greater_than($check_date)
    {
        //echo 'E.g. the check date is '.$check_date.', its weekday number is '.$weekday_no.' of 7.<br />';
        $weekday_no = date('N', strtotime($check_date)); // 1, 2, 3, ..., 7

        $monday_date = null;
        if (1 == $weekday_no) // it's already monday!
        {   $monday_date = date('Y-m-d', strtotime($check_date));
        }
        else
        {
            $reduce_days_count = $weekday_no - 1; // how many days to decrement to reach prev. monday.

            $reduced_time = strtotime($check_date.' -'.$reduce_days_count.' days');
            $monday_date  = date('Y-m-d', $reduced_time);
        }

        return $monday_date;
    }

    // generates prev/next values (for inline buttons) and titles (to show to end-user).
    public function compose_weeks_navigation($week_start_date)
    {
        $tmptime = strtotime($week_start_date.' +6 days');
        $week_end_date = date("Y-m-d", $tmptime);

        $tmptime = strtotime($week_end_date.' +1 days'); // Monday is +1 of Sunday (i.e. of $week_end_date).
        $week_next_title  = date("d F", $tmptime);
        $week_next  = date("Y-m-d", $tmptime);

        $tmptime = strtotime($week_start_date.' -7 days'); // prev. Monday is 7 days before this Monday (i.e. of $week_start_date).
        $week_prev_title  = date("d F", $tmptime);
        $week_prev  = date("Y-m-d", $tmptime);

        return array(
            'week_end_date' => $week_end_date,
            'navigation' => array(  'title_prev'=> $week_prev_title, // human-readable format.
                                    'title_next'=> $week_next_title, // human-readable format.
                                    'week_prev' => $week_prev, // inline buttons' date is of SQL-format
                                    'week_next' => $week_next, // inline buttons' date is of SQL-format
                                 ),
                    );
    }
}
