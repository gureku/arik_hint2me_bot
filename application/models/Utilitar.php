<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH . '/controllers/admin/SecuredMatrix.php'); // greco.
// require_once(APPPATH.'models/reut.php');


define("FMT_SUBKINDS_ARRAY_NAME", "%s%s");

class Utilitar extends CI_Model /* see http://codeigniter.com/wiki/Inserting_Multiple_Records_Into_a_Table/ */
{
    private $CI;
    const NT_ACTIONS    = 1;


    const GROUP_TYPE__USERS = 0;
    const GROUP_TYPE__NEWS  = 1;
    const GROUP_TYPE__ROLES = 2;

    const NT_USEFUL     = 3;
    const NT_INTRO      = 7;
    const NT_ABOUT      = 8;
    const NT_NEWS_UNPUBLISHED = 4;
    const NT_NEWS       = 5;
    const NT_CONTACTS   = 10;
    const NT_MARKET     = 11;
    const NT_PROP_LISTINGS  = 12;
    const NT_TOP_MENU_ITEMS = 13;

    const PP_NONE       = 0;
    const PP_DOCUMENT   = 1;
    const PP_THUMBNAIL  = 2;
    const PP_OBJECT_DOCUMENT    = 3;

    const BT_BUILDING   = 0;
    const BT_SECTION    = 1;
    const BT_APARTMENT  = 2;


    const FMT_SECT__NO_LINK__NAME       = 0;
    const FMT_SECT__NO_LINK__NAMEDATE   = 1;
    const FMT_SECT__LINK__NAME          = 2;
    const FMT_SECT__LINK__NAMEDATE      = 3;
    const FMT_SECT__LINK__DATE          = 4;
    const FMT_SECT__NO_LINK__DATE       = 5;

    private $excluded_chars = array('&', ';', '"', '\'', ',', );

	function __construct()
	{
		parent::__construct();

		$this->CI = &get_instance();
        $this->load->model('utilitar_db');

//        $this->CI->load->library('session');
//        $this->CI->load->library('tank_auth');
        $this->CI->load->library('image_lib');

        // see some patch for loading library from model. see http://stackoverflow.com/questions/2365591/load-a-library-in-a-model-in-codeigniter/2365848#2365848
	}


    // Note: works in conjuction with "view_ajax_script" and "view_ajax_dataContainer" views.
	public function composeAjaxQueryString($url, $linkTitle)
    {
        return "<a href='#' title='Quick view' onClick=\"javascript:ajaxUpdater('$url'); return false\">$linkTitle</a>";
    }
    
    public function getUserGroups()
    {
        return $this->getGroups(Utilitar::GROUP_TYPE__USERS);
    }

    public function getRolesList()
    {
        return $this->getGroups(Utilitar::GROUP_TYPE__ROLES);
    }

    public function getNewsGroups()
    {
        return $this->getGroups(Utilitar::GROUP_TYPE__NEWS);
    }

    public function getStaticContent()
    {
        return '';
    }

    private function getGroups($type)
    {
        $this->db->where('group_type', $type);
        $this->db->order_by('id');
        $query = $this->db->get('user_groups');

        return Utilitar_db::safe_resultSet($query);
    }

    function getUserGroup($groupId)
    {
        $this->db->where('group_type', Utilitar::GROUP_TYPE__USERS);
        return $this->getGroupById($groupId);
    }

    public function getGroupById($groupId)
    {
        $this->db->where('id', $groupId);
        $query = $this->db->get('user_groups');

        return Utilitar_db::safe_resultRow($query);
    }

    public function getGroupByName($group_name)
    {
        $this->db->where('name', $group_name);
        $query = $this->db->get('user_groups');

        return Utilitar_db::safe_resultRow($query);
    }

    public function deleteGroupById($groupId)
    {
        $this->db->where('id', $groupId);
        $query = $this->db->delete('user_groups');
    }

    function assignParentToMenuItem($menuId, $parent_menu_id)
    {
        if ($menuId <=  0 || $parent_menu_id <=0)
        {   return;
        }

        $this->CI->menu_utils->assignParentToMenuItem($menuId, $parent_menu_id);    
    }

    function assignNoParentToMenuItem($menuId, $parent_menu_id)
    {
        if ($menuId <= 0 /* || $parent_menu_id <=0 */)
        {   return;
        }

        $this->CI->menu_utils->assignParentToMenuItem($menuId, Menu_utils::MT_UNASSIGNED_MENU);    
    }

    function putUserIntoGroup($userId, $groupId)
    {
        if ($userId <=  0 || $groupId <=0)
        {   return false;
        }
        
        $data = array(  'userId' => $userId ,
                        'groupId' => $groupId,
                        );

        $this->db->insert('users_groups_link', $data);
        return true;
    }

    function reserveApartment($apartment_id, $manager_id, $manager_comments)
    {
        $this->writeHistory($apartment_id, $manager_id, $manager_comments);

        $data = array(  'status_reservation' => 1,
                        'manager_id' => $manager_id,
                         'manager_comments' => $manager_comments);

        $this->db->where('id', $apartment_id);
        $this->db->update('real_estate_apartments', $data);
    }

    function unReserveApartment($apartment_id, $supervisor_id, $supervisor_comments)
    {
        $this->writeHistory($apartment_id, $supervisor_id, $supervisor_comments);

        $data = array(  'status_reservation' => 0,
                        'manager_id' => -1,
                         'manager_comments' => $supervisor_comments);

        $this->db->where('id', $apartment_id);
        $this->db->update('real_estate_apartments', $data);
    }

    private function writeHistory($apartment_id, $manager_id, $manager_comments)
    {
        // apartment:
        $row = $this->getApartment($apartment_id);
        if (!$row)
        {   return FALSE;
        }

        $data = array(  'serial_number'     => $row->name,
                        'parent_object_id'  => $row->parent_object_id,
                        'is_commercial'     => $row->is_commercial,
                        'status_reservation'=> $row->status_reservation,
                        'date_released'     => date('YmdHis'),
                        'manager_id'        => $manager_id,
                        'manager_comments'  => $manager_comments,
                        'date_creation'     => date('YmdHis', strtotime($row->date_creation)),
                        'date_creation'     => date('YmdHis', strtotime($row->date_creation)),
                        'date_modification' => date('YmdHis'),
                    );

        $this->db->insert('history_real_estate', $data);
        return TRUE;
    }


    function currentUserHasRole($role_name)
    {
        $CI = &get_instance();
        $CI->load->model('tank_auth/users');
        $user_id = $CI->session->userdata('user_id');

        return $CI->users->user_has_role($user_id, $role_name);
    }

    function currentUserFIO()
    {
        $user_id = $this->CI->session->userdata('user_id');

        $user_profile = $this->utilitar_db->_getTableRow('user_profiles', 'user_id', $user_id);
        if (!$user_profile)
        {   return 'not logged in';
        }

        return $user_profile->name;
    }

    /*
    function basename_safe($path) {
        $path = rtrim($path,'/');
        $path = explode('/',$path);
        return end($path);
}
     */
    // see http://drupal.org/node/278425
    function locale_safe_basename($filename)
    {
        $pos = strrpos($filename, "/");
        if (false !== $pos)
        {
            return substr($filename, $pos+1);
        }

        return $filename;
    }

    function currentUserHasExclusiveContentAccess()
    {
        return false; // this column excluded from the DB (it was contradicting to the security-model)

        $CI = &get_instance();
        $CI->load->model('tank_auth/users');
        $user_id = $CI->session->userdata('user_id');

        $this->db->where("user_id", $user_id);
        $this->db->where('has_exclusive_access', '1');
        $query = $this->db->get('user_profiles', 1);
        return ($query->num_rows() > 0);
    }

    function removeUserFromGroup($userId, $groupId)
    {
        if ($userId <=  0 || $groupId <=0)
        {   return;
        }

        $this->db->where('userId', $userId);
        $this->db->where('groupId', $groupId);
        $this->db->delete('users_groups_link');
    }


    //
    // NOTE: pass nothing to get all users list.
    function getGroupMembers($groupId = -1)
    {
        $this->db->select("users.id, users.username as name, users_groups_link.groupId");
        $this->db->from('users');
        $this->db->join('users_groups_link', 'users.id = users_groups_link.userId', 'left');

        if ($groupId >= 0)
        {   $this->db->where('users_groups_link.groupId', $groupId);
        }

        $this->db->group_by('users.id');
        $query = $this->db->get();

        return Utilitar_db::safe_resultSet($query);
    }

    public function getActions()
    {
        return $this->utilitar_searcher->getNewsByType(Utilitar::NT_ACTIONS, Reut::LANG_DEFAULT);
    }

    private function getArrayOfNestedGroups($categoryId = -1)
    {
        $nestedCategories = array();

        if ($categoryId >= 0)
        {   $this->db->where('parent_id', $categoryId);
        }

        //print '<p>'.$this->db->_compile_select().'</p>';
        $query = $this->db->get('goods_groups');
        if ($query->num_rows() > 0)
        {
            foreach ($query->result() as $row)
            {   $nestedCategories[] = $row->id;
            }
        }

        return $nestedCategories;
    }


    public function wrapIntoDiv(&$rows)
    {
        $res = '';
        foreach ($rows as $district)
        {
            $res .= "<div class='CheckboxWrapper'><input type='checkbox' value='$district->id' id='regionX_$district->id' name='regions[]' /><label for='regionX_$district->id'>$district->name</label></div>";
        }
        return $res;
    }



    // Returns an object. Make sure that no empty/NULL fields get returned (that's for UI).
    public function getDocument($documentId)
    {
        if ($documentId < 0)
        {   return NULL;
        }

        $this->db->where('id', $documentId);
        $query = $this->db->get('docs');
        if ($query->num_rows() <= 0)
        {   return NULL;
        }

        $obj = $query->result();
        $obj = $obj[0];

        if (!$obj->description)
            $obj->description = '-';

        return $obj;
    }

    public function getDocuments()
    {
        $this->db->order_by('id DESC');
        $query = $this->db->get('docs');

        return Utilitar_db::safe_resultSet($query);
    }


    public function getDocumentRows($docType)
    {
        if ($docType >= 0)
        {   $this->db->where('docType', $docType);
        }

        $this->db->order_by('id DESC');
        $query = $this->db->get('docs');

        return Utilitar_db::safe_resultSet($query);
    }


    public function getUsefulInfo()
    {
        $res = $this->utilitar_searcher->getNewsByType(Utilitar::NT_USEFUL);
        if (count($res) == 0)
        {   return NULL;
        }

        $obj = $res[0];        

        if (!$obj->body)
            $obj->body = '-';

        return $obj;
    }


    //
    // Note that "news->body" represents the *URL path* for _main menu_ items.
    //
    private function getMainMenuItemByPath($main_menu_pathname)
    {
        $this->db->where('body', $main_menu_pathname);
        $this->db->where('is_action', Utilitar::NT_TOP_MENU_ITEMS);
        $this->db->limit(1);
        $query = $this->db->get('news');

        return Utilitar_db::safe_resultRow($query);
    }

    public function getAlbumPhotos($albumId, $max_count = -1)
    {
        return $this->CI->utilitar_db->_getTableContents('photo', 'id', 'album_id', $albumId, NULL, NULL, $max_count);
    }

    public function getPhoto($photo_id)
    {
        $this->db->where('id', $photo_id);
        $query = $this->db->get('photo');

        return Utilitar_db::safe_resultRow($query);
    }

    public function getAlbumCoverPhoto($albumId)
    {
        $this->db->limit(1);
        $this->db->where('album_id', $albumId);
        $this->db->order_by('id');
        $query = $this->db->get('photo');

        return Utilitar_db::safe_resultRow($query);
    }

    public function deleteFile($path)
    {
    log_message('error', "file to delete:$path");
        if (file_exists($path) && is_file($path))
        {   unlink($path);
        }
    }



    public function showPublicMessage($title, $msg)
    {
        $data = array();

        $this->generateSurroundings($data, 'none', '' /* $this->util_generateLeftMenu() */);

        $data['title'] = iconv('windows-1251', 'UTF-8', $title);
        $data['left_menu'] = '';
        $data['content_body'] = iconv('windows-1251', 'UTF-8', $msg);

        $data['content_title'] = $title;
        $this->load->view('public/view_msg', $data);
    }

    public function generateSurroundings($p1, $p2, $p3)
    {
     // this is just a stub
    }

    //----------------------------------------------------------------+
    // directory key management:

    // creates a key if {sessid:dir} pair does not exist or sessid is invalid:
    public function createDirectoryKey($dir, $postprocess_code, $dword = 0)
    {
        $key = NULL;

        if ($this->CI->session->userdata('session_id') !== FALSE)
        {
            $sessid = $this->CI->session->userdata('session_id');

            if ($dir[strlen($dir)-1] != '/')
            {
                $dir .= '/';
            }

            //---------------------------------------------------+
            // check if {sessid:dir} pair already exists:
            $this->db->where('sessid', $sessid);
            $this->db->where('dir', $dir);
            $query = $this->db->get('upload');
            if ($query && $query->num_rows() > 0)
            {
                $rows = $query->result();
                $row = $rows[0];
                return $row->key;
            }
            //---------------------------------------------------|


            $key = random_string('alnum', 16); // no more than 40 chars (db limitation currently).

            $data = array(  'key'           => $key,
                            'sessid'        => $sessid,
                            'dir'           => $dir,
                            'postprocess_code'   => $postprocess_code,
                            'dword'         => $dword,
                            );

            $this->db->insert('upload', $data);
        }

        // do some cleanup:
        $this->houseKeepingDirectoryKeys();

        return $key;
    }

    public function retrieveDirectory(&$dir_key)
    {
        $this->db->where('key', $dir_key);
        $query = $this->db->get('upload');
        if ($query->num_rows() > 0)
        {
            $rows = $query->result();
            $row = $rows[0];

            $sessid = $this->CI->session->userdata('session_id');
//            if session is valid:
//            if (0 == strcmp($sessid, $row->sessid)) greco: test this is the core problem now.
            {
                return array('dir' => $row->dir, 'postprocess_code' => $row->postprocess_code, 'dword' => $row->dword);
            }
        }

        return  NULL;
    }

    public function houseKeepingDirectoryKeys()
    {
        // delete all keys corresponding to non-existent sessid's:
        $qry = "SELECT * FROM upload u WHERE u.sessid NOT IN (SELECT session_id FROM ci_sessions)";
    }
    //----------------------------------------------------------------|

    
    public function getDocumentsServerPath()
    {
        $docs_dir = $this->utilitar_db->getOption('DocumentsDirectory', 'uploads/docsList');
        return $docs_dir;
    }

    public function getNews_AlbumId()
    {
        return $this->utilitar_db->getOption('news_album_id', '-1');
    }

    public function setNews_AlbumId($album_id)
    {
        $this->utilitar_db->setOption('news_album_id', $album_id);
    }

    public function getBerkanaTrainersGroupId()
    {
        return $this->utilitar_db->getOption('berk_trainers_group_id', '-1');
    }

    public function setBerkanaTrainersGroupId($group_id)
    {
        $this->utilitar_db->setOption('berk_trainers_group_id', $group_id);
    }

    public function getWelcomeWords()
    {
        return $this->utilitar_db->getOption('welcomeWords', '');
    }

    public function setWelcomeWords($welcome_words)
    {
        $this->utilitar_db->setOption('welcomeWords', $welcome_words);
    }

    public function getMeropriyatiya()
    {
        return $this->utilitar_db->getOption('meropriyatiya', '');
    }

    public function setMeropriyatiya($meropriyatiya)
    {
        $this->utilitar_db->setOption('meropriyatiya', $meropriyatiya);
    }

    // also creates the dir if missing:
    public function getBuildingPhotosDir($building_id)
    {
        $targetDir = $this->utilitar_db->getOption('BuildingsPhotoDir', 'uploads/buildingPhotos');
        if(!file_exists($targetDir))
        {   mkdir($targetDir);
        }
        $targetDir .= '/b'.intval($building_id);
        if(!file_exists($targetDir))
        {   mkdir($targetDir);
        }

        return $targetDir;
    }

    // also creates the dir if missing:
    public function getSectionsDocsDir($section_id)
    {
        $targetDir = $this->utilitar_db->getOption('SectionsDocsDir', 'uploads/sectionDocs');
        if(!file_exists($targetDir))
        {   mkdir($targetDir);
        }
        $targetDir .= '/sect'.intval($section_id);
        if(!file_exists($targetDir))
        {   mkdir($targetDir);
        }

        return $targetDir;
    }


    // also creates the dir if missing:
    public function getAptDirBySectionId($section_id, $apt_id)
    {
        $targetDir = $this->getSectionsDocsDir($section_id)."/apt$apt_id";

        if(!file_exists($targetDir))
        {   mkdir($targetDir);
        }
        $targetDir .= '/';

        return $targetDir;
    }

    public function getGoodsUpdateDir()
    {
        $docs_dir = $this->utilitar_db->getOption('GoodsUpdateDir', 'uploads/goodsList');
        return $docs_dir;
    }

    public function getDbBackupDir()
    {
        return $this->utilitar_db->getOption('DbBackupDirectory', 'db_backups');
    }

    public function is_multilanguage()
    {
        return ('true' == $this->utilitar_db->getOption('isMultilanguage', 'false'));
    }





    public function createSelectWithSelection($rows,
    					  $id, $name, $additionalString,
    					  $selectionCriteria, $withDefaultString = true)
    {
    	$str = "<select id='$id' name='$name' $additionalString>";

        // option for no selection:
    	if ($withDefaultString)
    	{   $str .= '<option value=-1>&lt;Не выбрано&gt</option>';
    	}
    	$str .= $this->fillSelectWithSelection($rows, $selectionCriteria);
    	$str .= '</select>';

    	return $str;
    }

    private function fillSelectWithSelection($rows, $selectionCriteria)
    {
        //----------------------------------------------------------------+
        // determine "name" or "title" field exist?
        $value__field_name = 'name'; // 'name' by default is the preferred one.
        foreach ($rows as $row)
        {
            if (!property_exists($row, 'name')) // give a preference to 'name' fieldname.
            {   if (property_exists($row, 'title'))
                {   $value__field_name = 'title';
                }
            }
            break; // makes no sense iterating more that once.
        }
        //----------------------------------------------------------------|


        $str = '';
    	foreach ($rows as $row)
    	{
    		$selection = ($selectionCriteria == $row->id) ? 'selected' : '';
    		$str .= sprintf('<option %s value="%s">%s</option>',
                                    $selection,
                                    $row->id,
                                    $row->$value__field_name);
    	}

    	return $str;
    }

    // The function fills in the "$js_array_name" Javascript aray by rows passed.
    // The "$js_array_name" array created right here.
    function createJSarray($rows, $js_array_name)
    {
        $str = '';

        $str .= "\n<script language=\"javascript\">";
        $str .= "var $js_array_name = new Array();";

        $count = 0;
        foreach ($rows as $row)
        {
            $str2 = $row->name;
            if (strlen($str2) == 0) {
                $str2 = "<not specified>".$count;
            }
            $str .= sprintf('%s[%d]=["%s", "%s"]; ', $js_array_name, $count, $row->id, $str2);
            $count++;
        }
        $str .= "\n</script>\n";

        return $str;
    }

    // todo: this function works for userGroups and members only but it's easy to unify it (use function callbacks???)
    function createNestedJavaScriptArrays($rows, $subKindsArrayName, $methodName)
    {
        $js_arrays_nested = '';
        foreach ($rows as $row)
        {
            // compose the array name:
            $js_array_name = sprintf( FMT_SUBKINDS_ARRAY_NAME, $subKindsArrayName, $row->id );

//            $members = $this->getGroupMembers($row->id);
            $members = $this->$methodName($row->id);
            $js_arrays_nested .= $this->createJSarray($members, $js_array_name);
        }

        return $js_arrays_nested;
    }


    // this is a proxy method to another model:
    function getSubMenuItems($parent_menu_id)
    {
        return $this->CI->menu_utils->getSubMenuItems($parent_menu_id, Reut::LANG_DEFAULT);
    }

    // creates CONTAINER array so we could reference from javascript the "subKinds" arrays whithin that container-array:
    function createContainerArray($js_container_array_name, $rows, $subKindsArrayName)
    {
        $str = '';

        $str .= "var $js_container_array_name = new Array();"; // this is SPARSE ARRAY.

        // fill in the container-array:
        foreach ($rows as $row)
        {
            // compose the array name:
            $js_array_name = sprintf( FMT_SUBKINDS_ARRAY_NAME, $subKindsArrayName, $row->id );
            $str .= sprintf('%s[%d]=[%s]; ', $js_container_array_name, $row->id, $js_array_name);
        }
        
        return $str;
    }

    public function get_defaultEmailOptions($subject, $message)
    {
        $data = array();
        $data['from']       = 'bots@walltouch.ru';
        $data['reply_to']   = 'no-reply@walltouch.ru';
        $data['to']         = 'no-reply@walltouch.ru'; // this value to be overriden!
        $data['subject']    = $subject;
        $data['message']    = $message;

        return $data;
    }

    public function send_email(&$data, $bNewlinesToBR = true)
    {
        if (
            (false !== strpos($this->input->ip_address(), '127.0.0.')) ||
            (false !== strpos($this->input->ip_address(), '0.0.0.0')) // sometimes localhost is at "0.0.0.0"...
        )
        {
//            header("Content-type: text/html;charset=utf-8");
            log_message('error', 'EMAIL CONTENTS:\n'.print_r($data, true));
            return false;
        }
        
//        $this->load->library('email');

        $this->CI->utilitar_db->setOption('step2', "done");

        $config = array(
            'mailtype'  => 'html',
            'charset'   => 'utf-8',//'windows-1251' // 'iso-8859-1'
        );
        $this->load->library('email', $config);

        $this->email->from      ($data['from']);
        $this->email->reply_to  ($data['reply_to']);
        $this->email->to        ($data['to']);
        $this->email->subject   ($data['subject']);
        //$this->email->message   ( base64_encode($data['message']) );

        if ($bNewlinesToBR)
        {   $this->email->message   (str_replace("\n", "<br>", $data['message']));
        }
        else
        {   $this->email->message   ($data['message']);
        }
//		$this->email->set_alt_message($data['alt_message']);

        if (isset($data['cc']))
        {   $this->email->cc    ($data['cc']);
        }

//        if (isset($data['bcc']))
//        {   $this->email->bcc       ($data['bcc']);
//        }

//       log_message('error', "send_email() data=\n".print_r($data, true));

        return $this->email->send();
    }


    public function generateMainMenuNEW($menu_for_top, $type, &$data, $current_language)
    {
        // note: ORDERED list!
        $standardPaths = array('about', 'intro', 'properties', 'market', 'contacts');
        $theUrl = base_url().'data/getContent';

        $top_menu = $this->CI->menu_utils->getTopMenuItems($current_language);

        $ctr = 0;
        $max_ctr = count($standardPaths)- 1;
        $menu_string = '';
        foreach ($top_menu as $menu)
        {
            if (0 == $menu_for_top)
            {
                $active_marker = '';
                if (is_numeric($type))
                {
                    $ng = $this->retrieveGroup(intval($type));
                    if ($ng)
                    {
                        $top_menu_row = $this->utilitar_searcher->getNewsById($ng->top_menu_id);
                        if ($top_menu_row)
                        {
                            if ($top_menu_row->body == $menu->body)
                            {
                                $active_marker = 'current';
                            }
                        }
                    }
                }
                else if ($type == $standardPaths[$ctr])
                {
                    $active_marker = 'current';
                }

                if ($max_ctr == $ctr)
                {   $active_marker .= ' last';
                }

                $menu_string .= sprintf("<li><a class='%s' href='%s/%s' title=''>%s</a></li>",
                                        $active_marker,
                                        $theUrl,
                                        $standardPaths[$ctr],
                                        (Reut::LANG_RU == $current_language) ? $menu->title_ru : $menu->title
                                        );
            }
            else
            {
                $menu_string .= sprintf("<li %s><a href='%s/%s' title=''>%s</a></li>",
                                        ($max_ctr == $ctr) ? 'class="last"' : '',
                                        $theUrl,
                                        $standardPaths[$ctr],
                                        (Reut::LANG_RU == $current_language) ? $menu->title_ru : $menu->title
                                        );
            }
            $ctr++;
        }

      return $menu_string;
    }

    //
    // supports EN and RU languages at the moment.
    //
    public function _rowBody(&$row)
    {
        if (Menu_utils::CUSTOM_TYPE__CODE_PARSE != $row->custom_type)
        {
//            log_message('error', 'XXcase1='.$row->body);
            return $row->body;
        }
        else
        {
//            log_message('error', 'XXcase2 (CUSTOM_TYPE__CODE_PARSE) = '.$row->body);
            $out = array();
            $format_str = $this->_custom_code_parse($row->body, $out);
            if ($format_str && count($out) > 0)
            {
//                log_message('error', 'XXcase3.');
                $sand_box = '3rd_party';
                $result_html = array();
                foreach ($out as $invoke)
                {
                    // todo: verify security access here (!)
                    
                    // todo: use caching
                    $this->load->helper("$sand_box/".$invoke['classname']);

                    // todo: use caching
                    $clazz = new $invoke['classname'];


                    // RTTI is in force:
                    // todo: use caching
                    if(!method_exists($clazz, $invoke['method']))
                    {   $result_html[]  = ''; // class method "$invoke['method']" does not exist!
                    }
                    else
                    {   $result_html[]  = $clazz->$invoke['method']($invoke['argv']);//invoke the class
                    }
                }
//                log_message('error', 'XXcase3-after='.print_r($result_html, true));


                $separator = "#result#";
                $separator_length = strlen($separator);
                
                $res = '';
                $offset = 0;
                $pos = 0;
                $ctr = 0;

                while(($pos=@strpos($format_str, $separator, $offset)) !== false)
                {
                    $original = substr($format_str, $offset, $pos - $offset);
                    $res .= $original.$result_html[$ctr++];
                    $offset = $pos + $separator_length;
                }

                $extract = substr($format_str, $offset);
                $res .= $extract;

                return $res;
            }

//            log_message('error', 'XXcase4 VOID');
            return '';
        }        
    }

    private function str_replace_once($needle , $replace , $haystack)
    {
        // Looks for the first occurence of $needle in $haystack
        // and replaces it with $replace.
        $pos = strpos($haystack, $needle);
        if ($pos === false)
        {   // Nothing found
            return $haystack;
        }
        return substr_replace($haystack, $replace, $pos, strlen($needle));
    }


    //
    // Fills in the "$out" array of calls (along with methods & arguments) to be invoked.
    // Returns string to be used as a format string based on calls result.
    //
    //  Expected input format:      $invoke(className,method1,param_1,param_2,...)...$invoke(className2,method2,param_1,param_2,param_3,...)...
    //
    //  todo: verify that helper_file/class/method do exist!!!
    private function _custom_code_parse($raw_string, &$out)
    {
        // print "input:<br><b>'".htmlentities($raw_string)."'</b><br><br><br>";

        $bracket_end = -1; // (!)
        $format_str = '';
        // find all occurences of "$invoke":
        $invoke_pos = 0;
        $offset = 0;
        $out = array();
        $ctr = 0;
        while (1)
        {
            $invoke_pos = strpos($raw_string, '$invoke', $offset);
            if (false === $invoke_pos)
            {   break;
            }

//            print 'iteration#'.(++$ctr).'<hr>';
//            print "input = '".htmlentities(substr($raw_string, $offset))."'<br>";

            $bracket_start  = strpos($raw_string, '(', $invoke_pos);
            $bracket_end    = strpos($raw_string, ')', $invoke_pos);
//            print "brackets: <b>$bracket_start - $bracket_end</b><br>";

            $extract = substr($raw_string, $offset, ($invoke_pos - $offset));
//            print "extracted: <b>'".htmlentities($extract)."'</b><br>";
//            print "remains: <b>'".htmlentities(substr($raw_string, $bracket_end+1))."'</b><br><br>";

            $format_str .= $extract.'#result#';

            if ($bracket_start !== false && $bracket_end !== false && ($bracket_start < $bracket_end))
            {   //print "offset = $offset, invoke_pos = $invoke_pos, bracket_end=$bracket_end<br>";
                
                $signature = explode(',', substr($raw_string, $bracket_start+1, $bracket_end-$bracket_start-1));
                if (count($signature) >= 2)
                {
                    // read arguments supplied:
                    $argv = (count($signature) > 2) ? array_slice($signature, 2) : array();

                    $out[] = array(
                                    // this is filename actually so let's clean up from directory traversal attacks:
                                    'classname' => basename(trim($signature[0])), // warning_ russian filenames will be corrupted. see utilitar->locale_safe_basename() if required.

                                    'method'    => trim($signature[1]),
                                    'argv'      => $this->_validate_arguments($argv),
                                    );
                }
            }
            else
            {   break;
            }

            $offset = $bracket_end + 1;
        }

        $extract = substr($raw_string, $bracket_end+1);
        $format_str .= $extract;

//        print '<br>format string: \''.htmlentities($format_str).'\'';
        return $format_str;
    }


    //
    // Method replaces macroses by actual values.
    //  Supported macroses list:
    //      URL_1, URL_2, URL_3, URL_4
    //
    private function _validate_arguments($argv)
    {
        $res = array();
        foreach ($argv as $arg)
        {
            $seg_number = -1;
            switch ($arg)
            {
                case 'URL_4':
                    $seg_number = 4;
                    break;
                
                case 'URL_3':
                    $seg_number = 3;
                    break;

                case 'URL_2':
                    $seg_number = 2;
                    break;

                case 'URL_1':
                    $seg_number = 1;
                    break;
            }

            if (($seg_number >= 0) && (false !== $this->uri->segment($seg_number)))
            {   $res[] = $this->uri->segment($seg_number);
            }
            else
            {   $res[] = str_replace('"', '', trim($arg)); // remove doublequotes. 
            }
        }

        return $res;
    }


    //
    // Generates top(and bottom) menu from top menu items.
    private function fillInTopAndBottomMenu(&$data, $current_top_menu_id, $current_language)
    {
        // this is custom case: just plain menu to be loaded:
        $data['top_menu']       = $this->load->view('pub/menu', $data, true);
        $data['bottom_menu']    = '';

        /*$menu_string_top    = '';
        $menu_string_bottom = '';

        $theUrl = base_url().'pages';

        $top_menu_items = $this->CI->menu_utils->getTopMenuItems($current_language);
        $ctr = 0;
        $max_ctr = count($top_menu_items) - 1;
        foreach ($top_menu_items as $menu)
        {
            $menu_string_top .= sprintf("<li><a class='%s %s' href='%s/%s' title=''>%s</a></li>",
                                    ($current_top_menu_id == $menu->id) ?   'current' : '',
                                    ($max_ctr == $ctr) ?            'last' : '',
                                    $theUrl,
                                    $menu->path,
                                    $menu->name
                                    );

            // todo: let's disable menu generation since currently for both menus used top_menu value.
            $menu_string_bottom .= sprintf("<li><a href='%s/%s' title=''>%s</a></li>",
                                        $theUrl,
                                        $menu->path,
                                        $menu->name
                                        );

            $ctr++;
        }

        $data['top_menu']       = $menu_string_top;
        $data['bottom_menu']    = $menu_string_bottom;*/
    }


    //
    // Generates left menu for _corresponding top menu item_
    //  Input params:
    //          $top_menu - the object containing top menu row corresponding to the currently selected (sub)item.
    //          $current_id - selected item id. Could be any of top menu, submenu, subsubmenu, etc.
    //          $current_language - language
    //
    // output: HTML containing the menu generated.
    private function generateLeftMenuRecursive($menu_id)
    {
        $subItems = $this->CI->menu_utils->getSubMenuItems($menu_id, Reut::LANG_DEFAULT);

        $res = array();
        foreach ($subItems as $menu)
        {
            $res[] = array( 'id' => $menu->id,
                            'name'=> $menu->name,
                            'path'=> $menu->path,
                            'subitems' => $this->generateLeftMenuRecursive($menu->id)
                            );
        }

        return $res;
    }


    private function visualizeLeftMenu($rows, $current_id)
    {
        $menu_string_left   = '';

        // algorithm: include only submenu items which belong to the same top menu.
        foreach ($rows as $value)
        {
            $arr_subitems = $value['subitems'];
            $class_active = is_numeric($current_id) ? (intval($current_id) == $value['id'] ? 'active' : '') : '';

            $menu_string_left .= sprintf("<li class='%s $class_active'><a class='$class_active' href='%s'><span>%s</span></a>",
                                            (count($arr_subitems) > 0) ? 'havechild' : '',
                                            base_url().'data/getContent/'.$value['id'],
                                            $value['name']
                                        );

                if (count($arr_subitems) > 0)
                {
                    $menu_string_left .= '<ul>';

                    foreach ($arr_subitems as $submenu)
                    {
                        $class_active = is_numeric($current_id) ? (intval($current_id) == $submenu['id'] ? 'active' : '') : '';

                        $menu_string_left .= sprintf("<li class='$class_active'><a class='$class_active' href='%s'>%s</a></li>",
                                                    base_url().'data/getContent/'.$submenu['id'],
                                                    $submenu['name']
                                                );
                    }
                    $menu_string_left   .= '</ul>';
                }

            $menu_string_left   .= '</li>';
        }


        return $menu_string_left;
    }

    //
    // Generates left menu from its submenu items.
    private function generateLeftMenu(&$subMenu_items, $current_id, $current_language)
    {
        $menu_string_left   = '';
        // algorithm: include only submenu items which belong to the same top menu.
        foreach ($subMenu_items as $menu)
        {
            $menu_string_left .= sprintf("<li><a %s href='%s'>%s</a></li>",
                    is_numeric($current_id) ? (intval($current_id) == $menu->id ? 'class="current"' : '') : '',
                    base_url().'data/getContent/'.$menu->id,
                    $menu->name
            );
        }

        return $menu_string_left;
    }


    /** a COPY-PASTE from tank_auth - todo: reuse instead!!
	 * Create CAPTCHA image to verify user as a human
	 *
	 * @return	string
	 */
	private function _create_captcha()
	{
		$this->load->plugin('captcha');

		$cap = create_captcha(array(
			'img_path'		=> './'.$this->config->item('captcha_path', 'tank_auth'),
			'img_url'		=> base_url().$this->config->item('captcha_path', 'tank_auth'),
			'font_path'		=> './'.$this->config->item('captcha_fonts_path', 'tank_auth'),
			'font_size'		=> $this->config->item('captcha_font_size', 'tank_auth'),
			'img_width'		=> $this->config->item('captcha_width', 'tank_auth'),
			'img_height'	=> $this->config->item('captcha_height', 'tank_auth'),
			'show_grid'		=> $this->config->item('captcha_grid', 'tank_auth'),
			'expiration'	=> $this->config->item('captcha_expire', 'tank_auth'),
			'word'	=> random_string('alnum', 5) // pass the random word to show to the user.
		));

		log_message('error', "CAPTCHA UTILITAR = ".print_r($cap, true));

		// Save captcha params in session
		$this->session->set_flashdata(array(
				'captcha_word' => $cap['word'],
				'captcha_time' => $cap['time'],
		));

		return $cap['image'];
	}

    private function isValidBranch($branch)
    {
        return (null != $branch);
    }

    //
    // Returns the view page (relative to /PUB folder) to load (according to the content being requested).
    //
    //  Note: do not use "$current_id_or_path" as it could be menu->path, too - use "$current_menu->id" to use integer ID for sure!
    //
    public function createMenusAndPageContent($current_id_or_path, &$data, $current_language, $with_left_menu)
    {
        $default_page_name  = 'about';

        // general items go here:
        $data['CI']        = $this->CI;

        $current_menu = $this->CI->menu_utils->getMenuItem($current_id_or_path, $current_language);
        if (!$current_menu)
        {   $current_menu = $this->CI->menu_utils->getMenuItem($default_page_name, $current_language);

            // if somebody deleted default page - well, find out any topmost item or exit:
            if (!$current_menu)
            {
                $top_menu = $this->CI->menu_utils->getTopMenuItems($current_language);

                foreach ($top_menu as $menu)
                {   $current_menu = $menu; // get the first available top menu.
                    $default_page_name = $menu->name;
                    break;
                }
            }

            // give some dummy page at least:
            if (!$current_menu)
            {
                $data['top_menu']   = '';
                $data['title']      = 'The Site is under construction.';
                $data['content']    = $data['title'];
                $data['menu_title'] = $data['title'];
                $data['menu_class'] = 'none';
                return;
            }
        }

        // extract top menu (corresponding to the currently selected menu):
        $topmostMenu = $this->CI->menu_utils->getTopmostMenuForItem($current_menu->id, $current_language);
        if (!$topmostMenu)
        {   // in case if no menu detected we assume default menu chosen:
            $topmostMenu = $this->CI->menu_utils->getMenuItem($default_page_name, $current_language);
        }


        //
        //  OK, let's customize the content by branch:
        //---------------------------------------------------+
        //  let's try branch customizations here:
        $branch           = $data['branch'];
        if ($this->isValidBranch($branch))
        {
            // get club-specific news only:
//            $news = $this->utilitar_searcher->getRecentNews();
            $this->session->set_userdata(array('branch_id' => $branch->id));
            $news = $this->util_categorized_items->_getNewsByClubs($branch->id, true);
        }
        else
        {
            $this->session->unset_userdata('branch_id'); // clean up each time!

            // get all the news:
            $news = $this->utilitar_searcher->getRecentNews();
        }
        //---------------------------------------------------|
        

        // 3. generate top(and bottom) menu from top menu items
        $this->fillInTopAndBottomMenu($data, $topmostMenu->id, $current_language);

        $subMenu_items = $this->CI->menu_utils->getSubMenuItems($topmostMenu->id, $current_language);


        // 4. generate left menu from its submenu items
        $data['left_menu'] = ($with_left_menu) ? $this->generateLeftMenu($subMenu_items, $current_menu->id, $current_language) : '';



        $data['title']          = $topmostMenu->name;
        $data['sub_menu_title'] = '';
        $data['menu_title']     = $data['title'];
        $data['menu_class']     = $this->getMenuClass($topmostMenu);


        if ($topmostMenu->id != $current_menu->id) // if submenu and menu are not the same - compose a chain:
        {
            $subtitle = $current_menu->name;
            if (strlen($data['title']) > 0 && strlen($subtitle) > 0)
            {   $data['sub_menu_title'] = ' / ';
            }

            $data['sub_menu_title'] .= $subtitle;

            // this is for page header:
            $data['title']           = $subtitle.' | '.$data['title'];
        }

        //---------------------------------------------------+
        // append the club name (if any):
        //TODO: adjust not only TITLE but the "PAGE's <H1>" as well!
        if ($this->isValidBranch($branch))
        {   $data['title']  .= ' ('.$branch->name.')';
        }
        else
        {   ; // $data['title']  .= ' [common]';
        }

        $data['title']  .= ' | '.lang('const.website_name');
        //---------------------------------------------------|


        // now the final accord: fill in the *page content* itself:
        $data['content'] = $this->_rowBody($current_menu, $current_language);

        if ((false !== strpos($data['content'], '&lt;object ')) || (false !== strpos($data['content'], '&lt;iframe ')))
        {   $data['content']    = html_entity_decode($data['content']);
        }

        switch ($topmostMenu->path)
        {
            case 'gostevaya':
                $full_tree  = $this->CI->thought->build_comments_tree_for_all_thoughts(Thought::VERIFICATION_STATUS_VERIFIED);
                $params     = array('CI' => $this->CI,
                                    'full_tree' => $full_tree,
                                    'is_admin'  => $this->tank_auth->is_admin(),
                                    'allow_new_topic'  => true
                                    );

                if ($this->session->flashdata('just_added'))
                {   $params['congrats'] = lang('const.thanksforposting');//'Thank you! Your post has been sent.<br />After review it will be posted here.';
                }
                else if ($this->session->flashdata('just_failed'))
                {   $params['congrats'] = $this->session->flashdata('just_failed');
                }

                $params['captcha_html'] = $this->_create_captcha();
                $data['content']        = $this->load->view("pub/view_blog_form", $params, true);
                
                $data['rightcol_data']  = $this->load->view("pub/rightcol_news", array(), true);
                break;

            case 'news':
                $tmp = array('news' => $news);

                $data['content']        = $this->load->view("pub/news", $tmp, true);
                $data['rightcol_data']  = $this->load->view("pub/rightcol_news", array(), true);
                break;

            case 'zanyatiya':
            case 'partnyori':
                $arr = array();
                if ('partnyori' === $topmostMenu->path)
                {   $this->util_trainings->fillInGroupTypes($arr, true, -1, Util_trainings::LEASE_TYPE__LESSEES_ONLY);
                }
                else
                {   $this->util_trainings->fillInGroupTypes($arr, true, -1, Util_trainings::LEASE_TYPE__OUR_OUWN_ONLY);
                }

//                $data['rightcol_data']  = $this->load_our_trainings_shortlist($arr);
                $data['content']        = $this->load->view('3rd_party/view_trainigs_list', $arr, true);
                break;

            case 'main':
                $arr = array();
                $this->util_trainings->fillInGroupTypes($arr, true, -1, Util_trainings::LEASE_TYPE__OUR_OUWN_ONLY);

                $sorted_group_types     = $this->sortGroupTypesByAge($arr['group_types']);
                $meropriyatiya          = $this->utilitar->getMeropriyatiya();

                $tmp                    = array('sorted_group_types'=> $sorted_group_types,
                                                'meropriyatiya'     => $meropriyatiya,
                                                );
                $shortlist              = $this->load->view("3rd_party/view_trainigs_shortlist_homepage", $tmp, true);
                $data['content']        = $this->getWelcomeWords().$shortlist.$data['content'];

                $data['rightcol_data'] = $this->load->view("pub/news", array('news' => $news, 'news_limit' => 3), true);
                break;

            case 'oplata':
                $data['no_picPreview']  = true;
                $data['rightcol_data']  = lang('const.payment.guide');

                $data['content']        = $this->load->view("pub/view_payment", array(), true);
                $data['rightcol_data'] = $this->load->view("pub/news", array('news' => $news, 'news_limit' => 3), true);
                break;

            case 'raspisanie':
            case 'raspisanie2':
            case 'raspisanie_modern':
            case 'raspisanie_setka':
                break;

            case 'raspisanie_print':
                $data['printable']  = true; // set this variable to load other template than "index.php"
                $data['title']      = sprintf('Расписание на %s', $this->utilitar_date->formatDateRu(date('Y-m-d')));

                if ($this->isValidBranch($branch))
                {   $data['title']  .= ' ('.$branch->name.')';
                }
                break;

            case 'request_trainings':
                $group_type_id          = intval($this->_safe_get($data['params'], 0));

                $tmp = array();
                $this->util_trainings->fillInGroupTypes($tmp, false, $group_type_id, Util_trainings::LEASE_TYPE__OUR_OUWN_ONLY);

                $data['content']        = $this->load->view("pub/view_request_trainings", $tmp, true);

                $data['no_picPreview']  = true;
                $data['rightcol_data'] = $this->load->view("pub/news", array('news' => $news, 'news_limit' => 3), true);
                break;

            case'tseni':
                break;

            case 'podtverzhdenie': // payment confirmation
                $amount         = $this->session->flashdata('payment_amount');
                $description    = $this->session->flashdata('payment_description');
                if (!($amount && $description))
                {   redirect();                
                }

                $order_id       = random_string('alnum', 5);
                
                $this->session->set_flashdata('order_id', $order_id);
                $this->session->keep_flashdata('payment_amount');
                $this->session->keep_flashdata('payment_description');

                $data['no_picPreview']  = true;
                $data['rightcol_data']  = lang('const.payment.guide');


                //----------------------------------------------------------------+
                $tmp = array(
                   'order_id'       => $order_id,
                   'amount'         => $amount,
                   'description'    => $description,
                );

                // some DB stuff here: todo: refactor this out!
                $this->db->insert('orders', $tmp);
                $this->CI->session->set_userdata('order_id', $order_id);

                // add one more param:
                $this->config->load('shop_options', TRUE);
                $tmp['ik_shop_id'] = $this->config->item('ik_shop_id', 'shop_options');
                //----------------------------------------------------------------|
                
                $data['content']    = $this->load->view("pub/view_payment_readonly", $tmp, true);
                break;

            case 'checkout_success':
                /*if (!$this->precheck_payment_status_request())
                {   redirect('pages/checkout_failure/');
                }*/

                $order_id = $this->CI->session->userdata('order_id');
                if (!$order_id)
                {   redirect();
                }

                $data['title'] = 'Checkout success';
                $this->CI->session->unset_userdata('order_id');
                $data['content'] = $this->load->view("pub/view_payment_ok", array('title' => lang('const.checkout.title'), 'order_id' => $order_id), true);
                break;

            case 'checkout_failure':
                // was there any payment before?
                $data['title'] = 'Checkout failure';
                $data['content']    = $this->load->view("pub/view_payment_fail", array('title' => lang('const.checkout.title')), true);
                break;

            default:
                $data['rightcol_data'] = $this->load->view("pub/news", array('news' => $news, 'news_limit' => 3), true);
                break;
        }
    }

    private function load_our_trainings_shortlist(&$arr)
    {
        $sorted_group_types     = $this->sortGroupTypesByAge($arr['group_types']);
        return $this->load->view('3rd_party/view_trainigs_shortlist', array('sorted_group_types' => $sorted_group_types), true);
    }

    public function _safe_get(&$arr, $index)
    {
        if (isset($arr[$index]))
        {   return $arr[$index];
        }
        return NULL;
    }

    public function pushToArray($param1, $param2, $param3, $param4)
    {
        $res = array();
        if ($param1)
        {   $res[] = $param1;
        }
        if ($param2)
        {   $res[] = $param2;
        }
        if ($param3)
        {   $res[] = $param3;
        }
        if ($param4)
        {   $res[] = $param4;
        }
        return $res;
    }

    // Note: the ages separation is inside this method:
    private function sortGroupTypesByAge(&$group_types)
    {
        //  Format: [0] age_from, [1] age_to, [2] color, [3] naming.
        // Sections iterated untill (age_to == 0) or till the end of the array.
        $sections = array(
            array(1, 2,     '#BE0926', 'года'),
            array(3, 4,     '#ED812B', 'года'),
            array(5, 7,     '#42A62A', 'лет'),
            array(8, 10,    '#5EB2DE', 'лет'),
            array(11, 17,   '#0A71B4', 'лет'),
            array(18, 0,   '#8E5EDD', 'лет'),
        );

        $res        = array();
        foreach ($sections as $section)
        {
            foreach ($group_types as $group_type)
            {
                $section_age_from   = $section[0];
                $section_age_to             = $section[1];
//                log_message('error', "ages: $age_to >= $group_type->age_from");

                if (
                    (   ($section_age_to > 0) && ($group_type->age_to > 0) &&
                        (
                            ($section_age_from <= $group_type->age_from) && ($group_type->age_from <= $section_age_to) ||
                            ($section_age_from <= $group_type->age_to) && ($group_type->age_to <= $section_age_to) ||
                            ($section_age_from > $group_type->age_from) && ($group_type->age_to > $section_age_to)
                        )
                    )
                    ||
                    (   /*($age_to > 0) &&*/ (0 == $group_type->age_to) &&
                        (
                            ($group_type->age_from >= $section_age_from) && ($group_type->age_from <= $section_age_to) /* ||
                            ($section_age_from <= $group_type->age_to) && ($group_type->age_to <= $age_to) ||
                            ($section_age_from > $group_type->age_from) && ($group_type->age_to > $age_to)*/
                        )
                    )
                    ||
                    (   /*($age_to > 0) &&*/ (0 == $group_type->age_to) &&
                        (
                            ($group_type->age_from <= $section_age_to) ||
                            ((0 == $section_age_to)&& ($group_type->age_from <= $section_age_from))/* ||
                            ($section_age_from <= $group_type->age_to) && ($group_type->age_to <= $age_to) ||
                            ($section_age_from > $group_type->age_from) && ($group_type->age_to > $age_to)*/
                        )
                    )
                    ||
                    (
                        (0 == $section_age_to) && ($group_type->age_from >= $section_age_from)
                    )
                )
                {
                    $res[$section_age_from][]   = $group_type;
                }
            }
        }

        $index = 0;
        foreach ($sections as $section)
        {
            if (!isset($res[$section[0]]))
            {
                unset($sections[$index]);
            }
            $index++;
        }
        return array('results' => $res, 'sections' => $sections);
    }

    private function getMenuClass(&$topmostMenu)
    {
        switch ($topmostMenu->path)
        {
            case 'main':
            case 'news':
            case 'tseni':
            case 'kontakti':
            case 'mir_fitnesa':
            case 'informatsiya':
            case 'oplata':
            case 'podtverzhdenie':
                return 'green';
                break;

            case 'raspisanie_print':
                return 'black';
                break;

            case 'fitnes_studiya':
            case 'detskiy_klub':
            case 'dni_rozhdeniya':
            case 'zanyatiya':
            case 'meropriyatiya':
            case 'request_trainings':
                return 'red';
                break;

            case 'raspisanie':
            case 'raspisanie_setka':
            case 'raspisanie2':
            case 'prepodavateli':
            case 'partnyori':
                return 'orange';
                break;

            case 'fotografii':
            case 'gostevaya':
                return 'blue';
                break;
            default:
                return 'noname';
                break;
        }

        return 'unknown';
    }

    private function getGroupIdWithThisNewsId($newsId)
    {
        $this->db->where('userId', $newsId);
        $query = $this->db->get('users_groups_link');
        if ($query->num_rows() <= 0)
        {   return -1;
        }

        $row = $query->result();
        return $row[0]->groupId;
    }

    private function retrieveGroup($newsId)
    {
        $news = $this->utilitar_searcher->getNewsById($newsId);
        if ($news)
        {
            $group_id = $this->getGroupIdWithThisNewsId($newsId);
            $group = $this->getGroupById($group_id);
            if ($group)
            {
                return $group;
            }
        }

        return NULL;
    }

    public static function starts_with($str, $start)
    {
        $pos = mb_strpos($str, $start);
        return (0 === $pos);
    }
    public function ends_with($FullStr, $EndStr)
    {
        // Get the length of the end string
        $StrLen = strlen($EndStr);
        // Look at the end of FullStr for the substring the size of EndStr
        $FullStrEnd = substr($FullStr, strlen($FullStr) - $StrLen);
        // If it matches, it does end with EndStr
        return $FullStrEnd == $EndStr;
    }

    public function registerDocument($file_name, $description = NULL)
    {
        // todo: check if file exists then don't make new DB record, just update the description:
        $description = $description;

        $doc_data = array(
           'path'           => $file_name,
           'description'    => $description,
           'docType'        => 0, // a flag: this is just a plain doc file.
        );
        
        $this->db->insert('docs', $doc_data);
    }

    public function registerPhotoUpload($albumId, $image_path, $thumbnail_path)
    {
        $data = array(
               'album_id'       => $albumId,
               'photo_path'     => $image_path,
               'thumbnail_path' => $thumbnail_path
            );
        $this->CI->utilitar_db->insert_or_update('photo_path', $image_path, 'photo', $data);
    }

    public function createThumbnail222($fileName, $width, $height, $postfix = '_thumb')
    {
        log_message('error', "createThumbnail($fileName, $width, $height, $postfix)");
        if ($this->_resizePhoto($fileName, $width, $height, $postfix))
        {   log_message('error', "createThumbnail - 1");
            return $this->getFilenameWithPostfix($fileName, $postfix);
        }

        log_message('error', "createThumbnail - 2");
        return 'undefined1';
    }


    //
    // Returns a postfix before the filename but prior to the file extension.
    // E.g. if given getFilenameWithPostfix("file1.jpg", '.POSTFIX') the output is "file1.POSTFIX.jpg"
    //
    public function getFilenameWithPostfix($fileName, $postfix)
    {
        log_message('error', "getFilenameWithPostfix($fileName, $postfix)");
        $path = 'undefined2';
        $pos = strrpos($fileName, '.');
        if (false !== $pos)
        {
            $ext = substr($fileName, $pos);
            $fname = substr($fileName, 0, $pos);
            $path = $fname.$postfix.$ext;
        }
        log_message('error', "getFilenameWithPostfix, path = $path");
        return $path;
    }


    //
    // a thumbnail file will be *additionally* created if the "$postfix" parameter will be passed in.
    //
    function _resizePhoto($fileName, $width, $height, $postfix = NULL)
    {
        $image_size = getimagesize($fileName);
        $mem_usage = $image_size[0] * $image_size[1] * 4; //4 bytes per pixel (RGBA)
        log_message('error', "Please upload smaller pic: mem_usage=$mem_usage");

        log_message('error', "_resizePhoto($fileName, $width, $height, $postfix)");
        $config = array();
        $config['image_library']    = 'gd2';
        $config['source_image']     = $fileName;
        $config['create_thumb']     = isset($postfix);
        $config['maintain_ratio']   = TRUE; // this doesn't work for smaller images! See My_image_lib.
        $config['quality']          = 100;
        $config['width']            = $width;
        $config['height']           = $height;

        if (isset($postfix))
        {   $config['thumb_marker'] = $postfix;
            $config['master_dim']   = 'width';
        }

//        $this->image_lib->clear();
        $res = $this->image_lib->initialize($config);
        log_message('error', "_resizePhoto::image_lib->initialize: res = $res");
        log_message('error', '_resizePhoto: image resize error1111111: '.$this->image_lib->display_errors());
        $res = $this->image_lib->resize();
        if(!$res)
        {   log_message('error', '_resizePhoto: image resize error: '.$this->image_lib->display_errors());
        }

        return $res;
    }

    public function createManagersArray()
    {
        $res = array();

        // insert default one:
        $res[-1] = lang('list.none');

        $managers = $this->getManagers();
        foreach ($managers as $mgr)
        {
            $res[$mgr->userId] = $mgr->name;
        }
        
        return $res;
    }

    public function getManagers()
    {
        $CI = &get_instance();
        $CI->load->model('tank_auth/users');

        return $CI->users->get_users_with_roles_full(SecuredMatrix::ROLE_MANAGER);
    }

    public function getManagersAndSupervisors()
    {
        $db_prefix = $this->db->dbprefix;
        $this->db->select("users.id, users.username, users.email, users_groups_link.userId, user_profiles.name, user_profiles.phone, user_profiles.phone2, user_profiles.phone_extension");

        $this->db->from('users_groups_link');
        $this->db->join('users', 'users.id = users_groups_link.userId', 'right');
        $this->db->join('user_profiles', 'users.id = user_profiles.user_id', 'left');
        $this->db->join('user_groups', 'users_groups_link.groupId = user_groups.id', 'left');

        $this->db->where('user_groups.group_type', Utilitar::GROUP_TYPE__ROLES);
        $this->db->where(sprintf("(%suser_groups.name='%s' OR %suser_groups.name='%s')",
                            $db_prefix,
                            SecuredMatrix::ROLE_SUPERVISOR,
                            $db_prefix,
                            SecuredMatrix::ROLE_MANAGER)
                        );
        $this->db->order_by('name');

        $query = $this->db->get();

        return Utilitar_db::safe_resultSet($query);
    }

    public function getFileExtension($file_path)
    {
        $pos = strrpos($file_path, '.');
        if (false !== $pos)
        {
            $ext = substr($file_path, $pos + 1);
            if (strlen($ext) > 0)
            {   return $ext;
            }
        }

        return NULL;
    }

    public function getAlbum($album_id)
    {
        return $this->CI->utilitar_db->_getTableRow('albums', 'id', $album_id);
    }

    public function getAlbums($with_private_albums = false)
    {
        if ($with_private_albums)
        {   return $this->CI->utilitar_db->_getTableContents('albums','date');
        }

        return $this->CI->utilitar_db->_getTableContents('albums','date', 'public_access', 1);
    }

    public function getAlbumPath($album_id)
    {
        return 'uploads/myalbum'.$album_id.'/';
    }


    public function getDocumentTypes()
    {
        return $this->CI->utilitar_db->_getTableContents('document_types','name');
    }

    public function wrapToTypesArray($rows, $topmost_item_value = NULL, $topmost_item_title = NULL)
    {
        $res = array();

        if ($topmost_item_value && $topmost_item_title)
        {   // insert default ones:
            $res[$topmost_item_value] = $topmost_item_title;
        }

        foreach ($rows as $elem)
        {   $res[$elem->id] = $elem->name;
        }

        return $res;
    }


    //
    // Transforms input array into key-value arrays.
    // E.g. for a sample input: wrapToKeyValueArrays($rows, array('id', 'name', 'lastname')) the output will be:
    //          array(
    //                  array("id" => "1", "name" => "Ivan", 'lastname' => 'Ivanych'),
    //                  array("id" => "2", "name" => "Petr", 'lastname' => 'Petrovich'),
    //                  ...
    //               )
    public function wrapToKeyValueArrays(&$rows, $key_field_names)
    {
        $res = array();

        foreach ($rows as $elem)
        {
            // write down all the properties of an object into the result array:
            $properties = array();
            foreach ($key_field_names as $property_name)
            {   $properties[$property_name] = $elem->$property_name;
            }

            $res[] = $properties;
        }

        return $res;
    }

    //
    // It converts classes (!) array to key => {array of values} array.
    // NOTE: this method provides ARRAY of VALUES for a single key!
    public function rowset__convert_to_array(&$rows, $key_field_name)
    {
        $res = array();
        foreach ($rows as $row)
        {   $res[$row->$key_field_name][] = $row; // here is the KEY with multiple VALUES!
        }

        return $res;
    }


    // this must be applied instead "basename()" because it works incorrectly with russian filenames.
    public function enrichDocFilenames(&$docs)
    {
        foreach ($docs as $doc)
        {
            $doc->beautifulName = $this->locale_safe_basename($doc->filepath);
        }
    }

    /**
    * quarterByDate()
    *
    * Return numeric representation of a quarter from passed free-form date.
    *
    * @param mixed $date
    * @return integer
    */
    function quarterByDate($date)
    {
        //     $quarter_title = lang('const.quarter');      --> unknown troubleloading lang-file from a model
        $quarter_title = iconv('windows-1251', 'UTF-8', " кв. ");
        $year_title = iconv('windows-1251', 'UTF-8', " г");

        return ((int)floor(date('m', strtotime($date)) / 3.1) + 1).$quarter_title.date('Y', strtotime($date)).$year_title;
    }


    //
    // This transforms strings like "2 000 000" to "2000000" integer.
    //
    public function shrinkPrice($price)
    {
        $res = str_replace(" ", '', $price);
        return intval($res);
    }


    public function getTagsJSArray()
    {
        $tags_str = $this->utilitar_db->getOption('NewsTags', '');
        $tags = explode(",", $tags_str);
        $str  = '[';
        foreach ($tags as $tag)
        {
            if (strlen($tag) == 0)
                continue;

           $str .= "'$tag',";
        }
        $str .= ']';

        return strtolower($str);
    }


    // helper functions; are useful when switching languages, for exaple.
    public function setLastUrl()
    {
        $CI =& get_instance();
        $CI->session->set_userdata(array('last_url' => current_url()));
    }

    // helper functions; are useful when switching languages, for exaple.
    public function getLastUrl()
    {
        $CI =& get_instance();
        $val = $CI->session->userdata('last_url');

        return ($val !== FALSE ? $val : base_url());
    }

    function remove_http($url)
    {
        return preg_replace("/^https?:\/\/(.+)$/i","\\1", $url);
    }

    function shortenString($input, $maxLen = 30)
    {
        if (strlen($input) > $maxLen) {
            $characters = floor($maxLen / 2);
            return substr($input, 0, $characters) . '...' . substr($input, -1 * $characters);
        }

        return $input;


        if(strlen($input) < $maxLen)
        {
            return $input;
        }

        $midPoint = floor(strlen($input) / 2);
        $startPoint = $midPoint - 1;

        return substr_replace($input, '...', $startPoint, 3);
    }


    public function getWidget($id)
    {
        return $this->CI->utilitar_db->_getTableRow('widgets', 'id', $id);
    }

    public function getWidgetByUID($uid)
    {
        return $this->CI->utilitar_db->_getTableRow('widgets', 'url', $uid);
    }

    public function getWidgets()
    {
        return $this->CI->utilitar_db->_getTableContents('widgets', 'custom_type ASC, name ASC');
    }


    //
    // returns a value submitted from a Checkbox form element:
    //
    public function isCheckBoxSet($value)
    {
        return ('' == $value) ? 0 : 1;// checkbox values to be checked this way.
    }


    //
    // Designed for admins only!
    //
    // 4 input params expected:
    //  $handler_url is the root URL which accepts both $owner_row_id and $photo_id (ordered!).
    //  E.g.:   http://mysite.com/index.php/admin/news/setUniversalPhotoId/$photo_owner_id/$new_photo_id
    //  $current_photo_id   - current photo assigned (if any)
    //  $owner_row_id       - e.g. row for which to set the photo)
    //  $photo_id via POST
    public function createPhotoChooserDivAdmin($album_id, $current_photo_id, $photo_owner_id, $url_photo_assignment_handler)
    {
        if ($photo_owner_id <= 0)
        {   return '<h2 style="color:blue;">Note: please save the record before assigning photos to it.</h2>';
        }

        $photo          = NULL;
        if ($current_photo_id > 0)
        {   $photo      = $this->getPhoto($current_photo_id);
        }

        $album          = $this->getAlbum($album_id);
        $photos         = $this->getAlbumPhotos($album_id);

        $tmp = array(   'photo'             => $photo,
                        'photos'            => $photos,
                        'current_photo_id'  => $current_photo_id,
                        'actionUrl'         => $url_photo_assignment_handler.'/'.$photo_owner_id,
                        'albumDescription'  => ($album? $album->description :''),
                    );

        return $this->load->view('admin/div_choosePhoto', $tmp, true);
    }

    public function get_current_user_id()
    {
        return $this->session->userdata('user_id');
    }

    /**     src: http://nadeausoftware.com/articles/2007/09/php_tip_how_strip_html_tags_web_page
    * Remove HTML tags, including invisible text such as style and
    * script code, and embedded objects.  Add line breaks around
    * block-level tags to prevent word joining after tag removal.
    */
    function strip_html_tags($text)
    {
        $text = preg_replace(
            array(
              // Remove invisible content
                '@<head[^>]*?>.*?</head>@siu',
                '@<style[^>]*?>.*?</style>@siu',
                '@<script[^>]*?.*?</script>@siu',
                '@<object[^>]*?.*?</object>@siu',
                '@<embed[^>]*?.*?</embed>@siu',
                '@<applet[^>]*?.*?</applet>@siu',
                '@<noframes[^>]*?.*?</noframes>@siu',
                '@<noscript[^>]*?.*?</noscript>@siu',
                '@<noembed[^>]*?.*?</noembed>@siu',
              // Add line breaks before and after blocks
                '@</?((address)|(blockquote)|(center)|(del))@iu',
                '@</?((div)|(h[1-9])|(ins)|(isindex)|(p)|(pre))@iu',
                '@</?((dir)|(dl)|(dt)|(dd)|(li)|(menu)|(ol)|(ul))@iu',
                '@</?((table)|(th)|(td)|(caption))@iu',
                '@</?((form)|(button)|(fieldset)|(legend)|(input))@iu',
                '@</?((label)|(select)|(optgroup)|(option)|(textarea))@iu',
                '@</?((frameset)|(frame)|(iframe))@iu',
            ),
            array(
                ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                "\n\$0", "\n\$0", "\n\$0", "\n\$0", "\n\$0", "\n\$0",
                "\n\$0", "\n\$0",
            ),
            $text );
        return str_replace($this->excluded_chars, '', strip_tags($text));
    }

    public static function mb_sprintf($format) {
        $argv = func_get_args() ;
        array_shift($argv) ;
        return Utilitar::mb_vsprintf($format, $argv) ;
    }

    private static function mb_vsprintf($format, $argv) {
        $newargv = array() ;

        $results = array();
        preg_match_all("`\%('.+|[0 ]|)([1-9][0-9]*|)s`U", $format, $results, PREG_SET_ORDER) ;

        foreach($results as $result) {
            list($string_format, $filler, $size) = $result ;
            if(strlen($filler)>1)
                $filler = substr($filler, 1) ;
            while(!is_string($arg = array_shift($argv)))
                $newargv[] = $arg ;
            $pos = strpos($format, $string_format) ;
            $format = substr($format, 0, $pos)
                      . ($size ? str_repeat($filler, $size-strlen($arg)) : '')
                        . str_replace('%', '%%', $arg)
                        . substr($format, $pos+strlen($string_format))
                        ;
        }

        return vsprintf($format, $newargv) ;
    }

    public static function mb_str_equal($str1, $str2)
    {
        return (0 === mb_strpos($str1, $str2) && (mb_strlen($str1) == mb_strlen($str2)));
    }
}

/*
    public function force_ssl()
    {
        if (!is_https())
        {   log_message('error', "TTT: let's redirect to HTTPS instead!");
            redirect('https://walltouch.ru/wtservice/read_data');
        }
        else
        {   log_message('error', "TTT: i am on HTTPS now! :)");
        }
    }
*/
