<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Util_tg_berkana extends CI_Model /* see http://codeigniter.com/wiki/Inserting_Multiple_Records_Into_a_Table/ */
{
	function __construct()
	{
		parent::__construct();
	}

    // NOTES: the result is used to show days as buttons: used in trainer's calendar generation.
    public static function get_dates_trainer_had_trainings($trainer_id, $date_from, $date_to)
    {

    }

    // TODO: get creds from DB (encrypted/salted) based on "$organization_id". Return null
    public static function berkana_get_organization_credentials($yurlico_id)
    {
        $yurlico = Utilitar_db::_get_entity('berk_yurlica', array('id' => $yurlico_id));
        $organizations = Utilitar_db::_get_entities('berk_organizations', array('yurlico_id' => $yurlico_id), 'id');
        if ((!$organizations) || (0 == count($organizations)))
        {   // if none found return a stub:
            return array(   'login'             => 'UNDEFINED1',
                            'password'          => 'UNDEFINED1',

                            'yurlico_id'        => $yurlico_id,
                            'organization_id'   => -888,
                        );
        }

        // otherwise return the first available org found for that yurlico:
        $org = array_shift($organizations);

        if ($yurlico && $yurlico->cloud_url && $yurlico->cloud_login) // NOTE: it is possible to have EMPTY PASSWORD (stupid but true), so we skip "empty password" check here!
        {   return array(   'login'             => $yurlico->cloud_login,
                            'password'          => $yurlico->cloud_pass,

                            'yurlico_id'        => $yurlico_id,
                            'organization_id'   => $org->id,
                        );
        }
        else
        {   return array(   'login'             => 'UNDEFINED2',
                            'password'          => 'UNDEFINED2',

                            'yurlico_id'        => $yurlico_id,
                            'organization_id'   => $org->id,
                );
        }
    }

    private static function _shorten_name($name)
    {
        return $name; // avoid shortening because later we won't be able to determine coursetype_id by hashmap("group_name_full")


        $replaceable = 'Индивидуальное';
        $pos = mb_strpos($name, $replaceable);
        if (0 === $pos)
        {   return 'Индив. '.mb_substr($name, mb_strlen($replaceable));
        }

        $replaceable = 'Ментальная';
        $pos = mb_strpos($name, $replaceable);
        if (0 === $pos)
        {   return 'Ментальн. '.mb_substr($name, mb_strlen($replaceable));
        }

        return $name;
    }

    //
    // This method called on WizardTrainerChecker::BTN_TRAINER_MENU click.
    // Returns groups for the trainer_id for specific day!
    // NOTES: Makes calls to Berkana cloud or to local (offline) DB, depending on cloud_url available for yurlico.
    public static function get_trainer_groups($pageIndex, $pageSize, $params = null)
    {
        log_message('error', "XXX Berkana API-3 params: ".print_r($params, true));

        $tg_user_id     = $params['trainer_id'];
        $bot_name       = $params['bot_name'];
        $yurlico_id     = -1;
        $organization_id= -1;
        $dateformat = WizardTrainerChecker::get_berkana_date_format($yurlico_id);

//        log_message('error', "QQQ get_trainer_groups($pageIndex, $pageSize) params=".print_r($params, true));
        $date           = null;
        $trainer_id     = null;

        if ($params)
        {   // if input params exist then they are following:
            $date       = isset($params['date']) ? $params['date'] : date($dateformat);
            $trainer_id = isset($params['trainer_id']) ? $params['trainer_id'] : -1;

            $yurlico_id     = $params['yurlico_id'];
            $organization_id= $params['organization_id'];
        }
        else
        {
            $current_user = WizardHelper::get_current_user();
            $trainer = Util_tg_berkana::get_trainer__by_tg_user_id($bot_name, $current_user->tg_user_id, $current_user->yurlico_id, $current_user->organization_id);
            if (!$trainer)
            {   log_message('error', "---- ERROR ERROR ERROR get_trainer_groups(): NO trainer not found for current user: tg_user_id=".$current_user->tg_user_id);
                return array();
            }

            $date = date($dateformat);
            $trainer_id = $trainer->id;

            $yurlico_id     = $trainer->yurlico_id;
            $organization_id= $trainer->organization_id;
        }

        if ($trainer_id <= 0)
        {
            $current_user = WizardHelper::get_current_user();
            if (Util_telegram::is_teacher($current_user, true))
            {
                $trainer = Util_tg_berkana::get_trainer__by_tg_user_id($bot_name, $current_user->tg_user_id, $current_user->yurlico_id, $current_user->organization_id);
                if (!$trainer)
                {   log_message('error', "---- ERROR ERROR ERROR get_trainer_groups222(): NO trainer not found for current user: tg_user_id=".$current_user->tg_user_id);
                    return array();
                }

                $trainer_id = $trainer->id;
            }
        }

        $date_from  = $date;//Util_tg_berkana::_get_berkana_styled_date_prev_month();         // e.g. '01/12/19'; // dd/mm/yy
        $date_to    = $date;//Util_tg_berkana::_get_berkana_styled_date_ENTIRE_this_month();  // e.g. '01/31/20'; // mm/dd/yy - BEWARE!

        $creds = Util_tg_berkana::berkana_get_organization_credentials($yurlico_id);

WizardTrainerChecker::bm_start('bm4');
        $cloud_groups = Util_tg_berkana::berkana_api_get_trainer_groups($trainer_id, $date_from, $date_to, $creds['yurlico_id'], $creds['organization_id']);
WizardTrainerChecker::bm_end('bm4');

        if (92 == $yurlico_id)
        {   ;//log_message('error', "XXX get_trainer_groups(y:$yurlico_id, trainer_id:$trainer_id): ".print_r($cloud_groups, true));
        }

        if (!isset($cloud_groups['rows']))
        {   log_message('error', "ERROR retrieving BERKANA TRAINER GROUPS! EXPLORE THE PRECEEDING LOG LINES!");
            return array();
        }
        $groups = $cloud_groups['rows'];

        $res = array();
        if (!Util_tg_berkana::is_dealing_with_cloud($yurlico_id))
        {   // this is assumed to be case for data retrieved from Berkana OFFLINE INSTALLATION:

            foreach ($groups as $group)
            {   // emulate Berkana Cloud response format:
                $res[] = array( 'id'            => $group['coursetype_id'],
                                'name'          => Util_tg_berkana::_shorten_name($group['training']),
                                'name_with_time'=> Util_tg_berkana::_get_name_with_time($group['plannedstarttime'], Util_tg_berkana::_shorten_name($group['training'])),
                                'visit_date'    => $group['planneddate'],
                                'start_time'    => $group['plannedstarttime'],
                                'teacher_id'    => $group['teacher_id'],
                              );
            }
        }
        else
        {   // this is Berkana cloud data:

            //--------------------------------------------------------------+
            // NOTE: Berkana cloud API provides NO coursetype_id, so we get that in a stupid (but still workable) way:
            $tr_groups_names_hashmap = array(); // key: group_name, value: group_id
            $ci = &get_instance();
            $dbrows = $ci->utilitar_db->_getTableContents('berk_coursetypes', 'name', 'yurlico_id', $yurlico_id, 'organization_id', $creds['organization_id']);

            $tr_groups_names_hashmap = DictionaryService::_to_key_value_array($dbrows, 'name', 'id'); // key: group_name, value: "id" of coursetype
            //--------------------------------------------------------------|
            foreach ($groups as $group)
            {
                $res[] = array( 'id'            => isset($tr_groups_names_hashmap[ $group['course'] ]) ? $tr_groups_names_hashmap[$group['course']] : -1,
                                'name'          => $group['course'],
                                'name_with_time'=> Util_tg_berkana::_get_name_with_time($group['actualtime'], $group['course']),
                                'visit_date'    => $group['planneddate'],
                                'start_time'    => $group['actualtime'],
                                'teacher_id'    => $group['teacherid'],
                              );
            }
        }

        return $res;
    }

    public static function _get_name_with_time($start_time, $coursetype_name)
    {
        return $start_time.' '.$coursetype_name;
    }

    // TODO: use $trainer_id!
    public static function _get_visitors($date_only, $trainer_id)
    {
/*  WARNING: THIS DOES NOT WORK for VISITS DURING MULTIPLE DAYS! :( It provides DUPLICATE RECORDS (with different days thoughs) for the same clients. Kept here just for a reference.
SELECT `tasys_clients`.*,  IF ((DATE(tasys_clients_visits.visit_date) = '2019-12-18'), 1, 0) AS visited, tasys_clients_visits.visit_date
    FROM `tasys_clients`
    LEFT JOIN `tasys_clients_visits` ON tasys_clients_visits.client_id = tasys_clients.id
    ORDER BY tasys_clients.name
*/
        $ci = &get_instance();
        $clients = array();
        $clients = $ci->utilitar_db->_getTableContents('clients', 'clients.name', 'type', 1, NULL, NULL); // TODO: refactor to use SINGLE SQL with JOIN instead!

        $clients_keyed = DictionaryService::_to_key_rowvalue_array($clients, 'id');
        $clients_ids = array_keys($clients_keyed);

        // now get visits for those clients:
        $db_prefix = $ci->db->dbprefix;
        $ci->db->where("(".$db_prefix."clients_visits.visit_date IS NULL OR DATE(".$db_prefix."clients_visits.visit_date) = '".$date_only."')");
        $ci->db->where_in($clients_ids);
        $query = $ci->db->get('clients_visits');
        $visits = Utilitar_db::safe_resultSet($query);


        // NOTE: all the succeeding transformation now looks REDUNDANT and TO BE REFACTORED!
        $res = array();
        $visits_keyed_by_user_id = DictionaryService::_to_key_rowvalue_array($visits, 'client_id');

        foreach ($clients as $user)
        {   $visit = isset($visits_keyed_by_user_id[$user->id]) ? $visits_keyed_by_user_id[$user->id] : null;
            $res[] = array('id' => $user->id, 'name' => $user->name, 'visited' => ($visit ? 1 : 0), 'visit_date' => ($visit ? $visit->visit_date : null));
        }

        return $res;
    }

    public static function get_total_count_of_visitors()
    {
        $ci = &get_instance();
        $ci->db->where('type', WC_S1::CL_TYPE__CLIENT);
        return $ci->db->count_all_results('clients');
    }

    public function is_user_visited_today($user_id)
    {
    //  NOTE that WHERE uses "OR" to concatenate conditions! Nice trick!!
    /*  SELECT `tasys_clients`.*,  IF (tasys_clients_visits.visit_date, TRUE, FALSE) AS visited
        FROM `tasys_clients`
        LEFT JOIN `tasys_clients_visits` ON tasys_clients_visits.client_id = tasys_clients.id
        WHERE (tasys_clients_visits.visit_date IS NULL OR DATE(tasys_clients_visits.visit_date) = '2019-12-18')
    */
        $date_only = date('Y-m-d'); // today.

        $ci = &get_instance();
        $db_prefix = $ci->db->dbprefix;
        $ci->db->select('clients.*, IF ('.$db_prefix.'clients_visits.visit_date, 1, 0) AS visited');
        $ci->db->join('clients_visits', 'clients_visits.client_id = clients.id', 'left');
        $ci->db->where('('.$db_prefix.'clients_visits.visit_date IS NULL OR '.Util_tg_berkana::_format_visitdate($date_only).')', null, false);
        $ci->db->limit(1);

        $query = $ci->db->get('clients');
        $visitor = Utilitar_db::safe_resultRow($query);

        return $visitor ? $visitor->visited : false;
    }

    public function set_username($user_id, $username)
    {
        $ci = &get_instance();

        $ci->db->where('id', $user_id);
        $ci->db->update('clients', array('name' => $username));
    }

    public static function remove_user_visit_for_date($user_id, $visit_date = null)
    {   log_message('error', "XXX remove_user_visit_for_date(uid:$user_id, visit_date:$visit_date), cleanedupdate: ".Util_tg_berkana::_format_visitdate($visit_date));

        $ci = &get_instance();

        $ci->db->where('client_id', $user_id);
        $ci->db->where(Util_tg_berkana::_format_visitdate($visit_date), null, false);

        $ci->db->update('clients_visits', array('visited' => false));

        //$ci->HLAbonements->revoke_visit($user_id);
        $ci->HLHistoryService->revoke_visit($user_id, $visit_date);
    }

    // If date== null then getting a visit for TODAY
    // NOTE: "$date_only" must have NO TIME specified!
    public static function get_user_visit_for_date($user_id, $date_only = null)
    {
        $ci = &get_instance();

        $ci->db->where('client_id', intval($user_id));
        $ci->db->where(Util_tg_berkana::_format_visitdate($date_only), null, false);
        $ci->db->limit(1);

        $query = $ci->db->get('clients_visits');
        return Utilitar_db::safe_resultRow($query);
    }

    public static function _format_visitdate($date_only = null)
    {
        $ci = &get_instance();
        $db_prefix = $ci->db->dbprefix;

        return "DATE(".$db_prefix."clients_visits.visit_date) = '".($date_only ? date('Y-m-d', strtotime($date_only)) : date('Y-m-d'))."'";
    }

    // If date== null then getting a visit for TODAY
    // NOTE: $date to be date-only, with no time specified!
    //      $user_type: this is either Client or Partner
    public static function get_clients_visits_for_date($date_only = null, $tr_group_id = null)
    {
        log_message('error', "DATEQUERY2 = DATE(visit_date) = ".($date_only ? $date_only : date('Y-m-d')));

        $ci = &get_instance();
        $ci->db->join('clients', 'clients.id = clients_visits.client_id', 'left');
        $ci->db->where('clients.type', 1 /*$user_type*/); // "1" is for WC_S1::CL_TYPE__CLIENT
        $ci->db->where(Util_tg_berkana::_format_visitdate($date_only), null, false);

        // for specific group only:
        if ($tr_group_id > 0)
        {   $ci->db->where('clients_visits.tr_group_id', $tr_group_id);
        }

        $query = $ci->db->get('clients_visits');
        return Utilitar_db::safe_resultSet($query);
    }

    // returns Berkana client row
    public static function get_tr_client($client_id, $yurlico_id, $organization_id)
    {
        $ci = &get_instance();
        $ci->db->where(array('yurlico_id' => $yurlico_id, 'organization_id' => $organization_id, 'id' => $client_id));
        $ci->db->limit(1);
        $query = $ci->db->get('berk_clients');

        return Utilitar_db::safe_resultRow($query);
    }

    // returns row id if created successfully.
    public static function create_tr_client($id_berkana, $firstname, $lastname, $family_id, $yurlico_id, $organization_id)
    {
        $ci = &get_instance();
        $ci->db->insert('berk_clients', array('id' => $id_berkana, 'firstname' => $firstname, 'lastname' => $lastname, 'family_id' => $family_id, 'yurlico_id' => $yurlico_id, 'organization_id' => $organization_id));

        return $ci->utilitar_db->get_last_inserted_id();
    }

    public static function get_client_by_id($user_id)
    {
        $ci = &get_instance();
        return $ci->utilitar_db->_getTableRow('clients', 'id', $user_id);
    }

    public static function get_client_name($user_id)
    {
        $ci = &get_instance();
        $row = $ci->utilitar_db->_getTableRow('clients', 'id', $user_id);

        return $row ? $row->name : '';
    }

    public static function _format_clients_visits_stats_for_today()
    {
        $visits_today = Util_tg_berkana::get_clients_visits_for_date();
        return "\nВизитов клиентов сегодня: ".count($visits_today).'.';
    }

    // NOTE: teacher ID (that's Berkana's field value) is contained inside 'extra_data' DB field of the ticket.
    public static function get_trainer_id__by_tg_user_id($bot_name, $tg_user_id, $yurlico_id, $organization_id)
    {
        $trainer = Util_tg_berkana::get_trainer__by_tg_user_id($bot_name, $tg_user_id, $yurlico_id, $organization_id);
        return $trainer ? (int)$trainer->extra_data : -1;
    }

    public static function get_trainer__by_trainer_id($bot_name, $trainer_id, $yurlico_id, $organization_id)
    {
/*  SELECT wt_tg_tickets.id AS ticket_id, wt_tg_tickets.extra_data, wt_tg_users.ticket_owner_name, wt_tg_tickets.role, wt_tg_tickets.yurlico_id, wt_tg_tickets.organization_id
    FROM wt_tg_tickets
    LEFT JOIN wt_tg_users ON wt_tg_users.messaging_id = wt_tg_tickets.messaging_id
    WHERE wt_tg_tickets.extra_data = 18 AND wt_tg_tickets.yurlico_id = 90 AND wt_tg_tickets.organization_id=60
 */
        $ci = &get_instance();
        $ci->db->select('tg_users.*, tg_tickets.*');
        $ci->db->join('tg_users', 'tg_users.messaging_id = tg_tickets.messaging_id', 'left');

        $ci->db->where( array(  'tg_tickets.extra_data'     => $trainer_id,
                                'tg_users.bot_name'         => $bot_name,   // a must! Same tg_user may have multiple accounts in different (my) bots.
                                'tg_tickets.role'           => WizardHelper::USER_ROLE__TRAINER,
                                'tg_tickets.yurlico_id'     => $yurlico_id,
                                'tg_tickets.organization_id'=> $organization_id,
                             )
                      );

        $query = $ci->db->get('tg_tickets');
        return Utilitar_db::safe_resultRow($query);
    }

    public static function get_trainer__by_tg_user_id($bot_name, $tg_user_id, $yurlico_id, $organization_id)
    {
        return Util_tg_berkana::_get_tg_user_by_id($bot_name, $tg_user_id, WizardHelper::USER_ROLE__TRAINER, $yurlico_id, $organization_id);
    }

    // NOTE: teacher ID (that's Berkana's field value) is contained inside 'extra_data' DB field of the ticket.
    public static function get_club_admin_id__by_tg_user_id($bot_name, $tg_user_id, $yurlico_id, $organization_id)
    {
        $admin = Util_tg_berkana::_get_tg_user_by_id($bot_name, $tg_user_id, WizardHelper::USER_ROLE__CLUB_ADMIN, $yurlico_id, $organization_id);
        return $admin ? (int)$admin->extra_data : -1;
    }

    private static function _get_tg_user_by_id($bot_name, $tg_user_id, $role_id, $yurlico_id, $organization_id)
    {
/*  SELECT wt_tg_tickets.id AS ticket_id, wt_tg_tickets.extra_data, wt_tg_users.ticket_owner_name, wt_tg_tickets.role, wt_tg_tickets.yurlico_id, wt_tg_tickets.organization_id
    FROM wt_tg_tickets
    LEFT JOIN wt_tg_users ON wt_tg_users.messaging_id = wt_tg_tickets.messaging_id
    WHERE wt_tg_users.tg_user_id = 113088389 AND wt_tg_tickets.yurlico_id = 90 AND wt_tg_tickets.organization_id=60
 */
        $ci = &get_instance();
        $ci->db->select('tg_users.*, tg_tickets.*');
        $ci->db->join('tg_users', 'tg_users.messaging_id = tg_tickets.messaging_id', 'left');

        $ci->db->where( array(  'tg_users.tg_user_id'       => $tg_user_id, // since same tg_user may participate in a number of bots and organizations and in different ROLES then ensure to filter that out.
                                'tg_users.bot_name'         => $bot_name,   // a must! Same tg_user may have multiple accounts in different (my) bots.
                                'tg_tickets.role'           => $role_id,
                                'tg_tickets.yurlico_id'     => $yurlico_id,
                                'tg_tickets.organization_id'=> $organization_id,
                             )
                      );

        $query = $ci->db->get('tg_tickets');
        return Utilitar_db::safe_resultRow($query);
    }

    //----------------------------------------------------------------------------------------------------------------------------+
    //                                              BERKANA CLOUD DATA (de)serialization routines:
    public static function cloud_to_db__save_clients(&$clients, $yurlico_id, $organization_id)
    {
        log_message('error', "QQQ SERIALIZE CLIENTS: ".print_r($clients, true));
        /*$client_ids = array_keys($clients);

        $ci = &get_instance();
        $ci->db->where_in();


        $ci->db->where("(".$db_prefix."clients_visits.visit_date IS NULL OR DATE(".$db_prefix."clients_visits.visit_date) = '".$date_only."')");
        $ci->db->where_in($clients_ids);
        $query = $ci->db->get('clients_visits');
        $visits = Utilitar_db::safe_resultSet($query);


        $query = $ci->db->get('berk_coursetypes');
        return Utilitar_db::safe_resultRow($query);

        foreach ($clients as $client)
        {

        }*/
    }

    public static function cloud_to_db__save_client_visits(&$visits, $yurlico_id, $organization_id)
    {
        log_message('error', "QQQ SERIALIZE VISITS: ".print_r($visits, true));

        throw new Exception('NYI');
    }

    //----------------------------------------------------------------------------------------------------------------------------|


    //----------------------------------------------------------------------------------------------------------------------------+
    //                                              BERKANA NETWORK API SECTION:
    // TODO: try to refactor and reuse _do_berkana_API_request() instead!
    // Returns auth. cookies on success.
    public static function berkana_authorize($yurlico_id, $login, $password)
    {
        $ci = &get_instance();
        $ci->load->model('util_berkana_data');

        $domain     = Util_tg_berkana::config_berkana_get_domain($yurlico_id);
        $cloud_port = Util_tg_berkana::config_berkana_get_cloud_port($yurlico_id);
        if (!$domain)
        {   // yurlico doesn't support cloud: it uses local installation.
            log_message('error', "---- ERROR: berkana_authorize(y:$yurlico_id, l:$login): no cloud domain exists.");
            return null;
        }

        $domain_url = $domain.':'.$cloud_port;
        $curl_url_multiversion = $domain_url."/crm/j_spring_security_check";

        // NOTE: new Spring v5 version does NOT support "crm/j_spring_security_check" and
        // uses "$domain.'crm/login'" URL instead!
        if ("https://conference.olla.tech:8443" == $domain_url)
        {	$curl_url_multiversion = $domain_url."/crm/login";
        }

        //--------------------------------------------------------------+
        // NOTE: we're going to use the output buffer to store the debug info!
        ob_start();
        $out = fopen('php://output', 'w');

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => $curl_url_multiversion, // port is uncluded.
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_SSL_VERIFYPEER => false,
          CURLOPT_SSL_VERIFYHOST => false,
          CURLOPT_HEADER => true, // get headers too with this line

          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
//                          CURLOPT_VERBOSE => true, // DEBUG mode.
                        CURLOPT_STDERR => $out, // Here we set the library verbosity and redirect the error output to the output buffer.
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "method=login&login=".$login."&password=".$password."&=",
          CURLOPT_HTTPHEADER => array(
            "content-type: application/x-www-form-urlencoded"
          ),
        ));

        $response = curl_exec($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        fclose($out);
        $err = curl_error($curl);

        curl_close($curl);

//        $data = ob_get_clean();
//        $data .= PHP_EOL . $response . PHP_EOL;
        // log_message('error', "XXXXXXXXXXXXX RAW RESPONSE #1 (HTTPCODE:$httpcode) for url='$curl_url_multiversion': $data, response=".print_r($response, true)."\n CURL ERROR=$err");

        //--------------------------------------------------------------+
        // find the cookies:
        $matches = array();
        preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $response, $matches);
        $auth_cookies = array();
        foreach($matches[1] as $item)
        {
            $cookie = array();
            parse_str($item, $cookie);
            $auth_cookies = array_merge($auth_cookies, $cookie);
        }
        //--------------------------------------------------------------|

        log_message('error', "---- berkana_authorize() v2 COOKIES RECEIVED: for login-pass $login/$password: ".print_r($auth_cookies, true));
        return $auth_cookies;
    }

    // this method (открыть меню "Статистика") returns dictionaries, like: schedules, users, courses, multisubscriptionDescriptions, membershiptype, locations, sourceInfos, compositeSchedules, paymenttypes, teachers:
    public function berkana_api_get_dictionary_data($yurlico_id)
    {
        $query_data = array('method' => 'loadReferences');
        return Util_tg_berkana::_do_berkana_API_request($yurlico_id, 'crm/statistic.htm', $query_data);
    }

    public static function berkana_api_set_client_visited($visited_tristate, $subscriptionElementId, $yurlico_id, $organization_id)
    {
        $query_data = array('subscriptionElementId' => $subscriptionElementId);
        switch ($visited_tristate)
        {
            case WizardTrainerChecker::STATE_NOT_VISITED_NO_GOOD_REASON: // пропустил: неуважительная причина
                $query_data['method'] = 'cancelAsVisited';
                break;

            case WizardTrainerChecker::STATE_VISITED: // посетил
                $query_data['method'] = 'markAsVisited';
                break;

            case WizardTrainerChecker::STATE_NOT_VISITED_GOOD_REASON: // пропустил: уважительная прочина
                $query_data['method'] = 'markAsNotVisitedWithReason';
                break;
        }

// отменить: итоговый стейт: "не посетил", неуважительная причина.
//method: cancelAsVisited

// отметить "не посетил: уважительная причина"
//method: markAsNotVisitedWithReason

// отметить: не посетил без уважительной причины
//method: cancelNotVisitedWithReason

        if (!Util_tg_berkana::is_dealing_with_cloud($yurlico_id))
        {   return Util_tg_berkana::_do_localDB_mark_visit($visited_tristate, $subscriptionElementId, $yurlico_id, $organization_id);
        }

        return Util_tg_berkana::_do_berkana_API_request($yurlico_id, 'crm/visits.htm', $query_data);
    }

    public static function get_localDB_visit($subscriptionElementId, $yurlico_id, $organization_id)
    {
        $ci = &get_instance();
        $ci->db->where(array('id' => $subscriptionElementId, 'yurlico_id' => $yurlico_id, 'organization_id' => $organization_id));
        $query = $ci->db->get('berk_clients_visits');

        return Utilitar_db::safe_resultRow($query);
    }


    // NOTE: fuck the backward compatibility with Android App: offline Berkana clients will be marked exclusively via Telegram,
    //      and MOBILE marks will be considered as the only TRUTH: until I setup new WebHook mechanism at Berkana offline windows service!!!
    private static function _do_localDB_mark_visit($visited_tristate, $subscriptionElementId, $yurlico_id, $organization_id)
    {
        // get the client: to be compatible with alignVersionsAndValues_MobileAndBerkana(..., $client_id, ...)
        $ci = &get_instance();

        // this is an alternative to usage of ->set_client_visit__mobile() because it seems to be updating WRONG DB field: "berk_clients_visits->schedule_id" instead if "berk_clients_visits->id"!!!
        $ci->db->where(array('id' => $subscriptionElementId, 'yurlico_id' => $yurlico_id, 'organization_id' => $organization_id));

        //    const STATE_NOT_VISITED_NO_GOOD_REASON  = 0; // НЕ посетил/-а (неуважительная причина)
        //    const STATE_VISITED                     = 1; // посетил/-а.
        //    const STATE_NOT_VISITED_GOOD_REASON     = 2; // НЕ посетил/-а (уважительная причина)

        if (WizardTrainerChecker::STATE_VISITED != $visited_tristate)
        {   // QUESTION: shall I mark "$visited_tristate" as zero in this case? For Berkana local DB compatibility...
            $ci->db->set('nvgr', (WizardTrainerChecker::STATE_NOT_VISITED_GOOD_REASON == $visited_tristate) ? 1 : 0);
        }

        $ci->db->set('mobileVisited', $visited_tristate);
        $ci->db->set('mobileVersion', '(IFNULL(mobileVersion, 0) + 1)', FALSE); // increment the mobile version!
        $ci->db->update('berk_clients_visits');
        $success_updating = ($ci->db->affected_rows() > 0);

        if (!$success_updating)
        {   log_message('error', "ERROR updating mobileVisit from Telegram :( Input params are: id:$subscriptionElementId, y:$yurlico_id, orgid:$organization_id");
        }

/*  $sql = "UPDATE wt_berk_clients_visits
            SET mobileVisited=$mobileVisited, mobileVersion=(IFNULL(mobileVersion, 0) + 1)
            WHERE yurlico_id=$yurlico_id AND organization_id=$organization_id AND schedule_id=$schedule_actual_day_id ";
*/

        // now let's align mobile and Berkana data:
        $ci->util_berkana_data->alignVersionsAndValues_MobileAndBerkana_telegram($yurlico_id, $organization_id, $subscriptionElementId);

        // TODO: test that: now the point is TO FEED THAT MOBILE VISIT DATA CORRECTLY to offline Berkana!! VERIFY that!
    }


    // NOTE: teacher data retrieved using statistics API.
    private function berkana_api_get_teacher_stats($teacher_id, $date_from, $date_to, $maxrows, $yurlico_id)
    {
        //--------------------------------------------------------------+
        // NOTE: we're going to use the output buffer to store the debug info!
        $query_data = array('_search'   => false,
                            'page'      => 1,
                            'rows'      => $maxrows,
                            'sidx'      => 'teacherid',
                            'sord'      => 'desc',
                            'teacherid' => $teacher_id,
                            'filterteacherdatefrom' => Util_tg_berkana::cloud_date_to_dmy($date_from),  // NOTE: only Berkana date format is alowed!
                            'filterteacherdateto'   => Util_tg_berkana::cloud_date_to_dmy($date_to),    // NOTE: only Berkana date format is alowed!
                            'method'    => 'filterTeachers',
                            );

        return Util_tg_berkana::_do_berkana_API_request($yurlico_id, 'crm/statistic.htm', $query_data);
    }

    // get children visited at specific day and time! For specific trainer for specific course! but wait... what if there will be MULTIPLE TIMES of the same trainer same day?? H-m...
    public static function berkana_get_cloud_coursetype($coursetype_id, $yurlico_id, $organization_id)
    {
        // log_message('error', "QQQ berkana_get_cloud_coursetype(coursetype_id:$coursetype_id, y:$yurlico_id, orgid:$organization_id)");

        $ci = &get_instance();
        $ci->db->where(array('id' => $coursetype_id, 'yurlico_id' => $yurlico_id, 'organization_id'=> $organization_id));
        $ci->db->limit(1);

        $query = $ci->db->get('berk_coursetypes');
        return Utilitar_db::safe_resultRow($query);
    }

    // NOTE: as there are mupltiple entries for the same coursetype, we limit SQL to return just one.
    public static function get_coursetype_name_by_schedule_id($schedule_id, $yurlico_id, $organization_id)
    {
/*
    SELECT wt_berk_coursetypes.id, wt_berk_coursetypes.name
    FROM wt_berk_coursetypes
    LEFT JOIN wt_berk_schedule ON wt_berk_schedule.coursetype_id = wt_berk_coursetypes.id
    WHERE wt_berk_coursetypes.yurlico_id = 90 AND wt_berk_schedule.yurlico_id = 90
    AND
    LIMIT 1
 */
        $ci = &get_instance();
        $db_prefix = $ci->db->dbprefix;

        $ci->db->select('berk_coursetypes.name');
        $ci->db->join('berk_schedule', 'berk_schedule.coursetype_id = berk_coursetypes.id', 'left');
        $ci->db->where(array('berk_coursetypes.yurlico_id' => $yurlico_id, 'berk_schedule.yurlico_id' => $yurlico_id, 'berk_schedule.id' => (int)$schedule_id));
        $ci->db->limit(1);
        $query = $ci->db->get('berk_coursetypes');
        $res = Utilitar_db::safe_resultRow($query);

        return $res ? $res->name : null;
    }

    // NOTE: as there are mupltiple entries for the same coursetype, we limit SQL to return just one.
    public static function get_coursetype($coursetype_id, $yurlico_id, $organization_id)
    {
        $ci = &get_instance();
        $ci->db->where(array('id' => (int)$coursetype_id, 'yurlico_id' => $yurlico_id, 'organization_id' => $organization_id));
        $ci->db->limit(1);
        $query = $ci->db->get('berk_coursetypes');
        return Utilitar_db::safe_resultRow($query);
    }


    public static function berkana_api_get_day_events($trainer_id, $coursetype_id, $group_date, $yurlico_id, $organization_id)
    {
        log_message('error', "XXX berkana API-1");

        // TODO: reuse berkana_visits_get_single_day() instead!

        // berkana accepts days of this format only, so we format it here:
        $date_formatted_4_berkana = Util_tg_berkana::cloud_date_to_dmy($group_date);


        log_message('error', "XXX berkana_visits_get_single_day(input date: $group_date, trainer_id:$trainer_id, coursetype_id:$coursetype_id, date_formatted_4_berkana: '$date_formatted_4_berkana')");

        $query_data = array('method' => 'loadDayLessons', 'visitsdate' => $date_formatted_4_berkana);
        //log_message('error', "ZZZ query_data for DAY EVENTS (is date format correct??): ".print_r($query_data, true));
        $entire_day_trainings = Util_tg_berkana::_do_berkana_API_request($yurlico_id, 'crm/visits.htm', $query_data);

        //log_message('error', "--- DAYTRAINIGS (y:$yurlico_id, orgid:$organization_id) by $group_date for coursetype_id:$coursetype_id: ".print_r($entire_day_trainings['elements'], true));
        if (!isset($entire_day_trainings['elements']))
        {   log_message('error', "--- ERROR RETRIEVING DAYTRAINIGS (y:$yurlico_id, orgid:$organization_id) by $group_date for coursetype_id:$coursetype_id. Berkana CLOUD responded: ".print_r($entire_day_trainings, true));
            return array();
        }

        return $entire_day_trainings['elements'];
    }


    public static function berkana_api_get_family_details($family_id, $yurlico_id, $organization_id)
    {
        log_message('error', "XXX berkana_api_get_family_details(family_id:$family_id)");

        $query_data = array('method' => 'loadClientInfo', 'childId' => 202, 'familyId' => null, 'subscriptionFilter' => 'active', 'page' =>  1); // also: childId: 202  familyId: null  subscriptionFilter: active  page: 1
        $family_data = Util_tg_berkana::_do_berkana_API_request($yurlico_id, 'crm/client.htm', $query_data);

        log_message('error', "XXX berkana_api_get_family_details() data: ".print_r($family_data, true));

        return $family_data;
    }

    // NOTE: local (offline) MySQL "date" type format is 'Y-m-d', while Berkana Cloud operates with 'd/m/Y' or 'd/m/y'. So we do cnversion here.
    public static function _berkana_get_trainer_groups_offline($trainer_id, $date_from, $date_to, $yurlico_id, $organization_id)
    {
        $ci = &get_instance();
        $ci->load->model('util_berkana_data');

        $dateformat = WizardTrainerChecker::get_berkana_date_format($yurlico_id);

        log_message('error', "XXX _berkana_get_trainer_groups_OFFLINE(trainer_id:$trainer_id, date_from:$date_from, date_to:$date_to, y:$yurlico_id, orgid:$organization_id), dateformat: '$dateformat'");
        $ts_date_from           = DateTime::createFromFormat($dateformat, $date_from)->getTimestamp();
        $ts_date_to             = DateTime::createFromFormat($dateformat, $date_to)->getTimestamp();

        $dateformat_local_mysql = 'Y-m-d'; // my offline DB date format.
        $local_db_date_from     = date($dateformat_local_mysql, $ts_date_from); // convert it to local (mySql) 'date' format to query my DB.
        $local_db_date_to       = date($dateformat_local_mysql, $ts_date_to);   // convert it to local (mySql) 'date' format to query my DB.

        $rows = $ci->util_berkana_data->get_trainer_trainings_between_dates($trainer_id, $yurlico_id, $organization_id, $local_db_date_from, $local_db_date_to);

        //log_message('error', "XXX _berkana_local_get_trainer_groups(trid:$trainer_id, date $local_db_date_from-$local_db_date_to, y:$yurlico_id, orgid:$organization_id):\n".print_r($rows, true));
//        log_message('error', "XXX _berkana_get_trainer_groups_offline(trainer_id:$trainer_id, input date: $date_from - $date_to, MYSQLDATE: '$local_db_date_from - $local_db_date_to', y:$yurlico_id, orgid:$organization_id): ".print_r($rows, true));
        log_message('error', "XXX _berkana_get_trainer_groups_offline(trainer_id:$trainer_id, input date: $date_from - $date_to, MYSQLDATE: '$local_db_date_from - $local_db_date_to', y:$yurlico_id, orgid:$organization_id)");

        $res = array();
        foreach ($rows as $row)
        {
            $res[] = array(
                            'coursetype_id'     => $row->coursetype_id,
                            'training'          => $row->coursetype_name,
                            'planneddate'       => $row->planneddate_cloud_fmt,
                            'plannedstarttime'  => $row->plannedstarttime,
                            'teacher_id'        => $row->trainer_id,
                        );
        }

        return array('rows' => $res); // this DIFFERS from Berkana Cloud response format!!!
    }

    //
    // if "$trainer_id" < 0 then we assime retrieving of ALL data requested, otherwise - just for a specific teacher.
    // WARNING: this returns TWO different formats depending whether there is CLOUD or OFFlINE Berkana data retrieved!!!
    public static function berkana_api_get_trainer_groups($trainer_id, $date_from, $date_to, $yurlico_id, $organization_id)
    {
        log_message('error', "ZZZ berkana_api_get_trainer_groups1(trainer_id:$trainer_id, dates:'$date_from' - '$date_to', y:$yurlico_id, orgid:$organization_id)");

        if (!Util_tg_berkana::is_dealing_with_cloud($yurlico_id))
        {   return Util_tg_berkana::_berkana_get_trainer_groups_offline($trainer_id, $date_from, $date_to, $yurlico_id, $organization_id);
        }


        // cloud berkana accepts only 'd/m/y' date format:
        $query_data = array('_search'   =>  false,
                            'rows'      =>  9999,
                            'page'      =>  1,
                            'sidx'      =>  'actualdate',
                            'sord'      =>  'asc',
                            'teacherid' =>  ($trainer_id > 0) ? $trainer_id : '', // Berkana accepts passing VOID FIELD to show all the teachers for the date interval specified.
                            'filterteacherdatefrom' =>  Util_tg_berkana::cloud_date_to_dmy($date_from),//'01/01/20',
                            'filterteacherdateto'   =>  Util_tg_berkana::cloud_date_to_dmy($date_to),//'31/01/20',
                            'method'    =>  'filterTeachers',
        );

        //log_message('error', "QQQ get_trainer_groups() query_data: ".print_r($query_data, true));

        return Util_tg_berkana::_do_berkana_API_request($yurlico_id, 'crm/statistic.htm', $query_data);
    }

    public static function cloud_date_to_dmy($date)
    {
        $length = strlen($date);
        if (8 == $length)
        {   return $date; // assumed to be 'd/m/y' so we return it as is.
        }
        else if (10 == $length)
        {   $tmp = DateTime::createFromFormat('d/m/Y', $date); // 10 symbols come for "27/02/2020", and lesser for "27/02/20" (d/m/y).
            return $tmp->format('d/m/y');
        }

        log_message('error', "---- WARNING cloud_date_to_dmy('$date') returns AS IS: with no conversion!");
        return $date;// return with no conversion!
    }

    public static function config_berkana_get_domain($yurlico_id)
    {
        $ci = &get_instance();
        $yurlico = $ci->utilitar_db->_getTableRow('berk_yurlica', 'id', $yurlico_id);

        return ($yurlico ? $yurlico->cloud_url : null);
    }

    // WARNING: returns 443 if no port has been specified! Be careful as this method cannot be used as a marker whther there is a CLOUD login or not.
    public static function config_berkana_get_cloud_port($yurlico_id)
    {
        $ci = &get_instance();
        $yurlico = $ci->utilitar_db->_getTableRow('berk_yurlica', 'id', $yurlico_id);

        $cloud_port = 443; // by default.
        if ($yurlico)
        {   $cloud_port = $yurlico->cloud_port; // DB port can be null.
            if ($cloud_port <= 0)
            {   $cloud_port = 443;
            }
        }

        return $cloud_port;
    }

    public static function is_dealing_with_cloud($yurlico_id)
    {
        return (null != Util_tg_berkana::config_berkana_get_domain($yurlico_id));
    }

    // retrieves data for the current user. Question: why not to make that data EXPIRABLE?
    private static function _compose_berk_dbcookies_key(&$current_user)
    {
        if (!$current_user)
        {   return null;
        }
        // compose DB key. Though Ticket itself fully describes yurlico, we add it just to ease the debugging:
        return 'y'.$current_user->yurlico_id.'_'.$current_user->tg_user_id.'_tid_'.$current_user->ticket_id; // considering yurlico_id, tg_user_id and its ticket
    }

    // returns AN ARRAY (with DESERIALIZED cookies) or null if n/a
    private static function _get_berk_cookies_from_dbcache($current_user = null)
    {
        $ci = &get_instance();
        if (!$current_user)
        {   $current_user = WizardHelper::get_current_user();
        }

        if (!$current_user)
        {   return null; // if Berkana access failed, then return null.
        }

        $db_key = Util_tg_berkana::_compose_berk_dbcookies_key($current_user);
        if (!$db_key)
        {   return null;
        }

        $db_value = $ci->utilitar_db->_getTableRow('berk_cached_cookies', 'key', $db_key);

        return $db_value ? unserialize($db_value->value) : null; // TODO: DECRYPT it.
    }

    // saves cookies under current user.
    // NOTE:    This could be converted for cron operations to ignore "current_user" however this might be dangerous for cross-users' access.
    //          So it might make sense creating db-sessions-cache for cron-only sessions.
    private static function _set_berk_cookies_into_dbcache(&$cookies_array)
    {
        $ci = &get_instance();
        $current_user = WizardHelper::get_current_user();
        $db_key = Util_tg_berkana::_compose_berk_dbcookies_key($current_user);
        if (!$db_key)
        {   log_message('error', "WARNING------ _set_berk_cookies_into_dbcache() NO CURRENT USER retrieved! Skipping from saving Berkana cookies into DB!");
            return;
        }

        // unconditionally delete existing cookies:
        $ci->db->where('key', $db_key);
        $ci->db->delete('berk_cached_cookies');

        $db_value = serialize($cookies_array); // TODO: ENCRYPT it
        $ci->db->insert('berk_cached_cookies', array('key' => $db_key, 'value' => $db_value));
        log_message('error', "---- berk_cached_cookies UPDATED with ".print_r($cookies_array, true));
    }

    private static function _do_berkana_API_request($yurlico_id, $url, $query_data)
    {
        // TODO: count the stats of the most used functions! Count it per each club (yurlico).
        log_message('error', "---- TTT API query_data: ".print_r($query_data, true));

        $domain = Util_tg_berkana::config_berkana_get_domain($yurlico_id);
        if (!$domain)
        {   // yurlico doesn't support cloud: it uses local installation.
            return null;
        }

        $cloud_port = Util_tg_berkana::config_berkana_get_cloud_port($yurlico_id);
        $full_url = $domain.':'.$cloud_port.'/'.$url;

        log_message('error', "---- TTT before calling berkana API: '".$full_url."'");

        $ci = &get_instance();
        $current_user = WizardHelper::get_current_user();
        //log_message('error', "AAA current_user: ".print_r($current_user, true));

        $auth_cookies = Util_tg_berkana::_get_berk_cookies_from_dbcache($current_user); // those also could be encrypted!
        if (!$auth_cookies)
        {   log_message('error', "---- TTT no Berk auth cookies in a cache. Retrieving auth cookies anew....");

            // 1. get the new ones:
            $creds          = Util_tg_berkana::berkana_get_organization_credentials($yurlico_id);
            //log_message('error', "wwwwwwww1 creds: ".print_r($creds, true));
            $auth_cookies   = Util_tg_berkana::berkana_authorize($creds['yurlico_id'], $creds['login'], $creds['password']); // login to Berkana cloud.
            if ($auth_cookies)
            {   Util_tg_berkana::_set_berk_cookies_into_dbcache($auth_cookies);
                log_message('error', "---- TTT Berk auth cookies received(case 1): ".print_r($auth_cookies, true));
            }
            else
            {
                log_message('error', "---- TTT Berk auth cookies CANNOT BE received!: ".print_r($auth_cookies, true));
            }
        }

        //log_message('error', "---- XXX Berk auth cookies (case 999): ".print_r($auth_cookies, true));

        if ((!$auth_cookies) || (0 == count($auth_cookies)))
        {   log_message('error', '---- TTT _do_berkana_API_request("'.$full_url.'") ERROR: Berkana AUTH CANNOT BE ACCOMPLISHED: possibly the club owners blocked either my IP or WT CLOUD USER account!!!');
            return array();
        }

        //log_message('error', "---- TTT _do_berkana_API_request(y:$yurlico_id, url:'".$full_url."')");
        $query_data['nd'] = time(); // Berkana requires that for some reason

        //log_message('error', "---- TTT _do_berkana_API_request() QUERY_DATA: ".print_r($query_data, true));

        $compound_result = Util_tg_berkana::_do_Berkana_curl_post($full_url, $query_data, $auth_cookies);
        if (200 != $compound_result['httpcode'])
        { // re-authorize:
            log_message('error', "---- TTT _do_berkana_API_request() WARNING: httpcode:".$compound_result['httpcode'].". Possibly existing cookies are outdated, let's auth anew and re-request the data...");

            $creds          = Util_tg_berkana::berkana_get_organization_credentials($yurlico_id);
            $auth_cookies   = Util_tg_berkana::berkana_authorize($creds['yurlico_id'], $creds['login'], $creds['password']); // login to Berkana cloud.

            // update caches:
            if ($auth_cookies)
            {   Util_tg_berkana::_set_berk_cookies_into_dbcache($auth_cookies);
                //log_message('error', "---- AAA BERK AUTH cookies received(case 2): ".print_r($auth_cookies, true));
            }

            // let's call an API for the second time:
            $compound_result = Util_tg_berkana::_do_Berkana_curl_post($full_url, $query_data, $auth_cookies);
        }

        //log_message('error', "---- berkana API result: ".print_r($compound_result, true));

        // if the 2nd auth request failed then let's fallback:
        if (200 != $compound_result['httpcode'])
        {
            log_message('error', "---- TTT _do_berkana_API_request('$full_url') ERROR ERROR ERROR ERROR FAILED for the 2nd time (HTTP code:".$compound_result['httpcode']."): re-authorization didn't help :(\n".print_r($compound_result, true));
            return null;
        }

         //log_message('error', "---- _do_berkana_API_request() RESULT: ".print_r($compound_result, true));
        if (is_string($compound_result['response']))
        {
            $json = json_decode($compound_result['response'], true);
//             log_message('error', "---- _do_berkana_API_request() RESULT JSON: ".print_r($json, true));

            if ($json && is_array($json) && (count($json) > 0))
            {   $json = Util_tg_berkana::utf8_converter($json);
            }
            else
            {   log_message('error', "----WARNING!!! TTT _do_berkana_API_request(y:$yurlico_id, '$url') response is UNEXPECTED (check it out!): ".print_r($json, true));
            }

            return $json;
        }
        else if (is_bool($compound_result['response']))
        {   return $compound_result['response'];
        }

        return $compound_result['response']; // return as is (that's unexpected by my code, actually!
    }

    private static function _do_Berkana_curl_post($full_url, &$query_data, &$auth_cookies_array)
    {
        ob_start();
        $out = fopen('php://output', 'w');

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => $full_url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_SSL_VERIFYPEER => false,
          CURLOPT_SSL_VERIFYHOST => false,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
//                     CURLOPT_VERBOSE => true, // DEBUG mode.
                    CURLOPT_STDERR => $out, // Here we set the library verbosity and redirect the error output to the output buffer.
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => http_build_query($query_data),
          CURLOPT_COOKIE => Util_tg_berkana::_berkana_auth_cookies_to_string($auth_cookies_array), // "JSESSIONID=5E79D9187144EEBC305F148203C42FF9; trcaglobal=gM5CU3n7Tbv2vqqbYcShGJcGWUWvlsZQ",
          CURLOPT_HTTPHEADER => array(
            "accept: application/json",
            "content-type: application/x-www-form-urlencoded"
          ),
        ));

        $response   = curl_exec($curl);
        $httpcode   = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $err        = curl_error($curl);
        fclose($out);
        curl_close($curl);

        return array('response' => $response, 'httpcode' => $httpcode, 'err' => $err);
    }

    // merges all the cookies passed in into a single string.
    private static function _berkana_auth_cookies_to_string(&$auth_cookies)
    {
        if (count($auth_cookies) <= 0)
        {   return '';
        }

        $separator = '; ';
        $res = '';
        foreach ($auth_cookies as $name => $value)
        {
            $res .= $name.'='.$value.$separator;
        }

        return substr($res, 0, strlen($res) - strlen($separator));
    }


    public static function utf8_converter($array)
    {
        if (is_array($array))
        {
            array_walk_recursive($array, function(&$item, $key){
                //if(!mb_detect_dcoding($item, 'utf-8', true)){
                        $item = html_entity_decode($item);
                //}
            });
        }
        else if (is_string($array))
        {   return html_entity_decode($array);
        }

        // otherwise either return original value or the one proceseed by array_walk_recursive():
        return $array;
    }

    public static function _get_berkana_styled_date_prev_month()
    {   return date('m/01/y', strtotime(date('Y-m')." -1 month")); // get the data beginning the very first day of that month.
    }

    // get current month data till its end (i.e. till its 27th/28th/30th/31st)
    // WARNING WARNING WARNING!!! This method returns ALREADY FORMATTED for BERKANA! Don't try to call strtotime() as it FAILS handling SOME dates passed in and I coudn't find why and how to overcome :(
    public static function _get_berkana_styled_date_ENTIRE_this_month()
    {
        return date('m/'.date('t').'/y'); // concatenate with MAX DAYS in current month: to see even UPCOMING trainings.
    }
    //                                              End of BERKANA NETWORK API SECTION.
    //----------------------------------------------------------------------------------------------------------------------------|


    // NOTE: $teacher_ids could be a single integer or an array of integers.
    public static function get_teachers($bot_name, $yurlico_id = null, $organization_id = null, $teacher_ids = null)
    {
/*
SELECT wt_berk_organizations.name, wt_berk_cities.*, wt_tg_users.tg_user_id, wt_tg_users.messaging_id, wt_tg_users.yurlico_id, wt_tg_users.ticket_owner_name, wt_tg_tickets.extra_data AS teacher_id
FROM wt_tg_users
LEFT JOIN wt_berk_organizations ON wt_berk_organizations.yurlico_id = wt_tg_users.yurlico_id
LEFT JOIN wt_berk_cities ON wt_berk_cities.id = wt_berk_organizations.city_id
WHERE
wt_berk_organizations.yurlico_id = wt_tg_users.yurlico_id
AND wt_berk_organizations.id = wt_tg_users.organization_id
AND wt_tg_users.role = 1 -- teacher
AND wt_tg_users.bot_name = 'WallTouch_Teacher_bot'
ORDER BY wt_tg_users.tg_user_id
 */
        $ci = &get_instance();
        $db_prefix = $ci->db->dbprefix;

        $ci->db->select('berk_organizations.name, berk_cities.*, tg_users.tg_user_id, tg_users.yurlico_id, tg_users.organization_id, tg_users.messaging_id, tg_users.ticket_owner_name, tg_users.logged_in, tg_tickets.extra_data AS teacher_id');
        $ci->db->join('berk_organizations', 'berk_organizations.yurlico_id = tg_users.yurlico_id', 'left');
        $ci->db->join('berk_cities', 'berk_cities.id = berk_organizations.city_id', 'left');
        $ci->db->join('tg_tickets', 'tg_tickets.messaging_id = tg_users.messaging_id', 'left');

        $ci->db->where(array(   'tg_users.role'                 => WizardHelper::USER_ROLE__TRAINER,
                                'tg_users.bot_name'             => $bot_name,
                            )
                      );

        if ($yurlico_id > 0)
        { // optional parameter
            $ci->db->where( array(  'berk_organizations.yurlico_id' => $yurlico_id,
                                    'tg_users.yurlico_id'           => $yurlico_id,
                                  )
                          );
        }
        if ($organization_id > 0)
        { // optional parameter
            $ci->db->where( array(  'berk_organizations.id'         => $organization_id,
                                    'tg_users.organization_id'      => $organization_id,
                                  )
                          );
        }

        if ($teacher_ids)
        {
            if ((is_array($teacher_ids)) && (count($teacher_ids) > 0))
            {
                $ci->db->where_in( 'tg_tickets.extra_data', $teacher_ids);
            }
            else if (is_integer($teacher_ids) && ($teacher_ids > 0))
            { // optional parameter
                $ci->db->where( 'tg_tickets.extra_data', (int)$teacher_ids);
            }
        }

        $ci->db->order_by('tg_users.tg_user_id');
        $query = $ci->db->get('tg_users');
        return Utilitar_db::safe_resultSet($query);
    }
}