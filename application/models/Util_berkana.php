<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH . '/controllers/admin/SecuredMatrix.php'); // greco.
require_once(APPPATH . '/models/Util_berkana_DTOs.php');        // greco.


//
//  WARNING: NO DATABASE ACTIONS ALLOWED IN THIS CLASS! Use "Util_berkana_data" instead!
//  WARNING: NO DATABASE ACTIONS ALLOWED IN THIS CLASS! Use "Util_berkana_data" instead!
//  WARNING: NO DATABASE ACTIONS ALLOWED IN THIS CLASS! Use "Util_berkana_data" instead!
//
//  This class deals with NO DATABASE ACTIONS and just (pre)processes Berkana data back and forward.
//
//  WARNING: NO DATABASE ACTIONS ALLOWED IN THIS CLASS! Use "Util_berkana_data" instead!
//  WARNING: NO DATABASE ACTIONS ALLOWED IN THIS CLASS! Use "Util_berkana_data" instead!
//  WARNING: NO DATABASE ACTIONS ALLOWED IN THIS CLASS! Use "Util_berkana_data" instead!
//
class Util_berkana extends CI_Model /* see http://codeigniter.com/wiki/Inserting_Multiple_Records_Into_a_Table/ */
{
    private $CI;


    function __construct()
    {
        parent::__construct();


        $this->CI = &get_instance();
        //$this->load->library('session');
        
        $this->load->model('utilitar');
        $this->load->model('util_berkana_data');
        $this->load->model('utilitar_db');
        $this->load->model('utilitar_date');

        $this->CI->load->model('tank_auth/users');

        $this->lang->load('data', 'russian');

        // see some patch for loading library from model. see http://stackoverflow.com/questions/2365591/load-a-library-in-a-model-in-codeigniter/2365848#2365848
    }

    public function testDB_Serializer()
    {
        $i = 0;
        $schedules      = $this->getRemoteSchedule(); // w/out params it gets all the schedule events: beware of huge dataset!
        foreach ($schedules as $schedule)
        {
            $tmp = new Berkana_Schedule();
            $tmp->serializeFromDBRow($schedule);
            $tmp->hashCode(); // ask to calculate hashCode()
            print_r($tmp);

            if (++$i > 2)
            {   break;
            }
        }
    }

    //
    // Uses row_id as a key, and the object - as a value.
    public static function transformToHashTable($db_rows, $id_row_name = 'id')
    {
        if (!is_array($db_rows))
        {   log_message('error', "ERROR: transformToHashTable() got not array. ".print_r($db_rows, true));
            return array();
        }

        $res = array();
        foreach ($db_rows as $db_row)
        {   $res[$db_row->$id_row_name] = $db_row;
        }

        return $res;
    }

    //
    //  Note: function returns visits/versions data for Berkana (Util_berkana_data::VISITS_TBL_BERKANA): not mobile ones!
    //
    // TODO: optimize to retrieve from DB only required data: i.e. as per non-void JSON arrays.
    private function getDBContentsHashed()
    {
        $this->util_berkana_data->logSessionYurlicoAndOrganization("getDBContentsHashed");

        // define dates interval relative to today (!):
        $days_before= 1; // NOTE: we MUST create 1 day overlapp compared with Postgre query: due to timezone differences possible.
                         // Otherwise not all visits get retrieved thus creating DUPLICATE INSERT KEYS errors!
        $days_after = 7;

        $trainers       = $this->util_berkana_data->getRemoteTrainers(true, false);
        $coursetypes    = $this->util_berkana_data->getTrainingNames(true); // get removed items as well.
//        $one_week_schedules = $this->util_berkana_data->getRemoteSchedule($days_before, $days_after);
        $rooms          = $this->util_berkana_data->getRemoteRooms();
        $organizations  = $this->util_berkana_data->getRemoteOrganizations();
        $clients        = $this->util_berkana_data->getRemoteClients();
        $families_subscribed    = $this->util_berkana_data->getRemoteFamilies(); // this DB table contains *subscribed families* ONLY!

        return array(   'trainers'              => Util_berkana::transformToHashTable($trainers),
                        'coursetypes'           => Util_berkana::transformToHashTable($coursetypes),
//                        'one_week_schedules'    => Util_berkana::transformToHashTable($one_week_schedules),
                        'rooms'                 => Util_berkana::transformToHashTable($rooms),
                        'organizations'         => Util_berkana::transformToHashTable($organizations),
                        'clients'               => Util_berkana::transformToHashTable($clients),
                        'families_subscribed'   => Util_berkana::transformToHashTable($families_subscribed),
                        );
    }


    public function parseJSON(&$berkana_class_object, $berkana_datatype_string, &$db_collection, &$json_collection, &$insert, &$update)
    {
        // provide void arrays to protect later from stupid NPEs during data retrieval:
        $insert[$berkana_datatype_string] = array();
        $update[$berkana_datatype_string] = array();
/*
if (Util_berkana_data::TBL_PLANNED_VISITS == $berkana_datatype_string)
{
        log_message('error', "+---------------------+");
        log_message('error', "ParseJSON: dumping ALL DB ROWS for TBL_PLANNED_VISITS =\n".print_r($db_collection, true));
        log_message('error', "|---------------------|");

        log_message('error', "+---------------------+");
        log_message('error', "ParseJSON: dumping ALL JSON for TBL_PLANNED_VISITS =\n".print_r($json_collection, true));
        log_message('error', "|---------------------|");
}
*/
        // NOTE: The "key" is row_id, "value" is all the posted properties for a row.
        foreach ($json_collection as $json_object)
        {
            // *manually* append the "row ID" property: it is currently missing from the original JSON posted data (python script to adjust for that).
            $json_object_id                         = $json_object['id'];
            $json_object__db_fields_allowed_only    = $berkana_class_object->createobject_db_fields_only($json_object);

            if (isset($db_collection[$json_object_id]))
            {   // then check if they are equal:
                $berkana_class_object->serializeFromDBRow($db_collection[$json_object_id]);

//                if (Util_berkana_data::TBL_PLANNED_VISITS == $berkana_datatype_string)
//                {   log_message('error', "CUSTOM DUMP ==== iterating over TBL_PLANNED_VISITS:\ndb row:\t".print_r($berkana_class_object, true)."json row:\t".print_r($json_object, true));
//                }

                //$json_object_hashcode = $berkana_class_object->calc_hash_from_JSON_object($json_object);  --> use object with DB-allowed fields instead:
                $json_object_hashcode = $berkana_class_object->calc_hash_from_JSON_object($json_object__db_fields_allowed_only);

                if (!$berkana_class_object->hashesAreEqual($json_object_hashcode))
                {
                    log_message('error', "HASH is DIFFERENT for DB against JSON:\n DB:\n"
                                    .print_r($berkana_class_object, true)
                                    //."\n JSON:\n".print_r($json_object, true)
                                    ."\nPure JSON object (only db fields allowed object) = "
                                    .print_r($json_object__db_fields_allowed_only, true));

                    $update[$berkana_datatype_string][] = $json_object__db_fields_allowed_only;
                }
                else
                { ; // DB and JSON items for $berkana_datatype_string table are equal. Skippping to add them.
                    //log_message('error', "HASHES ARE EQUAL:\nDB=".print_r($berkana_class_object, true)."\nJSON=".print_r($json_object__db_fields_allowed_only, true));
                }
            }
            else
            {
                //log_message('error', "CUSTOM DUMP ==== items MISSING from DB = ".print_r($json_object__db_fields_allowed_only, true));
                $insert[$berkana_datatype_string][] = $json_object__db_fields_allowed_only;
            }
        }

        /*if (Util_berkana_data::TBL_ROOMS == $berkana_datatype_string)
        {   log_message('error', "CUSTOM DUMP ====  INSERT:\t".print_r($insert[$berkana_datatype_string], true)."\nUPDATE:\t".print_r($update[$berkana_datatype_string], true));
        }*/
    }

    private function get_json_item_by_id_field(&$json_collection, $id)
    {
        foreach ($json_collection as $json_item)
        {
            if ($id == $json_item['id'])
            {   return $json_item;
            }
        }

        return NULL;
    }

    // uses alternate approach: finds row ids existing in DB, others  - to be inserted.
    public function parseJSON_v2(&$berkana_class_object, $berkana_datatype_string, &$json_collection, &$insert, &$update)
    {
        // provide void arrays to protect later from stupid NPEs during data retrieval:
        $insert[$berkana_datatype_string] = array();
        $update[$berkana_datatype_string] = array();


       // The steps are pretty simple:
       // 1. find existing rows
       // 1.1. then compare existing rows to JSON rows: to check if DB update is needed. If so - add them into $update[$berkana_datatype_string] array.
       // 2. non-existent rows go directly (after a clearance by "createobject_db_fields_only()") to $insert[$berkana_datatype_string] array.
       // 3. we're done.

        // find IDs already existing at DB:
        $json_collection_IDs        = $this->extract_IDs_from_array($json_collection);

        $db_existing_rows           = $this->get_db_data__by_IDs($json_collection_IDs, $berkana_datatype_string);
        $db_existing_rows_hashtable = Util_berkana::transformToHashTable($db_existing_rows); // used to quickly search by "$row->id".

        // These rows might POTENTIALLY need an update. Not necessarily, so we will check equality later:
        $db_existing_rows_IDs_to_UPDATE = array_keys($db_existing_rows_hashtable);

        $json_rows_IDs_to_INSERT = array_diff($json_collection_IDs, $db_existing_rows_IDs_to_UPDATE);
        log_message('error', "parseJSON_v2 to INSERT (new IDs, $berkana_datatype_string): ".count($json_rows_IDs_to_INSERT));
        log_message('error', "parseJSON_v2 to UPDATE (db existing rows IDs, $berkana_datatype_string): ".count($db_existing_rows_IDs_to_UPDATE));

        // Collect INSERT-able rows first:
        if (count($json_rows_IDs_to_INSERT))
        {
            foreach ($json_rows_IDs_to_INSERT as $json_id)
            {   $json_object        = $this->get_json_item_by_id_field($json_collection, $json_id);
                if (!$json_object)
                {   log_message('error', 'parseJSON_v2: Cannot access JSON element by id='.$json_id);
                    continue;
                }

                $json_object['id']  = $json_id;// append 'id' field! Needed for DB inserts/updates.

                $json_object__db_fields_allowed_only    = $berkana_class_object->createobject_db_fields_only($json_object);
                $insert[$berkana_datatype_string][]     = $json_object__db_fields_allowed_only;
            }

//            log_message('error', "parseJSON_v2 json_ROWS_to_insert = ".print_r($insert[$berkana_datatype_string], true));
        }
        else
        {   log_message('error', "parseJSON_v2 json_ROWS_to_insert: none to insert");
        }


        // Now let's update the existing data, if needed:
        $abonements_to_UPDATE = array();
        foreach ($db_existing_rows as $db_row)
        {
            $json_object = $this->get_json_item_by_id_field($json_collection, $db_row->id); // use the referrer.
            $json_object['id']  = $db_row->id;// append 'id' field! Needed for DB inserts/updates.

            $json_object__db_fields_allowed_only = DBRowSerializer::get_difference_json_object_to_update($db_row, $json_object, $berkana_class_object);
            if ($json_object__db_fields_allowed_only)
            {   log_message('error', "parseJSON_v2 json_object__db_fields_allowed_only=".print_r($json_object__db_fields_allowed_only, true));
                $update[$berkana_datatype_string][] = $json_object__db_fields_allowed_only;
            }
            else
            {   ;// hashes are equal.
            }
        }

        log_message('error', "parseJSON_v2 json_ROWS_to_UPDATE = ".print_r($update[$berkana_datatype_string], true));
    }


    // Note: manually extracts Client's fields.
    private function extractClientsFromDailyVisitsJSON(&$json_DailyVisits)
    {
        $res = array();
        foreach ($json_DailyVisits as $dailyVisit)
        {
            // ensure to insert no duplicates in clients' list: use client_id as a key:
            $client_id      = $dailyVisit['client_id'];
            $res[$client_id]= array('id' => $client_id, 'lastname' => $dailyVisit['lastname'], 'firstname' => $dailyVisit['firstname']);
        }

        // now let's convert it to the format used in other JSON-structures.
        return array_values($res);
    }

    //
    // NOTE: "$uniqueness_by_rows" not yet inmplemented (though usage analogue is already implemented here).
    // NOTE: function does not modify input array. Instead returns out the filtered array.
    private function get_json__without_existing_db_rows($json_messages, $uniqueness_by_rows)
    {
        if (0 == count($json_messages))
        {   return $json_messages; // nothing to go over.
        }
//        log_message('error', "json_messages=".print_r($json_messages, true));

        $json_message_ids = array();
        foreach ($json_messages as $json_message)
        {   $json_message_ids[] = $json_message['id'];
        }

        // extract those json messages which are already present in DB:
        $this->db->select('id');
        $this->util_berkana_data->dbfilter_yurlico_and_organization('berk_messages'); // NOTE: this call is mandatory to be present when accessing berkana assests!
        $this->db->where_in('id', $json_message_ids); // here we already ensured if this array is not empty.
        $query = $this->db->get('berk_messages');
        $db_messages_by_json = Utilitar_db::safe_resultSet($query);

        if (0 == count($db_messages_by_json))
        {   log_message('error', 'no existing DB visitor messages. Skipping processing of get_json__without_existing_db_rows().');
            return $json_messages; // all the JSON messages received are new to DB.
        }

        $json_messages_new_ones = array();
        foreach ($json_messages as $json_message)
        {
            $bFound = false;
            foreach ($db_messages_by_json as $db_message)
            {   // compare by id, family_id and child_id:
                if ($json_message['id'] == $db_message->id)
                {   $bFound = true;
                    break;
                }
            }

            if (!$bFound)
            {   $json_messages_new_ones[] = $json_message;
                ;//log_message('error', 'json message '.sprintf(' (id:%d, f:%d, c:%d) ', $json_message['id'], $json_message['family_id'], $json_message['child_id']).' was not in DB');
            }
            else
            {   ;//log_message('error', 'json message '.sprintf(' (id:%d, f:%d, c:%d) ', $json_message['id'], $json_message['family_id'], $json_message['child_id']).' WAS in DB');
            }
        }

        log_message('error', "NEW json_messages = ".print_r($json_messages_new_ones, true));

        return $json_messages_new_ones;
    }

    // NOTE: it is *extremely important* to keep fields list in sync with JSON fields!!!
    private function prepateAbonementsRawData($json_abonements_raw)
    {
        $basememberships        = array();
        foreach ($json_abonements_raw as $json_abonement_raw)
        {
            $bmshp_id   = $json_abonement_raw['bmshp_id']; // basemembership_id is abonement id in fact.

            $basememberships[$bmshp_id][] = array(  'child_id'      => $json_abonement_raw['child_id'],
                                                    'family_id'     => $json_abonement_raw['family_id'],
                                                    'coursetype_id' => $json_abonement_raw['coursetype_id'],
                                                    'planneddate'   => $json_abonement_raw['planneddate'],
                                                    'removed'       => $json_abonement_raw['removed'],
                                                 );
        }

        return $basememberships;
    }

    private function extractAbonementsFromRawData($json_abonements_RAW)
    {
        $json_abonements = $this->prepateAbonementsRawData($json_abonements_RAW);
//        log_message('error', "prepateAbonementsRawData() result=".print_r($json_abonements, true));
        
        $abonements_structured = array();
        foreach ($json_abonements as $basemembership_id => $array_of_visit_days)
        {
            if (isset($abonements_structured[$basemembership_id]) || (0 == count($array_of_visit_days)))
            {   continue; // this data has been already filled in before.
            }

//            log_message('error', "    array_of_visit_days($basemembership_id)=".print_r($array_of_visit_days, true));
            
            $earliest   = $array_of_visit_days[0]; // we always have at least 1 item at this line.
//            log_message('error', "abonements: EARLIEST(trtid:".$earliest['coursetype_id'].")=".print_r($earliest, true));

            // there might be just one visit, so we should be careful about that:
            $latest     = $array_of_visit_days[count($array_of_visit_days) - 1];
//            log_message('error', "abonements: LATEST(trtid:".$latest['coursetype_id'].")=".print_r($latest, true));

            $abonements_structured[$basemembership_id] = array( 'date_start'    => $earliest['planneddate'],
                                                                'date_end'      => $latest['planneddate'],

                                                                'child_id'      => $earliest['child_id'], // makes no difference if we use earliest of latest in here.
                                                                'family_id'     => $earliest['family_id'],// makes no difference if we use earliest of latest in here.
                                                                'coursetype_id' => $earliest['coursetype_id'],// makes no difference if we use earliest of latest in here.

                                                                'visits_count'  => count($array_of_visit_days),
                                                                'visits_passed' => $this->count_visits_passed_since_today($array_of_visit_days),
                                                                'removed'       => $earliest['removed'],
                                                              );
            if (1931 == $earliest['coursetype_id'])
            {   log_message('error', "extractAbonementsFromRawData(): SELECTED abonement (trtid=".$earliest['coursetype_id'].")=".print_r($abonements_structured[$basemembership_id], true));
            }
        }

//        log_message('error', "FINAL ABONEMENTS DATA: ".print_r($abonements_structured, true));
        return $abonements_structured;
    }


    // NOTE: return array contains non-unique values!
    public function extract_IDs_from_array($json_rows, $field_name = 'id')
    {
        if (!$json_rows || (0 == count ($json_rows)))
        {   log_message('error', "extract_IDs_from_array(): nothing to extract for $field_name");
            return array();
        }


        $array_of_IDs = array();
        foreach ($json_rows as $json_row)
        {   $array_of_IDs[] = $json_row[$field_name];
        }

        return $array_of_IDs;
    }

    // NOTE: it is a must to have 'id' field in the input rows!
    public function extract_IDs_from_objects($db_rows, $field_name = 'id')
    {
        if (!$db_rows)
        {   return array();
        }

        $array_of_IDs = array();
        foreach ($db_rows as $db_row)
        {   $array_of_IDs[] = $db_row->$field_name;
        }

        return $array_of_IDs;
    }

    //
    // Returns IDs array of DB rows, based on JSON rows' id.
    // Does yurlico_id & organization_id filtering behind the scenes.
    private function get_db_data__by_IDs($array_of_IDs, $table_name)
    {
        if (!$array_of_IDs || (0 == count($array_of_IDs)))
        {   return array();
        }
//        log_message('error', "get_db_data__by_IDs(): IDs count=".count($array_of_IDs));
        
        $this->util_berkana_data->dbfilter_yurlico_and_organization($table_name); // NOTE: this call is mandatory to be present when accessing berkana assests!
        $this->db->where_in('id', $array_of_IDs);

        $query = $this->db->get($table_name);
        return Utilitar_db::safe_resultSet($query);
    }

    //
    // The abonements UPDATE is done using complex logic: objects plain comparison make no sense here, thus we're not inheriting from "DBRowSerializer" class for Abonements.
    // Abomenets array key is basemembershipelement_id. Value is the abonement itself.
    private function handleAbonementsInsertUpdate($json_abonements, &$insert)
    {
        // N.B. the logic is not that simple: abomenets data at Berkana might update *incrementally*, so abonements' visits count may/will vary with time.

        // 1. find existing abonements
        //  1.1 check if update is needed for them.
        //  1.1. update carefully: date_end, visits_count, visits_passed
        // 2. abonements not in the DB are a subject for insert.
        $json_abonement_ids = array_keys($json_abonements); // actually those are "basemembership_id"s!
//        log_message('error', "json_abonement_ids(i.e. basemembership_ids)=".print_r($json_abonement_ids, true));

        // find IDs already existing at DB:
        $db_existing_abonements_rows= $this->get_db_data__by_IDs($json_abonement_ids, Util_berkana_data::TBL_ABONEMENTS);
//        log_message('error', "    DB_EXISTING_ABONEMENTS_ROWS (before timestamap modifications)=\n".print_r($db_existing_abonements_rows, true));
        // Here the  "date_start" and "date_start" are not in unix timestamp but in this format:
        //    [date_start] => 2015-09-01 03:00:00
        //    [date_end] => 2015-09-01 03:00:00
        //  ... so let's convert them to UNIX timestamp first:
        foreach ($db_existing_abonements_rows as $db_abonement_row)
        {
            $db_abonement_row->date_start = $this->utilitar_date->mysqldate_to_unix_timestamp($db_abonement_row->date_start);
            $db_abonement_row->date_end   = $this->utilitar_date->mysqldate_to_unix_timestamp($db_abonement_row->date_end);
        }

//        log_message('error', "    DB_EXISTING_ABONEMENTS_ROWS (AFTER timestamp conversion!)=\n".print_r($db_existing_abonements_rows, true));


        // These abonements might POTENTIALLY need an update. Not necessarily, in case if user lives with initialy bought abonement with no changes:
        $abonements_ids_to_UPDATE       = $this->extract_IDs_from_objects($db_existing_abonements_rows);

        // is this needed???
        $insert[Util_berkana_data::TBL_ABONEMENTS] = array();

        $json_abonements_ids_to_INSERT = array_diff($json_abonement_ids, $abonements_ids_to_UPDATE);
//        log_message('error', "abonements_ids_to_INSERT=".print_r($json_abonements_ids_to_INSERT, true));
//        log_message('error', "abonements_ids_to_UPDATE=".print_r($abonements_ids_to_UPDATE, true));


        if (count($json_abonements_ids_to_INSERT))
        {
            foreach ($json_abonements_ids_to_INSERT as $json_id)
            {   $item               = $json_abonements[$json_id];
                $item['id']         = $json_id;// append 'id' field! Needed for DB inserts/updates.

                $item['removed']    = $item['removed'] ? 1 : 0; // set to avoid NULLs in insert! :(

                //----------------------------------------------------------------------------+
                // It is a must to convert strings to int: mostly because timestamps will be distorted otherwise (by MySQL engine):
                $item['date_start'] = (int) $item['date_start'];
                $item['date_end']   = (int) $item['date_end'];
                //----------------------------------------------------------------------------|

                $insert[Util_berkana_data::TBL_ABONEMENTS][] = $item;
            }

//            log_message('error', "json_ABONEMENTS_to_insert = ".print_r($insert[Util_berkana_data::TBL_ABONEMENTS], true));
        }


        // now let's update the existing data, if needed:
        $abonements_to_UPDATE = array();
        foreach ($db_existing_abonements_rows as $db_existing_abonement)
        {
            $json_abonement = $json_abonements[$db_existing_abonement->id]; // use the referrer.

            if (1931 == $json_abonement['coursetype_id'])
            {   log_message('error', "onUPDATE abonement(ctid:1931): json=".print_r($json_abonement, true)."\nDB abonement=".print_r($db_existing_abonement, true));
            }

            $update_this = false;

            // case 1: start-date is adjusted to an earlier date:
            if ((NULL == $db_existing_abonement->date_start) || ($json_abonement['date_start'] < $db_existing_abonement->date_start))
            {
                // adjust the start-date of abonement are
                log_message('error', "JA0 ----will adjust abonement (ctid:".$json_abonement['coursetype_id']." date_start to earlier date: ".$json_abonement['date_start']);
//                $db_existing_abonement->date_start = date("Y-m-d", $json_abonement['date_start']); // update the db row's START DATE.
                $db_existing_abonement->date_start = $json_abonement['date_start']; // update the db row's START DATE.
                $update_this = true;
            }

            // case 2: end-date is adjusted to a later date:
            if ((NULL == $db_existing_abonement->date_end) || ($json_abonement['date_end'] > $db_existing_abonement->date_end))
            {
                // adjust the end-date of abonement are
                log_message('error', "JA1 ----will adjust abonement (ctid:".$json_abonement['coursetype_id']." date_end to later date: ".$json_abonement['date_end']);
//                $db_existing_abonement->date_end = date("Y-m-d", $json_abonement['date_end']); // update the db row's END DATE.
                $db_existing_abonement->date_end = $json_abonement['date_end']; // update the db row's END DATE.
                $update_this = true;
            }

            // case 3: visits total count also might change:
            if ($json_abonement['visits_count'] > $db_existing_abonement->visits_count)
            {
                // adjust the end-date of abonement are
                log_message('error', "JA2 ----will adjust abonement (ctid:".$json_abonement['coursetype_id']." visits_count to: ".$json_abonement['visits_count']);
                $db_existing_abonement->visits_count = $json_abonement['visits_count'];
                $update_this = true;
            }

            // case 4: visits total count also might change:
            if ($json_abonement['visits_passed'] > $db_existing_abonement->visits_passed)
            {
                // adjust the end-date of abonement are
                log_message('error', "JA3 ----will adjust abonement (ctid:".$json_abonement['coursetype_id'].") visits_passed to: ".$json_abonement['visits_passed']);
                $db_existing_abonement->visits_passed = $json_abonement['visits_passed'];
                $update_this = true;
            }

            if ($update_this)
            {   $abonements_to_UPDATE[] = $db_existing_abonement;
            }
        }


//        log_message('error', "json_ABONEMENTS_to_UPDATE = ".print_r($abonements_to_UPDATE, true));

        $yurlico_id     = Json_base::get_current_yurlico_id();
        $organization_id= Json_base::get_current_organization_id();
        foreach ($abonements_to_UPDATE as $row)
        {
//                log_message('error', "\tUPDATE ITEM:\n".print_r($row, true));
            $this->db->where('id', $row->id);
            $row_as_array = array(  'id'            => $row->id,
                                    'coursetype_id' => $row->coursetype_id,
                                    'child_id'      => $row->child_id,
                                    'family_id'     => $row->family_id,
                                    'date_start'    => $row->date_start,
                                    'date_end'      => $row->date_end,
                                    'visits_passed' => $row->visits_passed,
                                    'visits_count'  => $row->visits_count,
                                    'removed'       => $row->removed ? 1 : 0,
                                 );

            $this->util_berkana_data->protectedUpdate(Util_berkana_data::TBL_ABONEMENTS, $row_as_array, $yurlico_id, $organization_id);
        }
        log_message('error', "json_ABONEMENTS UPDATED (".count($abonements_to_UPDATE)." items).");
//        $update[Util_berkana_data::TBL_ABONEMENTS] = $abonements_to_UPDATE;
    }

    private function count_visits_passed_since_today($visits)
    {
        $time_now = strtotime(date('j-n-Y'));
        
        $passed = 0;
        foreach ($visits as $visit)
        {
//        log_message('error', "    visit_planneddate=".($visit['planneddate']).', now='.$time_now);
            if ($visit['planneddate'] < $time_now)
            {   $passed++;
            }
        }

        return $passed;
    }


    // called from read_data() that receives data from Berkana.
    public function handleJSONdata($user_id, &$entityBody)
    {
        if (is_array($entityBody))
        {
            $s = '';
            foreach ($entityBody as $key => $value)
            {   $s .= $key.' ('.count($value).'), ';
            }
            log_message('error', "JSON data keys (uid:$user_id): ".$s);
        }

//        log_message('error', "RAW DATA =".print_r($entityBody, true));

        $this->util_berkana_data->logSessionYurlicoAndOrganization("handleJSONdata");

        // 1. prepare DB data:
        $db_hashed          = $this->getDBContentsHashed();
        $db_trainers        = $db_hashed['trainers'];
        $db_coursetypes     = $db_hashed['coursetypes'];
        $db_rooms           = $db_hashed['rooms'];
        $db_organizations   = $db_hashed['organizations'];
        $db_clients         = $db_hashed['clients'];
        //$db_1week_schedules = $db_hashed['one_week_schedules'];
        $db_families_subscribed = $db_hashed['families_subscribed'];
        $db_new_messages    = array(); // for messages there is no need to deal with existing DB data: we read new (json) messages only.


//log_message('error', "db_1week_schedules=\n".print_r($db_1week_schedules, true));
        // 2. prepare JSON data:
        //----------------------------------------------------------------------------+
        // 2.1 initial check:
/*
        $missing_datasets = '';
        if (!isset($entityBody['Trainers']))
        {   $missing_datasets .= ' (Trainers)';
        }
        if (!isset($entityBody['Coursetype']))
        {   $missing_datasets .= ' (Coursetype)';
        }
        if (!isset($entityBody['Schedule']))
        {   $missing_datasets .= ' (Schedule)';
        }
        if (strlen($missing_datasets) > 0)
        {   log_message('error', "ERROR: not enough JSON data posted! Missing ".$missing_datasets);
            if (is_array($entityBody))
            {   log_message('error', "--- DATA POSTED indeed: \n".print_r(array_keys($entityBody), true));
            }
            return;
        }   */
        //----------------------------------------------------------------------------|

        $json_trainers              = (isset($entityBody['Trainers']))          ? $entityBody['Trainers'] : array();
        $json_coursetypes           = (isset($entityBody['Coursetype']))        ? $entityBody['Coursetype'] : array();
        $json_1week_schedule        = (isset($entityBody['Schedule']))          ? $entityBody['Schedule'] : array();

        $json_rooms                 = (isset($entityBody['Rooms']))             ? $entityBody['Rooms'] : array();
        $json_organizations         = (isset($entityBody['Organizations']))     ? $entityBody['Organizations'] : array();

        // NOTE: JSON will contain only 7 days' visits from Berkana Postgre, while from Webserver MySQL we retrieve 8 (!) days: to avoid timezone differences & unretrieved DB entries: leading to DUPLICATE PRIMARY KEYS during INSERT.
        $json_PlannedDailyVisits = isset($entityBody['DailyVisits']) ? $entityBody['DailyVisits'] : array();
        $json_clients       = $this->extractClientsFromDailyVisitsJSON($json_PlannedDailyVisits);
//        log_message('error', "json_coursetypes=".print_r($json_coursetypes, true));
//        log_message('error', "json_PlannedDailyVisits=".print_r($json_PlannedDailyVisits, true));
//        log_message('error', "json_clients from daily visits=".print_r($json_clients, true));
//        log_message('error', "===========================================");
//        log_message('error', "DB Trainers =\n".print_r($db_trainers, true));
//        log_message('error', "JSON Trainers =\n".print_r($json_trainers, true));
//        log_message('error', "===========================================");
//log_message('error', "json_1week_schedule=".print_r($json_1week_schedule, true));

        //----------------------------------------------------------------------------+
        // this is about personal messaging & news:
        $json_families_subscribed   = (isset($entityBody['SubscribedFamilies']))? $entityBody['SubscribedFamilies'] : array();

        // this parameter created to avoid sending "family_id" with each and every child - even to ones which are non-subscribed to mobile/WT:
        $json_children_subscribed   = (isset($entityBody['SubscribedChildren']))? $entityBody['SubscribedChildren'] : array();

        $json_messages              = (isset($entityBody['Messages']))? $entityBody['Messages'] : array(); 


        // this is not true abonements data: just a set of VISITS :(. We should extract real data from it.
        // TODO: move that to WTSync's responsibility instead.
        $json_abonements_RAW        = (isset($entityBody['Abonements']))? $entityBody['Abonements'] : array();
        //----------------------------------------------------------------------------|


        //----------------------------------------------------------------------------+
        // 4. start proceeding the DB and JSON data:
        $insert = array();
        $update = array();

        // data objects to convert input data:
        $berkana_trainer    = new Berkana_Trainer();
        $berkana_coursetype = new Berkana_Coursetype();
        $berkana_schedule   = new Berkana_Schedule();
        $berkana_room       = new Berkana_Room();
        $berkana_family     = new Berkana_Family();

        $berkana_organization   = new Berkana_Organization();
        
        $berkana_planned_visit  = new Berkana_PlannedVisit();
        $berkana_client         = new Berkana_Client();
        $berkana_message        = new Berkana_Message();

        // compare DB rows against JSON rows and separate into 'insert' and 'update' arrays:
        $this->parseJSON($berkana_trainer,      Util_berkana_data::TBL_TRAINERS,     $db_trainers,          $json_trainers,                 $insert,    $update);
        $this->parseJSON($berkana_coursetype,   Util_berkana_data::TBL_COURSETYPES,  $db_coursetypes,       $json_coursetypes,              $insert,    $update);
//        $this->parseJSON($berkana_schedule,     Util_berkana_data::TBL_SCHEDULE,     $db_1week_schedules,   $json_1week_schedule,           $insert,    $update);
        $this->parseJSON_v2($berkana_schedule,     Util_berkana_data::TBL_SCHEDULE,     $json_1week_schedule,           $insert,    $update);

        $this->parseJSON($berkana_organization, Util_berkana_data::TBL_ORGANIZATIONS,$db_organizations,     $json_organizations,            $insert,    $update);
        $this->parseJSON($berkana_room,         Util_berkana_data::TBL_ROOMS,        $db_rooms,             $json_rooms,                    $insert,    $update);
        $this->parseJSON($berkana_family,       Util_berkana_data::TBL_FAMILIES,     $db_families_subscribed,$json_families_subscribed,     $insert,    $update);
//        log_message('error', "JSON FAMILIES parsed OK! ".print_r($json_families_subscribed, true));

        $this->parseJSON($berkana_client,       Util_berkana_data::TBL_BERK_CLIENTS, $db_clients,           $json_clients,                  $insert,    $update);

        $this->parseJSON_v2($berkana_planned_visit, Util_berkana_data::TBL_PLANNED_VISITS, $json_PlannedDailyVisits,    $insert,    $update); // this version protets from duplicate DB keys!


        //----------------------------------------------------------------------------+
        // custom handling of Abonements raw data :( goes here:
        $json_abonements = $this->extractAbonementsFromRawData($json_abonements_RAW);
//        log_message('error', "json_abonements RAW DATA=".print_r($json_abonements_RAW, true));
//        log_message('error', "json_abonements EXTRACTED (".count($json_abonements)." items)=".print_r($json_abonements, true));

//        log_message('error', "json_families_subscribed RAW DATA=".print_r($json_families_subscribed, true));
//        log_message('error', "json_families_subscribedEXTRACTED (".count($json_families_subscribed)." items):\n".
//                    "INSERT: ".print_r($insert[Util_berkana_data::TBL_FAMILIES], true)."\nUPDATE: ".print_r($update[Util_berkana_data::TBL_FAMILIES], true));

        $this->handleAbonementsInsertUpdate($json_abonements, $insert);
/*

        if (isset($update[Util_berkana_data::TBL_ABONEMENTS]))
        {   log_message('error', "output abonements FOR UPDATE (".count($update[Util_berkana_data::TBL_ABONEMENTS])." items):\n".
                                                            print_r($update[Util_berkana_data::TBL_ABONEMENTS], true));
        }
        else
        {   log_message('error', "output abonements FOR UPDATE: none");        
        }
*/


//        TODO: insert/update this data, taking into account that abonement_start/end dates are subject to change!
//         So update start only if start_new <start_old, and update end only if end_new > end_old.
         
//        log_message('error', "ABONEMENTS EXTRACTED: ".print_r($json_abonements, true));
        // TODO: then insert/update into Util_berkana_data::TBL_ABONEMENTS

        //----------------------------------------------------------------------------|

        //----------------------------------------------------------------------------+
        // ignore DB messages: only insert NEW messages, IGNORING to update existing (in DB) messages:
        $json_messages = $this->get_json__without_existing_db_rows($json_messages, array());
        $this->parseJSON($berkana_message, 'berk_messages',     $db_new_messages,       $json_messages,                 $insert,    $update);
        //----------------------------------------------------------------------------|

//log_message('error', "json_1week_schedule(".count($json_1week_schedule).")=".print_r($json_1week_schedule, true));
//log_message('error', "json_PlannedDailyVisits(".count($json_PlannedDailyVisits).")=".print_r($json_PlannedDailyVisits, true));
//log_message('error', "json_clients(".count($json_clients).")=".print_r($json_clients, true));
//log_message('error', "db_coursetypes(".count($db_coursetypes).")=".print_r($db_coursetypes, true));
//log_message('error', "json_coursetypes(".count($json_coursetypes).")=".print_r($json_coursetypes, true));

        //----------------------------------------------------------------------------+
        // custom processing of Clients and PlannedVisits (are being extracted from the same JSON-structure):
/*
        $all_clients        = array();

        $void_db_array      = array();
        $void_update_array  = array();

        // by using void DB array we extract all info about clients into "$all_clients" array:
        $this->parseJSON($berkana_client, Util_berkana_data::TBL_BERK_CLIENTS, $void_db_array, $json_clients, $all_clients, $void_update_array);
        $all_clients        = isset($all_clients[Util_berkana_data::TBL_BERK_CLIENTS]) ? $all_clients[Util_berkana_data::TBL_BERK_CLIENTS] : array();

        $this->util_berkana_data->separateClientsForInsertAndUpdate($berkana_client, Util_berkana_data::TBL_BERK_CLIENTS, $all_clients, $insert,   $update);
*/

        // TODO: refactor to remove params passsing:
        $yurlico_id         = Json_base::get_current_yurlico_id();
        $organization_id    = Json_base::get_current_organization_id();

        // warning: this method uses "client_id" fieldname from config-SQL (Berkana Server) which is an alias to "Client->id"!
//        $this->util_berkana_data->JSON_insertNewPlannedVisits($berkana_planned_visit, $json_PlannedDailyVisits, $yurlico_id, $organization_id);

//        log_message('error', "RAW DATA FOR SCHEDULE = \n".print_r($json_1week_schedule, true));
//        log_message('error', "RAW DATA FOR TRAINERS = \n".print_r($json_trainers, true));
        //----------------------------------------------------------------------------|

        $count_insert = count($insert);
        $count_update = count($update);

//        log_message('error', "results for INSERT array ($count_insert items; yurlico, org_id: $yurlico_id, $organization_id):\n".print_r($insert, true));
//        log_message('error', "results for UPDATE array ($count_update items; yurlico, org_id: $yurlico_id, $organization_id):\n".print_r($update, true));


        //----------------------------------------------------------------------------+
        // DB update/insert:
        $this->util_berkana_data->insertJsonRecords($insert, $yurlico_id, $organization_id);
        $this->util_berkana_data->updateJsonRecords($update, $yurlico_id, $organization_id);

        
        // now process visits (mobile vs. berkana):
        if (count($json_PlannedDailyVisits) > 0)
        {   log_message('error', "handleJSONData: invoking visits alignment between Berkana and mobile (via alignVersionsAndValues_MobileAndBerkana()).");
            $this->util_berkana_data->alignVersionsAndValues_MobileAndBerkana($yurlico_id, $organization_id, $json_PlannedDailyVisits);
        }
        else
        {   log_message('error', "handleJSONData: no visits to align on. Skipping alignVersionsAndValues_MobileAndBerkana().");
        }

        // "Schedule" JSON contains also custom "comments" field which can be used for custom-fields parsing.
        // Those custom fields to be used when generating "ClubAdmin-view" of the Schedule: to provide an extended (!) info useful for Club Admins.
        $this->util_berkana_data->parse_schedule_comments__to_extract_custom_fields($yurlico_id, $organization_id);

        $this->generate_guids_for_organizations();

        // create family local user accounts (for a mobile login) for the ones that have none associated:
        $this->create_local_users_for_void_families();


        // set family_id for all the subcsribed (to mobile/WT) children:
        foreach ($json_children_subscribed as $json_child_subscribed)
        {
            $this->db->where('id', $json_child_subscribed['id']);
            $this->db->update('berk_clients', array('family_id' => $json_child_subscribed['family_id']));
        }


        // now let's create telegram tickets for *every* family that misses them:
        $families_for_TG = $this->init_telegram_for_families();
        $this->send_welcome_message_for_telegram_families($families_for_TG);

//        log_message('error', 'handleJSONdata: received '.count($json_children_subscribed).' client(s) subscribed to WT/mobile.');
        //----------------------------------------------------------------------------|
    }

    // NOTE: Guids are required to refer to an organization from external world.
    // NOTE: I'm using just unsigned integers rather than GUIDs (as guids seem to be excessive).
    public function generate_guids_for_organizations()
    {
        // generates guids for organizations which have none assigned at the moment.
        $this->db->where('guid IS NULL');
        $query = $this->db->get(Util_berkana_data::TBL_ORGANIZATIONS);
        $orgs_without_guids = Utilitar_db::safe_resultSet($query);

        foreach ($orgs_without_guids as $org)
        {
            $this->db->where('id', $org->id);
            $this->db->update(Util_berkana_data::TBL_ORGANIZATIONS, array('guid' => random_string('nozero', 9)));
        }
    }

    public function play_telegram()
    {
        // convert messages and subjects via htmspecialchars!
        // $this->db->where('tg_tickets.messaging_id IS NULL'); COMENTED OUT for DEBUG-ONLY!!! UNcomment when done!!!
        /*$query      = $this->db->get('berk_messages');
        $messages = Utilitar_db::safe_resultSet($query);
        foreach ($messages as $message)
        {
            $this->db->where('id', $message->id);
            $this->db->where('yurlico_id', $message->yurlico_id);
            $this->db->where('organization_id', $message->organization_id);

            $new_data = array('subject_new' => htmlspecialchars($message->subject), 'body_new' => htmlspecialchars($message->body), );
            $this->db->update('berk_messages', $new_data);
        }
        echo 'converted.';
        return;*/

        // now let's create telegram tickets for *every* family that misses them:
        $families_for_TG = $this->init_telegram_for_families();


        // Get valid families which have no "messaging_id" yet:
//        $this->db->select('berk_families.id, berk_families.email, berk_families.mkey, berk_families.local_user_id');
//        $this->db->where('berk_families.local_user_id IS NOT NULL');
//        $this->db->order_by('email');
//
//        $query      = $this->db->get(Util_berkana_data::TBL_FAMILIES);
//        $families_for_TG =   Utilitar_db::safe_resultSet($query);

//        log_message('error', "families_for_TG=".print_r($families_for_TG, true));

        // TODO: temporarily disabled!
        $this->send_welcome_message_for_telegram_families($families_for_TG);
    }

    // Get families that need user accounts (ROLE_MOBILE_CLIENT).
    private function get_void_families()
    {
        $this->db->where('mkey IS NOT NULL');   // ensure null passwords are not allowed.
        $this->db->where('mkey <> ""');         // ensure null passwords are not allowed.
        $this->db->where('local_user_id IS NULL');

        $query = $this->db->get(Util_berkana_data::TBL_FAMILIES);
        return Utilitar_db::safe_resultSet($query);
    }

    //  NOTE: initiates telegram tickets for (valid) families which have no "messaging_id" yet:
    //  Returns classes array (a rowset) of families handled.
    private function init_telegram_for_families()
    {
/*
SELECT wt_berk_families.yurlico_id, wt_berk_families.organization_id, wt_berk_families.id, wt_berk_families.email, wt_berk_families.mkey, wt_berk_families.local_user_id
FROM wt_berk_families
LEFT JOIN `wt_tg_tickets` ON wt_tg_tickets.local_userid = wt_berk_families.local_user_id
WHERE wt_tg_tickets.messaging_id IS  NULL
AND wt_berk_families.local_user_id IS NOT NULL
ORDER BY email
*/
        // Get valid families which have no "messaging_id" yet:
        $this->db->select('berk_families.yurlico_id, berk_families.organization_id, berk_families.id, berk_families.email, berk_families.mkey, berk_families.local_user_id');
        $this->db->join('tg_tickets', 'tg_tickets.local_userid = berk_families.local_user_id', 'left');
         $this->db->where('tg_tickets.messaging_id IS NULL');
        $this->db->where('berk_families.local_user_id IS NOT NULL');
        $this->db->order_by('email');

        $query      = $this->db->get(Util_berkana_data::TBL_FAMILIES);
        $families   = Utilitar_db::safe_resultSet($query);
        $res = array();
        foreach ($families as $family)
        {
            // if ('nvpolosova@gmail.com' == $family->email) {;}
            // NOTE: 3: WizardHelper::USER_ROLE__CLIENT
            if ($this->util_berkana_data->create_or_update_telegram_ticket($family->local_user_id, 3, $family->yurlico_id, $family->organization_id, $family->mkey, $family->email))
            {   $res[] = $family;
            }
        }

        return $res;
    }

    // uses htmlentities() to save into DB:
    private function add_WTmobile_message($subject, $message, $yurlico_id, $organization_id, $local_user_id, $family_id)
    {
        $this->db->insert('berk_messages',
            array(  'id'                => random_string('nozero'),
                    'yurlico_id'        => $yurlico_id,
                    'organization_id'   => $organization_id,
                    'child_id'          => $local_user_id,
                    'family_id'         => $family_id,
                    'subject'           => Util_berkana::encode_WT_message($subject), // otherwise DB-saving could fail. Maybe use htmlentities()?
                    'body'              => Util_berkana::encode_WT_message($message), // otherwise DB-saving could fail. Maybe use htmlentities()?
                    )
                );
    }

    // NOTE: a must to be called after retrieving WT messages from the DB!
    public static function encode_WT_message($subject_or_message)
    {
        return htmlspecialchars($subject_or_message);
    }

    // NOTE: a must to be called after retrieving WT messages from the DB!
    public static function decode_WT_message($subject_or_message)
    {
        return htmlspecialchars_decode($subject_or_message);
    }

    private function send_welcome_message_for_telegram_families(&$families)
    {
        // via WT messaging:
        foreach ($families as $family)
        {
            // if ('nvpolosova@gmail.com' == $family->email) {;}
            $msg = $this->load->view('theme/view_telegram_welcome_message', array(), true);
            $this->add_WTmobile_message('WallTouch Telegram: наше новое приложение!', $msg, $family->yurlico_id, $family->organization_id, $family->local_user_id, $family->id);
        }

        log_message('error', "FAMILIES TO BE EMAIL-ed: ".print_r($families, true));

        // via email:
        foreach ($families as $family)
        {
            $title      = 'Успешная активация аккаунта';
            $subject    = 'WallTouch Telegram: наше новое приложение!';
            $msg        = $this->load->view('theme/view_telegram_welcome_message', array(), true);
            $mail_body  = $this->load->view('email_templates/customer_email_template', array('title' => $title, 'subject' => $subject, 'message' => $msg), true);

            $data   = $this->utilitar->get_defaultEmailOptions($subject, $mail_body);
            $data['to']     = $family->email;
            $data['from']   = 'info@walltouch.ru';

            $this->utilitar->send_email($data, false);
        }
    }

    //
    // This function creates a user account if family has no no local user linked to. Ignores families with empty "mkey" field.
    // This is required to allow families to log into WT Client app later.
    //
    // NOTE: "mkey" some more checks might be added for the security reason.
    //
    private function create_local_users_for_void_families()
    {
        // get families that need user accounts (ROLE_MOBILE_CLIENT):
        $families_with_no_local_user = $this->get_void_families();
//        log_message('error', "VOID FAMILiES = ".print_r($families_with_no_local_user, true));

        $errors = array();
        foreach ($families_with_no_local_user as $family)
        {
            $userDTO_or_array = $this->util_berkana_data->create_user_account_VISITOR($family->yurlico_id, $family->organization_id, $family->id, $family->email, $family->mkey);

            if ($userDTO_or_array && is_array($userDTO_or_array))
            {   // error occured: it is logged in "$userDTO_or_array" array.
                log_message('error', "ERROR in create_local_users_for_void_families() for family id = ".$family->id.', y:'.$family->yurlico_id.', org_id:'.$family->organization_id);
            }
            else if (is_object($userDTO_or_array))
            {   // success creating user for that family!
                log_message('error', "CREATED USER ACCOUNT: ".print_r($userDTO_or_array, true)."\n for family: ".print_r($family, true));

                // let's update that family record by "local_user_id" and emptying "mkey" field as well:
                $this->db->where('id', $family->id);
                $this->util_berkana_data->dbfilter_yurlico_and_organization(Util_berkana_data::TBL_FAMILIES); // NOTE: this call is mandatory to be present when accessing berkana assests!

                // NOTE: you MUST NOT call this, because telegram passwords will be empty!!!
                //  $this->db->set('mkey', NULL);// NULL-ing "mkey" password field (just in case) after creating a valid local user. Note that we call special method for NULL-ing: default one doesn't work with NULLs.
                $this->db->update(Util_berkana_data::TBL_FAMILIES, array('local_user_id' => $userDTO_or_array->user_id));

                log_message('error', "create_local_users_for_void_families(): success creating local user for family id: ".$family->id.', yurlico: '.$family->yurlico_id.', org_id: '.$family->organization_id);

                // welcome via WT Client:
                $this->send_WT_welcome_message_2client($family->yurlico_id, $family->organization_id, $userDTO_or_array->user_id, $family->id);

                // welcome via email:
                $this->send_email_welcome_2client($family->email, $family->mkey);
            }
        }
    }

    public function send_email_welcome_2client($family_email, $password)
    {
        $title      = 'Успешная активация аккаунта';
        $subject    = 'Добро пожаловать в WallTouch!';
        $msg        = $this->load->view('theme/view_client_email_welcome', array('password' => $password), true);
        $mail_body  = $this->load->view('email_templates/customer_email_template', array('title' => $title, 'subject' => $subject, 'message' => $msg), true);


        $data   = $this->utilitar->get_defaultEmailOptions($subject, $mail_body);
        $data['to']     = $family_email;
        $data['from']   = 'info@walltouch.ru';

        $this->utilitar->send_email($data, false);
    }

    private function send_WT_welcome_message_2client($yurlico_id, $organization_id, $user_id, $family_id)
    {
        $msg = $this->load->view('theme/view_clientapp_welcome_message', array(), true);
        $this->add_WTmobile_message('Добро пожаловать в WallTouch!', $msg, $yurlico_id, $organization_id, $user_id, $family_id);
    }

    // used w/out supplying old password for a verification. Use it CAREFULLY!!
    private function override_user_password($user_id, $new_pass)
    {
        // Hash new password using phpass
        $hasher = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
        $hashed_password = $hasher->HashPassword($new_pass);

        // Replace old password with new one
        $this->users->change_password($user_id, $hashed_password);

        $this->db->where('user_id', $user_id);
        $this->db->update('user_profiles', array('userdata' => $new_pass));
    }

    // notify user about his email change (just for the security reasons)!
    private function notify_about_password_change($username, $email, $new_pass)
    {
        $title      = 'Успешно создан новый пароль';
        $subject    = 'Сгенерирован новый пароль для входа в WallTouch!';
        $msg        = $this->load->view('theme/view_mail_about_password_change', array('username' => $username, 'password' => $new_pass), true);
        $mail_body  = $this->load->view('email_templates/customer_email_template', array('title' => $title, 'subject' => $subject, 'message' => $msg), true);


        $data   = $this->utilitar->get_defaultEmailOptions($subject, $mail_body);
        $data['to']     = $email;
        $data['from']   = 'info@walltouch.ru';
        log_message('error', "XXXX notify_about_password_change($username, $email, $new_pass): ".print_r($data, true));

        $this->utilitar->send_email($data, false);
    }

    private function change_user_account_and_email($local_user_id, $email)
    {
        $this->db->where('id', $local_user_id);
        $this->db->update('users', array('username' => $email, 'email' => $email));
    }

    // notify teacher about email change (just for security reasons)!
    private function notify_teacher_about_email_change($username, $old_email, $new_email)
    {
        $title      = 'Изменён Ваш контактный email в системе WallTouch';
        $subject    = 'Изменён Ваш контактный email в системе WallTouch';
        $msg        = $this->load->view('theme/view_mail_about_email_change', array('username' => $username, 'old_email' => $old_email,  'new_email' => $new_email), true);
        $mail_body  = $this->load->view('email_templates/customer_email_template', array('title' => $title, 'subject' => $subject, 'message' => $msg), true);


        $data   = $this->utilitar->get_defaultEmailOptions($subject, $mail_body);
        $data['to']     = $old_email;
        $data['from']   = 'info@walltouch.ru';

        $this->utilitar->send_email($data, false);
    }

    public function _switchTrainerBan($teacher_id, $organization_id)
    {
        $yurlico_id     = $this->util_berkana_data->session_get_yurlico_id();
        $trainer        = $this->util_berkana_data->getRemoteTrainerByRemoteId($teacher_id, $yurlico_id, NULL);
//        log_message('error', "getRemoteTrainerByRemoteId1($teacher_id, $yurlico_id, NULL)=".print_r($trainer, true));

        if (!$trainer)
        {   log_message('error', "ERROR _switchTrainerBan(teacher_id:$teacher_id, y:$yurlico_id, org_id:$organization_id): teacher not found!");
            return -1; // trainer not found.
        }

        $trainer_user   = $this->users->get_user_by_id_full($trainer->local_user_id);
        if (!$trainer_user)
        {   log_message('error', "ERROR _switchTrainerBan(teacher_id:$teacher_id, y:$yurlico_id, org_id:$organization_id): teacher LOCAL USER not found!");
            return -2; // user account for a trainer has not been activated.
        }

//        log_message('error', "getRemoteTrainerByRemoteId: trainer_user=".print_r($trainer_user, true));

        // ensure teacher and suer belong to the same yurlico:
        if (    ($yurlico_id != $trainer->yurlico_id) ||
                ($yurlico_id != $trainer_user->yurlico_id))
        {   return -3; // don't mix up with another club's pedagogues!
        }

        // otherwise let's ban the pedagogue:
        if (1 == $trainer_user->banned)
        {   $this->users->unban_user($trainer_user->id);
            log_message('error', "_switchTrainerBan($teacher_id, $organization_id): UNBANNED the user.");
        }
        else
        {   $this->users->ban_user($trainer_user->id, 'Blocked by club owner on '.date("Y-m-d H:i:s"));
            log_message('error', "_switchTrainerBan($teacher_id, $organization_id): just BANNED the user!");
        }

        return 0;
    }

    // for errors returns error code. Otherwise returns "true".
    private function verify_yurlico_and_teacher_role(&$user_obj, $yurlico_id)
    {
        // check if:
        //  a) user belongs to the organization we're looking for
        //  b) user has TEACHER ROLE.

        if ($yurlico_id != $user_obj->yurlico_id)
        {   return -5; // users tries to edit data of another yurlico.
        }

        if (!$this->users->user_has_role($user_obj->id, SecuredMatrix::ROLE_TRAINER))
        {   return -6; // this method creates/updates data for TRAINER ROLE accounts only.
        }

        return true;
    }

    // It denies creating trainer accounts for ANY emails already existing in a system! Returns errcode == -4.
    // NOTE:    Returns an array (!) with error code. If success then also adds the newly generated password, too. E.g. array('errcode' => 123, 'pwd' => 'ASD123').
    public function _createOrUpdateTrainerPassword($teacher_id, $organization_id, $email)
    {
        // 1. carefully create a user.
        //    checks needed:
        //      1.1. get user by email. If none then we're fine to proceed further.
        //      1.2. otherwise do additional checks: a) user belongs to the organization we're looking for. b) user HAS TEACHER ROLE.
        // 2. unban user (just in case)
        // 3. generate new password
        // 4. notify via emails: to both old and new ones.
        // 5. regenerate TG ticket!
        // 6. DONE!

        $yurlico_id             = $this->util_berkana_data->session_get_yurlico_id();

        log_message('error', "_createOrUpdateTrainer(berkana_teacher_id:$teacher_id, org_id:$organization_id, $email), y:$yurlico_id");

        //----------------------------------------------------------------------------+
        // Verify trivial permissions first:
        $user_by_email = $this->users->get_user_by_email_full($email);
        if ($user_by_email)
        {
            $result = $this->verify_yurlico_and_teacher_role($user_by_email, $yurlico_id);
            if ($result !== true)
            {   log_message('error', "ERROR: _createOrUpdateTrainer(y:$yurlico_id): failed the check (errcode:$result) for user_BY_EMAIL: ".print_r($user_by_email, true));
                return array('errcode' => intval($result));
            }
        }

        // WARNING: $teacher_id is not a local user id, it is BerkanaID! So we shall find our own local user instead.
        $berkana_row_trainer = $this->util_berkana_data->getRemoteTrainerByRemoteId($teacher_id, $yurlico_id, $organization_id);

        $user_by_id = null;
        if ($berkana_row_trainer && ($berkana_row_trainer->local_user_id > 0))
        {   $user_by_id = $this->users->get_user_by_id_full($berkana_row_trainer->local_user_id);
        }

        if ($user_by_id)
        {
            $result = $this->verify_yurlico_and_teacher_role($user_by_id, $yurlico_id);
            if ($result !== true)
            {   log_message('error', "ERROR: _createOrUpdateTrainer(y:$yurlico_id): failed the check (errcode:$result) for user_BY_ID: ".print_r($user_by_id, true));
                return array('errcode' => intval($result));
            }
        }
        //----------------------------------------------------------------------------|

        $new_pass   = Util_berkana_data::generate_password_no_zeros(6);

        //----------------------------------------------------------------------------+
        //  Actions as per check matrix (get_user_by_XXXX()):
        //      byID  |   byEmail  |   ACTION
        //  --------------------------------------------------------------
        //      1     |      1     |    if (byID->id != byEmail->id) then { email is already taken. Try anew. } else { GENERATE NEW PASSWORD. }
        //      1     |      0     |    change user (byID) email and GENERATE NEW PASSWORD.
        //      0     |      1     |    email is already taken. Try anew.
        //      0     |      0     |    create new user, based on $berkana_row_trainer!

        if ($user_by_id && $user_by_email)
        {
            if ($user_by_id->id != $user_by_email->id)
            {   return array('errcode' => -4); // email is already taken.
            }
            else
            {   // GENERATE NEW PASSWORD.
                $this->_assign_new_password2trainer_and_notify($user_by_id->id, $new_pass, $user_by_id->name, $user_by_id->email, false);

                $new_user_by_id = $this->users->get_user_by_id_full($user_by_id->id);
                log_message('error', "GGG #1: GENERATE NEW PASSWORD for: ".print_r($new_user_by_id, true));
            }
        }
        else if ($user_by_id && !$user_by_email)
        {   // change user (byID) email and GENERATE NEW PASSWORD.

            //----------------------------------------------------------------------------+
            // change user (byID) email:
            $old_email = $user_by_id->email;
            $this->change_user_account_and_email($user_by_id->id, $email);
            $this->notify_teacher_about_email_change($user_by_id->name, $old_email, $email);

            // get user object anew:
            $user_by_id = $this->users->get_user_by_id_full($user_by_id->id);
            //----------------------------------------------------------------------------|

            // GENERATE NEW PASSWORD:
            $this->_assign_new_password2trainer_and_notify($user_by_id->id, $new_pass, $user_by_id->name, $user_by_id->email, false);

            $new_user_by_id = $this->users->get_user_by_id_full($user_by_id->id);
            log_message('error', "GGG #2: change user (byID) email and GENERATE NEW PASSWORD for: ".print_r($new_user_by_id, true));
        }
        else if (!$user_by_id && $user_by_email)
        {   // email is already taken. Try anew.
            return array('errcode' => -4); // email is already taken.
        }
        else if (!$user_by_id && !$user_by_email)
        {   // create new user, based on $berkana_row_trainer!
            $userDTO_or_array = $this->util_berkana_data->create_user_account_TRAINER($berkana_row_trainer, $email, $new_pass);
            if (!$userDTO_or_array || ($userDTO_or_array && is_array($userDTO_or_array)))
            {   // error occured: it is logged in "$userDTO_or_array" array.
                log_message('error', "ERROR when creating new TRAINER USER for berkana_teacher_id:$teacher_id, org_id:$organization_id, trainer_email:$email:\n".print_r($userDTO_or_array, true));
                return array('errcode' => -2); // error during account creation!
            }
            else if (is_object($userDTO_or_array))
            {   // success creating a user!

                // 1. update mapping of berkana_trainer's to local_user_id:
                $this->set_luid_of_trainer($berkana_row_trainer->id, $userDTO_or_array->user_id);

                $new_user_by_id = $this->users->get_user_by_id_full($userDTO_or_array->user_id); // for logging purposes only!
                log_message('error', "GGG #4: successfully created new user, based on berkana_row_trainer. Resulting user: ".print_r($new_user_by_id, true));

                // 2. notify:
                $this->send_email_welcome_2trainer($berkana_row_trainer->name, $berkana_row_trainer->email, $new_pass);
            }
        }
        //----------------------------------------------------------------------------|

        log_message('error', "FINISHED SUCCESSFULLY: _createOrUpdateTrainerPassword($teacher_id, $organization_id, $email), pwd: $new_pass");
        return array('errcode' => 0, 'pwd' => $new_pass);
    }

    // sending Welcome email to new users and a notification about the change - to the existing user.
    private function _assign_new_password2trainer_and_notify($user_id, $new_pass, $trainer_name, $trainer_email, $b_is_new_user)
    {
        // update user's details:
        $this->users->unban_user($user_id); // just in case.
        $this->override_user_password($user_id, $new_pass); // enforces password change w/out knowning its existing password. Use this CAREFULLY!

        if ($b_is_new_user)
        {   $this->send_email_welcome_2trainer($trainer_name, $trainer_email, $new_pass);
        }
        else
        {   $this->notify_about_password_change($trainer_name, $trainer_email, $new_pass);
        }
    }

    public function _change_telegram_ticket_of_Trainer($berkana_trainer_id, $organization_id, $new_password)
    {
        // 1. get trainer's ticket(s)
        // 2. change them to teacher's password.
        $yurlico_id = $this->util_berkana_data->session_get_yurlico_id();
        $trainer    = $this->util_berkana_data->getRemoteTrainerByRemoteId($berkana_trainer_id, $yurlico_id, $organization_id);
        if (!$trainer)
        {   log_message('error', "ERROR #1 finding local user by berkana_trainer_id=$berkana_trainer_id, y:$yurlico_id, org_id:$organization_id !!!");
            return;
        }

        $user = $this->users->get_user_by_id_full($trainer->local_user_id);
        if (!$user)
        {   log_message('error', "ERROR #2 cannot find a local user by local_user_id:$trainer->local_user_id. y:$yurlico_id, org_id:$organization_id !!!");
            return;
        }

        // NOTE: 1 == WizardHelper::USER_ROLE__TRAINER
        if ($this->util_berkana_data->create_or_update_telegram_ticket($trainer->local_user_id, 1, $yurlico_id, $organization_id, $new_password, $user->name))
        {   log_message('error', "_change_telegram_ticket_of_Trainer(berkana_trainer_id:$berkana_trainer_id, $organization_id, $new_password): changed tickets successfully?!");
        }
        else
        {   log_message('error', "ERROR #3 changing TG ticket! local_user_id:$user->id, $yurlico_id, $organization_id, y:$yurlico_id, org_id:$organization_id !!!");
            return;
        }
    }

    // sets "local_user_id" for a trainer.
    private function set_luid_of_trainer($berkana_trainer_id, $new_local_user_id)
    {
        $this->db->where('id', $berkana_trainer_id);
        $this->util_berkana_data->dbfilter_yurlico_and_organization(Util_berkana_data::TBL_TRAINERS); // NOTE: this call is mandatory to be present when accessing berkana assests!
        $this->db->update(Util_berkana_data::TBL_TRAINERS, array('local_user_id' => $new_local_user_id));
    }

    public function send_email_welcome_2trainer($username, $trainer_email, $password)
    {
        $title      = 'Успешная активация аккаунта';
        $subject    = 'Добро пожаловать в WallTouch!';
        $msg        = $this->load->view('theme/view_trainer_email_welcome', array('username' => $username, 'password' => $password), true);
        $mail_body  = $this->load->view('email_templates/customer_email_template', array('title' => $title, 'subject' => $subject, 'message' => $msg), true);


        $data   = $this->utilitar->get_defaultEmailOptions($subject, $mail_body);
        $data['to']     = $trainer_email;
        $data['from']   = 'info@walltouch.ru';

        log_message('error', "XXXX send_email_welcome_2trainer($username, $trainer_email, $password): ".print_r($data, true));
        $this->utilitar->send_email($data, false);
    }
}
