<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Utilitar_db extends CI_Model /* see http://codeigniter.com/wiki/Inserting_Multiple_Records_Into_a_Table/ */
{
    private $CI;

	function __construct()
	{
		parent::__construct();
	}

    // tries its best to return a field with *some* value.
    public function available_lang(&$row, $fieldName1, $fieldName2 = NULL)
    {
        if ($row->$fieldName1 != '')
        {   return $row->$fieldName1;
        }

        // assign second value only if the first one was not found:
        if (!$fieldName2)
        {   $fieldName2 = $fieldName1;
        }

        return $row->$fieldName2;
    }

	public function my_count_all($byRowName, $tableName, $where = '')
    {
        $sql = sprintf("SELECT COUNT(%s) AS numrows FROM %s%s ", $byRowName,  $this->db->dbprefix, $tableName);
        if ('' != $where)
        {   $sql .= " WHERE $where";
        }

        $query = $this->db->query($sql);

        if ($query->num_rows() == 0)
            return '0';

        $row = $query->row();
        return $row->numrows;
    }

    public function _getTableContents($table_name, $order_by = 'id', $where_parameter1 = NULL, $where_value1 = NULL, $where_parameter2 = NULL, $where_value2 = NULL, $limit = -1, $offset = 0, $group_by = NULL)
    {
        if ($where_parameter1 && $where_value1)
        {   $this->db->where($where_parameter1, $where_value1);

            if ($where_parameter2 && $where_value2)
            {   $this->db->where($where_parameter2, $where_value2);
            }
        }

        if ($limit > 0)
        {   $this->db->limit($limit, $offset);
        }

        if ($group_by > 0)
        {   $this->db->group_by($group_by);
        }


        $this->db->order_by($order_by);
//        log_message('error', "sql = ".$this->db->_compile_select());
        $query = $this->db->get($table_name);

        return Utilitar_db::safe_resultSet($query);
    }

    // N.B. This is *full duplicate* of _getTableContents(), just returns not a collection but an array.
    public function _getTableContents_array($table_name, $order_by = 'id', $where_parameter1 = NULL, $where_value1 = NULL, $where_parameter2 = NULL, $where_value2 = NULL, $limit = -1, $offset = 0, $group_by = NULL)
    {
        if ($where_parameter1 && $where_value1)
        {   $this->db->where($where_parameter1, $where_value1);

            if ($where_parameter2 && $where_value2)
            {   $this->db->where($where_parameter2, $where_value2);
            }
        }

        if ($limit > 0)
        {   $this->db->limit($limit, $offset);
        }

        if ($group_by > 0)
        {   $this->db->group_by($group_by);
        }


        $this->db->order_by($order_by);
//        log_message('error', "sql = ".$this->db->_compile_select());
        $query = $this->db->get($table_name);

        return Utilitar_db::safe_resultSetArray($query);
    }

    //
    // This method is applicable for functions that RETURN A SET OF ROWS!
    public static function safe_resultSet($query)
    {
        if ((!$query) || ($query->num_rows() <= 0))
        {   return array();
        }

        return $query->result();
    }

    //
    // This method is applicable for functions that RETURN An ARRAY OF a resultset!
    public static function safe_resultSetArray($query)
    {
        if ((!$query) || ($query->num_rows() <= 0))
        {   return array();
        }

        return $query->result_array();
    }

    //
    // This method is applicable to functions that in return expect ONLY ONE ROW ITEM!
    public static function safe_resultRow($query)
    {
        if ((!$query) || ($query->num_rows() <= 0))
        {   return NULL;
        }

        $res = $query->result();
        return $res[0];
    }

    public function _getTableRow($table_name, $where_parameter = NULL, $where_value = NULL, $order_by = NULL)
    {
        if ($where_parameter)
        {   $this->db->where($where_parameter, $where_value);
        }

        if ($order_by)
        {   $this->db->order_by($order_by);
        }

        $this->db->limit(1);
//        log_message('error', "sql = ".$this->db->_compile_select());
        $query = $this->db->get($table_name);

        return Utilitar_db::safe_resultRow($query);
    }

    public function _getTableRowLike($table_name, $like_parameter = NULL, $like_value = NULL)
    {
        if ($like_parameter && $like_value)
        {   $this->db->like($like_parameter, $like_value);
        }

        $this->db->limit(1);
//        log_message('error', "sql = ".$this->db->_compile_select());
        $query = $this->db->get($table_name);

        return Utilitar_db::safe_resultRow($query);
    }

    public function insert_or_update($key_field_name, $key_field_value, $table_name, $data)
    {
        $this->db->where($key_field_name, $key_field_value);
        $query = $this->db->get($table_name);

        if ($query->num_rows() > 0)
        {   $this->db->where($key_field_name, $key_field_value);
            unset($data[$key_field_name]); // no need to set the unique key in the table.
            $this->db->update($table_name, $data);
        }
        else
        {   $this->db->insert($table_name, $data);
        }
    }

    public function insert_or_update2($key_field_name1, $key_field_value1, $key_field_name2, $key_field_value2, $table_name, $data)
    {
        $this->db->where($key_field_name1, $key_field_value1);
        $this->db->where($key_field_name2, $key_field_value2);
        $query = $this->db->get($table_name);

        if ($query->num_rows() > 0)
        {
            // log_message('error', "XXX insert_or_update2($key_field_name1, $key_field_value1, $key_field_name2, $key_field_value2, $table_name): TO BE UPDATED (searched by: ($key_field_name1 = $key_field_value1) AND ($key_field_name2, $key_field_value2))");
            $this->db->where($key_field_name1, $key_field_value1);
            $this->db->where($key_field_name2, $key_field_value2);
            $this->db->update($table_name, $data);
        }
        else
        {   // log_message('error', "XXX insert_or_update2($key_field_name1, $key_field_value1, $key_field_name2, $key_field_value2, $table_name): TO BE INSERTED (searched by: ($key_field_name1 = $key_field_value1) AND ($key_field_name2, $key_field_value2))");
            $this->db->insert($table_name, $data);
        }
    }

    //
    // Returns row id of just inserted row.
    //
    public function get_last_inserted_id()
    {
        return $this->db->insert_id();
    }

    public function get_last_inserted_row($tablename, $id_fieldname = 'id')
    {
        $id = $this->get_last_inserted_id();
        return $this->_getTableRow($tablename, $id_fieldname, $id);
    }

    //
    // Useful while debugging queries.
    //
    public function get_sql_last_run()
    {
        return $this->db->last_query();
    }

    //
    // Useful while debugging queries. To be called BEFORE calling db->get()!
    //
    public function get_sql_compiled()
    {
        return $this->db->get_compiled_select();
    }

    //
    // Useful while debugging queries: returns SQL currently being constructed. NOTE: made protected and non-callable in recent CI versions!
    //
    /*public function get_sql_constructed()
    {
        return $this->db->_compile_select();
    }*/

    // get options set that *starts with* $startMask specified.
    public function getOptionsSet($startMask)
    {
        if ('' == $startMask)
        {   return array();
        }

        $this->db->where("name LIKE '$startMask%'");
        $query = $this->db->get('options');

        return Utilitar_db::safe_resultSet($query);
    }

    // get option's value from DB. Creates new option with default value if the one not exist.
    public function getOption($name, $defaultValue)
    {
        if ('' == $name)
        {   return NULL; //'unknnown option';
        }

        $this->db->where('name', $name);
        $query = $this->db->get('options');

        if (!$query || $query->num_rows() <= 0)
        {   // create one:
            $this->setOption($name, $defaultValue);
            return $defaultValue; // no such option exists.
        }

        $row = $query->result();

        return $row[0]->value;
    }

    // set option's value into DB:
    public function setOption($name, $value)
    {
        if ('' == $name)
        {   return; //'unknnown option';
        }

        $this->db->where('name', $name);
        $query = $this->db->get('options');

        if ($query->num_rows() <= 0)
        {
            // insert:
            $data = array('name' => $name, 'value' => $value);
            $this->db->insert('options', $data);
        }
        else
        {
            // update:
            $data = array('value' => $value);

            $this->db->where('name', $name);
            $this->db->update('options', $data);
        }
    }

    // Note: returns either array or string (depends on input params: single string or array of strings).
    public function db_prefix($table_names)
    {
        $db_prefix = $this->db->dbprefix;

        if (is_array($table_names))
        {
            foreach ($table_names as &$single_table)
            {
                if (is_string($single_table))
                {   $single_table = $db_prefix.$single_table;
                }

            }
            return $table_names;
        }
        else if (is_string($table_names))
        {   return $db_prefix.$table_names;
        }
    }


    //-----------------------------------------------------------+
    // generic DB methods here:
    public static function _create_entity($db_tablename, $params)
    {
        $ci = &get_instance();
        $ci->db->insert($db_tablename, $params);
        return $ci->utilitar_db->get_last_inserted_id();
    }

    public static function _update_entity($db_tablename, $entity_id, array $new_values, array $where_params = null)
    {
        $ci = &get_instance();

        if ($entity_id > 0)
        {   $ci->db->where('id', $entity_id); // this field is a must (hope that 'id' fieldname will be common in all my tables).
            // WARNING: in some cases there is NO "id" column!
        }

        if ($where_params) // those are optional params
        {   $ci->db->where($where_params);
        }

        return $ci->db->update($db_tablename, $new_values);
    }

    public static function _delete_entity($db_tablename, $entity_id, array $where_params = null)
    {
        $ci = &get_instance();

        if ($entity_id > 0)
        {   $ci->db->where('id', $entity_id); // this field is a must (hope that 'id' fieldname will be common in all my tables).
        }

        if ($where_params) // those are optional params
        {   $ci->db->where($where_params);
        }

        if ($entity_id || ($where_params & count($where_params) > 0))
        {   return $ci->db->delete($db_tablename);
        }
        else
        {   log_message('error', "---- WARNING: called _delete_entity('$db_tablename') with NO params set! Skiping it!!!");
        }
    }

    // combines "Insert or Update" function.
    // returns dbrow->id for both cases.
    public static function _upsert_entity($db_tablename, array $insert_update_values, array $where_params)
    {
        $row = Utilitar_db::_get_entity($db_tablename, $where_params);
        if ($row)
        {   Utilitar_db::_update_entity($db_tablename, isset($where_params['id']) ? $where_params['id'] : -1, $insert_update_values, $where_params);
            return $row->id;// 'id' is not always present!! :((
        }

        return Utilitar_db::_create_entity($db_tablename, $insert_update_values);
    }


    public static function _get_entity($db_tablename, array $where_params)
    {
        $ci = &get_instance();
        $ci->db->where($where_params);
        $query = $ci->db->get($db_tablename);

        return Utilitar_db::safe_resultRow($query);
    }

    public  static function _get_entities($db_tablename, $where_params = null, $order_by = null)
    {
        $ci = &get_instance();
        if ($where_params)
        {   $ci->db->where($where_params);
        }

        if ($order_by)
        {   $ci->db->order_by($order_by);
        }
        $query = $ci->db->get($db_tablename);

        return Utilitar_db::safe_resultSet($query);
    }
    //-----------------------------------------------------------|
}