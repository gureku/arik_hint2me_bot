<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Users
 *
 * This model represents user authentication data. It operates the following tables:
 * - user account data,
 * - user profiles
 *
 * @package	Tank_auth
 * @author	Ilya Konyukhov (http://konyukhov.com/soft/)
 */
class Users extends CI_Model
{
	private $table_name			= 'users';			// user accounts
	private $profile_table_name	= 'user_profiles';	// user profiles

	private $user_groups_table_name	        = 'user_groups'; // user groups
	private $user_groups_links_table_name	= 'users_groups_link'; // many-to-many table name


	function __construct()
	{
		parent::__construct();

		$ci =& get_instance();
		$this->table_name			= $ci->config->item('db_table_prefix', 'tank_auth').$this->table_name;
		$this->profile_table_name	= $ci->config->item('db_table_prefix', 'tank_auth').$this->profile_table_name;
	}

	/**
	 * Get user record by Id and by activated status true/false
	 *
	 * @param	int
	 * @param	bool
	 * @return	object
	 */
	function get_user_by_id($user_id, $activated)
	{
		$this->db->where('id', $user_id);
		$this->db->where('activated', $activated ? 1 : 0);

		$query = $this->db->get($this->table_name);
		if ($query && $query->num_rows() == 1) return $query->row();
		return NULL;
	}

    
    /**
    * Get user record by Id along with its profile row as well.
    *
    * @param	int
    * @param	bool
    * @return	object
    */
    function get_user_by_id_full($user_id)
    {
        $this->db->select('users.id, users.username, users.email, users.activated, users.banned, user_profiles.name, user_profiles.yurlico_id, user_profiles.organization_id, user_profiles.phone, user_profiles.description');
        $this->db->from('users');
        $this->db->join('user_profiles', 'user_profiles.user_id = users.id', 'left');
        $this->db->where('users.id', $user_id);

        $query = $this->db->get();

        if ($query->num_rows() <= 0)
        {   return NULL;
        }

        $rows = $query->result();
        return $rows[0];
    }

    // get users list along with their profile data:
    function get_users_list()
    {
        $this->db->select('users.id, users.username, users.email, users.created, users.activated, users.banned, user_profiles.name, user_profiles.phone');
        $this->db->from('users');
        $this->db->join('user_profiles', 'user_profiles.user_id = users.id', 'left');
        $this->db->order_by('users.id');

        $query = $this->db->get();

        if ($query->num_rows() <= 0)
        {   return array();
        }

        return $query->result();
    }

    function getUserRoleNames($user_id)
    {
    /*SELECT re_users.id AS userId, re_users.username, re_user_groups.name AS roleName
FROM re_users
LEFT JOIN re_users_groups_link ON re_users.id = re_users_groups_link.userId
LEFT JOIN re_user_groups ON re_users_groups_link.groupId = re_user_groups.id
WHERE userid = 6*/

        $this->db->select('users.id as userId, user_groups.name AS roleName');
        $this->db->from('users');
        $this->db->join('users_groups_link', 'users.id = users_groups_link.userId', 'left');
        $this->db->join('user_groups', 'users_groups_link.groupId = user_groups.id', 'left');
        $this->db->where('users.id', $user_id);

        $this->db->order_by('user_groups.name ASC');


        $query = $this->db->get();

        if ($query->num_rows() <= 0)
        {   return array();
        }

        return $query->result();
    }

    function set_user_details($user_id, $yurlico_id, $organization_club_id, $username, $name_fio, $account_status, $email, $phone, $description)
    {
        if (false !== $account_status)
        {   $this->db->set('activated', $account_status);
        }

        if ($email)
        {   $this->db->set('email', $email);
        }

        if ($username)
        {   $this->db->set('username', $username);
        }

        $this->db->where('id', $user_id);
        $this->db->update($this->table_name);

        $this->db->set('name', $name_fio);

        if ($phone)
        {   $this->db->set('phone', $phone);
        }

        if ($description)
        {   $this->db->set('description', $description);
        }

        if ($yurlico_id)
        {   $this->db->set('yurlico_id', $yurlico_id);
        }

        if ($organization_club_id)
        {   $this->db->set('organization_id', $organization_club_id);
        }

        $this->db->where('user_id', $user_id);
        $this->db->update($this->profile_table_name);
    }

	/**
	 * Get user record by login (username or email)
	 *
	 * @param	string
	 * @return	object
	 */
	function get_user_by_login($login)
	{
		$this->db->where('LOWER(username)=', strtolower($login));
		$this->db->or_where('LOWER(email)=', strtolower($login));

		$query = $this->db->get($this->table_name);
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}

	/**
	 * Get user record by username
	 *
	 * @param	string
	 * @return	object
	 */
	function get_user_by_username($username)
	{
		$this->db->where('LOWER(username)=', strtolower($username));

		$query = $this->db->get($this->table_name);
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}

	/**
	 * Get user record by email
	 *
	 * @param	string
	 * @return	object
	 */
	function get_user_by_email($email)
	{
		$this->db->where('LOWER(email)=', strtolower($email));

		$query = $this->db->get($this->table_name);
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}

	/**
	 * Get user record by email
	 *
	 * @param	string
	 * @return	object
	 */
	function get_user_by_email_full($email)
	{
        $db_prefix = $this->db->dbprefix;
	    
        $this->db->select('users.id, users.username, users.email, users.activated, users.banned, user_profiles.name, user_profiles.yurlico_id, user_profiles.organization_id, user_profiles.phone, user_profiles.description');
        $this->db->from($db_prefix.'users   users');
        $this->db->join($db_prefix.'user_profiles   user_profiles', 'user_profiles.user_id = users.id', 'left');
        $this->db->where('LOWER(users.email)=', strtolower($email));

        $query = $this->db->get();

//        log_message('error', "SQL = ".print_r($this->db->last_query(), true));

        if ($query->num_rows() <= 0)
        {   return NULL;
        }

        $rows = $query->result();
        return $rows[0];

	}

	/**
	 * Check if username available for registering
	 *
	 * @param	string
	 * @return	bool
	 */
	function is_username_available($username)
	{
		$this->db->select('1', FALSE);
		$this->db->where('LOWER(username)=', strtolower($username));

		$query = $this->db->get($this->table_name);
		return $query->num_rows() == 0;
	}

	/**
	 * Check if email available for registering
	 *
	 * @param	string
	 * @return	bool
	 */
	function is_email_available($email)
	{
		$this->db->select('1', FALSE);
		$this->db->where('LOWER(email)=', strtolower($email));
		$this->db->or_where('LOWER(new_email)=', strtolower($email));

		$query = $this->db->get($this->table_name);
		return $query->num_rows() == 0;
	}

	/**
	 * Create new user record
	 *
	 * @param	array
	 * @param	bool
	 * @return	array
	 */
	function create_user($data, $activated = TRUE)
	{
		$data['created'] = date('Y-m-d H:i:s');
		$data['activated'] = $activated ? 1 : 0;

		if ($this->db->insert($this->table_name, $data)) {
			$user_id = $this->db->insert_id();
			if ($activated)	$this->create_profile($user_id);
			return array('user_id' => $user_id);
		}
		return NULL;
	}

	/**
	 * Activate user if activation key is valid.
	 * Can be called for not activated users only.
	 *
	 * @param	int
	 * @param	string
	 * @param	bool
	 * @return	bool
	 */
	function activate_user($user_id, $activation_key, $activate_by_email)
	{
		$this->db->select('1', FALSE);
		$this->db->where('id', $user_id);
		if ($activate_by_email) {
			$this->db->where('new_email_key', $activation_key);
		} else {
			$this->db->where('new_password_key', $activation_key);
		}
		$this->db->where('activated', 0);
		$query = $this->db->get($this->table_name);

		if ($query->num_rows() == 1) {

			$this->db->set('activated', 1);
			$this->db->set('new_email_key', NULL);
			$this->db->where('id', $user_id);
			$this->db->update($this->table_name);

			$this->create_profile($user_id);
			return TRUE;
		}
		return FALSE;
	}

	/**
	 * Purge table of non-activated users
	 *
	 * @param	int
	 * @return	void
	 */
	function purge_na($expire_period = 172800)
	{
		$this->db->where('activated', 0);
		$this->db->where('UNIX_TIMESTAMP(created) <', time() - $expire_period);
		$this->db->delete($this->table_name);
	}

	/**
	 * Delete user record
	 *
	 * @param	int
	 * @return	bool
	 */
	function delete_user($user_id)
	{
		$this->db->where('id', $user_id);
		$this->db->delete($this->table_name);
		if ($this->db->affected_rows() > 0) {
			$this->delete_profile($user_id);
			return TRUE;
		}
		return FALSE;
	}

	/**
	 * Set new password key for user.
	 * This key can be used for authentication when resetting user's password.
	 *
	 * @param	int
	 * @param	string
	 * @return	bool
	 */
	function set_password_key($user_id, $new_pass_key)
	{
		$this->db->set('new_password_key', $new_pass_key);
		$this->db->set('new_password_requested', date('Y-m-d H:i:s'));
		$this->db->where('id', $user_id);

		$this->db->update($this->table_name);
		return $this->db->affected_rows() > 0;
	}

	/**
	 * Check if given password key is valid and user is authenticated.
	 *
	 * @param	int
	 * @param	string
	 * @param	int
	 * @return	void
	 */
	function can_reset_password($user_id, $new_pass_key, $expire_period = 900)
	{
		$this->db->select('1', FALSE);
		$this->db->where('id', $user_id);
		$this->db->where('new_password_key', $new_pass_key);
		$this->db->where('UNIX_TIMESTAMP(new_password_requested) >', time() - $expire_period);

		$query = $this->db->get($this->table_name);
		return $query->num_rows() == 1;
	}

	/**
	 * Change user password if password key is valid and user is authenticated.
	 *
	 * @param	int
	 * @param	string
	 * @param	string
	 * @param	int
	 * @return	bool
	 */
	function reset_password($user_id, $new_pass, $new_pass_key, $expire_period = 900)
	{
		$this->db->set('password', $new_pass);
		$this->db->set('new_password_key', NULL);
		$this->db->set('new_password_requested', NULL);
		$this->db->where('id', $user_id);
		$this->db->where('new_password_key', $new_pass_key);
		$this->db->where('UNIX_TIMESTAMP(new_password_requested) >=', time() - $expire_period);

		$this->db->update($this->table_name);
		return $this->db->affected_rows() > 0;
	}

	/**
	 * Change user password
	 *
	 * @param	int
	 * @param	string
	 * @return	bool
	 */
	function change_password($user_id, $new_pass)
	{
		$this->db->set('password', $new_pass);
		$this->db->where('id', $user_id);

		$this->db->update($this->table_name);
		return $this->db->affected_rows() > 0;
	}

	/**
	 * Set new email for user (may be activated or not).
	 * The new email cannot be used for login or notification before it is activated.
	 *
	 * @param	int
	 * @param	string
	 * @param	string
	 * @param	bool
	 * @return	bool
	 */
	function set_new_email($user_id, $new_email, $new_email_key, $activated)
	{
		$this->db->set($activated ? 'new_email' : 'email', $new_email);
		$this->db->set('new_email_key', $new_email_key);
		$this->db->where('id', $user_id);
		$this->db->where('activated', $activated ? 1 : 0);

		$this->db->update($this->table_name);
		return $this->db->affected_rows() > 0;
	}

	/**
	 * Activate new email (replace old email with new one) if activation key is valid.
	 *
	 * @param	int
	 * @param	string
	 * @return	bool
	 */
	function activate_new_email($user_id, $new_email_key)
	{
		$this->db->set('email', 'new_email', FALSE);
		$this->db->set('new_email', NULL);
		$this->db->set('new_email_key', NULL);
		$this->db->where('id', $user_id);
		$this->db->where('new_email_key', $new_email_key);

		$this->db->update($this->table_name);
		return $this->db->affected_rows() > 0;
	}

	/**
	 * Update user login info, such as IP-address or login time, and
	 * clear previously generated (but not activated) passwords.
	 *
	 * @param	int
	 * @param	bool
	 * @param	bool
	 * @return	void
	 */
	function update_login_info($user_id, $record_ip, $record_time)
	{
		$this->db->set('new_password_key', NULL);
		$this->db->set('new_password_requested', NULL);

		if ($record_ip)		$this->db->set('last_ip', $this->input->ip_address());
		if ($record_time)	$this->db->set('last_login', date('Y-m-d H:i:s'));

		$this->db->where('id', $user_id);
		$this->db->update($this->table_name);
	}

	/**
	 * Ban user
	 *
	 * @param	int
	 * @param	string
	 * @return	void
	 */
	function ban_user($user_id, $reason = NULL)
	{
		$this->db->where('id', $user_id);
		$this->db->update($this->table_name, array(
			'banned'		=> 1,
			'ban_reason'	=> $reason,
		));
	}

	/**
	 * Unban user
	 *
	 * @param	int
	 * @return	void
	 */
	function unban_user($user_id)
	{
		$this->db->where('id', $user_id);
		$this->db->update($this->table_name, array(
			'banned'		=> 0,
			'ban_reason'	=> NULL,
		));
	}

	function is_banned($user_id)
	{
	    $this->db->where('id', $user_id);
	    $this->db->where('banned', 1);

		// NOTE: is_active field is ignored.

		$query = $this->db->get($this->table_name);

		return ($query->num_rows() == 1);
	}

    function get_user_roles($user_id)
    {
        $this->db->select('userId, groupId, name, group_type');
        $this->db->from('users_groups_link');
        $this->db->join('user_groups', 'users_groups_link.groupId = user_groups.id', 'left');
        $this->db->where('user_groups.group_type', Utilitar::GROUP_TYPE__ROLES);
        $this->db->where('userId', $user_id);

        $query = $this->db->get();

        if ($query->num_rows() <= 0)
        {   return array();
        }
        
        return $query->result();
    }

    function get_users_with_roles($role_name)
    {
/*
SELECT re_users.username, re_users_groups_link.userId
FROM re_users_groups_link
RIGHT JOIN re_users ON re_users.id = re_users_groups_link.userId

LEFT JOIN re_user_groups ON re_users_groups_link.groupId = re_user_groups.id
WHERE re_user_groups.group_type = 2
AND re_user_groups.name = 'manager'
*/
        $db_prefix = $this->db->dbprefix;                         
        $this->db->select("users.username, users_groups_link.userId");
        $this->db->from('users_groups_link');
        $this->db->join('users', 'users.id = users_groups_link.userId', 'right');
        $this->db->join('user_groups', 'users_groups_link.groupId = user_groups.id', 'left');

        $this->db->where('user_groups.group_type', Utilitar::GROUP_TYPE__ROLES);
        $this->db->where('user_groups.name', $role_name);

        $query = $this->db->get();

        if ($query->num_rows() <= 0)
        {   return array();
        }

        return $query->result();
    }

    function get_users_with_roles_full($role_name)
    {
/*
SELECT re_users.username, re_users.email, re_users_groups_link.userId, re_user_profiles.phone, re_user_profiles.phone2, re_user_profiles.phone_extension
FROM re_users_groups_link
RIGHT JOIN re_users ON re_users.id = re_users_groups_link.userId

LEFT JOIN re_user_profiles ON re_users.id = re_user_profiles.user_id

LEFT JOIN re_user_groups ON re_users_groups_link.groupId = re_user_groups.id
WHERE re_user_groups.group_type = 2
AND re_user_groups.name = 'manager'
*/
        $db_prefix = $this->db->dbprefix;

        $this->db->select("users.id, users.username, users.email, users_groups_link.userId, user_profiles.name, user_profiles.phone, user_profiles.yurlico_id, user_profiles.organization_id");

        $this->db->from('users_groups_link');
        $this->db->join('users', 'users.id = users_groups_link.userId', 'right');
        $this->db->join('user_profiles', 'users.id = user_profiles.user_id', 'left');
        $this->db->join('user_groups', 'users_groups_link.groupId = user_groups.id', 'left');

        $this->db->where('user_groups.group_type', Utilitar::GROUP_TYPE__ROLES);
        $this->db->where('user_groups.name', $role_name);
        $this->db->order_by('name ASC');

        $query = $this->db->get();

        if ($query->num_rows() <= 0)
        {   return array();
        }

        return $query->result();
    }

    function user_has_role($user_id, $role_name)
    {
        if ($user_id <= 0)
        {   return false;
        }

        $this->db->select('userId, groupId, name, group_type');
        $this->db->join('user_groups', 'users_groups_link.groupId = user_groups.id', 'left');
        $this->db->where('user_groups.name', $role_name);
        $this->db->where('userId', $user_id);

//        log_message('error', "CASE3: SQL = ".$this->db->get_compiled_select('users_groups_link'));

        $query = $this->db->get('users_groups_link');

        return ($query->num_rows() > 0);
    }

    function user_email_has_role($user_email, $role_name)
    {
        if (0 == strlen($user_email))
        {   return false;
        }

/*SELECT wt_user_groups.name, wt_users.id as user_id
FROM wt_users
LEFT JOIN wt_users_groups_link ON wt_users_groups_link.userId = wt_users.id
LEFT JOIN wt_user_groups ON wt_users_groups_link.groupId = wt_user_groups.id
WHERE wt_user_groups.name = 'mobile_client'
AND wt_users.email='bebe@asd.com'*/

        $this->db->select('user_groups.name, users.id as user_id');
        $this->db->from('users');
        $this->db->join('users_groups_link', 'users_groups_link.userId = users.id', 'left');
        $this->db->join('user_groups', 'users_groups_link.groupId = user_groups.id', 'left');

        $this->db->where('user_groups.name', $role_name);
        $this->db->where('users.email', $user_email);

        $query = $this->db->get();

        return ($query->num_rows() > 0);
    }

    function get_role($role_name)
    {
        $this->db->where('group_type', Utilitar::GROUP_TYPE__ROLES);
        $this->db->where('name', $role_name);

        $query = $this->db->get('user_groups');
        if ($query->num_rows() <= 0)
        {
            return NULL;
        }

        $rows = $query->result();
        return $rows[0];
    }

    function delete_role($role_id)
    {
        $this->db->where('groupId', $role_id);
        $this->db->delete($this->user_groups_links_table_name);
    }


	/**
	 * Create an empty profile for a new user
	 *
	 * @param	int
	 * @return	bool
	 */
	private function create_profile($user_id)
	{
		$this->db->set('user_id', $user_id);
		return $this->db->insert($this->profile_table_name);
	}

	/**
	 * Delete user profile
	 *
	 * @param	int
	 * @return	void
	 */
	private function delete_profile($user_id)
	{
		$this->db->where('user_id', $user_id);
		$this->db->delete($this->profile_table_name);
	}
}

/* End of file users.php */
/* Location: ./application/models/auth/users.php */