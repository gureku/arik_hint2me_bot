<?php
defined('BASEPATH') OR exit('No direct script access allowed');


// Throwing this DISCARDS the data sent for a step by user, and abnormally terminates current step processing!
class StepBackException extends Exception {
    function __construct($message)
    {
        parent::__construct($message);
    }
}
