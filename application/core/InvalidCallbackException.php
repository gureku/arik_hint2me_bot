<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// This designates that the callback processing is invalid, most likely due to malformed callback parameters.
//
class InvalidCallbackException extends Exception {
    function __construct($message)
    {
        parent::__construct($message);
    }
}
