<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Factory
 *
 * @author RoundArh
 */
class Factory extends CI_Model
{
    //put your code here
    public function __construct()
    {   parent::__construct();
    }

    // ensures array item exists or returns NUL.
    public static final function safe_get(&$arr, $index)
    {
        $ci = &get_instance();
        $ci->load->helper('security');

        if (isset($arr[$index]))
        {   return $ci->security->xss_clean($arr[$index]);
        }

        return NULL;
    }
}
