<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// This DOESN'T TERMINATE CURRENT STEP's normal processing.
// Indeed it allows finishing the current step on its regular way, with the only difference: once finished, the step counter won't advance to the NEXT step.
//
// This is helpful when the step is not the final destination in a wizard rather than a "helper" step (like showing interim results in a read-only mode).
//
class StayOnSameStepException extends Exception {
    function __construct($message)
    {
        parent::__construct($message);
    }
}
