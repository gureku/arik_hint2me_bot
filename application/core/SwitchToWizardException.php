<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// as a parameter accepts the Wiard Class Name to switch to, with the step # to initialize.
class SwitchToWizardException extends Exception {
    private $desired_step_classname = NULL;
    private $metadata = NULL;


    // c'tor supports passing in not only the step class sequential index (which could change easily!) but the desired classname instead: trackable via refactorings!!!
    //  NOTE: we optionally can supply also the metadata require to the class to start acting immediately!
    function __construct($wizard_class_name, $wizard_step_number, $step_metadata = NULL)
    {
        $this->metadata = $step_metadata;

        if (is_string($wizard_step_number))
        {   $this->desired_step_classname = $wizard_step_number; // in this case it is string, not a number!
            parent::__construct($wizard_class_name);
            return;
        }

        parent::__construct($wizard_class_name, $wizard_step_number);
    }

    public function get_desired_step_classname()
    {
        return $this->desired_step_classname;
    }

    public function get_metadata()
    {
        return $this->metadata;
    }

    // call getMessage() to get WizardClassName.
    // call getCode() to get Wizard step number for activation.
}
