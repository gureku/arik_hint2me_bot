<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>

<link rel="stylesheet" href="https://unpkg.com/purecss@0.6.2/build/pure-min.css" integrity="sha384-UQiGfs9ICog+LwheBSRCt1o5cbyKIHbwjWscjemyBMT9YCUMZffs6UqUTd0hObXD" crossorigin="anonymous">

<html lang="en">
<head>
	<meta charset="utf-8">

    <style type="text/css">
    .pure-table th{
        vertical-align:middle;
        font-size:1.2em;
    }
    .comments{
        color:#6e0000;
    }

    .topalign
    { vertical-align:top;
    }

    table {
        border: 1px solid lightgray;
    }
    </style>
    
	<title>Список рекомендаций | Мобис</title>
</head>

<body>

<?php
    $ci = &get_instance();
    $ci->load->model('util_mobis');
    $ci->load->model('utilitar');

    $types_from_db = $ci->util_mobis->get_recomm_root_categories();
    $recomm_types  = $ci->utilitar->wrapToTypesArray($types_from_db);
    log_message('error', "QQQ recomm_types=".print_r($recomm_types, true));
    $ctr = 1;
?>
<div style="margin: 20px 40px; float:left;">
    <h1>Список рекомендаций:</h1>
    <table class="pure-table pure-table-horizontal">
        <thead>
            <tr>
                <th>#</th>
                <th>Тип рекомендации</th>
                <th>Подкатегория</th>
                <th>Название</th>
                <th>Содержание</th>
                <th><b>Комментарии специалиста</b></th>
            </tr>
        </thead>

        <tbody>
            <?php foreach ($case_contents as $case):?>
                <tr class="topalign">
                    <td><b><?=$ctr++?>.</b></td>
                    <td style="font-weight:bold;"><?=(isset($recomm_types[$case->parent_category_id])  ? $recomm_types[$case->parent_category_id] : 'undef' )?></td>
                    <td><?=$case->recomm_type_name?></td>
                    <td><?=$case->r_title?></td>
                    <td><?=$case->r_body?></td>
                    <td class="comments"><?=$case->comments?></td>
                </tr>
            <?php endforeach?>
        </tbody>
    </table>
</div>

</body>
</html>
