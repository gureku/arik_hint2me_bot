<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<link href='<?=base_url()?>ftp/css/admin/dropdown_one.css' rel='stylesheet' type='text/css'>

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<?php
    header( 'Expires: Sat, 26 Jul 1997 05:00:00 GMT' );
    header( 'Last-Modified: ' . gmdate( 'D, d M Y H:i:s' ) . ' GMT' );
    header( 'Cache-Control: no-store, no-cache, must-revalidate, max-age=0, private' );
    header( 'Cache-Control: post-check=0, pre-check=0', false );
    header( 'Pragma: no-cache' );
?>

<title><?=$title?></title>
<link href="<?=base_url()?>ftp/css/admin/stylesheet.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>ftp/css/admin/style.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
//<![CDATA[
base_url = '<?php echo base_url();?>index.php/';
//]]>
</script>
<?php
if (isset($extraHeadContent)) {
	echo $extraHeadContent;
}
?>
</head>
<body <?php if (isset($onload_handler)) print "onload='$onload_handler();'"; ?>>

<?php
    if (!isset($pagename))
    {   $pagename = 'undefined';
    };
?>

<ul id="menu">
    <li class="noborder"><a class="logo" href="https://walltouch.ru/" title="веб-сайт WallTouch" target=_blank></a></li>

    <li class="<?php if ('RecommLib' == $pagename) echo 'current'; ?>">
        <a href="<?=site_url('admin/recommLib');?>">Список рекомендаций</a>
    </li>

    <li class="<?php if ('Albums' == $pagename) echo 'current'; ?>">
        <a href="<?=site_url('admin/albums');?>">Альбомы</a>
    </li>

    <li class="<?php if ('Docs' == $pagename) echo 'current'; ?>">
        <a href="<?=site_url('admin/docs');?>">Загрузка документов</a>
    </li>

    <li class="<?php if ('Tm' == $pagename) echo 'current'; ?>">
        <a href="<?=site_url('admin/tm');?>">Управление доступом специалистов</a>
    </li>

    <li><a href="<?=site_url('auth/logout/');?>" style="background-color:#ffcccc;">Выйти</a></li>
</ul>

<div id="mainbody">