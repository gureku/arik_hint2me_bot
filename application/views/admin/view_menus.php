<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<style type="text/css">
.recomms ul{
  margin-bottom:20px;
  overflow:hidden;
}
.recomms li{
  line-height:1.5em;
  padding-bottom:20px;
}
.mr20
{margin-right:20px;}

.wrapspan {
    display:block;
    width:320px;
    word-wrap:break-word;
}
</style>
<h2><?=$title?></h2>

<?php $this->load->view('admin/view_ajax_script'); ?>

<div>
<a href="<?=$actionUri?>">[<?=$actionTitle?>] <img src='<?=base_url()?>images/admin/new.gif'></a>
</div>
<br><br>

<ul class="recomms">
<?php foreach ($recommendations_split as $root_category => $ajax_items):?>
<li><h2><?=$root_category?></h2>
    <?php foreach ($ajax_items as $item):?>
        <p>
            <span class="mr20 wrapspan_DISABLED"><?=$item['ajaxString']?></span><span class="mr20"><?=$item['editString']?></span><span><?=$item['deleteString']?><span>
        </p>
    <?php endforeach?>
</li>
<?php endforeach?>
</ul>

<br>
<br>
<?php
    $data['ajaxItemsTitle'] = '';
    $this->load->view('admin/view_ajax_dataContainer', $data);
?>

<br />