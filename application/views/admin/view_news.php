<link href="<?=base_url()?>css/calendar/cal.css" rel="stylesheet" type="text/css" />
<h2><?=$title?></h2>

<?php $this->load->view('admin/view_ajax_script'); ?>

<div>
<a href="<?=$actionUri?>">[<?=$actionTitle?>] <img src='<?=base_url()?>images/admin/new.gif'></a>
</div><br /><br />

<h3><?php if (count($compoundNews) > 0) {echo $section1Name;}?>: </h3>

<table class="plainlist">
<?php $clubs = $this->util_trainings->getClubs();
    $club_shortRefs = array();
foreach ($clubs as $club)
{   $club_shortRefs[$club->id] = substr($club->name, 0, 2);
}
?>
<?php foreach ($compoundNews as $data):?>
    <tr>
        <td><?=$data['title']?></td>
        <td>
        <?php
            if (isset($data['clubs'][0]))
            {   echo '['.$club_shortRefs[$data['clubs'][0]].']';
            }
        ?></td>
        <td>
            <?=$data['ajaxString']?>
        </td>
        <td><?=$data['editString']?></td>
        <td><?=$data['deleteString']?></td>
    </tr>
<?php endforeach ?>
</table>


<br />
<br />
<br />
<?php
    $data['ajaxItemsTitle'] = '';
    $this->load->view('admin/view_ajax_dataContainer', $data);
?>

<br />