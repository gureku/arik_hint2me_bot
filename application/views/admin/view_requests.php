<?php
$data['bMCEreadonly'] = TRUE;
$this->load->view('admin/view_tinyMCE_script', $data);
?>

<h2><?=$title?></h2>
<br />

<div class="categorymap">
    <div align='left' style="margin-left: 30px;">
    <?php
        $data = array();
        $data['title'] = '1) Новые (требуют активации со стороны Юрлица) запросы на подключение';
        $data['requests'] = $incoming_requests;
        $this->load->view('admin/view_requestsList', $data);
    ?>
    <hr style="width:30%; margin-top:20px;"/>
    <?php
        $data = array();
        $data['title'] = '<span style="color:blue">2) Требуют одобрения Админа (подключения активированы Юрлицом)</span>';
        $data['requests'] = $customer_activated_requests;
        $this->load->view('admin/view_requestsList', $data);
    ?>
    <hr style="width:30%; margin-top:20px;"/>
    <?php
        $data = array();
        $data['title'] = '<span style="color:green">3.1) Одобренные Админом запросы</span>';
        $data['requests'] = $approved_requests;
        $data['read_only'] = TRUE;
        $this->load->view('admin/view_requestsList', $data);
    ?>
    <hr style="width:30%; margin-top:20px;"/>
    <?php
        $data = array();
        $data['title'] = '<span style="color:red">3.2) Отклонённые Админом запросы</span>';
        $data['requests'] = $denied_requests;
        $this->load->view('admin/view_requestsList', $data);
    ?>
    </div>
</div>
