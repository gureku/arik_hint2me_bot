<link href="<?=base_url()?>css/calendar/cal.css" rel="stylesheet" type="text/css" />

<?php $this->load->view('admin/view_tinyMCE_script')?>

<script type="text/javascript" src="<?=base_url()?>ftp/js/friendlyUrl/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>ftp/js/friendlyUrl/jquery.friendurl.js"></script>

<script type="text/javascript">
$(function(){
  $('#thetitle').focus();
});
</script>


<div align="left" style="font-size:1.4em;">
<form name="myform" method=post action="<?=base_url()?>admin/<?=$actionUri?>">
<input type='hidden' name='item_id' value='<?=$row_id?>'>
<table>
    <tr>
        <td><label for="thetitle">Название рекомендации:</label><br>
            <input type=text style="font-size:1.1em;width:300px;" id=thetitle name=thetitle value='<?=$title?>'>
            </td>
        <td rowspan=2 style="padding-left: 30px;">
            <label for="thetitle">Содержание рекомендации:</label>
            <?=form_textarea('thebody', $body)?>
        </td>
    </tr>
    <tr style="vertical-align: top;">
        <td style="padding-top: 12px;"><label for="thecategory">Укажите категорию:</label><br>
            <select size="16" id="thecategory" name="thecategory" style="font-size:1.1em; width:305px;">
                <?=$sectioned_listbox?>
            </select>
        </td>
    </tr>
    <tr>
        <td colspan=2 style="text-align: right;">
            <input type=submit value='Сохранить'>
            <input type=reset value='Отменить' onclick='javascript:document.location.href="<?=$backUrl?>"' >
        </td>
    </tr>
</table>
</form>

</div>