<script type="text/javascript">
function assignPhoto(owner, photoId)
{
    <?php /* NOTE: $actionUrl must already contain the owner_row_id in a path! */ ?>
    var url= '<?=$actionUrl?>';
        $.post(url, {
            'photoId': photoId
        }, function(obj){
            if(obj['error'])
            {   alert('ERROR: ' + obj['error']);
            }
            else if (obj['success'])
            {   //alert(obj['success']);
                $("#photochooser td.highlighted").removeClass('highlighted');
                $(owner).parent().addClass('highlighted'); // assumed that parent is TD element.
                $(owner).blur();
            }

        }, "json");
}
</script>

<div align="left">
<br>
<?php if(strlen($albumDescription) > 0):?>
<strong>Об альбоме:</strong> <?=$albumDescription?>
<?php endif?>
</div>
<table id='photochooser' style='margin: 0px auto;' cellspacing="10px" cellpadding="5px">
<?php if (0 == count($photos)):?>
    <?php lang('admin.album.nophotos')?>
<?php endif?>
(<a href="<?=base_url()?>admin/mnt">выбрать альбом...</a>)

<?php
    $bReadOnly = (isset($read_only) && $read_only);
    $cols = 4;
    $i = 1;
    foreach ($photos as $photo)
    {
        if ($i > $cols)
        {   print '<tr>';
        }


        //-------------------------------------------------------------------------------------+
        //  the row:
        print ($photo->id == $current_photo_id) ? '<td class="highlighted">' : '<td>';

        printf("<a href='%s' target='_top'>%s <img src='%s'></a>",
            base_url().$photo->photo_path,
            $photo->title,
            base_url().$photo->thumbnail_path
        );

        // either choose for product or delete: don't show them at the same time - for usability:
//        if (isset($photo))
        {
            printf("<br>&nbsp;&nbsp;&nbsp;&nbsp;<a href='#' onclick='assignPhoto(this, %d); return false;' >%s <img src='%s'></a>",
                    $photo->id,
                    lang('admin.photo.assign'),
                    base_url().'images/admin/props.gif');
        }

        print '</td>';
        //-------------------------------------------------------------------------------------|

        if (++$i > $cols)
        {
            print '</tr>';
            $i = 1;
        }
    }
?>
</table>