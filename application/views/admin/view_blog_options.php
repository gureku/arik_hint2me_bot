<link href="<?=base_url()?>css/calendar/cal.css" rel="stylesheet" type="text/css" />
<h2><?=$title?></h2>

<div>
</div><br><br>

<div style="margin-left: 80px;">
    <form method="post" action='<?=base_url()?>admin/blog_contents/doSaveOptions' >
    <table>
        <tr>
            <td colspan=3 align=left><strong>General:</strong>
            </td>
        </tr>
        <tr>
            <td>
            <p><input type="checkbox" id="check_captcha" name="check_captcha" <?php if (1 == $check_captcha):?>checked<?php endif?> > <label for="check_captcha"></label></p>
            </td>
            <td colspan=2 align=left><label for="check_captcha">check CAPTCHA</label></td>
        </tr>

        <tr>
            <td>
            <p><input type="checkbox" id="check_ip_flood" name="check_ip_flood" <?php if (1 == $check_ip_flood):?>checked<?php endif?> > <label for="check_ip_flood"></label></p>
            </td>
            <td colspan=2 align=left><label for="check_ip_flood">IP flood check</label></td>
        </tr>


        <tr>
            <td colspan=3>&nbsp;
            </td>
        </tr>
         <tr>
            <td colspan=3 align=left><strong>Commenting:</strong>
            </td>
        </tr>
        <tr>
            <td colspan=3 align=left>
            <?php
                    $data['comboboxName']   = 'commenting_level';
                    $data['items']          = $commenting_levels;
                    $data['selectedItemId'] = $commenting_levels_value;
                    $data['defaultString']  = NULL;
                    $data['attribs']        = 'size="4"';
                    $this->load->view('admin/view_combobox', $data);
            ?>
            </td>            
        </tr>

        <tr>
            <td colspan=3>&nbsp;
            </td>
        </tr>

        <tr>
            <td colspan=3 align=left><strong>Mirroring posts to blogs:</strong>
            </td>
        </tr>
        <tr>
            <td>
            <p><input type="checkbox" id="post_to_twitter" name="post_to_twitter" <?php if (1 == $post_to_twitter):?>checked<?php endif?> > <label for="post_to_twitter"></label></p>
            </td>
            <td align=left>
                <label for="post_to_twitter">Twitter</label>
            </td>
            <td align=left>
                <a href="blogging_credentials/1">set credentials...</a>
            </td>
        </tr>
        <tr>
            <td>
            <p><input type="checkbox" id="post_to_lj" name="post_to_lj" <?php if (1 == $post_to_lj):?>checked<?php endif?> > <label for="post_to_lj"></label></p>
            </td>
            <td align=left>
                <label for="post_to_lj">LiveJournal</label>
            </td>
            <td align=left>
                <a href="blogging_credentials/2">set credentials...</a>
            </td>
        </tr>

        <tr>
            <td colspan=3>&nbsp;
            </td>
        </tr>
    </table>
<input type=submit value='<?=lang('button.save')?>'>
                <input type=reset value='<?=lang('button.cancel')?>' onclick='javascript:document.location.href="<?=base_url()?>admin/blog_contents/options"' >
    </form>
</div>