<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
  </div>
</div>
<div id="footer">
<br />
  Copyright &copy; 2016-<?=date('Y')?> by <a href="mailto:info@walltouch.ru">WallTouch</a>
</div>
</body>
</html>