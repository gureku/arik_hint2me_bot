<?php
$this->load->view("admin/header");
?>

<link href="<?=base_url()?>css/calendar/cal.css" rel="stylesheet" type="text/css" />

<h2><?=$title;?></h2>
<br />
<?php $this->load->view('admin/view_ajax_script');?>


<script type="text/javascript">
function myFunc(queryUrl)
{
    var url = "<?=base_url()?>" + queryUrl;
    var the_div = $('my_div');

    var the_div_goods = $('my_divGood');
    the_div_goods.update('');

    new Ajax.Request(url, {
        method: 'get',
        onCreate: function() {
            the_div.update('Loading....');
        },
        onSuccess: function(transport) {
            the_div.update(transport.responseText);
        }
    });

}

</script>
<br /><br />
<font color=red size='2'><?=lang('admin.no.user.duplications')?></font>
<br /><br /><br />
<form name='form1' action="<?=base_url()?>admin/groups/<?=$actionType?>" method=post enctype="multipart/form-data">
<input type='hidden' name='pId' value='<?=$userId?>'>

    <div align='left' style="margin-left: 30px;">
    <table>
        <tr><td><?=lang('admin.name_fio')?>:</td><td><input type=text style='width:300px;' name=name_fio value='<?=$name_fio?>'></td></tr>
        <tr><td>E-mail:</td><td><input type=text style='width:300px;' name=email value='<?=$email?>'></td></tr>
        <tr><td><?=lang('admin.phone')?>:</td><td><input type=text style='width:300px;' name=phone value='<?=$phone?>'></td></tr>
        <tr>
            <td><?=lang('admin.userstatus')?>:</td>
            <td>
                <?php
                    $data['comboboxName'] = 'is_active';
                    $data['items'] = $activity_types;
                    $data['selectedItemId'] = $p_type_value;
                    $data['defaultString'] = NULL;
                    $this->load->view('admin/view_combobox', $data);
                ?>
            </td>
        </tr>

        <tr><td><?=lang('admin.comments')?>:</td><td><textarea rows="6" cols="60" name="description"><?=$description?></textarea></td></tr>

        <?php if ($userId > 0):?>
            <tr><td><br></td><td><br><a href="<?=base_url()?>admin/groups/editPassword/<?=$userId?>"><?=lang('admin.changepass')?></a></td></tr>
        <?php else:?>
            <tr><td><?=lang('admin.pass')?>:</td><td><input type=password style='width:300px;' name=password1 value=''></td></tr>
            <tr><td><?=lang('admin.pass.retype')?>:</td><td><input type=password style='width:300px;' name=password2 value=''></td></tr>
        <?php endif?>
        <tr><td> </td>
        <td align='right'><input type=submit value='<?=lang('button.save')?>'>
        <input type=reset value='<?=lang('button.cancel')?>' onclick='javascript:document.location.href="<?=base_url()?>admin/groups/users"' >

        </td></tr>
    </table>
    </div>
</form>

<p>
<div id="my_div" name="my_div">
</div>

<p>
<div id="my_divGood" name="my_divGood">
</div>

<br /><br />
<?php
$this->load->view("admin/footer");
?>