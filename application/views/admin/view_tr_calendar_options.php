<link href="<?=base_url()?>css/calendar/cal.css" rel="stylesheet" type="text/css" />
<h2><?=$title?></h2>

<div>
</div><br><br>

<div style="margin-left: 80px;">
    <form method="post" action='<?=base_url()?>admin/trainings_schedule/doSaveOptions' >
    <table>
        <tr>
            <td colspan=3 align=left><strong>Показывать:</strong>
            </td>
        </tr>

        <?php
        $weekday_names = array( 1 => 'Понедельник',
                                2 => 'Вторник',
                                3 => 'Среду',
                                4 => 'Четверг',
                                5 => 'Пятницу',
                                6 => 'Субботу',
                                7 => 'Воскресенье');
        ?>

        <?php foreach ($options['weekdays'] as $day => $value):?>
        <tr>
            <td>
            <p><input type="checkbox" id="show_<?=$day?>" name="show_<?=$day?>" <?php if (1 == $value):?>checked<?php endif?> > <label for="show_<?=$day?>"></label></p>
            </td>
            <td colspan=2 align=left><label for="show_<?=$day?>"><?=$weekday_names[$day]?></label></td>
        </tr>
        <?php endforeach?>

        <tr>
            <td colspan=3>&nbsp;
            </td>
        </tr>

        <tr>
            <td>
            <p><input type="checkbox" id="check_separate_halls" name="check_separate_halls" <?php if (1 == $options['separate_halls']):?>checked<?php endif?> > <label for="check_separate_halls"></label></p>
            </td>
            <td colspan=2 align=left><label for="check_separate_halls">Раздельное расписание залов</label></td>
        </tr>

        <tr>
            <td colspan=3>&nbsp;
            </td>
        </tr>

        <?php if (isset($congratulations)):?>
        <tr>
            <td colspan=3><div style="padding-bottom: 20px; color:blue; font-size:large;"><?=$congratulations?></div>
            </td>
        </tr>
        <?php endif?>

    </table>
<input type=submit value='<?=lang('button.save')?>'>
                <input type=reset value='<?=lang('button.cancel')?>' onclick='javascript:document.location.href="<?=base_url()?>admin/trainings_schedule/options"' >
    </form>
</div>