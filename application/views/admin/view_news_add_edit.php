<link href="<?=base_url()?>css/calendar/cal.css" rel="stylesheet" type="text/css" />

<!-- for jQuery Datepicker -->
<link href="<?=base_url()?>ftp/jq/jquery-ui-1.8.4.custom.min.js" rel="stylesheet"/>

<script type="text/javascript" src="<?=base_url()?>ftp/jq/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>ftp/jq/jquery-ui-1.8.4.custom.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>ftp/jq/jquery.ui.datepicker-ru.js"></script>

<script type="text/javascript" language="javascript">
    $(document).ready(function(){
        $.datepicker.setDefaults($.datepicker.regional['ru']);

        $("#news_date").datepicker({
            showOn: 'both',
            buttonImage: '<?=base_url()?>js/calendar/calendar.gif',
            buttonImageOnly: true,
            showButtonPanel: true
        });
<?php
        /*$("#newsDate2").datepicker({
            showOn: 'both',
            buttonImage: '<?=base_url()?>ftp/images/jq/calendar.gif',
            buttonImageOnly: true
        });*/
?>
    });

    function openLink()
    {
        var link = $("#news_link").val();

        if (link.length > 0)
        {   if ((-1 == link.indexOf('http://')) && (-1 == link.indexOf('https://')))
            {
                link = 'http://' + link;
            }
            window.open(link);
        }
    }
</script>
<!-- // for jQuery Datepicker -->

<?php
if ($withRichEdit != 0)
    {   $this->load->view('admin/view_tinyMCE_script');
    }
?>
<style type="text/css">
.qq-uploader
{   text-align: left !important;
}
</style>

<h2><?=$title?></h2>

<?php if ($withRichEdit != 0):?>
<a href="<?=base_url()?>admin/news/<?=$newsId?>/0"><?=lang('admin.plainhtml')?></a>
<?php else:?>
<a href="<?=base_url()?>admin/news/<?=$newsId?>"><?=lang('admin.richedit')?></a>
<?php endif?>

<br><br>
<div align="left">
<form method=post action="<?=base_url()?>admin/news/doAddEdit">
<?=$hid_ownId?>
<input type=hidden name="recordId" value="<?=$recordId?>">
<br>


<div>
    <?=lang('const.news.date')?>: <input type="text" maxlength="10" size="10" id="news_date" name="news_date" value="<?=$news_date?>" />
</div>
<div>
    <?=lang('const.link')?>: <input type="text" maxlength="255" size="110" id="news_link" name="news_link" value="<?=$newsLink?>" />
    <a href='#' onclick='openLink(); return false;' title='View link in new window' class='linkimage'><img style='vertical-align:top; margin-top:2px;' src='<?=base_url()?>images/admin/eye.gif'></a>
</div>

<?=$UCI_radiobuttons?>

<?php if ($multilanguage):?>
    <div class="langbar gb">
        <label for="newsTitle"><?=lang('admin.menu.title')?>:</label><br>
        <input type=text style='width:300px;' id='newsTitle' name=newsTitle value='<?=$newsTitle?>'>
        <br><br>
        <?=form_textarea('newsBody', $body)?>
    </div>
    <div class="langbar ru">
        <label for="newsTitle_ru">Title:</label><br>
        <input type=text style='width:300px;' id=newsTitle_ru name=newsTitle_ru value='<?=$newsTitle_ru?>'>
        <br><br>
        <?=form_textarea('newsBody_ru', $body_ru)?>
    </div>
    <br>
    <br>
<?php else:?> <?php /* for single-language case: */?>
    <?php if (Reut::LANG_EN == Reut::LANG_DEFAULT):?>
    <div class="langbar gb">
        <label for="newsTitle"><?=lang('admin.menu.title')?>:</label><br>
        <input type=text style='width:300px;' id='newsTitle' name=newsTitle value='<?=$newsTitle?>'>
        <br><br>
        <?=form_textarea('newsBody', $body)?>
    </div>
    <?php else:?> <?php /* for russian language: */?>
    <div class="langbar ru">
        <label for="newsTitle_ru">Title:</label><br>
        <input type=text style='width:300px;' id=newsTitle_ru name=newsTitle_ru value='<?=$newsTitle_ru?>'>
        <br><br>
        <?=form_textarea('newsBody_ru', $body_ru)?>
    </div>
    <?php endif;?>
    <br>
    <br>
<?php endif;?>

<?=$uploader_component?>
<?=$photo_chooser?>

<br>
<p align="right">
<input type=submit value='<?=lang('button.save')?>'>
<input type=reset value='<?=lang('button.cancel')?>' onclick='javascript:document.location.href="<?=$backUrl?>"' >
</p>

</form>

</div>