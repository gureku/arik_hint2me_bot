var g_actions        = {
    jAction_1: 'url_1',
    jAction_2: 'url_2',
    jAction_3: 'url_3'
};

var g_grid_headers   = ['Код страны','Код региона', 'Город','Долгота','Широта','nbip'];

var g_gridmodel      =  [
    {
        name:'country_code',
        index:'country_code',
        width:80
    },
    {
        name:'region_code',
        index:'region_code',
        width:80
    },
    {
        name:'city',
        index:'city',
        width:120
    },
    {
        name:'latitude',
        index:'latitude',
        width:60,
        align: 'right'
    },
    {
        name:'longitude',
        index:'longitude',
        width:60,
        align: 'right'
    },
    {
        name:'nbip',
        index:'nbip',
        width:30
    }
];

