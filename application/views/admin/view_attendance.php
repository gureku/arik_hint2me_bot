<link href="<?=base_url()?>css/calendar/cal.css" rel="stylesheet" type="text/css" />

<!-- for jQuery Datepicker -->
<link href="<?=base_url()?>ftp/jq/jquery-ui-1.8.4.custom.min.js" rel="stylesheet"/>

<script type="text/javascript" src="<?=base_url()?>ftp/jq/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>ftp/jq/jquery-ui-1.8.4.custom.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>ftp/jq/jquery.ui.datepicker-ru.js"></script>
<script type="text/javascript" language="javascript">
    $(document).ready(function(){
        $.datepicker.setDefaults($.datepicker.regional['ru']);

        $("#date_from").datepicker({
            showOn: 'both',
            buttonImage: '<?=base_url()?>ftp/images/jq/calendar.gif',
            buttonImageOnly: true,
            showButtonPanel: true
        });

        $("#date_till").datepicker({
            showOn: 'both',
            buttonImage: '<?=base_url()?>ftp/images/jq/calendar.gif',
            buttonImageOnly: true,
            showButtonPanel: true
        });
    });
</script>
<!-- // for jQuery Datepicker -->


<script type="text/javascript">
function onSubmitClientsForm()
{
    var listbox = document.forms["form_by_clients"].elements["clientsList[]"];

    var idx = listbox.selectedIndex;
	if (idx < 0)
	{   alert('Please select a client first.');
	    return false;
	}

    document.forms["form_by_clients"].action += listbox.options[idx].value;
    return true;
}

function onSubmitGroupsForm()
{
    var listbox = document.forms["form_by_groups"].elements["groups[]"];

    var idx = listbox.selectedIndex;
	if (idx < 0)
	{   alert('Please select a group first.');
	    return false;
	}

	document.forms["form_by_groups"].action += listbox.options[idx].value;
    return true;
}
</script>

<h2><?=$title?></h2>

<br /><br />

<?php /* big DIV start:*/ ?>
<div align=left style="margin-left: 300px;">
<div align=left style='background-color:#ccffcc; width:50%; margin-bottom:20px; padding: 10px;'>
<p>By groups:</p>
    <form name='form_by_groups' action="<?=base_url()?>admin/attendance/byGroup" onSubmit="return onSubmitGroupsForm();" method=get enctype="multipart/form-data">
        <div align='left' style="margin-left: 30px;">
            <select id='groups' name='groups' size=5 style='width:250px;'>
            <?php foreach ($groups as $group): ?>
             <option value='<?=$group->id?>'><?=$group->name?>
            <?php endforeach ?>
            </select>

            <div align=left style="margin-left: 180px;">
            <input type=submit value='<?=lang('button.search')?>'>
            </div>
        </div>
    </form>
</div>


<div align=left style='background-color:#ffcccc; width:50%; margin-bottom:20px; padding: 10px;'>
<p>By clients:</p>
    <form name='form_by_clients' action="<?=base_url()?>admin/attendance/byClient/" onSubmit="return onSubmitClientsForm();" method=post enctype="multipart/form-data">
        <div align='left' style="margin-left: 30px;">
        <select id='clientsList[]' name='clientsList[]' size=5 style='width:250px;'>
            <?php foreach ($clients as $client): ?>
             <option value='<?=$client->id?>'><?=$client->name?>
            <?php endforeach ?>
            </select>

            <div align=left style="margin-left: 180px;">
            <input type=submit value='<?=lang('button.search')?>'>
            </div>
        </div>
    </form>
</div>

<div align=left style='background-color:#ccccff; width:50%; margin-bottom:20px; padding: 10px;'>
<p>By date:</p>
    <form name='form_by_dates' action="<?=base_url()?>admin/attendance/byDates" method=post enctype="multipart/form-data">
        <div align='left' style="margin-left: 30px;">
        <table>
            <tr>
                <td>date from:</td>
                <td><input type="text" maxlength="10" size="10" id="date_from" name="date_from" value="" />
                </td>
            </tr>
            <tr>
                <td>date till:</td>
                <td><input type="text" maxlength="10" size="10" id="date_till" name="date_till" value="" />
                </td>
            </tr>
            </table>

            <div align=left style="margin-left: 180px;">
            <input type=submit value='<?=lang('button.search')?>'>
            </div>
        </div>
    </form>
</div>

</div> <?php /* big DIV end.*/ ?>