<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<link href="<?=base_url()?>css/calendar/cal.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>js/calendar/ui_datepicker_custom.css" rel="stylesheet" type="text/css" />

<?php $this->load->view('admin/view_tinyMCE_script');?>

<!-- for jQuery Datepicker -->
<link href="<?=base_url()?>ftp/jq/jquery-ui-1.8.4.custom.min.js" rel="stylesheet"/>

<script type="text/javascript" src="<?=base_url()?>ftp/jq/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>ftp/jq/jquery-ui-1.8.4.custom.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>ftp/jq/jquery.ui.datepicker-ru.js"></script>
<script type="text/javascript" language="javascript">
    $(document).ready(function(){
        $.datepicker.setDefaults($.datepicker.regional['ru']);

        <?php foreach ($custom_controls as $key => $value):?>  <?php /* $value[1] contains the TYPE of a specially-tracked control */ ?>
            <?php if (  (count($value) > 1) && 'datetime_picker' == $value[1]):?>
            $("#<?=$key?>").datepicker({
                showOn: 'both',
                buttonImage: '<?=base_url()?>js/calendar/calendar.gif',
                buttonImageOnly: true,
                showButtonPanel: true
            });
            <?php elseif (  (count($value) > 1) && 'combobox_ajax_source' == $value[1]):?>
                $("#"+'<?=$key?>').change(function () {
                    var collectionId = '';
                    $("#<?=$key?> option:selected").each(function () {
                        collectionId = $(this).val();
                    });

                    var url = '<?=base_url()?>index.php/admin/genericTypes/getAjaxData/'+'<?=$typeName?>'+ '/' +collectionId;
                    var listbox = $(this); // remmeber to access it below, inside .get()
                    listbox.attr("disabled","true");
                    listbox.toggleClass("inProgress");

                    $.get(url, function(stickers)
                    {
                        var primary_data = stickers['primary'];
                        var HTML = "";
                        for(var key in primary_data)
                        {   HTML += "<option value='"+key+"'>" + primary_data[key]['value'] + "</option>";
                        }

                        $("#"+'<?=GenericTypes::CONTROL_PREFIX.$value[2]?>').html(HTML); // $value[2] should contain destination control ID to fill in.

                        // make a selection:
                        $('#'+'<?=GenericTypes::CONTROL_PREFIX.$value[2]?>' + ' option[value=' + '<?=$value[3]?>' + ']').attr('selected', 'selected');
                        //cleanSubItemDetailsHtml();
                        listbox.toggleClass("inProgress");
                        listbox.removeAttr("disabled");
                    }, 'json');
                })
                .change();
            <?php endif?>
        <?php endforeach?>
    });

</script>
<!-- // for jQuery Datepicker -->

<h2><?=$title?></h2>

<br><br>
<div align="left">
<form name='form1' id='form1' action="<?=base_url()?>index.php/admin/<?=$actionUri?>" method=post enctype="multipart/form-data">
    <input type=hidden name="recordId" value="<?=$recordId?>">
    <input type=hidden name="typeName" value="<?=$typeName?>">

        <?php /* now lets enumerate custom controls - to recognize then on a server - during POSTs: */ ?>
        <?php foreach ($custom_controls as $key => $value):?>
            <?php if ( (count($value) > 1) && ('hidden' == $value[1])):?>
            <?=$value[0]?>  <?php /* $value[1] contains the TYPE of a specially-tracked control */ ?>
            <?php endif?>
        <?php endforeach?>

    <div align='left' style="margin-left: 30px;">
    <table>
        <?php foreach ($custom_controls as $key => $value):?>
            <?php if (is_array($value) && (2 == count($value)) && ('hidden' == $value[1])):?>
                <?php /* do nothing: this is hidden control and has already been created above.*/?>
            <?php elseif (  (count($value) > 1) && 'combobox_ajax_source' == $value[1]):?>
                <tr>
                    <td><label for="<?=$key?>"><?=$custom_control_names[$key]?></label>:</td>
                    <?php /*DEBUG message:
                        if (is_array($value)) log_message('error', 'eeeeeeeeeeee custom_control: '.print_r($value, true)) */?>
                    <?php if (is_array($value)):?>
                        <td>
                            <?=$value[0]?>
                        </td>
                    <?php else:?>
                        <td>unknown value array passed!</td>
                    <?php endif?>
                </tr>
            <?php else:?>
                <tr>
                    <td><label for="<?=$key?>"><?=$custom_control_names[$key]?></label>:</td>
                    <?php /*DEBUG message:
                        if (is_array($value)) log_message('error', 'eeeeeeeeeeee custom_control: '.print_r($value, true)) */?>

                    <?php if (is_array($value)):?>
                        <td><?=$value[0]?>
                        </td>
                    <?php else:?>
                        <td><input type=text style='width:300px;' name=<?=$key?> id=<?=$key?> value='<?=$value?>'></td>
                    <?php endif?>
                </tr>
            <?php endif?>
        <?php endforeach?>

        <tr><td colspan=2>&nbsp;</td></tr>
        <tr><td> </td>
        <td align='right'><input type=submit value='<?=lang('button.save')?>'>
        <input type=reset value='<?=lang('button.cancel')?>' onclick='javascript:document.location.href="<?=$backUrl?>"' >

        <?php if (isset($extra_data)):?>
            <tr><td colspan=2>
            <?=$extra_data;?>
            </td></tr>
        <?php endif?>
        </td></tr>
    </table>
    </div>

</form>

</div>
