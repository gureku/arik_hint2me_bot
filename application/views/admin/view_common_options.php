<link href="<?=base_url()?>css/calendar/cal.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>js/calendar/ui_datepicker_custom.css" rel="stylesheet" type="text/css" />
<?php $this->load->view('admin/view_tinyMCE_script'); ?>

<h2><?=$title;?></h2>
<style type="text/css">
.warnMe
{
    color: red;
    font-size: 1.3em;
}

.customlist li{
   list-style:square;
    margin-left: 30px;
    padding-left: 15px;
}
</style>

<div align="left" id='internalContent'>
    <form name='myform' action="<?=base_url()?>admin/mnt/update" method=post enctype="multipart/form-data">
        <div align='left' style="margin-left: 30px;">
        <table>
            <tr>
                <td>Приветствие на Главной странице:</td>
                <td>
                    <textarea rows=3 cols=30 name=welcome_words wrap='virtual'><?=$welcome_words?></textarea>
                </td>
            </tr>
            
            <tr>
                <td>Альбом для Типов Групп:</td>
                <td>
                    <?php
                        $data['comboboxName'] = 'group_types_album';
                        $data['items'] = $albums;
                        $data['selectedItemId'] = $group_types_albumId;
//                         $data['defaultString'] = lang('const.choosealbum');
                        $this->load->view('admin/view_combobox', $data);
                    ?>
                </td>
            </tr>

            <tr>
                <td>Альбом для Новостей:</td>
                <td>
                    <?php
                        $data['comboboxName'] = 'news_album';
                        $data['items'] = $albums;
                        $data['selectedItemId'] = $news_albumId;
//                         $data['defaultString'] = lang('const.choosealbum');
                        $this->load->view('admin/view_combobox', $data);
                    ?>
                </td>
            </tr>

            <tr>
                <td>Альбом фотографий Педагогов:</td>
                <td>
                    <?php
                        $data['comboboxName'] = 'trainers_album';
                        $data['items'] = $albums;
                        $data['selectedItemId'] = $trainers_albumId;
//                         $data['defaultString'] = lang('const.choosealbum');
                        $this->load->view('admin/view_combobox', $data);
                    ?>
                </td>
            </tr>

            <tr><td colspan=2><br></td></tr>
            
            <tr>
                <td>Подключение к <strong>Trello</strong>:</td>
                <td>
                Trello <strong><?=($trello_connected ? '<font color=green>ПОДКЛЮЧЕН</font>' : '<font color=red>НЕ ПОДКЛЮЧЕН</font>')?></strong> (TODO-списков: <strong><?=count($trello_connected_TODO_lists)?></strong>)<br />
                Детали подключенных списков (в формате <strong>name (id), валидность, прямой email-адрес</strong>):
                <ul class='customlist'>
                    <?php foreach ($trello_connected_TODO_lists as $trello_list):?>
                    <li><?php echo 'Название: "'.$trello_list['name'].'" ('.$trello_list['list_id'].'), <b>'.($trello_list['is_valid'] ? '<font color=green>валидный</font>' : '<font color=red>НЕВАЛИДНЫЙ</font>').'</b>, '.$trello_list['board_email']?></li>
                    <?php endforeach?>
                </ul>
                </td>
            </tr>

            <tr><td colspan=2> </td></tr>
            <tr><td colspan=2>
            <?php if (isset($notification_message)) echo '<font color=#0283DD><h2>'.$notification_message.'</h2></font>';?>
            </td></tr>
            <tr><td><div style="padding-bottom:150px;"></div> </td>
            <td align='right'><input type=submit value='<?=lang('button.save')?>'>
            </td></tr>
        </table>
        </div>
    </form>
</div>