<style type="text/css">
.categorymap ul
{
 list-style-type: none;
}
.categorymap .finalli
{
    margin: 5px 0 10px 0;
}
.categorymap li
{
    margin-left: 5px;
}

.categorymap li .first
{
    border-top:1px dashed lightgray;
    padding: 2px 0;
}

.categorymap a
{
    margin: 2px 10px;
    background-color:lightgray;
    padding: 1px 4px;
    border: 1px outset gray;
}
.approve {color: darkgreen !important;}
.deny {color: red !important;}
</style>

<div class="categorymap">
 <div align='left' style="margin-left: 30px;">
 <h3><?=$title?>:</h3>
 <?php
     $img_props = base_url().'images/approve.png';
     $statusTexts = $this->util_registration->getRequestTextualStatuses();

     if (!isset($read_only))
     {   $read_only = false;
     }
 ?>
     <?php if (0 == count($requests)):?>
         <span style="font-style:italic;">&lt;отсутствуют&gt;</span>
     <?php else:?>
     <ol>
         <?php foreach($requests as $req):?>
             <li>
                 <ul>
                     <li class='first'>Организация: <strong>
                     <?php if ($req->yurlico_id > 0):?>
                        <a href="<?=base_url()?>admin/genericTypes/addOrEdit/berk_yurlica/<?=$req->yurlico_id?>"><?=$req->organization_name?></a>
                     <?php else:?>
                        <?=$req->organization_name?>
                     <?php endif?>
                     </strong>ИНН: <?=$req->inn?>; статус регистрации: <strong><?=$statusTexts[$req->status]?></strong></li>
                     <li>Запрос от: <?=date("d-M-Y, H:i:s", strtotime($req->date_requested))?>. Последнее изменение статуса: <?=date("d-M-Y, H:i:s", strtotime($req->date_status_change))?></li>
                 <?php if (!$read_only && (Util_registration::REQ_STATUS__CUSTOMER_ACTIVATED == $req->status)):?>
                     <li class="finalli">
                     <a class="approve" href="<?=base_url()?>admin/registration/approve_admin/<?=$req->activation_code?>">Подтвердить регистрацию</a>
                     <a class="deny" href="<?=base_url()?>admin/registration/deny_admin/<?=$req->activation_code?>">Запретить регистрацию</a>

<!--                 <a href="<?=base_url()?>admin/registration/notifyPendingActivation/<?=$req->activation_code?>">Выслать заново запрос о подтверждении</a>
-->
                     </li>
                 <?php endif?>
                 </ul>
             </li>
         <?php endforeach?>
     </ol>
     <?php endif?>
 </div>
</div>
