<link href="<?=base_url()?>css/calendar/cal.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>ftp/css/admin/nestedLi.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>ftp/css/admin/filterstyle.css" rel="stylesheet" type="text/css" />

<script src="<?=base_url()?>js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>ftp/js/filterApplication.js" type="text/javascript"></script>

<h2><?=$title?></h2>

<script type="text/javascript">
function roleAdd(roleId, userId)
{
    // todo: NYI
}

function roleRemove(roleId, userId, role_name, username)
{
if (false === confirm("Удалить роль '" + role_name +  "' у '"+ username+"'?"))
{
    return false;
}

    <?php /* NOTE: $actionUrl must already contain the owner_row_id in a path! */ ?>
    var url= '<?=base_url()?>admin/berk_accounts/removeUserRole';
        $.post(url, {
            'assignTo': -1,
            'itemIDs': userId,
            'removeFrom': roleId
        }, function(obj){
            location.reload();

        }, "json");

    return false;
}
</script>

<div id="pagewrap">
    <div id="search">
        <?php $this->load->view('admin/search_box', isset($searchBox_extra) ? array('searchBox_extra'=> $searchBox_extra) : null)?>
    </div>
    <div id="body">

<div align=left style="margin: 40px 0 20px 0">
<h3>1. Выберите Клуб для управления комнатами:</h3>
    <ul>
        <li><a href="<?=base_url()?>admin/berk_accounts/rooms_mapping"> [ все клубы ] </a></li>

        <?php foreach ($organizations as $organization):?>
        <li><a href="<?=base_url()?>admin/berk_accounts/rooms_mapping/<?=$organization->id?>">
            <?php if($organization_id == $organization->id):?>
                <p style="background-color:#ffff66; color: black; font-weight:bold;"><?=$organization->name?></p>
            <?php else:?>
                <?=$organization->name?>
            <?php endif?>
        </a></li>
        <?php endforeach?>
    </ul>

<?php if ($organization_id > 0):?>
    <br />

    <h3>2. Привязать КОМНАТЫ к организации "<?=$organization->smallname?>":</h3>
    <a class="button"  style="display:inline-block;" href="<?=base_url()?>admin/berk_accounts/addRoomsToOrganization/<?=$organization_id?>/0" title='Добавить комнаты, которые НЕ привязаны ни к одной организации.'>
            <strong>a) непривязанные</strong>
    </a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a class="button" style="display:inline-block;" href="<?=base_url()?>admin/berk_accounts/addRoomsToOrganization/<?=$organization_id?>/1" title='Добавить любые комнаты, в т.ч. и привязанные к какой-либо организации.'>
            <strong>b) любые</strong>
    </a>

    <br />
    <br />

    <table style="width: 90%; margin: 20px 0;" id="resultTable" class="listtable">
    <thead>
      <tr>
            <th>Название комнаты</th>
            <th>Клуб ("Огранизация")</th>
            <th>[control]</th>
      </tr>
    </thead>
    <tbody>
    <?php foreach ($rooms as $room):?>
        <tr>
            <td>
                <a href="<?=base_url()?>admin/genericTypes/addOrEdit/berk_rooms/<?=$room->id?>">
                <?=$room->title?>
                </a>
            </td>

            <td>
                <a href="<?=base_url()?>admin/genericTypes/show/berk_organizations/<?=$room->organization_id?>"><?=$room->organization_smallname?></a>
            </td>

            <td>
                <?php if ($room->organization_id > 0):?>
                <a href="<?=base_url()?>admin/berk_accounts/remove_room_from_organization/<?=$room->id?>/<?=$room->organization_id?>"
                        onclick="return confirm('Убрать комнату (\'<?=htmlentities($room->title)?>\') из клуба <?=htmlentities($room->organization_name)?>?')">убрать из клуба (организации)</a>
                <?php endif?>
            </td>
        </tr>
    <?php endforeach?>
    </tbody>
    </table>
<?php else:?>
<?php endif?>

</div>


    </div>
</div>

<br />