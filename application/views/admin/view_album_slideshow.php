<div align="left" style="width: 80%;font-size:1.3em;margin-bottom:20px;">
    Название: <strong><a style="font-size:1.1em; font-weight:normal;" href="<?=$link_edit?>"><?=$albumTitle?></a></strong>
</div>
<table style='margin: 0px auto;' cellspacing="10px" cellpadding="5px">
<?php if (0 == count($photos))
    print lang('admin.album.nophotos');
?>
<?php
    $bReadOnly = (isset($read_only) && $read_only);
    $cols = 4;
    $i = 1;
    foreach ($photos as $row)
    {
        if ($i > $cols)
        {   print '<tr>';
        }
        

        //-------------------------------------------------------------------------------------+
        //  the row:
        print '<td>';
        printf("<a href='%s' target='_blank'>%s <img width='130' src='%s'></a>",
            base_url().$row->photo_path,
            $row->title,
            base_url().$row->thumbnail_path
        );

        // either choose for product or delete: don't show them at the same time - for usability:
        if (isset($productId) && $productId > 0)
        {
            printf("<br>&nbsp;&nbsp;&nbsp;&nbsp;<a href='../goods/setProductImage/%d/%d'>%s <img src='%s'></a>",
                    $productId,
                    $row->id,
                    lang('admin.album.settoproduct'),
                    base_url().'images/props.gif');
        }
        else if (!$bReadOnly)
        {
            $ss = "Вы уверены, что хотите удалить \'".basename($row->photo_path)."\'?";
            echo "<br><a style='padding: 20px;' href='".base_url()."admin/albums/deletePhoto/$row->id' onclick=\"return confirm('".$ss."')\" title='Удалить изображение'><font color=red>удалить</font> <img src='".base_url().'images/admin/del.gif'."'></a>";
        }

        print '</td>';
        //-------------------------------------------------------------------------------------|

        if (++$i > $cols)
        {
            print '</tr>';
            $i = 1;
        }
    }
?>
</table>