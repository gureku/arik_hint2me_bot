<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<h2><?php print $title;?></h2>

<?php $this->load->view('admin/view_ajax_script'); ?>

<div>
<a href="<?=base_url()?>admin/groups/addEditUser">[<?=sprintf(lang('admin.tmpl.create.m'), lang('admin.category.user'))?>] <img src='<?=base_url()?>images/admin/new.gif'></a>
</div><br /><br />

<br />
<h3><?=lang('admin.users.registered')?>:</h3>
<br />
<ul>
<?php
    $admin_url = base_url().'admin';
    $img_delete = base_url().'images/admin/del.gif';
    foreach ($users as $user)
    {
        printf("<li>%s: <a href='$admin_url/groups/addEditUser/%d'><strong>%s</strong></a><br>", lang('admin.name_fio'), $user->id, $user->name);
//        printf("Account activated: %s<br />", $user->activated ? 'TRUE' : 'FALSE');
        printf("E-mail: <a href='$admin_url/groups/addEditUser/%d'><strong>%s</strong></a><br>", $user->id, $user->email);
        if ($user->phone)
        {   printf("%s: %s<br>", lang('admin.phone'), $user->phone);
        }
        printf("roles: <strong>$user->roles</strong><br>");
        //printf("%s: %s<br>", lang('admin.registered'), date("d M Y H:i:s", strtotime($user->created)));
        printf("<a href='".base_url()."admin/groups/doDeleteUser/%d'  onclick=\"return confirm('".lang('admin.confirm.userdelete')."')\"><font color=red>%s</font> <img src='$img_delete'></a></li><br>",  $user->id, lang('admin.delete'));
    }
?>
</ul>
