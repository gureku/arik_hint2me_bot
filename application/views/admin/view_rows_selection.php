<style type="text/css">
    .selectedRow td{
        background-color:#48507b !important;
        color:#fff !important;
    }
    .selectedRow a{
    color:#ff6666;
    }
</style>

<script type="text/javascript">
$(document).ready(function() {
    $('#resultTable tr')
    .filter(':has(:checkbox:checked)')
    .addClass('selectedRow')
    .end()

    .click(function(event)
    {
        increaseSelection = !$(this).hasClass('selectedRow');
        $(this).toggleClass('selectedRow');

        // notify subscribed on the event:
        var subscribers = $('.subscriber');
        if (subscribers)
        {   subscribers.trigger("notify.selectedCountChange", increaseSelection);
        }

        if (event.target.type !== 'checkbox')
        {
            $(':checkbox', this).attr('checked', function() {
                return !this.checked;
            });
        }
    });
});
</script>
