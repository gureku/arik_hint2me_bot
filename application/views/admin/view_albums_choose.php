<br>
<h3><?=$title?></h3>
<br>
<h4><?=lang('admin.menu.albums')?>:</h4>
<ul style="list-style-type: square;">
<?php
    $img_props = base_url().'images/props.gif';
    $view_contents = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='../albums/%s'>[ edit contents ]</a>";
    foreach ($albums as $album)
    {
        printf("<li><a title='Assign to estate' href='$method_name/%d/%d'>%s &nbsp;&nbsp;  %d image(s) <img src='$img_props'></a>$view_contents</li>",
        $estate_id,
        $album->id,
        $album->title,        
        $album->images_count,

        $album->id);
    }
?>
<br />
<li><?=lang('const.also.may')?> <a href="<?=base_url()?>admin/albums/add"><?=lang('const.create.album')?> <img src='<?=base_url()?>images/admin/new.gif'></a></li>
</ul>
<br /><br />

