<br>
<a href="<?=base_url()?>admin/genericTypes/addClientsToGroup/<?=$group_id?>" title='Add more clients to the group...'><strong>Add clients...</strong></a>
<br><br>

<?php if (count($rows)):?>
<div id="body">
    <table style="width: 100%" id="resultTable" class="listtable">
      <thead>
      <tr>
            <th>name</th>
            <th>age</th>
            <th>phone</th>
            <th>comments</th>
            <th>remove from group</th>
      </tr>
    </thead>
    <tbody>
    <?php foreach ($rows as $row):?>
        <tr>
            <td><a href="<?=base_url()?>admin/genericTypes/addOrEdit/clients/<?=$row->id?>"><?=$row->name?></a></td>
            <td><?=$row->birthdate?></td>
            <td><?=$row->phone?></td>
            <td><?=$row->comments?></td>
            <td><a href="<?=base_url()?>admin/genericTypes/remove_from_group/<?=$group_id?>/<?=$row->id?>" onclick="return confirm('Удалить <?=$row->name?> из группы?')">remove</a></td>
        </tr>
    <?php endforeach?>
    </tbody>
    </table>
</div>
<br>
<?php else: ?>
<strong>Клиентов в этой группе нету :)</strong>
<?php endif ?>