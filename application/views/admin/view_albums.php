<?php $this->load->view('admin/view_ajax_script'); ?>
<h2><?=$title?></h2>

<style type="text/css">
.padding10
{
    padding: 0 20px;
}
</style>

<div>
<a href="<?=base_url()?>admin/albums/add">[<?=sprintf(lang('admin.tmpl.create'), lang('admin.category.album'))?>] <img src='<?=base_url()?>images/admin/new.gif'></a>
</div><br /><br />

<h3><?=lang('admin.menu.albums')?>:</h3>

<table class="plainlist">
<?php foreach ($compoundData as $data):?>
    <tr>
        <td>
        <?php
        if (isset($data['title']))
            {   echo $data['title'];
            }
            else
            {   echo $data['ajaxString'];
            }
        ?></td>
        <td><?=$data['editString']?></td>
        <td><?=$data['deleteString']?></td>
    </tr>
<?php endforeach ?>
</table>
<br />
<br />
<br />

<?php
    $data['ajaxItemsTitle'] = '';
    $this->load->view('admin/view_ajax_dataContainer', $data);
?>

<br />
