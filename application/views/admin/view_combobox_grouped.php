<style type="text/css">

optgroup:before {
    padding-left: 14px;
    font-style:normal;
    background-color:#ffffcc;
    font-family:arial;
    font-size: 1.1em;
}

option
{
    color: black;
}

</style>

<?php if (isset($onchange_track)):?>
<script type="text/javascript">

<?php
    // For now write all possible JS-code below, in this file.
    // TODO: move to separate js-files and include them upon need only.
?>
function enableDisableControls()
{

    var lbTypes = document.forms["myform"].elements['<?=$comboboxName?>'];
    var idx = lbTypes.selectedIndex;
	{
	    document.forms["myform"].elements['newsBody'].disabled      = (idx > 0);
	    document.forms["myform"].elements['newsBody_ru'].disabled   = (idx > 0);
	    document.forms["myform"].elements['newsTitle'].disabled     = (idx > 0);
	    document.forms["myform"].elements['newsTitle_ru'].disabled  = (idx > 0);
	    document.forms["myform"].elements['newsPath'].disabled      = (idx > 0);
    }
}
</script>
<?php endif;?>

<select style='width:200px;'
<?php
    if (isset($attribs))
    {   echo $attribs;    
    }
?>
name='<?=$comboboxName?>'
id='<?=$comboboxName?>'
    <?php if (isset($onchange_track)):?>
    onchange="<?=$onchange_track?>"
    <?php endif;?>
>
<?php
    if (isset($defaultString) && ('' != $defaultString))
    {
        //option for none selection:
        printf("<option value=-1>$defaultString</option>");
    }
?>
<?php
    $GROUPING_NONE = '**&722!**';               // the very starting value.
    $current_grouping_name = $GROUPING_NONE;    // initialize it.

    foreach ($items as $key => $value)
    {
        if ($value['grouping_name'])
        {
            if ($current_grouping_name != $value['grouping_name'])
            {
                if ($GROUPING_NONE !== $current_grouping_name)
                {   echo '</optgroup>';
                }

                echo ' <optgroup label="'.$value['grouping_name'].'">';
                $current_grouping_name = $value['grouping_name'];
            }
        }

        $is_selected_string = (isset($selectedItemId) && ((int)$key == $selectedItemId)) ? 'selected' : '';
        $option_prefix      = $value['grouping_name'] ? $current_grouping_name.': ' : '';
        printf("<option value='%d' %s>%s</option>", (int)$key, $is_selected_string, $option_prefix.$value['value']);

        echo '</optgroup>';
    }
?>
</select>