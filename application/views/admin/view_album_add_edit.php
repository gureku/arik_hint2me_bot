<link href="<?=base_url()?>css/calendar/cal.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>js/calendar/ui_datepicker_custom.css" rel="stylesheet" type="text/css" />

<!-- for jQuery Datepicker -->
<link href="<?=base_url()?>ftp/jq/jquery-ui-1.8.4.custom.min.js" rel="stylesheet"/>
<script type="text/javascript" src="<?=base_url()?>ftp/jq/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>ftp/jq/jquery-ui-1.8.4.custom.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>ftp/jq/jquery.ui.datepicker-ru.js"></script>
<script type="text/javascript" language="javascript">
    $(document).ready(function(){
        $.datepicker.setDefaults($.datepicker.regional['ru']);

        $("#date_created").datepicker({
            showOn: 'both',
            buttonImage: '<?=base_url()?>js/calendar/calendar.gif',
            buttonImageOnly: true,
            showButtonPanel: true
        });
    });

// this helps closing the calendar on "Today" button click:
$.datepicker._gotoToday = function(id) {
    $(id).datepicker('setDate', new Date()).datepicker('hide').blur();
};

</script>
<!-- // for jQuery Datepicker -->

<h2><?=$title?></h2>

<br><br>
<div align="left">
<form name='form1' action="<?=base_url()?>admin/albums/<?=$actionType?>" method=post enctype="multipart/form-data">
    <input type="hidden" name='albumId' value="<?=$albumId?>">

    <div align='left' style="margin-left: 30px;">
    <table>
        <tr><td><?=lang('const.news.date')?>:</td><td><input type="text" maxlength="10" size="10" id="date_created" name="date_created" value="<?=$date_created?>" /></td></tr>
        <tr><td><?=lang('const.name')?>:</td><td><input type=text style='width:300px;' name=title value='<?=$albumTitle?>'></td></tr>
        <tr>
            <td><label for="public_access"><?=lang('const.public_access')?></label>:</td><td><input type="checkbox" class="checked" id="public_access" name="public_access" <?php if (1 == $public_access):?>checked<?php endif?> >
            </td>
        </tr>
        <tr><td><?=lang('const.description')?>:</td><td><textarea cols='51' rows='2' name=description><?=$albumDescription?></textarea></td></tr>
        <?php if ('doAdd' != $actionType):?>
            <tr style="background-color: rgb(248, 248, 248);"><td><?=lang('admin.album.uploadphotos')?>:</td>
            <td>
                    <div class="highslide-body">
                        <?=$uploader_component?>
                    </div>
            </td>
            </tr>
        <?php endif;?>
        <tr><td colspan=2>&nbsp;</td></tr>
        <tr><td> </td>
        <td align='right'><input type=submit value='<?=lang('button.save')?>'>
        <input type=reset value='<?=lang('button.cancel')?>' onclick='javascript:document.location.href="<?=$backUrl?>"' >

        </td></tr>
    </table>
    </div>

</form>

</div>
<br>
<br>
<br>

<h4><?=lang('admin.album.photos')?>:</h4>
<?php
    $data['photos'] = $photos;
    $data['productId'] = $productId;
    $this->load->view('admin/view_album_slideshow', $data);
?>