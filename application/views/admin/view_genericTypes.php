<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<link href="<?=base_url()?>css/calendar/cal.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>js/calendar/ui_datepicker_custom.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>ftp/css/admin/filterstyle.css" rel="stylesheet" type="text/css" />

<script src="<?=base_url()?>js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>ftp/js/filterApplication.js" type="text/javascript"></script>

<h2><?=$title?></h2>

<div>
<a href="<?=$actionUri?>">[<?=$actionTitle?>] <img src='<?=base_url()?>images/admin/new.gif'></a>
</div><br /><br />

<div id="pagewrap">
    <div id="search">
        <?php $this->load->view('admin/search_box', isset($searchBox_extra) ? array('searchBox_extra'=> $searchBox_extra) : null)?>
    </div>
    <div id="body">
        <table style="width: 90%" id="resultTable" class="listtable">
          <thead>
          <tr>
            <?php foreach ($headers as $header):?>
                <th><?=$header?></th>
            <?php endforeach?>
          </tr>
        </thead>
        <tbody>
        <?php foreach ($rows as $row):?>
            <tr>                                                        <?php /* try to read NAME or atl east TITLE properties: */ ?>
                <td><a href="../addOrEdit/<?=$typeName?>/<?=$row->id?>"><?=(isset($row->name) ? $row->name : $row->title) ?></a></td>
                <?php
                $counter =0;
                foreach ($rowsNames as $rn):?>
                    <?php /* skipping the very first column: it already has been printed a few lines above!!! */ ?>
                    <?php if ((0 != $counter++) && (!in_array($rn, $rowsExclude))):?>
                    <td><?=$row->$rn?></td>
                    <?php endif ?>                
                <?php endforeach?>
                <td><a href="../delete/<?=$typeName?>/<?=$row->id?>" onclick="return confirm('Удалить <?=(isset($row->name) ? $row->name : $row->title)?>?')">delete</a></td>
            </tr>
        <?php endforeach?>
        </tbody>
        </table>

        <div id='pagenav'><strong>page navigation goes here</strong></div>        
    </div>
</div>

<br />