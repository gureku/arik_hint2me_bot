<link href="<?=base_url()?>css/calendar/cal.css" rel="stylesheet" type="text/css" />

<h2><?=$title?></h2>

<br><br>
<div id="body" align=left>
<form name='form1' action="<?=base_url()?>admin/berk_accounts/doAddRoomsToOrganization" method=post enctype="multipart/form-data">
    <input type=hidden name="recordId" value="<?=$recordId?>">

    <div align='center' style="margin-left: 30px;">
    <table>

        <tr><td colspan=2>
            <select id='roomsList[]' name='roomsList[]' multiple size=25 style='width:400px;'>
            <?php foreach ($non_member_rooms as $nm): ?>
             <option value='<?=$nm->id?>'><?=$nm->title?></option>
            <?php endforeach ?>
            </select>
        </td></tr>
        <tr><td> </td>
        <td align='right'><input type=submit value='<?=lang('button.save')?>'>
        <input type=reset value='<?=lang('button.cancel')?>' onclick='javascript:document.location.href="<?=$backUrl?>"' >
        </td>
        </tr>
    </table>
    </div>

</form>


</div>