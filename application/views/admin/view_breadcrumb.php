<?php
$ctr = count($breadCrumbs);
if (1 == $ctr)
{
    print $breadCrumbs[0][1]; // print just name (i.e. no link)
}
else
{
    $i = 0;
    foreach ($breadCrumbs as $row)
    {
        if ('' != $row[0]) // if link passed:
        {   print "<a href='$row[0]'>$row[1]</a>";
        }
        else
        {   print "$row[1]"; // print just name (i.e. no link)
        }

        if (++$i < $ctr)
        {
            print ' > ';
        }
    }
}
?>