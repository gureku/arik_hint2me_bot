<div align=center>
<link href="<?=base_url()?>ftp/css/admin/fileuploader.css" rel="stylesheet" type="text/css">
	<div id="file-uploader-demo1" title="Drag-and-drop is supported in FF, Chrome. Progress-bar is supported in FF3.6+, Chrome6+, Safari4+">
		<noscript>			
			<p>Please enable JavaScript to use file uploader.</p>
			<?php
			// or put a simple form for upload here
			?>
		</noscript>         
	</div>
    
    <script src="<?=base_url()?>ftp/js/fileuploader.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        var uploader = null;
        function createUploader(){            
            uploader = new qq.FileUploader({
                element: document.getElementById('file-uploader-demo1'),
                action: '<?=base_url()?>admin/uploadHandler/exec/<?=$dir_token?>',
                onComplete: onCompleteHandler
            });
        }

        function onCompleteHandler(id, fileName, responseJSON)
        {
            if (responseJSON['success'])
            {
                <?php if (isset($page_reload_on_success) && (true == $page_reload_on_success)):?>
                if (!uploader.isUploading())
                {   window.location.reload();
                }
                <?php endif?>
            }
            else
            {   alert(responseJSON['details']);
            }
        }

        <?php
        // in your app create uploader as soon as the DOM is ready
        // don't wait for the window to load
        ?>
        window.onload = createUploader;     
    </script>
</div>