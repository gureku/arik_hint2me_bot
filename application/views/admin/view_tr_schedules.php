<link href="<?=base_url()?>css/calendar/cal.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>ftp/css/admin/filterstyle.css" rel="stylesheet" type="text/css" />

<script src="<?=base_url()?>js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>ftp/js/filterApplication.js" type="text/javascript"></script>

<h2><?=$title?></h2>

<div>
<a href="<?=$actionUri?>">[<?=$actionTitle?>] <img src='<?=base_url()?>images/admin/new.gif'></a>
</div><br /><br />

<div id="pagewrap">
    <div id="search">
        <?php $this->load->view('admin/search_box', isset($searchBox_extra) ? array('searchBox_extra'=> $searchBox_extra) : null)?>
    </div>
    <div id="body">
        <table style="width: 90%" id="resultTable" class="listtable">
          <thead>
          <tr>
                <th>Группа</th>
                <th>Расписание</th>
                <th>Тренер</th>
                <th>[control]</th>
          </tr>
        </thead>
        <tbody>
        <?php foreach ($rows as $row):?>
            <tr>
                <td><a href="<?=base_url()?>admin/genericTypes/addOrEdit/training_groups/<?=$row->tr_group_id?>"><?=$row->group_name?></a></td>
                <td>
                    <a href="<?=base_url()?>admin/trainings_schedule/addOrEdit/<?=$row->tr_group_id?>">
                        <?=$this->util_trainings->visualizeShortSchedule($g_trainings, $row->tr_group_id);?>
                    </a>
                </td>
                <td><?=$row->trainer_name?></td>
                <td><a href="<?=base_url()?>admin/trainings_schedule/delete/<?=$row->tr_group_id?>" onclick="return confirm('Удалить расписание группы \'<?=$row->group_name?>\'?')">delete</a></td>
            </tr>
        <?php endforeach?>
        </tbody>
        </table>

        <br><br><br>
        <?=$schedules_render?>
    </div>
</div>

<br />