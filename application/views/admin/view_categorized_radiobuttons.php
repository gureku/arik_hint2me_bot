<div id='UCI_div' style='margin: 8px 0 10px 44px'>
<!-- this component requres jQuery -->

<link href="<?=base_url()?>css/radiobuttons.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript" language="javascript">
<?php if ($recordId > 0):?>
    $(function () {
        $("input[name='uci_radiobuttons']").change(function(){
                var item_id         = <?=$recordId?>;
                var item_type       = <?=$UCI_item_type?>;
                var category_type   = <?=$UCI_category_type?>;;
                var category_value  = $(this).val();

                $.post('<?=base_url()?>admin/categorizedItems/setCategoryRadiobuttons', {
                    'item_id': item_id,
                    'item_type': item_type,
                    'category_type': category_type,
                    'category_value': category_value
                }, function(obj){
                    if(obj['error'])
                    {   alert('ERROR: ' + obj['error']);
                    }
                    else
                    {
                    }

                }, "json");
        });
    });
<?php endif?>
</script>

<p>
    <input type="radio" id="radio_all"
        value="-1"
        name="uci_radiobuttons"
        <?php   $intersection = array_intersect($UCI_category_typeIDs, $UCI_row_own_categoryIDs);
                if (0 == count($intersection)) { echo 'checked'; }
        ?>
    />
    <label for="radio_all"><span></span>Все клубы</label>
</p>

<?php foreach ($UCI_category_typeIDs as $category_typeID):?>
<?php $branch = $this->util_trainings->getClub($category_typeID);?>
<p style='margin-left:10px;'>
    <input type="radio" id="<?=$branch->id?>" name="uci_radiobuttons" value="<?=$branch->id?>" <?php if (in_array($branch->id, $UCI_row_own_categoryIDs)) echo 'checked'; ?>/>
    <label for="<?=$branch->id?>"><span></span><?=$branch->name?></label>
</p>
<?php endforeach?>
</div>