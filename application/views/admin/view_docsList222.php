<h2><?=$title?></h2>
<br />
<div class="highslide-body">
    <div align="left" style="margin: 0 0 0 70px; width: 52%;">
        <h4><?=lang('admin.spacelimited')?></h4><br>
        <?=$uploader_component?>
    </div>
<br>
<br>
<br>
<?php
    $data = array();

    if (!isset($read_only))
    {   $read_only = true;
    }
    $data['documents']      = $documents;
    $data['relativePath']   = $relativePath;
    $data['read_only']      = $read_only;
    if (isset($more_action_title))
    {   $data['more_action_title']  = $more_action_title;
        $data['more_action_url']    = $more_action_url;
    }
    $this->load->view("admin/$lister_component", $data);
?>
<br/><br/>
</div>
