<link href="<?=base_url()?>css/calendar/cal.css" rel="stylesheet" type="text/css" />
<script src="<?=base_url()?>js/jquery-1.4.2.min.js" type="text/javascript"></script>

<?php /* see http://abeautifulsite.net/blog/2008/09/jquery-context-menu-plugin/ */ ?>

<script type="text/javascript">
<?php /* clicking twice with a same status invokes the *clear visit* operation */ ?>
function cSwap(cell, clientId, targetDate){
    var currentOption = ($("#switchLeftClick").val());
    var status = -1;
    var newClassName = '';
    var group_id = ($("#groupId").val());

    if ('VISIT' == currentOption)
    {   newClassName = 'markVisited';
        status = 1;
    } else if ('ABSENCE' == currentOption)
    {   newClassName = 'markAbsence';
        status = 2;
    } else if ('CLEAR visit' ==currentOption)
    {   newClassName = 'markClearVisits';
        status = 3;
    } else
    {   alert ('unsupported option!');
        return;
    }

    // if clicked the same operation and the status was not *clear* then clear it:
    if ((3 != status) && (newClassName == cell.className))
    {   newClassName = 'markClearVisits';
        status = 3;
    }

    $.post("<?=base_url()?>admin/attendance/setAttendanceStatus", {clid: clientId, grid: group_id, status: status, target_date: targetDate}, function()
    {
        cell.className = newClassName; <?php /* no error check! :( */ ?>
    });
}
</script>

<script type="text/javascript">

    $(document).ready( function() {
        // switch markPresent
        $("#switchLeftClick").click( function()
        {
            var marker = $(this).val();
            <?php /* loop through 3 states: visit, absence, clear. */ ?>
            if ('VISIT' == marker)
            {   $(this).val('ABSENCE');
            }
            else if ('ABSENCE' == marker)
            {   $(this).val('CLEAR visit');
            }
            else if ('CLEAR visit' == marker)
            {   $(this).val('VISIT');
            }
        });
    });

</script>


<h2><?=$title?></h2>
<br><br>


<div align=left id="options" style='margin-right:15px;margin-bottom:14px;'>
    Отметить: <input type="button" id="switchLeftClick" value="VISIT" /> (клик два раза на одну и ту же клетку приводит к очистке статуса данной клетки)
</div>

<div id="pagewrap">
    <div id="body">
        <input type='hidden' id=groupId value='<?=$group->id?>'>
        <table id="resultTable" class="listtable">
          <thead>
          <tr>
            <td class='tinyHeader'>&nbsp;</td>
            <?php
                $styles = array(0 => 'color1', 1 => 'color2');
                $day_iterator = 999;
                $style_index = 0;

                if (count($group_days)) // protect from the case when no days are available in a group.
                {   $day_iterator = $group_days[0]['weekday']; /* get the very first weekday in a set. */
                }
            ?>
            <?php foreach ($group_days as $gd):?>
            <?php
                    $week_switched = ($day_iterator <= $gd['weekday']) ? 0 : 1;
                    if ($week_switched)
                    {   $style_index = ($style_index + 1) % 2;
                    }

                    $day_iterator = $gd['weekday']; // put new value.
                ?>
                <td class='tinyHeader <?=$styles[$style_index]?>'>
                <?=$gd['date']?><br><?=$gd['weekdayName']?>
                </td>
            <?php endforeach?>
          </tr>
        </thead>
        <tbody>
        <?php foreach ($payments as $row):?>
            <tr>
                <td><a href="../../genericTypes/addOrEdit/clients/<?=$row->client_id?>"><?=$row->clientName?></a> +<?=$row->trainings_count_bought?> занятия</td>
                <?php foreach ($group_days as $gd):?>
                    <?php
                    $css_style = $CI->util_trainings->getCellStyle($attendance_days, $row->client_id, $gd['fullDate']);
                    ?>
                <td class='<?=$css_style?>' onclick='cSwap(this, <?=$row->client_id?>, "<?=$gd['fullDate']?>")'></td>
                <?php endforeach?>
            </tr>
        <?php endforeach?>
        </tbody>
        </table>
    </div>
</div>


