<?php if (isset($onchange_track)):?>
<script type="text/javascript">

<?php
    // For now write all possible JS-code below, in this file.
    // TODO: move to separate js-files and include them upon need only.
?>
function enableDisableControls()
{

    var lbTypes = document.forms["myform"].elements['<?=$comboboxName?>'];
    var idx = lbTypes.selectedIndex;
	{
	    document.forms["myform"].elements['newsBody'].disabled      = (idx > 0);
	    document.forms["myform"].elements['newsBody_ru'].disabled   = (idx > 0);
	    document.forms["myform"].elements['newsTitle'].disabled     = (idx > 0);
	    document.forms["myform"].elements['newsTitle_ru'].disabled  = (idx > 0);
	    document.forms["myform"].elements['newsPath'].disabled      = (idx > 0);
    }
}
</script>
<?php endif;?>

<select style='width:200px;'
<?php
    if (isset($attribs))
    {   echo $attribs;    
    }
?>
name='<?=$comboboxName?>'
id='<?=$comboboxName?>'
    <?php if (isset($onchange_track)):?>
    onchange="<?=$onchange_track?>"
    <?php endif;?>
>
<?php
    if (isset($defaultString) && ('' != $defaultString))
    {
        //option for none selection
        printf("<option value=-1>$defaultString</option>");
    }
?>
<?php
foreach ($items as $key => $value)
{
    if (isset($selectedItemId) && ((int)$key == $selectedItemId))
    {   printf("<option value='%d' selected>%s</option>", (int)$key, $value);
    }
    else
    {   printf("<option value='%d'>%s</option>", (int)$key, $value);
    }
}
?>
</select>