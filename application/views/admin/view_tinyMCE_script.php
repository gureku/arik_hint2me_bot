<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script src="<?=base_url()?>tiny_mce/tiny_mce.js" type="text/javascript"></script>
 <script type="text/javascript">
 tinyMCE.init({
    // General options
    mode : "textareas",
    theme : "advanced",
    width : "400",
    height : "350",

    default_link_target:"_blank", // NOTE: undocumented!! Opens new links in NEW window ALWAYS! Makes sence for TG.

    remove_script_host: false,
    relative_urls : false,
    forced_root_block : false,
    force_p_newlines : false,
    <?php
    if (isset($bMCEreadonly) && $bMCEreadonly)
        print 'readonly : 1,';
    else
        print 'readonly : 0,';
    ?>
    plugins : "embed,safari,spellchecker,pagebreak,style,table,save,advhr,advimage,advlink,iespell,inlinepopups,insertdatetime,preview,searchreplace,print,contextmenu,paste,fullscreen,noneditable,visualchars,nonbreaking,",

    // Theme options
    theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,formatselect,fontselect,fontsizeselect,|,print,|,fullscreen",
//    theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent|,undo,redo,|,link,unlink,image,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
    theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,undo,redo,|,link,unlink,image,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
    theme_advanced_buttons3 : "",

/*
    theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,formatselect,fontselect,fontsizeselect",
    theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,code,help,|,insertdate,inserttime,preview,|,forecolor,backcolor",
    theme_advanced_buttons3 : "embed,|,tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,iespell,advhr,|,print,|,fullscreen",
*/

    theme_advanced_toolbar_location : "top",
    theme_advanced_toolbar_align : "left",
    theme_advanced_statusbar_location : "bottom",
    theme_advanced_resizing : true,

    // Office example CSS
    //content_css : "css/office.css",

    // Drop lists for link/image/media/template dialogs
    template_external_list_url : "js/template_list.js",
    external_link_list_url : "js/link_list.js",
    external_image_list_url : "js/image_list.js",
    media_external_list_url : "js/media_list.js",

    extended_valid_elements : "object[width|height|classid|codebase],param[name|value],embed[src|type|width|height|flashvars|wmode],iframe[src|class|width|height|name|align]",

    // Replace values for the template plugin
    template_replace_values : {
        username : "Some User",
        staffid : "991234"
    }
 });
 </script>