<?php
$this->load->view("admin/header");
?>
<link href="<?=base_url()?>css/calendar/cal.css" rel="stylesheet" type="text/css" />

<h2><?=$title;?></h2>
<br />

<br /><br />
<form name='form1' action="<?=base_url()?>admin/groups/doChangeUserPassword" method=post enctype="multipart/form-data">
<input type='hidden' name='pId' value='<?=$userId?>'>

    <div align='left' style="margin-left: 30px;">
    <table>
        <tr><td><?=lang('admin.pass.current')?>:</td><td><input type=password style='width:300px;' name=password_current value=''></td></tr>
    <?php if ($userId > 0):?>
        <tr><td><br><?=lang('admin.pass')?>:</td><td><br><input type=password style='width:300px;' name=password1 value=''></td></tr>
        <tr><td><?=lang('admin.pass.retype')?>:</td><td><input type=password style='width:300px;' name=password2 value=''></td></tr>
    <?php endif?>
        <tr><td> </td><td align='right'><input type=submit value='<?=lang('button.change')?>'> <input type=reset value='<?=lang('button.cancel')?>' onclick='javascript:document.location.href="<?=$backUrl?>"' ></td></tr>
    </table>
    </div>
</form>

<br><br>
<?php
$this->load->view("admin/footer");
?>