<div class="categorymap">
    <div align='left' style="margin-left: 30px;">
    <h3><?=lang('admin.menu.docs.list')?>:</h3><br />
    <?php
        $img_props = base_url().'images/download.png';

        if (!isset($read_only))
        {   $read_only = true;
        }
        ?>

        <?php if (0 == count($documents)):?>
            <span style="font-size:1.2em;">Загруженных документов нет</span>
        <?php else:?>
        <ul>
            <?php foreach($documents as $doc):?>
            <li>
                <a href='<?=base_url()?>admin/docs/download/<?=$doc->id?>' title='Скачать документ'><img width=24 src='<?=$img_props?>'></a>
                <span style='margin-left: 20px;font-size:1.1em; color:black;'><?=$relativePath.$doc->path?></span>

                <?php if (!$read_only):?>
                    <a style='padding: 20px;' title='Удалить документ' href='<?=base_url()?>admin/docs/delete/<?=$doc->id?>' onclick="return confirm('Вы уверены, что хотите удалить \'<?=basename($doc->path)?>\'?')">
                    <img src='<?=base_url()?>images/admin/del.gif'> <span style="color:red;">удалить</span>
                    </a>
                <?php endif?>

            </li>
            <?php endforeach?>
	    </ul>
        <?php endif?>
    </div>
</div>
