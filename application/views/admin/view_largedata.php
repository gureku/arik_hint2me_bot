<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
       <title><?=$title?></title>
       <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
       <meta name="X-UA-Compatible" content="IE=7" />
       <meta name="description" content="" />
       <meta name="keywords" content="" />

       <script language="javascript" type="text/javascript" src="<?=base_url()?>js/jq/jquery.core.js"></script>
       <script language="javascript" type="text/javascript" src="<?=base_url()?>js/jq/jquery-ui-1.8.4.custom.min.js"></script>

       <script src="<?=base_url()?>ftp/css/admin/new/jq/jqgrid/grid.locale-ru.js" type="text/javascript"></script>
       <script src="<?=base_url()?>ftp/css/admin/new/jq/jqgrid/jquery.jqGrid.min.js" type="text/javascript"></script>
       <script language="javascript" type="text/javascript">
           <?=$grid_variables?>
       </script>

       <script language="javascript" type="text/javascript" src="<?=base_url()?>ftp/css/admin/new/jq/jqGridLib.js"></script>
       <link rel="stylesheet" type="text/css" href="<?=base_url()?>ftp/css/admin/new/master.css" media="screen" />
       <!--[if IE]><link rel="stylesheet" type="text/css" href="<?=base_url()?>ftp/icetwice/css/ie.css" media="screen" /><![endif]-->
<?php
if (isset($extraHeadContent))
{   echo $extraHeadContent;
}
?>
   </head>
   <body <?php if (isset($onload_handler)) print "onload='$onload_handler();'"; ?>>
       <div id="Page">
           <div id="Header">
               <div id="Logo"><a href="mnt"><img src="<?=base_url()?>ftp/css/admin/new/images/logo.gif" alt="" title="Перейти в Панель администратора" /></a>
               <span style='text-align:right; padding-left:50px; padding-bottom:15px; vertical-align:middle;'><a href="<?=base_url()?>" target=_blank title='Просмотр сайта в новом окне'><img src='<?=base_url()?>images/admin/home.gif'></a></span>
               </div>
               <div id="Account">
                   <span style='padding-right:10px;'>Иванов А.К.</span>
                   <a href="<?=base_url()?>auth/logout/">Выход</a>
               </div>
           </div>
           <div id="Wrapper">
               <div id="Sidebar">
                   <ul class="Menu">
                        <?php 
                        /*
                            <li class="jCollapser">
                                <a href="#" class="jTrigger IconLink">Group 3</a>
                                <ul class="jAnimated Hidden">
                                    <li><a href="#" class="IconLink Dot">Item 3.1</a></li>
                                    <li><a href="#" class="IconLink Dot">Item 3.2</a></li>
                                    <li><a href="#" class="IconLink Dot">Item 3.3</a></li>
                                </ul>
                            </li>
                        */
                        ?>
                        <li class="jCollapser">
                            <a href="#" class="jTrigger IconLink">Login box</a>
                            <ul class="jAnimated">
                                <li><a href="<?=base_url()?>auth/logout" class="IconLink Dot"><?=lang('admin.lougout')?></a></li>
                                <li><a href="<?=base_url()?>" target=_blank class="IconLink Dot" title='Просмотр сайта в новом окне'><img src='<?=base_url()?>images/admin/home.gif'> Просмотр сайта</a></li>
                            </ul>
                        </li>

                        <li class="jCollapser">
                            <a href="#" class="jTrigger IconLink"><?=lang('admin.menu.common')?></a>
                            <ul class="jAnimated">
                                <li><a href="news" class="IconLink Dot"><?=lang('mnu.news.u.ru')?></a></li>
                                <li><a href="docs" class="IconLink Dot"><?=lang('mnu.docsupload')?></a></li>
                            </ul>
                        </li>

                        <li class="jCollapser">
                            <a href="#" class="jTrigger IconLink">Customers</a>
                            <ul class="jAnimated">
                                <li><?php echo anchor('admin/largedata/', 'Клиенты (ajax)', array('class'=>"IconLink Dot"));?></li>
                                <br>
                                <li><?php echo anchor('admin/genericTypes/show/clients', 'Клиенты', array('class'=>"IconLink Dot"));?></li>
                                <li><?php echo anchor('admin/genericTypes/show/training_groups', 'Группы', array('class'=>"IconLink Dot"));?></li>
                                <li><?php echo anchor('admin/trainings_schedule', '<strong>Расписание занятий</strong>', array('class'=>"IconLink Dot"));?></li>
                                <br>
                                <li><?php echo anchor('admin/attendance', '<strong>Посещение занятий</strong>', array('class'=>"IconLink Dot"));?></li>
                                <li><?php echo anchor('admin/genericTypes/show/payments', 'Платежи', array('class'=>"IconLink Dot"));?></li>
                                <br>
                                <li><?php echo anchor('admin/genericTypes/show/trainers', 'Тренеры', array('class'=>"IconLink Dot"));?></li>
                                <br>
                                <li><?php echo anchor('admin/genericTypes/show/group_types', 'Типы групп', array('class'=>"IconLink Dot"));?></li>
                                <li><?php echo anchor('admin/genericTypes/show/ticket_types', 'Типы абонементов', array('class'=>"IconLink Dot"));?></li>
                            </ul>
                        </li>

                        <li class="jCollapser">
                            <a href="#" class="jTrigger IconLink">Товары:</a>
                            <ul class="jAnimated">
                                <li><?php echo anchor('admin/genericTypes/show/goods', 'Товары', array('class'=>"IconLink Dot"));?></li>
                                <li><?php echo anchor('admin/genericTypes/show/goods_categories', 'Категории товаров', array('class'=>"IconLink Dot"));?></li>
                            </ul>
                        </li>

                        <li class="jCollapser">
                            <a href="#" class="jTrigger IconLink"><?=lang('admin.menu.blog')?></a>
                            <ul class="jAnimated Hidden">
                                <li><?php echo anchor('admin/blog_contents', lang('mnu.blog.entries'), array('class'=>"IconLink Dot"));?></li>
                                <li><?php echo anchor('admin/blog_contents/options', lang('mnu.blog.options'), array('class'=>"IconLink Dot"));?></li>
                            </ul>
                        </li>


                        <li class="jCollapser">
                            <a href="#" class="jTrigger IconLink"><?=lang('admin.menu.pages')?></a>
                            <ul class="jAnimated">
                                <li><?php echo anchor('admin/blog_contents', lang('mnu.blog.entries'), array('class'=>"IconLink Dot"));?></li>
                                <li><?php echo anchor('admin/blog_contents/options', lang('mnu.blog.options'), array('class'=>"IconLink Dot"));?></li>

                                <li><?php echo anchor('admin/menus', lang('admin.menu.pages'), array('class'=>"IconLink Dot"));?></li>
                                <li><?php echo anchor('admin/menus/menuGroupsNEW', lang('admin.menu.order'), array('class'=>"IconLink Dot"));?></li>
                                <br>
                                <li><?php echo anchor('admin/widgets', lang('admin.menu.widgets'), array('class'=>"IconLink Dot"));?></li>
                            </ul>
                        </li>

                        <li class="jCollapser">
                            <a href="#" class="jTrigger IconLink"><?=lang('admin.menu.albums')?></a>
                            <ul class="jAnimated">
                                <li><?php echo anchor('admin/albums', lang('admin.menu.albums'), array('class'=>"IconLink Dot", 'title' => lang('admin.menu.albums.hint')));?></li>
                            </ul>
                        </li>

                        <li class="jCollapser">
                            <a href="#" class="jTrigger IconLink"><?=lang('admin.menu.users')?></a>
                            <ul class="jAnimated Hidden">
                                <li><?php echo anchor('admin/groups/users', lang('admin.menu.users'), array('class'=>"IconLink Dot",'title' => lang('admin.menu.users.hint')));?></li>
                                <li><?php echo anchor('admin/groups/roles', lang('admin.menu.roles'), array('class'=>"IconLink Dot",'title' => lang('admin.menu.roles.hint')));?></li>
                            </ul>
                        </li>
                    </ul>
               </div>
               <div id="Content">
                   <table id="Grid"></table>
                   <div id="Pager"></div>
               </div>
               <div id="Rightbar">
                   <ul class="Menu">
                        <li><input type="button" class="jAction" id="jAction_1" value="Action 1" /></li>
                        <li><input type="button" class="jAction" id="jAction_2" value="Action 2" /></li>
                        <li><input type="button" class="jAction" id="jAction_3" value="Action 3" /></li>
                    </ul>
               </div>
           </div>
       </div>

<div id="footer">
Copyright &copy; 2016-<?=date('Y')?> by <a href="mailto:info@walltouch.ru">WallTouch</a>
</div>
   </body>
</html>
