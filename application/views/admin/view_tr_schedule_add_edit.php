<link href="<?=base_url()?>css/calendar/cal.css" rel="stylesheet" type="text/css" />

<?php
// $this->load->view('admin/view_tinyMCE_script');
?>

<script src="<?=base_url()?>js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>js/jquery.maskedinput-1.3.js" type="text/javascript"></script>
<script type="text/javascript">
   jQuery(function($) {
      $.mask.definitions['~']='[+-]';
      $('#time_1').mask("99:99");
      $('#time_1_end').mask("99:99");
      $('#time_2').mask("99:99");
      $('#time_2_end').mask("99:99");
      $('#time_3').mask("99:99");
      $('#time_3_end').mask("99:99");
      $('#time_4').mask("99:99");
      $('#time_4_end').mask("99:99");
      $('#time_5').mask("99:99");
      $('#time_5_end').mask("99:99");
      $('#time_6').mask("99:99");
      $('#time_6_end').mask("99:99");
      $('#time_7').mask("99:99");
      $('#time_7_end').mask("99:99");
   });
</script>


<br><br>
<div align="left">
<form name='form1' action="<?=base_url()?>admin/<?=$actionUri?>" method=post enctype="multipart/form-data">
    <input type=hidden name="recordId" value="<?=$recordId?>">

    <div align='left' style="margin-left: 180px;">
    <table class='lighttable' style='width:45%;'>
        <?php if (!isset($hide_groups_combobox)):?>
        <tr>
            <td>Группа:</td>
            <td colspan=4>
            <?php
                $data['comboboxName']   = 'trGroups';
                $data['items']          = $tr_groups;
                $data['selectedItemId'] = $tr_group_id;
                $data['attribs']        = 'size="7"';

                $this->load->view('admin/view_combobox', $data);
            ?>
            </td>
        </tr>
        <tr><td colspan=5>&nbsp;</td></tr>
        <?php else:?>
            <input type=hidden name="trGroups" value="<?=$tr_group_id?>"> <?php /* still must supply the groupId. */ ?>       
        <?php endif?>

<?php
$weekdays = array(  1   => 'Понедельник',
                    2   => 'Вторник',
                    3   => 'Среда',
                    4   => 'Четверг',
                    5   => 'Пятница',
                    6   => 'Суббота',
                    7   => 'Воскресенье');
?>
        <tr>
            <td>&nbsp;</td>
            <td style='text-align:center;'><strong>От</strong></td>
            <td style='text-align:center;'><strong>До</strong></td>
            <td colspan=2>&nbsp;</td>
        </tr>

<?php foreach ($weekdays as $day_index => $dayName):?>
        <tr>
            <td>&nbsp;</td>
            <?php
                $current_group_trainings    = $trainings[$tr_group_id];

                // contains all the day data for a group:
                $daily_data                = $current_group_trainings[$day_index];
            ?>
            <td><input type=text class='hasTimepicker' name='time_<?=$day_index?>' id='time_<?=$day_index?>' value='<?=($daily_data['g_time_start'] > 0 ? date('H:i', strtotime($daily_data['g_time_start'])) : '')?>' maxlength="5" size="5"></td>
            <td><input type=text class='hasTimepicker tp_end' name='time_<?=$day_index?>_end' id='time_<?=$day_index?>_end' value='<?=($daily_data['g_time_end'] > 0 ? date('H:i', strtotime($daily_data['g_time_end'])) : '')?>' maxlength="5" size="5"></td>
            <td><input type="checkbox" class="checked" id="cb_is_active_<?=$day_index?>" name="cb_is_active_<?=$day_index?>" <?=($daily_data['is_active'] ? 'checked': '');?> ></td>
            <td><label for="cb_is_active_<?=$day_index?>"><?=$dayName?></label></td>

            <td colspan=4>
            <?php

                $data['comboboxName']   = 'combo_halls_'.$day_index;
                $data['items']          = $tr_halls;
                $data['selectedItemId'] = $daily_data['tr_hall_id'];
                $data['attribs']        = 'size="1"';

//                $this->load->view('admin/view_combobox', $data);
                $this->load->view('admin/view_combobox_grouped', $data);
            ?>
            </td>
        </tr>
<?php endforeach ?>

        <tr><td colspan=5>&nbsp;</td></tr>
        <tr>
        <td colspan=5 align='right'><input type=submit value='<?=lang('button.save')?>'>
        <input type=reset value='<?=lang('button.cancel')?>' onclick='javascript:document.location.href="<?=$backUrl?>"' >
        </td></tr>
        
    </table>
    </div>

</form>

</div>
