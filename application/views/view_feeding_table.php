<?php defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>

<html lang="en">
<head>
<meta charset="utf-8">

<?php
//    header( 'Expires: Sat, 26 Jul 1997 05:00:00 GMT' );
//    header( 'Last-Modified: ' . gmdate( 'D, d M Y H:i:s' ) . ' GMT' );
//    header( 'Cache-Control: no-store, no-cache, must-revalidate, max-age=0, private' );
//    header( 'Cache-Control: post-check=0, pre-check=0', false );
//    header( 'Pragma: no-cache' );
//    header( "Content-Security-Policy: default-src https:; script-src https: 'unsafe-inline'; style-src https: 'unsafe-inline'" );
//    header( "Content-Security-Policy: script-src 'nonce-EDNnf03nceIOfn39fn3e9h3sdfa111' 'nonce-EDNnf03nceIOfn39fn3e9h3sdfa222'" );
?>
<style type="text/css">
 body {
   font-family: 'lato', sans-serif;
 }

 .container {
   max-width: 1000px;
   margin-left: auto;
   margin-right: auto;
   padding-left: 10px;
   padding-right: 10px;
 }

 h2 {
   font-size: 26px;
   margin: 20px 0;
   text-align: center;
 }
 h2 small {
   font-size: 0.5em;
 }

 .responsive-table li {
   border-radius: 3px;
   padding: 10px 20px;
   display: flex;
   justify-content: space-between;
   margin-bottom: 2px;
 }
 .responsive-table .table-header {
   background-color: #95A5A6;
   font-size: 14px;
   text-transform: uppercase;
   letter-spacing: 0.05em;
   font-weight:bold;
 }
 .responsive-table .table-row {
   background-color: #ffffff;
   box-shadow: 0px 0px 2px 0px rgba(0, 0, 0, 0.1);
 }
 .responsive-table .col-1 {
   flex-basis: 10%;
 }
 .responsive-table .col-2 {
   flex-basis: 40%;
 }
 .responsive-table .col-3 {
   flex-basis: 25%;
 }
 .responsive-table .col-4 {
   flex-basis: 25%;
 }
 @media all and (max-width: 767px) {
   .responsive-table .table-header {
     display: none;
   }
   .responsive-table li {
     display: block;
   }
   .responsive-table .col {
     flex-basis: 100%;
   }
   .responsive-table .col {
     display: flex;
     padding: 10px 0;
   }
   .responsive-table .col:before {
     color: #6C7A89;
     padding-right: 10px;
     content: attr(data-label);
     flex-basis: 50%;
     text-align: right;
   }
 }


.bgimg
{
    background-size:cover;
}

</style>

<script src="https://kit.tgfile.tech/js/jquery-3.5.1.min.js" nonce="EDNnf03nceIOfn39fn3e9h3sdfa111"></script>

	<title>Список питания учеников</title>
</head>

<body>

<div class="container" style="text-align:left; margin:1.8%; width: 80%; max-width:800px;">
    <h2 style="background-image: url(images/kitlogo.jpg); background-repeat: no-repeat; height: 41px; padding: 5px 0 0 50px; text-align:left; vertical-align:middle;"><?=$title?></h2>

  <ul class="responsive-table" style="text-align: center;">
    <li class="table-header">
      <div class="col col-1">Класс</div>
      <div class="col col-2">Ученики</div>
      <div class="col col-3">С питанием?</div>
    </li>

    <?php
        $diets_title = array(
            0 => "(без питания)",
            1 => "мясоедческое \xF0\x9F\x8D\x96",
            2 => "вегетарианское \xF0\x9F\x8C\xB1",
        );
    ?>
    <?php foreach ($class_students_mapping as $row):?>
    <?php if (0 == count($row['students'])):?>
     <li class="table-row">
      <div class="col col-1" data-label="Класс"><?=$row['name']?></div>
      <div class="col col-2" data-label="Ученики">Учеников не зарегистрировано</div>
      <div class="col col-3" data-label=""></div>
    </li>
    <?php continue;?>
    <?php endif?>

    <li class="table-row">
      <div class="col col-1" data-label="Класс"><?=$row['name']?></div>
      <div class="col col-2" data-label=""></div>
      <div class="col col-3" data-label=""></div>
    </li>

        <?php foreach ($row['students'] as $student):?>
        <li class="table-row">
          <div class="col col-1" data-label=""></div>
          <div class="col col-2" data-label="ФИО"><?=Util_school::format_client_name($student)?></div>
          <div class="col col-3" data-label="Питание"><?=$diets_title[intval($student->diet_type)]?></div>
        </li>
        <?php endforeach?>
    <?php endforeach?>
  </ul>
</div>

</body>
</html>