<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?=$js_array1?>
<?=$js_arrays_nested?>
<?=$js_array_users?>

<?php $this->load->view('admin/view_tinyMCE_script'); ?>

<?php /* this variable will be used in Javascript for initializing the combobox selection: */ ?>
<script language="javascript">
var v_subKindID='<?=$subKindID?>';
</script>

<script type="text/javascript">
<?=$containerArray?>
</script>

<script type="text/javascript">
function clearListBoxContents(theLB)
{
	theLB.options.length = 0;
}

function clearListBoxSelection(theLB)
{
	for(i=theLB.options.length-1;i>=0;i--)
	{	theLB.options[i].selected = false;
	}
}


function initSubkindsFromDB()
{
    var lbKinds = document.forms["myform"].elements['kindsList'];
    lbKinds.options[0].selected = true; // select the very first item - e.g. clean the selection.

    <?php /* init the subkinds listbox first: */ ?>
    showSubKinds();

    var lbSubKinds =document.forms["myform"].subKindsList;
    clearListBoxSelection(lbSubKinds);
    
    for(i=lbSubKinds.options.length-1;i>=0;i--)
    {
        if (v_subKindID == lbSubKinds.options[i].value)
        {
            lbSubKinds.options[i].selected = true;
            break;
        }
    }
}

function showSubKinds()
{
	var lbSubKinds =document.forms["myform"].subKindsList;
	var idx=document.forms["myform"].kindsList.selectedIndex;
	if (idx == 0) <?php /* skip either the very first item with word "Select an item..." or if no item is initially selected from the "Kinds" listbox. */ ?>
	{
	    fillLbUsers();
//		clearListBoxContents(lbSubKinds);
		return;
	}


    <?php /* since there'se one additional word at the begin of a list-box named "select...". */ ?>
	idx= idx - 1;

	<?php /* empty the list first: */ ?>
	clearListBoxContents(lbSubKinds);

	var subKindID = <?php echo $kindsArrayName; ?>[idx][0];
	var subArray = <?php echo $js_container_array_name; ?>[subKindID][0];

	for ( var i=0; i < subArray.length; i++ )
	{
		valval = subArray[i][0];
		txt = subArray[i][1];

		op  = new Option(txt, valval);
		lbSubKinds.options.add(op);
	}
}

function fillLbUsers()
{
    var lbSubKinds =document.forms["myform"].subKindsList;
    var usersArray = <?php echo $usersArrayName; ?>;

    clearListBoxContents(lbSubKinds);

    for ( var i=0; i < usersArray.length; i++ )
    {
        valval = usersArray[i][0];
        txt = usersArray[i][1];

        op  = new Option(txt, valval);
        lbSubKinds.options.add(op);
    }
}

function copySelectionFromTo(lbFromName, lbToName)
{
    var lbKinds = document.forms["myform"].elements['kindsList'];
    var lbFrom  = document.forms["myform"].elements[lbFromName];
    var lbTo    = document.forms["myform"].elements[lbToName];

    var idx = lbKinds.selectedIndex;
    
	if (idx < 0)
	{   alert('Please select a group first.');
	    return;
	}

    var idxFrom = lbFrom.selectedIndex;
	if (idxFrom < 0)
	{
	    alert('Please select user(s) first.');
	    return;
	}


    for(i=lbFrom.options.length-1;i>=0;i--)
    {
        if (lbFrom.options[i].selected)
        {
            var val = lbFrom.options[i].value;
            var txt = lbFrom.options[i].text;

            <?php /* add the option: */ ?>
            op  = new Option(txt, val);
            lbTo.options.add(op);
        }
	}

}


function removeSelectedItemFrom(lbFromName)
{
    var lbFrom  = document.forms["myform"].elements[lbFromName];

    var idx = lbFrom.selectedIndex;
	if (idx < 0)
	{   return;
	}

    for(i=lbFrom.options.length-1;i>=0;i--)
    {
        if (lbFrom.options[i].selected)
        {
        	<?php /* and remove the handled option from the source: */ ?>
        	lbFrom.remove(i);
        }
	}
}

function selectListBoxItems(opt)
{
    for (var intLoop=0; intLoop < opt.length; intLoop++)
    {   opt[intLoop].selected = true;
    }
}

function onSubmitForm()
{
    return true; // warning: function exists here!

    //	element = document.forms["myform"].existing;
    //	element.style.display = "none"; // set to "inline" to make it visible.

    var sel = selectListBoxItems(document.forms["myform"].existing);
    sel = selectListBoxItems(document.forms["myform"].toBeRemoved);

    return true;
}
</script>

<form name="myform" onSubmit="return onSubmitForm();" action="<?=base_url()?>admin/groups/sendMassEmail" method=post enctype="multipart/form-data">
    Groups:<br />
    <?=$combo?>
    <br /><br />
    Members:<br>
    <select id='subKindsList' name='subKindsList' size='5' style='width:300px;' multiple='select-multiple'>
    </select><br />
    <a href='#' onclick="javascript:copySelectionFromTo('subKindsList', 'chosenUsersList', 'remove'); return false;">
    <font color=green>add selected user(s) to mailing list</font>
    </a>

    <br /><br />
    Chosen Users:<br />
    <select id='chosenUsersList' name='chosenUsersList' size='5' style='width:300px;' multiple='select-multiple'>
    </select><br />
    <a href='#' onclick="javascript:removeSelectedItemFrom('chosenUsersList'); return false;">
    <font color=red>remove from mailing list</font>
    </a>
    <br /><br />
    Message:<br />
	<textarea rows="10" cols="60" name="message"></textarea><br />
	<br />
	<input type="submit" name="submit" value="���������" />
</form>


<script language="javascript">
initSubkindsFromDB();
</script>

