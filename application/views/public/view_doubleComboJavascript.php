<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?=$js_array1?>
<?=$js_arrays_nested?>
<?=$js_array_users?>


<?php /* this variable will be used in Javascript for initializing the combobox selection: */ ?>
<script language="javascript">
var v_subKindID='<?=$subKindID?>';
</script>

<script type="text/javascript">
<?=$containerArray?>
</script>

<script type="text/javascript">
<?php if (isset($order_up_down_Uri)):?>
function submitOrderedList(list)
{
    var sortedItemIDs = '';
    for(i = 0; i < list.options.length; i++)
    {
        if (i == 0)
        {   sortedItemIDs += list.options[i].value;
        }
        else
        {   sortedItemIDs += ',' + list.options[i].value;
        }
    }

    var postData = 'sortedItemIDs=' + sortedItemIDs;
    updateLinkedData('<?=$order_up_down_Uri?>', postData);
}
<?php endif?>

function clearListBoxContents(theLB)
{
	theLB.options.length = 0;
}

function clearListBoxSelection(theLB)
{
	for(i=theLB.options.length-1;i>=0;i--)
	{	theLB.options[i].selected = false;
	}
}


function initSubkindsFromDB()
{
    var lbKinds = document.forms["myform"].elements['kindsList'];
    lbKinds.options[0].selected = true; <?php /* select the very first item - e.g. clean the selection.  */ ?>

    <?php /* init the subkinds listbox first: */ ?>
    showSubKinds();

    var lbSubKinds =document.forms["myform"].subKindsList;
    clearListBoxSelection(lbSubKinds);
    
    for(i=lbSubKinds.options.length-1;i>=0;i--)
    {
        if (v_subKindID == lbSubKinds.options[i].value)
        {
            lbSubKinds.options[i].selected = true;
            break;
        }
    }
}

function showSubKinds()
{
	var lbSubKinds =document.forms["myform"].subKindsList;
	var idx=document.forms["myform"].kindsList.selectedIndex;
	if (idx <= 0) <?php /* skip either the very first item with word "Select an item..." or if no item is initially selected from the "Kinds" listbox. */ ?>
	{
		clearListBoxContents(lbSubKinds);
		return;
	}


    <?php /* since there'se one additional word at the begin of a list-box named "select...". */ ?>
	idx= idx - 1;

	<?php /* empty the list first: */ ?>
	clearListBoxContents(lbSubKinds);

	var subKindID = <?php echo $kindsArrayName; ?>[idx][0];
	var subArray = <?php echo $js_container_array_name; ?>[subKindID][0];

	for ( var i=0; i < subArray.length; i++ )
	{
		valval = subArray[i][0];
		txt = subArray[i][1];

		op  = new Option(txt, valval);
		lbSubKinds.options.add(op);
	}
}
<?php /* this invokes more processing after showing subkinds: fills in users list which are not contained in "arrSubKinds". */ ?>
function showSubKindsLinked()
{
    showSubKinds();

    var lbUsers =document.forms["myform"].usersList;
    clearListBoxContents(lbUsers);


    var idx=document.forms["myform"].kindsList.selectedIndex;
    if (idx <= 0) <?php /* skip either the very first item with word "Select an item..." or if no item is initially selected from the "Kinds" listbox.*/ ?>
    {
        fillLbUsers();
        return;
    }


    <?php /* since there'se one additional word at the begin of a list-box named "select...". */ ?>
    idx= idx - 1;


    var lbSubKinds =document.forms["myform"].subKindsList;
    var subKindID = <?php echo $kindsArrayName; ?>[idx][0];
    var usersArray = <?php echo $usersArrayName; ?>;

    for ( var i=0; i < usersArray.length; i++ )
    {
        valval = usersArray[i][0];

        var bFound = false;
        for(k=0; k < lbSubKinds.options.length; k++)
        {
            if (valval == lbSubKinds.options[k].value)
            {   bFound = true;
                break;
            }
        }

        <?php /* add only linked_items which doesn't belong to SubKinds list: */ ?>
        if (!bFound)
        {
            txt = usersArray[i][1];

            op  = new Option(txt, valval);
            lbUsers.options.add(op);
        }
    }
}

function fillLbUsers()
{
    var lbUsers =document.forms["myform"].usersList;
    var usersArray = <?php echo $usersArrayName; ?>;

    for ( var i=0; i < usersArray.length; i++ )
    {
        valval = usersArray[i][0];
        txt = usersArray[i][1];

        op  = new Option(txt, valval);
        lbUsers.options.add(op);
    }
}
 <?php /*returns -1 if nothing is selected*/ ?>
function getSelectedKindId(lbFromName, startIndex)
{
    var lbFrom  = document.forms["myform"].elements[lbFromName];
    var idx = lbFrom.selectedIndex;
	if (idx < startIndex) <?php /* skip either the very first item with word "Select an item..." or if no item is initially selected from the "Kinds" listbox. */ ?>
	{
		return -1;
	}

    <?php /* since there'se one additional word at the begin of a list-box named "select...". */ ?>
	idx= idx - startIndex;

	var subKindID = <?php echo $kindsArrayName; ?>[idx][0];
	return subKindID;
}

function invokeItemEdit()
{
	var subKindID = getSelectedKindId('kindsList', 1);
	if (subKindID < 0)
	{   return;
	}

	window.location = "<?=$link_edit?>" + subKindID;
}

function invokeItemDelete()
{
    var subKindID = getSelectedKindId('kindsList', 1);
	if (subKindID < 0)
	{   return;
	}


	if (!confirm('<?=lang('admin.confirmdelete')?>'))
    {   return false;
    }

	window.location = "<?=$link_delete?>" + subKindID;
}


// by Krijn Hoetmer ~ http://krijnhoetmer.nl/
// by Paul Gration ~ http://i-labs.co.uk/
function moveUp(element, startIndex) {
var dirty = false;
  for(i = startIndex; i < element.options.length; i++) {
    if(element.options[i].selected == true) {
      if(i != startIndex) {
        var temp = new Option(element.options[i-1].text,element.options[i-1].value);
        var temp2 = new Option(element.options[i].text,element.options[i].value);
        element.options[i-1] = temp2;
        element.options[i-1].selected = true;
        element.options[i] = temp;

        dirty = true;
      }
    }
  }

  if (dirty)
  {
    submitOrderedList(element);
  }
}


function moveDown(element, startIndex) {
var dirty = false;
  for(i = (element.options.length - 1); i >= startIndex; i--) {
    if(element.options[i].selected == true) {
      if(i != (element.options.length - 1)) {
        var temp = new Option(element.options[i+1].text,element.options[i+1].value);
        var temp2 = new Option(element.options[i].text,element.options[i].value);
        element.options[i+1] = temp2;
        element.options[i+1].selected = true;
        element.options[i] = temp;

        dirty = true;
      }
    }
  }

  if (dirty)
  {
    submitOrderedList(element);
  }
}

function moveSelectionFromTo(lbFromName, lbToName, operation)
{
    var lbKinds = document.forms["myform"].elements['kindsList'];
    var lbFrom  = document.forms["myform"].elements[lbFromName];
    var lbTo    = document.forms["myform"].elements[lbToName];


    var removeFrom  = '-1';
    var assignTo    = '-1';
    var itemsListChanged = '';

    var idx = lbKinds.selectedIndex;
	if (idx > 0) // +1 since the very first element is just wording "Choose..."
	{
	    if ('add' == operation)
	    {
            assignTo = lbKinds.options[idx].value;
	    }
	    else if ('remove' == operation)
	    {
	        removeFrom = lbKinds.options[idx].value;
	    }
	    else
	    {
	        alert('incorrect operation requested. Terminating.');
            return;
	    }
	}
	else
	{
	    alert('Please select a group first.');
	    return;
	}

    var idxFrom = lbFrom.selectedIndex;
	if (idxFrom < 0)
	{
	    alert('Please select a group first.');
	    return;
	}


    for(i=lbFrom.options.length-1;i>=0;i--)
    {
        if (lbFrom.options[i].selected)
        {
            var val = lbFrom.options[i].value;
            var txt = lbFrom.options[i].text;

            itemsListChanged += val + ',';

            <?php /* add the option: */ ?>
            op  = new Option(txt, val);
            lbTo.options.add(op);

        	<?php /* and remove the handled option from the source: */ ?>
        	lbFrom.remove(i);
        }
	}

    var postData = 'itemIDs=' + itemsListChanged + '&removeFrom=' + removeFrom + '&assignTo=' + assignTo;
	updateLinkedData('<?=$updateUri?>', postData);
}

function updateLinkedData(queryUrl, postData)
{
    var url = '<?=base_url()?>' + queryUrl;
    //var the_div = $('my_div');

    new Ajax.Request(url, {
        method: 'post',
        parameters: postData,
        onCreate: function() {
            //the_div.update('Loading....');
        },
        onSuccess: function(transport) {
            ;//the_div.update(transport.responseText);
            window.location.reload(); <?php /* invoke javascript arrays re-generation to avoid lots of js-code in here. - ??? */ ?>
        }
    });

}

function selectListBoxItems(opt)
{
    for (var intLoop=0; intLoop < opt.length; intLoop++)
    {   opt[intLoop].selected = true;
    }
}

function onSubmitForm()
{
    return true; // warning: function exists here!

    <?php
    //	element = document.forms["myform"].existing;
    //	element.style.display = "none"; // set to "inline" to make it visible.
    ?>

    var sel = selectListBoxItems(document.forms["myform"].existing);
    sel = selectListBoxItems(document.forms["myform"].toBeRemoved);

    return true;
}
</script>


<div align='center'>
<form name="myform" onSubmit="return onSubmitForm();" action=index.php? method=post enctype="multipart/form-data">
<table>
<tr>
    <td>&nbsp;</td>
    <td>
    <?php if (isset($sub_groups)) print $sub_groups;?>
    </td>
</tr>
<tr>
<td>
<?php if (isset($order_up_down_Uri)):?>
&nbsp;&nbsp;&nbsp;
    <a href='#' onclick="moveUp(document.getElementById('kindsList'), 1); return false;" title='move up'><img src='<?=base_url()?>images/admin/up.gif'> up </a><br />
    <a href='#' onclick="moveDown(document.getElementById('kindsList'), 1); return false;" title='move down'><img src='<?=base_url()?>images/admin/down.gif'> down</a>
<?php endif;?>
</td>
    <td><br><?=lang('admin.menu.groups')?>:<br><?=$combo?>
    </td>

    <td>
    <a href='#' onclick="javascript:invokeItemEdit(); return false;" title='Edit chosen group'><?=lang('admin.edit')?> <img src='<?=base_url()?>images/admin/props.gif'></a><br />
    <a href='#' onclick="javascript:invokeItemDelete(); return false;" title='Delete chosen group'><font color=red><?=lang('admin.delete')?></font> <img src='<?=base_url()?>images/admin/del.gif'></a>
    </td>
</tr>

<tr>
<td>
<?php if (isset($order_up_down_Uri)):?>
&nbsp;&nbsp;&nbsp;
        <a href='#' onclick="moveUp(document.getElementById('subKindsList'), 0); return false;" title='move up'><img src='<?=base_url()?>images/admin/up.gif'> up</a><br />
        <a href='#' onclick="moveDown(document.getElementById('subKindsList'), 0); return false;" title='move down'><img src='<?=base_url()?>images/admin/down.gif'> down</a>
<?php endif;?>
</td>
    <td><br /><?=lang('admin.group.members')?>:<br>
    <select id='subKindsList' name='subKindsList' size='5' style='width:300px;' multiple='select-multiple'>
        </select><br />
        <div align='right'>
        <a href='#' onclick="javascript:moveSelectionFromTo('subKindsList', 'usersList', 'remove'); return false;">
        <font color=red><?=lang('admin.remove.from.group')?></font>
        </a></div>
    </td>

    <td>
    <!-- &nbsp;&nbsp;&nbsp;
    <a href='#' title='Edit chosen group member'>edit <img src='<?=base_url()?>images/props.gif'></a><br />
    <a href='#' title='Delete chosen group member'><font color=red>delete</font>  <img src='<?=base_url()?>images/admin/del.gif'></a>
    -->
    </td>
    
</tr>

<tr colspan=3>
<td>
</td>
<td><br><?=lang('admin.availableelements')?>:<br>
    <?=$users_list?><br />
    <div align='right'>
    <a href='#' onclick="javascript:moveSelectionFromTo('usersList', 'subKindsList', 'add'); return false;">
    <font color=green><?=lang('admin.add.to.group')?></font>
    </a></div>
    </td>
</tr>
</table>
</form>
</div>


<script language="javascript">
initSubkindsFromDB();
</script>
