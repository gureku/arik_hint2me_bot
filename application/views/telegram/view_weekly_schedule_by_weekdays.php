<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php
$start_iterator = array(
    1 => 7,
    2 => 1,
    3 => 2,
    4 => 3,
    5 => 4,
    6 => 5,
    7 => 1,
);
//    $wd_numbers     = array(1 => 'Пн',  2 => 'Вт',  3 => 'Ср',  4 => 'Чт',  5 => 'Пт',  6 => 'Сб',  7 => 'Вс'); // to be used with date('N')!
    $wd_numbers_long= array(1 => 'ПОНЕДЕЛЬНИК',  2 => 'ВТОРНИК',  3 => 'СРЕДА',  4 => 'ЧЕТВЕРГ',  5 => 'ПЯТНИЦА',  6 => 'СУББОТА',  7 => 'ВОСКРЕСЕНЬЕ'); // to be used with date('N')!
?>
<?php
    $ci = &get_instance();

    $current_day_no = date('N') ;
    $week_day_no    = $start_iterator[$current_day_no];
    $days_handled   = 0;
for (; $days_handled++ < 7; ): ?>
<?php if (isset($week_days[$week_day_no])):?>
<?php $trainings_per_course = $week_days[$week_day_no];?>
             ---- <code><?php
if (($current_day_no == $week_day_no)) {echo "\xE2\x9C\x94";} else {echo "";} 
echo $wd_numbers_long[$week_day_no];?></code>: ----
<?php foreach ($trainings_per_course as $course_type_id => $trainings): ?>
<?php if (isset($coursetypes_array[$course_type_id])):?>
  <b><?=$coursetypes_array[$course_type_id]?></b>: <?=$ci->utilitar_date->shorten_list_of_time_events($coursetypes_array[$course_type_id], $trainings);?>

<?php endif?>
<?php endforeach ?>
<?php endif?>
<?php
$week_day_no--;
if (0 == $week_day_no)
{   $week_day_no = 7;
}
?>
<?php endfor ?>