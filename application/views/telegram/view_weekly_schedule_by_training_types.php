<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php
$current_day_no = date('w') - 1;
$days_handled = 0;

    $ci = &get_instance();
    $wd_numbers     = array(1 => 'Пн',  2 => 'Вт',  3 => 'Ср',  4 => 'Чт',  5 => 'Пт',  6 => 'Сб',  7 => 'Вс'); // to be used with date('N')!
    $wd_numbers_long= array(1 => 'ПОНЕДЕЛЬНИК',  2 => 'ВТОРНИК',  3 => 'СРЕДА',  4 => 'ЧЕТВЕРГ',  5 => 'ПЯТНИЦА',  6 => 'СУББОТА',  7 => 'ВОСКРЕСЕНЬЕ'); // to be used with date('N')!
?>
<?php foreach ($training_types_sorted as $coursetype_id => $trainings):?>
<?php if (isset($coursetypes_array[$coursetype_id])):?>
<?php $weekdays_list = $ci->utilitar_date->get_weekdays_list($wd_numbers, $trainings);?>
<?php if ($weekdays_list[0]) { echo "\xE2\x9C\x94";} else { echo "\xE2\x97\xBB"; };?> <b><?=$coursetypes_array[$coursetype_id]?></b>: <?=$weekdays_list[1]?>

<?php endif ?>
<?php endforeach ?>
