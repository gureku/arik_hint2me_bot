<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php
//    $wd_numbers     = array(1 => 'Пн',  2 => 'Вт',  3 => 'Ср',  4 => 'Чт',  5 => 'Пт',  6 => 'Сб',  7 => 'Вс'); // to be used with date('N')!
    $wd_numbers_long= array(1 => 'ПОНЕДЕЛЬНИК',  2 => 'ВТОРНИК',  3 => 'СРЕДА',  4 => 'ЧЕТВЕРГ',  5 => 'ПЯТНИЦА',  6 => 'СУББОТА',  7 => 'ВОСКРЕСЕНЬЕ'); // to be used with date('N')!
    $current_day_no = date('N') ;
    $ci = &get_instance();
?>
<?php foreach ($week_days as $week_day_no => $trainings): ?>
<?php /*log_message('error', "VIEW ITERATING OVER wd#$week_day_no: ".print_r($week_days[$week_day_no], true));*/?>
             ---- <code><?php
if (($current_day_no == $week_day_no)) {echo WizardTrainingsEditor::PIN_MARKER;}
echo $wd_numbers_long[$week_day_no];?></code>: ----
<?php foreach ($trainings as $training): ?>
  <b><?=$training['training']?></b>: <?=date('H:i', strtotime($training['datetime']['starttime']));?> (<?=$ci->utilitar_date->formatDurationSmart_from_to($training['datetime']['starttime'], $training['datetime']['endtime'])?>)
<?php endforeach ?>
<?php endforeach ?>