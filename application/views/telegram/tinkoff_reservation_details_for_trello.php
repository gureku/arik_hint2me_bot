<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?=$visit_type?> на **<?=$training_directions?>**

---

**Родитель:** <?=$parent_name?>, тел./email: <?=$parent_contacts?>


**Имя ребёнка (детей):** <?=$child_name?>

**Комментарии родителя:**

<?=$comments?>


----

_Задача создана <?=date('H:i, d/m/Y')?> (московское время), для клуба **<?=$organization_name?>**, через виджет от [walltouch.ru](http://walltouch.ru/pages/features.htm)_