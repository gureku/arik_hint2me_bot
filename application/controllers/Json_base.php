<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Json_base extends CI_Controller {

    const ERRCODE__SUCCESS  = 0;
    const ERRCODE__ERROR    = -1;

    function __construct()
    {
        parent::__construct();

        $this->CI = &get_instance();

        //$this->CI->load->model('utilitar');
        $this->CI->load->model('utilitar_db');
    }

	protected function index()
	{
	    log_message('error', "JSON_BASE->INDEX makes no sense so 401 error to you.");
        $this->output->set_status_header('401');
        //echo 'Direct entrance is not allowed';
//		redirect(base_url());
	}

    // NOTE: $extra_params_key_value should be of format: array of { key => value }, to be able to merge with main response data.
    public static function success_data($message, &$rows, $totalRows, $extra_params_key_value = null)
    {
        $res = array('rows' => $rows, 'totalRows' => $totalRows);
        if ($extra_params_key_value)
        {   $res = array_merge($res, $extra_params_key_value);
        }

        Json_base::success($message, $res);
    }

    // Echoes to the output stream the succcess json.
    public static function success($message, $additional_params = null, $wrapped4trace = null)
    {

        // NOTE: it is important not to print() but to call $this->output->set_output()! Otherwise Sesion get nervous! :)
        $CI = &get_instance();

        Json_base::add_CORS_headers();
		Json_base::set_json_header();

		if (is_array($additional_params) && isset($additional_params['typo_foolproof']))
        {   $returnValue = Json_base::_create_result_json(Json_base::ERRCODE__SUCCESS, $message); // ignore "$additional_params"  value.
        }
        else
        {   $returnValue = Json_base::_create_result_json(Json_base::ERRCODE__SUCCESS, $message, $additional_params);
        }

		// NOTE: this HTTP header is for ngnix only. Because for large (JSON) responses ngnix removes "Content-length" HTTP header.
		// Instead it inserts "Transfer-Encoding: chunked" but that "sliced" data is not understood by mobile clients (as it looks garbage).
		// This we add extra header specific to ngnix:

        $CI->output->set_header("X-Accel-Buffering: no");
        $CI->output->set_header("X-Accel-Limit-Rate: off");
//        log_message('error', "XX: response message: ".print_r($returnValue, true));
        $CI->output->set_output($returnValue);
    }

    public static function add_CORS_headers()
    {
        $CI = &get_instance();
        
//        $http_origin = $_SERVER['HTTP_ORIGIN'] ?? '*';
        $http_origin = isset($_SERVER['HTTP_ORIGIN']) ? $_SERVER['HTTP_ORIGIN']: '*';

        $CI->output->set_header("Access-Control-Allow-Origin: $http_origin");
        $CI->output->set_header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $CI->output->set_header("Access-Control-Allow-Headers: X-Requested-With, Accept, Accept-Encoding, Accept-Language, Authorization, Content-Type, Cache-Control, Connection, Cookie, Host, Pragma, Referer, User-Agent");
		$CI->output->set_header("Access-Control-Allow-Credentials: true");
    }

    // Echoes to the output stream the error json.
    public static function error($errcode, $message, $additional_params = null, $wrapped4trace = null)
    {
        // NOTE: it is important not to print() but to call $this->output->set_output()! Otherwise Sesion get nervous! :)
        $CI = &get_instance();

        // a fool-proof: if params passed in are indeed the "$wrapped4trace":
        if (is_array($additional_params) && isset($additional_params['typo_foolproof']))
        {   $returnValue = Json_base::_create_result_json($errcode, $message); // ignore "$additional_params"  value.
        }
        else
        {   $returnValue = Json_base::_create_result_json($errcode, $message, $additional_params);
        }

		Json_base::add_CORS_headers();
        Json_base::set_json_header();
        $CI->output->set_header("X-Accel-Buffering: no");
        $CI->output->set_header("X-Accel-Limit-Rate: off");
        $CI->output->set_output($returnValue)->_display();
		exit();
    }

    // call this when the input data is malformed.
    public static function error400($message)
    {
        $CI = &get_instance();

        $CI->output->set_status_header('400');

        Json_base::error(-1, $message);
    }

    // call if forbidden
    public static function error403($message)
    {
        $CI = &get_instance();

        $CI->output->set_status_header('403');

        Json_base::error(-5, $message);
    }

    // call if forbidden
    public static function error401($message)
    {
        $CI = &get_instance();

        $CI->output->set_status_header('401');

        Json_base::error(-6, $message);
    }

    // call this when the resource is not found.
    public static function error404($message)
    {
        $CI = &get_instance();

        $CI->output->set_status_header('404');

        Json_base::error(-1, $message);
    }

    // call this when the resource is not found.
    public static function error418($message, $token)
    {
        $CI = &get_instance();

        $CI->output->set_status_header('418');
        $returnValue = Json_base::_create_result_json(-1, $message, $token);

        $CI->output->set_output($returnValue);
    }

	public static function set_json_header()
	{
		$CI = &get_instance();
		$CI->output->set_content_type('json');

        //--------------------------------------------------------------+
        //          DEBUG-only!!!!
//        $headers = apache_request_headers();
//        $headers_str = '';
//        foreach ($headers as $header => $value) {
//            $headers_str .= "$header: $value <br />\n";
//        }
//        log_message('error', "REQUEST headers = \n".$headers_str);
//        log_message('error', "REQUEST cookies = \n".print_r($this->input->cookie(), true));
        //--------------------------------------------------------------|
	}

    private static function _create_result_json($errcode, $message, $additional_params = null)
    {
        $res = array('errcode' => $errcode, 'message' => $message);
        $returnValue = null;

        if ($additional_params && is_array($additional_params) && count($additional_params) > 0)
        {   $returnValue = json_encode(array_merge($res, $additional_params));
        }
        else
        {   $returnValue = json_encode($res);
        }

        //log_message('error', '-- JSON server replies '.strlen($returnValue).' bytes to '.Json_base::get_http_connection_ip_and_user_agent_string());
        return $returnValue;
    }

    public static function get_http_connection_ip_and_user_agent_string()
    {
        $CI = &get_instance();
        return '[IP:'.$CI->input->ip_address().', UA:"'.$CI->input->user_agent().'"]';
    }

    // this is useful when dealing with CORS!
    public static function preflight() {
		$method = $_SERVER['REQUEST_METHOD'];
		if($method == "OPTIONS") {
			self::add_CORS_headers();
			self::set_json_header();
			$CI = &get_instance();
			$CI->output->set_output('OK')->_display();
			exit();
		}
	}
}


/* End of file Data.php */
/* Location: ./application/controllers/data.php */
