<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . '/controllers/admin/AdminOnly.php');


class UploadHandler extends AdminOnly {

    private $allowed_extensions;

    function __construct()
    {
        parent::__construct();
        $this->allowed_extensions = array('jpg','jpeg','png','gif','pdf','xml','txt','doc','docx','xls','xlsx','csv','DOC','XML','XLS', 'JPG', 'JPEG', 'PNG', 'PDF');
    }

	public function index()
	{
	    if (!$this->isAllowed(__FUNCTION__))
        {   $this->warnDenied(true);
            return;
        }

	    $this->exec();
	}


	public function exec($dir_key = NULL, $param = NULL)
	{
	    if (!$this->isAllowed(__FUNCTION__))
        {   $this->warnDenied();
            return;
        }
        
	    if ( !($dir_key && $param) )
	    {   echo json_encode(array('success'=>false, 'details' => 'incorrect call'));
	        return;
	    }

	    $CI = &get_instance();
        $CI->load->model('utilitar');

        $ext = $CI->utilitar->getFileExtension($param);
        if (!$ext || !in_array($ext, $this->allowed_extensions))
	    {   echo json_encode(array('success'=>false, 'details' => 'file not allowed'));
	        return;
	    }

        // todo: security: exclude paths, exlude PHP-files & other executable scripts.

        // prepare the target dir first:
        $record = $CI->utilitar->retrieveDirectory($dir_key);
        if (!$record)
        {
            echo json_encode(array('success'=>false, 'details' => 'incorrect directory'));
            return;
        }
        else
		{
            $targetDir = $record['dir'];
            if (!$targetDir)
            {
                echo json_encode(array('success'=>false, 'details' => 'logged out'));
                return;
            }

            log_message('error', "QQQ targetDir=".print_r($targetDir, true));



            if(!is_dir($targetDir))
            {   //mkdir($targetDir, 0664, TRUE);
                mkdir($targetDir, 0777, TRUE);
            }
    
            /*if(!file_exists($targetDir))
            {   mkdir($targetDir);

            }*/

            $dest_filepath = $targetDir.$param;
        }


        $data = array();
        $input = fopen("php://input", "r");
        $fp = fopen($dest_filepath, "w");
        while ($data = fread($input, 1024))
        {   fwrite($fp, $data);
        }
        fclose($fp);
        fclose($input);



        switch ($record['postprocess_code'])
        {
            case Utilitar::PP_NONE: // do nothing.
                break;

            case Utilitar::PP_OBJECT_DOCUMENT:
                $object_id  = $record['dword'];
                $CI->utilitar->registerObjectDoc($object_id, $dest_filepath);
                break;

            case Utilitar::PP_DOCUMENT: // register the document in a DB
                $CI->utilitar->registerDocument($dest_filepath);
                break;

            case Utilitar::PP_THUMBNAIL: // create a thumbnail and record to the album;
                $album_id   = $record['dword'];
                $thumbnail_path = $CI->utilitar->createThumbnail222($dest_filepath, 260, 260); // {width, height}
                $CI->utilitar->_resizePhoto($dest_filepath, 800, 800, FALSE);
                $CI->utilitar->registerPhotoUpload($album_id, $dest_filepath, $thumbnail_path);
                break;
            default:
                break;
        }

        /*
        // Our processing, we get a hash value from the file
        //$return['hash'] = md5_file($_FILES['Filedata']['tmp_name']);
        $return['hash'] = md5_file($dest_filepath);


        // ... and if available, we get image data
        $info = @getimagesize($dest_filepath);

        if ($info) {
            $return['width'] = $info[0];
            $return['height'] = $info[1];
            $return['mime'] = $info['mime'];
        }*/

        echo json_encode(array('success'=>true, 'details' => 'ok'));
    }
}
?>