<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . '/controllers/admin/AdminOnly.php');

class Docs extends AdminOnly {

    function __construct()
    {
        parent::__construct();
    }

	function index()
	{
	    if (!$this->isAllowed(__FUNCTION__))
        {   $this->warnDenied(true);
            return;
        }

	    $this->load->helper(array('form', 'url'));
	    $CI = &get_instance();
        $CI->load->model('utilitar');


        $data = array();
        $data['title'] = lang('admin.menu.docs.manage');
        $data['actionType'] = 'doUploadFile';

        $data['documents'] = $CI->utilitar->getDocuments();
        $data['relativePath'] = base_url();//base_url().$CI->utilitar->getDocumentsServerPath();


        $dir = $CI->utilitar->getDocumentsServerPath();
        $data['dir_token']          = $CI->utilitar->createDirectoryKey($dir, Utilitar::PP_DOCUMENT);
        $data['uploader_component'] = $this->load->view("admin/div_upload", $data, true);

        $data['read_only'] = false;
        $data['lister_component']   = 'view_uploadedDocsList';
        $data['pagename'] = __CLASS__;

        $this->load->view("admin/header", $data);
        $this->load->view("admin/view_docsList222.php", $data);
        $this->load->view("admin/footer");
	}

    public function download($documentId = -1)
    {
        if (!$this->isAllowed(__FUNCTION__))
        {   $this->warnDenied(true);
            return;
        }

        $CI = &get_instance();
        $CI->load->model('utilitar');
        $document = $CI->utilitar->getDocument($documentId);
        if (!$document)
        {   redirect('admin/docs');
        }

        if (0 == $document->docType)
        {   $docs_root = $CI->utilitar->getDocumentsServerPath();
        }
        else
        {   $docs_root = $CI->utilitar->getGoodsUpdateDir();
        }

        //$full_path = "$docs_root/$document->path";    -->
        $full_path = $document->path;// greco! unchecked modification for GOODS.
        if (!file_exists($full_path))
        {   $CI->utilitar->showPublicMessage('� ������ ��������',
                                            '<br>�������� ���������� ��� ����������. '.$full_path  );
            return;
        }

        $this->load->helper('download');
        $fileData = file_get_contents($full_path); // read file contents.
        $return_filename = $CI->utilitar->locale_safe_basename($document->path);

        force_download($return_filename, $fileData);
    }

    // this is rather a hack/patch:
    private function deleteDbBackupFile(&$CI, &$document)
    {
        // extract db backup file from the description field:
        $descr = $document->description;
        $descr = $CI->utilitar->getDbTemplateFilename();// goods_categories_25-02-2010__17_30.gz

        $pos = strpos($document->description, $CI->utilitar->getDbTemplateFilename());
        if ($pos === false)
        {   return;
        }

        $fname = substr($document->description, $pos, strlen($document->description) - $pos - strlen('<br>'));

        $docs_root = $CI->utilitar->getDbBackupDir();
        $CI->utilitar->deleteFile("$docs_root/$fname");
    }

    // this deletes the requested category *only* - no goods (including of child groups') get deleted.
    public function delete($documentId)
    {
        if (!$this->isAllowed(__FUNCTION__))
        {   $this->warnDenied(true);
            return;
        }

        if ($documentId < 0)
        {   redirect("admin/docs");
            return;
        }

        $CI = &get_instance();
        $CI->load->model('utilitar');
        $document = $CI->utilitar->getDocument($documentId);
        if (!$document)
        {   redirect('admin/docs');
        }

        // delete the DB record:
        $this->db->delete('docs', array('id' => $documentId));


        // physically delete the file:
        if (0 == $document->docType)
        {   $docs_root = $CI->utilitar->getDocumentsServerPath();
            $back_url = 'docs';
        }
        else
        {   $docs_root = $CI->utilitar->getGoodsUpdateDir();
            $back_url = 'goodsUpdate';

            // disabled SQL-dumps delete: for safety purposes.
            // $this->deleteDbBackupFile($CI, $document);
        }
        $CI->utilitar->deleteFile($docs_root.'/'.$document->path);

        redirect('admin/docs');
    }
}