<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
//require_once(APPPATH . '/controllers/admin/AdminOnly.php');
//require_once(APPPATH . '/controllers/admin/BaseAdminController.php');
require_once(APPPATH . '/controllers/admin/ModeratorOnly.php');


abstract class BaseGenericController extends CI_Controller
{
//    private $TABLE_NAME;

    function __construct()
    {
        //parent::__construct();

//	    $this->TABLE_NAME   = 'undefined_table';
    }

    private static function get_ci__loaded()
    {   $ci = &get_instance();
		$ci->load->library('tank_auth');
        return $ci;
    }

	static function base__show(&$data, $type_name)
	{
/*
	    if (!$this->isAllowed(__FUNCTION__))
        {   $this->warnDenied();
            return;
        }
*/

	    $data['actionUri']  = '../addOrEdit/'.$type_name;
	}


    static function base__addOrEdit($type_name, &$row, &$data)
    {
        BaseGenericController::add_edit_stub($type_name, $row, $data);
    }

    // i wonder why php does fail if i call "$this->edit(param1, param2)". Instead, using alternative name works. (the all-mighty RTFM is here, i guess..)
    private static function add_edit_stub($type_name, $row, &$data)
    {
        $data['recordId']   = is_null($row) ? -1 : $row->id;
        $data['record']     = $row; // give access to entire row record from the view.
        $data['actionUri']  = 'genericTypes/doAddEdit';//'genericTypes/doAddEdit/'.$type_name;
        $data['backUrl']    = base_url().'index.php/admin/genericTypes/show/'.$type_name;
    }


    // if $requires_postprocessing = "true" then returns row id (either updated or just inserted)
    static function base__doAddEdit($rowId, &$data, $requires_postprocessing = FALSE)
    {
//	    if (!$this->isAllowed(__FUNCTION__))
//        {   $this->warnDenied();
//            return;
//        }

        $redirect_to= array_key_exists('backUrl', $data)    ? $data['backUrl']   : null;
        $table_name = array_key_exists('tableName', $data)  ? $data['tableName'] : null;
        $page_name  = array_key_exists('typeName', $data)   ? $data['typeName']  : null;

        unset($data['backUrl']);    // remove it from the list of data being written to DB
        unset($data['tableName']);  // remove it from the list of data being written to DB
        unset($data['typeName']);   // remove it from the list of data being written to DB

        $ci = BaseGenericController::get_ci__loaded();

        if ($rowId > 0)
        {   $ci->db->where('id', $rowId);
            $ci->db->update($table_name, $data);
        }
        else
        {   $ci->db->insert($table_name, $data);
        }

        if (!$requires_postprocessing)
        {   BaseGenericController::base__doRedirect($page_name, $redirect_to, $rowId);
        }


        // Data returned only if ostprocessing required. Nothing returned upon redirect (useless).
        return ($rowId > 0) ? $rowId : $ci->utilitar_db->get_last_inserted_id();
    }

    static function base__doRedirect($page_name, $redirect_to, $rowId)
    {
        if ($redirect_to)
        {   redirect($redirect_to);
        }
        else
        {
            if ($rowId > 0)
            {   redirect("index.php/admin/$page_name/$rowId", 'location', 302, 3000);
            }
            else
            {   redirect("index.php/admin/$page_name", 'location', 302, 3000);
            }
        }
    }

    function base__delete($page_name, $table_name, $rowId)
    {
        /*if (!$this->isAllowed(__FUNCTION__))
        {   $this->warnDenied(true);
            return;
        }*/
        $ci = BaseGenericController::get_ci__loaded();

        $ci->db->delete($table_name, array('id' => $rowId));
        redirect('index.php/admin/genericTypes/show/'.$page_name);
    }
}
