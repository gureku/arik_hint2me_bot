<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
//require_once(APPPATH . '/controllers/admin/AdminOnly.php');

require_once(APPPATH . '/libraries/Tank_auth.php');

require_once(APPPATH . '/controllers/admin/BaseGenericController.php');

class GenericTypes extends CI_Controller // extends BaseGenericController
                   //implements  IBaseAdminController
{
    const CUSTOM_CONTROL_NAME = 'customCtrlName_';
    const CONTROL_PREFIX = 'cc_engine_'; // this is required to avoid HTML-controls names duplication with any arbitrary (external) html-code.

    public $CI = NULL;

    private $supported_types    = array( // 1--
    // Format is as follows:
    //      type name => type title, array of headers to list rows, model name that implements data load, method_get_rows, method_get_single_row($id), data_section(...).
//INSERT INTO 'gurekucom_wt'.'kk_'
//	('id',
//	'title',
//	'body',
//	'type',
//	'last_update',
//	'order'
            'recommendations_library'   => array(   'Библиотека Рекомендаций',
                                        array('Название', 'Подкатегория','Описание','Очедёдность', 'Дата обновления', '[control]'),
                                        'util_mobis',
                                        'get_recommendations',
                                        'get_recommendation',

                                        // data section:    !!! this is bad: two places about DB :(  !!!
                                        array(  'tableName' => 'recommendations_library',
                                                'rowNames'  => array(   'title',

                                                                        // here comes custom HTML-control:
                                                                        array(  'r_name'    => 'type',
                                                                                'r_type'    => 'listbox', // this should be 'multilevellistbox'
                                                                                'r_datasrc' => 'get_recomm_subcategories',
                                                                                'r_enrich_field' => 'name', // for complex HTML controls: field name containing value suitable to show to a human. E.g. instead of trainerID you will see trainerNAME (from the result given by 'r_datasrc').
                                                                              ),

                                                                        // here comes custom HTML-control:
                                                                        array(  'r_name'    => 'body',
                                                                                'r_type'    => 'textarea'
                                                                              ),
                                                                              
                                                                        'order',
                                                                        'last_update',
                                                                    ),
                                                'rowsExclude'   => array(),
//                                                'onDelete'      => array('table_name'=> 'tr_schedule', 'column_name' => 'tr_group_id'), // remove also linked data on delete. Value to filter by is assumed the "CURRENT ROW_ID".
                                             ),
                                    ),
                                       ); // --1 end of "$supported_types".

    private static function get_ci__loaded()
    {   $ci = &get_instance();
		$ci->load->library('tank_auth');
        return $ci;
    }

    function __construct()
    {
//        BaseGenericController::__construct();
        parent::__construct();

        $this->CI = &get_instance();

        $this->load->helper(array('url', 'language'));
        $this->lang->load('data');

        $this->CI->load->library('tank_auth');

        /*log_message('error', "AO: before auth check");
        if (!$this->tank_auth->is_logged_in())
        {
                log_message('error', "AO: not logged in.");
                redirect('/auth/login/');
        }
        else if ($this->tank_auth->is_admin())
        {   log_message('error', "AO: is admin.");
            // allow access to admins.
        }
        else if ($this->tank_auth->is_moderator())
        {   log_message('error', "AO: is moderator.");
            redirect(base_url().'index.php/admin/genericTypes/show/recommendations_library');
        }
        else
        {   //echo "Dear ".$this->tank_auth->get_username().", welcome abroad!";
            log_message('error', "AO: no valid role supported.");
        }*/

//        $this->CI = $this->get_ci__loaded();
    }

    private function getTypeByName($type)
    {
        if (!isset($this->supported_types[$type]))
	    {
	        return NULL;
	    }

	    return $this->supported_types[$type];
    }


    //
    // Method returns two-dimensional array:    $res['primary'] = array(...) and $res['secondary'] = array(...) // 2nd array - only when needed.
    public function getAjaxData($type_name = null, $param1 = null)
    {
        log_message('error', "getAjaxData($type_name, $param1)");
        $res = array();

        switch ($type_name)
        {
            case 'berk_rooms': // get all locations for particular organization. Rooms also require Cabinet Users' list to be shown as well.
                {
                    // primary contains a list of rooms and secondary - list of CabinetUser accounts for them.
                    $this->CI->load->model('util_berkana_data');

                    $organization_id = intval($param1);
                    if ($organization_id <= 0)
                    {   $res['primary']     = array();
                        $res['secondary']   = array();
                    }
                    else
                    {   $organization_cabinets   = $this->CI->util_berkana_data->getOrganizationUserAccounts($organization_id, SecuredMatrix::ROLE_CABINET);
                        $res['primary']     = array();
                        $res['primary'][-1] = array('id' => -1, 'value' => 'Выберите из списка...');
                        foreach ($organization_cabinets as $cabinet_account)
                        {   $res['primary'][$cabinet_account->userId] = array('id' => $cabinet_account->userId, 'value' => $cabinet_account->username);
                        }

                        $res['secondary']   = array();
                        // $res['secondary'][] = // fill none. 
                    }
                }
                break;

            default:
                break;
        }

        print json_encode($res);
    }

    //
    // input params:
    //      'row_id'
    //      'value' - string value passed via "hidden" HTML field.
    //
    private function customProcessClientGroups($client_id, $value)
    {
        log_message('error', "TODO: customProcessClientGroups for row_id=$client_id \ndataaaaa=".print_r($value, true));
        // remove client from all groups first:
        $this->util_trainings->removeClientFromGroup($client_id, -1);

        // add new groups:
        $group_ids = explode(',', $value, -1); // the very last element is always a trash (why?) so skip it.
        log_message('error', "group_ids=".print_r($group_ids, true));
        foreach ($group_ids as $group_id)
        {   $this->util_trainings->addClientToGroup($client_id, $group_id);
        }
    }
    

    //----------------------------------------------------------------------------+
    //      todo: this code to be moved into separate HELPER-file...
    private function getType_title(&$type)
    {   return $type[0];
    }

    private function getType_headers(&$type)
    {   return $type[1];
    }

    private function getType_modelName(&$type)
    {   return $type[2];
    }

    private function getType_modelMethodRows(&$type)
    {   return $type[3];
    }

    private function getType_modelMethodRow(&$type)
    {   return $type[4];
    }

    private function getType_db_structure($type)
    {   return $type[5];
    }

    private function lookupRowByName($rowNames, $lookupName)
    {
        foreach ($rowNames as $row)
        {
            $control_name = is_array($row) ? $row['r_name'] : $row; // either for complex controls of plain texboxes.

            if (0 == strcmp($lookupName, $control_name))
            {   return $row;
            }
        }

        return NULL;
    }

    //
    // This is about 'render_list' and 'render_single_item' methods.
    //
    private function getType_custom_views(&$type)
    {   return (isset($type[6]) ? $type[6] : array());
    }


    private function getType_db_structure_tablename($type_name)
    {
        $actual_type = $this->getTypeByName($type_name);
	    if (!$actual_type)
	    {   return NULL;
	    }

        $db_structure = $this->getType_db_structure($actual_type);
        return $db_structure['tableName'];
    }

    private function getType_db_structure_onDeleteTableRowNames($type_name)
    {
        $actual_type = $this->getTypeByName($type_name);
	    if (!$actual_type)
	    {   return NULL;
	    }

        $db_structure = $this->getType_db_structure($actual_type);
        return isset($db_structure['onDelete']) ? $db_structure['onDelete'] : NULL;
    }


    //
    // Depending on "$plain_names_only" parameter:
    //      TRUE:   returns just plain row names.
    //
    //      FALSE:   returns rows structure (along with corresponding HTML-control).
    //
    // NOTE: how it works: if the iterator is an array then that's a HTML-control otherwise it's just a string (holding "row name").
    //
    private function getType_db_structure_rowNames($type_name, $plain_names_only = true)
    {
        $actual_type = $this->getTypeByName($type_name);
	    if (!$actual_type)
	    {   return array();
	    }

        $db_structure = $this->getType_db_structure($actual_type);

        if ($plain_names_only)
        {
            $res = array();
            foreach ($db_structure['rowNames'] as $rn)
            {   $res[] = is_array($rn) ? $rn['r_name'] : $rn; // either extract the name from the array or add the name (e.g. the only value).
            }

            return $res;
        }
        else    // return as is:
        {   return $db_structure['rowNames'];
        }
    }

    private function getType_db_structure_rowsToExclude($type_name)
    {
        $actual_type = $this->getTypeByName($type_name);
	    if (!$actual_type)
	    {   return array();
	    }

        $db_structure = $this->getType_db_structure($actual_type);
        return $db_structure['rowsExclude'];
    }

    private function _data_get_rows(&$type)
    {
        $method = $this->getType_modelMethodRows($type);
        return $this->query_datasrc($type, $method);
    }

    private function _data_get_row(&$type, $id)
    {
        $method = $this->getType_modelMethodRow($type);
        return $this->query_datasrc($type, $method, $id);
    }


    // Returns renderer-method name for a list-view.
    private function getType_render_methodName_list(&$type)
    {
        $views = $this->getType_custom_views($type);
        return (isset($views['render_list']) ? $views['render_list'] : NULL);
    }

    // Returns renderer-method name for a single-item-view.
    private function getType_render_methodName_single_item(&$type)
    {
        $views = $this->getType_custom_views($type);
        return (isset($views['render_single_item']) ? $views['render_single_item'] : NULL);
    }
    //----------------------------------------------------------------------------|


    //
    // 4 input params expected:
    //      $generic_type_name  - generic type name.
    //      $db_column_name     - exactly the same row_name from the Type_db_structure, refer to 'r_name' param.
    //      $owner_row_id       - e.g. row for which to set the photo)
    //      $photo_id via POST
    //
    public function setUniversalPhotoId($type_name, $db_column_name, $owner_row_id)
    {
        $photo_id       = intval($this->input->post('photoId'));
        $owner_row_id   = intval($owner_row_id);

        $type_name      = htmlspecialchars($this->input->xss_clean($type_name), ENT_QUOTES);
        $db_column_name = htmlspecialchars($this->input->xss_clean($db_column_name), ENT_QUOTES);

        $actual_type    = $this->getTypeByName($type_name);
        if (!$actual_type)
        {   log_message('error', "setUniversalPhoto: type '$type_name' not supported :(");
            echo json_encode(array('error' => 'Corrupted data has been passed in.'));
            return; // ignore it: some fake/corrupted data has been passed in.
        }

        $db_structure   = $this->getType_db_structure($actual_type);
        $row            = $this->lookupRowByName($db_structure['rowNames'], $db_column_name);
        if (!$row)
        {   log_message('error', "setUniversalPhoto: no valid $db_column_name requested");
            echo json_encode(array('error' => 'Incorrect db_column_name requested.'));
            return; // no valid $db_column_name requested for the particular $type_name. Skip processing.
        }

//        log_message('error', "setUniversalPhoto: row=".print_r($row, true));

        switch ($type_name)
        {
            case 'group_types':
            case 'trainers':
                if (is_array($row))
                {
                    $this->db->set($row['r_name'], $photo_id);
                    $this->db->where('id', $owner_row_id); // the target row is identified by 'id' column.
                    $this->db->update($this->getType_db_structure_tablename($type_name));
                    echo json_encode(array('success' => 'Updated successfully'));
                    return;
                }
            break;

            default:
                // do nothing for others:
                echo json_encode(array('error' => 'Incorrect parameters.'));
                break;
        }
    }


    public function removeSelectedUsers()
    {
       $ids = $this->input->post('chosen_items');
       log_message('error', "USER IDs = $ids");
       $res = true; // false on failure
       print json_encode(array((intval($res))));
    }

    public function setActivityForSelectedUsers()
    {
       $ids             = $this->input->post('chosen_items');
       $activity_type   = $this->input->post('activity_type');
//       log_message('error', "USER IDs = $ids");
       print json_encode(array((intval(123))));
    }

    // todo: rework to SQL instead.
    private function _in_obj_array($needle, &$items)
    {
        foreach ($items as $item)
        {
            if ($needle->id == $item->id)
            {   return true;
            }
        }

        return false;
    }


    // Note:
    //  Prints warning message and returns FALSE in case if access denied.
    //  Otherwise returns true (with no output).
    private function checkAccess($type_name)
    {
        log_message('error', "checkAccess($type_name)");
//        $this->CI->load->library('tank_auth');

        // check whitelisted URLs here:
/*
	    if ($this->CI->tank_auth->is_content_editor())
	    {
            switch ($type_name)
            {
                case 'trainers':
                case 'training_groups':
                case 'lessees':
                case 'group_types':
                {   return true;// allow access.
                }
            }

            echo 'Access denied. Please <a href="javascript:history.go(-1);">step back</a>.';
            return false;
	    }
*/

        return true;
    }

	function show($type_name = NULL, $parameter1 = NULL, $parameter2 = NULL)
	{
	    log_message('error', "CALLING show($type_name, $parameter1, $parameter2)");

	    $actual_type = $this->getTypeByName($type_name);
	    if (!$actual_type)
	    {
//	        redirect('admin/genericTypes');

	        print 'direct entrance here is not allowed.';
	        return;
	    }

        if (!$this->checkAccess($type_name))
        {   return;
        }

        $list_view_renderer = $this->getType_render_methodName_list($actual_type);
        if ($list_view_renderer)
        {
            // call custom listview renderer and exit:
            $this->$list_view_renderer($parameter1, $parameter2);
            return;
        }

	    $data = array();
	    BaseGenericController::base__show($data, $type_name);
        $data['CI']         = $this->CI;

        $data['typeName']       = $type_name; // no need to XSS-filter this.
        $data['title']          = $this->getType_title($actual_type).'s';
        $data['actionUri']      = '../addOrEdit/'.$type_name;
        $data['actionTitle']    = 'add a '.$this->getType_title($actual_type);

        $data['headers']        = $this->getType_headers($actual_type);
        $data['rows']           = $this->_data_get_rows($actual_type);
        $data['rowsNames']      = $this->getType_db_structure_rowNames($type_name);
        $data['rowsExclude']    = $this->getType_db_structure_rowsToExclude($type_name);

        $structured_rows = $this->getType_db_structure_rowNames($type_name, false);
        $this->enrichRowsByLinkedData($actual_type, $data['rows'], $data['rowsExclude'], $structured_rows);

	    $this->load->view("admin/header", $data);
        $this->load->view("admin/view_genericTypes", $data);
        $this->load->view("admin/footer");
	}


    //
	//  Called when generating a LIST of generic items.
	//  Enriches rows which are not in '$rowsExclude' set. Structure is in "$structured_rows" array.
	//
	//  NOTE: this method to be applied onto ROWS LISTS ONLY: e.g. no "item edit" page is allowed to use this method's return values: because it overrides DB-values ("enriches" them).
	//
	private function enrichRowsByLinkedData(&$actual_type, &$rows, &$rowsExclude, &$structured_rows)
	{
        foreach ($structured_rows as $row_name)
        {
            if (is_array($row_name))
            {
                if (!in_array($row_name['r_name'], $rowsExclude))
                {
                    switch ($row_name['r_type'])
                    {
                        case 'combobox_ajax_source':
                        case 'listbox':
                        case 'combobox':
                            // load the data:
                            if (isset($row_name['r_enrich_field']))
                            {
                                $method     = $row_name['r_datasrc'];
                                $constants  = $this->query_datasrc($actual_type, $method);

                                // replace 'r_name' column values by 'r_enrich_field' field from constants.
                                $this->enrichFieldsForRows( $rows,
                                                            $row_name['r_name'],
                                                                $constants,
                                                                $row_name['r_enrich_field']);
                            }
                            break;


                        case 'combobox_ajax_destination':   // This case denotes LINKED data so usual enrichFieldsForRows() call makes no sense.
                                                            // Data dependant enrichment should be called here.
//                        log_message('error', "KKK rows:\n".print_r($rows, true));
                            // load the data:
                            if (isset($row_name['r_enrich_field']))
                            {
                                $method     = $row_name['r_datasrc'];
                                $constants  = $this->query_datasrc($actual_type, $method);
//                                log_message('error', "KKK constants for row_name(".$row_name['r_name'].") :\n".print_r($constants, true));

                                // replace 'r_name' column values by 'r_enrich_field' field from constants.
                                $this->enrichFieldsForRowsLinkedData( $rows,
                                                            $row_name['r_name'],
                                                                $constants,
                                                                $row_name['r_enrich_field'],

                                                                $row_name['r_match_field_row'],
                                                                $row_name['r_match_field_constant']
                                                                );
                            }
                            break;


                        case 'TypeShortSchedule':
                            {
                                $custFieldName = $row_name['r_name'];
                                foreach ($rows as $row)
                                {
                                    $g_trainings        = $this->util_trainings->getGroupTrainings($row->id);
                                    $g_trainings        = $this->util_trainings->sortGroupTrainingsByDays($row->id, $g_trainings);
                                    $row->$custFieldName= $this->util_trainings->visualizeShortSchedule($g_trainings, $row->id);
                                }
                            }
                            break;

                        case 'TypeBranchNames':
                            {
                                $custFieldName = $row_name['r_name'];
                                foreach ($rows as $row)
                                {
                                    $row->$custFieldName= $this->util_trainings->getGroupTrainingsBranchesNames($row->id);
                                }
                            }
                            break;

                        case 'datetime_picker':
                            // do nothing.
                                    //'date'       => date('Y-m-d H:i:s', strtotime($news_date));
//                                    date('Y-m-d H:i:s');
                            break;

                        case 'textarea':
                            // do nothing.
                            break;

                        case 'client_groups_control':
                            // replace 'r_name' columns values by actual data:
                            if ('client_groups' == $row_name['r_name'])
                            {   $this->enrichClientGroupIdsByGroupNames($rows, $row_name['r_name']);
                            }
                            else
                            {   print 'WARNING: field "'.$row_name['r_name'].'" (type "'.$row_name['r_type'].'") is unsuppported by enricher.';
                            }
                            break;
                        default:
                            print 'WARNING: field "'.$row_name['r_name'].'" (type "'.$row_name['r_type'].'") is unsuppported by enricher222.';
                            break;
                    }
                }
            }
        }
	}


	private function enrichClientGroupIdsByGroupNames(&$rows, $row_field_name)
	{
	    $client_groups = $this->util_trainings->getTrainingGroups();
	    foreach ($rows as $row)
	    {
	        $res = $this->util_trainings->getClientGroups($row->id);
	        $str = '<ul id="inner" style="margin-left: 14px;">';
	        foreach ($res as $item)
	        {   $str .= "<li><a href='".base_url()."admin/genericTypes/addOrEdit/training_groups/".$item->group_id."'>$item->group_name</a></li>";
	        }
            $str .= '</ul>';

	        $row->client_groups = $str;
	    }
	}

    //
    // Compares ANY field named '$row_field_name' to "$constants->id" only!!!
    // This is from assumption that constants are vocabulary and actually keys for "$rows->$row_field_name" column.
    //
	private function enrichFieldsForRows(&$rows, $row_field_name, &$constants, $constant_field_name)
	{
	    foreach ($rows as $row)
	    {
	        foreach ($constants as $constant)
	        {
//                log_message('error', "CONSTANT = \"".print_r($constant, true).'"');

                if ($row->$row_field_name == $constant->id)
                {
                    // replace ID by value:
                    $row->$row_field_name = $constant->$constant_field_name;
                    break;
                }
	        }
	    }
	}

    //
    //  NOTE: to be used for cases when input $rows refer to data DEPENDANT on other FIELD.
    // Compares ANY field named '$row_field_name' to "$constants->id" only!!!
    // This is from assumption that constants are vocabulary and actually keys for "$rows->$row_field_name" column.
    //
	private function enrichFieldsForRowsLinkedData(&$rows, $row_field_name, &$constants, $constant_field_name, $row_dependant_field_name, $constant_dependant_field_name)
	{
	    foreach ($rows as $row)
	    {
	        foreach ($constants as $constant)
	        {
                if (    ($row->$row_field_name == $constant->id) &&
                        ($row->$row_dependant_field_name == $constant->$constant_dependant_field_name) // this line describes THE DEPENDANT fields' NAMES to be verified!
                    )
                {
                    // replace ID by value:
                    $row->$row_field_name = $constant->$constant_field_name;
                    break;
                }
	        }
	    }
	}

    // Returns StdClass object with partially (!) initialized fields.
    // Allows customization of new item field(s) if required.
	private function create_new_item($type_name)
	{
	    switch ($type_name)
	    {
	        case 'scenario':
	        {
	            /*
	            $v = new stdClass();

	            // Step # 1: Create all the *fields* first: refer to "fill_in_form_controls($typeName, $record, &$data)" method.
	            // Step # 2: Then custome values for some of the fields.
	            // Step # 3: And finally return that new object. Voila!
                return $v;
                */
            }
	    }

	    return NULL;
	}

    function addOrEdit($type_name = NULL, $id = -1, $extra_parameter = -1)
    {
        $actual_type = $this->getTypeByName($type_name);
	    if (!$actual_type)
	    {   redirect('admin/genericTypes');
	    }

//        log_message('error', "addOrEdit():");
        if (!$this->checkAccess($type_name))
        {   return;
        }

        $data = array();
        $data['CI']         = $this->CI;
        $data['breadCrumbs']= array();

        // common data:
        $data['typeName']   = $type_name; // no need to XSS-filter this.
        $data['title']      = 'add a '.$this->getType_title($actual_type);


        $row = NULL;
        if ($id > 0)
        {
            $row = $this->_data_get_row($actual_type, $id);

            if ($row)
            {  $data['title']      = sprintf("edit %s %s", $this->getType_title($actual_type), property_exists($row, 'name') ? $row->name : '# '.$row->id);
            }
        }
        else
        {   // init by some predefined values instead:
            $row = $this->create_new_item($type_name);
        }

        $this->fill_in_form_controls($type_name, $row, $data);

        $item_renderer = $this->getType_render_methodName_single_item($actual_type);
        if ($item_renderer)
        {
            // call custom item renderer:
            $data['extra_data'] = $this->$item_renderer($type_name, $row);
        }

        // call *after* the $row has been intialised:
        BaseGenericController::base__addOrEdit($type_name, $row, $data);


	    $this->load->view("admin/header", $data);
        $this->load->view("admin/view_genericTypes_add_edit", $data);
        $this->load->view("admin/footer");
    }


    //
    // A wrapper for complext data structures: enquires the '$method' for a data.
    //
    private function query_datasrc($actual_type, $method, $parameter = NULL)
    {
        $model_name = $this->getType_modelName($actual_type);
        $this->CI->load->model($model_name);

        return $this->CI->$model_name->$method($parameter);
    }

    private function createSelectableCombo(&$rows, &$row_name, $current_value)
    {
        return $this->CI->utilitar->createSelectWithSelection(  $rows,
                                                                GenericTypes::CONTROL_PREFIX.$row_name['r_name'],   // id
                                                                GenericTypes::CONTROL_PREFIX.$row_name['r_name'],   // name
                                                                isset($row_name['r_HTML_attribs']) ?
                                                                    $row_name['r_HTML_attribs'] : "size='5' style='width:500px;'", // extra attribs
                                                                $current_value,
                                                                !isset($row_name['r_no_default_string']));
    }

    //
    //  The control to be inited by "$current_value" (if any - it refers to the row value being *edited*. Is null for item just being *created*).
    //  IMPORTANT!: besides '$current_value' there is a $row_name['r_default_value'] which is used when no '$current_value' is defined (e.g. we're dealing with brand new, unsaved, item).
    //
    private function create_HTML_control($typeName, &$actual_type, &$row_name, $current_value, &$row)
    {
        switch ($row_name['r_type'])
        {
            case 'listbox':
            case 'combobox':
            case 'combobox_ajax_source':
//                log_message('error', "create_HTML_control(typeName=$typeName, actual_type=$actual_type, row_name=$row_name, current_value=$current_value, row=".print_r($row, true)."\nraw data=".print_r($row_name, true));
            
                $method = $row_name['r_datasrc'];
                $rows   = $this->query_datasrc($actual_type, $method);
                $combo  = $this->createSelectableCombo($rows, $row_name, $current_value);

                $res = array($combo);
                if ('combobox_ajax_source' == $row_name['r_type'])
                {   $res[] = $row_name['r_type'];
                    $res[] = $row_name['r_destination_control_id'];
                    $res[] = (null != $row) ? $row->$row_name['r_destination_control_id'] : -1; // actual VALUE is here
                }
                return $res;
                break;

            case 'combobox_ajax_destination':
//                log_message('error', "create_HTML_control(typeName=$typeName, actual_type=$actual_type, row_name=$row_name, current_value=$current_value, row=".print_r($row, true)."\nraw data=".print_r($row_name, true));

                $rows       = array(); // no data needed since it will be retrieved via AJAX request later onPageLoad().

                $combo = $this->createSelectableCombo($rows, $row_name, $current_value);

                return array($combo);
                break;

            case 'datetime_picker':
                $control_id = GenericTypes::CONTROL_PREFIX.$row_name['r_name'];
                $date_formatted = $current_value ? date('d.m.Y', strtotime($current_value)) : date('d.m.Y');
                return array(
                        '<input type="text" class="hasDatepicker" maxlength="10" size="10" id="'.$control_id.'" name="'.$control_id.'" value="'.$date_formatted.'" />',
                        'datetime_picker' // 2nd item of the array marks the *type* of the control being returned: required for Javascript-handling.
                            );
                break;

            case 'checkbox':
                $control_id = GenericTypes::CONTROL_PREFIX.$row_name['r_name'];
                return array(
                        '<input type="checkbox" size="8" id="'.$control_id.'" name="'.$control_id.'" '.($current_value > 0 ? 'checked':'').' />',
                        'checkbox' // 2nd item of the array marks the *type* of the control being returned: required for Javascript-handling.
                            );
                break;

            case 'textarea':
                $str = '<textarea rows="7" cols="80" name="'.GenericTypes::CONTROL_PREFIX.$row_name['r_name'].'">%s</textarea>';
                if (isset($row_name['r_default_value']) && strlen($current_value) <= 0)
                {   return array(
                        sprintf($str, $row_name['r_default_value'])
                            );
                }
                else
                {
                    return array(
                        sprintf($str, $current_value)
                            );
                }
                break;

            case 'client_groups_control':
                return array(
                        '<input type=hidden name="'.
                                                    GenericTypes::CONTROL_PREFIX.$row_name['r_name'].'" id="'.
                                                    GenericTypes::CONTROL_PREFIX.$row_name['r_name'].'" value="'.$current_value.'">',
                        'hidden' // 2nd item of the array marks the *type* of the control being returned.
                            );
                break;

            case 'hidden':
                return array(
                        $current_value,
                        'hidden', // 2nd item of the array marks the *type* of the control being returned.
                            );
                break;

            case 'choosePhoto':
                if (!$row)
                {   return array('<p style="background-color: yellow;"><strong>Внимание:</strong> список изображений появится только после сохранения этой записи.</p>');
                }

                $photo          = NULL;
                $photo_id       = intval($current_value);
                if ($photo_id > 0)
                {   $photo      = $this->CI->utilitar->getPhoto($photo_id);
                }


                $album_id       = $this->query_datasrc($actual_type, $row_name['r_album_getter']);
                $album          = $this->CI->utilitar->getAlbum($album_id);
                $photos         = $this->CI->utilitar->getAlbumPhotos($album_id);

                $group_type_id  = $row->id;

                $tmp = array(   'photo'     => $photo,
                                'photos'    => $photos,
                                'current_photo_id'  => $row->photo_id,
                                'actionUrl' => base_url().'admin/genericTypes/'.$row_name['r_request_handler'].'/'.$typeName.'/'.$row_name['r_name'].'/'.$group_type_id,
                                'albumDescription'  => ($album? $album->description :''),
                            );
                            
                $control    = $this->load->view('admin/div_choosePhoto', $tmp, true);
                return array($control);
                break;

            case 'TypeShortSchedule':
            case 'TypeBranchNames':
                return NULL; // no need to show this on item's addOrEdit page.
                break;

            default: // do nothing.
                break;
        }

        return '<strong>HTML control type <b>"'.$row_name['r_type'].'</b>" is not supported!</strong>';
    }

    //
    // NOTE:
    //  if no "$record" is passed then specific form control inited by '' string.
    //
    // WARNING:
    //  this is erratic for listboxes/comboboxes!
    //
    private function fill_in_form_controls($typeName, $record, &$data)
    {
        $custom_controls = array();
        $custom_control_names = array();

        $rowNames = $this->getType_db_structure_rowNames($typeName, false);
        $actual_type = $this->getTypeByName($typeName);
	    if (!$actual_type)
	    {   return;// todo: implement error handling here!!!
	    }

        foreach ($rowNames as $row_name)
        {
            if (is_array($row_name))
            {
                $control_name = $row_name['r_name'];
                $html_control = $this->create_HTML_control($typeName, $actual_type, $row_name, ($record) ? (isset($record->$control_name) ? $record->$control_name :'hidden'): NULL, $record);
                if (!$html_control)
                {   continue; // skip generating HTML-control if nothing returned.
                }

                $custom_controls[GenericTypes::CONTROL_PREFIX.$control_name] = $html_control;


                // todo: this is a patch: need to pass human-printed field name instead.
                $custom_control_names[GenericTypes::CONTROL_PREFIX.$control_name] = $control_name;
            }
            else
            {   // this is just for plain textboxes:
                $custom_controls[GenericTypes::CONTROL_PREFIX.$row_name] = ($record) ? $record->$row_name : '';

                // todo: this is a patch: need to pass human-printed field name instead.
                $custom_control_names[GenericTypes::CONTROL_PREFIX.$row_name] = $row_name;
            }
        }

//        print '<hr>';
//        print_r($custom_controls);
//        print '<hr>';
//        log_message('error', "controls=".print_r($custom_controls, true)."\ncontrol_names=".print_r($custom_control_names, true));
        
        $data['custom_controls']        = $custom_controls;
        $data['custom_control_names']   = $custom_control_names;
    }

    function doAddEdit()
    {
        $recordId   = intval($this->input->post('recordId'));
        $typeName   = $this->input->post('typeName');

        $actual_type = $this->getTypeByName($typeName);
	    if (!$actual_type)
	    {   log_message('error', "WARNING: doAddEdit(): unrecognized type passed ($typeName)");
	        return;
	    }

        $data = array();
        $custom_controls = array();

        // collect the data posted:
        $hiddens = array();
        $rowNames = $this->getType_db_structure_rowNames($typeName, false);
//        log_message('error', "genericTypes::doAddEdit():POST = ".print_r($_POST, true));
//        log_message('error', "rowNames=".print_r($rowNames, true));

        // for plain types just use corresponding db_row_names, otherwise parse custom controls and handle each one separately:
        foreach ($rowNames as $row)
        {
            $__rowname  = '';
            $__rowtype  = '';
            $__postprocess_routine = '';


            if (is_array($row))
            {   $__rowname  = $row['r_name'];
                $__rowtype  = $row['r_type'];
                if (
                    (0 == strcmp($__rowtype, 'choosePhoto'))    ||
                    (0 == strcmp($__rowtype, 'TypeShortSchedule')) ||
                    (0 == strcmp($__rowtype, 'TypeBranchNames'))
                )
                {   continue; // skip adding this value to POSTed data because it get filled by separate JS-request (AJAX). Here comes NULL always from the form posted.
                }

                if (isset($row['r_postprocess_routine']))
                {   $__postprocess_routine = $row['r_postprocess_routine'];
                }
            }
            else
            {   $__rowname  = $row;
            }

            $data[$__rowname] = $this->input->post(GenericTypes::CONTROL_PREFIX.$__rowname);
            log_message('error', "genericTypes::doAddEdit() rowname=$__rowname, type=$__rowtype, value=".$data[$__rowname].", __postprocess_routine=$__postprocess_routine");

            // collect custom controls data (if posted):
            switch ($__rowtype)
            {
                case 'datetime_picker': // datetime pickers store their data not in themselves but in hidden controls named w/out prefix "GenericTypes::CUSTOM_CONTROL_NAME".
                    log_message('error', "data for datetime_picker: ".print_r($this->input->post(GenericTypes::CONTROL_PREFIX.$__rowname), true));
                    if ($this->input->post(GenericTypes::CONTROL_PREFIX.$__rowname))
                    {
                        $custom_controls[$__rowtype] = array(   'row_name'  => $__rowname,
                                                                'value'     => $this->input->post(GenericTypes::CONTROL_PREFIX.$__rowname));
                    }
                    break;

                case 'checkbox': // checkboxes store their data not in themselves but in hidden controls named w/out prefix "GenericTypes::CUSTOM_CONTROL_NAME".
                    // greco: particularly CHECKBOXES works incorrectly!

                    log_message('error', "data for checkboxes from POST (".GenericTypes::CONTROL_PREFIX.$__rowname."): ".print_r($this->input->post(GenericTypes::CONTROL_PREFIX.$__rowname), true));
                    log_message('error', "POST verify data by posted variable: ".print_r(GenericTypes::CONTROL_PREFIX.$__rowname, true));
                    if ($this->input->post(GenericTypes::CONTROL_PREFIX.$__rowname))
                    {
                        log_message('error', "POST for CHECKBOX: value set to ".print_r($this->input->post(GenericTypes::CONTROL_PREFIX.$__rowname), true));
                        $custom_controls[$__rowtype] = array(   'row_name'  => $__rowname,
                                                                'value'     => $this->input->post(GenericTypes::CONTROL_PREFIX.$__rowname));
                    }
                    else
                    { // if data has not been posted then it is set to "0":
                        $custom_controls[$__rowtype] = array(   'row_name'  => $__rowname,
                                                                'value'     => 0);
                    }
                    break;

                case 'client_groups_control':
                    $hiddens[] = array( 'value'                 => $data[$__rowname],//$this->input->post(GenericTypes::CUSTOM_CONTROL_NAME.GenericTypes::CONTROL_PREFIX.$__rowname),
                                        'postprocess_routine'   => $__postprocess_routine,
                                        );
                    unset($data[$__rowname]); // exclude it: hiddens to be processed in a separate way (later).
                    break;

                case 'hidden':
                    log_message('error', "EXCLUDE IT: HIDDENS TO BE PROCESSED IN A SEPARATE WAY (LATER). Posted data = ".$data[$__rowname]);
                    
                    unset($data[$__rowname]); // exclude it: hiddens to be processed in a separate way (later).
                    $hiddens[] = array( 'value'                 => $this->input->post(GenericTypes::CUSTOM_CONTROL_NAME.GenericTypes::CONTROL_PREFIX.$__rowname),
                                        'postprocess_routine'   => $__postprocess_routine,
                                        );

                    log_message('error', "HIDDEN WHAT POSTED111? \n".print_r($this->input->post(GenericTypes::CUSTOM_CONTROL_NAME.GenericTypes::CONTROL_PREFIX.$__rowname), true));
                    log_message('error', "HIDDEN WHAT POSTED222? \n".print_r($this->input->post(GenericTypes::CUSTOM_CONTROL_NAME.GenericTypes::CONTROL_PREFIX.$__rowname), true));
                    break;

                default: // handled by default just fine.
                    break;
            }
        }

//        log_message('error', "HIDDENS=".print_r($hiddens, true));

        $this->convertPOSTedData($data, $custom_controls);

        //----------------------------------------------------------------------------+
        // POST's custom data handling for *composite* edit-pages (e.g. like for "training_groups"):
        switch ($typeName)
        {
            case 'lessees':
            case 'training_groups':
                $recordId   = Trainings_schedule::formDoAddEdit();
                if (is_string($recordId)) // then this contains the error message:
                {   log_message('error', "genericTypes::doAddEdit(): error processing custom POST for passed '$typeName' type. Err msg: ".$recordId);
                    return;
                }
                else
                    log_message('error', "genericTypes::doAddEdit(): success in processing custom POST for passed '$typeName' type!!!");

                break;


            case 'berk_rooms':
                $room_id            = $data['id'];
                $organization_id    = $data['organization_id'];
                $user_id            = $data['local_user_id_DUMMY'];

                $this->CI->load->model('util_berkana_data');
                $this->CI->util_berkana_data->assignCabinetAccountToRoom($room_id, $organization_id, $user_id);
                break;

            default:
                log_message('error', "genericTypes::doAddEdit(): Custom data handling: UNRECOGNIZED TYPE ('$typeName') or handling is not required.");
                break;
        }
        //----------------------------------------------------------------------------|

        $redirect_to = base_url()."index.php/admin/genericTypes/show/$typeName";

        $data['typeName']   = $typeName;
        $data['tableName']  = $this->getType_db_structure_tablename($typeName);
        $data['backUrl']    = $redirect_to;

        $postprocessing_is_required = (count($hiddens) > 0);
        $row_id = BaseGenericController::base__doAddEdit($recordId, $data, $postprocessing_is_required);

        // if postprocessing is required:
        if ($postprocessing_is_required)
        {
            foreach ($hiddens as $hidden)
            {   $routine = $hidden['postprocess_routine'];
                $this->$routine($row_id, $hidden['value']); // call a func and pass all params to it.
            }

            // finally do a redirect:
            BaseGenericController::base__doRedirect($typeName, $redirect_to, $row_id);
        }
    }


    //
    // Function performs *data conversion* of POSTed data into the format that's suitable for DB.
    // E.g. custom controls (checkbox, dateime picker, etc.) require data conversion.
    //
    private function convertPOSTedData(&$data, &$custom_controls)
    {
        log_message('error', 'post-process: custom_controls ='.print_r($custom_controls, true));

        foreach ($custom_controls as $key => $item)
        {
            switch ($key)
            {
                case 'datetime_picker':
                    $data[$item['row_name']] = date('Y-m-d H:i:s', strtotime($item['value']));
                    break;

                case 'checkbox':
                    $data[$item['row_name']] = $item['value'] ? 1 : 0; // specifically for checkbox: if no value passed then it should be set to FALSE, otherwise TRUE.
                    break;

                default:
                    log_message('error', 'WARNING: convertPOSTedData(): unsupported type "'.$custom_controls[$key].
                        '" for key = '.print_r($key, true).
                        ', value = '.print_r($item['value'], true));
                    break;
            }
        }
    }

    function delete($type_name, $rowId)
    {
//        log_message('error', "DELETE GENERIC for table $tableName");
        $tableName  = $this->getType_db_structure_tablename($type_name);
        if (!$tableName)
        {   redirect('admin/genericTypes');
        }

        $onDelete_table_row_names = $this->getType_db_structure_onDeleteTableRowNames($type_name);
        if ($onDelete_table_row_names)
        {
            log_message('error', "DELETE GENERIC: call linked data remove! type_name=$type_name");
            $this->db->where($onDelete_table_row_names['column_name'], $rowId); // 2nd element denotes "column_name". The target row is identified by 'id' column.
            $this->db->delete($onDelete_table_row_names['table_name']); // 1st element denotes "table name".
        }

        BaseGenericController::base__delete($type_name, $tableName, $rowId);
    }
}