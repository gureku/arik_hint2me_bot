<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

//-----------------------------------------------------------+
//  Copyright (c) 2010 
//
//  Author: garegin ghazaryan (greco.el)
//  File creation date:  24.07.2010 23:04:40
//  Ver. 0.2
//
//-----------------------------------------------------------|

require_once(APPPATH . '/controllers/admin/SecuredMatrix.php');


abstract class Secured extends CI_Controller
{
    // returns 'admin', 'client' or any other kind of access levev describing string.
    abstract protected function getNameSpace();


    function __construct()
    {
        parent::__construct();

        $this->load->helper(array('url', 'language'));
        $this->lang->load('data');

		$this->load->library('tank_auth');
    }

    // returns the class_name of the extender's class:
    private final function getClassName()
    {
        return get_class($this);
    }

    public final function isAllowed($method_name)
    {
        $this->load->library('tank_auth');
        $uid        = $this->tank_auth->get_user_id();
        $user_roles = $this->tank_auth->get_user_roles($uid);

        return SecuredMatrix::is_allowed(   $user_roles,
                                            $this->getNameSpace(),
                                            $this->getClassName(),
                                            $method_name    );
    }

    public final function isAllowedWithRoles(&$user_roles, $method_name)
    {
        return SecuredMatrix::is_allowed(   $user_roles,
                                            $this->getNameSpace(),
                                            $this->getClassName(),
                                            $method_name    );
    }

    public final function warnDenied($with_step_back = false)
    {
        log_message('error', "warnDenied(): ACCESS DENIED!!");
        print 'Access denied.';
        if ($with_step_back)
        {   print ' Please <a href="javascript:history.go(-1);">step back</a>.';
        }
    }

    function isAuthorized()
    {
        return $this->tank_auth->is_logged_in();
    }
}