<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . '/controllers/admin/AdminOnly.php');
require_once(APPPATH . '/controllers/admin/Secured.php');

//class Admin extends MembersParent {
class Mnt extends AdminOnly /*Secured*/
{
    private $CI = NULL;

    function __construct()
    {
        parent::__construct();

        $this->CI = &get_instance();
        $this->CI->load->model('utilitar');
    }

    function index()
    {
        if (!$this->isAllowed(__FUNCTION__))
        {   $this->warnDenied(true);
            return;
        }

        $data = array();
        $data['title']                  = 'Common Options';
        $data['berk_trainers_groupId']  = $this->utilitar->getBerkanaTrainersGroupId();
        $data['news_albumId']           = $this->utilitar->getNews_AlbumId();
        $data['group_types_albumId']    = 0;//$this->util_trainings->getGroupTypes_AlbumId();
        $data['trainers_albumId']       = 0;//$this->util_trainings->getTrainers_AlbumId();
        $data['welcome_words']          = $this->utilitar->getWelcomeWords();

        $data['local_roles']            = $this->utilitar->wrapToTypesArray($this->utilitar->getRolesList(), -1, '...роль не указана!');

        $albums = $this->utilitar->getAlbums(true);
        foreach ($albums as $album)
        {   $album->name = $album->title;
        }
        $data['albums'] = $this->utilitar->wrapToTypesArray($albums, -1, '...альбом не выбран!');

        if ($this->session->flashdata('saved_successfully'))
        {	// customize the view for a particular thought:
            $data['notification_message'] = 'Опции были сохранены успешно.';
        }
        $this->load->view("admin/header", $data);
        $this->load->view("admin/view_common_options", $data);
        $this->load->view("admin/footer");
    }

    function update()
    {
        if (!$this->isAllowed(__FUNCTION__))
        {   $this->warnDenied(true);
            return;
        }

//        $news_album         = intval($this->input->post('news_album'));
//        $this->utilitar->setNews_AlbumId($news_album);

        $this->session->set_flashdata('saved_successfully', 'true');
        redirect('admin/mnt');
    }
}