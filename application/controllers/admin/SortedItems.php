<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . '/controllers/admin/AdminOnly.php');

class SortedItems extends AdminOnly {

    function __construct()
    {
        parent::__construct();

        $this->load->model('utilitar_db');
        $this->load->model('util_sorted_items');
    }

    function index()
    {
        /*if (!$this->isAllowed(__FUNCTION__))
        {   $this->warnDenied(true);
            return;
        }*/

        redirect('admin/sortedItems/show/1');
    }

    function show($item_type)
    {
        $item_type  = intval($item_type);

        $data               = array();
        $data['title']      = 'Manage items order of type #'.$item_type;

        $data['item_type']  = $item_type;
        $data['rows']       = $this->util_sorted_items->getSortedItems($item_type);

        $this->load->view("admin/header", $data);

        // post-process retrieved data:
        switch ($item_type)
        {
/*
            case Util_sorted_items::SI_PROMO_BLOCK:
            {
                break;
            }
*/
            case Util_sorted_items::SI_SHOPS:
            {
                foreach ($data['rows'] as $shop)
                {
                    $shop->name .= ' ('.$shop->cityName.')'; // enrich it to ease administering.
                }
                break;
            }

            default:
            {   echo "sorted items type not handled by enriches";
                break;
            }
        }

        $this->load->view("admin/view_sortedItemsMapping", $data);

        $this->load->view("admin/footer");
    }


	function setObjectsOrder()
	{
/*	    if (!$this->isAllowed(__FUNCTION__))
        {   $this->warnDenied();
            return;
        }
*/

        $item_type      = intval($this->input->post('item_type'));
        $photosOrder    = $this->input->post('photosOrder'); // greco: are these items sorted?
        
        $photoIds       = explode(',', $photosOrder);

        $i = 0;
        foreach ($photoIds as $photoId)
        {
            $data   = array('sort_order' => $i++,  'item_type' => $item_type, 'item_id' => intval($photoId));
            $this->utilitar_db->insert_or_update2('item_type', $item_type, 'item_id', intval($photoId), 'sorted_items', $data);
        }

        redirect('admin/sortedItems/show/'.$item_type);
	}
}