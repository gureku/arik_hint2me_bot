<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . '/controllers/admin/AdminOnly.php');

class Albums extends AdminOnly {

    function __construct()
    {
        parent::__construct();
    }


	function index()
	{
        if (!$this->isAllowed(__FUNCTION__))
        {   $this->warnDenied(true);
            return;
        }


        $data = array();
	    $this->prepareData($data);

	    $this->load->view("admin/header", $data);
        $this->load->view("admin/view_albums", $data);
        $this->load->view("admin/footer");
	}

    // Same as index() but with no menu, etc. - just actual data.
	public function stripData($productId = NULL)
	{
	    if (!$this->isAllowed(__FUNCTION__))
        {   $this->warnDenied(true);
            return;
        }
        
	    $data = array();
	    $data['productId'] = $productId; // pass it forward.

	    $this->prepareData($data);
	    $this->load->view("admin/view_albums", $data);
	}

/*

    class UploadFileXhr {
    	function save($path){
    		$input = fopen("php://input", "r");
    		$fp = fopen($path, "w");
    		while ($data = fread($input, 1024)){
    			fwrite($fp,$data);
    		}
    		fclose($fp);
    		fclose($input);
    	}
    	function getName(){
    		return $_GET['name'];
    	}
    	function getSize(){
    		$headers = apache_request_headers();
    		return (int)$headers['Content-Length'];
    	}
    }

    class UploadFileForm {
      function save($path){
    		move_uploaded_file($_FILES['qqfile']['tmp_name'], $path);
    	}
    	function getName(){
    		return $_FILES['qqfile']['name'];
    	}
    	function getSize(){
    		return $_FILES['qqfile']['size'];
    	}
    }

    function handleUpload(){
    	$uploaddir = 'uploads/';
    	$maxFileSize = 100 * 1024 * 1024;

    	if (isset($_GET['qqfile'])){
    		$file = new UploadFileXhr();
    	} elseif (isset($_FILES['qqfile'])){
    		$file = new UploadFileForm();
    	} else {
    		return array(success=>false);
    	}

    	$size = $file->getSize();
    	if ($size == 0){
    		return array(success=>false, error=>"File is empty.");
    	}
    	if ($size > $maxFileSize){
    		return array(success=>false, error=>"File is too large.");
    	}

    	$pathinfo = pathinfo($file->getName());
    	$filename = $pathinfo['filename'];
    	$ext = $pathinfo['extension'];

    	// if you limit file extensions on the client side,
    	// you should check file extension here too

    	while (file_exists($uploaddir . $filename . '.' . $ext)){
    		$filename .= rand(10, 99);
    	}

    	$file->save($uploaddir . $filename . '.' . $ext);

    	return array(success=>true);
    }
*/



	public function strippedChooseAlbums($estateId)
	{
	    if ($estateId <= 0)
	    {   redirect(base_url().'admin/mnt');
	        return;
	    }

        // no trailing slash:
        $method_name = '../reObjects/setEstateImage'; // must accept 2 params: 1) estate_id; 2) album_id.

        $title = 'Click album to assign it to real estate object';

        $this->strippedChooseAlbumsExtended($title, $estateId, $method_name);
	}

	public function strippedChooseAlbumsBuildingProgress($estateId)
	{
	    if ($estateId <= 0)
	    {   redirect(base_url().'admin/mnt');
	        return;
	    }

        // no trailing slash:
        $method_name = '../reObjects/setAlbumBuildingProgress'; // must accept 2 params: 1) estate_id; 2) album_id.

        $title = lang('const.choose.build');

        $this->strippedChooseAlbumsExtended($title, $estateId, $method_name);
	}

	public function strippedChooseAlbumsBuildingDecoration($estateId)
	{
	    if ($estateId <= 0)
	    {   redirect(base_url().'admin/mnt');
	        return;
	    }

        // no trailing slash:
        $method_name = '../reObjects/setAlbumBuildingDecoration'; // must accept 2 params: 1) estate_id; 2) album_id.

        //$title = lang('const.choose.decoration').' <a href="#" onclick="javascript:divClear(); return false;">hide</a>';
        $title = lang('const.choose.decoration');

        $this->strippedChooseAlbumsExtended($title, $estateId, $method_name);
	}

	private function strippedChooseAlbumsExtended($title, $estateId, $method_name)
	{
	    $data = array();
	    $data['title'] = $title;
	    $data['estate_id'] = $estateId; // pass it forward.

        $CI = &get_instance();
        $CI->load->model('utilitar');
        $albums = $CI->utilitar->getAlbums(true);


	    $albumsEnriched = array();
	    foreach ($albums as $album)
	    {
	        $item = $album;
	        $item->images_count = $CI->utilitar->my_count_all('id', 're_photo', " `album_id` = $album->id ");
	        $albumsEnriched[] = $item;
	    }

	    $data['albums'] = $albumsEnriched;
	    $data['method_name'] = $method_name;

        $this->load->view("admin/view_albums_choose", $data);
	}

	private function viewSlideshow($albumId)
	{
        $CI = &get_instance();
        $CI->load->model('utilitar');

	    $data = array();
	    $data['read_only'] = TRUE;
        $data['photos'] = $CI->utilitar->getAlbumPhotos($albumId);
        $data['link_edit']  = base_url('admin/albums/'.$albumId, 'https');
        $this->load->view('admin/view_album_slideshow', $data);
	}


    private function prepareData(&$data)
    {
        $productId = NULL;
        if (isset($data['productId']))
        {   $productId = $data['productId'];
        }

        $CI = &get_instance();
        $CI->load->model('utilitar');

        $albums = $CI->utilitar->getAlbums(true);

        $img_edit = base_url().'images/admin/edit.gif';
        $img_delete = base_url().'images/admin/del.gif';

        $compoundData = array();
        $linkTitle = lang('admin.albums.viewphotos');
        foreach ($albums as $row)
        {
            $ss = "Вы уверены, что хотите удалить альбом \'".$row->title."\'?";

            $compoundData[] = array(
                'ajaxString' => $CI->utilitar->composeAjaxQueryString(base_url()."admin/albums/editStripped/$row->id/$productId", $row->public_access ? $row->title : '(*) '.$row->title),
                'editString' => "<a href='".base_url()."admin/albums/$row->id'>".lang('admin.edit')." <img src='$img_edit'></a>",
                'deleteString' => "<a href='".base_url()."admin/albums/delete/$row->id' onclick=\"return confirm('$ss')\"><font color=red>".lang('admin.delete')."</font> <img src='$img_delete'></a>",
//                'title' => $row->public_access ? $row->title : '(*) '.$row->title,
             );
        }

        $data['title'] = lang('admin.menu.albums.manage');
        $data['compoundData'] = $compoundData;
        $data['pagename'] = __CLASS__;
    }

    private function returnPostedFilesList()
    {
        // expected file names:
        $filenames = array('userfile1', 'userfile2', 'userfile3', 'userfile4', 'userfile5');

        $res = array();
        foreach ($filenames as $name)
        {
            if (isset($_FILES[$name]))
            {
                if ($_FILES[$name]['size'] > 0)
                {
                   $res[] = $name;
                }
            }
        }

        return $res;
    }
    

    public function delete($albumId)
    {
        if (!$this->isAllowed(__FUNCTION__))
        {   $this->warnDenied(true);
            return;//redirect('admin/mnt');
        }

        if ($albumId < 0)
        {   redirect("admin/albums");
            return;
        }

        $CI = &get_instance();
        $CI->load->model('utilitar');

        $photos = $CI->utilitar->getAlbumPhotos($albumId);

        // delete photo files from album:
        $this->db->where('album_id', $albumId);
        $this->db->delete('photo');

        // delete the album itself:
        $this->db->delete('albums', array('id' => $albumId));

        // physically delete photo files:
        foreach ($photos as $row)
        {
            $CI->utilitar->deleteFile($row->photo_path);
            $CI->utilitar->deleteFile($row->thumbnail_path);
        }

        redirect("admin/albums");
    }

    public function deletePhoto($photoId)
    {
        if (!$this->isAllowed(__FUNCTION__))
        {   $this->warnDenied(true);
            return;//redirect('admin/mnt');
        }

        $CI = &get_instance();
        $CI->load->model('utilitar');

        $row = $CI->utilitar->getPhoto($photoId);
        if ($row)
        {
            // delete image files:
            $CI->utilitar->deleteFile($row->photo_path);
            $CI->utilitar->deleteFile($row->thumbnail_path);
        }

        // delete the DB record:
        $this->db->delete('photo', array('id' => $photoId));


//        redirect("admin/albums/".$row->album_id);

        $data = array();
        $data['title'] = 'Photo deleted';
        $data['backUrl'] = base_url().'admin/albums/';
        $data['msg'] = "Photo deleted";

        redirect('admin/albums');

//        $this->load->view("admin/view_success", $data);
    }


    public function add()
    {
        if (!$this->isAllowed(__FUNCTION__))
        {   $this->warnDenied(true);
            return;//redirect('admin/mnt');
        }

        
        $data = array();
        $data['title']          = sprintf(lang('admin.tmpl.create'), lang('admin.category.album'));
	    $data['actionType']     = 'doAdd';
	    $data['albumTitle']     = '';
	    $data['albumDescription']  = '';
	    $data['productId']      = NULL; // pass valid productId if you want to assign a photo to it.
	    $data['albumId']        = -1;
	    $data['photos']         = array();
	    $data['public_access']  = 0;


        /*
	    $data['hid_ownIdField'] = "<input type='hidden' name='pId' value='$good->id'>";
	    */

        $data['date_created']   = date('d.m.Y');
        $data['backUrl']        = base_url().'admin/albums';

        $data['pagename'] = __CLASS__;

        $this->load->view("admin/header", $data);
        $this->load->view("admin/view_album_add_edit", $data);
        $this->load->view("admin/footer");
    }

    // Note: the method is ROUTE-d.
	public function edit($id = -1)
	{
        if (!$this->isAllowed(__FUNCTION__))
        {   $this->warnDenied(true);
            return;//redirect('admin/mnt');
        }

	    if (intval($id) <= 0)
	    {   redirect("admin/albums/add");
	        return;
	    }

	    $CI = &get_instance();
        $CI->load->model('utilitar');
        $row = $CI->utilitar->getAlbum($id);
        if (!$row)
	    {   redirect("admin/albums/add");
	        return;
	    }

        $data = array();
	    $data['title']      = sprintf(lang('admin.tmpl.edit').' "'.$row->title.'"', lang('admin.category.album'));
	    $data['actionType'] = 'doEdit';
	    $data['albumTitle'] = $row->title;
        $data['link_edit']  = base_url('admin/albums/'.$row->id, 'https');
	    $data['albumDescription']  = $row->description;
	    $data['productId']  = NULL; // pass valid productId if you want to assign a photo to it.
	    $data['albumId']    = $id;
	    $data['photos']     = $CI->utilitar->getAlbumPhotos($id);
	    $data['public_access']  = $row->public_access;


        /*
	    $data['hid_ownIdField'] = "<input type='hidden' name='pId' value='$good->id'>";
	    */

	    $dir = $CI->utilitar->getAlbumPath($id);
        $data['dir_token']          = $CI->utilitar->createDirectoryKey($dir, Utilitar::PP_THUMBNAIL, $id);
        $data['uploader_component'] = $this->load->view("admin/div_upload", $data, true);//$this->load->view("admin/view_upload222", $data, true);

        $data['date_created']   = date('d.m.Y', strtotime($row->date));
        $data['backUrl']        = base_url().'admin/albums';

        $data['pagename'] = __CLASS__;

	    $this->load->view("admin/header", $data);
        $this->load->view("admin/view_album_add_edit", $data);
        $this->load->view("admin/footer");
    }


    // pass valid productId if you want to assign a photo to it.
    public function editStripped($id = -1, $productId = NULL)
	{
        if (!$this->isAllowed(__FUNCTION__))
        {   $this->warnDenied();
            return;//redirect('admin/mnt');
        }


	    $CI = &get_instance();
        $CI->load->model('utilitar');
        $row = $CI->utilitar->getAlbum($id);
        if (!$row)
	    {   redirect("admin/albums/add");
	        return;
	    }


        $data = array();
	    $data['title']      = 'Edit album';
	    $data['actionType'] = 'doEdit';
	    $data['albumTitle'] = $row->title;
	    $data['albumDescription'] = $row->description;
	    $data['productId']  = $productId;
	    $data['albumId']    = $row->id;
        $data['link_edit']  = base_url('admin/albums/'.$row->id, 'https');


	    $data['photos']     = $CI->utilitar->getAlbumPhotos($id);

        $dir = $CI->utilitar->getAlbumPath($id);
        $data['dir_token']          = $CI->utilitar->createDirectoryKey($dir, Utilitar::PP_THUMBNAIL, $id);
        $data['uploader_component'] = $this->load->view("admin/div_upload", $data, true);//$this->load->view("admin/view_upload222", $data, true);

//        $this->load->view("admin/view_album_add_edit_old", $data); // i failed calling uploader_component_onInit so no upload component here :(
        $this->load->view("admin/view_album_slideshow", $data);
    }


    public function doAdd()
    {
        if (!$this->isAllowed(__FUNCTION__))
        {   $this->warnDenied();
            return;//redirect('admin/mnt');
        }

        $title          = $this->input->post('title');
        $description    = $this->input->post('description');
        $date_created   = $this->input->post('date_created');
        $public_access  = ('' == $this->input->post('public_access')) ? 0 : 1;// checkbox values to be checked this way.

        $data = array('title' => $title, 'description' => $description, 'public_access' => $public_access, 'date' => date('Y-m-d H:i:s', strtotime($date_created)));
        $this->db->insert('albums', $data);

        redirect("admin/albums");
    }

    public function doEdit()
    {
        if (!$this->isAllowed(__FUNCTION__))
        {   $this->warnDenied(true);
            return;
        }
        
        $title          = $this->input->post('title');
        $description    = $this->input->post('description');
        $date_created   = $this->input->post('date_created');
        $public_access  = ('' == $this->input->post('public_access')) ? 0 : 1;// checkbox values to be checked this way.
        $albumId        = $this->input->post('albumId');

        $data = array('title' => $title, 'description' =>$description, 'public_access' => $public_access, 'date' => date('Y-m-d H:i:s', strtotime($date_created)));

        $this->db->where('id', $albumId);
        $this->db->update('albums', $data);

        redirect("admin/albums");
    }
}