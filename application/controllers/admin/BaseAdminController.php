<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . '/controllers/admin/AdminOnly.php');

interface IBaseAdminController
{
    public function index();
    public function addOrEdit($id = -1);
    //public function delete($rowId);  this entirely is implemented in a base class - e.g. "BaseAdminController".
    public function doAddEdit();
}

abstract class BaseAdminController extends AdminOnly {
    private static $TABLE_NAME;
    private static $PAGE_NAME;


    function __construct($TABLE_NAME, $PAGE_NAME)
    {
        parent::__construct();

        BaseAdminController::$TABLE_NAME   = $TABLE_NAME;
        BaseAdminController::$PAGE_NAME    = $PAGE_NAME;
    }

    static function base__init($TABLE_NAME, $PAGE_NAME)
    {
        BaseAdminController::$TABLE_NAME   = $TABLE_NAME;
        BaseAdminController::$PAGE_NAME    = $PAGE_NAME;
    }

//    function index(&$data)
    static function base__index(&$data)
    {
/*        if (!$this->isAllowed(__FUNCTION__))
        {   $this->warnDenied();
            return;
        }*/
        $data['actionUri'] = BaseAdminController::$PAGE_NAME.'/addOrEdit';
    }


    static function base__addOrEdit(&$row, &$data)
    {
        BaseAdminController::add_edit_stub($row, $data);
    }

    // used to return a stub item for Edit-page. Is optional (yet). // todo: make it abstract.
    protected function getStubItem()
    {
        return NULL;
    }

    // i wonder why php does fail if i call "$this->edit(param1, param2)". Instead, using alternative name works. (the all-mighty RTFM is here, i guess..)
    private static function add_edit_stub($row, &$data)
    {
        $data['recordId']   = is_null($row) ? -1 : $row->id;
        $data['record']     = $row; // give accecss to entire row record from the view.
        $data['actionUri']  = sprintf('%s/doAddEdit', BaseAdminController::$PAGE_NAME);
        $data['backUrl']    = sprintf(base_url().'index.php/admin/%s', BaseAdminController::$PAGE_NAME);
    }

    static function base__doAddEdit($rowId, &$data)
    {
//        if (!$this->isAllowed(__FUNCTION__))
//        {   $this->warnDenied();
//            return;
//        }
        $ci = &get_instance();

        $redirect_to = array_key_exists('backUrl', $data) ? $data['backUrl'] : null;
        unset($data['backUrl']); // remove it from the list of data being written to DB

        if ($rowId > 0)
        {   $ci->db->where('id', $rowId);
            $ci->db->update(BaseAdminController::$TABLE_NAME, $data);
        }
        else
        {   $ci->db->insert(BaseAdminController::$TABLE_NAME, $data);
        }


        if ($redirect_to)
        {   redirect($redirect_to);
        }
        else
        {
            if ($rowId > 0)
            {   redirect("admin/BaseAdminController::$PAGE_NAME/$rowId", 'location', 302, 3000);
            }
            else
            {   redirect("admin/BaseAdminController::$PAGE_NAME", 'location', 302, 3000);
            }
        }
    }

    static function delete($rowId)
    {
        /*if (!$this->isAllowed(__FUNCTION__))
        {   $this->warnDenied();
            return;
        }*/
//        log_message('error', "DELETE BASE for table BaseAdminController::$TABLE_NAME");

        $ci = &get_instance();
        $ci->db->delete(BaseAdminController::$TABLE_NAME, array('id' => $rowId));
        redirect('admin/'.BaseAdminController::$PAGE_NAME);
    }
}
