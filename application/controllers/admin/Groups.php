<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . '/controllers/admin/AdminOnly.php');

class Groups extends AdminOnly {

    function __construct()
    {
        parent::__construct();
    }


	public function index()
	{
	    if (!$this->isAllowed(__FUNCTION__))
        {   $this->warnDenied(true);
            return;
        }

	    $additionalString = "onchange='showSubKindsLinked()' size='5' style='width:300px;'";

	    $CI = &get_instance();
        $CI->load->model('utilitar');

        $users = $CI->utilitar->getGroupMembers();

        $data = array();
	    $this->prepareData($data, FALSE, $users, $additionalString);


	    $config = $data['config_array'];
        $config['users_list']       = $CI->utilitar->createSelectWithSelection(     $users,
                                                                                    "usersList",
                                                                                    "usersList",
                                                                                    "size='5' style='width:300px;' multiple='select-multiple'",
                                                                                    -1,
                                                                                    FALSE);
        $config['updateUri']    = 'admin/groups/updateGroupMembers';
        $config['link_edit']      = base_url().'admin/groups/edit/';
        $config['link_delete']    = '#';

        $data['doubleComboJS']  = $this->load->view('public/view_doubleComboJavascript', $config, true);
        //======================|


	    $this->load->view("admin/header", $data);
        $this->load->view("admin/view_groups", $data);
        $this->load->view("admin/footer");
	}

	public function roles()
	{
	    if (!$this->isAllowed(__FUNCTION__))
        {   $this->warnDenied(true);
            return;
        }

	    $additionalString = "onchange='showSubKindsLinked()' size='5' style='width:300px;'";

	    $CI = &get_instance();
        $CI->load->model('utilitar');

        $users = $CI->utilitar->getGroupMembers();

        $data = array();
	    $this->prepareData($data, TRUE, $users, $additionalString);


	    $config = $data['config_array'];
        $config['users_list']       = $CI->utilitar->createSelectWithSelection(     $users,
                                                                                    "usersList",
                                                                                    "usersList",
                                                                                    "size='5' style='width:300px;' multiple='select-multiple'",
                                                                                    -1,
                                                                                    FALSE);
        $config['updateUri']    = 'admin/groups/updateGroupMembers';
        $config['link_edit']      = base_url().'admin/groups/edit_role/';
        $config['link_delete']    = base_url().'admin/groups/delete_role/';

        $data['doubleComboJS']  = $this->load->view('public/view_doubleComboJavascript', $config, true);
        //======================|


	    $this->load->view("admin/header", $data);
        $this->load->view("admin/view_groups", $data);
        $this->load->view("admin/footer");
	}


	public function groupEmail()
	{
	    if (!$this->isAllowed(__FUNCTION__))
        {   $this->warnDenied(true);
            return;
        }

	    $additionalString = "onchange='showSubKinds()' size='5' style='width:300px;'";

	    $CI = &get_instance();
        $CI->load->model('utilitar');

        $users = $CI->utilitar->getGroupMembers();

	    $data = array();
	    $this->prepareData($data, FALSE, $users, $additionalString);
        $data['title']  = 'Email to user groups'; // prepareData doesn't account a case for "groupEmail" so we do it manually here.

	    $config = $data['config_array'];
	    
        $data['doubleComboJS'] = $this->load->view('public/view_singleComboJavascript', $config, true);
        //======================|


	    $this->load->view("admin/header", $data);
        $this->load->view("admin/view_groups", $data);
        $this->load->view("admin/footer");
	}

	public function sendMassEmail()
	{
	    if (!$this->isAllowed(__FUNCTION__))
        {   $this->warnDenied(true);
            return;
        }

	    $data = array();
        $data['title'] = 'Email sent';
        $data['backUrl'] = base_url().'admin/mnt';
        $data['msg'] = "Congratulations!\nEmail sent to requested recipient(s).";
        $this->load->view("admin/view_success", $data);
	}


	public function updateGroupMembers()
	{
	    if (!$this->isAllowed(__FUNCTION__))
        {   $this->warnDenied();
            return;
        }

	    Groups::updateLinkedLists('putUserIntoGroup', 'removeUserFromGroup');
    }
    

    // todo: move to utilitar functions along with double combobox view, etc.!
    static function updateLinkedLists($utilitarMethodAdd, $utilitarMethodRemove)
    {
	    $itemIDs    = $_POST['itemIDs'];
	    $removeFrom = intval($_POST['removeFrom']);
	    $assignTo   = intval($_POST['assignTo']);

	    $CI = &get_instance();
        $CI->load->model('tank_auth/users');
        $CI->load->model('utilitar');

	    $group_remove_from  = $CI->utilitar->getGroupById($removeFrom);
	    $group_assign_to    = $CI->utilitar->getGroupById($assignTo);


	    // Note: prohibit admin roles management for everybody but admin:

        $user_id = $CI->session->userdata('user_id');
        $user = $CI->users->get_user_by_id_full($user_id);
        if ($user)
        {   if (!$CI->users->user_has_role($user_id, SecuredMatrix::ROLE_ADMINISTRATOR))
            {
                if (
                    ($group_remove_from && $group_remove_from->name == SecuredMatrix::ROLE_ADMINISTRATOR) ||
                    ($group_assign_to && $group_assign_to->name == SecuredMatrix::ROLE_ADMINISTRATOR)
                    )
                {
                    print 'access denied';
                    return;
                }
            }
        }
        else
        {   print 'access denied';
            return;
        }


        if (($assignTo < 0) && ($removeFrom <0))
        {   //print "\nOperation not understood. Skipping it.";
	        return;
        }


        // convert to array:
        $itemIDs = explode(",", $itemIDs);

        $CI = &get_instance();
        $CI->load->model('utilitar');

	    if ($assignTo >= 0)
	    {
	        foreach ($itemIDs as $item)
	        {
//                print "\nadd to gr=$assignTo item=".print_r($item, true);

	            if ('' == $item) // explode() gives empty elements :(
	            {   continue;
	            }
                $CI->utilitar->$utilitarMethodAdd(intval($item), intval($assignTo));
            }
	    }
	    else if ($removeFrom >= 0)
	    {
            foreach ($itemIDs as $item)
	        {
//	            print "\nremove from gr=$removeFrom item=".print_r($item, true);

                if ('' == $item) // explode() gives empty elements :(
	            {   continue;
	            }

                $CI->utilitar->$utilitarMethodRemove(intval($item), intval($removeFrom));
            }
	    }
    }

    private function prepareData(&$data, $bForRoles, &$users, $additionalString)
    {
        $groupId = NULL;
        if (isset($data['groupId']))
        {   $groupId = $data['groupId'];
        }

        $CI = &get_instance();
        $CI->load->model('utilitar');

        $rows = array();
        if ($bForRoles)
        {   $rows = $CI->utilitar->getRolesList();
        }
        else
        {   $rows = $CI->utilitar->getUserGroups();
        }

        $compoundData = array();

        $linkTitle = 'view members...';
        foreach ($rows as $row)
        {
            $compoundData[] = array(
                'ajaxString' => $CI->utilitar->composeAjaxQueryString("/gae/admin/groups/editStripped/$row->id/$groupId", $linkTitle),
                //'ajaxString' => $CI->utilitar->composeAjaxQueryString("http://www.google.com/", $linkTitle),
                'title' => $row->name,
             );
        }
        $data['compoundData'] = $compoundData;

        $data['title']  = $bForRoles ? lang('admin.menu.roles') : lang('admin.menu.groups');
        $data['action'] = $bForRoles ? 'add_role' : 'add';



        //======================+
        // select kinds first:
        $config = array();


        $config['subKindID'] = -1;
        $groupId = -1; // pass actual value here to make this preselected in a combo.


        $config['combo'] = $CI->utilitar->createSelectWithSelection($rows,
                                                                    "kindsList",
                                                                    "kindsList",
                                                                    $additionalString,
                                                                    $groupId);
        $config['kindsArrayName']   = "arrKinds";
        $subKindsArrayName          = "arrSubKinds";

        $config['js_array1']        = $CI->utilitar->createJSarray($rows, $config['kindsArrayName']);
        $config['js_arrays_nested'] = $CI->utilitar->createNestedJavaScriptArrays($rows, $subKindsArrayName, 'getGroupMembers');

        $config['js_container_array_name'] = "arrContainer";
        $config['containerArray']   = $CI->utilitar->createContainerArray(  $config['js_container_array_name'],
                                                                            $rows,
                                                                            $subKindsArrayName);

        $config['usersArrayName']   = 'arrUsers';
        $config['js_array_users']   = $CI->utilitar->createJSarray($users, $config['usersArrayName']);

        $data['config_array'] = $config;
    }

    public function deletePhoto($photoId)
    {
        if (!$this->isAllowed(__FUNCTION__))
        {   $this->warnDenied(true);
            return;
        }

        // calculate products count with that image assigned:
        $this->db->where('photo_id', $photoId);
        $query = $this->db->get('goods');
        $productsWithThatPhoto = $query->num_rows();


        // retrieve paths:
        $this->db->where('id',$photoId);
        $query = $this->db->get('photo');
        $row = $query->row();

        $img_path = $row->photo_path;
        $thumb_path = $row->thumbnail_path;


        // delete the files:
        if (file_exists($img_path))
        {   unlink($img_path); // the image.
        }

        if (file_exists($thumb_path))
        {   unlink($thumb_path); // the thumbnail.
        }

        // delete the DB record:
        $this->db->delete('photo', array('id' => $photoId));



        $data['title'] = 'Photo deleted';
        $data['backUrl'] = base_url().'admin/albums/';
        $data['msg'] = "Products count with that photo: $productsWithThatPhoto";
        $this->load->view("admin/view_success", $data);
    }

    private function _createThumbnail($sourceImage_filepath)
    {
        $config['image_library'] = 'gd2';
        $config['source_image'] = $sourceImage_filepath;
        $config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = 75;
        $config['height'] = 75;

        $this->load->library('image_lib', $config);
        if(!$this->image_lib->resize()) echo $this->image_lib->display_errors();
    }


    private function showAddOrEdit($actionType, $title, $groupType, $groupId = -1)
    {
	    if (!$this->isAllowed(__FUNCTION__))
        {   $this->warnDenied(true);
            return;
        }
    
        $data = array();

        $data['title']          = $title;
	    $data['actionType']     = $actionType;
	    $data['groupId']        = $groupId;
	    $data['groupName']      = '';
	    $data['groupName_ru']   = '';
	    $data['groupType']      = $groupType;

	    if ($groupId >= 0)
	    {
            $CI = &get_instance();
            $CI->load->model('utilitar');

            $ug = $CI->utilitar->getGroupById($groupId);
            if ($ug)
            {
                $data['groupId']        = $ug->id;
                $data['groupName']      = $ug->name;
                $data['groupName_ru']   = $ug->name_ru;
                $data['groupType']      = $ug->group_type;
            }
	    }

	    $backUrl = '../groups';
	    switch ($groupType)
	    {
	        case Utilitar::GROUP_TYPE__ROLES:
	            $backUrl = 'roles';
	        break;
	    }
	    $data['backUrl'] = base_url()."admin/groups/$backUrl";


        $this->load->view("admin/header", $data);
	    $this->load->view("admin/view_group_add_edit", $data);
	    $this->load->view("admin/footer");
    }

    public function add()
    {
        if (!$this->isAllowed(__FUNCTION__))
        {   $this->warnDenied(true);
            return;
        }

        $this->showAddOrEdit('doAdd', 'Add new group', Utilitar::GROUP_TYPE__USERS);
    }

    // Note: the method is ROUTE-d.
	public function edit($id)
	{
	    if (!$this->isAllowed(__FUNCTION__))
        {   $this->warnDenied(true);
            return;
        }

	    $this->showAddOrEdit('doEdit', 'Edit user group', Utilitar::GROUP_TYPE__USERS, $id);
    }


    public function add_role()
    {
        if (!$this->isAllowed(__FUNCTION__))
        {   $this->warnDenied(true);
            return;
        }

        $this->showAddOrEdit('doAdd', 'Create new role', Utilitar::GROUP_TYPE__ROLES);
    }

    // Note: the method is ROUTE-d.
    function edit_role($id)
    {
        if (!$this->isAllowed(__FUNCTION__))
        {   $this->warnDenied(true);
            return;
        }

        $this->showAddOrEdit('doEdit', 'Edit role', Utilitar::GROUP_TYPE__ROLES, $id);
    }

    public function delete_role($role_id = -1)
    {
	    if (!$this->isAllowed(__FUNCTION__))
        {   $this->warnDenied(true);
            return;
        }

        $role_id = intval($role_id);
        if ($role_id <=0 )
        {   return;
        }

        $CI = &get_instance();
        $CI->load->model('tank_auth/users');
        $CI->load->model('utilitar');

        $CI->users->delete_role($role_id);  // exclude users from that group first.
        $CI->utilitar->deleteGroupById($role_id); // ... then remove the group itself.

        redirect('admin/groups/roles');
    }


    // pass valid productId if you want to assign a photo to it.
    public function editStripped($id, $productId = NULL)
	{
	    if (!$this->isAllowed(__FUNCTION__))
        {   $this->warnDenied(true);
            return;
        }

        $CI = &get_instance();
        $CI->load->model('utilitar');

        $ug = $CI->utilitar->getUserGroup($id);

        $data = array();
        
	    $data['title'] = 'Edit user group';
	    $data['actionType'] = 'doEdit';
	    $data['groupId'] = $ug->id;
	    $data['groupName'] = $ug->name;

	    $backUrl = '../groups';
	    switch ($ug->group_type)
	    {
	        case Utilitar::GROUP_TYPE__ROLES:
	            $backUrl = 'roles';
	        break;
	    }
	    $data['backUrl'] = base_url()."admin/groups/$backUrl";

        $this->load->view("admin/view_group_add_edit", $data);
    }


    public function doAdd()
    {
	    if (!$this->isAllowed(__FUNCTION__))
        {   $this->warnDenied(true);
            return;
        }

        $title = $_POST["title"];
        $title_ru = $_POST["title_ru"];

        $description = $_POST["description"];
        $groupType = intval($_POST['groupType']);

        $data = array(
               'name' => $title,
               'name_ru' => $title_ru,
               'description' => $description,
               'group_type' => $groupType, // Utilitar::GROUP_TYPE__***
            );

        $this->db->insert('user_groups', $data);


        $data['title']      = 'Group added';
        $data['msg']        = "Group sucessfully added!";
        $data['backUrl'] = base_url().'admin/groups';
        if (1 == $groupType)
        {   $data['title']  = 'Menu added';
            $data['msg']    = "Menu item sucessfully added!";
            $data['backUrl'] = base_url().'admin/groups';
        }
        else if (2 == $groupType)
        {   $data['title']  = 'Role added';
            $data['msg']    = "Role sucessfully added!";
            $data['backUrl'] = base_url().'admin/groups/roles';
        }

        $this->load->view("admin/view_success", $data);
    }

    public function doEdit()
    {
	    if (!$this->isAllowed(__FUNCTION__))
        {   $this->warnDenied(true);
            return;
        }

        $title = $_POST["title"];
        $title_ru = $_POST["title_ru"];

        $description = $_POST["description"];
        $groupId = $_POST['groupId'];
        $groupType = intval($_POST['groupType']);

        $data = array(
               'name' => $title,
               'name_ru' => $title_ru,
               'description' => $description,
               'group_type' => $groupType, // Utilitar::GROUP_TYPE__***
            );

        $this->db->where('id', $groupId);
        $this->db->update('user_groups', $data);

        $data['title'] = 'Group edited';
        $data['msg'] = "Group sucessfully edited!";
        $data['backUrl'] = base_url().'admin/groups';
        $this->load->view("admin/view_success", $data);
    }

    public function users()
    {
	    if (!$this->isAllowed(__FUNCTION__))
        {   $this->warnDenied(true);
            return;
        }
    
        $CI = &get_instance();
        $CI->load->model('tank_auth/users');

        $data = array();
        $data['title'] = lang('admin.menu.users.hint');

        $users = $CI->users->get_users_list();
        $this->enrichUserRights($users);
        $data['users'] = $users;

        $this->load->view("admin/header", $data);
        $this->load->view("admin/view_users", $data);
        $this->load->view("admin/footer");
    }

    private function enrichUserRights(&$users)
    {
        $CI = &get_instance();
        $CI->load->model('tank_auth/users');
        $separator = ', ';

        foreach ($users as $user)
        {
            $roles_array = $CI->users->getUserRoleNames($user->id);
            $res = '';

            foreach ($roles_array as $role)
            {   $res .= $role->roleName.$separator;
            }

            if (strlen($res) > 0)
            {   $res = substr($res, 0, strlen($res) - strlen($separator));
            }

            $user->roles = $res;
        }
    }


    function addEditUser($user_id = -1)
    {
	    if (!$this->isAllowed(__FUNCTION__))
        {   $this->warnDenied(true);
            return;
        }

        $data = array();

        $data['title'] = sprintf(lang('admin.tmpl.create.m.u'), lang('admin.category.user'));
        $data['actionType'] = 'doAddEditUser';

        $data['userId']             = -1;
        $data['name_fio']           = '';
        $data['email']              = '';
        $data['phone']              = '';
        $data['description']        = '';

        $data['activity_types'] = array(0 => lang('admin.status.inactive'), 1 => lang('admin.status.active'));
        $data['exclusive_access_types'] = array(0 => lang('admin.exclusive.no'), 1 => lang('admin.exclusive.yes'));
        $data['p_type_value'] = 1;

        if ($user_id > 0)
        {
            $CI = &get_instance();
            $CI->load->model('tank_auth/users');

            $user = $CI->users->get_user_by_id_full($user_id);
            if ($user)
            {
                $data['title'] = 'Modify account "'.$user->email.'"';
                $data['name_fio']       = $user->name;
                $data['p_type_value']   = $user->activated;
                $data['userId']         = $user->id;
                $data['email']          = $user->email;
                $data['phone']          = $user->phone;
                $data['description']    = $user->description;
            }
        }

        $this->load->view("admin/view_user_add_edit", $data);
    }

    function editPassword($user_id = -1)
    {
	    if (!$this->isAllowed(__FUNCTION__))
        {   $this->warnDenied(true);
            return;
        }

        if ($user_id <= 0)
        {   redirect('groups/users');
        }

        $CI = &get_instance();
        $CI->load->model('tank_auth/users');

        $user = $CI->users->get_user_by_id_full($user_id);
        if (!$user)
        {   return;
        }

        $data = array();
        $data['title']      = sprintf(lang('admin.changepass.user'), $user->email);
        $data['actionType'] = 'doChangeUserPassword';
        $data['userId']     = $user->id;
        $data['backUrl']    = base_url()."admin/groups/addEditUser/$user->id";

        $this->load->view("admin/view_user_password_edit", $data);
    }

    function doChangeUserPassword()
    {
	    if (!$this->isAllowed(__FUNCTION__))
        {   $this->warnDenied(true);
            return;
        }

        $CI = &get_instance();
        $this->load->library('tank_auth');

        if (!$this->tank_auth->is_logged_in()) // todo: switch ON for release.
        {   redirect();
        }

        $user_id            = htmlspecialchars($this->security->xss_clean($this->input->post('pId')), ENT_QUOTES);
        $password_current   = htmlspecialchars($this->security->xss_clean($this->input->post('password_current')), ENT_QUOTES);
        $password1          = htmlspecialchars($this->security->xss_clean($this->input->post('password1')), ENT_QUOTES);
        $password2          = htmlspecialchars($this->security->xss_clean($this->input->post('password2')), ENT_QUOTES);


        if (0 == strlen($password_current) || 0 == strlen($password1) || 0 == strlen($password2))
        {
            $CI->utilitar->showPublicMessage('������ ��������� ������',
            '���������� ��������� ��� ����.<br>����������, <a href="javascript:history.go(-1);">��������� �������</a>.');
            return;
        }

        if (0 != strcmp($password1,$password2))
        {
            $CI->utilitar->showPublicMessage('Passwords do not match','Please, <a href="javascript:history.go(-1);">try again</a>');
            return;
        }

        // now let's change user password:
        if (!$this->tank_auth->change_password_for_user($user_id, $password_current, $password1))
        {
            $errors = $this->tank_auth->get_error_message();
            $errors_explanation = print_r($errors, true);
            $CI->utilitar->showPublicMessage('������ ��������� ������',
            '��������� ��������� ������:<br> '.$errors_explanation.'<br>����������, <a href="javascript:history.go(-1);">��������� �������</a>.');
            return;
        }

        redirect('admin/groups/users');
    }

    function doAddEditUser()
    {
	    if (!$this->isAllowed(__FUNCTION__))
        {   $this->warnDenied(true);
            return;
        }

        $CI = &get_instance();
        $CI->load->model('utilitar');

        $email_activation = FALSE;

        $user_id    = htmlspecialchars($this->security->xss_clean($this->input->post('pId')), ENT_QUOTES);
        $name_fio   = htmlspecialchars($this->security->xss_clean($this->input->post('name_fio')), ENT_QUOTES);
        $account_status = htmlspecialchars($this->security->xss_clean($this->input->post('is_active')), ENT_QUOTES);

        $email      = htmlspecialchars($this->security->xss_clean($this->input->post('email')), ENT_QUOTES);
        $phone      = htmlspecialchars($this->security->xss_clean($this->input->post('phone')), ENT_QUOTES);
        $description            = htmlspecialchars($this->security->xss_clean($this->input->post('description')), ENT_QUOTES);

        $username = $email; // greco: we do this way here.

        $password1  = NULL;
        $password2  = NULL;

        if ($user_id <= 0)
        { // passwords taken only during user creation. User edit does not involve password change (it's done on a separate page).
            $password1  = htmlspecialchars($this->security->xss_clean($this->input->post('password1')), ENT_QUOTES);
            $password2  = htmlspecialchars($this->security->xss_clean($this->input->post('password2')), ENT_QUOTES);

            if (0 != strcmp($password1,$password2))
            {
                $CI->utilitar->showPublicMessage('Passwords do not match','Please, <a href="javascript:history.go(-1);">try again</a>');
                return;
            }

            if (NULL == $password1 || '' == $password1)
        {
            $CI->utilitar->showPublicMessage('Missing password fields', 'Please, <a href="javascript:history.go(-1);">try again</a>');
            return;
        }
        }

        if ('' == $name_fio || '' == $email)
        {
            $CI->utilitar->showPublicMessage('Missing name or email fields', 'Please, <a href="javascript:history.go(-1);">try again</a>');
            return;
        }


        $data = array();

        // either create or update user account:
        if ($user_id <= 0) // create account
        {
            $user_data = NULL;
            if (!is_null($user_data = $this->tank_auth->create_user(    $email, // use email as username
                                                                        $email,
                                                                        $password1,
                                                                        $email_activation))
               )
            {
                // add more details to user account:
                $CI->users->set_user_details($user_data['user_id'], NULL, NULL, NULL, $name_fio, $account_status, NULL, $phone, $description);
                redirect('admin/groups/users');
                
//                $data['msg'] = "User account '".$user_data['username']."' successfully created.<br>";
//                //$data['msg'] .= "User has to change his password via '<u>forgot password</u>' link at login page.";
//
//                $data['title'] = 'User account created successfully';
            }
             else
            {   $data['msg']    = "Errors while creating user.<br>Please verify that account name and email are unique on a portal.";
                $data['title']  = 'User account is not created';
            }
        }
        else // update account
        {
            $CI = &get_instance();
            $CI->load->model('tank_auth/users');
            $CI->users->set_user_details($user_id, NULL, NULL, $username, $name_fio, $account_status, $email, $phone, $description);
            redirect('admin/groups/users');

//            $data['msg']    = "User account updated successfully.<br>";
//            $data['title']  = 'User account updated successfully';
        }

        $data['backUrl'] = base_url().'admin/groups/users';
        $this->load->view("admin/view_success", $data);
    }


    function doDeleteUser($user_id = -1)
    {
	    if (!$this->isAllowed(__FUNCTION__))
        {   $this->warnDenied(true);
            return;
        }

        $CI = &get_instance();
        $CI->load->model('tank_auth/users');

        $user = $CI->users->get_user_by_id_full($user_id);
        if (!$user)
        {   $CI->utilitar->showPublicMessage('Invalid account given.','<a href="'.base_url().'admin/groups/users">Back to users list</a>');
            return;
        }

        if ($CI->users->user_has_role($user_id, SecuredMatrix::ROLE_ADMINISTRATOR))
        {   $CI->utilitar->showPublicMessage('Administrative accounts deletion is prohibited','<a href="'.base_url().'admin/groups/users">Back to users list</a>');
        }
        else
        {
            $CI->users->delete_user(intval($user_id));
            redirect('admin/groups/users');
        }
    }
}