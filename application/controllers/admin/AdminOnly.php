<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH . '/controllers/admin/Secured.php');

// extend "Controller".
abstract class AdminOnly extends Secured {

    function __construct()
    {
        parent::__construct();
        
        $this->load->helper(array('url', 'language'));
        $this->lang->load('data');
        
		$this->load->library('tank_auth');

        log_message('error', "AO: before auth check");
		if (!$this->tank_auth->is_logged_in())
		{
                log_message('error', "AO: not logged in.");
                redirect('/auth/login/');
        }
        else if ($this->tank_auth->is_admin())
        {   log_message('error', "AO: is admin.");
            // allow access to admins: do nothing here, allow running the call as is.
            return;
        }
        else if ($this->tank_auth->is_moderator())
        {   log_message('error', "AO: is moderator.");
            redirect(base_url().'index.php/admin/genericTypes/show/recommendations_library');
        }
        else
        {   //echo "Dear ".$this->tank_auth->get_username().", welcome abroad!";
            log_message('error', "AO: no valid role supported.");
        }

        redirect();
    }

    public function getNameSpace()
    {
        return SecuredMatrix::NS_ADMIN;
    }

    function isAuthorized()
    {
        return $this->tank_auth->is_logged_in();
    }

    function index()
    {
    }
}