<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

//-----------------------------------------------------------+
//  Copyright (c) 2010 
//
//  Author: garegin ghazaryan (greco.el)
//  File creation date:  24.07.2010 23:04:40
//  Ver. 0.2
//
//-----------------------------------------------------------|


//
// Note: whitelisting approach used.
//
class SecuredMatrix
{
    const NS_ADMIN      = 'ns.admin';
    const NS_MODERATOR  = 'ns.moderator';
    const NS_EVERYONE   = 'ns.everyone';

    // predefined role names:
    const ROLE_ADMINISTRATOR    = 'administrator';
    const ROLE_MODERATOR        = 'moderator';
    const ROLE_TRAINER          = 'trainer';
    const ROLE_SERVER_BERKANA   = 'SERVER_berkana';
    const ROLE_MOBILE_CLIENT    = 'mobile_client'; // this is about those individuals which visit the Clubs and watch the schedule, etc.
    const ROLE_CABINET          = 'cabinet';
    const ROLE_YURLICO_WEBLOGIN = 'yurlico_web_login';

    // ensure to put ALL ROLES to this array!
    private static $roles = array(  SecuredMatrix::ROLE_ADMINISTRATOR,
                                    SecuredMatrix::ROLE_MODERATOR,
                                    SecuredMatrix::ROLE_TRAINER,
                                    SecuredMatrix::ROLE_SERVER_BERKANA,
                                    SecuredMatrix::ROLE_MOBILE_CLIENT,
                                    SecuredMatrix::ROLE_CABINET,
                                    SecuredMatrix::ROLE_YURLICO_WEBLOGIN);
    
    // NOTE: classnames are case-sensitive!!!
    private static $permissions = array(

        // format: namaspace_name => array(...)
        SecuredMatrix::NS_ADMIN => array (
                    // format: class_name => array(role_name => array(allowed_methodName1, allowed_methodName2, ...), role_name => array(allowed_methodName1, allowed_methodName2, ...))
                    'Mnt' => array( // format: role => array(allowed_methodName1, allowed_methodName2, ...)
                                        SecuredMatrix::ROLE_ADMINISTRATOR   => array('index','update'),
                                      ),
                    'News' => array( // format: role => array(allowed_methodName1, allowed_methodName2, ...)
                                        SecuredMatrix::ROLE_ADMINISTRATOR   => array('index','addOrEdit','doAddEdit','delete','loadReadOnly'),
                                        SecuredMatrix::ROLE_MODERATOR       => array('index','addOrEdit','doAddEdit','delete','loadReadOnly'),
                                      ),
                    'UploadHandler' => array( // format: role => array(allowed_methodName1, allowed_methodName2, ...)
                                        SecuredMatrix::ROLE_ADMINISTRATOR   => array('index','exec'),
                                        SecuredMatrix::ROLE_MODERATOR       => array('index','exec'),
                                      ),
                    'Docs' => array( // format: role => array(allowed_methodName1, allowed_methodName2, ...)
                                        SecuredMatrix::ROLE_ADMINISTRATOR   => array('index','delete','download'),
                                        SecuredMatrix::ROLE_MODERATOR       => array('index','delete','download'),
                                      ),

                    'Albums' => array( // format: role => array(allowed_methodName1, allowed_methodName2, ...)
                                        SecuredMatrix::ROLE_ADMINISTRATOR   => array('index','stripData','add','doAdd','edit','doEdit','editStripped','delete','deletePhoto'),
                                        SecuredMatrix::ROLE_MODERATOR   => array('index','stripData','add','doAdd','edit','doEdit','editStripped','delete','deletePhoto'),
                                      ),

                    'Menus' => array( // format: role => array(allowed_methodName1, allowed_methodName2, ...)
                                        SecuredMatrix::ROLE_ADMINISTRATOR   => array('index','menuGroupsNEW','addOrEdit','doAddEditNEW','deleteMenu','moveUpDown','updateMenuItems','loadReadOnly'),
                                        SecuredMatrix::ROLE_MODERATOR   => array('index','menuGroupsNEW','addOrEdit','doAddEditNEW','deleteMenu','moveUpDown','updateMenuItems','loadReadOnly'),
                                      ),
                    'Groups' => array( // format: role => array(allowed_methodName1, allowed_methodName2, ...)
                                        SecuredMatrix::ROLE_ADMINISTRATOR   => array('index','users','roles','add_role','edit_role','editPassword','doChangeUserPassword','updateGroupMembers','addEditUser', 'showAddOrEdit', 'doAdd', 'doAddEditUser', 'doDeleteUser'),
                                      ),

                    'Tm' => array( // format: role => array(allowed_methodName1, allowed_methodName2, ...)
                                        SecuredMatrix::ROLE_ADMINISTRATOR   => array('index','loadReadOnly','addOrEdit','doAddEdit', 'delete'),
                                      ),

                    'Orders'  => array( // format: role => array(allowed_methodName1, allowed_methodName2, ...)
                                        SecuredMatrix::ROLE_ADMINISTRATOR   => array('index',),
                                        SecuredMatrix::ROLE_MODERATOR       => array('index',),
                                      ),

                    'trainings_schedule'  => array( // format: role => array(allowed_methodName1, allowed_methodName2, ...)
                                        SecuredMatrix::ROLE_ADMINISTRATOR   => array('index','addOrEdit','doAddEdit', 'delete'),
                                        SecuredMatrix::ROLE_MODERATOR       => array('index','addOrEdit','doAddEdit', 'delete'),
                                      ),

                    'berk_accounts'  => array( // format: role => array(allowed_methodName1, allowed_methodName2, ...)
                                        SecuredMatrix::ROLE_ADMINISTRATOR   => array('index','addOrEdit','doAddEdit', 'delete'),
                                      ),

                    /*'genericTypes'  => array( // format: role => array(allowed_methodName1, allowed_methodName2, ...)

                                        // NOTE: the generics' line below gives access to *ALL GENERICS AT ONCE*!
                                        SecuredMatrix::ROLE_ADMINISTRATOR   => array('index','show','addOrEdit','delete'),
                                      ),*/

                    'GenericTypes'  => array( // format: role => array(allowed_methodName1, allowed_methodName2, ...)

                                        // NOTE: the generics' line below gives access to *ALL GENERICS AT ONCE*!
                                        SecuredMatrix::ROLE_ADMINISTRATOR   => array('index','show','addOrEdit','doAddEdit','delete'),
                                      ),
                             ),

        // format: namaspace_name => array(...)
        SecuredMatrix::NS_MODERATOR => array (
                    // format: class_name => array(role_name => array(allowed_methodName1, allowed_methodName2, ...), role_name => array(allowed_methodName1, allowed_methodName2, ...))

                    'GenericTypes'  => array( // format: role => array(allowed_methodName1, allowed_methodName2, ...)

                                        // NOTE: the generics' line below gives access to *ALL GENERICS AT ONCE*!
                                        SecuredMatrix::ROLE_MODERATOR       => array('index','show','addOrEdit','doAddEdit','delete'), // bad separation:
                                      ),
                             ),
    );

    static public function is_valid_role($role_name)
    {
        return in_array($role_name, SecuredMatrix::$roles);
    }

    static public function is_allowed(&$user_roles, $namespace, $class_name, $method_name)
    {
//        log_message('error', "looking for access to $class_name::$method_name(".print_r($user_roles, true).":".print_r($namespace, true).")");
        if (count($user_roles) == 0)
        {   log_message('error', "ZERO ROLE passed IN!");
        }
        
        if (array_key_exists($namespace, SecuredMatrix::$permissions))
        {
            $ns_permissions = SecuredMatrix::$permissions[$namespace];
            if (array_key_exists($class_name, $ns_permissions))
            {
                $class_permissions = $ns_permissions[$class_name];

                // Note: order/priority of roles is ignored!
                foreach ($user_roles as $role)
                {
                    if (array_key_exists($role->name, $class_permissions))
                    {
                        $role_permissions = $class_permissions[$role->name];
                        if (in_array($method_name, $role_permissions)) // try to find at least one role where that method is accessible:
                        {
//                            log_message('error', '... access granted.');
                            return true;
                        }
                    }
                }
            }
        }

        log_message('error', "... access DENIED! (namespace, class_name, method_name) = ($namespace, $class_name, $method_name)");
        return false;
    }
}