<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'libraries/Telegram.php');

include_once(APPPATH.'libraries/wizardry/core/WizardHelper.php');
include_once(APPPATH.'libraries/wizardry/library/WizardAuthorizer.php');
include_once(APPPATH.'libraries/wizardry/library/WizardUnauthorizer.php');
include_once(APPPATH.'libraries/wizardry/library/WizardSettings.php');

include_once(APPPATH.'libraries/wizardry/library/Tasker/WizardTrainerChecker.php');
include_once(APPPATH.'libraries/wizardry/library/Censorship/WizardCensor.php');

require_once(APPPATH . '/controllers/Json_base.php'); // greco.


class Tg extends CI_Controller {

    // N.B. To init call base_url().'index.php/tg/setup/setwebhook' to setup/unsetwebhook it.

    const PREFIX_CLIENT_INVITE = '/start invid'; // e.g. "https://telegram.me/tg_events_stream?start=invid889977"
    const LAST_PROCESSED_ID = 'last_processed_event_id'; // used to store the latest event about which users have been notified (via Telegram message).

    // ATTENTION! Ensure to use SAME values in these two constants!! They depend on each other!
    const PREFIX_CLUB_UID         = 'cuid';
    const PREFIX_OPEN_CLUB  = '/start cuid';

    const DETECTED_SPAM         = 11; // must start from 11 and higher!!!!
    const DETECTED_BAN          = 22;
    const DETECTED_SPAM_URLS    = 33;
    const DETECTED_BAN_AZERI    = 44;

    // NOTE: it is a must to put in the words in this array in LOWER-case only! Otherwise I'd need to call "strtolower()" which is stupid waste of CPU.
    // NOTE: those messages will be REMOVED, but the POSTER User WON'T be banned.
    const prohibited_entities_URLs =  array(
            '.notion.site',
            't.me/armenianmedicine', // too much spam
            'olga.zangieva',
//            '@russianteaminarmenia',
//            'instagram.com/',
//            't.me/cryptoitarmenia',
//            'relocation-school.com',
//            't.me/rentarmenia',
//            't.me/russianteaminarmenia',
//            'armreloc.ru',
            '094643060',
            't.me/yourprovodnik',
            'youtu.be/zexbtdhxtf4',
            't.me/+jqcwb7befg05mduy',
            't.me/+hbui9l9dqgg0n2fi',
            't.me/+2fadmxyctpyymtc0',
            't.me/greensmileyvn',
            't.me/kosmetologiarmenia',
            't.me/+ejqq4xj2pjg4yjnk',
            'localrent.com/en/armenia/?r=5775', // referral link
            't.me/+h_aeekdjz641nwvi',
            't.me/partnersintech',
            't.me/+ta9mplv943phmwni',
            't.me/noblockchease',
            't.me/+_ws0my_mrci3nzk0',
            't.me/sievn_real_estate_agency',
            '/admin',
            '/off',
            '/leave',
        );

    const prohibited_azeri_phrases_LOWERCASE_ONLY =  array(
            'karabakh is azerbaijan',
            'карабах - азербайджан',
            'карабах это азербайджан',
        );

    // NOTE: those messages will be REMOVED, but the POSTER User WON'T be banned.
    // NOTE: it is a must to put in the words in this array in LOWER-case only! Otherwise I'd need to call "strtolower()" which is stupid waste of CPU.
    const advertisement_LOWERCASE_ONLY = array(
            'fuck','pussy',' хуй', ' хер', 'нахер', 'пизда', 'пидоры', ' пидор', 'охуительно', 'охуел', 'охуеть',
//            'агентство оказывает следующие услуги',
//            'дом продается',
            'информируем вас о том, что палата адвокатов',
//            'сдается офисное помещение',
            'сраная ',
            'сраный ',
            'сраное ',
            'dr_sargis_harutyunyan',
//            'продается дом',
//            'у меня скидки до ',
//            'продается квартира',
//            'я тренер по теннису',
//            'квартира продается',
//            'являюсь собственником',
            'я врач-ортодонт из москвы',
//            'являюсь владелицей',
//            'являюсь владельцем',
//            'предоставляем услуги',
        );

    const ban_cursewords_LOWERCASE_ONLY = array(
            'беложоп',
    );

    // for these words the POSTER User will be BANNED immediately:
    // NOTE: it is a must to put in the words in this array in LOWER-case only! Otherwise I'd need to call "strtolower()" which is stupid waste of CPU.
    const ban_immediately_LOWERCASE_ONLY = array(
            '/leave',
            '/off',
            '/admin',
            'blablacararm',
            't.me/+ebx2xxkai4phyjni',
//            'я собственник',
//            'сдается от года',
//            'сдается маленький дом',
            '94527850',
            'my profits in my wallet',
//            'сдаётся маленький дом',
            'фінансову допомог',
            'diia_service_bot',
            'я врач-стоматолог',
            'сдается маленький дом',
            't.me/yerevanalive4',
            'infotop_course_bot',
            'copartambot',
            '77330591',
            't.me/+st88acw3ighhyzni',
            'просмотр пишите в личку',
//            'сдается коттедж',
//            'сдаеться ',
//            'сдаёться ',
//            'сдается жилое помещение',
            'кому нужна уборка в квартире или в доме пишите в личку',
//            'продам наличные доллары',
            'помощь с визой в европу',
            't.me/+_ws0my_mrci3nzk0',
            'can help you earn over',
            '94542463',
//            'сдается дома ',
            'есть контакт специалиста по визам',
            'помощь с оформлением рабочей визы в',
            'есть контакты специалиста по визам',
            'знаю хорошего специалиста по оформлению рабочей визы',
//            'организуем для вас все небходимые трейнинги',
//            'предлагаем вам работу в области логистики',
            ' ольга зангиева',
            'надеюсь вам тоже повезет... @rentarmenia',
            'why no one speaks', // porno bot
//            'сдаю жильё в ',
            't.me/noblockchease',
//            'решайте проблемы с документами на этом',
            't.me/yourprovodnik',
//            'информацию о наших услугах',
//            'перевод учредительных докуменов;',
            '77 92 60 55',
            't.me/partnersintech',
            'incognito llc',
//            'помогаю с открытием ооо ',
//            'сдаю трешку',
//            'сдаю двушку',
//            'сдаю однушку',
            ' бошки',
            'mj в армении',
            '99991150',
            '99990903',
//            'комнатную кваритиру',
//            'комнатная кваритира',
//            'сдается 1-комнатная',
//            'сдается 2-комнатная',
//            'сдается 3-комнатная',
//            'сдается 4-комнатная',
//            'сдаем свою 3-х комнатную',
//            'сдаем свою 2-х комнатную',
//            'сдаем 3-х комнатн',
//            'сдаем 2-х комнатн',
//            'сдаем 1 комнатн',
//            'сдаем 2х комнатн',
//            'сдаем 3х комнатн',
//            'сдаем 4и комнатн',
//            'сдаем 5и комнатн',
            't.me/+2fadmxyctpyymtc0',
            't.me/+ejqq4xj2pjg4yjnk',
            '@tpgfgnfl',
            '77354999',
            '096085757',
//            'предлагаем в аренду',
//            'сдается комната ',
            '@bibblers',
            'revcode.io',
            'онлайн школа nocode',
            'youtube.com/watch?v=kmk2qcfjh84',
            't.me/+et5g5qnktps2ntzi',
//            'даётся 1 комнатная квартира',
//            'даётся 2х комнатная квартира',
//            'даётся 3х комнатная квартира',
//            'сдаётся трёхкомнатная квартира',
//            'сдаётся двухкомнатная квартира',
//            'сдаётся однокомнатная квартира',
//            'сдаётся моя квартира',
//            'сдаю свою квартиру',
//            'сдам свою квартиру',
//            'для бронирования напишите нам',
//            'забронировать на нашем сайте',
            'продажа хороших бошек',
//            'сдается в аренду',
//            'сдаётся в аренду',
            't.me/baraxolkarmenia',
            'team2b.am',
            't.me/rossia_134',
            't.me/armenia852456',
            't.me/armeniarelocation',
            't.me/relocator_official',
            't.me/joinchat',
            'advocates.am',
            't.me/ciliciaclub',
//            't.me/kvartira_v_erevane',
            '@engline.school',
            '@valerisavi',
//            '@livinginarmenia',
            't.me/vahagn68',
            't.me/arminvests',
            't.me/agilefluent_chat',
            't.me/rentflatgyumrii',
            't.me/+giswl1d07lazymjk',
            't.me/+bqtjufbg0hy3odhi',
            't.me/+vuszco8lcq80nzzi',
//            'готовы вам предложить следующие юридические',
            '/davit_accountant',
            'just for +18 users',
//            't.me/livinginarmenia',
//            'ТГ-канал Жизнь в Армении (https://t.me/livinginarmenia) ',
            't.me/nemnogopravda',
            'armenianguidebot', // author: @valeri_ami
            'valeri_ami',
//            'юридическая и консалтинговая компания',
            't.me/erevan22',
            'cheeserelocation.ru',
//            'сдается помещение под',
//            'продаю дом', 'продам дом',
            'помогу достать наличные $',
            'lch.legal',
//            'сдаю офис',
            'помогу вам составить / проверить/ изменить договор',
            't.me/by_anna_mur',
            'tonus.onelink.me',
            '88-27-88',
            'думаете как выучить английский',
            'tonus.app',
            't.me/reexport2russia',
            't.me/armenianeveryday',
            't.me/natalliaanashkina',
            't.me/+vuszco8lcq80nzzi',
//            'бесплатную консультацию по вопросам реолакации',
//            'бесплатную консультацию по вопросам релокации',
//            'предоставляем такие услуги как встре',
//            'мы предлагаем занятия по курсу',
            'youtu.be/7uly263qpai', // дебилы нацисты
            'делюсь каналом по релокац',
//            'relocation.am',
//            'вот еще канал по релокации',
//            'продаю квартиру',
//            'продам квартиру',
//            'даю под аренду',
//            'дам под аренду',
            'написать нам для бронирования',
//            '#сдаюквартиу',
//            '#сдаюквартиру',
            '#develex',
            'в етой группе только русские',
//            '#сдаю квартиры дома',
//            'сдам квартиру', 'сдаю квартиру', 'сдам дом',  'сдаю дом',
//            'сдаю в аренду ',
            'фотографии можно посмотреть по ссылке ниже',
//            'сдаю комнату ',
//            'сдаётся комната ',
//            'сдается 1-комнатная',
//            'сдается 3-комнатная',
//            'сдается 3-комнатная',
//            'сдается 4-комнатная',
//            'сдается 5-комнатная',
//            'сдаётся 1 комнатная',
//            'сдаётся 2 комнатная',
//            'сдаётся 3 комнатная',
//            'сдаётся 4 комнатная',
//            'сдаётся 5 комнатная',
//            'сдается в аренду квартира',
//            'сдаётся  в аренду квартира',
//            'сдается  в аренду квартира',
//            'сдается в аренду офисное',
//            'сдаётся  в аренду офисное',
//            'сдается  в аренду офисное',
//            'сдаю 3х комнатную квартиру',
//            'сдаю 2х комнатную квартиру',
//            'сдаю 1 комнатную квартиру',
//            'сдаем удобное помещение',
//            'сдам удобное помещение',
//            'сдам помещение',
//            'сдаём помещение',
            'procurement service llc',
            'team2b.am',
            'yrdf.ru',
            'помогаю гражданам уехать из страны',
            'սենյականոց բնակարան',
            'cotref.space',
            'wowfurnitureforbusiness',
            'englishtoleaverussiaa',
//            'заинтересует 1 комнатная квартира',
//            'заинтересует 3-x комнатная квартира',
//            'заинтересует 2-х комнатная квартира',
            'могу предоставить онлайн консультацию',
            '#сдаю квартиры',
            'приглашаем вас в наш салон',
            '@pay_and_transfer',
            '@relocainworld',
//            'я представляю компанию', 'предоставляем финансовые и юридические услуги', 'предоставляем финансовые услуги', 'предоставляем юридические услуги', 'преподаю армянский язык',
//            't.me/russianinerevan',
            't.me/+4agad8nnwc01nwqy', // fucken Армения для Туристов
            'po*nh*b', // porn bots
            '@xxxporobot', // porn bots
//            't.me/russianemigration',
//            't.me/russkie_v_armenii',
            'нашла агрегатор по релокации',
            'очень подробно описывают и рассказывают нюансы по переезду',
            'заходим общаемся делимся знакомимся',
            'antiwarcommittee.info',
            'сдам 3х комнатную квартиру',
            'сдам 2х комнатную квартиру',
            'сдам 1 комнатную квартиру',
//            'кому необходимо помещение под офис',
//            'кому требуется помещение под офис',
//            'кому нужно помещение под офис',
            't.me/conciergetravelarmenia', // spam
            't.me/toexpedition.', // spam
            'քունած',
            'քունեմ',
            't.me/goarpr',
        );

	private $CI;
	
    function __construct()
    {
        parent::__construct();

        $this->CI = &get_instance();

        $this->CI->load->model('tank_auth/users');
       // $this->CI->load->library('tank_auth');

        $this->CI->load->model('utilitar_db');
        $this->CI->load->model('utilitar');
        $this->CI->load->model('utilitar_date');
        $this->CI->load->model('util_telegram');

        $this->load->helper('date');

        $this->wh = new WizardHelper(Util_telegram::get_bot_token());
        $wizard_father = new WizardFather();
        WizardHelper::resigterWizard($wizard_father);
        WizardHelper::resigterWizard(new WizardAuthorizer($wizard_father));     // TODO: get rid of this input param.
        WizardHelper::resigterWizard(new WizardUnauthorizer($wizard_father));   // TODO: get rid of this input param.

        WizardHelper::resigterWizard(new WizardTrainerChecker()); // version 2.0 of checking the visits
        WizardHelper::resigterWizard(new WizardCensor());

        WizardHelper::resigterWizard(new WizardSettings());
    }


    // modifies (fixes) the input, if needed
	public static function ensureKeyboardIsCorrect(&$buttons_array)
    {
        foreach ($buttons_array as &$button)
        {
            if (is_string($button))
            {   $button = array($button);
            }
        }
    }

	public function notify_new_task()
	{
        $telegram   = new Telegram(Util_telegram::get_bot_token()); // TODO: refactor this to use chat_id, user_id - instead of instantiating entire complex class for sending messages.

        $tg_users = $this->CI->utilitar_db->_getTableContents('tg_users','tg_user_id');
        foreach ($tg_users as $tg_user)
        {
            $tg_chat_id = $tg_user->tg_user_id;//$telegram->ChatID(); // use 113088389 for myself only.

            $message    = str_replace(array('\n'), chr(10), "Поступила новая задача #".(10 + rand(20, 88)).'\nДетали по задаче можно прочесть <strong>далее по тексту</strong>, а если потребуется - то и по <a href="http://walltouch.ru/pages/about.htm">ссылке</a>.');


            // create the data to be sent to TG:
            $content    = array('chat_id' => $tg_chat_id, 'text' => $message);

            // enrich it:
            $content['parse_mode'] = 'HTML'; // rich-text formatting?
            $content['disable_web_page_preview'] = true;

            $buttons_array = array(array('Начать работу', 'Отложить'), WizardBase::GO_BACK);
            if ($buttons_array)
            {
                // Create a permanent custom keyboard
    //            $keyb = $telegram->buildKeyBoard(array($buttons_array), $onetime=false, $resize = true);
    //            $keyb = $telegram->buildKeyBoard($buttons_array, $onetime=false, $resize = false);
                Tg::ensureKeyboardIsCorrect($buttons_array);
                $keyb = $telegram->buildKeyBoard($buttons_array, $onetime=false, $resize = true);
                $content['reply_markup'] = $keyb;
    //            log_message('error', "keyboard to be built=".print_r($buttons_array, true));
            }

    //        log_message('error', "echoTelegram(): ".print_r($content, true));

            //  send the message:
            $message_delivered = $telegram->sendMessage($content);
            log_message('error', "Message delivered OK to tg user id:".$tg_user->tg_user_id);
        }
	}

	public function index()
	{
	    redirect('auth/login');
	}

    public function setup($command = '')
    {
        $telegram = new Telegram(Util_telegram::get_bot_token());
        switch ($command)
        {
            case 'setwebhook':
                $r = $telegram->setWebHook(base_url('index.php/tg/process', 'https')); // SSL is a must.
                print_r($r);
                break;

            case 'unsetwebhook':
                $r = $telegram->setWebHook('');
                print_r($r);
                break;

            default:
                echo 'Setup command is unrecognized';
                break;
        }
    }

    private function pass_current_tg_user_id__bot_name__to_father($tg_user_id, $bot_name)
    {
        $father_wizard = WizardHelper::get_wizard_object(WizardFather::NAME);
        if (!$father_wizard)
        {   log_message('error', "Tg::process(): error accessing father wizard!");
        }
        else
        {   $father_wizard->set_tg_user_id__and__bot_name($tg_user_id, $bot_name);
        }
    }

    // NOTE:    Now handles not only the 'text' field (from regular posts) but also the 'caption' field from forwarded MEDIA messages!
    //          See the "$text" variable new assignment! (all the analysis algorythms remain intact so far, we just accounting ONE MORE FIELD: for forward-cases with media! Non-media forwards contain no 'caption'!)
    private function handleCensorship(&$telegram, &$message_structure)
    {
        $violations_found = 0; // a must to be initiated here!!!

        $from = isset($message_structure['from']) ? $message_structure['from'] : null;
        $text = isset($message_structure['text']) ? $message_structure['text'] : (isset($message_structure['caption']) ? $message_structure['caption'] : null);
        $chat_id = (isset($message_structure['chat']) && isset($message_structure['chat']['id'])) ? $message_structure['chat']['id'] : null;

        // detect if this event is about newly joined members:
        if (isset($message_structure['new_chat_members']))
        {   $bots_kicked        = $this->kick_off_joined_bots($message_structure, $chat_id, $telegram); // the very last item (if the list is not VOID!) contains the bot-adder person!
            $arabians_kicked    = $this->kick_off_arabic_names($message_structure, $chat_id, $telegram);// the very last item (if the list is not VOID!) contains the bot-adder person!
            return; // NOTE: identify whether they also posted messages (async execution -> taht's possible), so REMOVE those messages, too!
        }

        if (Tg::username_contains_curse_word($message_structure))
        {   $violations_found = Tg::DETECTED_BAN; // ban for it!
        }
        else if (Tg::contains_azeri_spam($telegram, $chat_id, $message_structure))
        {   $violations_found = Tg::DETECTED_BAN_AZERI; // ban for it!
        }
        else if ($this->handle_banned_chat_forwards($telegram, $chat_id, $message_structure))
        {   return;
        }
        else if ($this->handle_service_messages_and_ban_spammers_and_bots($telegram, $chat_id, $message_structure))
        {   return;
        }
        else if (Tg::contains_prohibited_entities($telegram, $chat_id, $message_structure))
        {   $violations_found = Tg::DETECTED_SPAM_URLS;
        }
        else if ($this->filter__contains_non_allowed_encoding($text))
        {    $violations_found = 2;
        }
        else if ($this->filter__contains_APK_attachments($message_structure))
        {    $violations_found = 3;
        }
        else
        {   $violations_found = $this->filter__contains_stopwords($text);
        }

        // otherwise this is about new message/edited message, so let's check them out, too:
        if ($violations_found > 0)
        {
            $telegram->deleteMessage(array('chat_id' => $chat_id, 'message_id' => $message_structure['message_id']));

            //-----------------------------------------------------------+
            $user_id = $from ? $from['id'] : null;
            $full_name = $from ? (isset($from['first_name']) ? $from['first_name'] : '' ) : '';
            $lastname = $from ? (isset($from['last_name']) ? $from['last_name'] : '' ) : '';

            if ($lastname)
            {   $full_name = trim($full_name.' '.$lastname);
            }
            //-----------------------------------------------------------|

            $message = "\xF0\x9F\x91\xAE ";
            switch ($violations_found)
            {
                case Tg::DETECTED_SPAM_URLS:
                    $message = "\xE2\x99\xBB спам-ссылка удалена. Автор ";
                    log_message('error', "---- CENSOR: SPAM URL identified from tguser id#".($from ? $from['id'] : 'USERID UNKNOWN: '.print_r($from, true))."\n$text");
                    break;

                case Tg::DETECTED_SPAM:
                    $message = "\xF0\x9F\x91\xAE Некорректное поведение";
                    log_message('error', "---- CENSOR: SPAM deleted from tguser id#".($from ? $from['id'] : 'USERID UNKNOWN: '.print_r($from, true))."\n$text");
                    break;

                case Tg::DETECTED_BAN:
                    $message = "\xF0\x9F\x9A\x91 отправляется в бан";
                    $telegram->kickChatMember(array('chat_id' => $chat_id, 'user_id' => $user_id)); // ban FOREVER.
                    log_message('error', "---- CENSOR: ban topic detected from tguser id#".($from ? $from['id'] : 'USERID UNKNOWN: '.print_r($from, true))."\n$text");
                    break;

                case Tg::DETECTED_BAN_AZERI:
                    $message = "\xF0\x9F\x90\x8F отправляется в бан \xE2\x99\xA8";
                    $telegram->kickChatMember(array('chat_id' => $chat_id, 'user_id' => $user_id)); // ban FOREVER.
                    log_message('error', "---- CENSOR: azeri detected from tguser id#".($from ? $from['id'] : 'USERID UNKNOWN: '.print_r($from, true))."\n$text");
                    break;

                case 2:
                    $message = "\xE3\x8A\x99 недопустимая кодировка текста от";
                    break;
                    
                case 3:
                    $message = "\xF0\x9F\x9A\xA8 Удалён исполняемый файл от";
                    log_message('error', "---- CENSOR: executable filel deleted from tguser id#".($from ? $from['id'] : 'USERID UNKNOWN: '.print_r($from, true)));
                    break;
            }

            $message .= " '$full_name'".' (написать <a href="tg://user?id='.$user_id.'">в личку</a>)';
            TelegramHelper::echoTelegram($message);
        }
        else
        {   // if no violations, then let's proceed further.
        
            $b_input_came_from_bot = $this->input_came_from_bot($message_structure);
            log_message('error', "--- input_came_from_bot: ".($b_input_came_from_bot ? 'YES' : 'NO'));


            // if came from bot, then we either expand (e.g. "/topic=123") it, or do nothing!
            if ($b_input_came_from_bot)
            {
                log_message('error', "--option came from BOT!");

                if (0 === strpos($text, WizardCensor::PREFIX_TOPIC_DETAILS))
                {
                    // 1. let's identify which string to cut off:
                    $num_chars_to_cut_off = strlen(WizardCensor::PREFIX_TOPIC_DETAILS); // default one is the most frequent one.

                    // 2. cut out the topic id:
                    $topic_id = substr($text, $num_chars_to_cut_off);
                    if (0 == strlen($topic_id))
                    {   log_message('error', "QQQ WARNING: cannot display topic contents from invite link ('$text'). Topic_id might be DAMAGED: '$topic_id'");
                        return false;
                    }

                    TelegramHelper::echoTelegram("См. расширенный ответ на топик номер $topic_id.\n\nqwerty qwery.");
                    return;
//                    WizardHelper::terminateAllRunningWizards($tg_user_id, $tg_chat_id); // a must to avoid any confusion with previously runned wizards and/or steps!!!
                }
            }
            else
            {   // came from human!

                $pos = strpos($text, WizardCensor::SKIP_PROCSSING_STOPWORD);
                if ($pos !== false)
                {   log_message('error', "NOTE: skipping processing of '$text' because of stop-word received in it!");
                    return;
                }

                // NOTE: I decided to DO spam/etc. parcing for quoted messages. At least I may allow it for chat admins. We don't do auto-search for quoted messages!
                if (isset($message_structure['reply_to_message']))
                {   log_message('error', "NOTE: we skip auto-answering for quоted (by everybody!) messages. Maybe that's wrong - dunno.");
                    return;
                    // Also check for: [chat][id] => this is group chat ID.
                }

                $topics_filtered = $this->search_in_FAQ_topics($text);
                if (count($topics_filtered) > 0)
                {
                    log_message('error', "--option came from HUMAN!");

                    // create the data to be sent to TG:
                    $content = array('chat_id' => $chat_id, 'parse_mode' => 'html');
                    $link = 'https://t.me/'.Util_telegram::get_bot_name().'?start=topic'.'_id12345';

                    $content['disable_web_page_preview'] = true;

                    if (!$b_input_came_from_bot)
                    {   // if input came from user then CITE IT:
                        $topic = reset($topics_filtered); // get the very first item. TODO: improve the search logic to provide relevancy!

//                        // NOTE: we don't do auto-search for quoted messages!
//                        if (isset($message_structure['reply_to_message']))
//                        {   log_message('error', "NOTE: we skip auto-answering for quoted (by everybody!) messages. Maybe that's wrong - dunno.");
//                            return;
//                            // Also check for: [chat][id] => this is group chat ID.
//                        }

                        $content['reply_to_message_id'] = $message_structure['message_id'];

                        log_message('error', "---- contains FAQ topics, but came from human");

    //                    $content['text'] = "Поиск по кючевой фразе:\nбла-бла-бла.\n\nПодробнее: <a href='$link'>".Util_telegram::get_bot_name().' '.WizardCensor::PREFIX_TOPIC_DETAILS.'bebeshka123';
    //                    $content['text'] = "Автопоиск по ключевой фразе:\n<code>бла-бла-бла.</code>\n\nПодробнее: <a href='$link'>спроси у бота</a>.";
                        //$str = "\xF0\x9F\x91\x89 <b>".$topic['title']."</b>\n\n";
                        $str = '';
                        $str .= $topic['description'];

//                        $str = "\xF0\x9F\x94\x8D ".str_replace(array('\n'), 'br', $topic['title'])."\n\n";
//                        $str .= str_replace(array('\n'), chr(10), $topic['description']);
                        
                        // $str .= "\n----\n<i>Добавьте в сообщение <code>".WizardCensor::SKIP_PROCSSING_STOPWORD."</code>, чтобы бот не отвечал на него</i>.";



//                        $link = 'https://t.me/'.Util_telegram::get_bot_name().'?start=setoptions';                        
//                        $str .= "\n\n<a href='$link'><i>Настроить бота под себя</i></a>";

                        $content['text'] = $str;
                        $message_delivered = $telegram->sendMessage($content);
                    }

//                    log_message('error', "---- handleCensorship() reply sent! Result: ".print_r($message_delivered, true));
                    log_message('error', "---- handleCensorship() reply sent!");
                }
                else
                {   log_message('error', "QQQ no FAQ words in '$text'. Skipped.");
                }
            }

            return;
        }
    }

    // returns true if:
    //  1. that was a message forwarded from one of chats denied
    //  2. the forwarded message itself contains prohibited items!
    private function handle_banned_chat_forwards(&$telegram, $chat_id, &$message_structure)
    {
        return false;// NYI!

        return isset($message_structure['forward_from_chat']) && in_array($message_structure['forward_from_chat'], WizardCensor::$banned_chats);
    }

    // returns true if that was a message containing prohibited links which are hid inside entities which are invisible in "text" directly.
    // NOTE: first searches in 1) HTML entities and 2) plain text.
    private static function username_contains_curse_word(&$message_structure)
    {
        if (!isset($message_structure['sender_chat']))
        {   return false;
        }

        $posted_by_username = $message_structure['sender_chat']['title'];
        log_message('error', "XXX username_contains_curse_word(): sender_chat='$posted_by_username', posted by bot: ".($message_structure['from']['is_bot'] ? "yes. ".$message_structure['from']['username'] : 'no.'));

        foreach (TG::ban_cursewords_LOWERCASE_ONLY as $curseword)
        {   $pos = strpos($posted_by_username, $curseword);
            if ($pos !== false)
            {   log_message('error', "---- found curse workd '$curseword' in username '$posted_by_username'");
                return true;
            }
        }

        return false;
    }

    // returns true if that was a message containing prohibited links which are hid inside entities which are invisible in "text" directly.
    // NOTE: first searches in 1) HTML entities and 2) plain text.
    private static function contains_prohibited_entities(&$telegram, $chat_id, &$message_structure)
    {
        //-----------------------------------------------------------+
        if (isset($message_structure['entities']))
        {
            // 1. search inside entities (HTML-formatted contents)
            $html_entities = $message_structure['entities'];
            foreach ($html_entities as $html_entity)
            {
                if (!isset($html_entity['url']))
                {   continue; // this is just HTML-formatted text with no link: skip it.
                }

                foreach (TG::prohibited_entities_URLs as $url)
                {
                    $pos = strpos($html_entity['url'], $url);
                    if ($pos !== false)
                    {   log_message('error', "---- found prohibited URL '$url' inside ENTITIES: ".$html_entity['url']);
                        return true;
                    }
                }
            }
        }
        //-----------------------------------------------------------|


        //-----------------------------------------------------------+
        // 2. search inside plain-text content:
        if (isset($message_structure['text']))
        {
            $text = mb_strtolower($message_structure['text']); // Russian encoding as well!
            foreach(TG::prohibited_entities_URLs as $v)
            {
                $pos = strpos($text, $v);
                if ($pos !== false)
                {   log_message('error', "---- found prohibited URL '$v' inside PLAINTEXT '$text' at position=$pos.");
                    return true;
                }
            }
        }
        //-----------------------------------------------------------|

        return false;
    }

    // returns true if that was a message containing prohibited azeri text.
    // NOTE: first searches in 1) HTML entities and 2) plain text.
    private static function contains_azeri_spam(&$telegram, $chat_id, &$message_structure)
    {
        //-----------------------------------------------------------+
        /*if (isset($message_structure['entities']))
        {
            // 1. search inside entities (HTML-formatted contents)
            $html_entities = $message_structure['entities'];
            foreach ($html_entities as $html_entity)
            {
                if (!isset($html_entity['url']))
                {   continue; // this is just HTML-formatted text with no link: skip it.
                }

                foreach (TG::prohibited_entities_URLs as $url)
                {
                    $pos = strpos($html_entity['url'], $url);
                    if ($pos !== false)
                    {   log_message('error', "---- found prohibited URL '$url' inside ENTITIES: ".$html_entity['url']);
                        return true;
                    }
                }
            }
        }*/
        //-----------------------------------------------------------|


        //-----------------------------------------------------------+
        // 2. search inside plain-text content:
        if (isset($message_structure['text']))
        {
            $text = mb_strtolower($message_structure['text']); // Russian encoding as well!
            foreach(TG::prohibited_azeri_phrases_LOWERCASE_ONLY as $v)
            {
                $pos = strpos($text, $v);
                if ($pos !== false)
                {   log_message('error', "---- found prohibited azeri word '$v' inside PLAINTEXT '$text' at position=$pos.");
                    return true;
                }
            }
        }
        //-----------------------------------------------------------|

        return false;
    }

    // returns true if that was a service message.
    private function handle_service_messages_and_ban_spammers_and_bots(&$telegram, $chat_id, &$message_structure)
    {
        $b_message_to_be_removed = (isset($message_structure['left_chat_participant']) || isset($message_structure['new_chat_participant']));

        if ($b_message_to_be_removed)
        {   $telegram->deleteMessage(array('chat_id' => $chat_id, 'message_id' => $message_structure['message_id']));
            log_message('error', '---- service message (message_id='.$message_structure['message_id'].') removed.');
        }

        // for newcomers also track spammers and block them immediately!
        if (isset($message_structure['new_chat_participant']) && isset($message_structure['new_chat_members']))
        {
            $from_tg_userid = $message_structure['from']['id'];
            if (113088389 == $from_tg_userid) // this is my ID.
            {   return false;
            }

            $new_chat_members = $message_structure['new_chat_members'];
            // ban all the bots added:
            $b_bots_added = false;
            foreach ($new_chat_members as $new_chat_member)
            {
                if (1 == $new_chat_member['is_bot'])
                {
                    $b_bots_added = true;
                    log_message('error', "-- BANNING a BOT: ".print_r($new_chat_member, true));
                    $telegram->kickChatMember(array('chat_id' => $chat_id, 'user_id' => $new_chat_member['id'])); // ban FOREVER.
                    $botname_to_be_banned = isset($new_chat_member['first_name']) ? $new_chat_member['first_name'] : 'id='.$new_chat_member['id'];
                    TelegramHelper::echoTelegram("\xF0\x9F\x91\xAE Бот '$botname_to_be_banned' забанен.");
                }
            }

            if ($b_bots_added)
            {
                // compose human-friendly name to show:
                $from_tg_name = isset($message_structure['first_name']) ? $message_structure['first_name'] : '';
                $from_tg_name .= isset($message_structure['last_name']) ? ' '.$message_structure['last_name'] : '';
                $from_tg_name =  trim($from_tg_name);
                $from_tg_name .= ' ('.$message_structure['id'].')';
                $from_tg_name =  trim($from_tg_name);

                log_message('error', "-- BANNING a bad guy who added bots: '$from_tg_name'");
                $telegram->kickChatMember(array('chat_id' => $chat_id, 'user_id' => $from_tg_userid)); // ban FOREVER.
                TelegramHelper::echoTelegram("\xF0\x9F\x91\xAE Вредитель '$from_tg_name' забанен \xF0\x9F\x98\x88!");
            }
        }

        return $b_message_to_be_removed;
    }

    // returns false if no stop words found or Tg::DETECTED_SPAM or Tg::DETECTED_BAN, correspondingly.
    private function filter__contains_stopwords($text)
    {
        $text = mb_strtolower($text); // Russian encoding as well!

        foreach(TG::advertisement_LOWERCASE_ONLY as $v)
        {
            $pos = strpos($text, $v);
            if ($pos !== false)
            {
                log_message('error', "---- found '$v' in '$text' at position=$pos!");
                log_message('error', "----TTT spam!!!");
                return Tg::DETECTED_SPAM;
                break;
            }
            else
            {   ; //nothing found so far.
            }
        }

        foreach(TG::ban_immediately_LOWERCASE_ONLY as $v)
        {
            $pos = strpos($text, $v);
            if ($pos !== false)
            {
                log_message('error', "---- found '$v' in '$text' at position=$pos!");
                log_message('error', "----TTT ban!!!");
                return Tg::DETECTED_BAN;
                break;
            }
            else
            {   ; //nothing found so far.
            }
        }

        return false;
    }

    //
    // Ensure not to loop into answering to the keyboards from my own searches!!
    private function input_came_from_bot(&$message_structure)
    {

        if (isset($message_structure['via_bot']))
        {
            // NOTE: I can do more checks through the structure:
//            [via_bot] => Array
//            (
//                [id] => 5217243881
//                [is_bot] => 1
//                [first_name] => FAQ bot
//                [username] => chat_assistant7_bot
//            )
            return true;
        }

        return false;
    }

    // returns unordered array of topics related (if any).
    //  1. tokenizes DB key phrases
    //  2. searches for those "tokens" within User's original query text
    //  3. TODO: improve matching algorythm or use out-of-the-box solution (fuzzy search, fulltext, TNTSearch, Porter Stemmer algorithm, )
    //      https://www.findbestopensource.com/product/fre5h-transliteration
    //      https://ourcodeworld.com/articles/read/1124/how-to-implement-a-fuzzy-search-fulltext-engine-using-tntsearch-in-symfony-5
    private function search_in_FAQ_topics($text)
    {

        $text = mb_strtolower($text); // Russian encoding as well!

        // 1. tokenize all search phrases from DB, linked to their original row.
        // 2. search for those phrases within the original text.

        // 1. get these fields only:
        $ci = &get_instance();
        $ci->db->select('id, title, description, LOWER(key_phrases)AS  key_phrases');
        $query = $ci->db->get('faq_topics');
        $topics = Utilitar_db::safe_resultSet($query);

        $topics_tokenized = array();
        foreach ($topics as $topic)
        {
            $key_phrases_tokenized = $topic->key_phrases ? explode("\n", $topic->key_phrases) : array(); // WARNING: tokenization doesn't remove trailing NEWLINE symbol which causes issues during string search!!!!!

            // TODO: I can save memory by MODIFYING the original object row!
            $topics_tokenized[] = array('id'                    => $topic->id,
                                        'title'                 => $topic->title,      // store the reference to save memory
                                        'description'           => $topic->description,// store the reference to save memory
                                        'key_phrases_tokenized' => $key_phrases_tokenized,
                                       );
        }

        // log_message('error', "-- tokenized topics: ".print_r($topics_tokenized, true));

        $topic_ids_found = array();
        $matches = array();
        $topics_processes = 0;
        foreach ($topics_tokenized as $topic)
        {
            $topics_processes++;
            //log_message('error', "---- ITEM #$topics_processes phrases (type=".gettype($topic['key_phrases_tokenized'])."): ".print_r($topic['key_phrases_tokenized'], true));

            if (0 == count($topic['key_phrases_tokenized']))
            {   log_message('error', " NO TOKENS passed. SKIPPING for #$topics_processes!!!!!! SKIPPING.");
                continue; // NO TOKENS passed. SKIPPING
            }

            $topic_found = null;
            foreach ($topic['key_phrases_tokenized'] as $key_phrase)
            {
                $trimmed_phrase = trim($key_phrase);
                if ('' == $trimmed_phrase)
                {   continue;
                }

                $ppp = strpos($text, $trimmed_phrase);

                $describeit = ($ppp === false) ? 'NOT FOUND' : 'FOUND!';
                if(false !== strpos($text, $trimmed_phrase))
                {   $matches[] = $topic;
                    $topic_ids_found[] = $topic['id'];
                    break;
                }
            }
        }

// TODO: later will add statistics tracking.
//        $ci->db->where_in('topic_id', $topic_ids_found);
//        $ci->db->update('faq_statistics', array('$topic_ids_found'));

//        log_message('error', "---- Search by topics: ".count($matches)." topic(s) matched: ".print_r($matches, true));
        log_message('error', "---- Search by topics: ".count($matches)." topic(s) matched");
        return $matches;
    }

    private function kick_off_users(&$users, $chat_id, &$telegram, $reason_text = null)
    {
        if (count($users) <= 0)
        {   return; // nobody to kick off.
        }

        log_message('error', "XXX: going to BAN these users for reason '$reason_text': ".print_r($users, true));

        foreach ($users as $user_id => $first_name)
        {   $telegram->kickChatMember(array('chat_id' => $chat_id, 'user_id' => $user_id)); // ban FOREVER.

            $msg = "\xF0\x9F\x91\xAE Пользователь $first_name забанен";
            if ($reason_text)
            {   $msg .= ' '.$reason_text;
            }
            $msg .= ' (написать <a href="tg://user?id='.$user_id.'">в личку</a>).';
            TelegramHelper::echoTelegram("\xF0\x9F\x91\xAE Пользователь '$first_name' забанен".' (написать <a href="tg://user?id='.$user_id.'">в личку</a>).');
        }
    }

    // TODO: move this to the WizardCensor instead!
    // Function detects if bots joined (and it is not THIS bot) and kicks their ass immediately.
    // It now also bans the user who added those bots!
    // Returns NULL or the list of users banned: to later delete their messages or smth. else. N.B.: the very last item (if the list is not VOID!) contains the bot-adder person!
    private function kick_off_joined_bots(&$message_structure, $chat_id, &$telegram)
    {
        if (!isset($message_structure['new_chat_members']))
        {   return null;
        }
        
        $new_chat_members = $message_structure['new_chat_members'];
        $users_to_ban = array();
        $self_botname_lowercase = strtolower(Util_telegram::get_bot_name());
        foreach ($new_chat_members as $new_chat_member)
        {   // Note that $new_chat_member['username'] is available ONLY if (1 == $new_chat_member['is_bot']). Otherwise that field is OPTIONAL and could be missing.
            if ($new_chat_member['is_bot'] && (0 !== strcmp($new_chat_member['username'], $self_botname_lowercase))) // kick all but ourselves
            {   $users_to_ban[ $new_chat_member['id'] ] = $new_chat_member['first_name'];
            }
        }

        // if there's somebody to ban, then:
        if (count($users_to_ban))
        {   // also identify WHO added those bots? It's not our admin for sure, so ban him, too!
            $who_added_those_bots__tg_user_id   = $message_structure['from']['id'];
            $who_added_those_bots__firstname    = isset($message_structure['from']['first_name']) ? $message_structure['from']['first_name'] : 'Mr. Spammer';
            $users_to_ban[$who_added_those_bots__tg_user_id] = $who_added_those_bots__firstname;
        }

        $this->kick_off_users($users_to_ban, $chat_id, $telegram, 'Bots detected!');
        return $users_to_ban;
    }

    // TODO: move this to the WizardCensor instead!
    // Function detects arabic titles and kicks them off (sorry! no better algorythm came to my mind YET).
    // Returns NULL or the list of users banned: to later delete their messages or smth. else. N.B.: the very last item (if the list is not VOID!) contains the bot-adder person!
    private function kick_off_arabic_names(&$message_structure, $chat_id, &$telegram)
    {
        if (!isset($message_structure['new_chat_members']))
        {   return null;
        }

        $new_chat_members = $message_structure['new_chat_members'];
        $users_to_ban = array();
        foreach ($new_chat_members as $new_chat_member)
        {   // for non-bots determine whether their usernames are arabic:
            if ((!$new_chat_member['is_bot']) && isset($new_chat_member['first_name']) && ($this->filter__contains_non_allowed_encoding($new_chat_member['first_name'])) )
            {   $users_to_ban[ $new_chat_member['id'] ] = $new_chat_member['first_name'];
            }
        }

        // if there's somebody to ban, then:
        if (count($users_to_ban))
        {
            // also identify WHO added those bots? It's not our admin for sure, so ban him, too!
            $who_added_those_bots__tg_user_id   = $message_structure['from']['id'];
            $who_added_those_bots__firstname    = isset($message_structure['from']['first_name']) ? $message_structure['from']['first_name'] : 'Mr. Spammer';
            $users_to_ban[$who_added_those_bots__tg_user_id] = $who_added_those_bots__firstname;
        }

        $this->kick_off_users($users_to_ban, $chat_id, $telegram, 'Arabic text detected!');

        return $users_to_ban;
    }

    // TODO: move this to the WizardCensor instead!
    private function filter__contains_non_allowed_encoding($text)
    {
        // Alternative way: if (preg_match("/^[\x0600-\x06FF]/i", $subject) > 0);  // see https://stackoverflow.com/questions/3541371/php-how-do-i-detect-if-an-input-string-is-arabic

        $encoding_name = mb_detect_encoding($text);
        if ('UTF-8' == $encoding_name)
        {
            if (preg_match('/[اأإء-ي]/ui', $text))
            {
                log_message('error', "XXX this is arabic: ".$text);
                return true;
            }
        }
        
//        log_message('error', "XXX this IS NOT arabic: ".$text);
        return false;
    }

    // TODO: move this to the WizardCensor instead!
    private function filter__contains_APK_attachments(&$message_structure)
    {
        return (isset($message_structure['document']) && (0 == strcmp($message_structure['document']['mime_type'], 'application/vnd.android.package-archive')));
    }

    // saves only non-inline messages! I.e. the ones that user TYPED IN manually.
    private function save_received_typed_in_msg_id(&$input_data, $tg_user_id, $tg_chat_id)
    {
        $ref = null;
        if (isset($input_data['message']))
        {   $ref = $input_data['message'];
        }
        else if (isset($input_data['edited_message']))
        {   $ref = $input_data['edited_message'];
        }
        else if (isset($input_data['callback_query']))
        {   //$ref = $input_data['callback_query']['message'];
            //log_message('error', "-- skipping saving inline message id.");
            return;
        }

        if (!$ref)
        {   log_message('error', "---- save_received_typed_in_msg_id() error retrieving message id!");
            return;
        }

        $msg_id_received = $ref['message_id'];
        TelegramHelper::set_last_received_message_id($tg_user_id, $tg_chat_id, $msg_id_received);
    }

    // if it's a user with Guest role and "Guest" name then update our records with its firstname/lastname
    private function set_tg_username_if_needed(&$current_user, &$input_data)
    {
        if (!$current_user)
        {   log_message('error', "---- ERROR: set_tg_username_if_needed(): for some reason the CURRENT_USER object is NULL?".print_r($current_user, true)."\n and input data is: ".print_r($input_data, true));

//            if (isset($input_data['message']) && isset($input_data['message']['from']) && isset($input_data['message']['from']))
//[message] => Array
//        (
//            [message_id] => 18442
//            [from] => Array
//                (
//                    [id] => 270734309
//                    [is_bot] =>
//                    [first_name] => Ksu
//                    [last_name] => Ksu
//                    [username] => kirsanovaksu

        // NOTE: I can extract tg user id from this structure:
//        [from] => Array
//                (
//                    [id] => 113088389
//                    [is_bot] =>
//                    [first_name] => GK
//                    [username] => WallTouch

            return;
        }

        if (WizardHelper::USER_ROLE__GUEST != $current_user->role)
        {   log_message('error', "---- WARNING current user exists, but its role='$current_user->role'. Skipping set_tg_username_if_needed().");
            return; // authorized users expected to be given names when creating tickets. That name to not be overwritten by a user.
        }

        // data may come from different type of messages (i check three of them):
        $message_ref = null;
        if (isset($input_data['message']))
        {   $message_ref = $input_data['message'];
        }
        else if (isset($input_data['edited_message']))
        {   $message_ref = $input_data['edited_message'];
        }
        else if (isset($input_data['callback_query']))
        {   $message_ref = $input_data['callback_query'];
        }

        if (!$message_ref)
        {   log_message('error', "set_tg_username_if_needed() found NO message ref in input data. Skipping");
            return; // authorized users expected to be given names when creating tickets. That name to not be overwritten by a user.
        }


        // update the name only if it is 'Guest'/'Гость':
        if ((0 == strcmp($current_user->ticket_owner_name, 'Guest')) ||
            (0 == strcmp($current_user->ticket_owner_name, 'Гость'))
           )
        {
            $from = $message_ref['from'];
            if (1 == $from['is_bot'])
            {   return; // the message is from bot.
            }

            $new_name = '';
            if (isset($from['first_name']))
            {   $new_name .= $from['first_name'].' ';
            }

            if (isset($from['last_name']))
            {   $new_name .= $from['last_name'].' ';
            }

            if (isset($from['username']))
            {   $new_name .= '(@'.$from['username'].') ';
            }

            $new_name = trim($new_name);

            $this->db->where(array( 'bot_name'  => Util_telegram::get_bot_name(),
                                    'tg_user_id'=> $current_user->tg_user_id,
                                    'role'      => WizardHelper::USER_ROLE__GUEST,
                                  )
                            );
            $this->db->update(Util_telegram::TBL__TG_USERS, array('ticket_owner_name' => $new_name));
        }
    }

    private function create_trello_task_via_email($trello_email_for_tasks, &$posted_data)
    {
        if (strlen($trello_email_for_tasks) <= 0)
        {   log_message('error', "No Trello-board email specified for yid:".$posted_data['yurlico_id'].", organization: '".$posted_data['organization_name']."'. Skipping task creation via email.");
            return;
        }

        $yurlico_id = $posted_data['yurlico_id'];
        $message = $this->load->view('telegram/tinkoff_reservation_details_for_trello', $posted_data, true);

        $subject = '#red Запись через АСД на 123, Tел.: жежерсфсдфсдфд - онлайн-запись через бот-помощник WallTouch.ru';


        $ci = &get_instance();
        $data       = $ci->utilitar->get_defaultEmailOptions($subject, $message);
        $data['from']= 'bots@walltouch.ru'; // override the default one.
        $data['to'] = $trello_email_for_tasks;//'walltouch+viwtwc1xt4vcirbghkkk@boards.trello.com';
        log_message('error', "XXX Trello task via emai created for yid:".$yurlico_id);

        $data['bcc']  = 'info@walltouch.ru';
        $ci->utilitar->send_email($data, false);
    }


    public function process($command = '')
    {
        $telegram = new Telegram(Util_telegram::get_bot_token());

        $this->load->model('util_telegram');
        $this->load->model('util_properties');

        $input_data = $this->security->xss_clean(json_decode(trim(file_get_contents('php://input')), true));
        //log_message('error', "TG input_data = ".print_r($input_data['message'], true));
//        log_message('error', "ZZZ input_data1: ".print_r($input_data, true));
//        log_message('error', "--- TG ENTIRE DATA: ".print_r($telegram, true));

        if (isset($input_data['my_chat_member']))
        {   log_message('error', "---- Tg->process(): skipping service message 'my_chat_member'!");
            return;// this is service message: generated when the permissions for somebody have been changed by admin!
        }

        $tg_user_id = $telegram->UserId();
        $tg_chat_id = $telegram->ChatID();

        $this->pass_current_tg_user_id__bot_name__to_father($tg_user_id, Util_telegram::get_bot_name()); // IMPORTANT: only after this method all other TH methods become valid!
        WizardHelper::set_TG_context($tg_user_id, $tg_chat_id, $input_data); // only this call will ensure that WizardHelper::get_current_user() will work correctly HERE. :(

        $current_user = WizardHelper::get_current_user();
        if (!$current_user)
        {   log_message('error', "----------- ERROR: TG current_user is NULL!");
        }

        // auto-update (each time?) tg user name based on incoming settings:
        $this->set_tg_username_if_needed($current_user, $input_data);

        $this->save_received_typed_in_msg_id($input_data, $tg_user_id, $tg_chat_id);// for future use: we possibly can edit the messages RECEIVED.

        // verify whether that's a message from the chat we're administering against spammers:
        $msg_struct = TelegramHelper::extract_supported_message_struct($input_data);
        if ($msg_struct)
        {
            log_message('error', "---- TG process(): msg_struct: ".print_r($msg_struct, true));
//            log_message('error', "XXX msg_struct: uncomment to see it.");
            if (WizardCensor::belongs_to_censored_chats($msg_struct))
            {
                if (WizardCensor::posted_by_chat_admin($msg_struct))
                {   log_message('error', "---- TG posted_by_chat_admin(): TRUE. So NO CENSORSHIP for chat admins!!");
                    return; // NO CENSORSHIP for chat admins!! :)
                }

                $this->handleCensorship($telegram, $msg_struct);
                return;
            }
            else if (false !== $from_chat_id = WizardCensor::is_a_forwarded_message($msg_struct))
            {   // this bot accepts forwards *from chat admins* only. Forwards are useful to deal with messages/threads. E.g. ban this or that thread, etc.
                // [date] => 1653116498
                // [forward_sender_name] => Вета
                // [forward_date] => 1652932283
                // [text]
                log_message('error', "RRRR this is a forwarded message from chat_id=$from_chat_id: ".print_r($msg_struct, true));
            }
            else
            {   log_message('error', "---- NOTE: this IS NOT A CENCORED CHAT. Shall we do ANYTHING HERE then? No, just SILENTLY quit w/out posting ANYTHING.\nAssuming that NO STATE CHANGE is needed!!!");
            }
        }
        else if (isset($input_data['callback_query']))
        {   ; // we're ok to proceed with callbacks below.
        }
        else if (isset($input_data['inline_query']))
        {   ; // we're ok to proceed with inline_query callbacks below.
        }
        else
        {   log_message('error', "ERROR: Cannot find any message or edited_message to handle!");
            return;
        }

//        $command = isset($input_data['message']['text']) ? $input_data['message']['text'] : $command; // accept either GET or POST. Might be a security issue?
        if (isset($input_data['message'])) // read data from the regular messages
        {   $command = isset($input_data['message']['text']) ? $input_data['message']['text'] : $command; // accept either GET or POST. Might be a security issue?
        }
        else if (isset($input_data['callback_query'])) // read it from a callback query. NOTE: It must provide a "$callback_data" parameter when calling a "buildInlineKeyBoardButton()"!
        {
            $command = isset($input_data['callback_query']['data']) ? $input_data['callback_query']['data'] : $command; // accept either GET or POST. Might be a security issue?
            log_message('error', "NOTE: TG controller recognized a CALLBACK query! command='$command'");
        }

        // Let's cut out the numbers from the command itself. E.g. "Семинары: 2" must become "Семинары", etc.
        $unstripped_command = $command; // now $unstripped_command makes no sense as we don't strip $command ANYMORE. TODO: Refactor according to this note later!!!
        log_message('error', "TG command = '$command'");

        //-----------------------------------------------------------+
        //  We also suport if user pasts the code right into the text message (as we've seen lots of times before:
        // for https://t.me/school_new_age_bot?start=invidVyk7WmT8
        $bot_fullpath_with_invite1 = 'https://t.me/'.Util_telegram::get_bot_name().'?start=invid';
        $bot_fullpath_with_invite2 = 'https://telegram.me/'.Util_telegram::get_bot_name().'?start=invid';
        //-----------------------------------------------------------+

        $tg_user = Util_telegram::get_tg_user_authorized_first($tg_user_id);
        if (!$tg_user)
        {   log_message('error', "---- ERROR ---- ERROR ---- ERROR: no tg_user existed and ALSO FAILED to create new one :(");
        }

        switch ($command)
        {
            case '/ifttt':
                // echoTelegram($message)
                {
                    $another_bot_id = '152462940:AAGFliyLssW0pMhJUgdCQNeJCGtELOSXZF0'; // my test bot, not linked to a webhook (this is a must as otherwise sending direct mesasges won't work!)
                    $message        = 'Что нам посмотреть дополнительно?';
                    $telegram       = new Telegram($another_bot_id);
                    $tg_chat_id     = $telegram->ChatID();
                    $message        = str_replace(array('\n', '-', '.'), chr(10), $message);
                    $content        = array('chat_id' => $tg_chat_id, 'text' => $message);
                    $telegram->sendMessage($content);
                }

                // triggering a custom event to my IFTTT account:
                log_message('error', "XXX: before processing IFTTT...");

// note: "value2" is a value recognized by ITFFF on their website inside Maker's options. NOTE: case-sensitive!!
                $this->endpoint_IFTTT('anotherEvent567', array('key1' => 'tt1val', 'value2' => 'tt2val', 'gugush' => 'bebe'));
                log_message('error', "XXX: after processing IFTTT.");

                break;

            case '/start':
                // wizard's chaining is possible:
                WizardHelper::terminateAllRunningWizards($tg_user_id, $tg_chat_id);

                // a mandatory call for each wizard! Sets the wizard's state:
                WizardStateManager::initiateWizard($tg_user_id, $tg_chat_id, WizardFather::NAME);
                WizardStateManager::setCurrentWizardNameAndStepNo($tg_user_id, $tg_chat_id, WizardFather::NAME, 0);


                $this->wh->runWizard($tg_user_id, $tg_chat_id, NULL, $unstripped_command, $input_data);
                break;

            default:
            {
                // if we are in a wizard then direct input to the wizard:
                $current_wizard_name = WizardStateManager::getCurrentWizardName($tg_user_id, $tg_chat_id);
                //$current_wizard_stepno = WizardStateManager::getCurrentWizardStepNumber($tg_user_id, $tg_chat_id);
                if ($current_wizard_name)
                {   $this->wh->runWizard($tg_user_id, $tg_chat_id, $command, $unstripped_command, $input_data);
                    return;
                }
                else
                {   log_message('error', "No running wizard detected! The command passed in: $command");
                    WizardStateManager::initiateWizard($tg_user_id, $tg_chat_id, WizardFather::NAME);
                    WizardStateManager::setCurrentWizardNameAndStepNo($tg_user_id, $tg_chat_id, WizardFather::NAME, 0);

                    $this->wh->runWizard($tg_user_id, $tg_chat_id, NULL, $unstripped_command, $input_data);
                }

//                $content = array('chat_id' => $tg_chat_id, 'text' => "Команда '$command' не распознана (wizard: $current_wizard_name)");
//                $telegram->sendMessage($content);
//                echo 'Command not supported';
                break;
            }
        }
    }

    private function endpoint_IFTTT($api, array $content/*, $post = true*/)
    {
    //  'https://maker.ifttt.com/use/sBmYSM4aVdIc31-dI0tcE'
        $trigger_event_url_IFTTT = 'https://maker.ifttt.com/trigger/'.$api.'/with/key/deh87gKFLwnW2gMPsqKv86';
$post = true; // enforcing POST to pass input params to it.
        $url    = $trigger_event_url_IFTTT;

        $reply = ($post ? $this->sendAPIRequest($url, $content) : $this->sendAPIRequest($url, array(), false));

        return json_decode($reply, true);
    }

     private function sendAPIRequest($url, array $content, $post = true)
     {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if ($post)
        {   curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $content);
        }
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    private function _send_personal_msg($tg_chat_id, $message)
    {
        $telegram   = new Telegram(Util_telegram::get_bot_token()); // TODO: refactor this to use chat_id, user_id - instead of instantiating entire complex class for sending messages.
        $message    = str_replace(array('\n'), chr(10), $message);

        // create the data to be sent to TG:
        $content    = array('chat_id' => $tg_chat_id, 'text' => $message);

        // enrich it:
        $content['parse_mode'] = 'HTML'; // rich-text formatting?
        $content['disable_web_page_preview'] = true;

        $buttons_array = array(array('Вы сделали мой день', 'Замечательно'), WizardBase::GO_BACK);
        if ($buttons_array)
        {
            // Create a permanent custom keyboard
//            $keyb = $telegram->buildKeyBoard(array($buttons_array), $onetime=false, $resize = true);
//            $keyb = $telegram->buildKeyBoard($buttons_array, $onetime=false, $resize = false);
            Tg::ensureKeyboardIsCorrect($buttons_array);
            $keyb = $telegram->buildKeyBoard($buttons_array, $onetime=false, $resize = true);
            $content['reply_markup'] = $keyb;
//            log_message('error', "keyboard to be built=".print_r($buttons_array, true));
        }

        //  send the message:
        $message_delivered = $telegram->sendMessage($content);
        log_message('error', "Message delivered OK to tg user id:$tg_chat_id");
    }

    // ensures array item exists or returns NUL.
    public static final function safe_get(&$arr, $index)
    {
        $ci = &get_instance();
        $ci->load->helper('security');

        if (isset($arr[$index]))
        {   return $ci->security->xss_clean($arr[$index]);
        }

        return NULL;
    }

    public function mr_test()
    {
        $tg_chat_id_4_Garik = 113088389;

        Json_base::set_json_header();
        $this->_send_personal_msg($tg_chat_id_4_Garik, 'hello to 419-4');
        // $this->_send_personal_msg($tg_chat_id_4_David, 'hello to 419-4');

        Json_base::success('успешно');
	}

    // M-ROUT mesasges receival goes here.
    public function mr_messages()
    {
        Json_base::set_json_header();

        $postData = json_decode($this->input->raw_input_stream, true);
        $messages = Tg::safe_get($postData, 'Messages');

        if (!$messages)
        {   Json_base::error400('No messages posted.');
            return;
        }

        // calc some stats:
        $reversal_count     = 0;
        $institutions       = array();
        $ranges             = array();
        $transaction_types  = array();
        $transaction_places = array();
        $phones = array();
        foreach ($messages as $message)
        {
            // split by major counterparts of each event:
            $general    = $message['General'];
            $transaction= $message['Trxn'];

            // gather some stats here:
            $institutions[$general['Institution']]  = true;
            $ranges[$general['RangeID']]            = true;
            $transaction_types[$general['TrxnType']]= true;

            $phones[]= $general['PhoneNumber'];
            $phones[]= $transaction['IsReversal'];
            if ($transaction['IsReversal'])
            {   $reversal_count++;
            }

            $transaction_places[]= $transaction['TrxnPlace'];
        }

        $tg_chat_id_4_Garik = 113088389;
        $tg_chat_id_4_David = 320385147;
        $tg_chat_id_4_Mher  = 75599805;
        $msg = "Принято <strong>".count($messages)."</strong> сообщений.";

        $msg .= "\nInstitutions: "      .implode(', ', array_keys($institutions));
        $msg .= "\nRanges: "            .implode(', ', array_keys($ranges));
        $msg .= "\nTransaction types: " .implode(', ', array_keys($transaction_types));
        $msg .= "\nTransaction places: ".implode(', ', array_keys($transaction_places));
        $msg .= "\nPhones: "            .implode(', ', $phones);
        $msg .= "\nReversal ops count: $reversal_count";

        // notify our team:
        $this->_send_personal_msg($tg_chat_id_4_David, $msg);
        $this->_send_personal_msg($tg_chat_id_4_Garik, $msg);
        $this->_send_personal_msg($tg_chat_id_4_Mher, $msg);

        Json_base::success(count($messages).' записей импортированы успешно.');
    }

    // Returns date/time difference in TOTAL MINUTES count.
    // If negative then the meeting DIDN'T HAPPEN (and abs(minutes) shows how many minutes remain), if positve then the event has PASSED already (and abs(minutes) shows how many minutes passed).
    public static function calc_difference_server_now__and_event_datetime(&$meeting)
    {
        // 1. get the NOW in event's (!) timezone:
        $mtg_timezone = $meeting->meeting_timezone ? $meeting->meeting_timezone : 'Europe/Moscow';

        // NOTE: only MANUALLY setting the timezone AFTER createing a DateTime works!!!
        // This sounds stupid (as DateTime constructor accepts Timezone) but constructor DOES NOT work: only MANUAL setting AFTER datetime creation helps!!!
        $tz = new DateTimeZone($mtg_timezone);
        $dt_srv = DateTime::createFromFormat('Y-m-d H:i', date('Y-m-d H:i')); // in server timezone!
        //$servertime_before_TZ = $dt_srv->format('Y-m-d H:i:s');
        $dt_srv->setTimezone($tz); // A MUST-have to SET IT EXPLICITLY (and not from DateTime constructor - it doesn't work that way!!!)
        //$servertime_after_TZ = $dt_srv->format('Y-m-d H:i:s');

        $strmtgdatetime = $meeting->date.' '.$meeting->time_from;
        $dt_event = DateTime::createFromFormat('Y-m-d H:i', $strmtgdatetime, $tz);
        if (!$dt_event)
        {   log_message('error', "---- ERROR ERROR ERROR calc_difference_server_now__and_event_datetime() in creating datetime createFromFormat('Y-m-d H:i', '$strmtgdatetime', mtg tz: '$mtg_timezone')");
            return 0;
        }

        $now_minutes_total  = Tg::_calc_total_minutes_of_datetime($dt_srv);
        $event_minutes_total= Tg::_calc_total_minutes_of_datetime($dt_event);
        $minutes_difference = $now_minutes_total - $event_minutes_total;
        log_message('error', "---- minutes_difference=$minutes_difference between NOW:".$dt_srv->format('Y-m-d H:i:s').' and event:'.$dt_event->format('Y-m-d H:i:s').' for '.$meeting->name);

        return $minutes_difference;
    }

    public static function _calc_total_minutes_of_datetime(&$datetime_obj)
    {
        //$years  = (int)$datetime_obj->format('Y');
        $months = (int)$datetime_obj->format('n');
        $days   = (int)$datetime_obj->format('j');
        $hours  = (int)$datetime_obj->format('G');
        $minutes= (int)$datetime_obj->format('i');

        return ($minutes + $hours*60 + $days*24*60 + $months*30*24*60);
    }

    // returns an array with updates messages.
    private static function get_bot_updates()
    {

        $msg = "\xE2\x84\xB9";
        $msg .= " Bot new version has been rolled out \xE2\x9C\xA8\n";
        $msg .= "Most of the efforts put to interface improvements:\n\n";
        $msg .= "1. <b>Calendar</b> made more compact: collapsing past months' events into a single button\n\n";
        $msg .= "2. Personal Subscriptions menu (now '<b>My Events</b>') now shows past events in a separate '\xF0\x9F\x93\xA6 Archive' button (differentiating upcoming events from passed ones)\n\n";
        $msg .= "3. <b>Multilanguage support</b> improved for Russian version\n";

        // add new updates to the end of the array!
        return array(
            $msg,
        );
    }

    // reminds about the event to all subscribed ("yes" and "maybe")
    public function notify_about_bot_update()
    {
        $ci = &get_instance();
        $ci->load->model('utilitar_db');
        $ci->load->model('util_telegram');

        $telegram   = new Telegram(Util_telegram::get_bot_token()); // TODO: refactor this to use chat_id, user_id - instead of instantiating entire complex class for sending messages.
        $options = array('disable_web_page_preview' => true);

        $closing_words = "\n\nFeel free sending your suggestions or bugs found to @walltouch \xF0\x9F\x98\x8E";
        $bot_updates = Tg::get_bot_updates();
        $msg = end($bot_updates);
        $msg .= $closing_words;

        $query = $ci->db->get('tg_users');
        $tg_users = Utilitar_db::safe_resultSet($query);

        $message = str_replace(array('\n'), chr(10), $msg); // do a message cleanup.

        foreach ($tg_users as $tg_user)
        {
            $tg_user_id = $tg_user->tg_user_id;

            TelegramHelper::hideRegularButtons($message, $tg_user_id); // cleaning up REGULAR BUTTONS (if any). Useful when RETURNING (actually cancelling) from Geo-location assignment menu.

            $extra_actions = array();
            $extra_actions['OK, clear'] = TelegramHelper::wrap_button_action_name(WC_S1::ACTION__STREAM_SHOW_EVENTS);
            $grouped_buttons = TelegramHelper::to_callback_grouped_buttons(WC_S1::NAME, __CLASS__, $extra_actions);

            $message_delivered =  TelegramHelper::echo_inline("\xF0\x9F\x99\x8F Thanks for staying with us!", $grouped_buttons, $options, $tg_user_id);
        }

        echo "<br />Update msg sent to ".count($tg_users).' participants!';
    }
}
