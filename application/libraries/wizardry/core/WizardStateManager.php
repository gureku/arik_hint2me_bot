<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'libraries/wizardry/core/WizardHelper.php');

class WizardStateManager
{
    const TBL__WIZARD_STATE  = 'tg_wizard_state';
    const TBL__STEPS_DATA    = 'tg_wizards_steps';

    //
    // NOTE:    calling this is a must for clean start. This creates required structures for the begin.
    //
    //  NOTE:   This method also allows initing Wizard by arbitrary (hopefully valid one!) step #. That is useful
    //          when Wizard contains a set of independent actions which could be executed at any time.
    public static function initiateWizard($tg_user_id, $tg_chat_id, $wizard_name, $step_no = 0)
    {
//        log_message('error', "    WSM: initiateWizard($tg_user_id, $tg_chat_id, $wizard_name) with step # $step_no");
        WizardStateManager::discardWizard($tg_user_id, $tg_chat_id, $wizard_name); // discard all other wizards (if any) for that user.
        WizardStateManager::saveStep($tg_user_id, $tg_chat_id, $wizard_name, $step_no, 'inition_wzrd'); // this only sets current wizard name, step # for the further usage. Metadata saved separately, at the upper level.
    }

    // returns the data stored in DB for a particular step.
    // NOTE: uses CURRENT TELEGRAM USER account for data retrieval.
    public static function getStepData($tg_user_id, $tg_chat_id, $wizard_name, $step_no)
    {
        $where_array = WizardStateManager::getDbFilter($tg_user_id, $tg_chat_id, $wizard_name, $step_no);
        return WizardStateManager::getState($where_array);
    }

    public static function getThisStepData(&$this_step_object)
    {
        $tg_context = WizardHelper::get_TG_context();
        $tg_user_id = $tg_context['tg_user_id'];
        $tg_chat_id = $tg_context['tg_chat_id'];

        $step_no    = $this_step_object->owning_wizard->getStepIndex(get_class($this_step_object));
        return WizardStateManager::getStepData($tg_user_id, $tg_chat_id, $this_step_object->owning_wizard->getName(), $step_no);
    }

    public static function getStepData_v2(&$owning_wizard_obj, $step_classname)
    {
        $tg_context = WizardHelper::get_TG_context();
        $tg_user_id = $tg_context['tg_user_id'];
        $tg_chat_id = $tg_context['tg_chat_id'];

        $step_no    = $owning_wizard_obj->getStepIndex($step_classname);
//        log_message('error', "WSM::getStepData_v2 step_no = $step_no for $step_classname of wizard ".$owning_wizard_obj->getName());
        return WizardStateManager::getStepData($tg_user_id, $tg_chat_id, $owning_wizard_obj->getName(), $step_no);
    }

    // returns the data stored in DB for a particular step.
    // NOTE: uses CURRENT TELEGRAM USER account for data retrieval.
    public static function getStepsAllData($tg_user_id, $tg_chat_id, $wizard_name)
    {
        $where_array = WizardStateManager::getDbFilter($tg_user_id, $tg_chat_id, $wizard_name);
        return WizardStateManager::getStatesRows($where_array);
    }

    // Get the data stored for a wizard (table "tg_wizard_state"). Ideally this data to be saved  in each step's extra fields (callback_messaging_id, callback_data) but as per today i FAILED making data saving to work correctly :(
    public static function getStepCallbackData($tg_user_id, $tg_chat_id, &$step_obj)
    {
        $ci = &get_instance();

        $wizard_name= $step_obj->owning_wizard->getName();
        $step_no    = $step_obj->owning_wizard->getStepIndex(get_class($step_obj));
        log_message('error', "---- WSM getStepCallbackData($tg_user_id, $tg_chat_id), ownerwizard:'$wizard_name', class step_no:$step_no");

        $where_array = WizardStateManager::getDbFilter($tg_user_id, $tg_chat_id, $wizard_name);
        $where_array['step_no'] = $step_no;

        //log_message('error', "XXX getcallbackdata: LET's CHECK the where_array: ".print_r($where_array, true).', classname='.get_class($step_obj));
        $ci->db->where($where_array);
        $query = $ci->db->get(WizardStateManager::TBL__WIZARD_STATE);
        $row = Utilitar_db::safe_resultRow($query);
        if (!$row)
        {   return null;
        }

        // also check that we either got some data or not:
        if (!$row->callback_messaging_id && !$row->callback_data)
        {   return null;
        }

        return array('callback_messaging_id' => $row->callback_messaging_id, 'callback_data' => $row->callback_data);
    }

    // useful after handling NON-CALLBACK commands.
    public static function clearStepCallbackData($tg_user_id, $tg_chat_id, &$step_obj)
    {
        $ci = &get_instance();

        $wizard_name= $step_obj->owning_wizard->getName();
        $step_no    = $step_obj->owning_wizard->getStepIndex(get_class($step_obj));

        $where_array = WizardStateManager::getDbFilter($tg_user_id, $tg_chat_id, $wizard_name);
        $where_array['step_no'] = $step_no;

        $update_array = array('callback_messaging_id' => null, 'callback_data' => null);
        $ci->db->where($where_array);
        $ci->db->update(WizardStateManager::TBL__WIZARD_STATE, $update_array);
    }

    // useful after handling NON-CALLBACK commands.
    public static function clearStepCallbackData_explicit($tg_user_id, $tg_chat_id, $wizard_name, $step_no)
    {
        $ci = &get_instance();

        $where_array = WizardStateManager::getDbFilter($tg_user_id, $tg_chat_id, $wizard_name);
        $where_array['step_no'] = $step_no;

        $update_array = array('callback_messaging_id' => null, 'callback_data' => null);
        $ci->db->where($where_array);
        $ci->db->update(WizardStateManager::TBL__WIZARD_STATE, $update_array);
    }

    // NOTE: Removes emojii symbols from the $step_value!!! Later during comparison this could bring DIFFERENCES if compared to DB state rather than RunTime state (i.e. in class variables)!
    // (over)writes the step data:
    public static function saveStep($tg_user_id, $tg_chat_id, $wizard_name, $step_no, $step_value)
    {
        $callback_query__message_id = TelegramHelper::get_callback_query__message_id(); // it is -1 if a non-callback query called.
        log_message('error', "XXX WSM: saveStep($tg_user_id, $tg_chat_id, wizard_name:$wizard_name, step_no:$step_no, step_value:'$step_value'), callback_query__message_id:$callback_query__message_id");
        $ci = &get_instance();

//        log_message('error', "before removing emojii: $step_value");
        $decodedEmojiis = WizardStateManager::decodeEmoticons($step_value);
//        log_message('error', "WizardStateManager::decodeEmoticons() == $decodedEmojiis FROM $step_value");

        $step_value  = WizardStateManager::removeEmoji($step_value); // emojiis deny storing the string into the DB!
//        $step_value = mb_convert_encoding($step_value, 'UTF-8');
//        log_message('error', "AFTER removing emojii: $step_value");

        $where_array = WizardStateManager::getDbFilter($tg_user_id, $tg_chat_id, $wizard_name, $step_no);
        $row         = WizardStateManager::getState($where_array);
        if ($row)
        { // update needed:
            log_message('error', "saveStep(): UPDATING data ($tg_user_id, $tg_chat_id, $wizard_name, $step_no, $step_value).");
            $ci->db->where($where_array);

            $update_array = array('step_data' => $step_value);
            $ci->db->update(WizardStateManager::TBL__STEPS_DATA, $update_array);
        }
        else
        { // insert needed:
            log_message('error', "saveStep(): INSERTING data ($tg_user_id, $tg_chat_id, $wizard_name, $step_no, $step_value).");
            $where_array['step_data']   = $step_value; // ammend a step by the "userdata".
            $ci->db->insert(WizardStateManager::TBL__STEPS_DATA, $where_array);
        }
    }

    // NOTE: Removes emojii symbols from the $step_value!!! Later during comparison this could bring DIFFERENCES if compared to DB state rather than RunTime state (i.e. in class variables)!
    // (over)writes the step METADATA:
    public static function saveStepMetadata(&$owning_wizard_obj, $step_classname, $step_metadata)
    {
        $step_no    = $owning_wizard_obj->getStepIndex($step_classname);
        WizardStateManager::saveStepMetadataByStepNo($owning_wizard_obj, $step_no, $step_metadata);
    }


    // NOTE: Removes emojii symbols from the $step_value!!! Later during comparison this could bring DIFFERENCES if compared to DB state rather than RunTime state (i.e. in class variables)!
    // (over)writes the step METADATA:
    public static function saveStepMetadataByStepNo(&$owning_wizard_obj, $step_no, $step_metadata)
    {
        $tg_context = WizardHelper::get_TG_context();
        $tg_user_id = $tg_context['tg_user_id'];
        $tg_chat_id = $tg_context['tg_chat_id'];

//        log_message('error', "    WSM: saveStep($tg_user_id, $tg_chat_id, $wizard_name, $step_no, '$step_metadata')");
        $ci = &get_instance();

//        log_message('error', "before removing emojii: $step_metadata");
        $decodedEmojiis = WizardStateManager::decodeEmoticons($step_metadata);
//        log_message('error', "WizardStateManager::decodeEmoticons() == $decodedEmojiis FROM $step_metadata");

        $step_metadata_value  = WizardStateManager::removeEmoji($step_metadata); // emojiis deny storing the string into the DB!
//        $step_metadata = mb_convert_encoding($step_metadata_value, 'UTF-8');
//        log_message('error', "AFTER removing emojii: $step_metadata_value");

        $where_array = WizardStateManager::getDbFilter($tg_user_id, $tg_chat_id, $owning_wizard_obj->getName(), $step_no);
        $row         = WizardStateManager::getState($where_array);
        if ($row)
        { // update needed:
            log_message('error', "UPDATING METADATA ($tg_user_id, $tg_chat_id, ".$owning_wizard_obj->getName().", $step_no, $step_metadata_value).");
            $ci->db->where($where_array);
            $ci->db->update(WizardStateManager::TBL__STEPS_DATA, array('metadata' => $step_metadata_value));
        }
        else
        { // insert needed:
            log_message('error', "INSERTING METADATA ($tg_user_id, $tg_chat_id, ".$owning_wizard_obj->getName().", $step_no, $step_metadata_value).");
            $where_array['metadata'] = $step_metadata_value; // ammend a step by the "userdata".
            $ci->db->insert(WizardStateManager::TBL__STEPS_DATA, $where_array);
        }
    }

    public static function equal_no_emoji($str1, $str2)
    {
        $str1_cleansed = WizardStateManager::removeEmoji($str1);
        $str2_cleansed = WizardStateManager::removeEmoji($str2);
        return ($str1_cleansed === $str2_cleansed);
    }

    // TODO: what about encode emotions?
    static public function decodeEmoticons($src) {
        $replaced = preg_replace("/\\\\u([0-9A-F]{1,4})/i", "&#x$1;", $src);
        $result = mb_convert_encoding($replaced, "UTF-16", "HTML-ENTITIES");
        $result = mb_convert_encoding($result, 'utf-8', 'utf-16');
        return $result;
    }

    // N.B. this SEEMS to be working BETTER in some cases!
    // I just need time to verify the replacement of removeEmoji()   :(
    public static function removeEmoji($text){
        return trim(preg_replace('/([0-9|#][\x{20E3}])|[\x{00ae}|\x{00a9}|\x{203C}|\x{2047}|\x{2048}|\x{2049}|\x{3030}|\x{303D}|\x{2139}|\x{2122}|\x{3297}|\x{3299}][\x{FE00}-\x{FEFF}]?|[\x{2190}-\x{21FF}][\x{FE00}-\x{FEFF}]?|[\x{2300}-\x{23FF}][\x{FE00}-\x{FEFF}]?|[\x{2460}-\x{24FF}][\x{FE00}-\x{FEFF}]?|[\x{25A0}-\x{25FF}][\x{FE00}-\x{FEFF}]?|[\x{2600}-\x{27BF}][\x{FE00}-\x{FEFF}]?|[\x{2900}-\x{297F}][\x{FE00}-\x{FEFF}]?|[\x{2B00}-\x{2BF0}][\x{FE00}-\x{FEFF}]?|[\x{1F000}-\x{1F6FF}][\x{FE00}-\x{FEFF}]?/u', '', $text));
    }
    // it is a must to call before storing in a DB.
    public static function removeEmoji_sometimes_doesnot_work($text)
    {
        $clean_text = "";

        // Match Emoticons
        $regexEmoticons = '/[\x{1F600}-\x{1F64F}]/u';
        $clean_text = preg_replace($regexEmoticons, '', $text);

        // Match Miscellaneous Symbols and Pictographs
        $regexSymbols = '/[\x{1F300}-\x{1F5FF}]/u';
        $clean_text = preg_replace($regexSymbols, '', $clean_text);

        // Match Transport And Map Symbols
        $regexTransport = '/[\x{1F680}-\x{1F6FF}]/u';
        $clean_text = preg_replace($regexTransport, '', $clean_text);

        // Match Miscellaneous Symbols
        $regexMisc = '/[\x{2600}-\x{26FF}]/u';
        $clean_text = preg_replace($regexMisc, '', $clean_text);

        // Match Dingbats
        $regexDingbats = '/[\x{2700}-\x{27BF}]/u';
        $clean_text = preg_replace($regexDingbats, '', $clean_text);

        return trim($clean_text);
    }

    // useful for StepBackException handling to avoid the trash:
    public static function discardStep($tg_user_id, $tg_chat_id, $wizard_name, $step_no)
    {
//        log_message('error', "    WSM: deleteStep($tg_user_id, $tg_chat_id, $wizard_name, $step_no)");
        $ci = &get_instance();

        $where_array = WizardStateManager::getDbFilter($tg_user_id, $tg_chat_id, $wizard_name, $step_no);
        $ci->db->where($where_array);
        $ci->db->delete(WizardStateManager::TBL__STEPS_DATA);
    }

    public static function getState($where_array)
    {
        $ci = &get_instance();
        $ci->db->where($where_array);
        $query = $ci->db->get(WizardStateManager::TBL__STEPS_DATA);

        return Utilitar_db::safe_resultRow($query);
    }

    public static function getStatesRows($where_array)
    {
        $ci = &get_instance();
        $ci->db->where($where_array);
        $query = $ci->db->get(WizardStateManager::TBL__STEPS_DATA);

        return Utilitar_db::safe_resultSet($query);
    }


    // warning: discards specific wizard's data only.
    public static function discardWizard($tg_user_id, $tg_chat_id, $wizard_name)
    {
//        log_message('error', "    WSM: discardWizard($tg_user_id, $tg_chat_id, '$wizard_name')");
        $where_array = WizardStateManager::getDbFilter($tg_user_id, $tg_chat_id, $wizard_name);

        $ci = &get_instance();

        // no cascaded deletes yet:
        $ci->db->where($where_array);
        $ci->db->delete(WizardStateManager::TBL__STEPS_DATA);     // 1. remove the steps

        $ci->db->where($where_array);
        $ci->db->delete(WizardStateManager::TBL__WIZARD_STATE);   // 2. remove the state
    }

/*
    public static function discardAllWizards($tg_user_id, $tg_chat_id)
    {
//        log_message('error', "    WSM: discardWizard($tg_user_id, $tg_chat_id, '$wizard_name')");
        $where_array = WizardStateManager::getDbFilter($tg_user_id, $tg_chat_id, null);

        $ci = &get_instance();

        // no cascaded deletes yet:
        $ci->db->where($where_array);
        $ci->db->delete(WizardStateManager::TBL__STEPS_DATA);     // 1. remove the steps

        $ci->db->where($where_array);
        $ci->db->delete(WizardStateManager::TBL__WIZARD_STATE);   // 2. remove the state
    }
*/

    private static function getWizardDBRow($tg_user_id, $tg_chat_id, $wizard_name)
    {
        $where_array = WizardStateManager::getDbFilter($tg_user_id, $tg_chat_id, $wizard_name); // here we need botIdCRC32, tg_userID, tg_chatID only.

        $ci = &get_instance();
        $ci->db->where($where_array);
        $query = $ci->db->get(WizardStateManager::TBL__WIZARD_STATE);
        return Utilitar_db::safe_resultRow($query);
    }

    // returns NULL if none is active.
    private static function getCurrentWizardDBRow($tg_user_id, $tg_chat_id)
    {
        return WizardStateManager::getWizardDBRow($tg_user_id, $tg_chat_id, null);
    }

    // returns NULL if none is active.
    public static function getCurrentWizardName($tg_user_id, $tg_chat_id)
    {
        $row = WizardStateManager::getCurrentWizardDBRow($tg_user_id, $tg_chat_id);
        return ($row ? $row->wizard_name : NULL);
    }

    // returns NULL if none is active or [0..N] integer.
    public static function getCurrentWizardStepNumber($tg_user_id, $tg_chat_id)
    {
        $row = WizardStateManager::getCurrentWizardDBRow($tg_user_id, $tg_chat_id);
        return ($row ? $row->step_no : NULL);
    }
    

    // NOTE: also automatically remembers the callback PARAMETERS EXISTING at the moment!
    public static function setCurrentWizardNameAndStepNo($tg_user_id, $tg_chat_id, $wizard_name, $step_no)
    {
        $where_array = WizardStateManager::getDbFilter($tg_user_id, $tg_chat_id, $wizard_name);

//        log_message('error', "QQQ called WSM::setCurrentWizardNameAndStepNo(".print_r(WizardHelper::flatten($where_array), true).", step_no:$step_no).");

        $callback_query__message_id = TelegramHelper::get_callback_query__message_id(); // it is -1 if a non-callback query called.
        $input_data = WizardHelper::get_tg_input_data();

        // for that step remmeber also the CALLBACK PARAMETERS (if any):
        $callback_data_array = array();
        if ($callback_query__message_id > 0)
        {   $callback_data_array['callback_messaging_id']   = $callback_query__message_id;
            $callback_data_array['callback_data']           = TelegramHelper::get_callback_query_data();
        }

        $ci = &get_instance();
        $row= WizardStateManager::getWizardDBRow($tg_user_id, $tg_chat_id, $wizard_name);

        //log_message('error', "XXX WMS: getWizardDBRow($tg_user_id, $tg_chat_id, $wizard_name): ".print_r($row, true));
        if ($row)
        { // update needed:
            $update_array = array('step_no' => $step_no);
            $ci->db->where($where_array);
            $ci->db->update(WizardStateManager::TBL__WIZARD_STATE, array_merge($callback_data_array, $update_array));
        }
        else
        { // insert needed:
            $where_array['step_no'] = $step_no; // append the step number to store.
            $ci->db->insert(WizardStateManager::TBL__WIZARD_STATE, array_merge($callback_data_array, $where_array));
        }
    }

    public static function getStepsStack($tg_user_id, $tg_chat_id)
    {
        $where_array = WizardStateManager::getDbFilter($tg_user_id, $tg_chat_id, NULL);
        $ci = &get_instance();
        $ci->db->where($where_array);
        $ci->db->order_by('lastupdate ASC, step_no ASC');
        $query = $ci->db->get(WizardStateManager::TBL__STEPS_DATA);

        return Utilitar_db::safe_resultSet($query);
    }

    // gAs per call stack.
    public static function getStepCalledBefore($tg_user_id, $tg_chat_id)
    {
        $where_array = WizardStateManager::getDbFilter($tg_user_id, $tg_chat_id, NULL);
        $ci = &get_instance();
        $ci->db->where($where_array);
        $ci->db->order_by('lastupdate DESC, step_no DESC');
        $ci->db->limit(1);
        $query = $ci->db->get(WizardStateManager::TBL__STEPS_DATA);

        return Utilitar_db::safe_resultRow($query);
    }

    // $wizard_name is allowed to be NULL: in that case that parameter get excluded from where array.
    private static function getDbFilter($tg_user_id, $tg_chat_id, $wizard_name, $step_no = -1)
    {
        $where_array = array('bot_id_crc32' => WizardHelper::get_bot_id_crc32(),
                             'tg_user_id'   => $tg_user_id,
                             'tg_chat_id'   => $tg_chat_id,
                             );

        if (null != $wizard_name)
        {   $where_array['wizard_name'] = $wizard_name;
        }

        // if not specified then return data for all the steps (not for just a single one):
        if ($step_no >= 0)
        {   $where_array['step_no'] = $step_no;
        }

        return $where_array;
    }

    public static function get_stripped_params(&$input_data, &$step_obj, $tg_user_id, $tg_chat_id)
    {
        if (!isset($input_data['message']))
        {   log_message('error', "NOTE: User uploaded no file or image. Skipping get_stripped_params().");
            return null;
        }

        // user passed some input, but for WHICH COMMAND??
        $step_callback_data = WizardStateManager::getStepCallbackData($tg_user_id, $tg_chat_id, $step_obj);
        if (!$step_callback_data)
        {   log_message('error', "---- NOTE: get_stripped_params() GOT NO CALLBACK DATA. Skipping!");
            return null;
        }

        $stripped_params = TelegramHelper::strip_callback_params($step_callback_data['callback_data']);
        return $stripped_params;
    }

}
?>
