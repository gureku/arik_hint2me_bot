<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'libraries/wizardry/core/WizardBasicStep.php');
include_once(APPPATH.'libraries/wizardry/core/WizardBase.php');
include_once(APPPATH.'libraries/wizardry/core/WizardNavigationHelper.php');

abstract class InlineActionHandlerBase
{
    abstract public static function process_callbacks(array $params); // must return either null or the result of WizardHelper::wrap_inline_result() call.
    abstract public static function get_supported_callbacks(); // list all callback actions' NAMES supported.

    // override if inline action does have actions expecting file attachments on input:
    public static function get_callbacks_accepting_attachments()
    {   return array();
    }

    // TODO: this to be (re)moved to a more generic solution/place. E.g. into WizardHelper.
    public final static function dictionary(&$step_translations, $phrase)
    {
        $phrase_translated = isset($step_translations[$phrase]) ? $step_translations[$phrase] : $phrase;
        return $phrase_translated;
    }

    // Method identifies inline action name (if any) and verifies whether there's an InlineAction which does support that tile attachment.
    public static function in_actions($inline_action_name, array $callback_names)
    {
        foreach ($callback_names as $callback_name)
        {   if (WizardStateManager::equal_no_emoji($callback_name, $inline_action_name)) // TODO: replace by simple strcmp() call, because inline actions contain ONLY ASCII symbols!!
            {   return true; // $callback_name;
            }
        }

        return false;
    }
}
