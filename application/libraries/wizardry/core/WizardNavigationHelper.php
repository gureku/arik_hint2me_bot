<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'libraries/Telegram.php');

include_once(APPPATH.'libraries/wizardry/core/WizardStateManager.php');

include_once(APPPATH.'libraries/wizardry/core/WizardBase.php');
include_once(APPPATH.'libraries/wizardry/core/WizardDAL.php');
include_once(APPPATH.'libraries/wizardry/core/TelegramHelper.php');

include_once(APPPATH.'libraries/wizardry/library/WizardAuthorizer.php');

include_once(APPPATH.'core/SwitchToWizardException.php');
include_once(APPPATH.'core/StayOnSameStepException.php');



// Being thrown if the list processed by PREV/NEXT is empty.
class TotalCountIsZero extends Exception
{   function __construct($message)
    {   parent::__construct($message);
    }
}

// Being thrown when reached the paging list BEGIN (by pressing BACK button on a paging navigator)
class ReachedBeginException extends Exception
{   function __construct($message)
    {   parent::__construct($message);
    }
}

// Being thrown when reached the paging list END (by pressing FORWARD button on a paging navigator)
class ReachedEndException extends Exception
{   function __construct($message)
    {   parent::__construct($message);
    }
}

class WizardNavigationHelper
{

    //  Returns extra callback-actions, generated from the data results: to be later inserted into Telegram as callbackable buttons.
    //  Uses "$data_retriever_method" to get the data needed.
    //  Uses "$wrap_db_rows_as_extra_actions_method" to wrap the DB data into $extra_actions
    //       Also ALERTS (i.e. uses _Telegram alerts_) if smth. goes in a wrong way.
    //      "$append_to_the_end" in the array of callbackable buttons to be inserted RIGHT BEFORE the navigation buttons.
    //  $data_type:
    //      1: VISITORS
    //      2: smth. else

    //  NOTE that any of these params could be either A STRING (i.e. that's a method name with NO input params expected),
    //          or AN ASSOC. ARRAY: naming both the method and a parameter with input params assoc. array. I.e. $method_callback: [ 'method' => 'someFunctionaName', 'params' => assoc. array(...) ]
    //
    //      $method_calc_items_totalcount: e.g. "WC_S1::get_clients_with_visits_today", "WC_S1::get_partners_with_visits_today"
    //      $method_data_retriever: e.g. "WC_S1::get_clients_with_visits_today", "WC_S1::get_partners_with_visits_today"
    //      $method_wrap_db_rows_into_extra_actions: e.g. "WC_S1::wrap_clients_list_into_actions($db_rows)", "WC_S1::wrap_partners_list_into_actions($db_rows)"
    public static function create_navigable_dataset($pageIndex, $pageSize, $move_forward, $data_type, $keep_silence, $navigation_callbacks)
    {
        // extract the actual methods to be called first:
        $method_calc_items_totalcount           = $navigation_callbacks['cb__get_total_count'];
        $method_data_retriever                  = $navigation_callbacks['cb__data_retriever'];
        $method_wrap_db_rows_into_extra_actions = $navigation_callbacks['cb__wrap_db_rows_into_extra_actions'];

        //-----------------------------------------------------------+
        // knowing the total amount is a must to deal with paging of any data:
        $total_count_of_items = 0;
        if (is_string($method_calc_items_totalcount)) // this is just a function call with NO INPUT PARAMS:
        {   $total_count_of_items = call_user_func($method_calc_items_totalcount); // >5.2.3
        }
        else if (is_array($method_calc_items_totalcount)) // otherwise we received a method WITH input params, so pass them in:
        {
            if (isset($method_calc_items_totalcount['method']) && isset($method_calc_items_totalcount['params']))
            {   $total_count_of_items = call_user_func($method_calc_items_totalcount['method'], $method_calc_items_totalcount['params']);
            }
        }
        //-----------------------------------------------------------|



        $b_add_next_prev_buttons = true;
        try
        {
            $pageIndex = WizardNavigationHelper::adjust_pageIndex($total_count_of_items, $pageIndex, $pageSize, $move_forward);
        }
        catch (ReachedBeginException $e)
        {   // We just notify user about incident, but still CONTINUE NORMAL FLOW (to retrieve data and wrap it into buttons).
            // In this case the "$pageIndex" stays intact - that's important.
            if (!$keep_silence)
            {   TelegramHelper::echoTelegram_alert($e->getMessage(), true);
            }
        }
        catch (ReachedEndException $e)
        {   // We just notify user about incident, but still CONTINUE NORMAL FLOW (to retrieve data and wrap it into buttons).
            // In this case the "$pageIndex" stays intact - that's important.
            if (!$keep_silence)
            {   TelegramHelper::echoTelegram_alert($e->getMessage(), true);
            }
        }
        catch (TotalCountIsZero $e)
        {   $b_add_next_prev_buttons = false; // no items to show - so we add no navigation buttons (in order to save the screen space).
            // And don't return actions' empty array by this exception, just because
            // the $method_wrap_db_rows_into_extra_actions($db_rows) may (!) want to add buttons independent
            // on emptyness of $db_rows: e.g. add custom button "Add new member" or similar.
        }


        // initiate by empty values:
        $db_rows        = array();
        $extra_actions  = array();


        // NOTE: let's invoke two callbacks passed in.
        // Reminding that they could be callable with NO DYNAMIC arguments or with DYNAMIC args passed in the associative array!

        //-----------------------------------------------------------+
        // 1. data retrieval callback:
        if (is_string($method_data_retriever)) // this is just a function call with NO INPUT PARAMS:
        {   $db_rows = call_user_func("$method_data_retriever", $pageIndex, $pageSize);
        }
        else if (is_array($method_data_retriever)) // otherwise we received a method WITH input params, so pass them in:
        {
            if (isset($method_data_retriever['method']) && isset($method_data_retriever['params']))
            {   $db_rows = call_user_func($method_data_retriever['method'], $pageIndex, $pageSize, $method_data_retriever['params']);
            }
        }
        //-----------------------------------------------------------|


        //-----------------------------------------------------------+
        // 2. wrap the data retrieved into the ACTIONS:
        if (is_string($method_wrap_db_rows_into_extra_actions)) // this is just a function call with NO INPUT PARAMS:
        {   $extra_actions  = call_user_func("$method_wrap_db_rows_into_extra_actions", $db_rows);
        }
        else if (is_array($method_wrap_db_rows_into_extra_actions)) // otherwise we received a method WITH input params, so pass them in:
        {
            if (isset($method_wrap_db_rows_into_extra_actions['method']) && isset($method_wrap_db_rows_into_extra_actions['params']))
            {   $extra_actions  = call_user_func($method_wrap_db_rows_into_extra_actions['method'], $db_rows, $method_wrap_db_rows_into_extra_actions['params']);
            }
        }
        //-----------------------------------------------------------|


        // 3. ...and finally let's enrich the "$extra_actions" by callbackable navigation buttons:
        if ($b_add_next_prev_buttons)
        {   WizardNavigationHelper::append_prev_next_buttons($extra_actions, $pageIndex, 1, $pageIndex, $pageSize, $total_count_of_items, $data_type);
        }

        return $extra_actions;
    }

    //
    // NOTE: the last three ($pageIndex, $pageSize, $total_count_of_items) params are used to generate some textual TITLES for the back/prev buttons.
    // This is in order to help user understand how far (s)he is from the begin/end of the dataset (s)he plays with.
    //  It is also vital to know WHICH $data_type is used for paging!
    private static function append_prev_next_buttons(&$extra_actions, $offset_integer, $buttons_set_type, $pageIndex, $pageSize, $total_count_of_items, $data_type)
    {

    log_message('error', "XXX append_prev_next_buttons(offset:$offset_integer, pi:$pageIndex, ps:$pageSize, total:$total_count_of_items)");
        if (0 == count($extra_actions))
        {   return; // nothing to append.
        }

        if ($total_count_of_items <= $pageSize)
        {   return; // nothing to append.
        }

        $title_prev = (1 == $buttons_set_type) ? WizardBasicStep::NAVIG_TITLE_PREV : WizardBasicStep::NAVIG_TITLE_EARLIER;
        $title_next = (1 == $buttons_set_type) ? WizardBasicStep::NAVIG_TITLE_NEXT : WizardBasicStep::NAVIG_TITLE_LATER;

        //-----------------------------------------------------------+
        // let's also hint user HOW MANY RECORDS remain in each navigation direction:
        $pageIndex++; // this is just to deal with the index starting from "1", not from "zero".
        $prev_count = ($pageIndex-1)*$pageSize;
/*
        if ($prev_count < 0)
        {   $prev_count = 0;
        }

        $next_count = $total_count_of_items - $pageIndex*$pageSize;
        if ($next_count < 0)
        {   $next_count = 0;
        }

        $title_prev .= " ($prev_count)";
        $title_next .= " ($next_count)";
*/
        if ($prev_count <= 0)
        {   $prev_count = 0;
            $title_prev = "\xE2\x8F\xAA";
        }
        else
        {   $title_prev .= " ($prev_count)";
        }

        $next_count = $total_count_of_items - $pageIndex*$pageSize;
        if ($next_count < 0)
        {   $next_count = 0;
            $title_next = "\xF0\x9F\x94\x9A";
        }
        else
        {   $title_next .= " ($next_count)";
        }

//        $title_prev .= " ($prev_count)";
//        $title_next .= " ($next_count)";
                //-----------------------------------------------------------|


        // add a stub button if reached the end and have ODD amount of buttons:
        if (count($extra_actions) % 2)
        {   $extra_actions[' '] = TelegramHelper::wrap_button_action_name(WC_S1::ACTION_SHOW_NEXT_ITEMS, $offset_integer); // again put actions which show other executor's tasks.
        }


        $extra_actions[$title_prev] = TelegramHelper::wrap_button_action_name(WC_S1::ACTION_SHOW_PREV_ITEMS, $offset_integer, $data_type); // again put actions which show other executor's tasks.
        $extra_actions[$title_next] = TelegramHelper::wrap_button_action_name(WC_S1::ACTION_SHOW_NEXT_ITEMS, $offset_integer, $data_type); // again put actions which show other executor's tasks.
    }

    //
    //  Returns adjusted $pageIndex (based on $move_forward direction and $pageSize) and throws an exception if reached the begin/end of the list, or the list is empty.
    private static function adjust_pageIndex($total_count_of_items, $pageIndex, $pageSize, $move_forward)
    {
        // if for some reason the "$total_count_of_items" is zero then add no navigation buttons at all:
        if (0 == $total_count_of_items)
        {   log_message('error', "WARNING: adjust_pageIndex(): the total count of items is ZERO. Skipping generating NAVIGAION'S CALLBACK BUTTONS for that.");
            throw new TotalCountIsZero('No items in a list');
        }

//        log_message('error', "XXX calling adjust_pageIndex(total_count_of_items:$total_count_of_items, pageIndex:$pageIndex, pageSize:$pageSize), move direction: ".($move_forward ? 'NEXT': 'PREV'));
        //-----------------------------------------------------------+
        // Here we're doing COMMON PART of paging & callbacks.
        // Let's check both the bounds and validate the new "$pageIndex":
        if ($move_forward)
        {   // step ahead:
            if ((($pageIndex + 1)*$pageSize) > $total_count_of_items)
            {   throw new ReachedEndException("\xE2\x84\xB9 Вы пролистали этот список ДО КОНЦА.");
            }
            else
            {   $pageIndex++; // otherwise proceed to the NEXT page of data.
            }
        }
        else
        {   // step backwards:
            if (($pageIndex - 1) < 0)
            {   throw new ReachedBeginException("\xE2\x84\xB9 Вы смотрите на самое НАЧАЛО СПИСКА.");
            }
            else
            {   $pageIndex--; // otherwise proceed to the PREVIOUS page of data.
            }
        }

        return $pageIndex;
    }


    // This is "Null object" pattern.
    // It is used to return a stub "$navigation_callbacks" structure for WizardNavigationHelper::create_navigable_dataset($pageIndex, $pageSize, $move_forward, $data_type, $keep_silence, $navigation_callbacks)!
    public static function get_navigation_callbacks_stub()
    {
        return array(   'cb__data_retriever'                    => 'WizardNavigationHelper::get_void',
                        'cb__wrap_db_rows_into_extra_actions'   => 'WizardNavigationHelper::get_void',
                        'cb__get_total_count'                   => 'WizardNavigationHelper::get_zero',
                        );
    }

    // used to return void array of db rows
    public static function get_void($stub1 = null, $stub2 = null)
    {   return array();
    }

    // used to return zero count of items in a DB
    public static function get_zero()
    {   return 0;
    }

}
