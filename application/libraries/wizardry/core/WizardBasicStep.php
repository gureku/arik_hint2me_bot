<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'core/SwitchToWizardException.php');
include_once(APPPATH.'core/StepBackException.php');
include_once(APPPATH.'core/StayOnSameStepException.php');

include_once(APPPATH.'libraries/wizardry/library/WizardFather.php');

/**
* WizardBasicStep methods' call sequence:
*   1. c'tor
*   2. On WizardHelper->executeStep() calls:
*                                               CURRENT_STEP->process_prev_step_input(PREVIOUS_STEP's INPUT handled here).
*   2. Then down in WizardHelper, calls WizardHelper->printListToTelegram() with:
*                                               CURRENT_STEP->get_hint() + CURRENT_STEP->get_keyboard().
*/

abstract class WizardBasicStep
{
    // separators used for callback params:
    const NAVIG_TITLE_EARLIER = "\xE2\xAC\x85 ранее";
    const NAVIG_TITLE_LATER = "позже \xE2\x9E\xA1";

    const NAVIG_TITLE_PREV = "\xE2\xAC\x85";
    const NAVIG_TITLE_NEXT = "\xE2\x9E\xA1";

    const CHECKMARK_SYMBOL  = "\xE2\x9C\x94";
    const NONE_SYMBOL       = "&#8205;"; // when printed, browser shows NO symbol at all! That's useful when printing profile images inside the message.

    private $step_hint;
    private $keyboard; // this is optional array of action names (strings) to show in a one-time keyboard.
    public  $owning_wizard; // the wizard that owns this step. Useful to get some common (to all steps) data.

    public function __construct(&$owning_wizard, $step_hint = null)
    {
        $this->owning_wizard   = $owning_wizard;
        $this->step_hint        = $step_hint;
        $this->keyboard         = null;//array(array(WizardBase::GO_BACK)); -- this code makes flickering (re-creating the keyboard on each printListToTelegram) the keyboard!!!
    }

    // depending on command the step hint might change so the method:
    public function set_hint($step_hint)
    {
        $this->step_hint = $step_hint;
    }

    // either steps current wizard back or calls the FatherWizard.
    public function step_back()
    {
        log_message('error', "default step_back() is called. Override for a customization.");
        throw new StepBackException("default step_back() is called. Override for a customization.");
//        throw new SwitchToWizardException(WizardFather::NAME, 0); // call papa if nobody else is around :)
    }


    // Can return array of ations (if any). They will be used to generate the one-time keyboard.
    public function get_keyboard()
    {   return $this->keyboard;
    }

    // NOTE: Inline keyboard takes priority over regular keyboard!! And that keyboard is optional for a step to implement.
    // Returns a COMPOUND structure, containing:
    //      'message': the message to print (a must have) to end-user.
    //      'inline_buttons':       (optional) inline buttons
    //      'inline_options':       (optional) inline buttons options
    //      'additional_params':    (optional) useful to accept external params to customize the generated buttons/message/etc. (e.g. PageIndex, PageSize, etc.)
    public static function get_inline_structure($additional_params = null)
    {   return null; // NOTE: each Step class MUST also call wrap_inline_result() before returning its results from the get_inline_structure() method!!
    }

    //
    // This method retuns language-specific set of strigs for languages OTHER THAN DEFAULT (English) language.
    // Class should override it to return key=>value dictionary of Step's string resourses.
    public static function translations($lang)
    {   return array(); // just a default value, so that there's no obligation to implement this method in sibling classes.
    }

    // NOTE: I failed finding a way to call child's render_dictionary() method from parent. samethe trick of calling itself "recursively" is using just single methodname to call different (parent/child) method from :)
    public final static function render_dictionary($lang)
    {
        //log_message('error', "render_dictionary($lang) BASIC, get_called_class:".get_called_class());
        $basic_translations = array();
        switch ($lang) // ensure to keep branches elements in sync!
        {
            case 'en': // english texts shall not be translated in any sence.
                break;

            case 'ru':
                $basic_translations = array(WizardBase::GO_BACK              => WizardBase::GO_BACK_RU,
                                            WizardSettings::BTN_LAUNCH       => "\xe2\x9a\x99 Опции",
                                            WizardAuthorizer::BTN_LOGIN      => "\xF0\x9F\x94\x91 Войти",
                                            WizardUnauthorizer::BTN_LOGOUT   => "\xE2\x98\x9D Выйти",
                                            );
                break;

            case 'am':
                $basic_translations = array(WizardBase::GO_BACK              => WizardBase::GO_BACK_AM,
                                            //WizardSettings::BTN_LAUNCH       => "\xe2\x9a\x99 Опции",
                                            //WizardAuthorizer::BTN_LOGIN      => "\xF0\x9F\x94\x91 Войти",
                                            //WizardUnauthorizer::BTN_LOGOUT   => WizardBase::GO_BACK_AM,
                                            );
                break;

            default: // use default (English-only) translation in worst case.
                break;
        }

        $child_translations = static::translations($lang); // call child translations (via late static binding!)

        return array_merge($basic_translations, $child_translations);
    }

    // Accepts a phrase on input (usually an English one) and a language to translate to.
    // no the phrase is not found or the language requested is n/a in a Step class, the original phrase returned.
    public final static function translate($lang_code, $phrase)
    {
        $step_translations = self::render_dictionary($lang_code); // note the word "static" to call child's static method (late static binding)!!!
        //log_message('error', "called BASIC translate('$lang_code', '$phrase') with STEP dictionary ".print_r($step_translations, true));
        $phrase_translated = isset($step_translations[$phrase]) ? $step_translations[$phrase] : $phrase;
        return $phrase_translated;
    }

    // uses input dictionary. Useful to save time when translating multiple strings at a time (e.g. a set of buttons).
    public final static function dictionary(&$step_translations, $phrase)
    {
        $phrase_translated = isset($step_translations[$phrase]) ? $step_translations[$phrase] : $phrase;
        return $phrase_translated;
    }

    // returns a hint for a step.
    public function get_hint()
    {   return $this->step_hint;
    }


    //
    // On input accepts ($userdata). Returns false or NULL if processing failed (or no output required along with staying on the same step!).
    // NOTE: It may throw exceptions (SwitchToWizardException, StepBackException) if needed, rather than doing return true/false.
    // This method's responsibility the retrieval of the data entered on the PREVIOUS step(s) (if any).
    public function process_prev_step_input($userdata, $tg_user_id, $tg_chat_id)
    {   return true;
        // NOTE: "return NULL" is also possible. Useful for cases when NOTHING to be printed abck to Telegram in return: neither new keyboard nor the get_hint()!
        // It is different from the StayOnTheSamePageException: e.g. that exception is useless when called from within process_input().
    }

    // Returns -1 if failed or the "$message_id" on success.
    public function handle_callbacks($userdata)
    {
        $cb_input = TelegramHelper::get_callback_input($userdata);

        log_message('error', "NOTE: override basig handle_callbacks(callback_query__message_id:".$cb_input['cb_message_id'].", callback_params:'".$cb_input['cb_params']."') if you need callback handling there");
        $message_id = TelegramHelper::echo_inline("Override step's callback handler, if needed");

        return $message_id; // returns -1 if failed.
    }
}