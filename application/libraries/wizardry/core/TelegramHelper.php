<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'libraries/Telegram.php');
include_once(APPPATH.'core/InvalidCallbackException.php');

// is tightly coupled with Telegram library (@author Gabriele Grillo <gabry.grillo@alice.it>).
// Deals with Telegram-specific output, alerts and keyboards.
class TelegramHelper
{
    const LAST_MSG_ID__PREFIX = 'last_msg_id_';
    const LAST_RCVD_MSG_ID__PREFIX = 'last_rcvd_msg_id_';
    const CALLBACK_PREFIX = 'cbbtn_'; // callback buttons' prefix
    const CALLBACK_PARAMS_SEPARATOR = '#';

    //-----------------------------------------------------------+
    // these are to unify params extraction:
    const IDX_PARAM         = 2; // NOTE: this call produces 3 params: TelegramHelper::wrap_button_action_name(WPPLMGR_S1::ACTION_SET_TASK_EXECUTOR, $task_id);
    const IDX_PARAM_EXTRA   = 3; //       ...and this call produces 4 params: TelegramHelper::wrap_button_action_name(WPPLMGR_S1::ACTION_SET_TASK_EXECUTOR, $task_id, $executor_id);
    const IDX_PARAM_EXTRA_PLUS  = 4; //       ...and this call produces 4 params: TelegramHelper::wrap_button_action_name(WPPLMGR_S1::ACTION_SET_TASK_EXECUTOR, $task_id, $executor_id);
    //-----------------------------------------------------------|

//============================================ MESSAGING SECTION START =================================================
    // sometimes keyboard passed as string, not as array. Telegram class doesn't recognize them.
    private static function ensureKeyboardIsCorrect(&$buttons_array)
    {
        foreach ($buttons_array as &$button)
        {
            if (is_string($button))
            {   $button = array($button);
            }
        }
    }
    
    // NOTE: ensure to call "set_last_message_id()" each time after successfull sending of message - for later reuse!
    public static function echoTelegram($message, $buttons_array = null, $disable_web_page_preview = true)
    {
        $telegram   = new Telegram(Util_telegram::get_bot_token()); // TODO: refactor this to use chat_id, user_id - instead of instantiating entire complex class for sending messages.
        $tg_chat_id = $telegram->ChatID();

        TelegramHelper::cleanup_input_message($message);


        // create the data to be sent to TG:
        $content    = array('chat_id' => $tg_chat_id, 'text' => $message);

        // enrich it:
        $content['parse_mode'] = 'HTML'; // rich-text formatting?
        $content['disable_web_page_preview'] = $disable_web_page_preview ? 'true' : 'false'; // disable anchor and links preview.

        if ($buttons_array)
        {
            // Create a permanent custom keyboard
//            $keyb = $telegram->buildKeyBoard(array($buttons_array), $onetime=false, $resize = true);
//            $keyb = $telegram->buildKeyBoard($buttons_array, $onetime=false, $resize = false);
            TelegramHelper::ensureKeyboardIsCorrect($buttons_array);
            $keyb = $telegram->buildKeyBoard($buttons_array, $onetime=false, $resize = true);
            $content['reply_markup'] = $keyb;
//            log_message('error', "keyboard to be built=".print_r($buttons_array, true));
        }

//        log_message('error', "echoTelegram(): ".print_r($content, true));

        //  send the message:
        $message_delivered = $telegram->sendMessage($content);

        //  ...also remember its message_id for later use:
        return TelegramHelper::save_valid_message_id_from_TG_response($telegram->UserID(), $tg_chat_id, $message_delivered);
    }

    // Unfortunately hiding the REGULAR buttons is possible via sending a text message: empty message (which just hides the keyboard) is not possible :(
    public static function hideRegularButtons($message, $tg_user_id)
    {
        $telegram= new Telegram(Util_telegram::get_bot_token()); // TODO: refactor this to use chat_id, user_id - instead of instantiating entire complex class for sending messages.
        $content = array('chat_id' => $tg_user_id, 'text' => $message); // WARNING: it is not always "$telegram->UserId()" is valid!! So I EXPLICITLY pass in the tg_user_id to message to!
        $content['reply_markup'] = $telegram->buildKeyBoardHide(false); // this is to hide previously shown REGULAR buttons!
        $content['parse_mode'] = 'html'; // show rich messages.
        
        $message_delivered = $telegram->sendMessage($content); // try also: editMessageReplyMarkup(), editMessageText() or editMessageCaption().

        return $message_delivered;
    }

    // sends message to specified user
    public static function messageTo($tg_chat_id, $message, $buttons_array = null, $disable_web_page_preview = true)
    {
        $telegram   = new Telegram(Util_telegram::get_bot_token()); // TODO: refactor this to use chat_id, user_id - instead of instantiating entire complex class for sending messages.

        TelegramHelper::cleanup_input_message($message);


        // create the data to be sent to TG:
        $content    = array('chat_id' => $tg_chat_id, 'text' => $message);

        // enrich it:
        $content['parse_mode'] = 'HTML'; // rich-text formatting?
        $content['disable_web_page_preview'] = $disable_web_page_preview ? 'true' : 'false'; // disable anchor and links preview.

        if ($buttons_array)
        {
            TelegramHelper::ensureKeyboardIsCorrect($buttons_array);
            $keyb = $telegram->buildKeyBoard($buttons_array, $onetime=false, $resize = true);
            $content['reply_markup'] = $keyb;
        }

        //  send the message:
        $message_delivered = $telegram->sendMessage($content);

        //  ...also remember its message_id for later use:
        return TelegramHelper::save_valid_message_id_from_TG_response($telegram->UserID(), $tg_chat_id, $message_delivered);
    }

    // NOTE: ensure to call "set_last_message_id()" each time after successfull sending of message - for later reuse!
    public static function echoTelegram_send_location($message, $buttons_titles = null, $disable_web_page_preview = true)
    {
        $telegram   = new Telegram(Util_telegram::get_bot_token()); // TODO: refactor this to use chat_id, user_id - instead of instantiating entire complex class for sending messages.
        TelegramHelper::cleanup_input_message($message);

        $index = 0;
        $buttons = array();
        foreach ($buttons_titles as $title)
        {
            if (is_string($title))
            {   $buttons[] = $telegram->buildKeyboardButton($title, false, (0 == $index++)); // the very first button we use as a "Set location" button.
            }
            else
            {   $buttons[] = $telegram->buildKeyboardButton('Arrays not supported');
            }
        }

        // Create a permanent custom keyboard
        $keyb = $telegram->buildKeyBoard(array($buttons), $onetime=false, $resize = true);
        $content = array(   'chat_id'                   => $telegram->ChatID(),
                            'text'                      => $message,
                            'parse_mode'                => 'HTML', // rich-text formatting
                            'disable_web_page_preview'  => $disable_web_page_preview ? 'true' : 'false', // disable anchor and links preview.
                            'reply_markup'              => $keyb,
                        );

        $message_id = NULL;
        $telegram->sendMessage($content);

        return $message_id;
    }

    // returns false if no location received or an array with lat/lon (e.g. {'latitude' => 41.xxxxxx, 'longitude' => 43.yyyyyy}
    public static function user_location_received(&$input_data)
    {
        if (!isset($input_data['message']) || !isset($input_data['message']['location']))
        {   //log_message('error', "No user geo-location passed in. Skipping.");
            return false;
        }

        return $input_data['message']['location'];
    }

    // returns false or the inline query entire struct (if exists).
    public static function inline_query_received(&$input_data)
    {   return isset($input_data['inline_query']) ? $input_data['inline_query'] : false;
    }

    private static function cleanup_input_message(&$message, $b_use_default_msg = true)
    {
        if ($message)
        {   $message    = str_replace(array('\n'), chr(10), $message);
        }
        else if ($b_use_default_msg)
        {   $message = 'Пожалуйста, выберите из кнопок ниже либо нажмите /start';
        }
    }

    // NOTE: parameter "$options" supports the following options:
    //  'buttons_chunk_size' - i.e. $options['buttons_chunk_size'] - the chunk size for the buttons.
    //  'message_id'        - i.e. $options['message_id']
    //  'inline_message_id' - i.e. $options['inline_message_id']
    //  'answer_data'       - i.e. $options['answer_data']
    public static function echoTelegram_inline_v3($message, $buttons_array__new_format = null, array $options = null, $tg_chat_id = null)
    {
        TelegramHelper::cleanup_input_message($message);

        $telegram   = new Telegram(Util_telegram::get_bot_token()); // TODO: refactor this to use chat_id, user_id - instead of instantiating entire complex class for sending messages.
        if (!$tg_chat_id)
        {   $tg_chat_id = $telegram->ChatID();
        }

        $content = array('chat_id' => $tg_chat_id, 'text' => $message, 'parse_mode' => 'HTML');
        
        
        // log_message('error', "---- RRR echoTelegram_inline_v3() inline buttons: ".print_r($buttons_array__new_format, true)."\noptions: ".print_r($options, true));
        if (is_array($buttons_array__new_format))
        {   // creating inline buttons:
            $buttons_chunk_size = isset($options['buttons_chunk_size']) ? $options['buttons_chunk_size'] : 1;
            $chunked_buttons    = array_chunk($buttons_array__new_format, $buttons_chunk_size, true);

            $kbd_options = array();
            foreach ($chunked_buttons as $item)//$btn_name => $btn_cb_data)
            {
                if (TelegramHelper::is_assoc_array($item))
                {
                    $kbd_row = array();
                    foreach ($item as $btn_name => $btn_value)
                    {   $kbd_row[] = $telegram->buildInlineKeyBoardButton($btn_name, $url = '', $callback_data = $btn_value);
                    }

                    $kbd_options[] = $kbd_row;
                }
                else
                {   log_message('error', "WARNING: keyboard unhandled case: nonASSOCIATIVE ITEM passed in! ".print_r($item, true));
                }
            }

            log_message('error', "--- XXX: old inline buttons formatted: ".print_r($kbd_options, true));

            $inline_keyboard = $telegram->buildInlineKeyBoard($kbd_options);
            if ($inline_keyboard)
            {   $content['reply_markup'] = $inline_keyboard;
            }
        }

        return TelegramHelper::_basic_inline_send($telegram, $content, $options, $tg_chat_id);
    }

    // NOTE: expects to_callback_grouped_buttons() output to be passed in as $extra_actions!
    // NOTE: parameter "$options" supports the following options:
    //  'message_id'        - i.e. $options['message_id']
    //  'inline_message_id' - i.e. $options['inline_message_id']
    //  'answer_data'       - i.e. $options['answer_data']
    public static function echo_inline($message, $extra_actions = null, array $options = null, $tg_chat_id = null)
    {
        TelegramHelper::cleanup_input_message($message);

        $telegram = new Telegram(Util_telegram::get_bot_token()); // TODO: refactor this to use chat_id, user_id - instead of instantiating entire complex class for sending messages.
        if (!$tg_chat_id)
        {   $tg_chat_id = $telegram->ChatID();
        }

        $content = array('chat_id' => $tg_chat_id, 'text' => $message, 'parse_mode' => 'HTML');

        //-----------------------------------------------------------+
        // let's build the inline buttons hierarchy:
        $grouped_inlines = array();

        if ($extra_actions)
        {
            foreach ($extra_actions as $key => $value)
            {
                //log_message('error', "--- KKK: key='$key', value='".print_r($value, true)."'");
                if (is_int($key))
                {   // this is an array inside. Let's ensure that:
                    if (is_array($value))
                    {
                        if (TelegramHelper::is_assoc_array($value))
                        {
                            $tmp = array();
                            foreach ($value as $key_inside => $value_inside) // this is for a group of buttons:
                            {
                                if (is_array($value_inside) && isset($value_inside['url']))
                                {   $tmp[] = $telegram->buildInlineKeyBoardButton($key_inside, $url = $value_inside['url']); // this way i build URL-button!
                                }
                                else if (   WizardHelper::mb_str_equal($key_inside, Util_telegram::STR_SEARCH_RU) ||    // CUSTOM CHECK1: for the Search button we put INLINE URL ref button instead of simple inline button!
                                            WizardHelper::mb_str_equal($key_inside, Util_telegram::STR_SEARCH))         // TODO: find better way to mark the INLINE SEARCH button!!
                                {   $tmp[] = $telegram->buildInlineKeyboardButton($key_inside, null, null, null, '');   // TG Search magic happens ONLY when the 3-rd param is NULL.
                                }
                                else
                                {   $tmp[] = $telegram->buildInlineKeyBoardButton($key_inside, $url = '', $callback_data = $value_inside);
                                }
                            }

                            $grouped_inlines[] = $tmp;
                        }
                        else
                        {   log_message('error', "--- ERROR: what the hell to do with this ARRAY1: ".print_r($value, true)); // impossible case1
                        }
                    }
                    else
                    {   log_message('error', "--- ERROR: what the hell to do with this ARRAY2: ".print_r($value, true)); // impossible case2.
                    }
                }
                else
                {   // this is strings pair:
                    // CUSTOM CHECK2: for the Search button we put INLINE URL ref button instead of simple inline button!
                    if (WizardHelper::mb_str_equal($key, Util_telegram::STR_SEARCH_RU) || WizardHelper::mb_str_equal($key, Util_telegram::STR_SEARCH)) // TODO: find better way to mark the INLINE SEARCH button!!
                    {   $grouped_inlines[] = array($telegram->buildInlineKeyboardButton($key, null, null, null, ''));
                    }
                    else
                    {   $grouped_inlines[] = array($telegram->buildInlineKeyBoardButton($key, $url = '', $callback_data = $value));
                    }
                }
            }
        }
        //-----------------------------------------------------------|
        //log_message('error', "--- XXX: new inline buttons formatted: ".print_r($grouped_inlines, true));

        $inline_keyboard = $telegram->buildInlineKeyBoard($grouped_inlines);
        if ($inline_keyboard)
        {   $content['reply_markup'] = $inline_keyboard;
        }

        return TelegramHelper::_basic_inline_send($telegram, $content, $options, $tg_chat_id);
    }

    // returns message_id received upon sending.
    //  'message_id'        - i.e. $options['message_id']
    //  'inline_message_id' - i.e. $options['inline_message_id']
    //  'answer_data'       - i.e. $options['answer_data']
    public static function _basic_inline_send(&$telegram, &$content, $options, $tg_chat_id)
    {
        $content['disable_web_page_preview'] = isset($options['disable_web_page_preview']) ? $options['disable_web_page_preview'] : false;

        //-----------------------------------------------------------+
        // NOTE: editMessage() supports 2 ways of editing messages. Check them both:
        $b_edit_mesage = false;
        $answer_data = null;
        if ($options)
        {
            if (isset($options['message_id']))  // the reply could be to REGULAR message:
            {   $content['message_id'] = $options['message_id'];
                $b_edit_mesage = true;
            }
            else if (isset($options['inline_message_id'])) // the reply also could be to INLINE message:
            {   $content['inline_message_id'] = $options['inline_message_id'];
                $b_edit_mesage = true;
            }

            $answer_data = isset($options['answer_data']) ? $options['answer_data'] : null;
        }
        //-----------------------------------------------------------|

        //log_message('error', "---- TG Helper full CONTENT (right before sending to TG srv):\n".print_r($content, true));

        $message_delivered = $b_edit_mesage ? $telegram->editMessageText($content) : $telegram->sendMessage($content);
        $message_id = TelegramHelper::save_valid_message_id_from_TG_response($telegram->UserID(), $tg_chat_id, $message_delivered);

        // NOTE: answerCallbackQuery() must be called for callback buttons: TG requires that!
        TelegramHelper::echo_callback_answer($telegram, $answer_data);

        if ($message_id <= 0)
        {   log_message('error', "---- TG Helper error sending inline message. Details: ".print_r($message_delivered, true));
            if (isset($message_delivered['error_code']) && (400 == $message_delivered['error_code']))
            {
                log_message('error', "---- TG Helper this error means BUTTON CALLBACK DATA SIZE is GREATER than allowed 64 bytes.\n
                Rename Action name to a SHORTER version or REDUCE CALLBACK parameters passed in!!!\n".print_r($content['reply_markup'], true));
            }
        }

//        log_message('error', "---- TG Helper CONTENT: ".print_r($content, true));
//        log_message('error', "---- TG Helper OPTIONS: ".print_r($options, true));

        return $message_id;
    }

    // NOTE: answerCallbackQuery() must be called either way for callback buttons (TG requires that!).
    // Here we just determine whether a user alert to be shown or not.
    // $answer_data structure:
    //      'answer_text' (optional): the text to be shown.
    //      'show_popup' (optional): if is set, then a popup alert is shown
    private static function echo_callback_answer(&$telegram, $answer_data)
    {
        $input_data = WizardHelper::get_tg_input_data();
        if (!isset($input_data['callback_query']))
        {   return false;
        }

        $callback_query_id  = $input_data['callback_query']['id'];
        // $callback_data      = $input_data['callback_query']['data'];

        $output = array();
        $output['callback_query_id'] = $callback_query_id;

        if (is_array($answer_data) && isset($answer_data['answer_text'])) // makes no sense showing an alert whose text is n/a.
        {   // $answer_data structure:
            //      'answer_text' (optional): the text to be shown.
            //      'show_popup' (optional): if is set, then a popup alert is shown
            $output['text'] = $answer_data['answer_text'];

            if (isset($answer_data['show_popup']))
            {   $output['show_alert'] = $answer_data['show_popup'];
            }
        };

        return $telegram->answerCallbackQuery($output); // On success, True is returned.
    }

    // to be used by inline button ID parsing and its' params.
    public static function wrap_button_action_name($action_name, $id = -1, $extra_value = null, $extra_value2 = null)
    {
        //log_message('error', "TH: wrap_button_action_name(actionname:$action_name, id:$id, ev1:$extra_value, ev2:$extra_value2");
        
        $res = $action_name.TelegramHelper::CALLBACK_PARAMS_SEPARATOR.$id;
        if ($extra_value)
        {   $res .= TelegramHelper::CALLBACK_PARAMS_SEPARATOR.$extra_value;
        }

        if ($extra_value2)
        {   $res .= TelegramHelper::CALLBACK_PARAMS_SEPARATOR.$extra_value2;
        }

        return $res;
    }

    // used to show notification or alert (popup dialog) to a user.
    public static function echoTelegram_alert($message, $b_show_popup = false)
    {
        if (strlen($message) <= 0)
        {   return false;
        }

        $telegram = new Telegram(Util_telegram::get_bot_token()); // TODO: refactor this to use chat_id, user_id - instead of instantiating entire complex class for sending messages.

        // True is returned on success:
        return TelegramHelper::echo_callback_answer($telegram, array('answer_text' => $message, 'show_popup' > $b_show_popup));
    }

    // saves only VALID message_id from the message received!
    private static function save_valid_message_id_from_TG_response($user_id, $chat_id, array $message_delivered)
    {
        $message_id = TelegramHelper::extract_message_id_from_TG_response($message_delivered);
        if ($message_id > 0)
        {   TelegramHelper::set_last_message_id($user_id, $chat_id, $message_id);
        }

        return $message_id;
    }

    // returns -1 if not found.
    private static function extract_message_id_from_TG_response(array $message_delivered)
    {
        $message_id = -1;
        if ($message_delivered && isset($message_delivered['result']))
        {
            if (1 == $message_delivered['ok'])
            {   $message_id = $message_delivered['result']['message_id'];
            }
            else
            {   log_message('error', "ERROR: extract_message_id_from_TG_response(): cannot retrieve message_id! The message being parsed:\n".print_r($message_delivered, true));
            }
        }

        return $message_id;
    }

    //
    // For "$images_array" it accepts two input formats:
    //      1. plain array of URLs or
    //      2. compound array of images, including: "url", "caption".
    public static function echoTelegramImages($message, $images_array, $disable_notification = false)
    {
        $telegram   = new Telegram(Util_telegram::get_bot_token()); // TODO: refactor this to use chat_id, user_id - instead of instantiating entire complex class for sending messages.
        $tg_chat_id = $telegram->ChatID();

        // if we need to send a message then:
        if (strlen($message) > 0)
        {
            // $message    = str_replace(array('\n'), chr(10), $message);
            TelegramHelper::cleanup_input_message($message);
            $content    = array('chat_id' => $tg_chat_id,
                                'text' => $message,
                                'parse_mode' => 'HTML',// rich-text formatting?
                                'disable_web_page_preview' => 'true',// disable anchor and links preview.
                                'disable_notification' => 'true',// send it silently because this is just a part of message.
                                );

            $ret_value = $telegram->sendMessage($content);
        }

        $res = array();
        if (is_string($images_array))
        {
            $content    = array('chat_id' => $tg_chat_id, 'photo' => (string) $images_array);

            if ($disable_notification)
            {   $content['disable_notification'] = 'true';
                log_message('error', "WWW: notifications disabled!");
            }

            // send the content. WARNING: stores answers for the ALL images passed in! I.e. "echoTelegramImages()" return value format differs from the echoTelegram()!
            return array($telegram->sendPhoto($content));
        }

        // this is array:
        foreach($images_array as $image)
        {
            // prepare the content:
            $content    = array('chat_id' => $tg_chat_id);
            if (is_string($image))
            {   $content['photo'] = $image;
                log_message('error', "WWW: is string!");
            }
            else if (is_array($image)) // looks this is compound data, so:
            {
                log_message('error', "WWW: is array!");

                 if (isset($image['url']))
                 {  $content['photo'] = $image['url'];
                 }
                 if (isset($image['caption']))
                 {  $content['caption'] = $image['caption'];
                 }
            }

            if ($disable_notification)
            {   $content['disable_notification'] = 'true';
                log_message('error', "WWW: notifications disabled!");
            }

            // send the content. WARNING: stores answers for the ALL images passed in! I.e. "echoTelegramImages()" return value format differs from the echoTelegram()!
            $res[] = $telegram->sendPhoto($content);
        }

        return $res;
    }

    // stores the last message id sent for later reuse per user per chat!
    public static function set_last_message_id($tg_user_id, $tg_chat_id, $message_id)
    {
        $variable_part = WizardDAL::calc_crc32($tg_user_id.':'.$tg_chat_id);

        log_message('error', "SAVE_MESSAGE_ID: $message_id");
        $ci = &get_instance();
        $ci->utilitar_db->setOption(TelegramHelper::LAST_MSG_ID__PREFIX.$variable_part, $message_id);
    }

    // returns '-1' if none has been stored before.
    public static function get_last_message_id($tg_user_id, $tg_chat_id)
    {
        $variable_part = WizardDAL::calc_crc32($tg_user_id.':'.$tg_chat_id);

        $ci = &get_instance();
        return $ci->utilitar_db->getOption(TelegramHelper::LAST_MSG_ID__PREFIX.$variable_part, -1);
    }

    // stores the last message id RECEIVED for later reuse per user per chat!
    public static function set_last_received_message_id($tg_user_id, $tg_chat_id, $message_id)
    {
        $variable_part = WizardDAL::calc_crc32($tg_user_id.':'.$tg_chat_id);

        //log_message('error', "RECEIVED_MESSAGE_ID: $message_id");
        $ci = &get_instance();
        $ci->utilitar_db->setOption(TelegramHelper::LAST_RCVD_MSG_ID__PREFIX.$variable_part, $message_id);
    }

    // returns '-1' if none has been stored before.
    public static function get_last_received_message_id($tg_user_id, $tg_chat_id)
    {
        $variable_part = WizardDAL::calc_crc32($tg_user_id.':'.$tg_chat_id);

        $ci = &get_instance();
        return $ci->utilitar_db->getOption(TelegramHelper::LAST_RCVD_MSG_ID__PREFIX.$variable_part, -1);
    }

//============================================ MESSAGING SECTION END ===================================================

//============================================ KEYBOARD SECTION START ==================================================
    // NOTE: on error returns NULL so that the recipient understands whether the error happened and that's not a keyboard to rwap up into Telegram Keyboard.
    //
    // On input gets an associative array of format {button_title -> button_callback_param}
    public static function buildCallbackKeyboard(&$telegram_obj, $buttons_assoc_array)
    {
        if (!$buttons_assoc_array || !is_array($buttons_assoc_array))
        {   return null;
        }

        if (0 == count($buttons_assoc_array)) // guarantees that keyboard will be built from non-void arrays only.
        {   return null;
        }

        $kbd_options = array();
        foreach ($buttons_assoc_array as $btn_name => $btn_cb_data)
        {   $kbd_options[] = $telegram_obj->buildInlineKeyBoardButton($btn_name, $url = '', $callback_data = $btn_cb_data);
        }

        return $telegram_obj->buildInlineKeyBoard(array($kbd_options));
    }


//============================================ KEYBOARD SECTION START ==================================================
    // NOTE: on error returns NULL so that the recipient understands whether the error happened and that's not a keyboard to rwap up into Telegram Keyboard.
    //
    // On input gets an plain array of format [] = {'title'=>button_title, 'cbdata' => button_callback_param}
    public static function buildCallbackKeyboard_newformat(&$telegram_obj, $buttons_array__new_format)
    {
        if (!$buttons_array__new_format || !is_array($buttons_array__new_format))
        {   return null;
        }

        if (0 == count($buttons_array__new_format)) // guarantees that keyboard will be built from non-void arrays only.
        {   return null;
        }

        log_message('error', "XXX buttons_array__new_format:".print_r($buttons_array__new_format, true));
        $kbd_options = array();
        foreach ($buttons_array__new_format as $item)
        {
            $btn_name   = $item['title'];
            $btn_cb_data= $item['cbdata'];

            $kbd_options[] = $telegram_obj->buildInlineKeyBoardButton($btn_name, $url = '', $callback_data = $btn_cb_data);
        }
        return $kbd_options;


        //----------------------------------------------------------------------+
        // demo for Minigalin!
        $kbd_row1 = array();
        $kbd_row1[] = $telegram_obj->buildInlineKeyBoardButton("\xE2\x8F\xB0 Перезвон +10 мин", $url = '', $callback_data = '111');
        $kbd_row1[] = $telegram_obj->buildInlineKeyBoardButton("\xF0\x9F\x93\x85 Записался на дату", $url = '', $callback_data = '222');

        $kbd_row2 = array();
        $kbd_row2[] = $telegram_obj->buildInlineKeyBoardButton("\xE2\x8F\xB0 Отложить +14 дней", $url = '', $callback_data = '333');
        $kbd_row2[] = $telegram_obj->buildInlineKeyBoardButton("\xE2\x9E\x96 Занести в Минус", $url = '', $callback_data = '444');

        $kbd_row3 = array();
        $kbd_row3[] = $telegram_obj->buildInlineKeyBoardButton("\xE2\x97\x80", $url = '', $callback_data = '5551');
        $kbd_row3[] = $telegram_obj->buildInlineKeyBoardButton("\xE2\x96\xB6", $url = '', $callback_data = '555');

        log_message('error', "QQQ: kbd_options (new format!): ".print_r($kbd_options, true));

        return $telegram_obj->buildInlineKeyBoard(array($kbd_row1, $kbd_row2, $kbd_row3));
        //----------------------------------------------------------------------|
    }

    // NYI finally!
    public static function strip_callback_params(&$callback_params)
    {
//        log_message('error', "XXXXXXXXXXXXXXX strip_callback_params: ".print_r($callback_params, true));
        if (!TelegramHelper::is_valid_callback_button($callback_params))
        {   log_message('error', "---- ERROR: XXX: invalid callback button! ".print_r($callback_params, true));
            throw new InvalidCallbackException('Invalid callback button');
        }

        $split = explode(TelegramHelper::CALLBACK_PARAMS_SEPARATOR, $callback_params);
        log_message('error', "NOTE: strip_callback_params() result: ".print_r($split, true));

        if (!isset($split[2])) // this is assumed to be a currently selected date, passed along with callback_param!
        {   log_message('error', "ERROR: strip_callback_params(): Unrecognized callback command. Is it FORMATTED incorrectly??\n".print_r($callback_params, true));
            throw new InvalidCallbackException('Unrecognized callback command. Is it FORMATTED incorrectly?');
        }

        // format expected: "WizardChildren#WPPLMGR_S2_HandleChildCommand#btn_next|29.11.2017"
        // if User pressed Prev/Next buttons then we should know that!

        //$date = $split[2]; // this is the ACTUAL DATA passed in with the command (see the format in a line above)!
        return $split;
    }

    // returns "-1" or an "$input_data['callback_query']['message']['message_id'];".
    public static function get_callback_query__message_id()
    {
        // NOTE: it must contain a "$callback_data" parameter!
        $input_data = WizardHelper::get_tg_input_data();

        // read it from a callback query. If there is none returns -1:
        $callback_message_id = isset($input_data['callback_query']) ? $input_data['callback_query']['message']['message_id'] : -1;
        return $callback_message_id;
    }

    // TODO: refactor to reuse TelegramHelper::get_callback_query()
    public static function is_callback_query()
    {
        // NOTE: it must contain a "$callback_data" parameter!
        $input_data = WizardHelper::get_tg_input_data();

        // read it from a callback query.
        return isset($input_data['callback_query']);
    }

    public static function get_callback_query()
    {
        // NOTE: it must contain a "$callback_data" parameter!
        $input_data = WizardHelper::get_tg_input_data();

        // read it from a callback query.
        return isset($input_data['callback_query']) ? $input_data['callback_query'] : null;
    }

    // WARNING: it returns data only DURING CALLBACK calls!!!
    public static function get_callback_query_data()
    {
        $cbquery = TelegramHelper::get_callback_query();
        return $cbquery ? $cbquery['data']: null;
    }

    // expected input format: <callback_prefix>#<wizard_name>#<step_name>#<value:string>
    public static function is_valid_callback_button($input)
    {
        $res = explode(TelegramHelper::CALLBACK_PARAMS_SEPARATOR, $input);
        return (count($res) >= 2);  // NOTE: the mirror function create_callback_param() uses 2 hashtags
                                    // to separate wizard_name/wizard_stepname/callback_value. That defines whether
                                    // the input value is a callback button or just an input.
                                    // NOTE: That approach could be error-prone. If so, we will use a very custom prefix for all the callback params instead...
    }

    // "$userdata" contains also the callback prefix that I use for a doublecheck.
    // The output format: <wizard_name>#<step_name>#<value:string>
    public static function extract_callback_parameter($userdata)
    {
        // strip it from a callbackbutton prefix that I insert all the time:
        return mb_substr($userdata, strlen(TelegramHelper::CALLBACK_PREFIX));
    }

    public final static function get_callback_input($userdata)
    {
        $callback_params            = TelegramHelper::extract_callback_parameter($userdata);
        $callback_query__message_id = TelegramHelper::get_callback_query__message_id(); // it is -1 if a non-callback query called.

        return array('cb_params' => $callback_params, 'cb_message_id' => $callback_query__message_id);
    }

    public static function is_assoc_array(array $array)
    {
        // Keys of the array
        $keys = array_keys($array);

        // If the array keys of the keys match the keys, then the array must
        // not be associative (e.g. the keys array looked like {0:0, 1:1...}).
        return array_keys($keys) !== $keys;
    }

    // Accepts both simple array and associative arrays: for simple arrays creates assoc. array of {btn_title => btn_title} format.
    // For assoc. arrays uses the values passed in.
    //
    // Uses create_callback_param() to create GLOBALLY-PARSABLE (i.e. UNIQUE ACROSS ALL THE WIZARDS and STEPS) callback params for any button.
    // In return it creates an assoc. array of {button_name:button_callbackdata} pairs format.
    public static function titles_to_callback_button_holders($wizard_name, $wizard_step_name, $buttons_assoc_array)
    {
        log_message('error', "--- XXX: titles_to_callback_button_holders($wizard_name, $wizard_step_name) buttons: ".print_r($buttons_assoc_array, true));
        
        if (0 == count($buttons_assoc_array))
        {   log_message('error', "WARNING: TelegramHelper::titles_to_callback_button_holders($wizard_name, $wizard_step_name) given NO TITLES to convert to buttons!!! Overwriting for safety.");
            $buttons_assoc_array= array('Default inline action');
            $wizard_name        = 'WizardFather';
            $wizard_step_name   = 'WF_S1_show_commands';
        }

        // log_message('error', "XXX TH::titles_to_callback_button_holders given ".count($buttons_assoc_array).' items to convert to buttons.');
        // this function may malfunction on assoc. arrays of integer keys format: {1 => 'some data', 2 => 'another data'}
        if (TelegramHelper::is_assoc_array($buttons_assoc_array))
        {
            $res = array();
            foreach ($buttons_assoc_array as $button_name => $button_callbackdata)
            {   $res[$button_name] = TelegramHelper::create_callback_param($wizard_name, $wizard_step_name, $button_callbackdata);
            }

            return $res;
        }

        // for non-associative arrays passed in do preparation first:
        $titles_associative_arr = array();
        foreach ($buttons_assoc_array as $title)
        {   $titles_associative_arr[$title] = $title;
        }

        // and call the same method with newly created assoc. array then:
        return TelegramHelper::titles_to_callback_button_holders($wizard_name, $wizard_step_name, $titles_associative_arr);
    }

    //
    // Accepts "$buttons_assoc_array" with nesting (so called "groups" of buttons).
    // NOTE: it's a must to call TelegramHelper::echo_inline() (instead of TelegramHelper::echoTelegram_inline_v3()) for such buttons!
    // TODO: get rid everywhere from TelegramHelper::echoTelegram_inline_v3().
    public static function to_callback_grouped_buttons($wizard_name, $wizard_step_name, $buttons_assoc_array)
    {
        // let's build the inline buttons hierarchy:
        $grouped_inlines = array();
        foreach ($buttons_assoc_array as $key => $value)
        {
            //log_message('error', "--- KKK: key='$key', value='".print_r($value, true)."'");
            if (is_int($key))
            {   // this is an array inside. Let's ensure that:
                if (is_array($value))
                {
                    if (TelegramHelper::is_assoc_array($value))
                    {
                        $tmp = array();
                        foreach ($value as $key_inside => $value_inside)
                        {
                            if (is_array($value_inside))
                            {   $tmp[$key_inside] = $value_inside; // pass the array further - as is! It will be handled during echo_inline()!
                            }
                            else
                            {   $tmp[$key_inside] = TelegramHelper::create_callback_param($wizard_name, $wizard_step_name, $value_inside);//$telegram->buildInlineKeyBoardButton($key_inside, $url = '', $callback_data = $value_inside);
                            }
                        }

                        $grouped_inlines[] = $tmp;
                    }
                    else
                    {   log_message('error', "--- ERROR: what the hell to do with this ARRAY1: ".print_r($value, true)); // impossible case1
                    }
                }
                else
                {   log_message('error', "--- ERROR: what the hell to do with this ARRAY2: ".print_r($value, true)); // impossible case2.
                }
            }
            else
            {   // this is strings pair:
                if (is_array($value))
                {   $grouped_inlines[$key] = $value; // pass the array further - as is! It will be handled during echo_inline()!
                }
                else
                {   $grouped_inlines[$key] = TelegramHelper::create_callback_param($wizard_name, $wizard_step_name, $value);//array($telegram->buildInlineKeyBoardButton($key, $url = '', $callback_data = $value));
                }
            }
        }

        return $grouped_inlines;
    }

    // output format: <callback_prefix>#<wizard_name>#<step_name>#<value:string>
    private static function create_callback_param($wizard_name, $wizard_step_name, $value_string)
    {
        //log_message('error', "TH: correct creation of callback buttons!wn:$wizard_name, stepno:$wizard_step_name, input value:$value_string");
        //  NOTE: calling sprintf with params FAILS due to $value_string contents! So use old plain string concatenation instead!
        //  NOTE: you might need to SANITIZE the "$value_string" because Telegram doesn't accept (sliently!!) ":" and "_" symbols there!
        //  NOTE: if at some time Telegram will cut the callback param string due to its length then you may use "$wizardName.'#'.crc32($stepName)" instead.
        return TelegramHelper::CALLBACK_PREFIX.$wizard_name.TelegramHelper::CALLBACK_PARAMS_SEPARATOR.$wizard_step_name.TelegramHelper::CALLBACK_PARAMS_SEPARATOR.$value_string;
    }
//============================================ KEYBOARD SECTION END ====================================================

    // downloads both images and files.
    // returns the files structures downloaded.
    public static function download_files_posted($tg_user_id, &$input_data, $dest_dir_root = null)
    {
        $ci = &get_instance();

        // use default dir if none pasted:
        if (!$dest_dir_root)
        {   $dest_dir_root = '..'.DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'telegram_files' . DIRECTORY_SEPARATOR;
        }

        if (!$ci->utilitar->ends_with($dest_dir_root, DIRECTORY_SEPARATOR))
        {   $dest_dir_root .= DIRECTORY_SEPARATOR;
        }

        $ci->load->service('FileService');

        log_message('error', "QQQ entering download_files_posted(): ".print_r($input_data, true));

        $files_downloaded = array();
        $telegram = new Telegram(Util_telegram::get_bot_token()); // TODO: refactor this to use chat_id, user_id - instead of instantiating entire complex class for sending messages.

        if (!isset($input_data['message']))
        {   log_message('error', "download_files_posted() no MESSAGE parameter passed. Nothing to process.");
            return array();
        }

        $message = $input_data['message'];

        if (isset($message['photo']))
        {
            $photos_array = $message['photo'];
            log_message('error', "QQQ PHOTOS posted: ".print_r($photos_array, true));
            $file = TelegramHelper::tg_get_largest_imagefile($telegram, $photos_array); // TODO: this method is WRONG because it makes EXTRA tg netwrok calls! Instead use identify_largest_image_sent() which does NOT sent setwork requets!!
            if (!$file)
            {   log_message('error', "ERROR: download_files_posted() cannot get maximal imagefile.");
                return array();
            }

            //---------------------------------------------------------------------------------------------------------------------+
            // prepare filenames for download:
            $extension = pathinfo($file['file_path'], PATHINFO_EXTENSION);
            $path_parts = pathinfo($file['file_path']);


            // create user-specific telegram upload folder:
            $dir = $dest_dir_root.$tg_user_id;

            // create the user dir, if it doesn't exist yet:
            $dirpath = APPPATH . $dir;
            if (!file_exists($dirpath))
            {   mkdir($dirpath, 0755, true);
                // log_message('error', "XXX FILEHANDLING: just created a directory '$dirpath' missing!");
            }

            $local_file_path = null;
            do
            {   $local_file_path = $dir . DIRECTORY_SEPARATOR . random_string('alnum', 12);
            } while (file_exists(APPPATH . $local_file_path));


            $local_file_path = APPPATH . $local_file_path;

            $telegram->downloadFile($file['file_path'], $local_file_path);

            //-----------------------------------------------------------+
            // strip by parts as some may be null/empty:
            $file_title = isset($message['caption']) ? $message['caption'] : null;

            $db_fname   = isset($path_parts['filename']) ? $path_parts['filename'] : 'unnamed';
            $db_fext    = isset($path_parts['extension']) ? $path_parts['extension'] : 'extension';
            $file_name  = $db_fname.'.'.$db_fext;
            //-----------------------------------------------------------|

            $id = $ci->FileService->register_file($tg_user_id, $file_name, $file_title, $local_file_path);
            //---------------------------------------------------------------------------------------------------------------------|

            $files_downloaded[] = array('id'=>$id, 'filename' => $file_title, 'filetitle' => $file_title, 'local_file_path' => $local_file_path);
        }

        if (isset($message['document']))
        {
//            [document] => Array
//                (
//                    [file_name] => Kvant_01_2018.pdf
//                    [mime_type] => application/pdf
//                    [file_id] => BQADAgAD3wIAAkflYEi3dblmImSSTwI
//                    [file_size] => 3728061
//                )
            $files_array = $message['document'];
            foreach ($files_array as $tg_file)
            {
                $file_raw = $telegram->getFile($tg_file['file_id']);
                if (!$file_raw)
                {   log_message('error', "docs: error calling getFile() for ".print_r($tg_file, true));
                    continue;
                }

                log_message('error', "docs tg file raw data: ".print_r($file_raw, true));

                if ((!isset($file_raw['ok'])) || (1 != $file_raw['ok']))
                {   log_message('error', "Error getFile() returned invalid file data: ".print_r($file_raw, true));
                    continue;
                }

                $ttt = $file_raw['result'];
                $files_downloaded[] = array('id'=> $ttt['file_id'], 'file_path' => $ttt['file_path']);
    //-----------------------------------------------------------+
    //  $file_raw contents:
    //(   [ok] => 1
    //    [result] => Array
    //        (
    //            [file_id] => AgADAgADv6kxG3LxYEgPsKKRogTm5hjqtw4ABKOG-RaqEeSvXV4EAAEC
    //            [file_size] => 937
    //            [file_path] => photos/file_0.jpg
    //        )
            }

            log_message('error', "QQQ FILES received: ".print_r($files_array, true));
        }

        return $files_downloaded;
    }

    // telegram supplies multiple thumbnails per single image file. So we identify the largest one.
    public static function identify_largest_image_sent(&$tg_files_array)
    {
        if (isset($tg_files_array['file_size']))
        {   log_message('error', "it LOOKS a single document file to handle. Check it out!");
            return $tg_files_array; // in fact this is a single TG file sent.
        }

        //log_message('error', "---- NOTE: it LOOKS like A SET OF IMAGE-files to handle. Check it out!");

        if (!is_array($tg_files_array))
        {   log_message('error', "---- ERROR: identify_largest_image_sent() unexpectedly received not an array! ".print_r($tg_files_array, true));
            return $tg_files_array;
        }

        $max_size = 0;
        $max_item = null;
        foreach ($tg_files_array as $tg_file)
        {
            $filesize = $tg_file['file_size'];

            // find the max. size:
            if ($max_size < $filesize)
            {   $max_size = $filesize;
                $max_item = $tg_file;
            }
        }

        // for photos made by camera there's no '$max_item['mime_type']' so add it manually:
        if (!isset($max_item['mime_type']))
        {   $max_item['mime_type'] = 'image/jpeg'; // as this is a case for an array of files sent this is much likely an image file, thus I assume this could be jpeg mime type.
        }

        return $max_item;
    }

    // telegram supplies multiple files (thumbnails) per single image file. So we choose the largest one to download only it.
    private static function tg_get_largest_imagefile(&$telegram, &$tg_files_array)
    {
        $max_size = 0;
        $max_item = null;
        foreach ($tg_files_array as $tg_file)
        {
            $file_raw = $telegram->getFile($tg_file['file_id']);
            if (!$file_raw)
            {   log_message('error', "Error calling getFile() for ".print_r($tg_file, true));
                continue;
            }

//            log_message('error', "tg_get_largest_imagefile FILE structure: ".print_r($file_raw, true));

            if ((!isset($file_raw['ok'])) || (1 != $file_raw['ok']))
            {   log_message('error', "Error getFile() returned invalid file data: ".print_r($file_raw, true));
                continue;
            }
//-----------------------------------------------------------+
//  $file_raw contents:
//(   [ok] => 1
//    [result] => Array
//        (
//            [file_id] => AgADAgADv6kxG3LxYEgPsKKRogTm5hjqtw4ABKOG-RaqEeSvXV4EAAEC
//            [file_size] => 937
//            [file_path] => photos/file_0.jpg
//        )
//)
//-----------------------------------------------------------|

            $file = $file_raw['result'];
            $filesize = $file['file_size'];

            // find the max. size:
            if ($max_size < $filesize)
            {   $max_size = $filesize;
                $max_item = $file;
            }
        }

        return $max_item;
    }

    public static function choose_attachments_of_type(array &$attachments, $mime_type)
    {
        $res = array();
        foreach ($attachments as $attachment)
        {
            if ($mime_type == $attachment->mime_type)
            {   $res[] = $attachment;
            }
        }

        return $res;
    }

    // mark the tg file as removed. Returns "local_tg_file_id" if it was found.
    public static function remove_specific__tg_file_type__attachment($tg_file_type, $meeting_id, $file_owner_tg_ticket_id)
    {
        $where_array = array('ticket_id' => $file_owner_tg_ticket_id, 'type' => $tg_file_type);

        $ci = &get_instance();
        $ci->db->where($where_array);
        $query = $ci->db->get('tg_files');
        $row = Utilitar_db::safe_resultRow($query);
        $local_tg_file_id = $row ? $row->id  : null;

        $ci->db->where($where_array);
        $ci->db->update('tg_files', array('removed' => true, 'removed_datetime' => 'now()')); // set the "removed" flad and its datetime as well.

        return $local_tg_file_id;
    }
    //
    // Registers Telegram file attachment in my DB and returns "$local_tg_file_id" for the created/existing TG File.
    // Parses input Telegram file structure and inserts it into DB.
    //  "$tg_file_type" is for my "tg_files.type" column. NULL is for regular attachment, otherwise is some custom attachment type.
    public static function register_tg_attachment($prefix, &$telegram, $file_owner_tg_ticket_id, &$tg_file_struct, $tg_file_type = null)
    {
        $ci = &get_instance();

        $tg_file_struct = TelegramHelper::identify_largest_image_sent($tg_file_struct); // this is a sign: a single file has been sent, so get that (and not its thumbnails).

        // only if the file IS registered in our records, it makes sense checking whether that file is ALREADY assigned to THIS SPECIFIC meeting:
        $local_tg_file_id       = -1;

        // detect whether there's already a TG file existing:
        $tg_registered_file = TelegramHelper::get_tg_file_by_unique_id($tg_file_struct['file_unique_id']);  // gets just a TG-file registered INSPITE OF any meeting.
        if ($tg_registered_file)
        {
            log_message('error', "---- register_tg_attachment() TG file with id:'".$tg_file_struct['file_unique_id']."' is ALREADY available:\n".print_r($tg_registered_file, true));
            $new_values = array('removed' => false);
            Utilitar_db::_update_entity('tg_files', $tg_registered_file->id, $new_values); // restore it!
            Utilitar_db::_update_entity('meeting_user_files', null, $new_values, array('local_tg_file_id' => $tg_registered_file->id)); // restore it!
            $local_tg_file_id = $tg_registered_file->id; // otherwise get the local id of a TG file.
        }
        else
        {
            if (!isset($tg_file_struct['file_name']))
            {   // manual audio recordings have no file_name, so we make a trick below.
                // if filename is n/a (that's true for audio-messages) then 1) get the files count of the same mime-type and set an incremental filename to it explicitly:
                //$attachments = Util_meetings::get_meeting_tg_attachments($meeting_id);
                //$of_mime_type = TelegramHelper::choose_attachments_of_type($attachments, $tg_file_struct['mime_type']);
                //log_message('error', "ATTACHMENTS of_mime_type: ".print_r($of_mime_type, true));

                if (isset($tg_file_struct['file_path']))
                {   $ci->load->model('utilitar');
                    $tg_file_struct['file_name'] = $prefix.$ci->utilitar->locale_safe_basename($tg_file_struct['file_path']);
                }
                else
                {   $tg_file_struct['file_name'] = $prefix.'.'.TelegramHelper::mimetype_to_extension($tg_file_struct['mime_type']);
                }
            }

            $local_tg_file_id = TelegramHelper::insert_tg_file_v2($file_owner_tg_ticket_id, $tg_file_struct, $telegram, $tg_file_type);
        }

        return $local_tg_file_id;
    }

    public static function mimetype_to_extension($mimetype)
    {
        $ext = 'jpg';
        switch ($mimetype)
        {
            case 'image/png':
            case 'image/apng':
                return 'png';

            case 'image/bmp':
                return 'bmp';

            case 'image/gif':
                return 'gif';

            case 'image/x-icon':
                return 'ico';

            case 'jpg':
            case 'image/jpeg':
                return 'jpg';
                return 'jpg';

            case 'image/svg+xml':
                return 'xml';
            case 'image/tiff':
                return 'tiff';

            case 'image/webp':
                return 'webp';

            case 'audio/wave':
            case 'audio/wav':
            case 'audio/x-wav':
            case 'audio/x-pn-wav':
                  return 'wav';

            case 'audio/webm':
                return 'webm';

            case 'video/webm':
                return 'webm';

            case 'audio/ogg':
            case 'video/ogg':
            case 'application/ogg':
                return 'ogg';

            case 'audio/mpeg':
            case 'video/mpeg':
                return 'mpeg';

            case 'application/msword':
                return 'doc';

            case 'application/pdf':
                return 'pdf';

            case 'text/plain':
                return 'txt';

            case 'application/vnd.ms-excel':
                return 'xls';

            case 'application/vnd.ms-powerpoint':
                return 'ppt';

            case 'text/csv':
                return 'csv';

            case 'application/gzip':
            case 'application/zip':
            case 'application/x-7z-compressed':
                return 'zip';

            case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
                return 'docx';

            case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
                return 'xlsx';

            case 'application/vnd.openxmlformats-officedocument.presentationml.presentation':
                return 'pptx';
        }

        return $ext;
    }

    // registers the file as a "Telegram file".
    // returns "$local_tg_file_id" (i.e. returns the local row id, corresponding to that tg file).
    public static function insert_tg_file($file_owner_tg_user_id, array $tg_file_struct, &$telegram)
    {
        $ci = &get_instance();

        $data = array(  'tg_user_id'    => $file_owner_tg_user_id, // set the file owner (uploader)
                        'file_id'       => $tg_file_struct['file_id'],
                        'file_unique_id'=> $tg_file_struct['file_unique_id'],
                        'file_size'     => $tg_file_struct['file_size']
                     );

        if (!isset($tg_file_struct['mime_type']))
        {   // if file mime-type is unknown (that happens when user posts camera shot, not a file stored), then ask Telegram servers about the mime-type they assigned to the file that user just uploaded:
            $filename_mimetype = TelegramHelper::get_remote_TG_file_details($telegram, $tg_file_struct['file_id']);

            // it's possible that remote file retrieval failed. So check it here:
            $data['file_name'] = $filename_mimetype ? $filename_mimetype['file_name'] : random_string('alnum', 4).'.jpg';
            $data['mime_type'] = $filename_mimetype ? $filename_mimetype['mime_type'] : 'image/jpeg'; // hardcode may be wrong for non-image uploads. TODO: CHECK IT!
        }
        else // use existing data:
        {   $data['file_name'] = $tg_file_struct['file_name'];
            $data['mime_type'] = $tg_file_struct['mime_type'];
        }

        $ci->db->insert('tg_files', $data);
        $local_tg_file_id = $ci->db->insert_id();

        return $local_tg_file_id;
    }

    // registers the file as a "Telegram file".
    // returns "$local_tg_file_id" (i.e. returns the local row id, corresponding to that tg file).
    public static function insert_tg_file_v2($file_owner_tg_ticket_id, array $tg_file_struct, &$telegram, $tg_file_type)
    {
        $ci = &get_instance();

        $data = array(  'ticket_id'     => $file_owner_tg_ticket_id, // set the file owner's ticket id.
                        'file_id'       => $tg_file_struct['file_id'],
                        'file_unique_id'=> $tg_file_struct['file_unique_id'],
                        'file_size'     => $tg_file_struct['file_size'],
                        'type'          => $tg_file_type, // this is my "tg_files.type" column
                     );

        if (!isset($tg_file_struct['mime_type']))
        {   // if file mime-type is unknown (that happens when user posts camera shot, not a file stored), then ask Telegram servers about the mime-type they assigned to the file that user just uploaded:
            $filename_mimetype = TelegramHelper::get_remote_TG_file_details($telegram, $tg_file_struct['file_id']);

            // it's possible that remote file retrieval failed. So check it here:
            $data['file_name'] = $filename_mimetype ? $filename_mimetype['file_name'] : random_string('alnum', 4).'.jpg';
            $data['mime_type'] = $filename_mimetype ? $filename_mimetype['mime_type'] : 'image/jpeg'; // hardcode may be wrong for non-image uploads. TODO: CHECK IT!
        }
        else // use existing data:
        {   $data['file_name'] = $tg_file_struct['file_name'];
            $data['mime_type'] = $tg_file_struct['mime_type'];
        }

        $ci->db->insert('tg_files', $data);
        $local_tg_file_id = $ci->db->insert_id();

        return $local_tg_file_id;
    }

    // it returns ANY TG file registered by us. Accepts REMOTE file_unique_id (i.e. not the local file id:int!).
    public static function get_tg_file_by_unique_id($tg_file_unique_id)
    {
        $ci = &get_instance();
        return $ci->utilitar_db->_getTableRow('tg_files', 'file_unique_id', $tg_file_unique_id);
    }

    // it returns ANY TG file registered by us. Accepts REMOTE file_unique_id (i.e. not the local file id:int!).
    public static function get_tg_files_by_unique_id_array(array $tg_file_unique_ids_array)
    {
        if (0 == count($tg_file_unique_ids_array))
        {   return array();
        }

        $ci = &get_instance();

        $ci->db->where_in('file_unique_id', $tg_file_unique_ids_array);
        $ci->db->order_by('mime_type, file_name');
        $query = $ci->db->get('tg_files');

        return Utilitar_db::safe_resultSet($query);
    }

    // asks Telegram server for file data. Returns just two fileds of that: 'file_name' and 'mime_type'.
    public static function get_remote_TG_file_details(&$telegram, $tg_file_id)
    {
        $res = array();
        $compound = $telegram->getFile($tg_file_id);
        if (!$compound)
        {   log_message('error', "---- ERROR calling Telegram->getFile() for tg_file_id:'$tg_file_id'.");
            return null;
        }

        log_message('error', "---- RAW structure received from Telegram server: ".print_r($compound, true));
        $file_descriptor = $compound['result'];

        // NOTE: 'file_path' contains not only the extension, but also the pseudo-subpath (e.g. 'photo', 'document',e.g. 'photo/file_1.jpg', etc.) that also may hint us about its mime-type.
        $fnames = explode('/', $file_descriptor['file_path']);
        $res['file_name'] = (2 == count($fnames)) ? $fnames[1] : $fnames[0]; // NOTE: here I ASSUME that Telegram ALWAYS assigns the subpath to a file...

        $exts = explode('.', $res['file_name']); // get the extension (if any)
        $mime_type = (2 == count($exts)) ? $exts[1] : 'image/jpeg'; // hardcode may be wrong for non-image uploads. TODO: CHECK IT!;

        // try to bring to common types (at least for image types):
        if (    (0 === strcmp('jpg', $mime_type)) ||
                (0 === strcmp('jpeg', $mime_type))
           )
        {   $mime_type = 'image/jpeg';
        }
        else if (0 === strcmp('png', $mime_type))
        {   $mime_type = 'image/png';
        }

        // either hardcode image/jpeg or get it manually:
        $res['mime_type'] = $mime_type;

        return $res;
    }

    // returns null for any other messages/events received.
    public static function extract_supported_message_struct(&$input_data)
    {
        if (isset($input_data['message']))
        {   return $input_data['message'];
        }
        else if (isset($input_data['edited_message']))
        {   return $input_data['edited_message'];
        }
        else if (isset($input_data['channel_post']))
        {   return $input_data['channel_post'];
        }
        else if (isset($input_data['edited_channel_post']))
        {   return $input_data['edited_channel_post'];
        }

        return null;
    }
}