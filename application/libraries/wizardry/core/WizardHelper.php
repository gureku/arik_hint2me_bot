<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'libraries/Telegram.php');

include_once(APPPATH.'libraries/wizardry/core/WizardStateManager.php');

include_once(APPPATH.'libraries/wizardry/core/WizardBase.php');
include_once(APPPATH.'libraries/wizardry/core/WizardDAL.php');
include_once(APPPATH.'libraries/wizardry/core/TelegramHelper.php');

include_once(APPPATH.'libraries/wizardry/library/WizardAuthorizer.php');

include_once(APPPATH.'core/SwitchToWizardException.php');
include_once(APPPATH.'core/StayOnSameStepException.php');


class WizardHelper
{
    const USER_ROLE__TRAINER    = 1; // i.e. SPECIALIST.
    const USER_ROLE__CLIENT     = 3; // NOTE: this constant has been used (indirectly!) in WT codes ALREADY!!! Don't change it!!!
    const USER_ROLE__GUEST      = 4;
    const USER_ROLE__CHILD      = 5; // "child" belongs to "client" via "family_members" table relation. "Child" in other words is a DEPENDANT.

    const USER_ROLE__PARTNER    = 5; // роль "независимый партнёр".
    const USER_ROLE__CLUB_OWNER = 6; // роль "управляющий клубом".
    const USER_ROLE__CLUB_ADMIN = 7; // роль "администратор клуба"

    // current context data:
    private static $tg_user_id;
    private static $tg_chat_id;
    private static $input_data; // data supplied by Telegram to Tg->process(). Useful when parcing inlineKeyboard replies etc.
    private static $translated_texts; // language-based text

    private static $unstripped_command;
    private static $wizards; // an array of wiazrds supported.
    private static $router_inline_handlers; // a routing table for the inline actions.

    private $CI = NULL;


    public function __construct($bot_id)
    {
        WizardHelper::$wizards              = array();
        WizardHelper::$router_inline_handlers= array();

        $this->CI = &get_instance();
        $this->CI->load->model('util_telegram');
    }

    public static function get_bot_id()
    {
        return Util_telegram::get_bot_token();
    }

    public static function get_bot_id_crc32()
    {
        return Util_telegram::get_bot_id_crc32();
    }

    public static function getUnstrippedCommand()
    {   return WizardHelper::$unstripped_command;
    }
    
    public static function resigterWizard($wizard_object)
    {
        // no duplicates allowed:
        $already_registered_wizard = WizardHelper::get_wizard_object($wizard_object->getName());
        if ($already_registered_wizard)
        {   return;
        }

        array_push(WizardHelper::$wizards, $wizard_object);

//        $dump_all_steps = $this->getStepsString($wizard_object);
//        log_message('error', "resigterWizard(): new wizard '".$wizard_object->getName()."' added.\nALL STEPS: $dump_all_steps");
    }

    // Assigns (i.e. does not append!) inline routing handler classes.
    public static function routing_assign(array $inline_handler_classnames)
    {
        WizardHelper::$router_inline_handlers = array(); // clean-up first. Do I really need to remove all first? Maybe better would be to collect (APPEND) them all???
        foreach ($inline_handler_classnames as $inline_handler_classname)
        {   WizardHelper::$router_inline_handlers[] = $inline_handler_classname; // store the classname.
        }

        // TODO: Then here I could add a check (by calling $actions_names = call_user_func($inline_handler_classname.'::get_supported_callbacks')) to verify
        //       whether NO ACTION HANDLING OVERLAP exists between any of the Handler classes registered. Rare situation but still could eliminate stupid bugs.
    }

    // finds in a routing table the InlineActionHandler class which handles particular action.
    private static function _get_inline_handler_classname($inline_action_name)
    {
        foreach (WizardHelper::$router_inline_handlers as $inline_handler_classname)
        {
            $actions_names = call_user_func($inline_handler_classname.'::get_supported_callbacks');
            if (in_array($inline_action_name, $actions_names))
            {   return $inline_handler_classname;
            }
        }

        return null;
    }

    // Returns true/false if inline has been invoked.
    public static function routing_invoke(&$current_user, array $context)
    {
        $userdata = $context['userdata'];
        $cb_input = TelegramHelper::get_callback_input($userdata);

        $callback_params            = $cb_input['cb_params'];
        $callback_query__message_id = $cb_input['cb_message_id'];
        if ($callback_query__message_id <= 0)
        {   log_message('error', "---- routing_invoke(): ERROR :no inline message passed in");
            return false;
        }

        $stripped_params    = TelegramHelper::strip_callback_params($callback_params);
        $custom_action_name = null;
        if (count($stripped_params) >= 3)
        {   $custom_action_name = isset($stripped_params[2]) ? $stripped_params[2] : null;
        }

        if (!$custom_action_name)
        {   log_message('error', "---- routing_invoke(): ERROR: no custom_action_name passed in.");
            return false;
        }

        $inline_handler_classname =  WizardHelper::_get_inline_handler_classname($custom_action_name);
        if (!$inline_handler_classname)
        {   log_message('error', "---- routing_invoke(): WARNING: routing is NOT DEFINED for '$custom_action_name' inline action!");
            return false;
        }

        try
        {
            $authentication_needed = !WizardFather::action_is_for_anonyms($custom_action_name, $current_user);

            if ($authentication_needed && (!Util_telegram::is_authorized($current_user)))
            {   $msg = 'Not authorized.';
                $extra_actions = array();
                $extra_actions[WizardBase::GO_BACK] = TelegramHelper::wrap_button_action_name(WC_S1::ACTION__STREAM_SHOW_EVENTS);

                $translated_extra_actions = WizardHelper::translate_button_names($extra_actions, $current_user->lang); // let's localise them

                $grouped_buttons = TelegramHelper::to_callback_grouped_buttons($inline_handler_classname, $inline_handler_classname, $translated_extra_actions);

                $options = array();
                TelegramHelper::echo_inline($msg, $grouped_buttons, $options);

                log_message('error', "---- WARNING: auth needed for '$custom_action_name' action for tg_user=".$current_user->tg_user_id);
                return true;
            }

            // static class method call (As of PHP 5.2.3):
            $params = array(
                'userdata'          => $userdata,
                'custom_action_name'=> $custom_action_name,
                'stripped_params'   => $stripped_params,
                'current_user'      => $current_user,
            );

            $wrapped = call_user_func($inline_handler_classname.'::process_callbacks', $params); // It's a must to return value after wrapping it! I.e. always call "WizardHelper::wrap_inline_result()" from within process_callbacks().
            if (!$wrapped)
            {   log_message('error', "---- routing_invoke(): error calling process_callbacks() for class '$inline_handler_classname'!");
                return false;
            }

            if (true === $wrapped)
            {   // handled normally just nothing more to return:
                log_message('error', "---- routing_invoke(): process_callbacks() handled normally, just nothing more to return for class '$inline_handler_classname'!");
                return true;
            }

            $msg                = $wrapped['message'];
            $extra_actions      = $wrapped['actions'];
            $options            = $wrapped['inline_options'];
            $handler_class_name = $wrapped['handler_class'];

            if ($extra_actions)
            {
                if ($callback_query__message_id > 0)
                {   $options['message_id'] = $callback_query__message_id;
                    // $options['answer_text'] => $warning_about_reaching_the_limit,
                }

                $translated_extra_actions = WizardHelper::translate_button_names($extra_actions, $current_user->lang); // let's localise them

                $grouped_buttons = TelegramHelper::to_callback_grouped_buttons($handler_class_name, $handler_class_name, $translated_extra_actions);
                TelegramHelper::echo_inline($msg, $grouped_buttons, $options);

                //log_message('error', "---- routing_invoke(): INLINE BUTTONS to show: ".print_r($grouped_buttons, true));
            }
            else
            {   //log_message('error', "---- routing_invoke(): WARNING: NO inline BUTTONS to show for action '$custom_action_name'!");
                TelegramHelper::echo_inline($msg);
            }
        }
        catch (InvalidCallbackException $e)
        {   log_message('error', "---- routing_invoke(): unrecognized INLINE COMMAND: ".print_r($userdata, true));
            TelegramHelper::echoTelegram_alert("Unrecognized INLINE COMMAND1 :(", true);
            return false;
        }

        return true;
    }

    // TODO:
    //  This should preparate not only plain actions but also grouped actions as well.
    //  See to_callback_grouped_buttons() as an example.
    public static function translate_button_names($extra_actions, $lang)
    {
        switch ($lang)
        {
            case 'ru':
                break;

            case 'am':
                break;

            default:
                break;
        }

        return $extra_actions; // temporarily return buttons as is.
    }

    // Returns the very first admin's messaging_id for this bot!
    public static function get_admin_messaging_id($yurlico_id, $organization_id)
    {
        $ci = &get_instance();

        $ci->db->where( array(  'yurlico_id'        => $yurlico_id,
                                'organization_id'   => $organization_id,
                                'role'              => WizardHelper::USER_ROLE__CLUB_ADMIN,
                             )
                        );

        // also filter by org_id, yurlico_id!
        $ci->db->limit(1);

        $query = $ci->db->get('tg_tickets');

        $row = Utilitar_db::safe_resultRow($query);
        return ($row ? $row->messaging_id : NULL);
    }

    public static function set_tg_user_role($tg_user_id, $bot_name, $role_code)
    {
        $ci = &get_instance();

        $ci->db->where(array('tg_user_id' => $tg_user_id, 'bot_name' => $bot_name));
        $ci->db->update(Util_telegram::TBL__TG_USERS, array('role' => $role_code));
    }


    public static function get_current_user_role()
    {
        $current_user = WizardHelper::get_current_user();
        if (!$current_user)
        {   log_message('error', "ERROR in get_current_user_role(): no current user found!");
            return -1;
        }

        return $current_user->role;
    }

    public static function current_user_has_role($role_id)
    {
        $current_user_role = WizardHelper::get_current_user_role();
        if ($current_user_role <= 0)
        {   return false;
        }
        return ($current_user_role == $role_id);
    }

    // returns string or NULL
    public static function get_current_user_name()
    {
        $current_user = WizardHelper::get_current_user();
        return ($current_user ? $current_user->ticket_owner_name : NULL);
    }

    // first tries to retrieve LOGGED_IN user, if failed - then the GUEST user with the same "tg_user_id".
    // The idea is that guest users are allowed to send messages, for that they are granted by "Guest Ticket" (with its own unique "messaging_id").
    public static function get_current_user()
    {
        $tg_context = WizardHelper::get_TG_context();
        return Util_telegram::get_tg_user_authorized_first($tg_context['tg_user_id']);
    }

    public static function get_current_user_messaging_id()
    {
        $current_user = WizardHelper::get_current_user();
        return ($current_user ? $current_user->messaging_id : NULL);
    }

    // returns the RAW data received from Telegram. Usually the mssage structure or inline_reply message structure.
    public static function get_tg_input_data()
    {
        $tg_context = WizardHelper::get_TG_context();
        return $tg_context['input_data'];
    }

    // flattens nested array to the string.
   public static function flatten($array)
    {
        if (!is_array($array))
        {   return '';
        }
        
//        log_message('error', "flatten() called: ".print_r($array, true));

        $flat = '';
        foreach ($array as $value)
        {
            if (is_array($value))
            {
                $flat .= WizardHelper::flatten($value);
            }
            else
            {
                $flat .= '"'.$value.'", ';
            }
        }
        return $flat;
    }

    public static function get_wizard_object($wizard_name)
    {
        if (!$wizard_name || ('' == $wizard_name))
        {   return null;
        }

        foreach (WizardHelper::$wizards as $wizard)
        {   if ($wizard_name == $wizard->getName())
            {   return $wizard;
            }
        }

        return null;
    }

    // NOTE: more params you pass less DB queries will run here.
    public function getStepObject($tg_user_id, $tg_chat_id, $current_wizard_object = null, $passed_steps_count = -1)
    {
        if (!$current_wizard_object)
        {   $current_wizard_name    = WizardStateManager::getCurrentWizardName($tg_user_id, $tg_chat_id);
            $current_wizard_object  = WizardHelper::get_wizard_object($current_wizard_name);
            if (!$current_wizard_object)
            {   return null;
            }
        }

        if ($passed_steps_count < 0)
        {   $passed_steps_count = WizardStateManager::getCurrentWizardStepNumber($tg_user_id, $tg_chat_id);
        }

        return $current_wizard_object->getStep($passed_steps_count);
    }

    private static function _str_search_in_array($str, &$options)
    {
        foreach ($options as $option)
        {
            if (0 === mb_strpos($option, $str))
            {   return true;
            }
        }

        return false;
    }

    // verifies if the keyboard contains the userdata (exact match only!) supplied.
    // Returns action title (from keyboard array) if found, or null if not.
    public static function inputBelongsToStepKeyboard($userdata, &$step_obj)
    {
        $actions = $step_obj->get_keyboard();
        if (!$actions)
        {   log_message('error', "Step '".$step_obj->get_hint()."' has no keyboard defined. Exiting.");
            return null;
        }

        foreach ($actions as $action)
        {
            if (is_array($action) && WizardHelper::_str_search_in_array($userdata, $action)) // does in_array() do byte-wise equivalence???
            {   return $userdata;
            }
            else if (is_string($action) && (0 === mb_strpos($action, $userdata))) // find exact match (only!)
            {   return $userdata;
            }
        }

        return null;
    }

    private static function getCurrentWizardObject($tg_user_id, $tg_chat_id)
    {
        $current_wizard_name = WizardStateManager::getCurrentWizardName($tg_user_id, $tg_chat_id);
        return WizardHelper::get_wizard_object($current_wizard_name);
    }

    // Note: this also could generate buttons.
    private function printListToTelegram(&$step_object, $tg_user_id, $tg_chat_id, $hide_the_hint = false)
    {
        $msg     = '';
        if (!$hide_the_hint)
        {   $msg = $step_object->get_hint();
        }

        if (null != $msg)
        {   $msg .= '\n';
        }
        else
        {   $msg = '';
        }

        $inline_struct = call_user_func(get_class($step_object) .'::get_inline_structure'); // >5.2.3. Here we're calling STATIC METHODS which may be overriden in child classes.
        if (!$inline_struct)
        {   TelegramHelper::echoTelegram($msg, $step_object->get_keyboard(), true);
        }
        else
        {   // this is useful when we want to print inline buttons, not the regular ones:
            $handler_class      = $inline_struct['handler_class'];
            $msg                = isset($inline_struct['message']) ? $inline_struct['message'] : null;
            $inline_buttons     = isset($inline_struct['actions']) ? $inline_struct['actions'] : null;
            $inline_btn_options = isset($inline_struct['inline_options']) ? $inline_struct['inline_options'] : null;


            $grouped_buttons = TelegramHelper::to_callback_grouped_buttons(   $handler_class, // this duplication to be REVIEWED first.
                                                                                $handler_class,
                                                                                $inline_buttons
                                                                            );
            TelegramHelper::echo_inline($msg, $grouped_buttons, $inline_btn_options, $tg_user_id);
        }
    }

    public static function mb_str_equal($str1, $str2)
    {
        return (0 === mb_strpos($str1, $str2) && (mb_strlen($str1) == mb_strlen($str2)));
    }


    // throws: StepBackException, SwitchToWizardException.
    private function executeTheStep($tg_user_id, $tg_chat_id, &$wizard, &$step, $command, $input_data)
    {
        //log_message('error', "executeTheStep($tg_user_id, $tg_chat_id, ".$wizard->getName().", ".get_class($step).", command:'$command')...".print_r($step, true));
        //log_message('error', "WH executeTheStep() command = '$command'");

        if ((WizardBase::GO_BACK == $command) || ((WizardBase::GO_BACK_RU == $command)) || ((WizardBase::GO_BACK_AM == $command)))
        {   $step->step_back(); // stepping back! Usually an exception shall be thrown: either to Father or to itself. No need to save current step's data before.
            return;
        }
        else if ('/start' == $command)
        {   throw new SwitchToWizardException(WizardFather::NAME, 0);
        }

        //-----------------------------------------------------------+
        // two *atlernate* flows possible: input error and "stay on the same step" exception:
        $bStay_on_same_step         = false;
        $bInvalid_input             = false;
        $do_not_output_to_Telegram  = false;
        try
        {   // NOTE: this could throw also other exceptions (which handled at level of runWizard() call).
            $someresult                 = $step->process_prev_step_input($command, $tg_user_id, $tg_chat_id, $input_data);
            $bInvalid_input             = !$someresult;
            $do_not_output_to_Telegram  = (NULL === $someresult); // check particularly for NULL!
        }
        catch (StayOnSameStepException $e)
        {   $bStay_on_same_step = true;
        }

        // do logging in case of unusual flows:
        if ($bInvalid_input)
        {   log_message('error', 'WH CHECK bInvalid_input = TRUE');
        }
        if ($bStay_on_same_step)
        {   log_message('error', 'WH CHECK bStay_on_same_step = TRUE');
        }
        //-----------------------------------------------------------|

        $current_step_index     = $wizard->getStepIndex(get_class($step));

        // NOTE: if process_prev_step_input() returns NULL, then nothing to be printed out and saved into the WSM state database: all the data to be processed/stored by the Step itself.
        if (!$do_not_output_to_Telegram)
        {
            // store the data chosen for the further use (in any case):
//            log_message('error', "WH: now will save the step: saveStep($tg_user_id, $tg_chat_id, ".$wizard->getName().", $current_step_index, $command)");
            WizardStateManager::saveStep($tg_user_id, $tg_chat_id, $wizard->getName(), $current_step_index, $command); // set data for the next step.

            //  Override the step object (take the previous step instead) if input was invalid.
            //  Btw we are ok to print THIS step for "StayOnSameStepException" cases:
            if ($bInvalid_input)
            {   $step = $this->getStepObject($tg_user_id, $tg_chat_id, $wizard, $current_step_index - 1);
            }

            $this->printListToTelegram($step, $tg_user_id, $tg_chat_id); // print the hint and the keyboard.
        }
        else
        {
            log_message('error', "executeTheStep(): telegram OUTPUT DENIED for ".get_class($step));
        }

        // now advance *step pointer* to the next one (if any) or to Father:
        if (!$bStay_on_same_step && !$bInvalid_input)
        {   if (!WizardHelper::incrementStepIfNotLast($tg_user_id, $tg_chat_id, $wizard->getName(), $current_step_index, $wizard->getStepsCount() - 1))
            {   throw new SwitchToWizardException(WizardFather::NAME, 0);
            }
        }
        else
        {   // staying on the same page. No step increment needed.
            WizardStateManager::setCurrentWizardNameAndStepNo($tg_user_id, $tg_chat_id, $wizard->getName(), $current_step_index);
        }
    }

    // returns true if incremented or false if is not possible.
    private static function incrementStepIfNotLast($tg_user_id, $tg_chat_id, $wizard_name, $current_step_index, $steps_count_index)
    {
//        log_message('error', "incrementStepIfNotLast(): current_step_index:$current_step_index < steps_count_index:$steps_count_index?");
        if ($current_step_index < $steps_count_index)
        {   //log_message('error', "incrementStepIfNotLast(): #$current_step_index is NOT the last step of a wizard '$wizard_name'!");
            WizardStateManager::setCurrentWizardNameAndStepNo($tg_user_id, $tg_chat_id, $wizard_name, $current_step_index + 1);
            return true;
        }

        //log_message('error', "incrementStepIfNotLast(): #$current_step_index IS THE LAST step of a wizard '$wizard_name'...");
        return false;
    }

    // set currently executed wizard's tg_user_id and chat_id:
    public static final function set_TG_context($tg_user_id, $tg_chat_id, $input_data)
    {
        WizardHelper::$tg_user_id = $tg_user_id;
        WizardHelper::$tg_chat_id = $tg_chat_id;
        WizardHelper::$input_data = $input_data;
    }

    // returns the context in which the wizard runs *currently*.
    public static final function get_TG_context()
    {
        if (WizardHelper::$tg_user_id <= 0)
        {   // retrieve missing user_id and chat_id fro the input data:
            if (WizardHelper::$input_data && isset(WizardHelper::$input_data['message']))
            {
                if (isset(WizardHelper::$input_data['message']['from']) && isset(WizardHelper::$input_data['message']['from']['id']))
                {   WizardHelper::$tg_user_id = WizardHelper::$input_data['message']['from']['id'];
                }

                if (isset(WizardHelper::$input_data['message']['chat']) && isset(WizardHelper::$input_data['message']['chat']['id']))
                {   WizardHelper::$tg_chat_id = WizardHelper::$input_data['message']['chat']['id'];
                }
            }
        }
        return array('tg_user_id' => WizardHelper::$tg_user_id, 'tg_chat_id' => WizardHelper::$tg_chat_id, 'input_data' => WizardHelper::$input_data);
    }



    // Throws StepBackException, SwitchToWizardException exceptions, which handled outside of this method: by caller itself!
    //
    //  NOTE: if you passed $wizard and/or $step, it is YOUR responsibility to ensure that they correspond to each other, and $wizard != null !!!
    //  NOTE: "$input_data" is whole the data received from Telegram!
    private function executeWizard($tg_user_id, $tg_chat_id, $command = null, $wizard = null, $step = null, $input_data = null)
    {
//        log_message('error', "executeWizard($tg_user_id, $tg_chat_id, $command): what is CONTEXT here?");

        // if nothing passed in then get the current one:
        if (!$wizard)
        {   $wizard = WizardHelper::getCurrentWizardObject($tg_user_id, $tg_chat_id);
        }

        // if the second attempt failed then we're done:
        if (!$wizard)
        {   log_message('error', "executeWizard1($tg_user_id, $tg_chat_id, $command): no current wizard, so will raise CallFATHER exception...");
            throw new SwitchToWizardException(WizardFather::NAME, 0); // call papa if nobody else is around :)
        }

//        log_message('error', "executeWizard2FULLDATA($tg_user_id, $tg_chat_id, $command): ".print_r($wizard, true));
//        log_message('error', "executeWizard2($tg_user_id, $tg_chat_id, $command): ".print_r($wizard->getName(), true));

        // set currently executed wizard's tg_user_id and chat_id:
        WizardHelper::set_TG_context($tg_user_id, $tg_chat_id, $input_data);

        // if nothing passed in then get the current one:
        if (!$step)
        {   $step   = $this->getStepObject($tg_user_id, $tg_chat_id, $wizard);
        }

        // if the second attempt failed then we're done:
        if (!$step)
        {   throw new SwitchToWizardException(WizardFather::NAME, 0); // call papa if nobody else is around :)
        }

        // NOTE: exceptions here are possible! (SwitchToWizardException, StepBackException)
//        log_message('error', "executeWizard(): before calling executeTheStep()...");
        $this->executeTheStep($tg_user_id, $tg_chat_id, $wizard, $step, $command, $input_data);
//        log_message('error', "executeWizard(): wizard step ".get_class($step)." executed. Before calling discardIfLastStep()...");

        // if no exception raised up then:
        $this->discardIfLastStep($tg_user_id, $tg_chat_id, $wizard, $step);
//        log_message('error', "executeWizard: THE VERY END OF IT!");
    }

    // debug-only.
    private function getStepsString(&$wizard)
    {
        $dump_all_steps = '';
//        log_message('error', "WWWW = ".print_r($wizard, true));
        foreach ($wizard->getSteps() as $step)
        {   $dump_all_steps .= (is_object($step) ? get_class($step) : 'STRING: '.$step).', ';
        }

        return $dump_all_steps;
    }

    private function discardIfLastStep($tg_user_id, $tg_chat_id, &$wizard, &$step)
    {
        $step_descr = "discardIfLastStep(): step(".get_class($step)."): '".(is_object($step) ? $step->get_hint() : 'STRING: '.$step)."'";
//        log_message('error', "discardIfLastStep(): $step_descr");

        $current_step_index = $wizard->getStepIndex(get_class($step));
        $max_index          = $wizard->getStepsCount() - 1;

        $dump_all_steps = $this->getStepsString($wizard);
        log_message('error', "discardIfLastStep(".$wizard->getName()."), $step_descr. Current step index = $current_step_index of $max_index.\nALL STEPS: $dump_all_steps");

        if ($current_step_index == $max_index) // indexes are zero-based, so "-1" is a must here.
        {
//            log_message('error', "discardIfLastStep(): this is acknowledged to be the LAST step.");

            if (WizardFather::NAME == get_class($wizard))
            {   // log_message('error', "discardIfLastStep(): this is FATHER's last step.");

                WizardStateManager::setCurrentWizardNameAndStepNo($tg_user_id, $tg_chat_id, $wizard->getName(), 0); // set it to Father's the very first step.
                return;
            }
            else
            {   log_message('error', "discardIfLastStep(): Hey! This is the VERY LAST STEP for ".$wizard->getName());
                throw new SwitchToWizardException(WizardFather::NAME, 0); // call papa if nobody else is around :)
            }
        }
        //log_message('error', "discardIfLastStep(): NOT the last step for wizard ".$wizard->getName());
    }

    // NOTE: $unstripped_command used for passing raw data submitted by user (useful in some cases).
    //  TODO: make static.
    //  NOTE: "$input_data" is whole the data received from Telegram!
    public function runWizard($tg_user_id, $tg_chat_id, $command = null, $unstripped_command = null, $input_data = null)
    {
         // keep it in case if asked later by some step:
        $ci = &get_instance();
        WizardHelper::$unstripped_command = $ci->security->xss_clean($unstripped_command);

        try
        {   $this->executeWizard($tg_user_id, $tg_chat_id, $command, null, null, $input_data);
        }
        catch (Exception $e)
        {
            if ($e instanceof StepBackException)
            {   // just set the current step number for a wizard:
                $next_step = $this->getStepObject($tg_user_id, $tg_chat_id);
                log_message('error', "StepBackException for ".get_class($next_step));

                $next_step_index    = $next_step->owning_wizard->getStepIndex(get_class($next_step));
                $index_step_minus_2 = $next_step_index - 2; // since it is next, then to come back two steps instead of one.
                if ($index_step_minus_2 >= 0)
                {
                    // NOTE: no need to discard prev. step's data: it will get overwritten and also is useful for knowing the *canceled* choice.
                    log_message('error', "StepBackException reverting from ".get_class($next_step)."(#$next_step_index) to step # $index_step_minus_2");

                    try
                    {
                        $step_minus_2       = $this->getStepObject($tg_user_id, $tg_chat_id, $next_step->owning_wizard, $index_step_minus_2);
                        $step_minus_2_data_obj  = WizardStateManager::getStepData($tg_user_id, $tg_chat_id, $next_step->owning_wizard->getName(), $index_step_minus_2);
                        $step_minus_2_data_value = NULL;
                        if (!$step_minus_2_data_obj)
                        {   log_message('error', "ERROR WH: step_minus_2_data is NULL! No data will be passed for executeTheStep().");
                        }
                        else
                        {   $step_minus_2_data_value = $step_minus_2_data_obj->step_data;
                        }

                        // TODO!!!
                        // TODO: how to run this as a normal call to runWizard() instead??? HANDLING EXCEPTIONS THIS WAY IS A PAIN!!!
                        // TODO!!!
                        if (!$next_step)
                        {   log_message('error', "ERROR WH: next_step is NULL!!!");
                        }

                        $this->executeTheStep($tg_user_id, $tg_chat_id, $next_step->owning_wizard, $step_minus_2, $step_minus_2_data_value, $input_data);
                    }
                    catch (Exception $e)
                    {   log_message('error', "WARNING!!! runWizard() MANUALLY StepBackException-ing failed! :(\n".print_r($e, true));
                        $this->performWizardSwitch($tg_user_id, $tg_chat_id, WizardFather::NAME, 0); // call papa if failed everything else... :(
                    }
                }
                else // if that was the very FIRST step of a wizard then just switch to a Father:
                {   log_message('error', "StepBackException should call papa");
                    $this->performWizardSwitch($tg_user_id, $tg_chat_id, WizardFather::NAME, 0);
                }
                // now wait for the next TG/process/{command} call.
            }
            else if ($e instanceof SwitchToWizardException)
            {   // we get here either because 1) requested manually or 2) once current wizard finished (upon last step execution).
                $new_wizard_name        = $e->getMessage();
                $new_wizard_step_number = $e->getCode();
                $desired_step_classname = $e->get_desired_step_classname();
                $step_metadata          = $e->get_metadata();
                $b_keyboard_redraw_needed = true; // $e->is_echo_needed();
                log_message('error', "runWizard(): SwitchToWizardException(wizard_name:$new_wizard_name, step:$new_wizard_step_number, desired_step_classname=$desired_step_classname)");

                if (NULL != $desired_step_classname) // if it has been passed, then:
                {   // recalculate the $new_wizard_step_number by step classname:
                    $new_wizard_step_number = 0; // some protection in case of errors below.

                    $new_wizard = WizardHelper::get_wizard_object($new_wizard_name);
                    if ($new_wizard)
                    {   $new_wizard_step_number = $new_wizard->getStepIndex($desired_step_classname);
                        if ($new_wizard_step_number < 0)
                        {   $new_wizard_step_number = 0; // some protection in case of errors below.
                        }
                    }
                }

                // passing default values to $recurrence and $b_keyboard_redraw_needed, to pass the metadata:
                $this->performWizardSwitch($tg_user_id, $tg_chat_id, $new_wizard_name, $new_wizard_step_number, false, $b_keyboard_redraw_needed, $step_metadata);                
            }
            else
            {   throw $e; // throw it out.
            }
        }
    }

    // NOTE: if the wizard requested is same then it keeps its data, or cleans the previous wizard's data otherwise.
    private function performWizardSwitch($tg_user_id, $tg_chat_id, $new_wizard_name, $new_wizard_step_number, $recurrence = false, $b_keyboard_redraw_needed = true, $step_metadata = NULL)
    {
    log_message('error', "performWizardSwitch() new_wizard_name=$new_wizard_name, new_wizard_step_number=$new_wizard_step_number, recurrence=".($recurrence?'TRUE':'false'));
        $new_wizard = WizardHelper::get_wizard_object($new_wizard_name);
        if (!$new_wizard)
        {   log_message('error', "runWizard(): Unknown wizard requested: '$new_wizard_name'. Not switching to it!");
            return;
        }

        $current_wizard = WizardHelper::getCurrentWizardObject($tg_user_id, $tg_chat_id);
//        $redraw_keyboard_needed = (NULL !== $current_wizard); // happens when previously executed Wizard consisted of just 1 step (which did no keyboard readraw) - so no need to print a Father's keyboard anew (and disturb users by pick noise).
        $current_wizard_name = is_object($current_wizard) ? $current_wizard->getName() : $current_wizard; // what are cases when the wizard is NOT n object??
        log_message('error', "performWizardSwitch(): from current_wizard='$current_wizard_name' to new wizard='$new_wizard_name', step#$new_wizard_step_number");
        if (0 != strcmp($current_wizard_name, $new_wizard_name))
        {   // no wizard states clean-up in case if Wizard jumps between ITS OWN steps!
            WizardStateManager::discardWizard($tg_user_id, $tg_chat_id, $current_wizard_name);
            $current_wizard = null; // ensure it won't be used!

            // then initialize the new one:
            WizardStateManager::initiateWizard($tg_user_id, $tg_chat_id, $new_wizard->getName(), $new_wizard_step_number);// a mandatory call for each wizard! Sets the wizard's state.

            // NOTE: save it only if not null! Otherwise existing metadata will be occasionally deleted by new SwitchToWizardException() with default parameters!!!
            if ($step_metadata)
            {   WizardStateManager::saveStepMetadataByStepNo($new_wizard, $new_wizard_step_number, $step_metadata);
            }
        }
        else
        {   log_message('error', "no wizard state discard made because the SAME WIZARD called! Just mark the step as current.");
            // no need to save the data (by calling WizardStateManager::saveStep()) and overwrite the previously existing data (e.g. in case of SwitchToWizardException).
//            WizardStateManager::saveStep($tg_user_id, $tg_chat_id, $current_wizard_name, $new_wizard_step_number, 'inition1');
            WizardStateManager::setCurrentWizardNameAndStepNo($tg_user_id, $tg_chat_id, $current_wizard_name, $new_wizard_step_number);

            // NOTE: save it only if not null! Otherwise existing metadata will be occasionally deleted by new SwitchToWizardException() with default parameters!!!
            if ($step_metadata)
            {   WizardStateManager::saveStepMetadataByStepNo($current_wizard, $new_wizard_step_number, $step_metadata);
            }
        }

        $current_step   = $new_wizard->getStep($new_wizard_step_number);
        log_message('error', "new wizard current step: ".get_class($current_step).', title: "'.$current_step->get_hint().'"');
        if ($b_keyboard_redraw_needed)
        {   $this->printListToTelegram($current_step, $tg_user_id, $tg_chat_id, ($current_step instanceof WF_S1_show_commands));
        }

//        log_message('error', "case 111: incrementStepIfNotLast() returned FALSE, WIZARD_NAME=".$new_wizard->getName().". current_step->get_keyboard()==".print_r($current_step->get_keyboard(), true));

        // now wait for the next TG/process/{command} call:
        // now advance *step pointer* to the next one (only if it makes sense):
        if (!WizardHelper::incrementStepIfNotLast($tg_user_id, $tg_chat_id, $new_wizard->getName(), $new_wizard_step_number, $new_wizard->getStepsCount() - 1))
        {
            log_message('error', "case 222: incrementStepIfNotLast() returned FALSE, WIZARD_NAME=".$new_wizard->getName().". current_step->get_keyboard()==".print_r($current_step->get_keyboard(), true));
            if (!$recurrence)
            {
                $current_wizard = WizardHelper::getCurrentWizardObject($tg_user_id, $tg_chat_id);
                log_message('error', "case 333, current_wizard=$current_wizard");

                // e.g. if current step have no keyboard (which is not always true - it could be a set of steps, some of previous ones could redraw the keyboard on its own):
                $b_keyboard_redraw_needed = ((NULL !== $current_step->get_keyboard())
                                            || (get_class($new_wizard) == 'WizardUnauthorizer'));
                $this->performWizardSwitch($tg_user_id, $tg_chat_id, WizardFather::NAME, 0, true, $b_keyboard_redraw_needed);
            }
            else
            {   log_message('error', "incrementStepIfNotLast() this was the LAST STEP called and IGNORED (!) by recurrence, wizard='$new_wizard_name', step # $new_wizard_step_number");
            }
        }
    }

    // wizards' chaining allows (might allow) running multiple wizards simultaneously. So let's kill them all (but Father?).
    public static function terminateAllRunningWizards($tg_user_id, $tg_chat_id)
    {
        while (true)
        {
            $current_wizard_name = WizardStateManager::getCurrentWizardName($tg_user_id, $tg_chat_id);
            if ($current_wizard_name)
            {   WizardStateManager::discardWizard($tg_user_id, $tg_chat_id, $current_wizard_name);
                continue;
            }

            return;
        }
    }

    // this method retuns language-specific set of strigs
    public static function translations($lang)
    {
        if (WizardHelper::$translated_texts)
        {   return WizardHelper::$translated_texts;
        }
        //log_message('error', "translations($lang) CHILD, get_called_class:".get_called_class());

        // otherwise init it:

        switch ($lang)
        {
            case 'ru':
                WizardHelper::$translated_texts = array(
		            Util_telegram::STR_SEARCH               => Util_telegram::STR_SEARCH_RU,
                    OptionsIA::BTN_CANCEL_LOGIN             => OptionsIA::BTN_CANCEL_LOGIN_RU,

                    OptionsIA::BTN_NO                       => OptionsIA::BTN_NO_RU,

                    OptionsIA::BTN_LANGUAGES => OptionsIA::BTN_LANGUAGES_RU,

                    WizardBase::GO_BACK => WizardBase::GO_BACK_RU,

                    WizardSettings::BTN_START_GUEST       => WizardSettings::BTN_START_GUEST_RU,
                    WizardSettings::BTN_START_GUEST_RESCHEDULE   => WizardSettings::BTN_START_GUEST_RESCHEDULE_RU,

                    WizardBase::GO_BACK              => WizardBase::GO_BACK_RU,
                    WizardBase::GO_BACK_HOME         => WizardBase::GO_BACK_HOME_RU,
                    WizardSettings::BTN_LAUNCH       => "\xe2\x9a\x99 Опции",
                    WizardAuthorizer::BTN_LOGIN      => "\xF0\x9F\x94\x91 Войти",
                    WizardUnauthorizer::BTN_LOGOUT   => "\xE2\x98\x9D Выйти",
                );
                break;
        }

        return WizardHelper::$translated_texts;
    }

    //  NOTE: each Step class must call wrap_inline_result() before returning its results from the get_inline_structure() method!!
    //  NOTE:   The most important line here is about setting the 'handler_class' by a CHILD CLASSNAME,
    //          so that later we generate TG inline keyboard correctly. Otherwise the keyboard
    //          generation FAILS SILENTLY (I spent 2 days finding that silly bug).
    public final static function wrap_inline_result($msg, $extra_actions, $inline_options = array('buttons_chunk_size' => 2), $handler_classname = null)
    {
        $current_user = WizardHelper::get_current_user();
        $dict = null;// WizardHelper::translations($current_user->lang);

        //log_message('error', "---- wrap_inline_result() inline buttons: ".print_r($extra_actions, true));

        $extra_actions_translated = array();
        if (!$dict)
        {   $extra_actions_translated = $extra_actions; // no translation is available.
        }
        else
        {
            //-----------------------------------------------------------+
            // translate the buttons:
            $grouped_inlines = array();
            foreach ($extra_actions as $key => $value)
            {
                //log_message('error', "--- KKK: key='$key', value='".print_r($value, true)."'");
                if (is_int($key))
                {   // this is an array inside. Let's ensure that:
                    if (is_array($value))
                    {
                        if (TelegramHelper::is_assoc_array($value))
                        {
                            $tmp = array();
                            foreach ($value as $key_inside => $value_inside) // this is for a group of buttons:
                            {
                                //$tmp[strtoupper($key_inside)] = $value_inside;
                                $tmp[WizardBasicStep::dictionary($dict, $key_inside)] = $value_inside;
                            }

                            $extra_actions_translated[] = $tmp;
                        }
                        else
                        {   ;//log_message('error', "--- ERROR: what the hell to do with this ARRAY1: ".print_r($value, true)); // impossible case1
                        }
                    }
                    else
                    {   ;//log_message('error', "--- ERROR: what the hell to do with this ARRAY2: ".print_r($value, true)); // impossible case2.
                    }
                }
                else
                {   // this is strings pair:
                    //$extra_actions_translated[strtoupper($key)] = $value;
                    //$extra_actions_translated[strtoupper($key)] = $value;
                    $extra_actions_translated[WizardBasicStep::dictionary($dict, $key)] = $value;
                }
            }

            //log_message('error', "---- wrap_inline_result() inline buttons TRANSLATED: ".print_r($extra_actions, true));
            //-----------------------------------------------------------|
        }


        $inline_struct = array(
                                // The following line is THE MOST IMPORTANT (!!!) to generate TG inline keyboard correctly:
                                'handler_class' => $handler_classname ? $handler_classname : get_called_class(), // a must to know the CHILD CLASS NAME! Otherwise VERY COMPLEX Telegram error will happen SILENTLY: keyborad WON'T get GENERATED!!! U-gh, spent 2 days to fix that..

                                'message'       => $msg,
                                'actions'       => $extra_actions_translated, // those are NOT inline buttons yet! They will be wrapped properly by WizardHelper later.
                                'inline_options'=> $inline_options,

                        // NOTE: it is also possible passing 'message_id' to inline edit button.
                    );

        return $inline_struct;
    }
}
