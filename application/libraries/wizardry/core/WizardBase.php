<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
* WizardBase methods' call sequence:
*   1. c'tor
*   2. initSteps() - lazy load/call
*/
abstract class WizardBase
{
    private $name;

    protected $steps;

    const ARROW_LEFT = "\xE2\x86\xA9";
    const GO_BACK    = "\xE2\x86\xA9 Back";
    const GO_BACK_RU   = "\xE2\x86\xA9 Назад";
    const GO_BACK_AM   = "\xE2\x86\xA9 Հետ";

    const GO_BACK_HOME   = "\xF0\x9F\x8F\xA0 Back to home";
    const GO_BACK_HOME_RU  = "\xF0\x9F\x8F\xA0 В начало";
    const GO_BACK_HOME_AM   = "\xF0\x9F\x8F\xA0 Դեպի սկիզբ";

    public function __construct($wizard_name) // TODO: get rid of taking $wizard_name on input: just use get_class($this) for that.
    {
        if (empty($wizard_name))
        {   throw new Exception("The wizard name is missing");
        }

        $this->name = $wizard_name;
        $this->steps= null; // it is a must to init by null here (see "ensureInited()").
    }

    // must init the "$steps" property by WizardBasicStep-inherited objects:
    abstract protected function initSteps();

    public final function getName()
    {   return $this->name;
    }

    public final function getStepsCount()
    {   $this->ensureInited(); // a must to call in each method using steps.
        return count($this->steps);
    }

    // remove this method! debug-only!
    public final function getSteps()
    {   $this->ensureInited(); // a must to call in each method using steps.
        return $this->steps;
    }

    public final function getStep($index)
    {   $this->ensureInited(); // a must to call in each method using steps.
        return (isset($this->steps[$index]) ? $this->steps[$index] : null);
    }

    // returns the step index (if found in steps array of a wizard).
    // Useful for rerieving data specific to the particular step.
    public final function getStepIndex($step_class_name)
    {
        $step_no = 0;
        foreach ($this->steps as $step_obj)
        {
            if ($step_class_name == get_class($step_obj))
            {   return $step_no;
            }

            $step_no++;
        }

        return -1; // invalid step class requested!
    }

    private function ensureInited()
    {   if (!$this->steps) // the C'tor assigned "null" first.
        {   $this->initSteps(); // let's create steps (if none exists).
            //log_message('error', "after ensureInited(): steps count=".count($this->steps));
        }
    }
}
