<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once(APPPATH.'libraries/wizardry/core/WizardBase.php');


class WizardDAL
{
    const LANG_RU = 1;
    const LANG_EN = 2;

    const MENU_ITEM_NEW_ROW = '-'; // separates rows with newline in TG menus.

    public static function calc_crc32($class_name)
    {
        return crc32($class_name);
    }

    // Beautifies the raw (from DB) keyboard so that it could be sent over to Telegram as a menu.
    public static function get_db_keyboard($step_class_name, $lang_code = WizardDAL::LANG_RU)
    {
        $keyboard_raw = WizardDAL::get_db_keyboard_raw(WizardHelper::get_bot_id_crc32(), $step_class_name, $lang_code);

        // compose an array:
        $res = array();
        foreach ($keyboard_raw as $key)
        {
            if (WizardDAL::MENU_ITEM_NEW_ROW == $key->title)
            {   // TODO: somehow start new array (i.e. TG menu new row) inside the $res!!!
            }

            $res[] = $key->title; // NOTE: new row separators to be suppported!
        }

        return array($res, array(WizardBase::GO_BACK)); // adjust new line with GO_BACK command by default.
    }

    // returns null if not found, or a DB row (tg_keyboards) if found.
    public static function search_raw_keyboard__by_title(&$raw_keyboard, $title)
    {
//        log_message('error', "QQQ search_raw_keyboard__by_title: searching '$title' in: ".print_r($raw_keyboard, true));

        foreach ($raw_keyboard as $row)
        {
            if ($title == $row->title) // todo: would this work?!
            {   return $row;
            }
        }

        return null;
    }


    // Returns keyboard DB rows for a specific Step-class: in the language specified, for a specific bot.
    // N.B.: no language fallback solution created as per now (29.06.2016).
    //
    // $lang_code: integer type. 1 - Russian, 2: English
    public static function get_db_keyboard_raw($bot_id_crc32, $step_class_name, $lang_code)
    {
        // check for unsupported language:
        if ($lang_code != WizardDAL::LANG_RU && $lang_code != WizardDAL::LANG_EN)
        {   $lang_code = WizardDAL::LANG_RU; // default to Russian.
        }

        $ci = &get_instance();
        $ci->db->where(array( 'bot_id_crc32'      => $bot_id_crc32,
                              'step_class_crc32'  => WizardDAL::calc_crc32($step_class_name),
                              'lang_code'         => intval($lang_code), )
                         );

        $ci->db->order_by('order');
        $query = $ci->db->get('tg_keyboards');

        return Utilitar_db::safe_resultSet($query);
    }

    // sets all (!) the keyboard items for a step.
    public static function set_keyboard($bot_id_crc32, $step_class_human_readable, $step_class_crc32, $menu_items_list, $lang_code = WizardDAL::LANG_RU)
    {
        $ci = &get_instance();

        // 1. remove step's entire menu:
        $ci->db->where(array( 'bot_id_crc32'    => $bot_id_crc32,
                              'step_class_crc32'=> $step_class_crc32,
                              'lang_code'       => intval($lang_code), )
                         );
        $ci->db->delete('tg_keyboards');


        // 2. insert menu new items (take the order into account):
        $order = 1;
        foreach ($menu_items_list as $item)
        {
            if (0 == strcmp(WizardBase::GO_BACK, $item))
            {   continue;
            }

            $where_array = array(   'bot_id_crc32'      => $bot_id_crc32,
                                    'step_class_name'   => $step_class_human_readable,
                                    'step_class_crc32'  => $step_class_crc32,
                                    'title'             => $item,
                                    'lang_code'         => $lang_code,
                                    'order'             => $order++
                                );

            $ci->db->insert('tg_keyboards', $where_array);
        }
    }
}
