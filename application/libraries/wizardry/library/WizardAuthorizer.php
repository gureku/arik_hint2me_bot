<?php
defined('BASEPATH') OR exit('No direct script access allowed');


include_once(APPPATH.'libraries/wizardry/core/WizardBasicStep.php');
include_once(APPPATH.'libraries/wizardry/core/WizardBase.php');



class WizardAuthorizer extends WizardBase
{
    const NAME = __CLASS__;

    const BTN_LOGIN     = "\xF0\x9F\x94\x91 Login";

    private $wizard_father; // extra-field.

    public function __construct(&$wizard_father)
    {
        parent::__construct(__CLASS__);
        $this->wizard_father = $wizard_father;
    }

    protected function initSteps()
    {
        // Note: steps' order makes sense:
        $this->steps = array(   new WAS1_AskPassword($this, "\xF0\x9F\x9B\x82 Enter your password"),
                                new WAS2_VerifyKey($this/*, 'Проверка ключа'*/));
    }

    public function getWizardFather()
    {
        return $this->wizard_father;
    }
}


class WAS1_AskPassword extends WizardBasicStep
{
    public function get_keyboard()
    {   return array(array(WizardBase::GO_BACK));
    }

    public function process_prev_step_input($userdata, $tg_user_id, $tg_chat_id)
    {
        if (WF_S1_show_commands::CMD_LOGOUT == $userdata)
        {
            $ci = &get_instance();

            $ci->load->model('util_telegram');
            $ci->util_telegram->do_tg_logout($tg_user_id);

//            $wf = $this->owning_wizard->getWizardFather();
//            $res = WizardHelper::set_tg_user_role($wf->tg_user_id, WizardFather::$bot_name, WizardHelper::USER_ROLE__GUEST);

            throw new SwitchToWizardException(WizardFather::NAME, 0);
        }

        return true;
    }
}

class WAS2_VerifyKey extends WizardBasicStep
{
    public function process_prev_step_input($userdata, $tg_user_id, $tg_chat_id)
    {
        if ('' == $userdata) // this & action check shall be default for every Step!
        {   //$this->set_hint('Cancel choosen1');
            log_message('error', "WAS2_VerifyKey no input ($userdata, $tg_user_id, $tg_chat_id)");
            throw new Exception("No password provided!");
        }

        log_message('error', "WAS2_VerifyKey($userdata, $tg_user_id, $tg_chat_id)");

        $ci = &get_instance();
        $ci->load->model('util_telegram');

        $res = $ci->util_telegram->do_tg_login($tg_user_id, $userdata);
        if ($res)
        {
            $tg_user = Util_telegram::get_tg_user_authorized_first($tg_user_id);
            $this->set_hint($tg_user ? 'Здравствуйте, '.$tg_user->ticket_owner_name.'!' : 'Здравствуйте!'); // a fall-back solution.
        }
        else
        {   $this->set_hint('Ключ неверный.');
        }

        return $res;
        // throw new SwitchToWizardException(WizardFather::NAME, 0);
    }
}
