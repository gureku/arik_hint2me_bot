<?php
defined('BASEPATH') OR exit('No direct script access allowed');


include_once(APPPATH.'libraries/wizardry/core/WizardBasicStep.php');
include_once(APPPATH.'libraries/wizardry/core/WizardBase.php');

include_once(APPPATH.'libraries/wizardry/library/Tasker/OptionsIA.php');

class WizardFather extends WizardBase
{
    const NAME = __CLASS__;

    // WARNING: untill called set_tg_user_id__and__bot_name(...), these fields are invalid!
    public $tg_user_id;
    public static $bot_name;

    public function __construct()
    {
        parent::__construct(__CLASS__);

        WizardHelper::routing_assign(array('OptionsIA', ));
    }

    public function set_tg_user_id__and__bot_name($tg_user_id, $bot_name)
    {
        $this->tg_user_id       = $tg_user_id;
        WizardFather::$bot_name = $bot_name;
    }

    protected function initSteps()
    {
        // Note: steps' order makes sense:
        $this->steps = array(   new WF_S1_show_commands($this),
                                new WF_S2_process_command($this),
                            );
    }

    // returns true if NOT needed to authenticate, otherwise returns false.
    // this is WHITE-LISTING.
    public static function action_is_for_anonyms($custom_action_name, $current_user)
    {
        $anonym_actions = array(

            WizardSettings::ACTION_SHOW_MENU,
            WI_S1::ACTION_SHOW_LANGUAGES,
            WI_S1::ACTION_LOGIN,
            WI_S1::ACTION_LOGOUT_CANCEL,

            // Berkana training related functionality:,
            WC_S1::ACTION_SHOW_GROUP_PARTICIPANTS,

            // stream-related events:,
            WC_S1::ACTION__STREAM_SHOW_EVENTS,
            WC_S1::ACTION__STREAM_SHOW_WEEK_EVENTS,
            WC_S1::ACTION__STREAM_EVENT_DETAILS,
            WC_S1::ACTION__STREAM_EVENT_DETAILS_WEEK_CTRL,
            WC_S1::ACTION__STREAM_SHOW_CALENDAR,
            WC_S1::ACTION__STREAM_SHOW_CALENDAR_PAST,
            WC_S1::ACTION__STREAM_SET_TIMEZONE,

            // school actions:,
            WC_S1::ACTION_SCHOOL_SUBSCRIBE_FOR_TEST,
            WC_S1::ACTION_SCHOOL_TEST_BEFORE8,
            WC_S1::ACTION_SCHOOL_TEST_8PLUS,

            // meetings:,
            WC_S1::ACTION_MEETING_MANAGE_SUBSCRIPTIONS,
            WC_S1::ACTION_MEETING_SET_SUBSCRIPTION,

            // set options:
            WizardSettings::BTN_LAUNCH,
            WI_S1::ACTION_SET_LANGUAGE_AM,
            WI_S1::ACTION_SET_LANGUAGE_RU,
            WI_S1::ACTION_SET_LANGUAGE_EN,

            WI_S1::ACTION_SET_USER_ZOOMLINK,
            WI_S1::ACTION_SET_USER_TIMEZONE,
        );

        return in_array($custom_action_name, $anonym_actions);
    }
}

class WF_S1_show_commands extends WizardBasicStep
{
    const CMD_LOGIN = "\xF0\x9F\x94\x91 Authorize";
    const CMD_RESERVATION = "\xF0\x9F\x8E\xAB Забронировать...";
    const CMD_LOGOUT= "\xF0\x9F\x94\x92 Log out";

    // TODO: allow printing IMAGE PREVIEWS! Currently disabled by WizardHelper->printListToTelegram() :(
    public function get_hint()
    {
        $current_user = WizardHelper::get_current_user();

        $img_url = "<a href=\"../../images/SBL_logo.png\">".WizardBasicStep::NONE_SYMBOL."</a>";
        $img_url = '<a href="https://tlgrm.walltouch.ru/tg_sbl_events/images/SBL_logo.png">'.WizardBasicStep::NONE_SYMBOL.'</a>';

        $lang = $current_user ? $current_user->lang : 'en';
        $msg = Util_telegram::get_bot_welcome_message($lang).'\n'.Util_telegram::get_bot_welcome_details($lang);

        return $msg;
    }

    public function get_keyboard()
    {
        return $this->get_menu_items__by_role();
    }

     // Initial Actions/Buttons ar returned HERE!:
    private function get_menu_items__by_role()
    {
        $options = array();
        $current_user = WizardHelper::get_current_user();

        $options[] = WI_S1::get_main_static_buttons($current_user);
        return $options;
    }

    // use it when you want "\x" presentation of UNICODE symbol. E.g. same way I decoded GEAR (Settings) icon for myself.
    private function unicode_to_X_format()
    {
        $gear_icon = json_decode('"\u2699"'); // note the two type of quotes: it is important, otherwise decode won't work!

        $length = strlen($gear_icon);
        $result = '';

        for ($i = 0; $i < $length; $i++)
        {   $result .= '\\x'.str_pad(dechex(ord($gear_icon[$i])),2,'0',STR_PAD_LEFT);
        }

        return $result;
    }

    // greco: the root (i.e. starter) commands to be added here!
    // The "$command" received by FatherWizard corresponds to cases FatherWizard does support.
    // returns null if the requested name is not known to FatherWizard.
     public function getWizardClassName($command)
     {
        $mapping = array(
            '/start'        => 'WizardFather',


            WF_S1_show_commands::CMD_LOGIN     => 'WizardAuthorizer',
            WF_S1_show_commands::CMD_LOGOUT    => 'WizardUnauthorizer',

            // multi-language support:
            WizardSettings::BTN_LAUNCH              => 'WizardSettings',
            WizardTrainerChecker::BTN_TRAINER_MENU  => 'WizardTrainerChecker',

            // same wizard for different roles:
            WizardSettings::BTN_START_CLIENT        => 'WizardTrainerChecker',
            WizardSettings::BTN_START_TEACHER       => 'WizardTrainerChecker',
            WizardSettings::BTN_START_ADMIN         => 'WizardTrainerChecker',
            WizardSettings::BTN_START_GUEST         => 'WizardTrainerChecker',
            WizardSettings::BTN_START_GUEST_RESCHEDULE => 'WizardTrainerChecker',

            //WizardPeopleChecker::MARK_VISITS=> 'WizardPeopleChecker',
//            'Сообщений'     => 'WizardMessages',
//            "\xE2\x9C\x8F Редактировать расписание" => 'WizardTrainingsEditor',
        );
        if (isset($mapping[$command]))
        {   return $mapping[$command];
        }

        return null;
     }

     public function process_prev_step_input($userdata, $tg_user_id, $tg_chat_id)
     {
        // immediately switch to the main wizard to provide inline buttons, etc.:
        throw new SwitchToWizardException(WizardTrainerChecker::NAME, 0);
     }
}


class WF_S2_process_command extends WizardBasicStep
{
    public function get_keyboard()
    {
        return array(); // NOTE: shall we return the very same keyboard as for Step #1 here?
        // return array(array("Вернуться"));
    }

    public function process_prev_step_input($userdata, $tg_user_id, $tg_chat_id)
    {
        // determine if any of child wizards to be called:
        log_message('error', "WF->WF_S2_process_command: process_prev_step_input(userdata='$userdata')");

        $step_obj       = new WF_S1_show_commands($this->owning_wizard, "dummy");

        $wizard_class_name = $step_obj->getWizardClassName($userdata);
        if ($wizard_class_name)
        {   log_message('error', "FW: right before switching to new wizard '$wizard_class_name'..");
            throw new SwitchToWizardException($wizard_class_name, 0);
        }
        /*else if (in_array($userdata, WChldr_S0_ChooseCase::mock_get_children_names())) // greco: if those are child names thn switch to WizardChildren!
        {   throw new SwitchToWizardException(WizardChildren::NAME, 0, $userdata);
        }*/

        $current_user = WizardHelper::get_current_user();
        $lang = $current_user->lang;
        $dict = self::render_dictionary($lang); // get the language-specific dictionary of a class.

        // Callback buttons handling here (those buttons have separate handling flow):
        $callback_query__message_id = TelegramHelper::get_callback_query__message_id(); // it is -1 if a non-callback query called.

        $input_data = WizardHelper::get_tg_input_data();
        log_message('error', "FFF WF_S2_process_command process_prev_step_input(): ($userdata, tguserid:$tg_user_id, tgchatid:$tg_chat_id), callback_query__message_id:$callback_query__message_id");

        //log_message('error', "-- INPUT_DATA: ".print_r($input_data, true));
        log_message('error', "---- WF_S2_process_command userdata: ".print_r($userdata, true));

        if ('' == $userdata && ($callback_query__message_id <= 0)) // this & action check shall be default for every Step!
        {   log_message('error', "step77: no input given ($userdata, $tg_user_id, $tg_chat_id)");
            return false;
        }

        //-----------------------------------------------------------+
        // Callback buttons handling here (those buttons have separate handling flow):
        if ($callback_query__message_id > 0) // check if is it a callback request.
        {
            $res = WizardHelper::routing_invoke($current_user, array('userdata' => $userdata));
            if (!$res)
            {
                throw new SwitchToWizardException(WizardTrainerChecker::NAME, 0);

//                $this->handle_callbacks($userdata);
//                log_message('error', "pppppppppppppppppppppppppppppppppppppppppppppppppppp1");
//                return null;
            }
            else
            {   return null;
            }
        }


// user passed some input, but for WHICH COMMAND??
            $step_callback_data = WizardStateManager::getStepCallbackData($tg_user_id, $tg_chat_id, $this);
            if (!$step_callback_data)
            {   log_message('error', "QQQQQQQQQQ WHAT TO DO IN NEGATIVE SCENARIO??");
                return false;
            }

            $ci = &get_instance();

            $stripped_params= TelegramHelper::strip_callback_params($step_callback_data['callback_data']);
            $msg = Util_telegram::get_string('unrecognized_command', $lang);

            // TODO: h-m-m..  this condition shall be already covered by WizardHelper::routing_invoke() call above! But why we didn't capture it earlier?!??
            if ($step_callback_data && isset($stripped_params[2]) && isset($stripped_params[3]))
            {
//                [1] => WC_S1
//                [2] => AddAbonement
//                [3] => 5

                // so the data entered NOW is the one that was asked by the following INLINE command. So let's process it accordingly:

                $caller_inline_action = $stripped_params[2];
                log_message('error', "FFF caller_inline_action:$caller_inline_action");
                switch ($caller_inline_action)
                {
                    case WC_S1::ACTION_SCHOOL_TEST_8PLUS:
                    case WC_S1::ACTION_SCHOOL_TEST_BEFORE8:
                        log_message('error', "ZZZ WF recorded phone number!");
                        $b_age_below8_years = WizardStateManager::equal_no_emoji(WC_S1::ACTION_SCHOOL_TEST_BEFORE8, $caller_inline_action);
                        $msg = "\xE2\x98\x8E <b>".$userdata."</b> was recorded as your phone number for further communication.\n\nThe candidate is <b>".($b_age_below8_years ? '8 years old or younger' :  'above the age of 8' );
                        $msg .= '.</b>';
                        $msg .= "\n\n";
                        $msg .= "\xF0\x9F\x9A\x80 CIS Language School Administrator will contact you soon.\nStay tuned!";

                        TelegramHelper::echoTelegram($msg);

                        //-----------------------------------------------------------+
                        // NOTE: we cannot call switchToWizardException as this method is being handled OUTSIDE of runWizard() :( So processing is done manually - that's dirty trick.
                        // So doing context cleanup for the new wizard manually:
                        WizardStateManager::clearStepCallbackData($tg_user_id, $tg_chat_id, $this);


                        WizardHelper::set_TG_context($tg_user_id, $tg_chat_id, ''); // ensure to CLEAN THIS UP: expecially misc. INLINE CALLBACK data.
                        WizardStateManager::clearStepCallbackData_explicit($tg_user_id, $tg_chat_id, WizardTrainerChecker::NAME, 0); // housekeeping!
                        log_message('error', "---- HOUSEKEEPNIG DONE?");
                        //-----------------------------------------------------------|

                        // throw new SwitchToWizardException(WizardTrainerChecker::NAME, 0);

                        break;
                }
            }

        log_message('error', "FatherWizard: unsopported wizard called! (name='$userdata'). Stepping BACK to Father then....");
        throw new StepBackException("Called from Wizard Father due to unknown wizard name requested ('$userdata')...");
    }

    public function handle_callbacks($userdata)
    {
        $cb_input = TelegramHelper::get_callback_input($userdata);

        $callback_params            = $cb_input['cb_params'];
        $callback_query__message_id = $cb_input['cb_message_id'];

        $ci = &get_instance();

        try
        {
            //-----------------------------------------------------------+
            // TODO: determine which parent command called this callback??
            log_message('error', "FFF WF_S2_process_command handle_callbacks() BEFORE strip_callback_params:\n".print_r($callback_params, true));
            $stripped_params    = TelegramHelper::strip_callback_params($callback_params);
            //log_message('error', "FFF handle_callbacks() AFTER strip_callback_params(callback_query__message_id:$callback_query__message_id): ".print_r($stripped_params, true));

            $client_id            = -1; // TODO: move this variable to the place where it is needed!
            $custom_action_name = null;
            if (count($stripped_params) >= 3)
            {   $custom_action_name = isset($stripped_params[2]) ? $stripped_params[2] : null;
            }

            if (count($stripped_params) >= 4)
            {   $client_id = isset($stripped_params[3]) ? intval($stripped_params[3]) : -1;
            }

            if ($client_id <= 0)
            {   log_message('error', "TODO: TODO: TODO: handle the case when there's NO USER_ID passed in!");
            }

            $datatype = isset($stripped_params[4]) ? intval($stripped_params[4]) : -1;
            //-----------------------------------------------------------|

            //-----------------------------------------------------------+
            $msg = '';
            $is_user_visited = -1;
            $extra_actions = array();
            $action_title = 'undefined';
            $buttons_chunk_size = 2;
            $disable_web_page_preview = false;
            $ci = &get_instance();
            $ci->load->model('util_meetings');

            $current_user = WizardHelper::get_current_user();
            $lang = $current_user->lang;

            $dict = self::render_dictionary($lang); // get the language-specific dictionary of a class.
            //-----------------------------------------------------------|

            log_message('error', "FFF handle_callbacks1() client_id:$client_id, custom_action_name:'$custom_action_name'");
            $grouped_buttons = null; // some inline actions could modify incoming inline keyboard and return it back - by assigning to "$grouped_buttons".
            // if this is CUSTOM command (WTF I named "custom command"? O-h my oh...) then handle it differently:
            if ($custom_action_name)
            {
                log_message('error', "FFF This is a CUSTOM COMMAND: '$custom_action_name'");
                $trainer_id = Util_tg_berkana::get_trainer_id__by_tg_user_id(WizardFather::$bot_name, $current_user->tg_user_id, $current_user->yurlico_id, $current_user->organization_id);
                $dateformat = WizardTrainerChecker::get_berkana_date_format($current_user->yurlico_id);

                log_message('error', "FFF STEP1 custom_action_name:$custom_action_name");
                
                switch ($custom_action_name)
                {
                    case WC_S1::ACTION__STREAM_SHOW_EVENTS:
                        $dateformat = 'd/m/Y';//WizardTrainerChecker::get_berkana_date_format($current_user->yurlico_id);
                        $msg = WF_S1_show_commands::get_starter_info($current_user, $specific_date, $dateformat, $extra_actions);
                        break;

                    case WizardSettings::BTN_LAUNCH:
//                            $dateformat = 'd/m/Y';//WizardTrainerChecker::get_berkana_date_format($current_user->yurlico_id);
//                            $msg = WF_S1_show_commands::get_starter_info($current_user, $specific_date, $dateformat, $extra_actions);
                        throw new SwitchToWizardException(WizardSettings::NAME, 0);

                        break;

                    case WI_S1::ACTION_SET_LANGUAGE_AM:
                    case WI_S1::ACTION_SET_LANGUAGE_RU:
                    case WI_S1::ACTION_SET_LANGUAGE_EN:
                        if (WizardStateManager::equal_no_emoji(WI_S1::ACTION_SET_LANGUAGE_AM, $custom_action_name))
                        {   $msg = "\xF0\x9F\x87\xA6\xF0\x9F\x87\xB2 Դուք ընտրեցիք Հայերենը";
                            Util_telegram::set_tg_user_language('am', $current_user->tg_user_id);
                        }
                        else if (WizardStateManager::equal_no_emoji(WI_S1::ACTION_SET_LANGUAGE_RU, $custom_action_name))
                        {   $msg = "\xF0\x9F\x87\xB7\xF0\x9F\x87\xBA Вы переключились на русский интерфейс.";
                            Util_telegram::set_tg_user_language('ru', $current_user->tg_user_id);
                        }
                        else if (WizardStateManager::equal_no_emoji(WI_S1::ACTION_SET_LANGUAGE_EN, $custom_action_name))
                        {   $msg = "\xF0\x9F\x87\xAC\xF0\x9F\x87\xA7 You switched to English interface.";
                            Util_telegram::set_tg_user_language('en', $current_user->tg_user_id);
                        }

                        TelegramHelper::echoTelegram($msg);
                        throw new SwitchToWizardException(WizardTrainerChecker::NAME, 0);

                        break;
                }
            }
            else
            {
                log_message('error', "----------------- WARNING WARNING WARNING: THIS BRANCH TO BE DELETED! What's it for?!??");
            }

            // there are cases when we do provide the inline buttons OURSELVES! So account that.
            if (!$grouped_buttons)
            {   // we create inline buttons ONLY if it is not generated manually (yeah, that's possible by MODIFYING the input inline kbd!)
                $grouped_buttons = TelegramHelper::to_callback_grouped_buttons(   WC_S1::NAME,
                                                                                    __CLASS__,
                                                                                    $extra_actions
                                                                                );
            }

            if ($callback_query__message_id > 0)
            {   $options['message_id'] = $callback_query__message_id;
                // $options['answer_text'] => $warning_about_reaching_the_limit,
            }

            $options['buttons_chunk_size'] = $buttons_chunk_size;
            $options['disable_web_page_preview'] = $disable_web_page_preview;

            //log_message('error', "INLINE BUTTONS to show1: ".print_r($grouped_buttons, true));
            TelegramHelper::echo_inline($msg, $grouped_buttons, $options);
        }
        catch (InvalidCallbackException $e)
        {   log_message('error', "WARNING: WF_S1_route_callback_param->route_callback_param(): unrecognized INLINE COMMAND: ".print_r($userdata, true));
            TelegramHelper::echoTelegram_alert("Unrecognized INLINE COMMAND1 :(", true);
        }
    }
}


