<?php
defined('BASEPATH') OR exit('No direct script access allowed');


include_once(APPPATH.'libraries/wizardry/core/WizardBasicStep.php');
include_once(APPPATH.'libraries/wizardry/core/WizardBase.php');


class WizardUnauthorizer extends WizardBase
{
    const NAME = __CLASS__;

    const BTN_LOGOUT    = "\xE2\x98\x9D Log out...";

    private $wizard_father; // extra-field.

    public function __construct(&$wizard_father)
    {
        parent::__construct(__CLASS__);
        $this->wizard_father = $wizard_father;
    }

    protected function initSteps()
    {
        // Note: steps' order makes sense:
        $this->steps = array(new WUAS1($this));
    }

    public function getWizardFather()
    {
        return $this->wizard_father;
    }
}

class WUAS1 extends WizardBasicStep
{
    public function get_hint()
    {
        $ci = &get_instance();
        $ci->load->model('util_telegram');

        $tg_context = WizardHelper::get_TG_context();
        $ci->util_telegram->do_tg_logout($tg_context['tg_user_id'], false);
//        throw new SwitchToWizardException(WizardFather::NAME, 0); // TODO: support this!!! For some reason it ruins the server!
    }
}

