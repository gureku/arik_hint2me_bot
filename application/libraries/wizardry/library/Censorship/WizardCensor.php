<?php
defined('BASEPATH') OR exit('No direct script access allowed');


include_once(APPPATH.'libraries/wizardry/core/WizardBasicStep.php');
include_once(APPPATH.'libraries/wizardry/core/WizardBase.php');

// Manages the chats it is being added as an admin for stop-words or other unwanted activities (as per rules per each chat).
class WizardCensor extends WizardBase
{
    const NAME = __CLASS__;
    const garik_tg_id = 113088389;

    const SKIP_PROCSSING_STOPWORD   = '#невопрос';

    const PREFIX_SHOWHELP           = '/showhelp';
    const PREFIX_TOPIC_DETAILS      = '/topic'; // e.g. "https://telegram.me/tg_events_stream?start=topic889977"

    const ACTION_OTHER_FINDINGS     = 'MoreTopicsButThis'; // gets more topics but the ones listed explicitly.


    public static $censored_chats_ids; // an associative array with IDs monitored and chat's administrators' IDs list!

    // messages from these chats to be removed (banned?) immediately:
    const banned_chats = array(
        'livinginarmenia',
    );

    public function __construct()
    {
        parent::__construct(__CLASS__);
        $ci = &get_instance();

        WizardCensor::$censored_chats_ids = array(
                                                -1001654493563  => array(1633600108 /*Арик*/, WizardCensor::garik_tg_id), // "2022 Relocation / Армения"
                                                -1001263136090  => array(/*WizardCensor::garik_tg_id*/), // "проверка чатбота - Арик"
                                              );
    }

    protected function initSteps()
    {
        // Note: steps' order makes sense:
        $this->steps = array(   new WCENSOR_S0($this, 'step0'),
                                new WCENSOR_S1_Stub($this, 'step1'),
                            );
    }

    public function getWizardFather()
    {
        return $this->wizard_father;
    }

    public static function get_supported_callbacks()
    {
        return array(
                        WizardCensor::ACTION_OTHER_FINDINGS,
                    );
    }

    public static function process_callbacks(array $params)
    {
        $userdata           = $params['userdata'];
        $custom_action_name = $params['custom_action_name'];
        $stripped_params    = $params['stripped_params'];
        $current_user       = $params['current_user'];

        log_message('error', "INLINE: ".__CLASS__." called for '$custom_action_name' action.");

        $msg = "Sorry, I cannot handle '$custom_action_name' inline action yet (1).";
        $extra_actions = array();

        return WizardHelper::wrap_inline_result($msg, $extra_actions, array('buttons_chunk_size' => 2/*Util_telegram::get_option($current_user->tg_user_id, 'SHOW_EVENTS', 2)*/), __CLASS__);
    }

    public static function belongs_to_censored_chats(&$message_structure)
    {
        if (isset($message_structure['chat']) && isset($message_structure['chat']['id']))
        {   $from_chat_id = $message_structure['chat']['id'];
            return in_array($from_chat_id, array_keys(WizardCensor::$censored_chats_ids));
        }

        return false;
    }

    // if not false then returns $from_chat_id.
    public static function is_a_forwarded_message(&$message_structure)
    {   // there are TWO (!!!) ways of message forwarding and now both are being traced! Wow! :))
        if (isset($message_structure['forward_sender_name']) || isset($message_structure['forward_from_chat']) && isset($message_structure['chat']['id']))
        {   return $message_structure['chat']['id'];
        }

        return false;
    }

    // also searches for multiple links only:
    public static function contains_just_links($text)
    {
        $match = null;
        preg_match_all('#\bhttps?://[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#', $text, $match);

        //---------------------------------------------------+
        $urls_total_length = 0;

        $items = $match[0];
        foreach ($items as $url)
        {   $urls_total_length += strlen($url);
        }
        //---------------------------------------------------|

        $flexibility = 2;
        return ((strlen($text) - $urls_total_length) <= $flexibility); // we allow 2 extra symbols to be splitting between multiple URLs (if there are many) and still consider that URL-only text!
    }

    public static function posted_by_chat_admin(&$message_structure)
    {
        $user_id = (isset($message_structure['from']) && isset($message_structure['from']['id'])) ? $message_structure['from']['id'] : null;
        if ($user_id  <= 0)
        {   log_message('error', "---- WARNING: posted_by_chat_admin(): either originator User or destination Chat ID is invalid in data: ".print_r($message_structure, true));
            return false;
        }

//        log_message('error', "---- posted_by_chat_admin(tguid:'$user_id') ALL chat IDs and their admins list: ".print_r(WizardCensor::$censored_chats_ids, true));

        // NOTE: $chat_id is negative for Groups: that's totally acceptable!
        $chat_id = (isset($message_structure['chat']) && isset($message_structure['chat']['id'])) ? $message_structure['chat']['id'] : null;
        $admins_of_the_chat = isset(WizardCensor::$censored_chats_ids[$chat_id]) ? WizardCensor::$censored_chats_ids[$chat_id] : array();


        // this is for cases when the message is posted by GroupAnonymousBot on behalf of the group (not by admins of the chat):
        if ((1 == $message_structure['from']['is_bot']) && isset($message_structure['sender_chat']) && ($chat_id = $message_structure['sender_chat']['id']))
        {   log_message('error', "---- posted_by_chat_admin()=true on behalf of group.");
            return true;
        }

        return in_array($user_id, $admins_of_the_chat);
    }

    // "$inline_query" input paraim is Telegram-formatted structure.
    // returns telegram-ready "$results" array.
    public static function search_by(&$inline_query)
    {
        if (!isset($inline_query['query']) || !isset($inline_query['id']))
        {
            log_message('error', "----- Inline query malformed? ".print_r($inline_query, true));
            throw new Exception('search_by(): Inline query malformed!');
        }

        $res = array();
        $res[] = array(
                "type"                  => "contact",
                'id'                    => bin2hex(random_bytes(32)),
                "phone_number"          => "по названия или содержимому",
                "first_name"            => "\xF0\x9F\x94\x8E Искать",
                "last_name"             => '',

                "input_message_content" => array("message_text" => WizardCensor::PREFIX_SHOWHELP),
             );

        $topics = WizardCensor::_search_for_FAQ_topics($inline_query['query']);
        foreach ($topics as $topic)
        {
            $res[] = array(
                    "type"                  => "article",
                    'id'                    => $topic->id, //bin2hex(random_bytes(32)),
                    "title"                 => $topic->title,
                    "last_name"             => 'key phrases',//$topic->key_phrases,
                    "input_message_content" => array("message_text" => WizardCensor::PREFIX_TOPIC_DETAILS.' '.$topic->id),

                 );
//            $res[] = array(
//                    "type"                  => "contact",
//                    'id'                    => $parent->id, //bin2hex(random_bytes(32)),
//                    "phone_number"          => 'aa-'.$parent->phone, //'+342 (222) '.random_string('numeric', 7),
//                    "first_name"            => $parent->firstname, //'Папа Вася_'.$i,
//                    "last_name"             => 'bb-'.$parent->lastname,
//                    "input_message_content" => array("message_text" => SchoolIA::PREFIX_SHOW_PARENT_CHILDREN.' '.$parent->id),
//                 );
         }

        return $res;
    }
    
    private static function _get_all_FAQ_topics_ids()
    {   $rows = Utilitar_db::_get_entities('faq_topics', null, 'title');
        $tmp_assoc  = DictionaryService::_to_key_rowvalue_array($rows, 'id');
        return array_keys($tmp_assoc);
    }

    private static function _search_for_FAQ_topics($title_or_description_tags)
    {
        $ci = &get_instance();
        $ci->db->from('faq_topics');

        if (mb_strlen($title_or_description_tags))
        {   $ci->db->like('title', $title_or_description_tags);
            $ci->db->or_like('description', $title_or_description_tags);
            $ci->db->or_like('key_phrases', $title_or_description_tags);
        }
        else    // if nothing passed in, then return top 5.
        {   $ci->db->limit(7);
        }

        $ci->db->order_by('title');

        $query = $ci->db->get();
        return Utilitar_db::safe_resultSet($query);
    }

    public static function render_topic_details($topic_id, &$extra_actions)
    {
        $topic = Utilitar_db::_get_entity('faq_topics', array('id' => $topic_id));

        $msg = "Детали по топику #$topic_id:\n\n";
        $msg .= $topic ? $topic->description : 'не найден';

//        $extra_actions[] = array(   WizardBase::GO_BACK     => TelegramHelper::wrap_button_action_name(SchoolIA::ACTION_VIEW_PARENT_KIDS, $topic_id),
//                                );

        return $msg;
    }
}

class WCENSOR_S0 extends WizardBasicStep
{
    const NAME = __CLASS__;

    // returns a hint for a step.
    public function get_hint()
    {   return 'цензор: Шаг 0'; // TODO: determine the current user logged in.
    }

    public function get_keyboard()
    {
        $commands = WCENSOR_S0::get_keyboard_buttons();
        $commands   = array_chunk($commands, 2);

        // adding back-button at the bottom:
        $commands[] = array(WizardBase::GO_BACK);

        return $commands;
    }

    public static function get_keyboard_buttons()
    {   return array('no kbd0 needed');
    }

    public function process_prev_step_input($userdata, $tg_user_id, $tg_chat_id)
    {
        if ('' == $userdata) // this & action check shall be default for every Step!
        {   log_message('error', "WCENSOR_S0: no input given ($userdata, $tg_user_id, $tg_chat_id)");
            return false;
        }

        log_message('error', "WCENSOR_S0 input: ($userdata, $tg_user_id, $tg_chat_id)");

        $msg = 'none';
        $tasks = array();
        $extra_actions = null;
        if (WizardStateManager::equal_no_emoji("somebuttonname", $userdata))
        {
            // some code goes here.
        }

        TelegramHelper::echoTelegram('WCENSOR_S0 finished!');
        return false; // let's show smth to user.

        return null; // enforce to make choice.
    }
}

class WCENSOR_S1_Stub extends WizardBasicStep
{
    const NAME = __CLASS__;

    // returns a hint for a step.
    public function get_hint()
    {   return 'цензор: Шаг 1'; // TODO: determine the current user logged in.
    }

    public function get_keyboard()
    {
        $commands = WCENSOR_S0::get_keyboard_buttons();
        $commands   = array_chunk($commands, 2);

        // adding back-button at the bottom:
        $commands[] = array(WizardBase::GO_BACK);

        return $commands;
    }

    public static function get_keyboard_buttons()
    {   return array('no kbd1 needed');
    }


    public function process_prev_step_input($userdata, $tg_user_id, $tg_chat_id)
    {
        if ('' == $userdata) // this & action check shall be default for every Step!
        {   log_message('error', "WCENSOR_S1_Stub: no input given ($userdata, $tg_user_id, $tg_chat_id)");
            return false;
        }

        log_message('error', "WCENSOR_S1_Stub input : ($userdata, $tg_user_id, $tg_chat_id)");

        $msg = 'none';
        $tasks = array();
        $extra_actions = null;
//        if (WizardStateManager::equal_no_emoji(WPPLMGR_S0::BTN_CREATE_TASK, $userdata))
//        {
//            // some code goes here.
//        }

        TelegramHelper::echoTelegram('WCENSOR_S1_Stub finished!');
        return false; // let's show smth to user.

        return null; // enforce to make choice.
    }
}