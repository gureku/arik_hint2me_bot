<?php
defined('BASEPATH') OR exit('No direct script access allowed');


include_once(APPPATH.'libraries/wizardry/core/WizardBasicStep.php');
include_once(APPPATH.'libraries/wizardry/core/WizardBase.php');
include_once(APPPATH.'libraries/wizardry/core/WizardNavigationHelper.php');


// Serves the online reservations
class WizardSettings extends WizardBase
{
    const NAME = __CLASS__;
    const BTN_LAUNCH = "\xe2\x9a\x99 Options";

    const BTN_START_CLIENT  = "\xF0\x9F\x9A\x80 My Lessons";
    const BTN_START_TEACHER = "\xF0\x9F\x9A\x80 My Lessons";
    const BTN_START_ADMIN   = "\xF0\x9F\x9A\x80 My School";
    const BTN_START_GUEST       = "\xF0\x9F\x8E\xAF Schedule an assessment";
    const BTN_START_GUEST_RU    = "\xF0\x9F\x8E\xAF Записаться на тестирование";
    const BTN_START_GUEST_AM    = "\xF0\x9F\x8E\xAF Գրանցվել տեստավորման";
    const BTN_START_GUEST_RESCHEDULE   = "\xF0\x9F\x8E\xAF Re-schedule an assessment";
    const BTN_START_GUEST_RESCHEDULE_RU  = "\xF0\x9F\x8E\xAF Перезаписаться на тестирование";
    const BTN_START_GUEST_RESCHEDULE_AM  = "\xF0\x9F\x8E\xAF Վերագրանցվել տեստավորման";

    const ACTION_SHOW_MENU = "ActionSettingsShowMenu";

    public function __construct()
    {
        parent::__construct(__CLASS__);
        $ci = &get_instance();

        $ci->load->model('util_telegram');

        WizardHelper::routing_assign(array('OptionsIA',));
    }

    protected function initSteps()
    {
        // Note: steps' order makes sense:
        $this->steps = array(   new WI_S0($this, 'h-m?'),
                                new WI_S1($this),

                                new WI_S3_Stub($this, 'шаг-заглушка'),
                            );
    }

    public function getWizardFather()
    {   return $this->wizard_father;
    }
}

class WI_S0 extends WizardBasicStep
{
    const NAME = __CLASS__;

/*
    // returns translations only.
    public static function translations($lang)
    {
        switch ($lang)
        {
            case 'ru':
                return array(   WI_S1::str_yourtimezone => 'Ваш часовой пояс',
                                WI_S1::str_timezone     => "\xF0\x9F\x95\x97 Часовой пояс...",
                                WizardSettings::BTN_LAUNCH => "\xe2\x9a\x99 Настройки",

                                WizardSettings::BTN_START_CLIENT => "\xF0\x9F\x9A\x80 Мои Занятия",
                                WizardSettings::BTN_START_TEACHER => "\xF0\x9F\x9A\x80 Мои Занятия",
                                WizardSettings::BTN_START_ADMIN => "\xF0\x9F\x9A\x80 Моя Школа",
                             );
                break;

            case 'am':
                return array(   WI_S1::str_yourtimezone => 'Ձեր ժամային գոտին',
                                WI_S1::str_timezone     => "\xF0\x9F\x95\x97 ժամային գոտին...",
                                //WizardSettings::BTN_LAUNCH => "\xe2\x9a\x99 Настройки",

                                WizardSettings::BTN_START_CLIENT => "\xF0\x9F\x9A\x80 Իմ դասերը",
                                WizardSettings::BTN_START_TEACHER => "\xF0\x9F\x9A\x80 Իմ խմբերը",
                                WizardSettings::BTN_START_ADMIN => "\xF0\x9F\x9A\x80 Իմ դպրոցը",
                             );
                break;
        }

        return array();
    }
*/

    public static function get_inline_structure($additional_params = null)
    {
        $current_user = WizardHelper::get_current_user();
        $dict = self::render_dictionary($current_user->lang); // get the language-specific dictionary of a class.

        $extra_actions = array();

        $extra_actions[] = array("\xE2\x98\x95 F.A.Q.inline" => array('url' => Util_telegram::get_bot_FAQ_url()), // that way I create a "URL link button".
                                    );

        if (Util_telegram::is_authorized($current_user) && Util_telegram::is_teacher($current_user, true))
        {   $extra_actions[] = array(
                "\xF0\x9F\x8E\xA6 Set Zoom Link"=> TelegramHelper::wrap_button_action_name(WI_S1::ACTION_SET_USER_ZOOMLINK),
                WI_S1::str_timezone => TelegramHelper::wrap_button_action_name(WI_S1::ACTION_SET_USER_TIMEZONE),
            );

            $extra_actions[] = array(OptionsIA::BTN_LANGUAGES => TelegramHelper::wrap_button_action_name(WI_S1::ACTION_SHOW_LANGUAGES),
                                    );
        }
        else
        {   $extra_actions[] = array(OptionsIA::BTN_LANGUAGES => TelegramHelper::wrap_button_action_name(WI_S1::ACTION_SHOW_LANGUAGES),
                                     WI_S1::str_timezone => TelegramHelper::wrap_button_action_name(WI_S1::ACTION_SET_USER_TIMEZONE),
                                    );
        }

        $grp = array();
        if (Util_telegram::is_authorized($current_user))
        {
            $grp[WizardUnauthorizer::BTN_LOGOUT] = TelegramHelper::wrap_button_action_name(WI_S1::ACTION_LOGOUT);
        }
        else
        {   $grp[WizardAuthorizer::BTN_LOGIN] = TelegramHelper::wrap_button_action_name(WI_S1::ACTION_LOGIN);
        }
        $grp[WizardBase::GO_BACK] = TelegramHelper::wrap_button_action_name(WC_S1::ACTION__STREAM_SHOW_EVENTS);

        $extra_actions[] = $grp;

        $msg = '';
        switch ($current_user->lang)
        {   case 'en':
                $msg = WI_S1::str_options_available;
                break;

           case 'ru':
                $msg = WI_S1::str_options_available_ru;
                break;

           case 'am':
                $msg = WI_S1::str_options_available_am;
                break;
        }

        return WizardHelper::wrap_inline_result($msg, $extra_actions, array('buttons_chunk_size' => 2));
    }
}

class WI_S1 extends WizardBasicStep
{
    const NAME = __CLASS__;

    const ACTION_LOGOUT         = "ActionOptionLogOut";
    const ACTION_LOGIN          = "ActionOptionLogIn";
    const ACTION_LOGOUT_CONFIRM = "ActionOptionLogOutConfirm";
    const ACTION_LOGOUT_CANCEL  = "ActionOptionLogOutCancel";
    const ACTION_SHOW_EVENTS    = "ActionOptionShowEvents";

    // language actions:
    const ACTION_SHOW_LANGUAGES  = "ActionShowLanguages";
    const ACTION_SET_LANGUAGE_RU = "ActionSetLangRu";
    const ACTION_SET_LANGUAGE_EN = "ActionSetLangEn";
    const ACTION_SET_LANGUAGE_AM = "ActionSetLangAm";
    const ACTION_SET_USER_TIMEZONE = "ActionSetUserTimezone";
    const ACTION_SET_USER_ZOOMLINK = "ActionSetUserZoomLink";

    // lang resources:
    const str_confirmlogout     = "\xE2\x9A\xA0 Please <b>confirm</b> whether you want to log out:";
    const str_confirmlogout_ru  = "\xE2\x9A\xA0 Пожалуйста, <b>подтвердите</b> выход из системы:";
    const str_logout_yes        = "\xE2\x9C\x94 Yes, I want to log out!";
    const str_logout_no         = "\xE2\x9C\x96 Nope! I stay.";
    const str_cmd_unrecognized  = "Command is not recognized.";
    const str_yourtimezone      = 'Your timezone';
    const str_sharelocation     = "\xF0\x9F\x93\x8C Share Location";
    const str_tz_press_button = "\xE2\x8F\xA9  Press the \"\xF0\x9F\x93\x8C Share Location\" button below for the bot <b>to identify your timezone</b>.";
    const str_explan_tz_sharing = "Then the bot will be able to show the scheduled events <b>in your local time</b> (rather than the meeting's master time).\n\nAlso recommended when travelling.";
    const str_timezone = "\xF0\x9F\x95\x97 Timezone...";
    const str_options_available = 'Options available:';
    const str_options_available_ru = 'Доступные настройки:';
    const str_options_available_am = 'Options available:';


/*
    // returns translations only.
    public static function translations($lang)
    {
        switch ($lang)
        {
            case 'ru':
                return array(   WI_S1::str_confirmlogout    => "\xE2\x9A\xA0 Пожалуйста, <b>подтвердите</b> выход из системы:",
                                WI_S1::str_logout_yes       => "\xE2\x9C\x94 Да, я хочу выйти!",
                                WI_S1::str_logout_no        => "\xE2\x9C\x96 Нет",
                                WI_S1::str_cmd_unrecognized => "Команда не распознана",
                                WI_S1::str_sharelocation    => "\xF0\x9F\x93\x8C Поделиться геопозицией",
                                WI_S1::str_yourtimezone     => 'Ваш часовой пояс',
                                WI_S1::str_timezone         => "\xF0\x9F\x95\x97 Часовой пояс...",
                                WizardSettings::BTN_LAUNCH => "\xe2\x9a\x99 Настройки",

                                WI_S1::str_tz_press_button    => "\xE2\x8F\xA9  Нажмите внизу кнопку \"\xF0\x9F\x93\x8C Поделиться геопозицией\", чтобы бот <b>определил Ваш часовой пояс</b>.",

                                WI_S1::str_explan_tz_sharing    => "Таким образом мы сможем показывать расписание Ваших занятий <b>в Вашем же часовом поясе</b> (а не в часовом поясе педагога).".
                                  "\n\nВо время путешествий также рекомендуем менять часовой пояс в боте.",

                                WI_S1::str_options_available=> 'Доступные опции:',
                            );
                break;

            case 'am':
                return array(   WI_S1::str_yourtimezone => 'Ձեր ժամային գոտին',
                                WI_S1::str_timezone     => "\xF0\x9F\x95\x97 ժամային գոտին...",
                            );
                break;
        }

        return array();
    }
*/

    // TODO: refactor to remove copy-pase: re-use OptionsIA.php -> case WizardSettings::BTN_LAUNCH, case WizardSettings::ACTION_SHOW_MENU code.
    public function step_back()
    {
        $telegram   = new Telegram(Util_telegram::get_bot_token()); // TODO: refactor this to use chat_id, user_id - instead of instantiating entire complex class for sending messages.

        $current_user = WizardHelper::get_current_user();
        $lang = Util_telegram::get_tg_user_language($current_user->tg_user_id);
        $title = ('ru' == $lang) ? '... возвращаемся в меню Опций' : '... returning to Оptions';


        $message        = str_replace(array('\n'), chr(10), $title); // do a message cleanup
        $content        = array('chat_id' => $telegram->UserId(), 'text' => $message, 'disable_web_page_preview' => false ,'parse_mode' => 'HTML');
        $buttons_array  = array(array(WizardTrainerChecker::BTN_TRAINER_MENU, WizardSettings::BTN_LAUNCH));
        Tg::ensureKeyboardIsCorrect($buttons_array); // a must to call!

        $keyb = $telegram->buildKeyBoard($buttons_array, $onetime=false, $resize = true);
        $content['reply_markup'] = $keyb;

        $message_delivered = $telegram->sendMessage($content);

        throw new SwitchToWizardException(get_class($this->owning_wizard), 0); // after archive view go to the messages main menu instead of stepping back.
    }

    public static function get_main_static_buttons($current_user = null)
    {
        if (!$current_user)
        {   $current_user = WizardHelper::get_current_user();
        }

        $dict = self::render_dictionary($current_user->lang); // get the language-specific dictionary of a class.
        $title_options = WizardSettings::BTN_LAUNCH;
        $title_button1 = '';

        if (Util_telegram::is_authorized($current_user))
        {
            if (Util_telegram::is_client($current_user, true))
            {   $title_button1 = self::dictionary($dict, WizardSettings::BTN_START_CLIENT);
            }
            else if (Util_telegram::is_teacher($current_user, true))
            {   $title_button1 = self::dictionary($dict, WizardSettings::BTN_START_TEACHER);
            }
            else if (Util_telegram::is_club_admin($current_user, true))
            {   $title_button1 = self::dictionary($dict, WizardSettings::BTN_START_ADMIN);
            }
        }
        else
        {   // for guests allow to schedule an assessment:
            $ci = &get_instance();
        }

        return array($title_button1, $title_options);
    }

    // TODO: refactor and move common parts to WizardHelper/TelegramHelper instead!
    // WARNING: callbacks can be from DIFFFERENT buttons at this step!
    public function handle_callbacks($userdata)
    {
        $cb_input = TelegramHelper::get_callback_input($userdata);
                
        $callback_params            = $cb_input['cb_params'];
        $callback_query__message_id = $cb_input['cb_message_id'];

        $ci = &get_instance();

        $current_user = WizardHelper::get_current_user();
        $lang = $current_user->lang;
        $dict = self::render_dictionary($lang); // get the language-specific dictionary of a class.

        try
        {
            //-----------------------------------------------------------+
            $stripped_params    = TelegramHelper::strip_callback_params($callback_params);

            $client_id            = -1; // TODO: move this variable to the place where it is needed!
            $custom_action_name = null;
            if (count($stripped_params) >= 3)
            {   $custom_action_name = isset($stripped_params[2]) ? $stripped_params[2] : null;
            }
            //-----------------------------------------------------------|

            //-----------------------------------------------------------+
            $msg = '';
            $extra_actions = array();
            $action_title = 'undefined';
            $buttons_chunk_size = 2;
            $disable_web_page_preview = false;
            //-----------------------------------------------------------|

            //log_message('error', "Settings handle_callbacks() client_id:$client_id, custom_action_name:'$custom_action_name'");
            $grouped_buttons = null; // some inline actions could modify incoming inline keyboard and return it back - by assigning to "$grouped_buttons".
            // if this is CUSTOM command (WTF I named "custom command"? O-h my oh...) then handle it differently:
            if ($custom_action_name)
            {
                log_message('error', "Settings: is a CUSTOM COMMAND: '$custom_action_name'");

                switch ($custom_action_name)
                {
                    case WI_S1::ACTION_SHOW_EVENTS:
                        throw new SwitchToWizardException(WizardTrainerChecker::NAME, 0); // switch to itself.
                        break;

                    default:
                        $msg = "Action '$custom_action_name' handling is NYI.";
                        $extra_actions[WizardBase::GO_BACK] = TelegramHelper::wrap_button_action_name(WI_S1::ACTION_LOGOUT_CANCEL);
                        break;
                }
            }
            else
            {   log_message('error', "Settings ----------------- WARNING WARNING WARNING: THIS BRANCH TO BE DELETED! What's it for?!??");
            }

            // there are cases when we do provide the inline buttons OURSELVES! So account that.
            if (!$grouped_buttons)
            {   // we create inline buttons ONLY if it is not generated manually (yeah, that's possible by MODIFYING the input inline kbd!)
                $grouped_buttons = TelegramHelper::to_callback_grouped_buttons(WI_S1::NAME, __CLASS__, $extra_actions);
            }

            if ($callback_query__message_id > 0)
            {   $options['message_id'] = $callback_query__message_id;
                // $options['answer_text'] => $warning_about_reaching_the_limit,
            }

            $options['buttons_chunk_size'] = $buttons_chunk_size;
            $options['disable_web_page_preview'] = $disable_web_page_preview;

            TelegramHelper::echo_inline($msg, $grouped_buttons, $options, $current_user->tg_user_id);
        }
        catch (InvalidCallbackException $e)
        {   log_message('error', "WARNING: WI_S1_route_callback_param->route_callback_param(): unrecognized INLINE COMMAND: ".print_r($userdata, true));
            TelegramHelper::echoTelegram_alert("Unrecognized INLINE COMMAND :(", true);
        }
    }

    public static function save_location_and_notify_user(&$res, $tg_user_id)
    {
        $ci = &get_instance();
        $ci->load->model('util_telegram');
        $ci->load->model('utilitar_date');
        $res = $ci->utilitar_date->reverse_geocoding($res); // identify the timezone where user is located currently.

        $humanfriendly_response = 'Error callling reverse geo lookup.';
        $hint = '';
        if (isset($res['httpcode']))
        {
            if (200 != $res['httpcode'])
            {   $humanfriendly_response = 'Error resolving your location details. Drop a line about that to @walltouch to get assistance.';
                log_message('error', "---- ERROR RESOLVING LOCATION details (for tg_user_id:$tg_user_id): ".print_r($res, true));
            }
            else
            {   $compound = json_decode($res['response'], true);
                if (isset($compound['countryName']) && isset($compound['zoneName']))
                {   Util_telegram::set_tg_user_timezone_and_dst($tg_user_id, $compound);

                    $humanfriendly_response = "\xE2\x9C\xA8 Congratulations!\nYour timezone has been set to:\n<b>".$compound['zoneName']."</b>";
                    $humanfriendly_response .= " (<i>".$compound['countryName']."</i>).";
                    $hint = "\n\n<i>Hint:</i> update the timezone once/if you travelled: to see the events schedule in your <b>local time</b>.";
                }
                else
                {   $humanfriendly_response = "Sorry, timezone identification failed.\nDrop a line about that to @walltouch to get assistance.";
                    log_message('error', "---- ERROR DECODING GEO-JSON (for tg_user_id:$tg_user_id): ".print_r($compound, true));
                }
            }
        }
        else
        {   log_message('error', "---- ERROR CALLING reverse geoservice: ".print_r($res, true));
        }

        $ci = &get_instance();
        $ci->load->model('util_telegram');
        $bot_token = Util_telegram::get_bot_token();
        $telegram = new Telegram($bot_token);

        // TG preparations:
        $message = str_replace(array('\n'), chr(10), $humanfriendly_response); // do a message cleanup
        TelegramHelper::hideRegularButtons($message, $tg_user_id); // cleaning up REGULAR BUTTONS (if any). Useful when RETURNING (actually cancelling) from Geo-location assignment menu.

        $extra_actions = array();
        $extra_actions['OK'] = TelegramHelper::wrap_button_action_name(WC_S1::ACTION__STREAM_SHOW_EVENTS);
        $grouped_buttons = TelegramHelper::to_callback_grouped_buttons(WC_S1::NAME, __CLASS__, $extra_actions);

        $message_delivered = TelegramHelper::echo_inline($hint, $grouped_buttons);

        return $message_delivered;
    }

    public function process_prev_step_input($userdata, $tg_user_id, $tg_chat_id)
    {
        // Callback buttons handling here (those buttons have separate handling flow):
        $callback_query__message_id = TelegramHelper::get_callback_query__message_id(); // it is -1 if a non-callback query called.

        $input_data = WizardHelper::get_tg_input_data();
        log_message('error', "WS2 process_prev_step_input(): ($userdata, tguserid:$tg_user_id, tgchatid:$tg_chat_id), callback_query__message_id:$callback_query__message_id");

        $res = TelegramHelper::user_location_received($input_data);
        if (is_array($res))
        {
            $tg_message_delivered = WI_S1::save_location_and_notify_user($res, $tg_user_id);
            return null;// i.e. output nothing: some inline messaging already happened!
        }

        if ('' == $userdata && ($callback_query__message_id <= 0)) // this & action check shall be default for every Step!
        {   log_message('error', "WS2: no input given ($userdata, $tg_user_id, $tg_chat_id)");
            return false;
        }

        //log_message('error', "MMM WI_S1->process_prev_step_input(): ($userdata, $tg_user_id, $tg_chat_id), callback_query__message_id:$callback_query__message_id, input_data: ".print_r($input_data, true));
        $buttons_chunk_size = 2;

        //-----------------------------------------------------------+
        // Callback buttons handling here (those buttons have separate handling flow):
        if ($callback_query__message_id > 0) // check if is it a callback request.
        {
            $current_user = WizardHelper::get_current_user();
            $res = WizardHelper::routing_invoke($current_user, array('userdata' => $userdata));
            if (!$res)
            {   $this->handle_callbacks($userdata);
            }

            return null;
        }

        $current_user = WizardHelper::get_current_user();
        $lang = $current_user->lang;
        $trainer_id = Util_tg_berkana::get_trainer_id__by_tg_user_id(WizardFather::$bot_name, $current_user->tg_user_id, $current_user->yurlico_id, $current_user->organization_id);

        $ci = &get_instance();

        $msg = 'none';
        $users = array();
        $extra_actions = null;
        $disable_web_page_preview = false;
        if (WizardStateManager::equal_no_emoji(WizardSettings::BTN_LAUNCH, $userdata))
        {
            throw new SwitchToWizardException(WizardSettings::NAME, 0); // switch to itself.
        }
        else
        {   // user passed some input, but for WHICH COMMAND??
            $step_callback_data = WizardStateManager::getStepCallbackData($tg_user_id, $tg_chat_id, $this);
            if (!$step_callback_data)
            {   log_message('error', "Settings: what to do in negative scenario??");
                return false;
            }

            $stripped_params= TelegramHelper::strip_callback_params($step_callback_data['callback_data']);
            $msg = Util_telegram::get_string('unrecognized_command', $lang);

            // TODO: h-m-m..  this condition shall be already covered by WizardHelper::routing_invoke() call above! But why we didn't capture it earlier?!??
            if ($step_callback_data && isset($stripped_params[2]) && isset($stripped_params[3]))
            {
                $caller_inline_action = $stripped_params[2];
                log_message('error', "Settings: caller_inline_action: '$caller_inline_action'");
                switch ($caller_inline_action)
                {
                    case WC_S1::ACTION_MEETING_SET_EVENT_DETAILS_URL:
                        $meeting_id = intval($stripped_params[3]);
                        $meeting = Util_meetings::get_meeting($meeting_id);

                        log_message('error', "XXX meeting found: ".print_r($meeting, true).", userdata: '$userdata'");
                        // TODO: parse date & time, also somehow identify the timezone.
                        $msg = "A link to a web-page for\"<strong>$meeting->name</strong>\" saved successfully";
                        Util_meetings::set_meeting_details_webpage($meeting_id, $userdata);

                        WC_S1::generate_changeevent_buttons($lang, $meeting, $extra_actions);
                        break;

                    case WC_S1::ACTION_MEETING_RENAME:
                        $meeting_id = intval($stripped_params[3]);
                        $meeting = Util_meetings::get_meeting($meeting_id);

                        $msg = "Event name changed  from '$meeting->name' to \"<strong>$userdata</strong>\".\n";
                        Util_meetings::set_meeting_title($meeting_id, $userdata);

                        WC_S1::generate_changeevent_buttons($lang, $meeting, $extra_actions);
                        break;

                    case WI_S1::ACTION_LOGIN:
                        $ci->load->model('util_telegram');
                        $res = $ci->util_telegram->do_tg_login($tg_user_id, $userdata);
                        if ($res)
                        {
                            $tg_user = Util_telegram::get_tg_user_authorized_first($tg_user_id);
                            $msg = '<b>'.$tg_user->ticket_owner_name.'</b>, '.Util_telegram::get_bot_welcome_message($lang);


                            // try to update previous message:
                            $tg_last_msg_id = TelegramHelper::get_last_message_id($current_user->tg_user_id, $current_user->tg_user_id);
                            if ($tg_last_msg_id > 0)
                            {   TelegramHelper::echo_inline($msg, array(), array('message_id' => $tg_last_msg_id));
                            }
                            else
                            {   // still notify user:
                                TelegramHelper::echoTelegram($msg);
                            }

                            throw new SwitchToWizardException(WizardTrainerChecker::NAME, 0); // switch to itself.
                        }

                        if ('ru' == $current_user->lang)
                        {   $msg = "\xE2\x9A\xA0 Ошибка в пароле!\n\n";
                            $msg .= "\xF0\x9F\x9B\x82 Пожалуйста, пришлите мне <b>Ваш</b> пароль:";
                        }
                        else
                        {   $msg = "\xE2\x9A\xA0 The password is incorrect!\n\n";
                            $msg .= "\xF0\x9F\x9B\x82 Please send me <b>your</b> password:";
                        }

                        $extra_actions[OptionsIA::BTN_CANCEL_LOGIN] = TelegramHelper::wrap_button_action_name(WI_S1::ACTION_LOGOUT_CANCEL);
                        break;

                    case WI_S1::ACTION_LOGOUT_CANCEL:
                        throw new SwitchToWizardException(WizardSettings::NAME, 0); // switch to itself.
                        break;

                    case WI_S1::ACTION_SHOW_EVENTS:
                        throw new SwitchToWizardException(WizardTrainerChecker::NAME, 0); // switch to itself.
                        break;

                    default:
                        $msg = self::translate($lang, WI_S1::str_cmd_unrecognized);
                        $extra_actions[self::translate($lang, WizardBase::GO_BACK)] = TelegramHelper::wrap_button_action_name(WI_S1::ACTION_LOGOUT_CANCEL);

                        log_message('error', "WARNING WSettings: unhandled INLINE COMMAND received: '$caller_inline_action'");
                        break;
                }
            }

            if ($extra_actions)
            {
                $options = array();
                if ($callback_query__message_id > 0)
                {   $options['message_id'] = $callback_query__message_id;
                    // $options['answer_text'] => $warning_about_reaching_the_limit,
                }

                // set the number of columns in per buttons row:
                $options['buttons_chunk_size'] = $buttons_chunk_size;
                $options['disable_web_page_preview'] = $disable_web_page_preview;

                $grouped_buttons = TelegramHelper::to_callback_grouped_buttons(WI_S1::NAME, __CLASS__, $extra_actions);
                log_message('error', "BBB INLINE BUTTONS to show2: ".print_r($grouped_buttons, true));
                TelegramHelper::echo_inline($msg, $grouped_buttons, $options);
            }
            else
            {
                log_message('error', "BBB INLINE BUTTONS to show3: NO BUTTONS!");
                TelegramHelper::echo_inline($msg);
            }

            return null;
        }

        return null; // enforce to make choice.
    }
}

class WI_S3_Stub extends WizardBasicStep
{
    public function get_keyboard()
    {
        return array(WizardBase::GO_BACK);
    }

    public function process_prev_step_input($userdata, $tg_user_id, $tg_chat_id)
    {
        if ('' == $userdata) // this & action check shall be default for every Step!
        {   log_message('error', "WI_S3_Stub: no input given ($userdata, $tg_user_id, $tg_chat_id)");
            return false;
        }

        log_message('error', "STUB: WI_S3_Stub->process_prev_step_input($userdata, $tg_user_id, $tg_chat_id) returns FALSE.");
        return false;
    }
}
