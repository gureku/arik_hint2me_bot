<?php
defined('BASEPATH') OR exit('No direct script access allowed');


include_once(APPPATH.'libraries/wizardry/core/InlineActionHandlerBase.php');
include_once(APPPATH.'libraries/wizardry/core/WizardBasicStep.php');
include_once(APPPATH.'libraries/wizardry/core/WizardBase.php');
include_once(APPPATH.'libraries/wizardry/core/WizardNavigationHelper.php');


// Serves online reservations
class OptionsIA extends InlineActionHandlerBase
{
    const NAME = __CLASS__;

    const FLAG_AM = "\xF0\x9F\x87\xA6\xF0\x9F\x87\xB2";
    const FLAG_EN = "\xF0\x9F\x87\xAC\xF0\x9F\x87\xA7";
    const FLAG_RU = "\xF0\x9F\x87\xB7\xF0\x9F\x87\xBA";
    const ACTION_VIEW_PAYMENTS_MENU     = "ActionOptionsViewPaymentsMenu";
    const ACTION_PAYMENTS_NEW           = "ActionOptionsNewPayment";
    const ACTION_CLIENT_PAYMENTS_HISTORY= "ActOptionsClientPaymentsHistory";

    const BTN_NEW_PAYMENT = "\xE2\x9E\x95 New Payment";
    const BTN_PAYMENTS = "\xF0\x9F\x94\xB0 Payments";
    const BTN_PAYMENTS_HISTORY = "\xF0\x9F\x95\x97 Payments History";

    const BTN_NEW_PAYMENT_RU = "\xE2\x9E\x95 Новая квитанция";
    const BTN_PAYMENTS_RU = "\xF0\x9F\x94\xB0 Платежи";
    const BTN_PAYMENTS_HISTORY_RU = "\xF0\x9F\x95\x97 Квитанции";

    const BTN_NEW_PAYMENT_AM = "\xE2\x9E\x95 Նոր վճարում";
    const BTN_PAYMENTS_AM = "\xF0\x9F\x94\xB0 Վճարումներ";
    const BTN_PAYMENTS_HISTORY_AM = "\xF0\x9F\x95\x97 Անցյալ վճարումներ";

    const BTN_LANGUAGES = "Languages...";
    const BTN_LANGUAGES_RU = "Языки...";
    const BTN_LANGUAGES_AM = "Լեզուներ...";

    const BTN_CANCEL_LOGIN = "\xE2\x86\xA9 Cancel logging in";
    const BTN_CANCEL_LOGIN_RU = "\xE2\x86\xA9 Отменить";
    const BTN_CANCEL_LOGIN_AM = "\xE2\x86\xA9 Cancel logging in";

    const BTN_NEW_INVITE_LINK = "\xE2\x9E\x95 Generate invite link";
    const BTN_NEW_INVITE_LINK_RU = "\xE2\x9E\x95 Создать приглашение";
    const BTN_NEW_INVITE_LINK_AM = "\xE2\x9E\x95 Generate invite link";

    const BTN_YES_GENERATE = "\xE2\x9E\x95 Yes, generate";
    const BTN_YES_GENERATE_RU = "\xE2\x9E\x95 Да, создать";
    const BTN_YES_GENERATE_AM = "\xE2\x9E\x95 Այո, ստեղծել";

    const BTN_NO = "No";
    const BTN_NO_RU = "Нет";
    const BTN_NO_AM = "Ոչ";

    public function __construct()
    {
        parent::__construct(__CLASS__);
        $ci = &get_instance();

        $ci->load->model('util_telegram');
    }

    // override if inline action does have actions expecting file attachments on input:
    public static function get_callbacks_accepting_attachments()
    {   return array(OptionsIA::ACTION_PAYMENTS_NEW, );
    }

    public static function get_supported_callbacks()
    {
        return array_merge( OptionsIA::get_callbacks_accepting_attachments(),
        
                            array(  WizardSettings::ACTION_SHOW_MENU,
                                    WizardSettings::BTN_LAUNCH,
                                    WI_S1::ACTION_LOGIN,
                                    WI_S1::ACTION_LOGOUT,
                                    WI_S1::ACTION_LOGOUT_CONFIRM,
                                    //WI_S1::ACTION_SHOW_EVENTS,
                                    WI_S1::ACTION_LOGOUT_CANCEL,
                                    WI_S1::ACTION_SET_LANGUAGE_EN,
                                    WI_S1::ACTION_SET_LANGUAGE_AM,
                                    WI_S1::ACTION_SET_LANGUAGE_RU,
                                    WI_S1::ACTION_SET_USER_TIMEZONE,
                                    WI_S1::ACTION_SET_USER_ZOOMLINK,
                                    WI_S1::ACTION_SHOW_LANGUAGES,

                                    OptionsIA::ACTION_VIEW_PAYMENTS_MENU,
                                    OptionsIA::ACTION_CLIENT_PAYMENTS_HISTORY,
                              )
                  );
    }

    public static function process_callbacks(array $params)
    {
        $ci = &get_instance();

        $userdata           = $params['userdata'];
        $custom_action_name = $params['custom_action_name'];
        $stripped_params    = $params['stripped_params'];
        $current_user       = $params['current_user'];

        if (!$current_user)
        {   // attempt #2:
            $current_user = WizardHelper::get_current_user();
            log_message('error', "---- attempt #2 for current user: ".print_r($current_user, true));
        }

        log_message('error', __CLASS__."::process_callbacks(): called for '$custom_action_name' action with current_user: ".print_r($current_user, true));

        $ci = &get_instance();
        $dict = array(); // WizardBasicStep::render_dictionary($current_user->lang); // get the language-specific dictionary of a class.

        $msg = "Sorry, I cannot handle '$custom_action_name' inline action yet (2).";
        $extra_actions = array();
        switch ($custom_action_name)
        {
            case OptionsIA::ACTION_PAYMENTS_NEW:
                $msg = "\n\xF0\x9F\x93\x8E Attach the file (screenshot or other document) about your recent payment.";

                switch ($current_user->lang)
                {
                    case 'ru':
                        $msg = "\xF0\x9F\x93\x8E Прикрепите файл (скриншот или пр. документ), подтверждающий Вашу оплату.";
                        break;

                    case 'am':
                        $msg = "\xF0\x9F\x93\x8E Կցեք վձարման որևէ ֆայլ (կամ screenshot)`";
                        break;
                }

                $extra_actions[WizardBase::GO_BACK] = TelegramHelper::wrap_button_action_name(OptionsIA::ACTION_VIEW_PAYMENTS_MENU);
                break;

            case OptionsIA::ACTION_CLIENT_PAYMENTS_HISTORY:
                OptionsIA::generate_for_client_contract_details_card($current_user->tg_user_id, $current_user->ticket_id, $current_user->lang, $msg, $extra_actions, true);
                break;

            case OptionsIA::ACTION_VIEW_PAYMENTS_MENU:
                OptionsIA::generate_for_client_contract_details_card($current_user->tg_user_id, $current_user->ticket_id, $current_user->lang, $msg, $extra_actions, false);
                break;

            case WizardSettings::BTN_LAUNCH:
            case WizardSettings::ACTION_SHOW_MENU:
            case WI_S1::ACTION_LOGOUT_CANCEL:
                $complex = WI_S0::get_inline_structure();
                $msg            = $complex['message'];
                $extra_actions  = $complex['actions'];

                log_message('error', "OPTIONS BUTTONS: ".print_r($extra_actions, true));
                break;

            case WI_S1::ACTION_LOGIN:
                log_message('error', "IA handling LOGIN....");
                if ('ru' == $current_user->lang)
                {   $msg = "Введите пароль, пожалуйста.\n";
                    $msg .= "\n<i>Примечание: аутентификация нужна для получения доступа к полной функциональности бота.</i>";
                }
                else
                {   $msg = "Please enter your password.\n";
                    $msg .= "\n<i>Note: Authentication is mandatory to access the bot and enjoy its full functionality.</i>";
                }

                $extra_actions[OptionsIA::BTN_CANCEL_LOGIN_RU] = TelegramHelper::wrap_button_action_name(WI_S1::ACTION_LOGOUT_CANCEL);
//                $msg = "\xF0\x9F\x9B\x82 Please <b>send me your password</b>:";
//                $msg .= "\n\n<i>Note1: Authentication is mandatory to access School bot and enjoy its functionality.</i>";
//                $extra_actions["\xE2\x86\xA9 Cancel logging in"] = TelegramHelper::wrap_button_action_name(WI_S1::ACTION_LOGOUT_CANCEL);
                break;

            case WI_S1::ACTION_LOGOUT:
                $msg = ('ru' == $current_user->lang) ? WI_S1::str_confirmlogout_ru : WI_S1::str_confirmlogout;

                $extra_actions[] = array(  WI_S1::str_logout_yes => TelegramHelper::wrap_button_action_name(WI_S1::ACTION_LOGOUT_CONFIRM),
                                           WI_S1::str_logout_no  => TelegramHelper::wrap_button_action_name(WI_S1::ACTION_LOGOUT_CANCEL),
                                        );
                break;

            case WI_S1::ACTION_LOGOUT_CONFIRM:
                $ci->load->model('util_telegram');
                $res = $ci->util_telegram->do_tg_logout($current_user->tg_user_id);
                $msg = ('ru' == $current_user->lang) ? "\xE2\x84\xB9 Вы успешно вышли из системы" : "\xE2\x84\xB9 You logged out successfully!";

                // try to update previous message:
                $tg_last_msg_id = TelegramHelper::get_last_message_id($current_user->tg_user_id, $current_user->tg_user_id);
                if ($tg_last_msg_id > 0)
                {   TelegramHelper::echo_inline($msg, array(), array('message_id' => $tg_last_msg_id));
                }
                else
                {   // still notify user:
                    TelegramHelper::echoTelegram($msg);
                }

                throw new SwitchToWizardException(WizardTrainerChecker::NAME, 0); // switch to itself.
                break;

//            case WI_S1::ACTION_SHOW_EVENTS:
//                throw new SwitchToWizardException(WizardTrainerChecker::NAME, 0); // switch to itself.
//                break;

            case WI_S1::ACTION_LOGOUT_CANCEL:
                $complex = WI_S0::get_inline_structure();
                $msg            = $complex['message'];
                $extra_actions  = $complex['actions'];
                break;

            case WI_S1::ACTION_SET_LANGUAGE_EN:
                Util_telegram::set_tg_user_language('en', $current_user->tg_user_id);
                $msg = OptionsIA::FLAG_EN.' You switched to English interface.';
                //$msg .= "\n You may click /start to reload all the buttons.";

                $complex = WI_S0::get_inline_structure();
                $extra_actions  = $complex['actions'];
                break;

            case WI_S1::ACTION_SET_LANGUAGE_AM:
                Util_telegram::set_tg_user_language('am', $current_user->tg_user_id);
                $msg = OptionsIA::FLAG_AM.' Դուք ընտրեցիք Հայերեն լեզուն';

                $complex = WI_S0::get_inline_structure();
                $extra_actions  = $complex['actions'];
                break;

            case WI_S1::ACTION_SET_LANGUAGE_RU:
                Util_telegram::set_tg_user_language('ru', $current_user->tg_user_id);
                $msg = OptionsIA::FLAG_RU.' Вы переключились на русский интерфейс.';

                $complex = WI_S0::get_inline_structure();
                $extra_actions  = $complex['actions'];
                break;

            case WI_S1::ACTION_SET_USER_TIMEZONE:
                // 1. firstly HIDE the previous inline message
                // 2. then show Geolocation button.

                $msg = '';
                $tz_msg = WizardBasicStep::dictionary($dict, WI_S1::str_yourtimezone);
                if ($current_user->timezone_id)
                {
                    $tz_row = $ci->utilitar_date->get_timezone_row_by_timezone_id($current_user->timezone_id);
                    $msg = $tz_msg.': <b>'.$tz_row->zone_name.'</b> (<i>'.$tz_row->country_name.'</i>).';
                }
                else
                {   $msg = "\xF0\x9F\x94\xB9 ".$tz_msg.' is <b>not set</b>.';
                }

                $msg .= "\n\n";
                $msg .= WizardBasicStep::dictionary($dict, WI_S1::str_explan_tz_sharing);


                $tg_last_msg_id = TelegramHelper::get_last_message_id($current_user->tg_user_id, $current_user->tg_user_id);
                if ($tg_last_msg_id > 0)
                {   TelegramHelper::echo_inline($msg, array(), array('message_id' => $tg_last_msg_id));
                }

                TelegramHelper::echoTelegram_send_location(WI_S1::str_tz_press_button, array(WizardBasicStep::dictionary($dict, WI_S1::str_sharelocation), WizardBasicStep::dictionary($dict, WizardBase::GO_BACK)));
                return true; // process_callbacks() handled normally, just nothing more to return.
                
                break;

            case WI_S1::ACTION_SHOW_LANGUAGES:
                switch ($current_user->lang)
                {   case 'ru':
                        $msg = "Выберите язык интерфейса в боте.";
                        $msg .= "\nВаш текущий язык ";
                        $msg .= OptionsIA::FLAG_RU.' Русский';
                        break;

                    case 'am':
                        $msg = "Set your interface language.";
                        $msg .= "\nYour current language is ";
                        $msg .= OptionsIA::FLAG_AM.' Armenian';
                        break;

                    case 'am':
                    default:
                        $msg = "Set your interface language.";
                        $msg .= "\nYour current language is ";
                        $msg .= OptionsIA::FLAG_EN.' English';
                        break;
                }
                $msg .= '.';

                $extra_actions[] = array(   //'Հայերեն '.OptionsIA::FLAG_AM => TelegramHelper::wrap_button_action_name(WI_S1::ACTION_SET_LANGUAGE_AM),
                                            'On English '.OptionsIA::FLAG_EN => TelegramHelper::wrap_button_action_name(WI_S1::ACTION_SET_LANGUAGE_EN),
                                            'На Русском '.OptionsIA::FLAG_RU => TelegramHelper::wrap_button_action_name(WI_S1::ACTION_SET_LANGUAGE_RU),
                            );

                $extra_actions[WizardBase::GO_BACK] = TelegramHelper::wrap_button_action_name(WI_S1::ACTION_LOGOUT_CANCEL);
                $disable_web_page_preview = true;
                break;
        }

        return WizardHelper::wrap_inline_result($msg, $extra_actions, array('buttons_chunk_size' => 2/*Util_telegram::get_option($current_user->tg_user_id, 'SHOW_EVENTS', 2)*/), __CLASS__);
    }
}
