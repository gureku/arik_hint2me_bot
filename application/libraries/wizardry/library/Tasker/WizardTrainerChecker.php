<?php
defined('BASEPATH') OR exit('No direct script access allowed');


include_once(APPPATH.'libraries/wizardry/core/WizardBasicStep.php');
include_once(APPPATH.'libraries/wizardry/core/WizardBase.php');
include_once(APPPATH.'libraries/wizardry/core/WizardNavigationHelper.php');

include_once(APPPATH.'libraries/wizardry/library/Tasker/OptionsIA.php');

// Serves the online reservations
class WizardTrainerChecker extends WizardBase
{
    const NAME = __CLASS__;

    public function __construct()
    {
        parent::__construct(__CLASS__);
        $ci = &get_instance();

        $ci->load->model('util_telegram');
        $ci->load->service('DictionaryService');

        WizardHelper::routing_assign(array('OptionsIA', ));
    }

    protected function initSteps()
    {
        // Note: steps' order makes sense:
        $this->steps = array(   new WC_S0($this, 'h-m?'),
                                new WC_S1($this),

                                new WC_S3_Stub($this, 'шаг-заглушка'),
                            );
    }

    public function getWizardFather()
    {
        return $this->wizard_father;
    }

    // benchmark start
    public static function bm_start($start_name)
    {
        $ci = &get_instance();
        $ci->benchmark->mark($start_name);
    }

    // benchmark end
    public static function bm_end($start_name, $prefix = '')
    {
        $ci = &get_instance();
        $end_name = $start_name.'_end';
        $ci->benchmark->mark($end_name);
        log_message('error', "--- BENCHMARK($prefix.$start_name)=".$ci->benchmark->elapsed_time($start_name, $end_name).' sec.');
    }
}

class WC_S0 extends WizardBasicStep
{
    const NAME = __CLASS__;

    // returns a hint for a step.
    public function get_hint()
    {
        $current_user = WizardHelper::get_current_user();
        //log_message('error', "XXX current_user1: ".print_r($current_user, true));
        if (Util_telegram::is_guest($current_user, false))
        {   return 'Для доступа к функциям требуется авторизация';
        }
        else if (Util_telegram::is_club_owner($current_user, false))
        {   return 'Добро пожаловать владельцу клуба!';
        }

        return 'Привет от Отмечатора :)'; // TODO: determine the current user logged in.
    }


    //
    // Returns a compound structure showing THE LIST OF TEACHER'S GROUPS (as buttons) at the DATE requested (or just today, if none was explicitly specified).
    //
    // NOTE1: two user objects are used to help to Club Admin: current_user (which might be an Admin) and the Teacher ($berk_trainer) requested (if any). So we account both funcionality below.
    // NOTE2: we must call $this->wrap_inline_result() before returning from this method!!
    // this method called by WizardHelper on very first entry to the wizard. It is expected to show GROUPS LIST for TODAY, capable drilling DOWN to see the GROUP MEMBERS.
    // For that:
    // 1. get current trainer's/Admin's groups for today
    // 2. inside each group put action ACTION_SHOW_GROUP_PARTICIPANTS, specifying its group_id
    public static function get_inline_structure($additional_params = null)
    {
        // this check is needed for CURRENT USER independently whether the info requested fo himself or for another trainer!
        $current_user = WizardHelper::get_current_user();
        if (!Util_telegram::is_authorized($current_user))
        {   ; // TODO: NYI
        }

        if (!$current_user->lang)
        {   Util_telegram::set_tg_user_language('ru', $current_user->tg_user_id); // KIT school uses single language: russian one!
            $current_user->lang = 'ru'; // enforce to Russian language.
        }

        $ci = &get_instance();
        $ci->load->model('util_telegram');

        //-----------------------------------------------------------+
        // get the date asked:
        $dateformat = 'd/m/Y';//WizardTrainerChecker::get_berkana_date_format($current_user->yurlico_id);
        $specific_date = null;
        if ($additional_params && isset($additional_params['date']))
        {   $specific_date = $additional_params['date']; // use the passed in date.
        }

log_message('error', "---- WWW getinlinestruct");
        $extra_actions = array();
        $dateformat = 'd/m/Y';//WizardTrainerChecker::get_berkana_date_format($current_user->yurlico_id);
        $msg = WC_S1::get_starter_info($current_user, $specific_date, $dateformat, $extra_actions);

        return WizardHelper::wrap_inline_result($msg, $extra_actions, array('buttons_chunk_size' => Util_telegram::get_option($current_user->tg_user_id, 'SHOW_EVENTS', 2)));
    }
}

class WC_S1 extends WizardBasicStep
{
    const NAME = __CLASS__;

    private static $ru_weekdays_months;
    private static $en_weekdays_months;

    const ACTION_SHOW_USER_DETAILS      = 'ShowUserDetails';

    const ACTION_EDIT_USER              = 'EditUser'; // currently in the EDIT menu we provide USER INVITE LINK. So this menu is also important!
    const ACTION_UPLOAD_USER_PHOTO      = 'UploadUserPhoto';
    const ACTION_CHANGE_USER_NAME       = 'ChangeUsername';
    const ACTION_CREATE_USER_INVITE_LINK= 'CreateUserInviteLink'; // by default generates a unique link () for the user to get it.

    const TITLE_SHOW_CLIENTS_LIST       = "\xF0\x9F\x9A\xB9 Мои клиенты";
    const ACTION_SHOW_CLIENTS_LIST      = "\xE2\x86\xA9 К списку клиентов...";

    // Berkana training related functionality:
    const ACTION_SHOW_GROUP_PARTICIPANTS    = 'ShowTrGrpMembers';
    const ACTION_SCHOOL_VIEW_GROUP_STUDENTS = 'ActionViewGroupStudents'; // this is similar to ACTION_SHOW_GROUP_PARTICIPANTS but shows students by 'clients_group_assignment' table data.
    const ACTION_SHOW_DAY_GROUPS_FOR_TRAINER= 'ShowTrainerDayGroups';
    const ACTION_SHOW_TRAINER_CALENDAR      = 'ShowTrnCalendar';
    const ACTION_TR_CLIENT_VISIT_TOGGLE     = 'InvTrClVst'; // WARNING: I made its alias SHORTER to make enough room to pass INLINE values: otherwise Telegram itself REJECTS handling LEEEEEEENGTHY INLINE strings as params and says 'BAD BUTTONS' or smth. like that.

    // access-related (administrative) actions:
    const ACTION_ACCESS_SHOW_USERSLIST          = 'AccShowUsersList';
    const ACTION_ACCESS_EDIT_USER_PERMISSIONS   = 'AccEditUserPerms';
    const ACTION_ACCESS_NEW_TICKET              = 'AccCreateTicket';
    const ACTION_ACCESS_NEW_TICKET_CONFIRMED    = 'AccCreateTicketConfirmed'; // used to ask once again before generation of new ticket.
    const ACTION_ACCESS_REVOKE_TICKET           = 'AccRevokeTicket';
    const ACTION_ACCESS_REVOKE_TICKET_CONFIRMED = 'AccRevokeTicketConfirmed'; // used to ask once again before revoking of existing ticket.
    const ACTION_SHOW_GROUPS_BY_AGES            = 'ShowGrpsByAges';

    const ACTION_SHOW_PREV_ITEMS        = 'ShowPrevItems';
    const ACTION_SHOW_NEXT_ITEMS        = 'ShowNextItems';

    // stream-related events:
    const ACTION__STREAM_SHOW_EVENTS        = 'Act_ShowEvents';
    const ACTION__STREAM_SHOW_WEEK_EVENTS   = 'Act_WeekEvents';
    const ACTION__STREAM_EVENT_DETAILS      = 'Act_Details';
    const ACTION__STREAM_EVENT_DETAILS_WEEK_CTRL = 'Act_DetailsWeekCtrl';
    const ACTION__STREAM_EVENT_PARTICIPANTS = 'Act_EventParticipants';
    const ACTION__STREAM_SHOW_VISITS        = 'Act_ShowVisits';
    const ACTION__STREAM_MARK_USER_VISIT    = 'Act_MarkUserVisit';
    const ACTION__STREAM_EDIT_EVENT_TITLE   = 'Act_EditEventTitle';
    const ACTION__STREAM_EDIT_EVENT_DESCR   = 'Act_EditEventDescr';
    const ACTION__STREAM_EDIT_EVENT_DATES   = 'Act_EditEventDates';
    const ACTION__STREAM_CREATE_EVENT       = 'Act_CreateEvent';
    const ACTION__STREAM_SHOW_CALENDAR      = 'Act_ShowCalendar';
    const ACTION__STREAM_SHOW_CALENDAR_PAST = 'Act_ShowCalendarPast'; // shows past months events.
    const ACTION__STREAM_SET_TIMEZONE       = 'Act_SetTimezone';

    // school actions:
    const ACTION_SCHOOL_SHOW_CLIENTS        = 'Act_ShowClients';
    const ACTION_SCHOOL_CREATE_TR_GROUP     = 'Act_CreateTrainingGroup';
    const ACTION_SCHOOL_CREATE_TEACHER      = 'Act_CreateTeacher';
    const ACTION_SCHOOL_CREATE_CLIENT       = 'Act_CreateClient';
    const ACTION_VIEW_SCHOOL_MENU           = 'Act_ViewSchoolMenu';
    const ACTION_VIEW_CLUB_CONTACTS         = 'Act_ViewClubContacts';
    const ACTION_SCHOOL_VIEW_WAITING_LIST   = 'Act_ViewWaitingList';
    const ACTION_SCHOOL_SET_CLIENT_MEMBERSHIP   = 'Act_SetClientMembership';
    const ACTION_SCHOOL_CHOOSE_TEACHER_TO_ASSESS_CLIENT  = 'Act_ChooseTeacher2Assess';
    const ACTION_SCHOOL_REJECT_CLIENT       = 'Act_RejectClient';
    const ACTION_SCHOOL_SUBSCRIBE_FOR_TEST  = 'Act_Subscribe4Test';
    const ACTION_SCHOOL_TEST_BEFORE8        = 'Act_TestBefore8Years';
    const ACTION_SCHOOL_TEST_8PLUS          = 'Act_Test8PlusYearsOld';
    const ACTION_SCHOOL_ASSIGN_CLIENT_TO_TEACHER = 'Act_Client4TeacherTesting';
    const ACTION_SCHOOL_ASSIGN_CLIENT_TO_GROUP = 'Act_AssignClient2Group'; // after testing client to be either rejected or assigned to a group
    const ACTION_SCHOOL_RENAME_CLIENT       = 'Act_RenameClient';
    const ACTION_SCHOOL_RENAME_TEACHER      = 'Act_RenameTeacher';
    const ACTION_SCHOOL_VIEW_TR_GROUPS      = 'Act_ViewTrGroups';
    const ACTION_SCHOOL_VIEW_TR_GROUP_DETAILS   = 'Act_ViewTrGroupDetails';
    const ACTION_SCHOOL_MANAGE_GROUP_INVITATION = 'Act_ManageGroupInvitation';
    const ACTION_SCHOOL_CHOOSE_TEACHER_FOR_GROUP = 'Act_ChoosetTeacher2Group';
    const ACTION_SCHOOL_ASSIGN_TEACHER_TO_GROUP = 'Act_AssignTeacher2Group';
    const ACTION_SCHOOL_VIEW_TEACHERS       = 'Act_ViewTeachers';
    const ACTION_SCHOOL_VIEW_TEACHER_DETAILS= 'Act_ViewTeacherDetails';
    const ACTION_SCHOOL_CREATE_TEACHER_INVITATION       = 'Act_CreateTeacherInvite';
    const ACTION_SCHOOL_CONFIRM_TEACHER_NEW_INVITATION  = 'Act_ConfirmNewInvite';
    const ACTION_SCHOOL_EDIT_TR_GROUP_SCHEDULE          = 'Act_EditTrGrpSchedule';
    const ACTION_SCHOOL_CHANGE_TR_GROUP_WEEKDAY         = 'Act_ChangeGroupWeekday';
    const ACTION_SCHOOL_DELETE_WEEKDAY          = 'Act_WDayDelete';
    const ACTION_SCHOOL_TR_GROUP_RENAME     = 'Act_RenameTrGroup';

    // meetings:
    const ACTION_MEETING_EDIT_EVENT             = 'Act_MeetingShowEventEditControls';
    const ACTION_MEETING_ATTACH_FILES           = 'Act_MeetingAttachFiles';
    const ACTION_MEETING_ATTACH_CLIENT_FILES    = 'Act_MtgAttachClientFiles';
    const ACTION_MEETING_SHOW_ATTACHED_CLIENT_FILES = 'Act_MtgShowAttachedClientFiles';
    const ACTION_MEETING_EDIT_CLIENT_FILES      = 'Act_MtgEditClientFiles';
    const ACTION__MEETING_EDIT_ATTACHMENTS      = 'Act_MeetingEditExistingAttachments'; // show ALL the attachments in a meeting
    const ACTION__MEETING_DELETE_CLIENT_FILE    = 'Act_MtgDeleteClientAttachment';
    const ACTION__MEETING_DELETE_ATTACHMENT     = 'Act_MeetingDeleteAttachment';
    const ACTION_MEETING_SET_RECURRENCE         = 'Act_MeetingSetRecurrence';
    const ACTION_MEETING_RENAME                 = 'Act_MeetingRename';
    const ACTION_MEETING_SET_EVENT_DATETIME     = 'Act_MeetingSetDateTime';
    const ACTION_MEETING_SET_WEB_CONFERENCE_LINK= 'Act_MeetingSetWebConferenceLink';
    const ACTION_MEETING_SET_PRICE              = 'Act_MeetingSetPrice';
    const ACTION_MEETING_SET_EVENT_DETAILS_URL  = 'Act_MeetingSetDetailesURL';
    const ACTION_MEETING_MANAGE_SUBSCRIPTIONS   = 'Act_MeetingManageSubscriptions';
    const ACTION_MEETING_SET_SUBSCRIPTION       = 'Act_MeetingSetSubscription';

    // used to select navigable items from DB:
    const DATATYPE_VISITORS  = 1; // i.e. clients

    const MODE_EVENTS_HELPER= 1;
    const MODE_SCHOOLBOT    = 2;

    // these two are different beasts: groups are sets of people to be inquiried for visits.
    const DATATYPE_TRAINER_GROUPS    = 3; // list of TR_GROUPS
    const DATATYPE_TR_GROUP_PEOPLE   = 4; // list of people in a chosen TR_GROUP
    const DATATYPE_MEETINGS          = 5;

    const PAGE_SIZE         = 11; // abonements paging

    const CL_TYPE__CLIENT   = 1;
    const CL_TYPE__PARTNER  = 2;
    const CL_TYPE__TRAINEE  = 3; // a special case for TRAINERS and their pupils.

    const MSG_CLIENTS_LIST  = "Список Клиентов.";

    // lang resources:
    const str_newgroup = "\xE2\x9E\x95 New Group";
    const str_clients = "\xF0\x9F\x91\xAA Clients";
    const str_school = "\xF0\x9F\x8F\xAB School";

    const str_groups = "\xF0\x9F\x8E\x93 Groups";
    const str_teachers = "\xF0\x9F\x91\x93 Teachers";

    const str_schedule = "\xF0\x9F\x94\xB9 Schedule";
    const str_setteacher = "\xF0\x9F\x91\x89 Set Teacher";

    const str_rename    = "\xE2\x9C\x8F Rename";
    const str_rename_ru = "\xE2\x9C\x8F Переименовать";
    const str_description = "\xF0\x9F\x93\x8B Description";
    const str_description_ru = "\xF0\x9F\x93\x8B Описание";
    const str_datetime  = "\xF0\x9F\x93\x85 Date & Time";
    const str_price     = ' Price';
    const str_zoomconf  = "\xF0\x9F\x8E\xA6 Zoom-conference";
    const str_web       = "\xF0\x9F\x94\x97 Web-page";
    const str_allevents = "\xE2\x86\xA9 All Events";

    const str_participants      = "\xF0\x9F\x91\xAA Participants";
    const str_participants_ru   = "\xF0\x9F\x91\xAA Участники";
    const str_participants_am   = "\xF0\x9F\x91\xAA Անդամներ";

    const str_edit                  = "\xE2\x9C\x8F Edit";
    const str_attachfiles           = "\xF0\x9F\x93\x8E Attach files...";
    const str_deleteattachments     = "\xE2\x9D\x8C Delete attachments...";
    const str_deleteattachments_ru  = "\xE2\x9D\x8C Удалить вложения...";
    const str_deleteattachments_am  = "\xE2\x9D\x8C Հեռացնել հավելումը...";

    const str_delete        = "\xE2\x9D\x8C Delete ";
    const str_delete_ru     = "\xE2\x9D\x8C Удалить";
    const str_mysubscriptions     = "мои подписки";
    const str_mysubscriptions_ru    = "мои подписки ru";
    const str_mysubscriptions_am    = "мои подписки am";
    const str_back2events    = "str_back2events";

    // override if inline action does have actions expecting file attachments on input:
    public static function get_callbacks_accepting_attachments()
    {   return array();
    }

    public function step_back()
    {
        $current_user = WizardHelper::get_current_user();
        TelegramHelper::hideRegularButtons('returning from Timezone...', $current_user->tg_user_id); // cleaning up REGULAR BUTTONS (if any). Useful when RETURNING (actually cancelling) from Geo-location assignment menu.

        throw new SwitchToWizardException(get_class($this->owning_wizard), 0); // after archive view go to the messages main menu instead of stepping back.
    }


    public static function get_starter_info(&$current_user, $specific_date, $dateformat, &$extra_actions)
    {
        $ci = &get_instance();

        log_message('error', "---- WTC get_starter_info(specific_date:'$specific_date', dateformat='$dateformat' in TGEVENTS");

        // format the textual info & message:
        $msg = $current_user->ticket_owner_name.'\n';
        $msg .= '\n'.Util_telegram::get_bot_welcome_message($current_user->lang);
        $extra_actions[WizardSettings::BTN_LAUNCH] = TelegramHelper::wrap_button_action_name(WizardSettings::ACTION_SHOW_MENU);

        return $msg;
    }

    // TODO: refactor and move common parts to WizardHelper/TelegramHelper instead!
    // WARNING: callbacks can be from DIFFFERENT buttons at this step!
    public function handle_callbacks($userdata)
    {
        $cb_input = TelegramHelper::get_callback_input($userdata);

        $callback_params            = $cb_input['cb_params'];
        $callback_query__message_id = $cb_input['cb_message_id'];

        $ci = &get_instance();
        $ci->load->model('util_properties');

        try
        {
            //-----------------------------------------------------------+
            // TODO: determine which parent command called this callback??
            log_message('error', "МММ WC_S1 handle_callbacks() BEFORE strip_callback_params:\n".print_r($callback_params, true));
            $stripped_params    = TelegramHelper::strip_callback_params($callback_params);
            //log_message('error', "МММ handle_callbacks() AFTER strip_callback_params(callback_query__message_id:$callback_query__message_id): ".print_r($stripped_params, true));

            $client_id            = -1; // TODO: move this variable to the place where it is needed!
            $custom_action_name = null;
            if (count($stripped_params) >= 3)
            {   $custom_action_name = isset($stripped_params[2]) ? $stripped_params[2] : null;
            }

            if (count($stripped_params) >= 4)
            {   $client_id = isset($stripped_params[3]) ? intval($stripped_params[3]) : -1;
            }

            if ($client_id <= 0)
            {   log_message('error', "TODO: TODO: TODO: handle the case when there's NO USER_ID passed in!");
            }

            $datatype = isset($stripped_params[4]) ? intval($stripped_params[4]) : -1;
            //-----------------------------------------------------------|

            //-----------------------------------------------------------+
            $msg = '';
            $is_user_visited = -1;
            $extra_actions = array();
            $action_title = 'undefined';
            $buttons_chunk_size = 2;
            $disable_web_page_preview = false;
            $ci = &get_instance();

            $current_user = WizardHelper::get_current_user();
            //-----------------------------------------------------------|

            log_message('error', "МММ handle_callbacks() client_id:$client_id, custom_action_name:'$custom_action_name'");
            $grouped_buttons = null; // some inline actions could modify incoming inline keyboard and return it back - by assigning to "$grouped_buttons".
            // if this is CUSTOM command (WTF I named "custom command"? O-h my oh...) then handle it differently:
            if ($custom_action_name)
            {
                log_message('error', "МММ This is a CUSTOM COMMAND: '$custom_action_name'");
                switch ($custom_action_name)
                {
                    case WizardSettings::BTN_LAUNCH:
                        throw new SwitchToWizardException(WizardSettings::NAME, 0);
                        break;
                }
            }
            else
            {   log_message('error', "----------------- WARNING WARNING WARNING: THIS BRANCH TO BE DELETED! What's it for?!??");
            }

            // there are cases when we do provide the inline buttons OURSELVES! So account that.
            if (!$grouped_buttons)
            {   // we create inline buttons ONLY if it is not generated manually (yeah, that's possible by MODIFYING the input inline kbd!) 
                $grouped_buttons = TelegramHelper::to_callback_grouped_buttons(   WC_S1::NAME,
                                                                                    __CLASS__,
                                                                                    $extra_actions
                                                                                );
            }

            if ($callback_query__message_id > 0)
            {   $options['message_id'] = $callback_query__message_id;
                // $options['answer_text'] => $warning_about_reaching_the_limit,
            }

            $options['buttons_chunk_size'] = $buttons_chunk_size;
            $options['disable_web_page_preview'] = $disable_web_page_preview;

            // use this method to translate the buttons:
            $translated = WizardHelper::wrap_inline_result($msg, $grouped_buttons, array('buttons_chunk_size' => $buttons_chunk_size));
            
            TelegramHelper::echo_inline($msg, $translated['actions'], $options);
        }
        catch (InvalidCallbackException $e)
        {   log_message('error', "WARNING: WC_S1_route_callback_param->route_callback_param(): unrecognized INLINE COMMAND: ".print_r($userdata, true));
            TelegramHelper::echoTelegram_alert("Unrecognized INLINE COMMAND1 :(", true);
        }
    }

    public function process_prev_step_input($userdata, $tg_user_id, $tg_chat_id)
    {
        $ci = &get_instance();
        $ci->load->model('util_properties');
        $ci->load->model('util_telegram');

        $current_user = WizardHelper::get_current_user();

        // Callback buttons handling here (those buttons have separate handling flow):
        $callback_query__message_id = TelegramHelper::get_callback_query__message_id(); // it is -1 if a non-callback query called.

        $input_data         = WizardHelper::get_tg_input_data();
        $stripped_params    = WizardStateManager::get_stripped_params($input_data, $this, $tg_user_id, $tg_chat_id);
        $inline_action_name = ($stripped_params && isset($stripped_params[2])) ? $stripped_params[2] : null;

        log_message('error', "---- XXX STEP_1 process_prev_step_input(): ($userdata, tguserid:$tg_user_id, tgchatid:$tg_chat_id), callback_query__message_id:'$callback_query__message_id'");

        log_message('error', "---- INPUT_DATA: ".print_r($input_data, true));
//        log_message('error', "---- XXX USERDATA: ".print_r($userdata, true));

        if (isset($input_data['message']['via_bot']))
        {   // this looks like a search-result sent by bot.
            $message_string = $input_data['message']['text'];

            log_message('error', "--- DID I FIND substring '".WizardCensor::PREFIX_TOPIC_DETAILS."' here?\n\n".print_r($message_string, true));
            if ((false !== strpos($message_string, WizardCensor::PREFIX_TOPIC_DETAILS))
               )
            {
                $class_id   = isset($stripped_params[3]) ? intval($stripped_params[3]) : null;
                $extra_actions = array();
                $msg = '';

//                log_message('error', "---- YYY stripped_params: ".print_r($stripped_params, true));
//                log_message('error', "---- YYY input data: ".print_r($input_data, true));

                if (false !== strpos($message_string, WizardCensor::PREFIX_TOPIC_DETAILS))
                {   // Since an inline SEARCH returns both Parent and Student rows (why not!), we shall consider both types here:
                    $topic_id  = intval(substr($input_data['message']['text'], strlen(WizardCensor::PREFIX_TOPIC_DETAILS))); // TODO: it's better to get the class_id also from here!
                    log_message('error', "----------------- search for specific topic ID! : '$topic_id'");

                    $msg = WizardCensor::render_topic_details($topic_id, $extra_actions);
                }

                $grouped_buttons = TelegramHelper::to_callback_grouped_buttons(WC_S1::NAME, __CLASS__, $extra_actions);
                TelegramHelper::echo_inline($msg, $grouped_buttons);

                return null;// i.e. output nothing: some inline messaging already happened!
            }

            if (isset($input_data['message']['contact']))
            {
                $contact = $input_data['message']['contact'];
                log_message('error', "---- check out the CONTACT: ".print_r($contact, true));
                $msg = "Choose parent's '<b>".$contact['first_name'].' '.$contact['last_name'].'</b>, phone: '.$contact['phone_number']."' child or add new one to the class <b>ABC</b>!";
                $extra_actions = array();


                $extra_actions[] = array(
                        "\xF0\x9F\x99\x8B Добавить ребёнка..." => TelegramHelper::wrap_button_action_name(WC_S1::ACTION__STREAM_SHOW_EVENTS),
                        WizardBase::GO_BACK => TelegramHelper::wrap_button_action_name(SchoolIA::ACTION_MANAGE_CLASS_STUDENTS),
                );
                $grouped_buttons = TelegramHelper::to_callback_grouped_buttons(WC_S1::NAME, __CLASS__, $extra_actions);
                TelegramHelper::echo_inline($msg, $grouped_buttons);

                return null; // nothing more to do: we handled the input that was expected by some (InlineHandler class) code.
            }
            else if (isset($input_data['message']['article']))
            {
                log_message('error', "---- ARTICLE received: ".print_r($input_data['message'], true));
            }
            else
            {
                log_message('error', "--------------- UNEXPECTED BRANCH !!!!!!!!!!!!!!!!!!!!!!!!!!!");
            }
        }

        $inline_query = TelegramHelper::inline_query_received($input_data);
        if (is_array($inline_query))
        {
            log_message('error', "---- WTC: inline query received: ".print_r($inline_query, true));

            $search_results = null;
            if (true /*Util_telegram::is_club_admin($current_user, true)*/)
            {   $search_results = WizardCensor::search_by($inline_query);
            }
            else
            {
                $search_results = array();
                $search_results[] = array(
                        "type"                  => "contact",
                        'id'                    => bin2hex(random_bytes(32)),
                        "phone_number"          => "Поиск доступен только администраторам системы.",
                        "first_name"            => "\xF0\x9F\x9A\xAB Недостаточно",
                        "last_name"             => ' прав доступа.',

                        "input_message_content" => array("message_text" => '/start'),
                     );
            }

            $telegram = new Telegram(Util_telegram::get_bot_token()); // TODO: refactor this to use chat_id, user_id - instead of instantiating entire complex class for sending messages.
            $res = $telegram->answerInlineQuery(array(  'inline_query_id'   => $inline_query['id'], // strval($inline_query['id']),
                                                        'results'           => json_encode($search_results),
                                                        'ok'                => true,
                                                        'cache_time'        => 1,
//                                                        'is_personal'       => false, // disable caching!!!!
//                                                        'cache_time'       => 10, // cache for 10 seconds only.
                                                     )
                                               );
            return null;
        }
        else if (isset($input_data['message']['via_bot']) )//&& $input_data['message']['contact'])
        {
            log_message('error', "--- data received via bot: ".print_r($input_data['message']['via_bot'], true));
            // 1. identify current inline action which requested that data
            // 2. suplpy that data to that inline bot fo furhter processing
        }
        else
        {   ;//log_message('error', "--- data received222: ".print_r($input_data, true));
        }

        //-----------------------------------------------------------+
        // with multilanguage suport checking regular buttons' text complicates the things:
        $btn_title_back     = WizardBase::GO_BACK;
        $btn_title_options  = WizardSettings::BTN_LAUNCH;

        $start_buttons = array(
            WizardSettings::BTN_START_CLIENT,
            WizardSettings::BTN_START_TEACHER,
            WizardSettings::BTN_START_ADMIN,
            WizardSettings::BTN_START_GUEST,
            WizardSettings::BTN_START_GUEST_RESCHEDULE,
        );
        //-----------------------------------------------------------|

        if (WizardStateManager::equal_no_emoji($btn_title_back, $userdata))
        {   throw new SwitchToWizardException(WizardTrainerChecker::NAME, 0);
        }
        else if (WizardStateManager::equal_no_emoji($btn_title_options, $userdata))
        {   throw new SwitchToWizardException(WizardSettings::NAME, 0);
        }
        else
        {   // check each possible case in a loop:
            foreach ($start_buttons as $button_title)
            {
                if (WizardStateManager::equal_no_emoji($button_title, $userdata))
                {   throw new SwitchToWizardException(WizardTrainerChecker::NAME, 0);
                }
            }
        }

        $extra_actions = array();
        $b_is_for_meeting = false; // i.e. the file has been attached for a meeting.
        $res = false;

        if ($inline_action_name)
        {
            // try to identify if this is a file attachment for some of actions supporting file attahcment handling:
            if (   InlineActionHandlerBase::in_actions($inline_action_name, WC_S1::get_callbacks_accepting_attachments()) ||
                   InlineActionHandlerBase::in_actions($inline_action_name, OptionsIA::get_callbacks_accepting_attachments())
               )
            {   ;//
            }
        }

        if (false === $res)
        {   ;// this means nothing has been attached. So just continue to the regular flow.
        }
        else if (is_array($res)) // if those file(s) have been attached, then this is the branch to enter:
        {
            $msg = null;

            if (WizardStateManager::equal_no_emoji(OptionsIA::ACTION_PAYMENTS_NEW, $inline_action_name))
            { // handle the Payment file attachment action: results here
                $msg = $res['msg'];
                $extra_actions[WizardBase::GO_BACK] = TelegramHelper::wrap_button_action_name(OptionsIA::ACTION_VIEW_PAYMENTS_MENU);
                $grouped_buttons = TelegramHelper::to_callback_grouped_buttons(WC_S1::NAME, __CLASS__, $extra_actions);
                
                TelegramHelper::echo_inline($msg, $grouped_buttons);

                return null;// i.e. output nothing: some inline messaging already happened!
            }

            $msg = $res['msg']."\n\n".$msg;

            $grouped_buttons = TelegramHelper::to_callback_grouped_buttons(WC_S1::NAME, __CLASS__, $extra_actions);
            TelegramHelper::echo_inline($msg, $grouped_buttons, array('buttons_chunk_size' => 2));
            //-----------------------------------------------------------|

            return null;// i.e. output nothing: some inline messaging already happened!
        }

        if ('' == $userdata && ($callback_query__message_id <= 0)) // this & action check shall be default for every Step!
        {   log_message('error', "step1: no input given ($userdata, $tg_user_id, $tg_chat_id)");
            return false;
        }

        $buttons_chunk_size = 2;

        //-----------------------------------------------------------+
        // Callback buttons handling here (those buttons have separate handling flow):
        if ($callback_query__message_id > 0) // check if is it a callback request.
        {
            $res = WizardHelper::routing_invoke($current_user, array('userdata' => $userdata));
            if (!$res)
            {   $this->handle_callbacks($userdata);
            }

            return null;
        }

        $ci = &get_instance();
        $ci->load->model('util_telegram');
        $ci->load->model('util_properties');

        $msg = 'none';
        $extra_actions = null;
        $disable_web_page_preview = false;

        if (WizardStateManager::equal_no_emoji(WizardSettings::BTN_LAUNCH, $userdata))
        {
            throw new SwitchToWizardException(WizardSettings::NAME, 0);
        }
        else
        {   // user passed some input, but for WHICH COMMAND??
            $msg = Util_telegram::get_string('unrecognized_command');

            // TODO: h-m-m..  this condition shall be already covered by WizardHelper::routing_invoke() call above! But why we didn't capture it earlier?!??
            if ($inline_action_name && $stripped_params && isset($stripped_params[3]))
            {
                // so the data entered NOW is the one that was asked by the following INLINE command. So let's process it accordingly:
                log_message('error', "WTC inline_action_name:'$inline_action_name'");
                switch ($inline_action_name)
                {
                    default:
                        log_message('error', "WARNING WTC: unhandled INLINE COMMAND received: '$inline_action_name'");
                        break;
                }
            }

            if ($extra_actions)
            {
                $options = array();
                if ($callback_query__message_id > 0)
                {   $options['message_id'] = $callback_query__message_id;
                    // $options['answer_text'] => $warning_about_reaching_the_limit,
                }

                // set the number of columns in per buttons row:
                $options['buttons_chunk_size'] = $buttons_chunk_size;
                $options['disable_web_page_preview'] = $disable_web_page_preview;

                $grouped_buttons = TelegramHelper::to_callback_grouped_buttons(WC_S1::NAME, __CLASS__, $extra_actions);
                TelegramHelper::echo_inline($msg, $grouped_buttons, $options);

                log_message('error', "INLINE BUTTONS to show2: ".print_r($grouped_buttons, true));
            }
            else
            {
                log_message('error', "INLINE BUTTONS to show99: NO BUTTONS!");
                TelegramHelper::echo_inline($msg);
            }

            // let's update the message which is already shown on a screen:
            return null;
        }

        return null; // enforce to make choice.
    }

    // WARNING: it is a must to PRIOR CALL ATTACHMENTS_IS_FOR_MEETING() to ensure this one is a correct method to call!
    public static function extract_inline_message_counterparts(&$input_data)
    {
        // get TG message's counterparts:
        $tg_message     = $input_data['message'];
        log_message('error', "---- Attachments tg message1: ".print_r($tg_message, true));

        $caption        = isset($tg_message['caption']) ? $tg_message['caption'] : null; // the title that user gave to a bunch of attachments.
        // NOTE: for multiple files this is set, for a SINGLE file (but with MULTIPLE thumbnails) this is a flag to use for SINGLE photo processing!!! :)))
        $media_group_id = isset($tg_message['media_group_id']) ? $tg_message['media_group_id'] : null; // hurray!! See the note above about identifying SINGLE vs. MULTIPLE images upload.

        return array('tg_message' => $tg_message, 'caption' => $caption, 'media_group_id' => $media_group_id);
    }
}

class WC_S3_Stub extends WizardBasicStep
{
    public function get_keyboard()
    {
        return array(WizardBase::GO_BACK);
    }

    public function process_prev_step_input($userdata, $tg_user_id, $tg_chat_id)
    {
        if ('' == $userdata) // this & action check shall be default for every Step!
        {   log_message('error', "WC_S3_Stub: no input given ($userdata, $tg_user_id, $tg_chat_id)");
            return false;
        }

        log_message('error', "STUB: WC_S3_Stub->process_prev_step_input($userdata, $tg_user_id, $tg_chat_id) returns FALSE.");
        return false;
    }
}
