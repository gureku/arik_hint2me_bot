<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FileService
 *
 * @author RoundArh
 */
class FileService extends Service
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('utilitar_db');
    }

    public function register_file($user_id, $file_name, $file_title, $file_path)
    {
        $fileId = null;
        $this->db->trans_start();
        $this->db->insert('fileinfo', array('name' => $file_name, 'title' => $file_title, 'created_by' => $user_id));
        $fileId = $this->db->insert_id();
        $this->db->insert('filedata', array('id' => $fileId, 'path' => $file_path));
        $this->db->trans_complete();
        return $fileId;
    }

    // to delete physically call:
    //    $id = 1234; // file id to remove.
    //    $user_id = $this->ion_auth->get_user_id();
    //    $fileData = $this->FileService->GetFileData($user_id, $id);
    //    unlink(APPPATH . $fileData->Path);
    //    $this->FileService->DeleteFile($user_id, $id);
    public function DeleteFile($user_id, $fileId)
    {
        $this->db->where('id', $fileId);
        $this->db->delete('fileinfo');
    }

    public function GetFileInfo($fileId)
    {
        return $this->utilitar_db->_getTableRow('filedata', 'id', $fileId);
    }

    public function GetFileData($user_id, $fileId)
    {
        $this->db->select('fi.id, fi.name , d.Path ');
        $this->db->from('fileinfo fi');
        $this->db->join('filedata d', 'fi.id = d.id', 'inner');
        $this->db->where('fi.id', $fileId);
        $query = $this->db->get();
        //$res = $this->utilitar_db->_getTableRow('filedata', 'id', $fileId);
        return $query->row();
    }

}
