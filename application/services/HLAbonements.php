<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * helps dealing with HL Abonements
 *
 * @author gureku
 */
class HLAbonements extends Service
{
    const ABONEMENTS_TBL = 'clients_abonements';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('utilitar_db');
    }

    // client_abonements fields: id  client_id  visits_max_count  price  visits_count  valid_from  is_active  created


    // from the set of abonements returns the actual one
    public function get_client_abonements($client_id, $active_only = false)
    {
        if ($active_only)
        {   return $this->utilitar_db->_getTableContents(HLAbonements::ABONEMENTS_TBL, 'valid_from', 'client_id', $client_id, 'is_active', 1);
        }
        else
        {   return $this->utilitar_db->_getTableContents(HLAbonements::ABONEMENTS_TBL, 'valid_from', 'client_id', $client_id);
        }
    }


    // from the set of abonements returns the actual one.
    public function get_current_abonement($client_id)
    {
        $user_abonements = $this->get_client_abonements($client_id, true);
        return (count($user_abonements) > 0)? $user_abonements[0] : null;
    }

    // this method automatically identifies what is the CURRENT abonement, and adjusts it. On failre returns FALSE.
    // increments visits count of the abonement.
    public function add_visit($client_id)
    {
        $ci = &get_instance();

        $abonement = $this->get_current_abonement($client_id);
        if (!$abonement)
        {   log_message('error', "ERROR: add_visit(client_id:$client_id): no current abonement found!");
            return;
        }

        $this->db->where('id', $abonement->id);
        $this->db->where('client_id', $client_id);
        $this->db->set('visits_count', 'visits_count+1', FALSE); // increment
        
        $this->db->update(HLAbonements::ABONEMENTS_TBL);

        $abonement = $this->get_current_abonement($client_id);
        if ($abonement->visits_count >= $abonement->visits_max_count)
        {   $this->deactivate_abonement($abonement->id);
        }
    }

    // this method automatically identifies what is the CURRENT abonement, and adjusts it. On failre returns FALSE.
    // increments visits count of the abonement.
    public function create_abonement($client_id, $valid_from_date, $max_count)
    {
        $valid_from_date = date("Y-m-d", strtotime($valid_from_date)); // leave date-only, i.e. no time.
        $data = array(  'client_id'         => $client_id,
                        'is_active'         => true,
                        'valid_from'        => $valid_from_date,
                        'visits_max_count'  => $max_count,
                     );
        $ci = &get_instance();
        $ci->db->insert(HLAbonements::ABONEMENTS_TBL, $data);
    }

    // this method automatically identifies what is the CURRENT abonement, and adjusts it. On failre returns FALSE.
    // decrements visits count of the abonement.
    public function revoke_visit($client_id)
    {
        $abonement = $this->get_current_abonement($client_id);
        if (!$abonement)
        {   log_message('error', "ERROR: revoke_visit(client_id:$client_id): no current abonement found!");
            return;
        }

        $this->db->where('id', $abonement->id);
        $this->db->where('client_id', $client_id);
        $this->db->where('visits_count > 0'); // ensure to not to decrement to get negative values.
        $this->db->set('visits_count', 'visits_count-1', FALSE); // decrement
        $this->db->set('is_active', true);

        $this->db->update(HLAbonements::ABONEMENTS_TBL);
    }

    // this might be called externally by manually disabling abonement, or internally: by exhausting all its visits.
    public function deactivate_abonement($abonement_id)
    {
        $this->db->where('id', $abonement_id);
        $this->db->update(HLAbonements::ABONEMENTS_TBL, array('is_active' => false));
    }
}
