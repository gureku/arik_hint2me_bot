<?php defined('BASEPATH') OR exit('No direct script access allowed');

class HistoryBaseService extends Service
{
    // 1. Entity types supported by history:
    const ENTITY_TYPE__TASK         = 1; // not used in our bot.

    const ENTITY_TYPE__VISIT        = 2;
    const ENTITY_TYPE__ABONEMENT    = 3;

    // 2. Event types supported by history:
    const EVENT_CREATED = 1; // db event
    const EVENT_UPDATED = 2; // db event
    const EVENT_REMOVED = 3; // db event

    const EVENT_PAID_FOR    = 4; // financial event
    const EVENT_COMMENTED   = 5; // when somebody leaves a comment
    const EVENT_FILE_ATTACHED = 6; // on file attached
    const EVENT_FILE_DETACHED = 7; // on file detached


    public function __construct()
    {
        parent::__construct();
        $this->load->model('utilitar_db');
    }


    //
    // Returs id (integer) of event created!
    //
    // $event_type              - one of HistoryBaseService::EVENT_* constants.
    // $initiator_account_id    - might be null - so that's System (job) account.
    // $entity_type             - ticket/company/etc.
    // $entity_id               - is about ticket/company/whatever, to what that event is related to.
    // $extra_data              - is string holding related data.
    protected function create_event($event_type, $entity_type, $initiator_account_id, $entity_id, $extra_data = null, $extra_data_integer = null, $extra_data_integer2 = null)
    {
        $this->db->insert('event', array(   'type_event'            => $event_type,
                                            'type_entity'           => $entity_type,
                                            'initiator_account_id'  => $initiator_account_id,
                                            'entity_id'             => $entity_id,
                                            'extra_data'            => $extra_data,
                                            'extra_data_integer'    => $extra_data_integer,
                                            'extra_data_integer2'   => $extra_data_integer2,
                                        )
                          );

        return $this->db->insert_id();
    }

    public function get_event($event_id)
    {   return $this->utilitar_db->_getTableRow('event', 'id', $event_id);
    }

    // gets all events, w/out filtering specific entity types!
    public function get_events($order_by = 'event.created DESC')
    {
        return $this->utilitar_db->_getTableContents('event', $order_by);
    }

    // to be called before returning any history event to a front-end.
    private function deserialize_basic_event(&$event)
    {
        $res = array('id'           => $event->id,
                     'entity_id'    => $event->entity_id,
                     'created'      => $event->created,
                     'user'         => $event->account_name,
                     'action'       => $event->action,
                     'extra_data'   => $event->extra_data,
                     'extra_data_integer' => $event->extra_data_integer,
                     );

        if (HistoryBaseService::EVENT_COMMENTED == $event->type_event)
        {   //$res['action'] = $event->extra_data;
        }

        return $res;
    }

    // enriching basic history event:
    private function deserialize_project_event($ticket_event)
    {
/*        $statuses = array(  Dictionaries::TICKET_STATUS_DRAFT               => 'Черновик',
                            Dictionaries::TICKET_STATUS_OPEN                => 'Опубликовать',
                            Dictionaries::TICKET_STATUS_ASSIGNED            => 'Взята в работу',
                            Dictionaries::TICKET_STATUS_MOVING_TO_PLACE     => 'Специалист выехал',
                            Dictionaries::TICKET_STATUS_ARRIVED_TO_PLACE    => 'Специалист прибыл',
                            Dictionaries::TICKET_STATUS_NEED_CLARIFICATION  => 'Требуется уточнение',
                            Dictionaries::TICKET_STATUS_READY_PARTIALLY     => 'Готова частично',
                            Dictionaries::TICKET_STATUS_READY_COMPLETELY    => 'Готова полностью',
                            Dictionaries::TICKET_STATUS_CLOSED              => 'Закрыта',
                         );*/

        $row = $this->deserialize_basic_event($ticket_event);

        // upon commment do the following:
        if (HistoryBaseService::EVENT_COMMENTED == $ticket_event->type_event)
        {
            $row['text']    = $row['extra_data'];
            $row['status']  = Dictionaries::get_task_status_name_by_id($ticket_event->extra_data_integer);
        }
        else
        {   $row['status']  = $row['extra_data'];;
        }

        $row['address'] = rand(1, 5) % 2 ? "Лубянка ".random_string('numeric', 2) : "Московский пр.".random_string('numeric', 2);
        $row['date'] = $row['created'];

        //------------------------------------------------+
        // renamings: later do that right in the client-side code:
//        $row['text'] = $row['action'];
//        if (!$ticket_event->extra_data_integer)
//        {   $row['status']  = 'Создание';
//        }
//        $row['status']  = $row['extra_data'];//Dictionaries::get_task_status_name_by_id(rand(1, 4));
        //------------------------------------------------+

        // later do that right in the client-side code. Now: everything else but comment-events are unified for the client.
        $row['type'] = (HistoryBaseService::EVENT_COMMENTED == $ticket_event->type_event) ? 1 : 2;

        unset($row['created']);
        unset($row['account']);

        return $row;
    }

    // formats events to show to client
    public function wrap_events_for_client(&$events)
    {
        $res = array();
        foreach ($events as $event)
        {
            switch ($event->type_entity)
            {
                case HistoryBaseService::ENTITY_TYPE__TASK:
                    $res[] = $this->deserialize_project_event($event);
                    break;

                default:
                    log_message('error', " ---- EVENT ENTITY TYPE '$event->type_entity' is UNKNOWN, so using basic set of events properties only.");
                    $res[] = $this->deserialize_basic_event($event);
                    break;
            }
        }

        return $res;
    }

    private function _historyevents_filter_to_DB($entity_id, $entity_type)
    {
        if ($entity_id > 0)
        {   $this->db->where('entity_id', $entity_id);
        }
        
        $this->db->where('type_entity', $entity_type);
    }

    public function get_events_array__for_entity($entity_id, $entity_type, $pageIndex, $pageSize)
    {
/*
  SELECT event.*, IFNULL(users.username, "Система") AS account_name
  FROM event
  LEFT JOIN users ON users.id = event.initiator_account_id
  ORDER BY event.created DESC
 */
        $this->db->select('id');
        $this->_historyevents_filter_to_DB($entity_id, $entity_type);
        $this->db->from('event');
        $totalRows = $this->db->count_all_results();

        $this->db->select($this->db->dbprefix.'event.*, '.$this->db->dbprefix.'event_type.name AS action, IFNULL('.$this->db->dbprefix.'users.username, "Система") AS account_name');
        $this->_historyevents_filter_to_DB($entity_id, $entity_type);
        $this->db->join('users', 'users.id = event.initiator_account_id', 'left');
        $this->db->join('event_type', 'event_type.id = event.type_event', 'left');

        $this->db->limit($pageSize, $pageIndex * $pageSize);

        $this->db->order_by('event.created DESC');
        $query = $this->db->get('event');

//        $rows = Utilitar_db::safe_resultSetArray($query);
        $rows = Utilitar_db::safe_resultSet($query);

        return new SearchResults($rows, $totalRows);
    }

    // Returns events happened AFTER the datetime specified.
    // Optionally may filter by specific entity_id.
    public function get_events_after($datetime, $entity_type, $entity_id = null)
    {
/*
  SELECT event.*, IFNULL(users.username, "Система") AS account_name
  FROM event
  LEFT JOIN users ON users.id = event.initiator_account_id
  WHERE event.created > $datetime
  ORDER BY event.created
 */
        $this->db->select($this->db->dbprefix.'event.*, '.$this->db->dbprefix.'event_type.name AS action, IFNULL('.$this->db->dbprefix.'users.username, "Система") AS account_name');
        $this->_historyevents_filter_to_DB($entity_id, $entity_type);
        $this->db->join('users', 'users.id = event.initiator_account_id', 'left');
        $this->db->join('event_type', 'event_type.id = event.type_event', 'left');

        $this->db->where('event.created > ', $datetime);

        // NOTE:    It is A MUST to order by created date and not in reverse order (i.e. not in "event.created DESC"),
        //          otherwise proxy->aggregate_events() will be broken!!!
        $this->db->order_by('event.created');

        $query = $this->db->get('event');

        return Utilitar_db::safe_resultSet($query);
    }

    // returns events of certain EVENT types or all types.
    // $event_types_array - list of event type IDs!
    public function get_events_by_event_type($event_types_array, $entity_id = 0, $order_by = 'event.created DESC')
    {
        if (!$event_types_array || !is_array($event_types_array))
        {
            if ($entity_id <= 0)
            {   return $this->get_events($order_by);
            }
            else
            {   return $this->utilitar_db->_getTableContents('event', $order_by, 'entity_id', $entity_id);
            }
        }

        if ($entity_id > 0)
        {   $this->db->where('entity_id', $entity_id);
        }

        // choose only specific EVENT types:
        $this->db->where_in('type_event', $event_types_array);
        $this->db->order_by($order_by);

        $query = $this->db->get('event');

        return Utilitar_db::safe_resultSet($query);
    }

    // returns events of certain ENTITY type
    public function get_events_by_entity_type($entity_type, $order_by = 'event.created DESC', $entity_id = null)
    {
        log_message('error', "History: step2");
        // choose only specific ENTITY types:
        $this->db->where('type_entity', (int)$entity_type);
        if ($entity_id)
        {   $this->db->where('entity_id', (int)$entity_id);
        }
        $this->db->order_by($order_by);

        $query = $this->db->get('event');

        $ttt = Utilitar_db::safe_resultSet($query);
        log_message('error', "History: step3: ".print_r($ttt, true));
        return $ttt;
    }
}
