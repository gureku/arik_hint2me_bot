<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once (APPPATH . '/services/HistoryBaseService.php');


// specific to HL-bot events: visits, abonements, payments.
class HLHistoryService extends HistoryBaseService
{
    public function __construct()
    {   parent::__construct();
    }

    public function add_visit($client_id, $visit_date = null)
    {
        // we MUST pass version on creation (only!). Otherwise the "version" will be incremented inside the add_update() function twice.
        return $this->_register_event(HistoryBaseService::EVENT_CREATED, HistoryBaseService::ENTITY_TYPE__VISIT, $client_id, $visit_date, null, 1); // entity creation is the only event where the version to be passed in EXPLICITLY.
    }

    public function abonement_add($client_id, $abonement_start_date, $visits_count)
    {
        // we MUST pass version on creation (only!). Otherwise the "version" will be incremented inside the add_update() function twice.
        return $this->_register_event(HistoryBaseService::EVENT_CREATED, HistoryBaseService::ENTITY_TYPE__ABONEMENT, $client_id, $abonement_start_date, null, 1, $visits_count); // entity creation is the only event where the version to be passed in EXPLICITLY.
    }

    public function revoke_visit($client_id, $visit_date = null)
    {   return $this->_register_event(HistoryBaseService::EVENT_REMOVED, HistoryBaseService::ENTITY_TYPE__VISIT, $client_id, $visit_date);
    }

    // as we keep track of recent events processed, pass the "$soonest_event_id" in for that.
    public function get_events_after_id($soonest_event_id)
    {
        $this->db->where('id > ', $soonest_event_id);
        $this->db->where_in('type_entity', array(HistoryBaseService::ENTITY_TYPE__VISIT, HistoryBaseService::ENTITY_TYPE__ABONEMENT));
        $this->db->order_by('created');

        $query = $this->db->get('event');

        return Utilitar_db::safe_resultSet($query);
    }


    // The method tracks also ticket's current status.
    private function _register_event($event_type, $entity_type, $ticket_id, $extra_data = null, $status_id = null, $version = null, $extra_data_integer2 = null)
    {
        $current_user = WizardHelper::get_current_user();

        $initiator_account_id = $current_user ? $current_user->tg_user_id : NULL;
        return $this->create_event($event_type, $entity_type, $initiator_account_id, $ticket_id, $extra_data, $status_id, $extra_data_integer2);
    }
}
