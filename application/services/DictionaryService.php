<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *
 * @author gureku
 */
class DictionaryService extends Service
{
    public function __construct()
    {
        parent::__construct();
        // $this->load->model('utilitar_db');
    }

    public static function _to_key_value_array(&$rows, $key_name = 'id', $value_name = 'name')
    {
        $res = array();
        foreach ($rows as $row)
        {   $res[$row->$key_name] = $row->$value_name;
        }

        return $res;
    }

    public static function _to_key_value_array_from_array(&$rows, $key_name = 'id', $value_name = 'name')
    {
        $res = array();
        foreach ($rows as $row)
        {   $res[$row[$key_name]] = $row[$value_name];
        }

        return $res;
    }

    public static function _to_id_name_array(&$rows, $db_rowname1, $db_rowname2, $db_rowname3 = null)
    {
        $res = array();
        foreach ($rows as $row)
        {   $tmp = array('id' => $row->$db_rowname1, 'name' => $row->$db_rowname2);
            if ($db_rowname3) // allow retrieving more than just 2 columns.
            {   $tmp[$db_rowname3] = $row->$db_rowname3;
            }
            $res[] = $tmp;
        }

        return $res;
    }

    public static function _to_id_name_array_from_array(&$rows, $db_rowname1, $db_rowname2)
    {
        $res = array();
        foreach ($rows as $row)
        {   $res[] = array('id' => $row[$db_rowname1], 'name' => $row[$db_rowname2]);
        }

        return $res;
    }

    // opposite to "_to_key_value_array()", this creates keys with full DB row, instead of single value.
    public static function _to_key_rowvalue_array(&$rows, $key_name)
    {
        $res = array();
        foreach ($rows as $row)
        {   $res[$row->$key_name] = $row;
        }

        return $res;
    }
}
