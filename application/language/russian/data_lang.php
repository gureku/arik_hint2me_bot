<?php

$lang['mnu.about']              = 'about us';
$lang['mnu.clientsolutions']    = 'client solutions';
$lang['mnu.properties']         = 'property listings';
$lang['mnu.market']             = 'market studies';
$lang['mnu.contacts']           = 'contact us';
$lang['mnu.news']               = 'news';
$lang['mnu.blog.entries']       = 'Постинги';
$lang['mnu.blog.options']       = 'Настройки';
$lang['mnu.tr_schedule.options']= 'Настройки';

$lang['js.contact.link']        = 'For further information, please, contact us';


// Russian texts go here:
$lang['mnu.about.ru']           = 'о компании';
$lang['mnu.clientsolutions.ru'] = 'услуги компании';
$lang['mnu.properties.ru']      = 'список проектов';
$lang['mnu.market.ru']          = 'обзор рынка';
$lang['mnu.contacts.ru']        = 'контакты';
$lang['mnu.news.ru']            = 'новости';
$lang['mnu.news.u.ru']          = 'Новости';
$lang['mnu.docsupload']         = 'Загрузка документов';
$lang['mnu.goodsupdate']        = 'Обновление товаров';


$lang['js.contact.link.ru']     = 'Если вы хотите получить дополнительную информацию, пожалуйста свяжитесь с нами';


// admin section translation:
$lang['admin.welcome']          = 'Консоль администратора';
$lang['admin.lougout']          = 'Выйти';
$lang['admin.menu']             = 'Страницы';
$lang['admin.menu.manage.hint'] = 'Добавить/удалить страницы';
$lang['admin.menu.pages']       = 'Страницы';
$lang['admin.menu.common']      = 'Прочее';
$lang['admin.menu.blog']        = 'Блог';
$lang['admin.menu.widgets']     = 'Widgets';


$lang['admin.menu.countries']   = 'Страны';
$lang['admin.menu.cities']      = 'Города';
$lang['admin.menu.airports']    = 'Аэропорты';
$lang['admin.menu.aircompanies']= 'Авиакомпании';
$lang['admin.menu.campagnes']   = 'Кампании';

$lang['admin.menu.order']       = 'Порядок отображения страниц';
$lang['admin.menu.pages.hint']  = 'Добавить/удалить страницы';
$lang['admin.menu.order.hint']  = 'Порядок отображения страниц';
$lang['admin.menu.childof']     = 'Эта страница входит в';
$lang['admin.menu.title']       = 'Название';
$lang['admin.menu.titlehint']   = 'Подсказка';
$lang['admin.menu.url.hint']    = 'Вводится только на английском; текст должен быть уникальным (среди подобных URL на сайте).';

$lang['admin.menu.topmost']   = '&lt; верхнее меню &gt;';;
$lang['admin.menu.undefined']   = '&lt; не определено &gt;';

$lang['admin.page.create']      = 'создать страницу';
$lang['admin.page.create.u']    = 'Создать страницу';
$lang['admin.page.edit.u']      = 'Редактировать страницу \'%s\'';
$lang['admin.plainhtml']        = 'простой HTML';
$lang['admin.richedit']         = 'rich edit';

$lang['admin.menu.albums']      = 'Альбомы';
$lang['admin.menu.albums.manage']   = 'Управление альбомами';
$lang['admin.menu.albums.hint'] = 'Управление изображениями, фотографиями';


$lang['admin.menu.news.manage'] = 'Управление новостями';
$lang['admin.menu.campaignes.manage'] = 'Управление кампаниями';
$lang['admin.menu.docs.manage'] = 'Управление загруженными документами';
$lang['admin.menu.docs.types'] = 'Типы документов...';
$lang['admin.menuorder.manage'] = 'Управление порядком отображения страниц';
$lang['admin.menu.docs.list'] = 'Загруженные документы';
$lang['admin.docs.nofiles']   = 'Загруженных документов нет.';
$lang['admin.menu.news.list']   = 'Новости';
$lang['admin.menu.campaignes.list']   = 'Кампании';


$lang['admin.menu.developers']   = 'Застройщики';
$lang['admin.menu.buildings']   = 'Объекты';
$lang['admin.menu.sections']   = 'Секции';
$lang['admin.menu.apartments']   = 'Квартиры';


$lang['admin.menu.developers.list']   = 'Список застройщиков';
$lang['admin.menu.developers.manage']   = 'Управление застройщиками';


$lang['admin.spacelimited']   = '<font color=red>Внимание: </font> место на сервере ограничено.<br />Пожалуйста, удаляйте ненужные файлы заблаговременно.';


$lang['admin.menu.users']       = 'Пользователи';
$lang['admin.menu.users.hint']  = 'Управление аккаунтами пользователей';
$lang['admin.users.registered'] = 'Существующие аккаунты';
$lang['admin.menu.roles']       = 'Роли пользователей';
$lang['admin.menu.roles.hint']  = 'Управление ролями пользователей';
$lang['admin.menu.groups']       = 'Группы';

$lang['admin.albums.viewphotos']= 'просмотреть изображения...';

$lang['admin.edit']             = 'редактировать';
$lang['admin.delete']           = 'удалить';
$lang['admin.create']           = 'создать';
$lang['admin.save']             = 'Сохранить';
$lang['admin.cancel']           = 'Отменить';

$lang['admin.confirmdelete']    = 'Вы уверены, что хотите удалить %s?';
$lang['admin.confirmdelete.s']    = 'Вы уверены, что хотите удалить';
$lang['admin.confirmdelete.apt']    = 'Вы уверены, что хотите удалить квартиры согласно указанной дате?';
$lang['admin.confirm.userdelete']   = 'Аккаунт будет удалён. Продолжить?';
$lang['admin.registered']       = 'зарегистрирован';
$lang['admin.account']          = 'Аккаунт';
$lang['admin.name_fio']         = 'ФИО';
$lang['admin.phone']            = 'Телефон';
$lang['admin.userstatus']       = 'Статус аккаунта';
$lang['admin.pass']             = 'Пароль';
$lang['admin.pass']             = 'Пароль';
$lang['admin.pass.current']     = 'Текущий пароль';
$lang['admin.pass.retype']      = 'Пароль (заново)';

$lang['admin.status.active']    = 'Активен';
$lang['admin.status.inactive']  = 'Заблокирован';



$lang['admin.category.album']   = 'альбом';
$lang['admin.category.user']    = 'пользователя';
$lang['admin.category.role']    = 'роль';

$lang['admin.album.settoproduct']   = 'выбрать для продукта';
$lang['admin.album.name']       = 'Название альбома';
$lang['admin.album.photos']     = 'Список изображений';
$lang['admin.album.nophotos']   = 'В альбоме изображений нет.';
$lang['admin.album.uploadphotos']   = 'Загрузить изображения';


$lang['admin.photo.assign']     = 'назначить';


$lang['admin.tmpl.addsection']  = "Добавить секцию к объекту '%s'";
$lang['admin.tmpl.create']          = 'создать новый %s';
$lang['admin.tmpl.create.f']        = 'создать новую %s';
$lang['admin.tmpl.create.m']        = 'создать нового %s';
$lang['admin.tmpl.create.m.u']      = 'Создать нового %s';

$lang['admin.tmpl.delete']          = 'удалить %s';
$lang['admin.tmpl.edit']            = 'Редактировать %s';

$lang['admin.no.user.duplications']    = '<b>Внимание:</b> имя аккаунта и email должны быть уникальны по порталу: пересечений не разрешается!';

$lang['admin.addcontact']           = 'добавить контакт...';
$lang['admin.addbuilding']          = 'создать объект...';
$lang['admin.buildings.none']       = 'Объектов нет.';
$lang['admin.contacts.none']        = 'Контактов нет.';

$lang['admin.availableelements']= 'Доступные элементы';
$lang['admin.add.to.group']     = 'добавить в выбранную группу';
$lang['admin.remove.from.group']     = 'удалить из выбранной группы';
$lang['admin.group.members']     = 'Члены группы';

$lang['button.upload.picture']        = 'Загрузить изображение';
$lang['button.save']        = 'Сохранить';
$lang['button.apply']       = 'Применить';
$lang['button.addsave']     = 'Добавить/Сохранить';
$lang['button.cancel']      = 'Отменить';
$lang['button.upload']      = 'Загрузить';
$lang['button.return']      = 'Вернуться';
$lang['button.change']      = 'Изменить';
$lang['button.delete']      = 'Удалить';
$lang['button.refresh']     = 'Обновить';
$lang['button.search']      = 'Найти';

$lang['button.next']        = 'Далее';
$lang['button.back']        = 'Назад';
$lang['button.pay']         = 'Оплатить';


$lang['list.none']        = 'Выберите из списка...';

$lang['admin.changepass']       = 'Изменить пароль...';
$lang['admin.exclusive']        = 'Доступ к экслклюзивным записям';
$lang['admin.exclusive.yes']    = 'Есть';
$lang['admin.exclusive.no']     = 'Нет';
$lang['admin.comments']         = 'Комментарии';

$lang['admin.changepass.user']      = 'Изменить пароль пользователя \'%s\'';


$lang['age.short']      = '%d';
$lang['age.long']       = '%d,%d';


$lang['const.public_access']    = 'Публичный доступ';
$lang['const.exclusivity']  = 'Эксклюзивность';
$lang['const.website_name'] = 'Радуга. Фитнес-школа. Детский клуб.'; // see tank_auth same variable.
$lang['reservation.status'] = 'Статус бронирования';


$lang['const.document.changetype']= 'Изменить тип выбранных';
$lang['const.quarter']      = ' кв.';
$lang['const.name']         = 'Название';
$lang['const.description']  = 'Описание';
$lang['const.link']         = 'Ссылка';
$lang['const.address']      = 'Адрес';
$lang['const.deliveryAddress']= 'Адрес доставки';
$lang['const.phone']        = 'Телефон';
$lang['const.developer']    = 'Застройщик';
$lang['const.contacts']     = 'Контакты';
$lang['const.building']     = 'Объект';
$lang['const.buildings']    = 'Объекты';
$lang['const.sections']     = 'Секции';
$lang['const.districts']    = 'Районы';
$lang['const.control']      = 'Управление';
$lang['const.notes']        = 'Заметки';
$lang['const.build.readyness']  = 'Готовность';
$lang['const.savesection']  = 'Добавление квартир доступно после сохранения секции';
$lang['const.built']        = 'Сдан';
$lang['const.rating']       = 'Рейтинг';
$lang['const.rating.hint']  = 'Допустимые значения: 1, 2 и 3';
$lang['const.district']     = 'Район';
$lang['const.built.not']    = 'Строится';
$lang['const.benefits']     = 'Преимущества';
$lang['const.descr.common'] = 'Общее описание';
$lang['const.descr.tech']   = 'Техническое описание';
$lang['const.comments']     = 'Комментарии';
$lang['const.comments.disabled']    = 'no commenting';
$lang['const.comments.one_level']   = 'one level';
$lang['const.comments.any_level']   = 'any level';





$lang['const.documents.choose']      = 'Пожалуйста, выберите один и более документов для изменения типа.';
$lang['const.doctype.choose']   = 'Пожалуйста, выберите тип документа из списка.';
$lang['const.document.name']    = 'Название документа';
$lang['const.document.name']    = 'Тип документа';
$lang['const.doctype.none']     = 'Нетипизированный';
$lang['const.date.released']    = 'Дата сдачи';
$lang['const.date.built']       = 'Дата постройки';
$lang['const.singlemeter']      = 'Цена одного метра';
$lang['const.costtotal']        = 'Общая цена';
$lang['const.shaxmatka']        = 'Шахматная ведомость';
$lang['const.releasedate']      = 'Срок сдачи';
$lang['const.status']           = 'Статус';
$lang['const.promo']            = 'Promo';
$lang['const.album.decoration'] = 'Фото отделки';
$lang['const.album.building']   = 'Фото строительства';
$lang['const.sections.savefirst']  = 'Заметка: секции будут доступны после сохрания объекта.';
$lang['const.album.savefirst']  = 'Заметка: работа с фотографиями будет доступна после сохрания объекта.';
$lang['const.choosealbum']      = 'Выбрать фотоальбом...';
$lang['const.viewpics']         = 'Просмотр фотографий';
$lang['const.also.may']         = 'Также можно';
$lang['const.create.album']     = 'создать альбом';
$lang['const.choose.decoration']= 'Кликните на альбоме для выбора в качестве Альбома Отделки';
$lang['const.choose.build']     = 'Кликните на альбоме для выбора в качестве Альбома Строительства';
$lang['const.roomscount']       = 'Кол-во комнат';
$lang['const.is.commerce']      = 'Коммерция';
$lang['const.is.exclusive']     = 'Экслюзив';
$lang['const.is.reserved']      = 'Забронирована';
$lang['const.yes']              = 'Да';
$lang['const.no']               = 'Нет';
$lang['const.section']          = 'Секция';
$lang['const.floor']            = 'Этаж';
$lang['const.areatotal']        = 'S общ, м.кв.';
$lang['const.arealiving']       = 'S жил, м.кв.';
$lang['const.pricemeter']       = '$/m2';
$lang['const.pricetotal']       = '$/total';
$lang['const.mgr']              = 'Менеджер';
$lang['const.archive']          = 'Удалить квартиры с датой создания вплоть до: ';
$lang['const.uploaded.not']     = 'Не загружена';
$lang['const.delete.shaxmatka'] = 'Удалить шахматную ведомость';
$lang['const.obj.photo.location'] = 'Фото расположения';
$lang['const.obj.photo.small']  = 'Фото объекта (маленькое)';
$lang['const.obj.photo.big']    = 'Фото объекта (большое)';
$lang['const.obj.coordinates']  = 'Координаты (Yandex):';
$lang['const.docs.uploaded']    = 'Загруженные файлы';
$lang['const.apt.photo']        = 'Фото';
$lang['const.apt.photo.delete'] = 'Удалить фото';
$lang['const.email']            = 'Email';
$lang['const.phone']            = 'Тел.';
$lang['const.phone2']           = 'Тел2.';
$lang['const.phoneext']         = 'ext.';
$lang['const.free']             = 'Свободна';
$lang['const.reserved']         = 'Бронь';
$lang['const.developer.choose'] = 'Выбрать застройщика';
$lang['const.developer.all']    = 'все застройщики';
$lang['const.news.date']        = 'Дата создания';
$lang['const.emailnotif.subj']       = 'Радуга: новое сообщение на сайте';
$lang['const.emailnotif.body']       = "Пользователь '%s' оставил сообщение:<hr><font color='blue'>%s</font><hr><br>Администрировать его можно по адресу: %s<br><br>С уважением, ваш Движок ;)";
$lang['const.payment.guide']    = '<p>Рекомендуем <strong>в деталях платежа</strong> указать ФИО ребенка, дату и назначение платежа (мероприятия).</p><br /><p>При успешном окончании платежа Вам буден выдан индивидуальный номер платежа - запишите его, пожалуйста - он может пригодиться.</p>';
$lang['const.request.guide']    = '<p>Выберите те <strong>занятия</strong>, которые могут Вас заинтересовать, добавьте комментарий и оставьте контакты (email и/или телефон) - и мы с Вами обязательнон свяжемся!</p>';
$lang['admin.upload']           = 'загрузить';
$lang['const.checkout.title']   = 'Оплата услуг';
$lang['const.training.request.title']   = 'Радуга: Запись на занятие (%s)';


$lang['const.thanksforposting'] = 'Спасибо за сообщение!<br>
<br>
После проверки модератора сообщение появится на сайте.<br>
Оставайтесь на связи!';

//----------------------------------------------------------------+
// ICE TWICE: for translation:
$lang['season.hits']    = 'Хиты сезона';
$lang['novelties']      = 'Новинки';
$lang['news.all']       = 'Все новости';
$lang['cart.navigateTo']= 'Перейти в корзину';
$lang['cart.inCart']    = 'В корзине товаров';
$lang['order.price']     = 'Сумма заказа';
$lang['order.price.discounted'] = 'Сумма заказа с учетом скидки';

$lang['cart.price']     = 'на сумму';
$lang['cart.currency']  = 'руб';
$lang['cart.add']       = 'КУПИТЬ!';
$lang['cart.addedInto'] = 'Товар добавлен в';
$lang['cart.intoCart']  = 'корзину';
$lang['cart.isEmpty']   = 'Ваша корзина пуста.';

$lang['collection.from']= 'из коллекции';
$lang['collection.moreFrom']= 'Еще наклейки из коллекции';

$lang['recalculate']    = 'Пересчитать';

$lang['catalog.large']  = 'Каталог Товаров';

$lang['purchase.prepare'] = 'Оформление покупки';
$lang['cabinet']        = 'Личный кабинет';
$lang['cabinet.enter']  = 'Войти в личный кабинет';
$lang['login']          = 'Войти';
$lang['logout']         = 'Выйти';
$lang['device.ask']     = 'Найти свой девайс';
$lang['design.yourself']= 'Нарисовать наклейку самому';
$lang['sticker']        = 'Наклейка';
$lang['for']            = 'для';

$lang['my.purchase']    = 'Мои покупки';
$lang['my.contacts']    = 'Мои контактные данные';
$lang['my.designs']     = 'Мои дизайны';
$lang['password.change']= 'Сменить пароль';
$lang['found.no.page']  = '<h2>Уп-с, похоже Вы нашли нашу несуществующую страницу!</h2><br /><h3>Попробуйте выбрать из этих списков:</h3>';


$lang['menu.about']     = 'О нас';
$lang['menu.news']      = 'Новости';
$lang['menu.main']      = 'Главная';
$lang['menu.faq']       = 'FAQ';
$lang['menu.forum']     = 'Форум';
$lang['menu.contacts']  = 'Контакты';
$lang['menu.video']     = 'Видео';
$lang['menu.catalog']   = 'Каталог';
$lang['menu.partners']  = 'Партнеры';
$lang['menu.actions']   = 'Акции';

$lang['wiz.cart']           = 'Корзина';
$lang['wiz.authorization']  = 'Авторизация';
$lang['wiz.delivery']       = 'Способ доставки и контактные данные';
$lang['wiz.approval']       = 'Подтверждение заказа';

$lang['search.results']     = 'Результаты поиска';
$lang['search.simple']      = 'Простой поиск';
$lang['search.advanced']    = 'Расширенный поиск';

$lang['category.designs']       = 'Дизайны';
$lang['category.devices']       = 'Устройства';
$lang['category.categories']    = 'Категории';
$lang['category.news']          = 'Новости';

$lang['const.all_devices']  = 'Все устройства';
$lang['const.all_images']   = 'Все изображения';
$lang['const.device']       = 'Устройство';
$lang['const.price']        = 'Цена';
$lang['const.collection']   = 'Коллекция';
$lang['const.from']         = 'от';
$lang['const.till']         = 'до';

$lang['wm.attestatcheck']   = 'Проверить аттестат';


$lang['price.rub']   = 'Цена (руб)';
$lang['sum.rub']   = 'Сумма (руб)';
$lang['items.count'] = 'Количество (шт)';
$lang['wm.attestatcheck']   = 'Проверить аттестат';
$lang['wm.attestatcheck']   = 'Проверить аттестат';

$lang['err.positiveNumber']   = 'Укажите целое положительное число';


$lang['courier']           = 'Курьером';
$lang['courier.area']      = 'по Санкт-Петербургу и Москве';
$lang['plain_letter']      = 'Простым письмом (Почта России)';
$lang['ordered_letter']    = 'Заказным письмом (Почта России)';
$lang['self_delivery']     = 'Самовывоз';
$lang['self_delivery.area']= 'г. Санкт-Петербург';


$lang['order.delivered']        = 'доставлено';
$lang['order.not_paid']         = 'заказ не оплачен';
$lang['order.in_delivery_paid'] = 'в доставке (заказ оплачен)';
$lang['order.unknown']          = 'уточните у оператора';

$lang['const.acceptTerms']      = 'Я принимаю условия';
$lang['const.quality']          = 'Вы можете загрузить <strong>jpg</strong>, <strong>png</strong>, 
                                    <strong>CDR</strong>, <strong>EPS</strong> или <strong>tiff</strong> (300 dpi, 120х120 мм - для наклейки на телефон, 270х370 мм - для наклейки на iPad/ноутбук).<br>
                                    В противном случае мы не гарантируем качество.';

$lang['hint.chooseFile']        = 'Выберите файл в формате <b>.jpg</b> или <b>.png</b>';
$lang['hint.accept']            = 'Вам необходимо согласиться с требованиями к загружаемым изображениям.';
$lang['hint.setZipcode']        = 'Укажите почтовый индекс';

$lang['const.zipcode']          = 'Почтовый индекс';

$lang['const.pass']             = 'Текущий пароль';
$lang['const.pass.new']         = 'Новый пароль';
$lang['const.pass.retype']      = 'Повторите новый пароль';


//----------------------------------------------------------------|
